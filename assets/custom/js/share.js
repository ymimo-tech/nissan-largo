
'use strict';

var timeout;

function start_longpolling(){
	var evtSource = new EventSource(BaseUrl+'dashboard/longpolling');
	evtSource.addEventListener("sync", function(e) {
		var obj = JSON.parse(e.data);
		var exist=0;

		if(obj.newc.inbound.length>0){
			$.each(obj.newc.inbound, function (i,newc){
				if(newc.newrec){
					var title="New Inbound";
				}else{
					var title="Inbound Updated";
				}
				toastr['info']("<a href='"+BaseUrl+"inbound/detail/"+newc.id+"' target='_blank'>"+newc.nomor+"</a>",title);
			});
			//content_datatable.getDataTable().ajax.reload();
			exist=1;
		}

		if(obj.newc.outbound.length>0){
			$.each(obj.newc.outbound, function (i,newc){
				if(newc.newrec){
					var title="New Outbound";
				}else{
					var title="Outbound Updated";
				}
				toastr['info']("<a href='"+BaseUrl+"outbound/detail/"+newc.id+"' target='_blank'>"+newc.nomor+"</a>",title);
			});
			//content_datatable.getDataTable().ajax.reload();
			exist=1;
		}

		if(obj.newc.item.length>0){
			$.each(obj.newc.item, function (i,newc){
				if(newc.newrec){
					var title="New Item";
				}else{
					var title="Item Updated";
				}
				toastr['info']("<a href='"+BaseUrl+"referensi_barang' target='_blank'>"+newc.sku+"</a><br/>"+newc.nama,title);
			});
			//content_datatable.getDataTable().ajax.reload();
			exist=1;
		}

		if(obj.newc.supplier.length>0){
			$.each(obj.newc.supplier, function (i,newc){
				if(newc.newrec){
					var title="New Supplier";
				}else{
					var title="Supplier Updated";
				}
				toastr['info']("<a href='"+BaseUrl+"referensi_supplier' target='_blank'>"+newc.kd_supplier+"</a><br/>"+newc.nama,title);
			});
			//content_datatable.getDataTable().ajax.reload();
			exist=1;
		}

		if(exist){
			evtSource.close();
			start_longpolling();
		}

	}, false);
}

$(document).ready(function(){

	$('.filter-indicator').on('click', function(){
		clearTimeout(timeout);

		timeout = setTimeout(function(){
			$('.filter-container .show-hide').trigger('click');
		}, 100);
	});

	$('.show-hide').on('click', function(){
		var ctrl = $(this);
		if(ctrl.hasClass('expand')){
			$('.filter-indicator').html('Hide filter');
		}else{
			$('.filter-indicator').html('Show filter');
		}
	});

	$('input[id^="qty"]').bind('paste', function(e){
		e.preventDefault();
	});

	$('table').bind('paste', 'input[class^="qty-"]', function(e){
		e.preventDefault();
	});

	$('body').tooltip({
		selector: '[data-toggle=tooltip]'
	});

	$('.add-item').tooltip('show');

	$('.add_item').on('click', function(){
		$('.add-item').removeAttr('data-toggle');
		$('.add-item').tooltip('destroy');
	});

	//start_longpolling();
});



function base64_encode(data) {
	//  discuss at: http://phpjs.org/functions/base64_encode/
	// original by: Tyler Akins (http://rumkin.com)
	// improved by: Bayron Guevara
	// improved by: Thunder.m
	// improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: Rafal Kukawski (http://kukawski.pl)
	// bugfixed by: Pellentesque Malesuada
	//   example 1: base64_encode('Kevin van Zonneveld');
	//   returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
	//   example 2: base64_encode('a');
	//   returns 2: 'YQ=='

	var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
	var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
	ac = 0,
	enc = '',
	tmp_arr = [];

	if (!data) {
		return data;
	}

	do { // pack three octets into four hexets
		o1 = data.charCodeAt(i++);
		o2 = data.charCodeAt(i++);
		o3 = data.charCodeAt(i++);

		bits = o1 << 16 | o2 << 8 | o3;

		h1 = bits >> 18 & 0x3f;
		h2 = bits >> 12 & 0x3f;
		h3 = bits >> 6 & 0x3f;
		h4 = bits & 0x3f;

		// use hexets to index into b64, and append result to encoded string
		tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
	} while (i < data.length);

	enc = tmp_arr.join('');

	var r = data.length % 3;

	return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}

function post(path, params, method, newTab) {
	method = method || "post"; // Set method to post by default if not specified.

	// The rest of this code assumes you are not using a library.
	// It can be made less wordy if you use one.
	var form = document.createElement("form");
	form.setAttribute("method", method);
	form.setAttribute("action", path);

	if(newTab == 'Y')
	form.setAttribute("target", '_blank');

	for(var key in params) {
		if(params.hasOwnProperty(key)) {
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);

			form.appendChild(hiddenField);
		}
	}

	document.body.appendChild(form);
	form.submit();
}

function isEmail(email){
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	return re.test(email);
}

function isNumeric(e){
	//var key = window.event ? event.keyCode : event.which;
	var key = e;
	var evtobj = window.event? event : e;

	/*
	$('#' + e.target.id).bind("paste",function(e) {
	e.preventDefault();
});
*/

if (evtobj.shiftKey)
return false;

// if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 46
// || event.keyCode == 37 || event.keyCode == 39 || (event.keyCode >= 96 && event.keyCode <= 105)) {
// return true;
// }
if (key.keyCode == 8 || key.keyCode == 9 || key.keyCode == 46
	|| key.keyCode == 37 || key.keyCode == 39 || (key.keyCode >= 96 && key.keyCode <= 105)) {
		return true;
	}
	else if ( key.keyCode < 48 || key.keyCode > 57 ) {
		return false;
	}
	else return true;
}

var formatMoney = function(n, c, d, t){
	var c = isNaN(c = Math.abs(c)) ? 2 : c,
	d = d == undefined ? "." : d,
	t = t == undefined ? "," : t,
	s = n < 0 ? "-" : "",
	i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
	j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

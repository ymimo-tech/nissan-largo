
	'use strict';

	var MimoPeriode = {
		params: function(options){

			var periode = options.selector.periode,
				from = options.selector.from,
				to = options.selector.to,
				clear = options.selector.clear;

			var handler = {
				_initialize: function(){
					var self = this;

					var opt = [
						{value: 'custom', label: 'Custom'},
						{value: 'today', label: 'Today'},
						{value: 'this_week', label: 'This Week'},
						{value: 'this_month', label: 'This Month'},
						{value: 'this_year', label: 'This Year'},
						{value: 'yesterday', label: 'Yesterday'},
						{value: 'last_week', label: 'Last Week'},
						{value: 'last_month', label: 'Last Month'},
						{value: 'last_year', label: 'Last Year'}
					];

					var len = opt.length;
					var tpl = '<option value="{{ val }}">{{ label }}</option>',
						html = '';

					for(var i = 0; i < len; i++){
						html += tpl;
						html = html.replace(/{{ val }}/gi, opt[i].value);
						html = html.replace(/{{ label }}/gi, opt[i].label);
					}

					$(periode).html(html);

					$(periode).select2('destroy');
					$(periode).select2({
						allowClear: false,
						width: null
					});

					setTimeout(function(){
						$(periode).val('this_month').trigger('change');
					}, 300);

					$(periode).on('change', function(){

						var ctrl = $(this),
							val = ctrl.val();

						self._dateHandler(val);
					});

					$(from + ', ' + to).on('change', function(){
						$(periode).select2('val', 'custom');
					});

					$(clear).on('click', function(){
						$(from + ', ' + to).val('');
						$(periode).select2('val', 'custom');
					});
				},

				_dateHandler: function(val){

					if(val == 'today'){

						$(from).val(moment().format('DD/MM/YYYY'));
						$(to).val(moment().format('DD/MM/YYYY'));

					}else if(val == 'this_week'){

						var froms = moment().isoWeekday(1);
						$(from).val(froms.format('DD/MM/YYYY'));

						var tos = froms.add(6, 'd');
						$(to).val(tos.format('DD/MM/YYYY'));

					}else if(val == 'this_month'){

						var froms = moment().startOf('month');
						$(from).val(froms.format('DD/MM/YYYY'));

						var tos = moment().endOf('month');
						$(to).val(tos.format('DD/MM/YYYY'));

					}else if(val == 'this_year'){

						var froms = moment().startOf('year');
						$(from).val(froms.format('DD/MM/YYYY'));

						var tos = moment().endOf('year');
						$(to).val(tos.format('DD/MM/YYYY'));

					}else if(val == 'yesterday'){

						var dt = moment().add(-1, 'd');

						$(from).val(dt.format('DD/MM/YYYY'));
						$(to).val(dt.format('DD/MM/YYYY'));

					}else if(val == 'last_week'){

						var froms = moment().subtract(1,'weeks').isoWeekday(1); //play here if you want start from sunday - saturday
						$(from).val(froms.format('DD/MM/YYYY'));

						var tos = froms.add(6, 'd');
						$(to).val(tos.format('DD/MM/YYYY'));

					}else if(val == 'last_month'){

						var froms = moment().subtract(1,'months').startOf('month');
						$(from).val(froms.format('DD/MM/YYYY'));

						var tos = moment().subtract(1,'months').endOf('month');
						$(to).val(tos.format('DD/MM/YYYY'));

					}else if(val == 'last_year'){

						var froms = moment().subtract(1,'years').startOf('year');
						$(from).val(froms.format('DD/MM/YYYY'));

						var tos = moment().subtract(1,'years').endOf('year');
						$(to).val(tos.format('DD/MM/YYYY'));

					}
				},

				init: function(){
					var self = this;
					self._initialize();
				}
			}

			return handler;
		}
	}

var _App = function(){

    var initialize = function(){
        pageInteraction();
        warehouseFilterHandler();
    }

    var pageInteraction = function(){
        getOnlineUser();

        $('.dropdown-user').mouseenter(function(){
            getOnlineUser();
        })

    }

    var warehouseFilterHandler = function(){

        $('#warehouse-filter').select2();
        $('#warehouse-filter').val(Cookies.get('largo-warehouse-select')).trigger('change');
        $('#warehouse-filter').change(function(){
            Cookies.set('largo-warehouse-select', $(this).val());
        });

    }

    var getOnlineUser = function(){
        $.ajax({
            type:'GET',
            url:BaseUrl+'user/getonlineuser',
            success: function(response){
                obj = jQuery.parseJSON( response );
                $('#userOnline').html(" Online User : " + obj.result.online_user + " / " + obj.result.limit_user);
            },
            error: function(err){
                console.log(err)
            }
        })
    }

    return {
        init: function(){
            initialize();
        }
    }
}();


$(function () {
    _App.init();
});

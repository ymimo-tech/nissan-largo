var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
	
	var dataTable,
		dataTable1,
		dataTable2;
	
	var initialize = function(){
		initTable();
		initTableItem();
		initTablePacking();
	}	
	
	var initTable = function () {

        dataTable = new Datatable();
		
		var totalItem = 0,
			totalQty = 0;
		
        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getDetailPicking", // ajax source
					"data": {
						shipping_id: $('#data-table-form input[name="id"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true }
                ],
                "order": [
                    [2, "DESC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1, 2, 3, 4, 5, 6, 7]
					}
				]
            }
        });
	}
	
	var initTablePacking = function () {
		dataTable2 = new Datatable();

		dataTable2.init({
			src: $("#table-list-packing"),
			onSuccess: function (grid, response) {
				if(response.customGroupAction=='OK'){
					toastr['success'](response.customGroupActionMessage,"Success");
				}
			},
			onError: function(grid){

			},
			onDataLoad: function(grid) {

			},
			loadingMessage: 'Loading...',
			dataTable: {
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
				"dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
				"bStateSave": false,

				"lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getDetailPacking", // ajax source
					"data": {
						shipping_id: $('#data-table-form input[name="id"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					// { "orderable": false, "visible": false },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false },
					// { "orderable": true },
                    // { "orderable": true },
                    // { "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [3]
					},
					{
						"render": function(data, type, row){
							return '<div align="center"><button class="btn green btn-xs detail" type="button">\
										Show Detail\
									</button></div>';
						},
						"targets": [4]
					}
				]
            }
        });
    }
	
	var initTableItem = function () {

        dataTable1 = new Datatable();
		
		var totalItem = 0,
			totalQty = 0;
		
        dataTable1.init({
            src: $("#table-list-item"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getDetailItem", // ajax source
					"data": {
						shipping_id: $('#data-table-form input[name="id"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [4]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [6]
					},
					{
						"render": function(data, type, row){
							return '<div align="center"><button class="btn green btn-xs detail" type="button">\
										Show Detail\
									</button></div>';
						},
						"targets": [-1]
					}
				]
            }
        });
    }
	
	var childTableTpl = function(data){
		var tpl = '<div style="width: 90%; margin: 0 auto; text-align: left;">\
						<h4>Serial Number List</h4>\
				   </div>\
				   <table id="child-table-'+data[1]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
						<thead>\
							<tr>\
								<th><div align="center">No.</div></th>\
								<th><div align="center">Serial Number</div></th>\
								<th><div align="center">Qty</div></th>\
								<th><div align="center">Expired Date</div></th>\
								<th><div align="center">Date In</div></th>\
								<th><div align="center">Loaded By</div></th>\
								<th><div align="center">Action</div></th>\
							</tr>\
						</thead>\
						<tbody>\
						</tbody>\
				   </table>';
				   
		return tpl;
	}


	var childTablePacking = function(data){
		var tpl = '<div style="width: 90%; margin: 0 auto; text-align: left;">\
						<h4>Serial Number List</h4>\
				   </div>\
				   <table id="child-table-'+data[1]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
						<thead>\
							<tr>\
								<th><div align="center">No.</div></th>\
								<th><div align="center">Item Code</div></th>\
								<th><div align="center">Item Name</div></th>\
								<th><div align="center">Serial Number</div></th>\
								<th><div align="center">Qty</div></th>\
							</tr>\
						</thead>\
						<tbody>\
						</tbody>\
				   </table>';
				   
		return tpl;
	}
	
	var initChildTable = function(data){
		childTable = new Datatable();		
		
        childTable.init({
            src: $("#child-table-"+data[1]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"language": {
					"infoEmpty": ""
				},
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getScannedList", // ajax source
					"data": {
						shipping_id: data[1],
						id_barang: data[2]
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1,2,3,4]
					}
				]
            }
        });
	}

	var initChildTablePacking = function(data){
		childTable = new Datatable();
		console.log(data);		
		
        childTable.init({
            src: $("#child-table-"+data[1]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"language": {
					"infoEmpty": ""
				},
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getItemPacking", // ajax source
					"data": {
						shipping_id: data[4],
						packing_id: data[1]
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true , visible: false},
					{ "orderable": false, visible: false }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1,2,3,4]
					}
				]
            }
        });
	}
	
	var pageInteraction = function(){

		$(document).on('click','#data-table-cancel',function(e){

			e.preventDefault();
			
			if($(this).hasClass('disabled-link'))
				return false;

			var r = confirm("Anda yakin ingin menghapus serial number ini ?");
			if (r == true) {

				$.ajax({
					url:BaseUrl+page_class+'/cancel_item',
					method: "POST",
					data:
					{
						shipping_id:$('#data-table-form input[name="id"]').val(),
						serial_number:$(this).attr('data-id')
					},
					success: function(response){

						var button = $(document).find('.shown').find('button');

						button.trigger('click');
						button.trigger('click');

	                    toastr['success'](response.message,"Success");

					},
					error: function(err){
						console.log(err);
					}
				})

			} else {
				return false;
			}
		});
		
		$('.print-manifest').on('click', function(e){
            e.preventDefault();
			
			var ctrl = $(this);
				
			if(ctrl.hasClass('disabled-link'))
				return false;
				
            post(BaseUrl + page_class + '/print_manifest',
			{
				shipping_id: $('#data-table-form input[name="id"]').val()
			}, '', 'Y');
        });
		
		$('#table-list-item').on('click', '.detail', function(e){
			e.preventDefault();
			
			var tr = $(this).closest('tr');
			var row = dataTable1.getDataTable().row( tr ); 
			
			if(tr.hasClass('shown')){
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
				
				childTable.getDataTable().destroy();
			}else{
				$('#table-list-item tbody tr').each(function(){
					var type = $(this).attr('role');
					
					//if(type === undefined){
						console.log('here');
						//$(this).hide();
						var r = dataTable1.getDataTable().row( $(this) ); 
						r.child.hide();
						$(this).removeClass('shown');
					//}
				});
			
				// Open this row
				row.child( childTableTpl(row.data()) ).show();
				tr.addClass('shown');
				
				initChildTable(row.data());
			}
		});

		$('#table-list-packing').on('click', '.detail', function(e){
			e.preventDefault();
			
			var tr = $(this).closest('tr');
			var row = dataTable2.getDataTable().row( tr ); 
			
			if(tr.hasClass('shown')){
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
				
				childTable.getDataTable().destroy();
			}else{
				$('#table-list-packing tbody tr').each(function(){
					var type = $(this).attr('role');
					
					//if(type === undefined){
						console.log('here');
						//$(this).hide();
						var r = dataTable2.getDataTable().row( $(this) ); 
						r.child.hide();
						$(this).removeClass('shown');
					//}
				});
			
				// Open this row
				row.child( childTablePacking(row.data()) ).show();
				tr.addClass('shown');
				
				initChildTablePacking(row.data());
			}
		});
	}
        
    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };  
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});
var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dataTable,
		SHIPPING_ID;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#periode, #loading, #pl, #status').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		setTimeout(function(){
            $('#periode').val(current_filter).trigger('change');
			initTable();
		}, 500);
	}

	var reset = function(){
		$("#loading").val(null).trigger("change");
		$('#pl').val(null).trigger('change');
		$('#status').val(null).trigger('change');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_month');
	}

    var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + "outbound/getShippingList", // ajax source
					"data": {
						id_shipping: $('#loading').val(),
						pl_id: $('#pl').val(),
						from: $('#from').val(),
						to: $('#to').val(),
						status: $('#status').val(),
                        periode:$('#periode').val(),
                        warehouse:$('#warehouse-filter').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
        },
        "columns": [
					{ "orderable": false, "visible": false },
          { "orderable": false },
					{ "orderable": true },
					{ "orderable": false },
          { "orderable": false },
          { "orderable": true },
          { "orderable": true },
          { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
          { "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
          ],
        "order": [
            [0, "desc"]
        ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4,5,6,7,8,9,10,11]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl+'inbound/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="kd_inbound"]').val(json.kd_inbound);
					$('#data-table-form input[name="tanggal_inbound"]').val(json.tanggal_inbound);
					$('#data-table-form select[name="id_supplier"]').val(json.id_supplier).selectpicker("refresh");
					$('#data-table-form select[name="id_inbound_document"]').val(json.id_inbound_document).selectpicker("refresh");
					$('#data-table-form #added_rows').append(json.inbound_barang_html);
					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});
					$(".remove_row").click(function(){
						$(this).closest("tr").remove();
					});
				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var closeLoading = function(id){
		App.blockUI();
		$.ajax({
			url: BaseUrl + page_class + '/closeLoading',
			type: 'POST',
			dataType: 'JSON',
			data: {
				shipping_id: id
			},
			success: function(response){
				if(response.status == 'OK'){

					dataTable.getDataTable().ajax.reload();
					toastr['success'](response.message, "SUCCESS");

				}else{
					toastr['error'](response.message, "Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var checkDo = function(){

		if (!/\S/.test($('#do-number').val())) {
			$('.do-group').addClass('has-error');
			toastr['error']("Document number required.", "Error");
			return false;
		}

		App.blockUI({
			target: '#modal-do'
		});

		var doNumber = $('#do-number').val();

		$.ajax({
			url: BaseUrl + page_class + '/checkDoNumber',
			type: 'POST',
			dataType: 'JSON',
			data: {
				do_number: doNumber,
				shipping_id: SHIPPING_ID
			},
			success: function(response){
				if(response.status == 'OK'){
					$('#modal-do').modal('hide');
					printDo(doNumber);
					initTable();
					//dataTable.getDataTable().ajax.reload();
				}else{
					App.unblockUI('#modal-do');
					toastr['error'](response.message, "Error");
				}
			},
			error: function(response){
				App.unblockUI('#modal-do');
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var printDo = function(doNumber){
		App.unblockUI('#modal-do');
		post(BaseUrl + page_class + '/print_delivery_order',
		{
			do_number: doNumber,
			shipping_id: SHIPPING_ID
		}, '', 'Y');
	}

    var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    	});

		$(document).on('click','.data-table-print-surat-jalan',function(e){
            e.preventDefault();

            post(BaseUrl + page_class + '/print_surat_jalan',{shipping_id:$(this).attr('data-id')}, '','Y');
        });

		$(document).on('click','.data-table-print-packing-document',function(e){
            e.preventDefault();

            post(BaseUrl + page_class + '/print_packing_document',{shipping_id:$(this).attr('data-id')}, '','Y');
        });

		$("#loading")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'loading/getLoadingCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.shipping_id,
							text: item.shipping_code
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$("#pl")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'picking_list/getPlCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.pl_id,
							text: item.pl_name
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			checkDo();
		});

		$(document).on('click','.data-table-print-do',function(e){
            e.preventDefault();

			if($(this).hasClass('disabled-link'))
				return false;

			var rowdata = dataTable.getDataTable().row($(this).parents('tr')).data();
			if(rowdata[8] == '-'){
				toastr['error']("Loading process not finish yet.", "Error");
				return false;
			}else{

				var ctrl = $(this),
					id = ctrl.attr('data-id'),
					doNumber = ctrl.attr('data-do');

				SHIPPING_ID = id;

				$.ajax({
					url: BaseUrl + page_class + '/getDoNumber',
					type: 'POST',
					dataType: 'JSON',
					data: {
						shipping_id: SHIPPING_ID
					},
					success: function(response){

						var doNumb = response.do_number;

						$('.do-group').removeClass('has-error');

						if(doNumb != null && doNumb != ''){
							$('#do-number').val(doNumb);
							$('#do-number').attr('disabled', '');
						}else{
							$('#do-number').val('');
							$('#do-number').removeAttr('disabled');
						}

						$('#modal-do').modal({
							backdrop: 'static',
							keyboard: true
						});
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});

				/*
				$('.do-group').removeClass('has-error');

				if(doNumber != null && doNumber != ''){
					$('#do-number').val(doNumber);
					$('#do-number').attr('disabled', '');
				}else{
					$('#do-number').val('');
					$('#do-number').removeAttr('disabled');
				}

				$('#modal-do').modal({
					backdrop: 'static',
					keyboard: true
				});
				*/
			}
        });

		$(document).on('click','.data-table-print-plate',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id'),
				plate = ctrl.attr('data-plate');

			if(ctrl.hasClass('disabled-link'))
				return false;

            $.ajax({
				url: BaseUrl + page_class + '/printPlate',
				type: 'POST',
				dataType: 'JSON',
				data: {
					shipping_id: id,
					plate: plate
				},
				success: function(response){
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				},
				error: function(response){
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
        });

		$(document).on('click','.data-table-print-ol',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            $.ajax({
				url: BaseUrl + page_class + '/printOuterLabel',
				type: 'POST',
				dataType: 'JSON',
				data: {
					shipping_id: id
				},
				success: function(response){
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				},
				error: function(response){
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
        });

		$(document).on('click','.data-table-print',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_manifest',
			{
				shipping_id: id
			}, '', 'Y');
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            location.href = BaseUrl + page_class + "/edit/" + id;
        });

		$(document).on('click','.data-table-close',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            closeLoading(id);
        });

        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');

			if($(this).hasClass('disabled-link'))
				return false;

            bootbox.confirm("Are you sure want remove this data ?", function(result) {
                if(result){
					App.blockUI();
                    $.ajax({
                        type:'POST',
                        url:BaseUrl + page_class + '/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){

								if(json.status == 'OK'){
									toastr['success'](json.message,"Success");
									dataTable.getDataTable().ajax.reload();
								}else{
									toastr['error'](json.message, "Error");
								}

                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You don't have autority to delete this data","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
							App.unblockUI();
                        },
						error: function(response){
							App.unblockUI();
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
                    });
                }
            });
        });

		$(document).on('click','.data-table-cancel',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			$.ajax({
				url : BaseUrl + page_class + '/cancel',
				method : "POST",
				dataType : "JSON",
				data : { shipping_id : id },
				success : function(response){

	           		if(response.status){
	            		initTable();
	            		toastr['success'](response.message,"success");
	            	}else{
	            		toastr['error'](response.message,"error");
	            	}

				},
				error : function(err){
            		toastr['error'](err.message,"error");
				}
			})

        });

    }

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_inbound: {
                    required: true
                },
                tanggal_inbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_inbound_document: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'inbound/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            dataTable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

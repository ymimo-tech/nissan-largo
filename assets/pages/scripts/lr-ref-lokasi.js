var Ref_lokasi = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

    var content_datatable;
	var locId = [];

    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
                "bDestroy": true,
                "processing": true,
                "serverSide": true,
                // "language": {
                    // "lengthMenu": "Display _MENU_ records",
                    // "info" : ", Display _START_ to _END_ of _TOTAL_ entries"
                // },
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl+page_class+"/get_list", // ajax source
                    "data": {
                        bin_name: $('#bin-name').val(),
                        bin_desc: $('#bin-desc').val(),
                        category: $('#category').val(),
                        type: $('#type').val(),
                        area: $('#area').val(),
                        status: $('#status').val(),

                    },
                },
                "columns": [
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": false },
                    // { "orderable": false },
                ],
                "order": [
                    [2, "asc"]
                ],// set first column as a default sort by asc
            }
        });
    }


	var showModalPrint = function(id, code){

		if(id){

			$('#data-modal-form input[name="id"]').val(id);

			$('#qty-print').val('');
			$('.label-qty').html('Code');

			$('.qty-print-location').show();
			$('.qty-print-location').html(code);
			$('#qty-print').hide();
			$('.required').hide();

			$('#modal-print').modal({
				backdrop: 'static',
				keyboard: true
			});

		}else{

			$('.label-qty').html('Qty');

			if($('#datatable_ajax tbody input[type="checkbox"]').is(':checked')){

				locId = [];

				$('#datatable_ajax tbody input[type="checkbox"]').each(function(){
					var ctrl = $(this);

					if(ctrl.is(':checked'))
						locId.push(ctrl.val());

				});

				App.blockUI();

				$('#data-modal-form input[name="id"]').val('');
				$('.qty-print-location').html(locId.length);

				$('.qty-print-location').show();
				$('#qty-print').hide();
				$('.required').hide();

				$('#modal-print').modal({
					backdrop: 'static',
					keyboard: true
				});

				App.unblockUI();

			}else{

				App.blockUI();
				$.ajax({
					url: BaseUrl + page_class + '/getQtyPrint',
					type: 'POST',
					dataType: 'JSON',
					data: {

					},
					success: function(response){

						var data = response;
						$('#data-modal-form input[name="id"]').val('');
						$('.qty-print-location').html(data.qty);

						$('.qty-print-location').show();
						$('#qty-print').hide();
						$('.required').hide();

						$('#modal-print').modal({
							backdrop: 'static',
							keyboard: true
						});

						App.unblockUI();
					},
					error: function(response){
						App.unblockUI();
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});
			}
		}
	}

	var printBarcode = function(){

		var $btn = $('#submit-print');
			$btn.button('loading');

		var qty = $('.qty-print-location').html(),
			check = 0;

		if($('#data-modal-form input[name="id"]').val().length > 0){
			qty = $('.qty-print-location').html();
			check = 1;
		}

		if (!/\S/.test(qty)) {
			if(check == 0){
				$('.qty-print-group').addClass('has-error');
			}

			$btn.button('reset');
			toastr['error']("Qty print cannot empty and must be greater than 0","Error");
			return false;
		}else{
			if(qty <= 0){

				if(check == 0){
					$('.qty-print-group').addClass('has-error');
				}

				$btn.button('reset');
				toastr['error']("Qty print cannot empty and must be greater than 0","Error");
				return false;
			}
		}

		var params = 'multiple';
		if(check == 1)
			params = 'single';

		if($('#datatable_ajax tbody input[type="checkbox"]').is(':checked'))
			params = 'partial';

		postdata = {
			params: params,
			code: qty,
			loc_id: locId
		}

		$.ajax({
			url: BaseUrl + page_class + '/printZpl',
			type: 'POST',
			dataType: 'JSON',
			data: postdata,
			success: function(response){
				if(response.status == 'OK'){
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				}else
					bootbox.alert(response.message);

				setTimeout(function(){
					$btn.button('reset');
				}, 1000);
				//App.unblockUI();
				/*
				var zpl = response.zpl;

				var ctrl = $(zpl);

				var postdata = {
					sid: $('#sid').val(),
					pid: $('#pid').val(),
					printerCommands: zpl
				}
				// store user printer settings & commands in server cache
				$.post(BaseUrl + 'webclientprint/ProcessPrint.php',
					postdata,
					function() {
						 // Launch WCPP at the client side for printing...
						 var sessionId = $("#sid").val();
						 jsWebClientPrint.print('sid=' + sessionId);
					}
				);
				*/
			},
			error: function(response){
				$btn.button('reset');
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    var reset = function(){
        $("#bin-name").val(null).trigger("change");
        $("#bin-desc").val(null).trigger("change");
        $("#category").val(null).trigger("change");
        $("#area").val(null).trigger("change");
        $("#type").val(null).trigger("change");
        $("#status").val(null).trigger("change");

    }


    var pageInteraction = function(){

        // $('#referensi-lokasi-form input[name="row"], #referensi-lokasi-form input[name="level"]').on('keydown',function(e){

        //     if(isNumeric(e)){
        //         return isNumeric(e);
        //     }else{
        //         if ( e.keyCode == 190 ) {
        //             return e.key;
        //         }
        //     }

        //     return false;

        // });

        $('#referensi-lokasi-form input[name="rack"], #referensi-lokasi-form input[name="row"], #referensi-lokasi-form input[name="level"], #referensi-lokasi-form input[name="bin"], #referensi-lokasi-form input[name="color_code_bin"], #referensi-lokasi-form input[name="color_code_level"]').on('keydown',function(e){

            var len = $(this).val().length;
            if(e.keyCode != 8 && e.keyCode != 9){
                if(len > 3){
                    return false;
                }
            }

        });

		$('.data-table-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{

			});
		});

        $('#form-search').on('submit', function(e){
            e.preventDefault();
            handleRecords();
        });

        $('#reset').on('click', function(){
            reset();
            handleRecords();
        });

		$('#loc-type,#loc_category_id,#loc_area_id,#loc_type_id,#status').select2();

		$("#status")
            .select2({
                placeholder: "-- All --",
                allowClear:true,
                data: [
                    {id:'ACTIVE',text:'ACTIVE'},
                    {id:'DEACTIVE',text:'DEACTIVE'}
                ]
            });
		$('#status').val(null).trigger('change');

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});

		$('.btn-print').on('click', function(){

			if(!$('#datatable_ajax tbody input').is(':checked')){
				toastr['error']('Please check at least one row to print', 'ERROR');
				return false;
			}

			showModalPrint();
		});

        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
            $('#loc_category_id,#loc_area_id,#loc_type_id,#status').val('').trigger('change');
        });

        $('#data-table-filter').submit(function(e){
            e.preventDefault();
            data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
            data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
            data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
            data-table_datatable.getDataTable().ajax.reload();
        });

        $('.hide-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').hide();
            $('.filter-hide-control,.msg-filter').show();
        });

        $('.show-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').show();
            $('.filter-hide-control,.msg-filter').hide();
        });

        $('.reset-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter input[name="nama_data-table"]').val('');
            $('#data-table-filter input[name="kota"]').val('');
            $('#data-table-filter textarea[name="alamat_data-table"]').val('');
            data-table_datatable.clearAjaxParams();
            data-table_datatable.getDataTable().ajax.reload();
        });

        $('.data-table-add').click(function(e){
            e.preventDefault();

			$('.type-group').show();
			$('#loc-type').val('BINLOC').trigger('change');

            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });

		$(document).on('click','.data-table-print',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
			var rowdata = content_datatable.getDataTable().row($(this).parents('tr')).data();
            showModalPrint(id, rowdata[2]);
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl+'referensi_lokasi/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    console.log(json);
                    if(! json.no_data){
                        $('#referensi-lokasi-form input[name="id"]').val(json.loc_id);
                        $('#referensi-lokasi-form input[name="loc_name"]').val(json.loc_name);
                        $('#referensi-lokasi-form input[name="loc_desc"]').val(json.loc_desc);
                        $('#referensi-lokasi-form select[name="loc_category_id"]').val(json.loc_category_id).trigger('change');
                        $('#referensi-lokasi-form select[name="loc_area_id"]').val(json.loc_area_id).trigger('change');
                        $('#referensi-lokasi-form select[name="loc_type_id"]').val(json.loc_type_id).trigger('change');
                        $('#referensi-lokasi-form select[name="status"]').val(json.status).trigger('change');
                        $('#referensi-lokasi-form input[name="rack"]').val(json.rack);
                        $('#referensi-lokasi-form input[name="row"]').val(json.row);
                        $('#referensi-lokasi-form input[name="level"]').val(json.level);
                        $('#referensi-lokasi-form input[name="bin"]').val(json.bin);
                        $('#referensi-lokasi-form input[name="color_code_bin"]').val(json.color_code_bin);
                        $('#referensi-lokasi-form input[name="color_code_level"]').val(json.color_code_level);

						if(json.loc_type == 'TRANSIT' || json.loc_type == 'SENTOUT' ||
							json.loc_type == 'INBOUND' || json.loc_type == 'OUTBOUND' ||
							json.loc_type == 'QC' || json.loc_type == 'NG')
							$('.type-group').hide();
						else
							$('.type-group').show();

						$('#loc-type').val(json.loc_type).trigger('change');

                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want to remove data ?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'referensi_lokasi/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.status == 'OK'){
                                toastr['success'](json.message,"SUCCESS");
                                content_datatable.getDataTable().ajax.reload();
                            }else{
                                toastr['error'](json.message,"ERROR");
                            }
                        }
                    });
                }
            });
        });
    }

    var handleFormSettings = function() {
        $('#referensi-lokasi-form').validate({
            debug: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                loc_name: {
                    required: true
                },
                loc_category_id: {
                    required: true
                },
                loc_area_id: {
                    required: true
                },
                loc_type_id: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'referensi_lokasi/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#referensi-lokasi-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Change data success.","Success");
                            }else{
                                toastr['success']("Save data success.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You dont have authority to add data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You dont have authority to change data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleRecords();
            pageInteraction();
            handleFormSettings();
            /*handleBootstrapSelect();
            handleDatePickers();

            */
        }

    };
}();

jQuery(document).ready(function() {
    Ref_lokasi.init();
});

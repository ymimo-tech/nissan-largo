var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
    
    var dTable;
	
	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});
		
		$('#supplier, #periode').select2({
			allowClear: false,
			width: null
		});
		
		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});
		
		dateFilter.init();
		setTimeout(function(){
			initTable();
		}, 500);
	}
	
	var initTable = function () {

        dTable = new Datatable();
		
        dTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getList", // ajax source
					"data": {
						id_receiving: $('#rcv').val(),
						id_po: $('#inbound').val(),
						id_supplier: $('#supplier').val(),
						from: $('#from').val(),
						to: $('#to').val(),
						gr: $('#gr').val(),
						warehouse: $('#warehouse-filter').val(),
						location: $('#location').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
					/*
					dataSrc: function(json) {
						totalItem = json.total_item;
						totalQty = json.total_qty;
						
						return json.data;
					}
					*/
                },
                "columns": [
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true }
                  ],
                "order": [
                    [7, "ASC"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4,5,7]
					}
				]
			}
		});
	}
	
	var reset = function(){
		$("#rcv").val(null).trigger("change");
		$("#inbound").val(null).trigger("change");
		$("#gr").val(null).trigger("change");
		$('#supplier').select2('val', '');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'today');
	}
	
    var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    	});
		
		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});
		
		$('#reset').on('click', function(){
			reset();
		});
		
		$("#rcv")
		  .select2({
			multiple: false, 
			allowClear: true,
			width: null,
			placeholder: "Type here...",  
			minimumInputLength: 3,  
			ajax: { 
				url: BaseUrl + 'receiving/getReceivingCode',  
				dataType: 'json',  
				type: 'POST',
				data: function (q, page) {  
					return {  
						query: q.term
					};  
				},  
				processResults: function (data, params) {		
					var results = [];
					
					$.each(data.result, function(index, item){
						results.push({
							id: item.id_receiving,
							text: item.kd_receiving
						});
					});
					return {
						results: results
					};
				}
			}
		});
		
		$("#inbound")
		  .select2({
			multiple: false, 
			allowClear: true,
			width: null,
			placeholder: "Type here...",  
			minimumInputLength: 3,  
			ajax: { 
				url: BaseUrl + 'receiving/getInboundCode',  
				dataType: 'json',  
				type: 'POST',
				data: function (q, page) {  
					return {  
						query: q.term
					};  
				},  
				processResults: function (data, params) {		
					var results = [];
					
					$.each(data.result, function(index, item){
						results.push({
							id: item.id_inbound,
							text: item.kd_inbound
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('.gr').on('click', function(){
			post(BaseUrl +'putaway/download_gr',
			{
				from: $('#from').val(),
				to: $('#to').val(),
				status: $('#gr').val()
			});
		});

		$("#location")
		  .select2({
			multiple: false, 
			allowClear: true,
			width: null,
			placeholder: "Type here...",  
			minimumInputLength: 3,  
			ajax: { 
				url: BaseUrl + 'putaway/getLocation',  
				dataType: 'json',  
				type: 'POST',
				data: function (q, page) {  
					return {  
						query: q.term
					};  
				},  
				processResults: function (data, params) {		
					var results = [];
					
					$.each(data.result, function(index, item){
						results.push({
							id: item.location_id,
							text: item.location_code
						});
					});
					return {
						results: results
					};
				}
			}
		});
    }
        
    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };  
}();

jQuery(document).ready(function() {
    Handler.init();
});
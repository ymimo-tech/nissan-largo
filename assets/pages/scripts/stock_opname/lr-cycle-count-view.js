var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dataTable,
		CC_ID;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#periode, #cc, #status').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		setTimeout(function(){
            $('#periode').val(current_filter).trigger('change');
			initTable();
		}, 500);
	}

	var reset = function(){
		$("#cc").val(null).trigger("change");
		$('#status').val(null).trigger('change');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_month');
	}

    var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getList", // ajax source
					"data": {
						cc_id: $('#cc').val(),
						status: $('#status').val(),
						from: $('#from').val(),
						to: $('#to').val(),
                        periode:$('#periode').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                  ],
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4,5,6,7,8]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

	var closeCc = function(id){
		App.blockUI();
		$.ajax({
			url: BaseUrl + page_class + '/closeCc',
			type: 'POST',
			dataType: 'JSON',
			data: {
				cc_id: CC_ID,
				remark: $('#remark').val()
			},
			success: function(response){
				if(response.status == 'OK'){

					$('#modal-cc').modal('hide');

					dataTable.getDataTable().ajax.reload();
					toastr['success'](response.message, "SUCCESS");

				}else{
					toastr['error'](response.message, "Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var updateFinishDate = function(id){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/finishCc',
			type: 'POST',
			dataType: 'JSON',
			data: {
				cc_id: id
			},
			success: function(response){
				if(response.status == 'OK'){
					toastr['success'](response.message, "SUCCESS");
				}else{
					toastr['error'](response.message, "ERROR");
				}

				dataTable.getDataTable().ajax.reload();

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

    var pageInteraction = function(){

		$('#submit-cc').on('click', function(e){
			e.preventDefault();
			closeCc();
		});

		$("#cc")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + page_class + '/getCcCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.cc_id,
							text: item.cc_code
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

		$(document).on('click','.data-table-finish',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            bootbox.confirm("Are you sure want finish this stock opname process ?", function(result) {
                if(result){
					updateFinishDate(id);
				}
			});
        });

		$(document).on('click','.data-table-print',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_cc_doc',
			{
				cc_id: id
			}, '', 'Y');
        });

        $(document).on('click','.data-table-print2',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_cc_doc2',
			{
				cc_id: id
			}, '', 'Y');
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            location.href = BaseUrl + page_class + "/edit/" + id;
        });

		$(document).on('click','.data-table-check',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			location.href = BaseUrl + page_class + '/check_result/' + id;
        });

		$(document).on('click','.data-table-close',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			CC_ID = id;

			var rowdata = dataTable.getDataTable().row($(this).parents('tr')).data();
			if(rowdata[6] == 'Close'){
				toastr['error']("Stock Opname already close", "ERROR");
			}

			$('#remark').val('');

            $('#modal-cc').modal({
				bakdrop: 'static',
				keyboard: true
			});
        });

        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');

			if($(this).hasClass('disabled-link'))
				return false;

            bootbox.confirm("Are you sure want remove this data ?", function(result) {
                if(result){
					App.blockUI();
                    $.ajax({
                        type:'POST',
                        url:BaseUrl + page_class + '/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){

								if(json.status == 'OK'){
									toastr['success'](json.message,"Success");
									dataTable.getDataTable().ajax.reload();
								}else{
									toastr['error'](json.message, "Error");
								}

                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You don't have autority to delete this data","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
							App.unblockUI();
                        },
						error: function(response){
							App.unblockUI();
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
                    });
                }
            });
        });
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Handler.init();
});

var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var initialize = function(){

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: moment().format('DD/MM/YYYY')
		});

		var id = $('input[name="id"]').val();
		if(id.length > 0)
			edit(id);

		$('#type').select2();
	}

	var reset = function(){
		$('#data-table-form input[name="id"]').val('');
		$('#data-table-form input[name="params"]').val('');
		$('#data-table-form input[name="cc_code"]').val('');
		$('#data-table-form input[name="tanggal_cc"]').val(moment().format('DD/MM/YYYY'));
		$('#data-table-form #added_rows_item').html('');
		$('#data-table-form #added_rows_loc').html('');
	}

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/getEdit',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(!json.no_data){
					$('#data-table-form input[name="cc_code"]').val(json.cc_code);
    				$('#data-table-form input[name="no_person"]').val(json.no_person);
					$('.cc-code').html(json.cc_code);
					//$('.date-picker').datepicker("update", json.cc_time);

					$('.date-picker').datepicker('remove');

					$('.date-picker').datepicker({
						orientation: "left",
						format: 'dd/mm/yyyy',
						autoclose: true,
						startDate: json.cc_time
					});

					$('.date-picker').datepicker("update", json.cc_time);

					$('#data-table-form select[name="type"]').val(json.cc_type).trigger("change");
					$('#type').attr('disabled', '');

					if(json.cc_type == 'SN'){

						$('#data-table-form #added_rows_item').append(json.html);

						$('#item-table').show();
						$('#loc-table').hide();

					}else{

						$('#data-table-form #added_rows_loc').append(json.html);

						$('#item-table').hide();
						$('#loc-table').show();

					}

					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

					if($('#data-table-form input[name="is_used"]').val() == 'false'){

						$(".remove_row").click(function(){
							$(this).closest("tr").remove();
						});

					}else{

						$('select[name="id_barang[]"]').attr('disabled', '');
						$('select[name="loc_id[]"]').attr('disabled', '');
						$('.btn-delete').hide();
						$('.add_item').parent('tr').hide();

					}
				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    var pageInteraction = function(){

		$('.add_loc').on('click', function(){
			$('.add-loc').removeAttr('data-toggle');
			$('.add-loc').tooltip('destroy');
		});

		$('#type').on('change', function(){
			var ctrl = $(this),
				val = ctrl.val();

			if(val == 'SN'){
				$('#item-table').show();
				$('#loc-table').hide();
			}else{
				$('#item-table').hide();
				$('#loc-table').show();

				setTimeout(function(){
					$('.add-loc').tooltip('show');
				}, 100);
			}
		});

		$(document).on('change', '#data-table-form select.add_rule', function(){
			$(this).closest('.form-group').removeClass('has-error');
		});

		$('#save-new').on('click', function(){
			$('input[name="params"]').val('new');
			$('#data-table-form').submit();
		});

		$(".add_loc").click(function(e){
            $.ajax({
                url:BaseUrl + page_class + "/get_add_loc_row",
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows_loc").append("<tr id='target_new_row'><td colspan='4'>Please Wait</td></tr>");
                },
                success:function(html){
                    $("#target_new_row").replaceWith(html);

                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();
                    });

					$('#data-table-form select.add_rule,.area_select').css('width', '100%');
					$('#data-table-form select.add_rule,.area_select').select2();
                }
            });
        });

        $(".add_item").click(function(e){
            $.ajax({
                url:BaseUrl + page_class + "/get_add_item_row",
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows_item").append("<tr id='target_new_row'><td colspan='3'>Please Wait</td></tr>");
                },
                success:function(html){
                    $("#target_new_row").replaceWith(html);

                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();
                    });

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();
                }
            });
        });

        $(document).on('change','.area_select',function(){
            var me = $(this);
            $.ajax({
                type:'POST',
                url:BaseUrl + page_class + "/get_loc",
                data:{'loc_area_id':me.val()},
                dataType:"html",
                beforeSend:function() {
                    me.closest('tr').block({message:null,overlayCSS:{'background':'transparent'}});
                },
                success:function(html){
                    me.closest('tr').find(".loc_select").html(html);
                    me.closest('tr').find('.loc_select').trigger('change');
                    me.closest('tr').unblock();
                }
            });
        });
    }

	var newCycleCountCode = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getNewCycleCountCode',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){
					$('#data-table-form input[name="cc_code"]').val(response.cycle_count_code);
					$('#data-table-form .cc-code').html(response.cycle_count_code);
				}else{
					toastr['error']("Generate new stock opname number failed. Please refresh page","Form Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                cc_code: {
                    required: true
                },
                no_person: {
                    required: true
                },
                tanggal_cc: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {

				var chk = true;
				var itemId = [];

				if($('#data-table-form input[name="tanggal_cc"]').val().length != 10){
					$('.date-picker').addClass('has-error');
					chk = false;
				}

				var idSelector = '#item-table',
					added = '#added_rows_item';

				if($('#type').val() == 'LOC'){
					idSelector = '#loc-table';
					added = '#added_rows_loc';
				}

				if($(idSelector + ' ' + added + ' tr').length <= 0){

					var msg = "Please add item first";
					if(idSelector == '#loc-table')
						msg = "Please add location first";

					toastr['error'](msg, "ERROR");
					return false;
				}

				$('#data-table-form ' + idSelector + ' select.add_rule').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

                    var parent='';
                    if(ctrl.hasClass('loc_select')){
                        var parent=ctrl.parent('td').prev().find('select').val()+'_';
                    }

					itemId.push(parent+val);

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					if(val == 'undefined' || val == '') {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}
				});

				$(idSelector + ' input.add_rule').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					var id = $('input[name="id"]').val();
					if(id.length > 0){
						if(val <= 0) {
							ctrl.closest('.form-group').addClass('has-error');
							chk = false;
						}
					}
				});


				if(!chk){
					toastr['error']("Please check again.","Form Error");
					return false;
				}

                console.log(itemId);

				var hasDuplicate = !itemId.every(function(v,i) {
					return itemId.indexOf(v) == i;
				});

				if(hasDuplicate){

					var idSelector = '#item-table';
					if($('#type').val() == 'LOC')
						idSelector = '#loc-table';

					$('#data-table-form '+idSelector+' select.add_rule').not('.area_select').each(function(index1, item1){

						$.each($('#data-table-form select.add_rule').not(this), function (index2, item2) {

							if ($(item1).val() == $(item2).val()) {
								$(item1).closest('.form-group').addClass('has-error');
							}

						});

					});

					if($('#type').val() == 'SN')
						toastr['error']("Duplicate item. Please fix it first","Form Error");
					else
						toastr['error']("Duplicate location. Please fix it first","Form Error");

					return false;
				}

				App.blockUI();

				var postdata = $(form).serialize();

				if($('input[name="id"]').val().length > 0)
					postdata = postdata + '&type=' + $('#type').val();


                $.ajax({
					type:'POST',
					url: BaseUrl + page_class + '/proses',
					dataType:'json',
					data: postdata,
					beforeSend:function(){
						//$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
						//$('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
						$('.form-group').removeClass('has-error');
					},
					success:function(json){
						if(json.success==1){
							if(json.edit==1){
								toastr['success']("Save changes success.","Success");
							}else{
								toastr['success']("Save success.","Success");
							}

							var params = $('input[name="params"]').val();

							if(params == 'new'){
								reset();
								newCycleCountCode();
							}else{
								setTimeout(function(){
									location.href = BaseUrl + page_class;
								}, 500);
							}
							//content_datatable.getDataTable().ajax.reload();
							//$('#form-content').modal('hide');
						}else{
							if(json.no_add){
								toastr['warning']("You dont have autority to add data.","Warning");
							}else if(json.no_edit){
								toastr['warning']("You dont have autority to change data..","Warning");
							}else{
								toastr['error']("Please refresh the page and try again later.","Error Occured");
							}
						}
						App.unblockUI();
						//$('.modal-content').unblock();
						//$('.spinner').remove();
					},
					error: function(response){
						App.unblockUI();
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            handleFormSettings();
        }

    };
}();

jQuery(document).ready(function() {
    Handler.init();
});

var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
	
	var dataTable,
		forms;
	
	var initialize = function(){
		initTable();
	}
	
	var initTable = function () {

        dataTable = new Datatable();		
		
        dataTable.init({
            src: $("#table_user"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/get_list/",
                    "data": {
                        name: $('#name').val(),
                        username: $('#username').val(),
                        roles: $('#roles').val(),
                        status: $('#status').val()
                    },
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + page_class + '/login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [1, "ASC"]
                ]
            }
        });
    }
	
	var pageInteraction = function(){

        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
            $("#form-content select").val("refresh").selectpicker("refresh");
        });

		$('.data-table-add').click(function(e){
            e.preventDefault();
			
            $('#warehouse').val(null).trigger('change');
			$('#zone').val(null).trigger('change');
            

			forms.destroy();
			setTimeout(function(){
				handleFormSettings();
			}, 100);
			
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
			
			forms.destroy();
			setTimeout(function(){
				handleFormSettings();
			}, 100);
			
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl+'user/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    if(! json.no_data){
                        console.log(json)
                        $('#data-table-form input[name="id"]').val(json.user_id);
                        $('#data-table-form input[name="nik"]').val(json.nik);
                        $('#data-table-form input[name="nama"]').val(json.nama);
                        $('#data-table-form input[name="user_name"]').val(json.user_name);
                        //$('#data-table-form input[name="password"]').val(json.user_password);
                        $('#data-table-form select[name="grup"]').val(json.grup_id).selectpicker("refresh");
                        $('#data-table-form select[name="status"]').val(json.is_active).selectpicker("refresh");
                        $('#warehouse').val(json.warehouses).trigger('change');
                        $('#zone').val(json.zones).trigger('change');


                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Anda yakin akan menghapus data?", function(result) {
                if(result){
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'user/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){
                                toastr['success']("Data berhasil dihapus.","Success");
                                dataTable.getDataTable().ajax.reload();
                            }else{
                                if(json.no_delete){
                                    toastr['warning']("Anda tidak mempunyai otoritas untuk menghapus data.","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
                        }
                    });
                }
            }); 
        });

		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});
		
		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});
		
		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});
		
		$('#modal-print').on('shown.bs.modal', function () { 
			$('#qty-print').focus();
		});
		
		$('#button-print-qty-barcode').on('click', function(e){
			e.preventDefault();
			
			var id = $('#data-table-form input[name="id"]').val();
			
			showModalPrint(id);
		});

		// Request License Key from Server

		$('.data-table-renew').click(function(){
			$.removeCookie('largo_license', { path: '/'});
			location.reload();
		})

		// End - Request License Key from Server

	}

	var handleFormSettings = function() {
				
        forms = $('#data-table-form').validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-block', // default input error message class
				focusInvalid: true, // do not focus the last invalid input
				rules: {
					nik: {
						required: true
					},
					nama: {
						required: true
					},
					grup: {
						required: true
					},
					user_name: {
						required: true
					},
					password: {
						required: $('#data-table-form input[name="id"]').val().length <= 0
					},
					status: {
						required: true
					}
				},

				invalidHandler: function(event, validator) { //display error alert on form submit   
					toastr['error']("Periksa kembali form input.","Form Error");
				},

				highlight: function(element) { // hightlight error inputs
					$(element)
						.closest('.form-group').addClass('has-error'); // set error class to the control group
				},
				
				unhighlight:function(element){
					 $(element)
						.closest('.form-group').removeClass('has-error');
				},
				
				success: function(label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},

				/*errorPlacement: function(error, element) {
					if(element.next('span').hasClass('help-inline')){
						error.insertAfter(element.next('span.help-inline'));
					}else{
						error.insertAfter(element);
					}
					
				},*/
				
				errorPlacement: function(error,element) {
					return true;
				},
				submitHandler: function(form) {
					$.ajax({
						type:'POST',
						url:BaseUrl+'user/proses',
						dataType:'json',
						data: $(form).serialize(),
						beforeSend:function(){
							$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
							$('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
							$('.form-group').removeClass('has-error');
						},
						success:function(json){
							if(json.success==1){
								if(json.edit==1){
									toastr['success']("User data updated","Success");
								}else{
									toastr['success']("New user created","Success");
								}
								dataTable.getDataTable().ajax.reload();
								$('#form-content').modal('hide');
							}else{
								if(json.no_add){
									toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
								}else if(json.no_edit){
									toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
								}else{
									toastr['error']("Please refresh the page and try again later.","Error Occured");
								}
							}
							$('.modal-content').unblock();
							$('.spinner').remove();
						}
					});
				}
			});
    }

    var warehouseOptionHandler = function(){

		var row = '<option value="all">Select All</option>';
		row = row + $('#warehouse').html();

		$('#warehouse').html(row);

		$('#warehouse').select2();

        $('#warehouse').on('change', function(e){

        	var ctrl = $(this);

    		var allOption = $('#warehouse option').length;
    		var selectedOption = $('#warehouse option:selected').length;

        	if($.inArray('all',ctrl.val()) != -1){
        		if(allOption != selectedOption){
	        		$('#warehouse option').each(function(){
	        			$(this).prop('selected',true);
	        		})
	        		$('#warehouse option[value="all"]').text('Deselect All');
	        		$('#warehouse option[value="all"]').prop('selected',false);

        		}else{

	        		$('#warehouse option').each(function(){
	        			$(this).prop('selected',false);
	        		})
	        		$('#warehouse option[value="all"]').text('Select All');

        		}

        	}
        	else
        	{

        		if((allOption-1) == selectedOption){

	        		$('#warehouse option[value="all"]').text('Deselect All');

        		}else{

	        		$('#warehouse option[value="all"]').text('Select All');

        		}

        	}
        	$(this).trigger('change.select2');

        });

        $('#warehouse').on('select2:open', function(e){

        	e.preventDefault();

    		var allOption = $('#warehouse option').length;
    		var selectedOption = $('#warehouse option:selected').length;
    		if((allOption-1) == selectedOption){
    			$('#select2-warehouse-results li[id$="all"]').text('Deselect All');
    		}else{
    			$('#select2-warehouse-results li[id$="all"]').text('Select All');
    		}
        })

    }

    var zoneOptionHandler = function(){

		var row = '<option value="all">Select All</option>';
		row = row + $('#zone').html();

		$('#zone').html(row);

		$('#zone').select2();

        $('#zone').on('change', function(e){

        	var ctrl = $(this);

    		var allOption = $('#zone option').length;
    		var selectedOption = $('#zone option:selected').length;

        	if($.inArray('all',ctrl.val()) != -1){
        		if(allOption != selectedOption){
	        		$('#zone option').each(function(){
	        			$(this).prop('selected',true);
	        		})
	        		$('#zone option[value="all"]').text('Deselect All');
	        		$('#zone option[value="all"]').prop('selected',false);

        		}else{

	        		$('#zone option').each(function(){
	        			$(this).prop('selected',false);
	        		})
	        		$('#zone option[value="all"]').text('Select All');

        		}

        	}
        	else
        	{

        		if((allOption-1) == selectedOption){

	        		$('#zone option[value="all"]').text('Deselect All');

        		}else{

	        		$('#zone option[value="all"]').text('Select All');

        		}

        	}
        	$(this).trigger('change.select2');

        });

        $('#zone').on('select2:open', function(e){

        	e.preventDefault();

    		var allOption = $('#zone option').length;
    		var selectedOption = $('#zone option:selected').length;
    		if((allOption-1) == selectedOption){
    			$('#select2-zone-results li[id$="all"]').text('Deselect All');
    		}else{
    			$('#select2-zone-results li[id$="all"]').text('Select All');
    		}
        })

    }

    var filterFormHandler = function(){

        $('#form-search').on('submit', function(e){
            e.preventDefault();
            initTable();
        });

        $("#status").select2();

        $("#name")
          .select2({
            multiple: false,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            minimumInputLength: 2,
            ajax: {
                url: BaseUrl + 'user/option/users/id/nama',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (d) {
                            return {
                                text: d.name,
                                id: d.id
                            }
                        })
                    };
                }
            }
        });

        $("#username")
          .select2({
            multiple: false,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            minimumInputLength: 2,
            ajax: {
                url: BaseUrl + 'user/option/users/id/user_name',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (d) {
                            return {
                                text: d.name,
                                id: d.id
                            }
                        })
                    };
                }
            }
        });

        $("#roles")
          .select2({
            multiple: false,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            minimumInputLength: 2,
            ajax: {
                url: BaseUrl + 'user/option/hr_grup/grup_id/grup_nama',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (d) {
                            return {
                                text: d.name,
                                id: d.id
                            }
                        })
                    };
                }
            }
        });



    }
        
    return {

        init: function () {
			initialize();
			pageInteraction();
			handleFormSettings();
            warehouseOptionHandler();
            zoneOptionHandler();
			filterFormHandler();
        }

    };  
}();

jQuery(document).ready(function() {
    handler.init();
});
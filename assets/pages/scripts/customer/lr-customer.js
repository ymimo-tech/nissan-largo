var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
    
    var dataTable;
	
	var initialize = function(){
		
		initTable();
		
	}
	
	var initTable = function () {

        dataTable = new Datatable();
		
        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getList", // ajax source
					"data": {
						pl_id: $('#data-table-form input[name="id"]').val(),
						id_outbound: $('#data-table-form input[name="id_outbound"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": false },
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1, 3, -1]
					}
				]
            }
        });
    }
	
	var getEdit = function(id){
		
		App.blockUI();
		
		$.ajax({
			url: BaseUrl + page_class + '/getEdit',
			dataType: 'JSON',
			type: 'POST',
			data: {
				customer_id: id
			},
			success: function(response){
				if(response.status == 'OK'){
					
					var data = response.data;
					
					$('#form-content input[name="id"]').val(data.id_customer);
					$('#customer-code').val(data.customer_code);
					$('#customer-name').val(data.customer_name);
					$('#customer-address').val(data.address);
					$('#customer-city').val(data.city);
					$('#customer-postcode').val(data.postal_code);
					$('#customer-country').val(data.country);
					$('#customer-phone').val(data.phone);
					$('#customer-fax').val(data.fax);
					$('#customer-cp').val(data.contact_person);

					$('#warehouse').val(null).trigger('change');
					$('#warehouse').val(data.warehouses).trigger('change');
					
					$('#form-content').modal({
						keyboard: true,
						backdrop: 'static'
					});
					
				}else{
					toastr['error'](response.message, 'ERROR');
				}
				
				App.unblockUI();
			},
			error: function (response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}
	
    var pageInteraction = function(){

    	/* Enable Select2.js*/

		$('.data-table-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{
				
			});
		});

		$("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
        });
	
		$('.data-table-add').on('click', function(){

			$('#warehouse').val(null).trigger('change');

			$('#form-content').modal({
				keyboard: true,
				backdrop: 'static'
			});
		});
		
		$(document).on('click', '.data-table-edit', function(){
			var ctrl = $(this),
				id = ctrl.attr('data-id');
				
			getEdit(id);
		});
		
		$(document).on('click', '.data-table-delete', function(e){
			e.preventDefault();
			
			var ctrl = $(this),
				id = ctrl.attr('data-id');
				
            bootbox.confirm("Are you sure want to remove data ?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl + page_class + '/delete',
                        data:{'customer_id':id},
                        dataType:'json',
                        success:function(response){
                            if(response.status == 'OK'){
                                toastr['success'](response.message,"Success");
                                dataTable.getDataTable().ajax.reload();
                            }else{
                                if(response.no_delete){
                                    toastr['warning']("You dont have authority to delete data.","Warning");
                                }else{
                                    toastr['error'](response.message,"Error Occured");
                                }
                            }
                        }
                    });
                }
            }); 
		});
		
    }

    var handleFormSettings = function() {
        $('#customer-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                customer_code: {
                    required: true
                },
                customer_name: {
                    required: true
                },
				customer_address: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }
                
            },*/
            
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
				
				var postdata = {
					customer_id: $('#customer-form input[name="id"]').val(),
					customer_code: $('#customer-form input[name="customer_code"]').val(),
				};
	 
				$.ajax({
					type:'POST',
					url: BaseUrl + page_class + '/checkCustomerCode',
					dataType:'json',
					data: postdata,
					beforeSend:function(){
						$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
						$('#customer-form button[type="submit"]').after('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-left:10px"/>');
						$('.form-group').removeClass('has-error');
					},
					success:function(response){
						if(response.status == 'OK'){
							
							save();
							
						}else{
							toastr['error'](response.message, 'ERROR');
						}
						
						$('.modal-content').unblock();
                        $('.spinner').remove();
					}, 
					error: function(response){
						$('.modal-content').unblock();
                        $('.spinner').remove();
						
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});

            }
        });
    }
	
	var save = function(){
		$.ajax({
			url: BaseUrl + page_class + '/save',
			dataType: 'JSON',
			type: 'POST',
			data:{
				customer_id: $('#customer-form input[name="id"]').val(),
				customer_code: $('#customer-code').val(),
				customer_name: $('#customer-name').val(),
				customer_address: $('#customer-address').val(),
				customer_city: $('#customer-city').val(),
				customer_postcode: $('#customer-postcode').val(),
				customer_country: $('#customer-country').val(),
				customer_phone: $('#customer-phone').val(),
				customer_fax: $('#customer-fax').val(),
				contact_person: $('#customer-cp').val(),
				warehouse: $('#warehouse').val()
			},
			success: function(response){
				
				if(response.status == 'OK'){
					
					toastr['success'](response.message, 'SUCCESS');
					$('#form-content').modal('hide');
					dataTable.getDataTable().ajax.reload();
					
				}else{
					if(json.no_add){
						toastr['warning']("You dont have authority to add data.","Warning");
					}else if(json.no_edit){
						toastr['warning']("You dont have authority to change data.","Warning");
					}else{
						toastr['error']("Please refresh the page and try again later.","Error Occured");
					}
				}
				
				$('.modal-content').unblock();
                $('.spinner').remove();
				//App.unblockUI();
			},
			error: function(response){
				$('.modal-content').unblock();
                $('.spinner').remove();
				
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    var warehouseOptionHandler = function(){

		var row = '<option value="all">Select All</option>';
		row = row + $('#warehouse').html();

		$('#warehouse').html(row);

		$('#warehouse').select2();

        $('#warehouse').on('change', function(e){

        	var ctrl = $(this);

    		var allOption = $('#warehouse option').length;
    		var selectedOption = $('#warehouse option:selected').length;

        	if($.inArray('all',ctrl.val()) != -1){
        		if(allOption != selectedOption){
	        		$('#warehouse option').each(function(){
	        			$(this).prop('selected',true);
	        		})
	        		$('#warehouse option[value="all"]').text('Deselect All');
	        		$('#warehouse option[value="all"]').prop('selected',false);

        		}else{

	        		$('#warehouse option').each(function(){
	        			$(this).prop('selected',false);
	        		})
	        		$('#warehouse option[value="all"]').text('Select All');

        		}

        	}
        	else
        	{

        		if((allOption-1) == selectedOption){

	        		$('#warehouse option[value="all"]').text('Deselect All');

        		}else{

	        		$('#warehouse option[value="all"]').text('Select All');

        		}

        	}
        	$(this).trigger('change.select2');

        });

        $('#warehouse').on('select2:open', function(e){

        	e.preventDefault();

    		var allOption = $('#warehouse option').length;
    		var selectedOption = $('#warehouse option:selected').length;
    		if((allOption-1) == selectedOption){
    			$('#select2-warehouse-results li[id$="all"]').text('Deselect All');
    		}else{
    			$('#select2-warehouse-results li[id$="all"]').text('Select All');
    		}
        })

    }	
    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            handleFormSettings();
            warehouseOptionHandler();
        }

    };  
}();

jQuery(document).ready(function() {
    Handler.init();
});
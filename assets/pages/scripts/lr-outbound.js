var Ref_barang = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    
    var content_datatable;
    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                "language": {
                    "lengthMenu": "Display _MENU_ records",
                    "info" : ", Display _START_ to _END_ of _TOTAL_ entries"
                },
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl+page_class+"/get_list", // ajax source
                },
                "columns": [
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false }
                  ],
                "order": [
                    [1, "desc"]
                ],// set first column as a default sort by asc
            }
        });
    }

    var pageInteraction = function(){
        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
            $("#form-content select").val('').selectpicker('refresh');
            $("table#outbound-barang .added_row").remove();
        });
        $("#customer-form-content").on('hidden.bs.modal', function(){
            $('#customer-form-content .form-group').removeClass('has-error');
            $('#customer-form-content input').val('');
        });
        $('#data-table-filter').submit(function(e){
            e.preventDefault();
            data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
            data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
            data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.hide-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').hide();
            $('.filter-hide-control,.msg-filter').show();
        });
        $('.show-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').show();
            $('.filter-hide-control,.msg-filter').hide();
        });
        $('.reset-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter input[name="nama_data-table"]').val('');
            $('#data-table-filter input[name="kota"]').val('');
            $('#data-table-filter textarea[name="alamat_data-table"]').val('');
            data-table_datatable.clearAjaxParams();
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
        $('.form-close').click(function(e){
            e.preventDefault();
            $(this).closest(".modal").modal('hide');
        });
        $("#new_customer").click(function(e){
            e.preventDefault();
            $("#customer-form-content").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl+'outbound/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    if(! json.no_data){
                        $('#data-table-form input[name="id"]').val(json.id_outbound);
                        $('#data-table-form input[name="kd_outbound"]').val(json.kd_outbound);
                        $('#data-table-form input[name="tanggal_outbound"]').val(json.tanggal_outbound);
                        $('#data-table-form input[name="alamat"]').val(json.alamat);
                        $('#data-table-form select[name="id_customer"]').val(json.id_customer).selectpicker("refresh");
                        $('#data-table-form select[name="jenis_outbound"]').val(json.id_outbound_document).selectpicker("refresh");
                        $('#data-table-form #added_rows').append(json.outbound_barang_html);
                        $('#data-table-form select.add_rule').each(function() {
                            $(this).rules("add", "required");
                        });
                        $('#data-table-form input.add_rule').each(function() {
                            $(this).rules("add", "required");
                        });
                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                        $(".remove_row").click(function(){
                            $(this).closest("tr").remove();
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });
        $(".add_item").click(function(e){
            $.ajax({
                url:BaseUrl+"outbound/get_add_item_row",
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows").append("<tr id='target_new_row'><td colspan='4'>Please Wait</td></tr>");
                },
                success:function(html){
                    $("#target_new_row").replaceWith(html);
                    $(".selectpicker").selectpicker("refresh");
                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();
                    });
                    $('#data-table-form select.add_rule').each(function() {
                        $(this).rules("add", "required");
                    });
                    $('#data-table-form input.add_rule').each(function() {
                        $(this).rules("add", "required");
                    });
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Anda yakin akan menghapus data?", function(result) {
                if(result){
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'outbound/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){
                                toastr['success']("Data berhasil dihapus.","Success");
                                content_datatable.getDataTable().ajax.reload();
                            }else{
                                if(json.no_delete){
                                    toastr['warning']("Anda tidak mempunyai otoritas untuk menghapus data.","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
                        }
                    });
                }
            }); 
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.mdatepicker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

    }

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_outbound: {
                    required: true
                },
                tanggal_outbound: {
                    required: true
                },
                id_customer: {
                    required: true
                },
                jenis_outbound: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }
                
            },*/
            
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'outbound/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error'](json.message,"Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });

        $('#data-table-form-customer').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                customer_name: {
                    required: true
                },
                phone: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'outbound/save_new_customer',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('#data-table-form-customer .modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form-customer button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#customer-form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('#data-table-form-customer .modal-content').unblock();
                        $('#data-table-form-customer .spinner').remove();
                        loadCustomers();
                    }
                });
            }
        });
    }

    var loadCustomers = function() {
        $.ajax({
            type:'POST',
            url:BaseUrl+'outbound/load_customers',
            dataType:'html',
            beforeSend:function(){
                $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                $('.form-group').removeClass('has-error');
            },
            success:function(html){
                $("select[name='id_customer']").html(html);
                $("select[name='id_customer']").selectpicker("refresh");
                $('.modal-content').unblock();
                $('.spinner').remove();
            }
        });
    }
        
    return {

        //main function to initiate the module
        init: function () {
            handleRecords();
            pageInteraction();
            handleFormSettings();
            handleDatePickers();
        }

    };  
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});
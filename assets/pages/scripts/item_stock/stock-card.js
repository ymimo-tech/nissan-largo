var Handler = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    
	var initialize = function(){
		setTimeout(function(){
			initTable(0);
		}, 1000);
	}
	
	var initTable = function(start){
	
		App.blockUI({
			target: '.stock-card'
		});
	
		$.ajax({
			url: BaseUrl + page_class + '/getStockCard',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_barang: $('input[name="id"]').val(),
				warehouse: $('#warehouse-filter').val(),
				start: start
			},
			success: function(response){
				
				var data = response.data,
					len = data.length,
					tpl = '<tr>\
								<td>{{ date }}</td>\
								<td><div align="right">{{ in }}</div></td>\
								<td><div align="right">{{ out }}</div></td>\
								<td><div align="right" class="balance">{{ balance }}</div></td>\
								<td><div align="center">{{ by }}</div></td>\
								<td><div align="center"><a href="{{ url }}" target="_blank">{{ ref }}</a></div></td>\
						   </tr>',
					row = '';
					
				var total = $('.total').html();
					total = total.replace(' ', '');
					total = total.replace('PCS', '');
					total = total.replace(',', '');
					
				//setTimeout(function(){	
				for(var i = 0; i < len; i++){
					row += tpl;
					
					var url = '';
					
					var in1 = 0;
					if(data[i].type == 'IN'){
						in1 = data[i].qty;
					}
					
					var out = 0;
					if(data[i].type == 'OUT'){
						out = data[i].qty;
					}

					if(data[i].type_doc == 'RCV'){

						url = BaseUrl + 'receiving/detail/' + data[i].id_doc;

					}else if(data[i].type_doc == 'PICK'){

						url = BaseUrl + 'picking_list/detail/' + data[i].id_doc;

					}else if(data[i].type_doc == 'KIT'){

						url = BaseUrl + 'order_kitting/detail/' + data[i].id_doc;

					}else if(data[i].type_doc == 'CC'){

						url = BaseUrl + 'cycle_count/check_result/' + data[i].id_doc;

					}else{

						url = 'javascript:;';

					}
					
					if(start <= 0){
						
						if(i > 0){
						
							var j = ( i - 1 );
							
							var tp = data[j].type;
							
							if(tp == 'IN'){
								
								total = parseFloat(total) - parseFloat(data[j].qty);
							}
							
							if(tp == 'OUT'){
								total = parseFloat(total) + parseFloat(data[j].qty);
							}
						
						}
						
					}
					
					if(start > 0){
						
						var j = 1;
						
						$('#stock-card-table tbody tr').each(function(){
							
							var ctrl = $(this);
							
							if(j == $('#stock-card-table tbody tr').length){
								setTimeout(function(){
									var t = ctrl[0].cells[3].textContent;
									var n = ctrl[0].cells[1].textContent;
									var o = ctrl[0].cells[2].textContent;
									
									if(parseFloat(n) != 0){
										t = parseFloat(t) - parseFloat(n);
									}
									
									if(parseFloat(o) != 0){
										t = parseFloat(t) + parseFloat(o);
									}
									
									total = t;
								}, 300);
							}
							
							j++;
							
						});
						
					}
					
					row = row.replace(/{{ date }}/gi, data[i].date);
					row = row.replace(/{{ in }}/gi, in1);
					row = row.replace(/{{ out }}/gi, out);
					//row = row.replace(/{{ balance }}/gi, data[i].balance);
					row = row.replace(/{{ balance }}/gi, total);
					//row = row.replace(/{{ balance }}/gi, '{{ ' + i + ' }}');
					row = row.replace(/{{ by }}/gi, data[i].user_name);
					row = row.replace(/{{ url }}/gi, url);
					row = row.replace(/{{ ref }}/gi, data[i].doc);
				}
				
				/*
				var tr = $(row),
					len = tr.length,
					r = '',
					v = 0,
					a = [];
					
				for(var i = (len-1); i >= 0; i--){
					
					if(i == len){
						var in1 = tr[i].cells[1].innerText,
							out = tr[i].cells[2].innerText;
							
						if(in1 != 0){
							v = in1;
							tr[i].cells[3].innerHTML = in1;
						}else{
							v = out;
							tr[i].cells[3].innerHTML = out;
						}
						
						r += $(tr[i]).prop('outerHTML');
					}
					
					if(i < len){
					
						var in1 = tr[i].cells[1].innerText,
							out = tr[i].cells[2].innerText;
							
						if(in1 != 0 && out == 0){
							v = parseFloat(v) + parseFloat(in1);
							tr[i].cells[3].innerHTML = v;
						}else if(in1 == 0 && out != 0){
							v = parseFloat(v) - parseFloat(out);
							tr[i].cells[3].innerHTML = v;
						}
						
						r += $(tr[i]).prop('outerHTML');
					
					}
					
					row = row.replace('{{ ' + i + ' }}', v);
				}
				*/
				
				
				
					if(start > 0)
						$('#stock-card-table tbody').append(row);
					else
						$('#stock-card-table tbody').html(row);
					
					// if($('#stock-card-table tbody tr').length < $('input[name="card_total"]').val()){
					// 	$('.more').show();
					// }else{
						// $('.more').hide();
					// }
				
				//}, 1000);
				
				App.unblockUI('.stock-card');
				$('#stock-card-table').DataTable({
					"destroy": true,
					"ordering": false
				});
			},
			error: function(response){
				App.unblockUI('.stock-card');
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
		
	}
	
	var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
			$('#stock-card-table').DataTable().destroy();
    		initTable(0);
    	})
		
		$('.more').on('click', function(){
			initTable($('#stock-card-table tbody tr').length);
		});

		$('#export-stockcard').on('click', function(){

			var total = $('.total').html();
				total = total.replace(' ', '');
				total = total.replace('PCS', '');
				total = total.replace(',', '');

			post(BaseUrl + page_class + '/stockcard_to_excel',{
				id_barang: $('input[name="id"]').val(),
				all_qty: total
			});
		});

	}
		
	return {

        init: function() {
			initialize();
			pageInteraction();
        }

    };	
}();

jQuery(document).ready(function() {
    Handler.init();
});
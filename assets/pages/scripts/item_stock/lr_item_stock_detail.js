var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable,
		dataTable2,
		reprint = [];

	var initialize = function(){
		initTable();
		//initTableQty();
		initQty();
	}

	var initQty = function(){
		$.ajax({
			url:  BaseUrl + page_class + '/getListQty',
			type: 'POST',
			dataType: 'JSON',
			data:{
				id_barang: $('input[name="id"]').val(),
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){

				var rcv = $(response.rec_qty).html() || 0,
					on = $(response.onhand_qty).html() || 0,
					total = response.total_qty

/*				var total = parseInt(rcv) + parseInt(on);

				if(isNaN(total))
					total = 0;
*/
				$('.total').html(total);

				$('.rec h1').html(response.rec_qty);
				$('.rec span').html('RECEIVING');

				$('.on h1').html(response.onhand_qty);
				$('.on span').html('ON HAND');

				$('.all h1').html(response.allocated_qty);
				$('.all span').html('PICKED');

				$('.sus h1').html(response.suspend_qty);
				$('.sus span').html('HOLD');

				$('.ava h1').html(response.available_qty);
				$('.ava span').html('AVAILABLE');

				$('.ng h1').html(response.damage_qty);
				$('.ng span').html('DAMAGE');
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var initTable = function (params) {

        dataTable = new Datatable();

        dataTable.init({
            src: $("#table_receiving"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/get_list_detail/", // ajax source
					"data": {
						id_barang: $('input[name="id"]').val(),
						warehouse: $('#warehouse-filter').val(),
						params: params
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
          					{ "orderable": false },
          					{ "orderable": false },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": false },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true }
                ],
                "order": [
                    [2, "ASC"]
                ],
				        "columnDefs": [
        				  {
        						"render": function(data, type, row){
        							return ('<div vertical-align="top" align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
        						},
        						"targets": [1, 2, 3, 4, 5, 6, 7, 8, 10]
        					}
        				]
            }
        });
    }

	var initTableQty = function () {

        dataTable2 = new Datatable();

        dataTable2.init({
            src: $("#table-qty"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"bPaginate": false,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getListQty", // ajax source
					"data": {
						id_barang: $('input[name="id"]').val(),
						warehouse: $('#warehouse-filter').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
          					{ "orderable": false },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1, 2, 3, 4, 5]
					}
				]
            }
        });
    }

	var showModalPrint = function(){

		var c = 0;

		reprint = [];

		$('#table_receiving tbody input[type="checkbox"]').each(function(){
			if($(this).is(':checked')){
				reprint.push($(this).val());
				c++;
			}
		});

		$('.qty-print').html(c);
		$('.type_print').val('serial-number');

		$('#modal-print').modal({
			backdrop: 'static',
			keyboard: true
		});

	}

	var showModalPrintLicensePlate = function(){

		var c = 0;

		reprint = [];

		$('#table_receiving tbody input[type="checkbox"]').each(function(){
			if($(this).is(':checked')){
				if($(this).attr('data-lp') !== '-'){
					if($.inArray($(this).attr('data-lp'),reprint)){
						reprint.push($(this).attr('data-lp'));
						c++;
					}
				}
			}
		});

		$('.qty-print').html(c);
		$('.type_print').val('license-plate');

		$('#modal-print').modal({
			backdrop: 'static',
			keyboard: true
		});

	}

	var printBarcode = function(){

		var $btn = $('#submit-print');
			$btn.button('loading');

		var url="";

		if(reprint.length > 0){
			if($('#type_print').val() == "serial-number"){
				url = BaseUrl + page_class + '/printZpl';
			}else{
				url = BaseUrl + page_class + '/printZplLP';
			}

			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'JSON',
				data: {
					sn: reprint
				},
				success: function(response){

					if(response.status == 'OK'){
						location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
					}else
						bootbox.alert(response.message);

					$('#modal-print').modal('hide');
					$btn.button('reset');

					/*
					var zpl = response.zpl;

					var ctrl = $(zpl);

					var postdata = {
						sid: $('#sid').val(),
						pid: $('#pid').val(),
						printerCommands: zpl
					}
					// store user printer settings & commands in server cache
					$.post(BaseUrl + 'webclientprint/ProcessPrint.php',
						postdata,
						function() {
							 // Launch WCPP at the client side for printing...
							 var sessionId = $("#sid").val();
							 jsWebClientPrint.print('sid=' + sessionId);
						}
					);
					*/
				},
				error: function(response){
					$btn.button('reset');
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
		}else{

			toastr['error']('Please check outer label is empty', 'ERROR');

			$('#modal-print').modal('hide');
			$btn.button('reset');

		}
	}

	var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    		initQty();
    		initTableQty();
    	})

		$('#reload').on('click', function(){
			initTable();
		});

		$('.qty').on('click', 'a', function(){
			var ctrl = $(this),
				params = ctrl.attr('data-params');

			initTable(params);
		});

		$('#reprint').on('click', function(){
			if(!$('#table_receiving tbody input[type="checkbox"]').is(':checked')){
				toastr['error']('Please check row at least one for reprint', 'ERROR');
				return false;
			}

			showModalPrint();
		});

		$('#reprint-license-plate').on('click', function(){
			if(!$('#table_receiving tbody input[type="checkbox"]').is(':checked')){
				toastr['error']('Please check row at least one for reprint', 'ERROR');
				return false;
			}

			showModalPrintLicensePlate();
		});

		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});

		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});

		$('#modal-print').on('shown.bs.modal', function () {
			$('#qty-print').focus();
		});

		$('#button-print-qty-barcode').on('click', function(e){
			e.preventDefault();

			var id = $('#data-table-form input[name="id"]').val();

			showModalPrint(id);
		});

		$('#export-detail').click(function(){
			post(BaseUrl + page_class + '/get_list_detail_export',{
				id_barang: $('input[name="id"]').val(),
				warehouse: $('#warehouse-filter').val()
			});
		})
	}

    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    handler.init();
});

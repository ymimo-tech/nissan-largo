var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable,
		dataTable2,
		reprint = [];

	var content_datatable = '';

	var unique_code = '';

	var id_item = '';

	var initialize = function(){

		id_item = $("#id_item").val();
		console.log(id_item);

		$('#item,#code, #status_item,#status_quarantine, #cat, #loc, #type, #uom, #auto_release, #quarantine').select2();

		$('#periode_out').select2({
			allowClear: false,
			width: null
		});

		$('#periode').select2({
			allowClear: false,
			width: null
		});

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date',
			}
		});

		var dateFilter2 = MimoPeriode.params({
			selector :{
				periode: '#periode_out',
				from: '#from_out',
				to: '#to_out',
				clear: '#clear-date-out'
			}
		});

		dateFilter.init();
		dateFilter2.init();
		setTimeout(function(){
            $('#periode').val('this_year').trigger('change');
			initTable();
		}, 500);

		initTable();
		//initTableQty();
		initQty();
	}

	var initQty = function(){
		$.ajax({
			url:  BaseUrl + page_class + '/getListQty',
			type: 'POST',
			dataType: 'JSON',
			data:{
				id_barang: $('input[name="id"]').val(),
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){

				var rcv = $(response.rec_qty).html() || 0,
					on = $(response.onhand_qty).html() || 0,
					total = response.total_qty

/*				var total = parseInt(rcv) + parseInt(on);

				if(isNaN(total))
					total = 0;
*/
				$('.total').html(total);

				$('.rec h1').html(response.rec_qty);
				$('.rec span').html('RECEIVING');

				$('.on h1').html(response.onhand_qty);
				$('.on span').html('ON HAND');

				$('.all h1').html(response.allocated_qty);
				$('.all span').html('ALLOCATED');

				$('.sus h1').html(response.suspend_qty);
				$('.sus span').html('HOLD');

				$('.ava h1').html(response.available_qty);
				$('.ava span').html('AVAILABLE');

				$('.ng h1').html(response.damage_qty);
				$('.ng span').html('DAMAGE');
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var initTable = function (params='') {

		dataTable = new Datatable();
		// console.log(this.href.substring(this.href.lastIndexOf('/') + 1));

        dataTable.init({
            src: $("#table_receiving"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/get_list_detail/", // ajax source
					"data": {
						id_barang: $('input[name="id"]').val(),
						warehouse: $('#warehouse-filter').val(),
						status_quarantine: $('#status_quarantine').val(),
						status_item: $('#status_item').val(),
						from_in: $('#from').val(),
						to_in: $('#to').val(),
						periode_in:$('#periode').val(),
						from_out: $('#from_out').val(),
						to_out: $('#to_out').val(),
            periode_out:$('#periode_out').val(),
						params: params
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
          					{ "orderable": false },
          					{ "orderable": false },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": true , "visible" : false},
          					{ "orderable": true },
          					{ "orderable": false },
          					{ "orderable": true },
          					{ "orderable": true , "visible" : false},
          					{ "orderable": true },
          					{ "orderable": true },
          					{ "orderable": false },
          					{ "orderable": false },
                ],
                "order": [
                    [2, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div style="vertical-align:top; text-align:center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1, 2, 3, 4, 5, 6, 7, 8]
					}
				]
            }
        });
    }

	var initTableQty = function () {

        dataTable2 = new Datatable();

        dataTable2.init({
            src: $("#table-qty"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"bPaginate": false,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getListQty", // ajax source
					"data": {
						id_barang: $('input[name="id"]').val(),
						warehouse: $('#warehouse-filter').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div style="vertical-align:top; text-align:center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1, 2, 3, 4, 5]
					}
				]
            }
        });
    }

	var showModalPrint = function(){

		var c = 0;

		reprint = [];

		$('#table_receiving tbody input[type="checkbox"]').each(function(){
			if($(this).is(':checked')){
				reprint.push($(this).val());
				c++;
			}
		});

		$('.qty-print').html(c);
		$('.type_print').val('serial-number');

		$('#modal-print').modal({
			backdrop: 'static',
			keyboard: true
		});

	}

	var showModalPrintLicensePlate = function(){

		var c = 0;

		reprint = [];

		$('#table_receiving tbody input[type="checkbox"]').each(function(){
			if($(this).is(':checked')){
				if($(this).attr('data-lp') !== '-'){
					if($.inArray($(this).attr('data-lp'),reprint)){
						reprint.push($(this).attr('data-lp'));
						c++;
					}
				}
			}
		});

		$('.qty-print').html(c);
		$('.type_print').val('license-plate');

		$('#modal-print').modal({
			backdrop: 'static',
			keyboard: true
		});

	}

	var printBarcode = function(){

		var $btn = $('#submit-print');
			$btn.button('loading');

		var url="";

		if(reprint.length > 0){
			if($('#type_print').val() == "serial-number"){
				url = BaseUrl + page_class + '/printZpl';
			}else{
				url = BaseUrl + page_class + '/printZplLP';
			}

			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'JSON',
				data: {
					sn: reprint
				},
				success: function(response){

					if(response.status == 'OK'){
						location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
					}else
						bootbox.alert(response.message);

					$('#modal-print').modal('hide');
					$btn.button('reset');

					/*
					var zpl = response.zpl;

					var ctrl = $(zpl);

					var postdata = {
						sid: $('#sid').val(),
						pid: $('#pid').val(),
						printerCommands: zpl
					}
					// store user printer settings & commands in server cache
					$.post(BaseUrl + 'webclientprint/ProcessPrint.php',
						postdata,
						function() {
							 // Launch WCPP at the client side for printing...
							 var sessionId = $("#sid").val();
							 jsWebClientPrint.print('sid=' + sessionId);
						}
					);
					*/
				},
				error: function(response){
					$btn.button('reset');
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
		}else{

			toastr['error']('Please check outer label is empty', 'ERROR');

			$('#modal-print').modal('hide');
			$btn.button('reset');

		}
	}

	var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    		initQty();
    		initTableQty();
    	})

		$('#reload').on('click', function(){
			initTable();
		});

		$('.qty').on('click', 'a', function(){
			var ctrl = $(this),
				params = ctrl.attr('data-params');

			initTable(params);
		});

		$('#reprint').on('click', function(){
			if(!$('#table_receiving tbody input[type="checkbox"]').is(':checked')){
				toastr['error']('Please check at least one row for reprint', 'ERROR');
				return false;
			}

			showModalPrint();
		});

		$('#reprint-license-plate').on('click', function(){
			if(!$('#table_receiving tbody input[type="checkbox"]').is(':checked')){
				toastr['error']('Please check at least one row for reprint', 'ERROR');
				return false;
			}

			showModalPrintLicensePlate();
		});

		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});

		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});

		$('#modal-print').on('shown.bs.modal', function () {
			$('#qty-print').focus();
		});

		$('#button-print-qty-barcode').on('click', function(e){
			e.preventDefault();

			var id = $('#data-table-form input[name="id"]').val();

			showModalPrint(id);
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();

			PARAMS = '';

			initTable();
		});

		$('#reset').on('click', function(){
			$('#item, #status, #cat, #loc, #loc-type,#status_item, #uom').val(null).trigger('change');
		});

		$('#export-detail').click(function(){
			post(BaseUrl + page_class + '/get_list_detail_export',{
				id_barang: $('input[name="id"]').val(),
				warehouse: $('#warehouse-filter').val()
			});
		})

		$('#allrelease').click(function(){
			console.log('success');

			var unique_code = $('input[name="sn[]"]:checked').map(function () { return this.value; }).get();
			var data_qty = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-qty'); }).get();
			var data_statusItem = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-statusItem'); }).get();
			var data_maxQuarantine = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-maxQuarantine'); }).get();
			var data_inDate = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-inDate'); }).get();
			console.log(data_qty);

			console.log(unique_code);
			$.ajax({
				url: BaseUrl + "quarantine/manual_release/?unique_code=" + unique_code + "&item_id=" + id_item + "&data_qty=" + data_qty + "&data_statusItem=" + data_statusItem + "&data_maxQuarantine=" + data_maxQuarantine + "&data_inDate=" + data_inDate,
				type: "GET",
				dataType : 'json',
				success:function(data) {
					console.log(data.status);
					if(data.status){
						toastr['success']("Item is successfully released.","Success");
						dataTable.getDataTable().ajax.reload();
					}else{
						toastr['error']('Serial Number '+data.serial_number+' cannot be processed. You must Hold it first!', 'ERROR');
						dataTable.getDataTable().ajax.reload();
						return false;
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Error Release Item');
				}
			});
		});

		$('#allreject').click(function(){
			console.log('success');

			$('#modal-remark-allreject').modal({
				keyboard: true,
				backdrop: 'static'
			});

			var unique_code = $('input[name="sn[]"]:checked').map(function () { return this.value; }).get();
			var data_qty = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-qty'); }).get();
			var data_statusItem = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-statusItem'); }).get();
			var data_maxQuarantine = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-maxQuarantine'); }).get();
			var data_inDate = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-inDate'); }).get();

			console.log(unique_code);
			$('#data-modal-remark-allreject').on('submit', function(e){
				e.preventDefault();
			$.ajax({
				url: BaseUrl + "quarantine/manual_reject/?unique_code=" + unique_code + "&item_id=" + id_item + "&data_qty=" + data_qty + "&data_statusItem=" + data_statusItem + "&data_maxQuarantine=" + data_maxQuarantine + "&data_inDate=" + data_inDate + "&reason_reject=" + $('#remark_allreject').val(),
				type: "GET",
				dataType : 'json',
				success: function (data) {
					console.log(data.status);
					if(data.status){
						toastr['success']("Item is successfully rejected.","Success");
						dataTable.getDataTable().ajax.reload();
					}else{
						toastr['error']('Serial Number '+data.serial_number+' cannot be processed. You must Hold it first!', 'ERROR');
						dataTable.getDataTable().ajax.reload();
						return false;
					}
					$('#modal-remark-allreject').modal('hide');
					$('#remark_allreject').val('');
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Error Reject Item');
				}
			});
		});

		});

		$('#allhold').click(function(){
			console.log('success');

			var unique_code = $('input[name="sn[]"]:checked').map(function () { return this.value; }).get();
			var data_qty = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-qty'); }).get();
			var data_statusItem = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-statusItem'); }).get();
			var data_maxQuarantine = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-maxQuarantine'); }).get();
			var data_inDate = $('input[name="sn[]"]:checked').map(function () { return $(this).attr('data-inDate'); }).get();

			console.log(unique_code);
			$.ajax({
				url: BaseUrl + "quarantine/manual_hold/?unique_code=" + unique_code + "&item_id=" + id_item + "&data_qty=" + data_qty + "&data_statusItem=" + data_statusItem + "&data_maxQuarantine=" + data_maxQuarantine + "&data_inDate=" + data_inDate,
				type: "GET",
				dataType : 'json',
				success:function(data) {
					if(data.status){
						toastr['success']("Item is successfully hold.","Success");
						dataTable.getDataTable().ajax.reload();
					}else{
						toastr['error']('Serial Number '+data.serial_number+' cannot be processed', 'ERROR');
						dataTable.getDataTable().ajax.reload();
						return false;
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Error Hold Item');
				}
			});
		});

		$(document).on('click', '.data-table-release', function(e){
			e.preventDefault();
			var unique_code = $(this).attr('data-id');

			bootbox.confirm("Are you sure to release this Serial Number: " + unique_code + " ?", function(result){
				if(result){
					$.ajax({
						type : 'POST',
						url	: BaseUrl + 'quarantine/action_release',
						data : { 'unique_code' : unique_code},
						dataType : 'json',
						success:function(result) {
							if(result){
								console.log(result);
								toastr['success']["Item is successfully released.","Success"];
								dataTable.getDataTable().ajax.reload();
								// window.location.reload();
							}else{
                                // if(result.no_delete){
                                //     toastr['warning']("Anda tidak mempunyai otoritas untuk menghapus data.","Warning");
                                // }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
					});
				}
			});
		});

		$(document).on('click', '.data-table-reject', function(e){
			unique_code = $(this).attr('data-id');
			console.log(unique_code);

			$('#modal-remark').modal({
				keyboard: true,
				backdrop: 'static'
			});

		});

		$('#data-modal-remark').on('submit', function(e){
			e.preventDefault();

			console.log('hello succes');
			console.log($('#remark').val());

			$.ajax({
				type : 'POST',
				url	: BaseUrl + 'quarantine/action_reject',
				data : {
					'unique_code' : unique_code,
					'remark' : $('#remark').val()
				},
				dataType : 'json',
				success:function(result) {
					if(result){
						console.log(result);
						toastr['success']["Item is successfully rejected.","Success"];
						dataTable.getDataTable().ajax.reload();
						$('#submit-remark').button('reset');
						// window.location.reload()
					}else{
						// if(result.no_delete){
						//     toastr['warning']("Anda tidak mempunyai otoritas untuk menghapus data.","Warning");
						// }else{
							toastr['error']("Please refresh the page and try again later.","Error Occured");
						}

					$('#remark').val('');
					$('#modal-remark').modal('hide');

					},
					error: function(response){
						App.unblockUI();

						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
			});
		});

		$(document).on('click', '.data-table-hold', function(e){
			e.preventDefault();
			var unique_code = $(this).attr('data-id');
			bootbox.confirm("Are you sure to HOLD Serial Number " + unique_code + " ?", function(result){
				if(result){
					$.ajax({
						type : 'POST',
						url	: BaseUrl + 'quarantine/action_hold',
						data : { 'unique_code' : unique_code},
						dataType : 'json',
						success:function(result) {
							if(result){
								console.log(result);
								toastr['success']["Item is successfully hold.","Success"];
								dataTable.getDataTable().ajax.reload();
							}else{
                                // if(result.no_delete){
                                //     toastr['warning']("Anda tidak mempunyai otoritas untuk menghapus data.","Warning");
                                // }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
					});
				}
			});
		});

	}

    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    handler.init();
});

var Ref_barang = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    
    var content_datatable,
		PARAMS = '';
	
	var initialize = function(){
		$('#item,#code, #status, #cat, #loc, #loc-type, #uom, #auto_release, #quarantine').select2();
		
		initQty();
	}
	
	var initQty = function(){
		$.ajax({
			url:  BaseUrl + 'dashboard/getActiveStock',
			type: 'POST',
			dataType: 'JSON',
			data:{
                warehouse : $('#warehouse-filter').val()
			},
			success: function(response){

                var style;
				var total = response.total;
				
				if(isNaN(total))
					total = 0;
				
				$('.total').html(total.toLocaleString());
				
				var rec = parseFloat(response.data[0]['qty']);
				if(rec > 0){
                    if(rec > 100000){
                        style="style='font-size:80%'"
                    }
					rec = '<a href="javascript:;" data-params="receiving" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail"'+style+'>'+rec.toLocaleString()+'</a>';
				}
				
				var on = parseFloat(response.data[1]['qty']);
				if(on > 0){
                    if(on > 100000){
                        style="style='font-size:80%'"
                    }
					on = '<a href="javascript:;" data-params="onhand" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail"'+style+'>'+on.toLocaleString()+'</a>';
				}
				
				var all = parseFloat(response.data[2]['qty']);
				if(all > 0){
                    if(all > 100000){
                        style="style='font-size:80%'"
                    }
					all = '<a href="javascript:;" data-params="allocated" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail"'+style+'>'+all.toLocaleString()+'</a>';
				}
				
				var spn = parseFloat(response.data[3]['qty']);
				if(spn > 0){
                    if(spn > 100000){
                        style="style='font-size:80%'"
                    }
					spn = '<a href="javascript:;" data-params="suspend" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail"'+style+'>'+spn.toLocaleString()+'</a>';
				}
				
				var dam = parseFloat(response.data[4]['qty']);
				if(dam > 0){
                    if(dam > 100000){
                        style="style='font-size:80%'"
                    }

					dam = '<a href="javascript:;" data-params="damage" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail"'+style+'>'+dam.toLocaleString()+'</a>';
				}
				
				var avail = parseFloat(response.data[5]['qty']);
				if(avail > 0){
                    if(avail > 100000){
                        style="style='font-size:80%'"
                    }
					avail = '<a href="javascript:;" data-params="available" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail"'+style+'>'+avail.toLocaleString()+'</a>';
				}
				
				$('.rec h1').html(rec);
				$('.rec span').html('RECEIVING');
				
				$('.on h1').html(on);
				$('.on span').html('ON HAND');
				
				$('.all h1').html(all);
				$('.all span').html('ALLOCATED');
				
				$('.sus h1').html(spn);
				$('.sus span').html('HOLD');
				
				$('.ng h1').html(dam);
				$('.ng span').html('DAMAGE');
				
				$('.ava h1').html(avail);
				$('.ava span').html('AVAILABLE');
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}

		});
        $.ajax({
            url:  BaseUrl + page_class + "/getLowQty",
            type: 'POST',
            dataType: 'JSON',
            data:{
                warehouse : $('#warehouse-filter').val()
            },
            success: function(response){
                $('.low-qty').html(response.total);
            },
            error: function(response){
                if(response.status == 401)
                    location.href = BaseUrl + 'login';
                else
                    bootbox.alert(response.responseText);
            }
        });

	}
	
    var handleRecords = function (params) {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
				if(response.customGroupAction=='OK'){
					toastr['success'](response.customGroupActionMessage,"Success");
				}
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // "language": {
					// "lengthMenu": "Display _MENU_ records",
					// "info" : ", Display _START_ to _END_ of _TOTAL_ entries"
				// },
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"bDestroy": true, 
				"autoWidth": true,
				//"scrollX": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl + page_class + "/getList", // ajax source
					"data":{
                        id_barang: $('#item').val(),
                        code_barang: $('#code').val(),
						id_satuan: $('#uom').val(),
						id_qc: $('#status').val(),
						auto_release: $('#auto_release').val(),
						quarantine: $('#quarantine').val(),
						loc_type: $('#loc-type').val(),
                        warehouse: $('#warehouse-filter').val(),
						params: params
					}
                },
				"columns": [
					//{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
                    { "orderable": true },
					// { "orderable": true },
					// { "orderable": true },
					{ "orderable": true },
					// { "orderable": false },
					{ "orderable": true },
					{ "orderable": true }
				  ],
                "order": [
                    [1, "DESC"]
                ],
                // set first column as a default sort by asc
				"columnDefs": [
					//{ "width": "7%", "targets": [1] },
					//{ "width": "10%", "targets": [2] },
					//{ "width": "5%", "targets": [6] },
					{
						"render": function(data, type, row){
							
							var st = data.split(' ');
							var a = parseFloat(st[0]);
							
							var m = row[6].split(' ');
							var b = parseFloat(m[0]);
							
							if(a <= b){
								return ('<div align="right"><span class="label label-danger">{{ data }}</span></div>').replace(/{{ data }}/gi, data);
							}else if(a > b){
								return ('<div align="right"><span class="label label-success">{{ data }}</span></div>').replace(/{{ data }}/gi, data);
							}else{
								return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
							}							
						},
						"targets": [5]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data || 0);
						},
						"targets": [4]
					},
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data || 0);
						},
						"targets": [0, 5]
					}
				]
            }
        });
    }

    var pageInteraction = function(){

        $('#warehouse-filter').change(function(){
            initQty();
            handleRecords();
        });

        $('#export-format-soh').click(function(){
            post(BaseUrl + page_class + '/exportStockOnHand',
            {
                warehouse: $('#warehouse-filter').val()
            });

        });

        // $('#periode').select2({
		// 	allowClear: false,
		// 	width: null
		// });

		// $('.date-picker').datepicker({
		// 	orientation: "left",
		// 	format: 'dd/mm/yyyy',
		// 	autoclose: true
		// });

		// var dateFilter = MimoPeriode.params({
		// 	selector :{
		// 		periode: '#periode',
		// 		from: '#from',
		// 		to: '#to',
		// 		clear: '#clear-date'
		// 	}
		// });

		// var dateFilter2 = MimoPeriode.params({
		// 	selector :{
		// 		periode: '#periode_out',
		// 		from: '#from_out',
		// 		to: '#to_out',
		// 		clear: '#clear-date-out'
		// 	}
		// });

		// dateFilter.init();

        $("#item")
          .select2({
            multiple: true,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            minimumInputLength: 3,
            ajax: {
                url: BaseUrl + 'quarantine/option',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        $("#code")
          .select2({
            multiple: true,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            minimumInputLength: 3,
            ajax: {
                url: BaseUrl + 'quarantine/code',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        // $("#loc")
        //   .select2({
        //     multiple: false,
        //     allowClear: true,
        //     width: null,
        //     placeholder: "-- All --",
        //     minimumInputLength: 0,
        //     ajax: {
        //         // url: BaseUrl + 'referensi_lokasi/options',
        //         url: BaseUrl + 'quarantine/duration',
        //         dataType: 'json',
        //         type: 'POST',
        //         data: function (q, page) {
        //             return {
        //                 query: q.term
        //             };
        //         },
        //         processResults: function (data) {
        //             return {
        //                 results: $.map(data.results, function (item) {
        //                     return {
        //                         text: item.name,
        //                         id: item.id
        //                     }
        //                 })
        //             };
        //         }
        //     }
        // });
        		
		$('.low-qty').on('click', function(){
			var params = 'low';
				PARAMS = params;
			
			$('#item, #status, #cat, #loc, #loc-type, #uom').val(null).trigger('change');
			
			handleRecords(params);
		});
		
		$('.qty').on('click', 'a', function(){
			var ctrl = $(this),
				params = ctrl.attr('data-params');
				
				PARAMS = params;
			
			$('#item, #status, #cat, #loc, #loc-type, #uom').val(null).trigger('change');
			
			handleRecords(params);
		});
		
		$('#export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{
				id_barang: $('#item').val(),
				id_satuan: $('#uom').val(),
				id_qc: $('#status').val(),
				id_kategori: $('#cat').val(),
				loc_id: $('#loc').val(),
				loc_type: $('#loc-type').val(),
                warehouse: $('#warehouse-filter').val(),
				params: PARAMS
			});
		});

        $('#export-format2').on('click', function(){
            post(BaseUrl + page_class + '/export_format2',
            {
                id_barang: $('#item').val(),
                id_satuan: $('#uom').val(),
                id_qc: $('#status').val(),
                id_kategori: $('#cat').val(),
                loc_id: $('#loc').val(),
                loc_type: $('#loc-type').val(),
                warehouse: $('#warehouse-filter').val(),
                params: PARAMS
            });
        });
		
		$('#form-search').on('submit', function(e){
			e.preventDefault();
			
			PARAMS = '';
			
			handleRecords();
		});
		
		$('#reset').on('click', function(){
			$('#item, #status, #cat, #loc, #loc-type, #uom, #code, #auto_release, #quarantine').val(null).trigger('change');
		});
		
        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
        });
        $('#data-table-filter').submit(function(e){
            e.preventDefault();
            data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
            data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
            data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.hide-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').hide();
            $('.filter-hide-control,.msg-filter').show();
        });
        $('.show-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').show();
            $('.filter-hide-control,.msg-filter').hide();
        });
        $('.reset-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter input[name="nama_data-table"]').val('');
            $('#data-table-filter input[name="kota"]').val('');
            $('#data-table-filter textarea[name="alamat_data-table"]').val('');
            data-table_datatable.clearAjaxParams();
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });
        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl+'referensi_barang/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    console.log(json);
                    if(! json.no_data){
                        $('#referensi-barang-form input[name="id"]').val(json.id_barang);
                        $('#referensi-barang-form input[name="kd_barang"]').val(json.kd_barang);
                        $('#referensi-barang-form input[name="nama_barang"]').val(json.nama_barang);
                        $('#referensi-barang-form select[name="id_kategori"]').val(json.id_kategori);
                        $('#referensi-barang-form select[name="id_satuan"]').val(json.id_satuan);
                        $('#referensi-barang-form input[name="tipe_barang"]').val(json.tipe_barang);
                        $('#referensi-barang-form select[name="shipment_type"]').val(json.shipment_type);
                        $('#referensi-barang-form select[name="fifo_period"]').val(json.fifo_period);
                        $('#referensi-barang-form select[name="id_supplier"]').val(json.id_owner);
                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Anda yakin akan menghapus data?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'referensi_barang/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){
                                toastr['success']("Data berhasil dihapus.","Success");
                                content_datatable.getDataTable().ajax.reload();
                            }else{
                                if(json.no_delete){
                                    toastr['warning']("Anda tidak mempunyai otoritas untuk menghapus data.","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
                        }
                    });
                }
            }); 
        });

        $('#loc').select2({
            ajax: {
                url: BaseUrl + page_class + "/duration",
                dataType: 'json',
                method: 'POST',
                delay: 250,
                cache: false,
                data: function (params) {
                    return {
                        term: params.term,
                        page: params.page
                    };
                },          
                processResults: function(data) {
                    var result = $.map(data, function (loc) { return { id: loc.id, text: loc.name }});
                    return { results: result };
                }
            }
        });

        $('#sync').click(function(){

            $('#sync').attr('disabled','disabled');

            $.ajax({
                url: BaseUrl+"sync/send_to_staging_stok",
                dataType:'json',
                method:'POST',
                success : function(response){
    
                    $('#sync').removeAttr('disabled');
                    toastr['success']("Sync Success","Success");
                },
                error: function(err){
                    console.log(err);
                }
            });

        });

    }

    var handleFormSettings = function() {
        $('#referensi-barang-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                kd_barang: {
                    required: true
                },
                nama_barang: {
                    required: true
                },
                tipe_barang: {
                    required: true
                },
                owner_name: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }
                
            },*/
            
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'referensi_barang/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#referensi-barang-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }
		
	return {

        //main function to initiate the module
        init: function () {
			initialize();
            handleRecords();
            pageInteraction();
            handleFormSettings();
            /*handleBootstrapSelect();
            handleDatePickers();
            
            */
        }

    };	
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});
var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
	
	var dataTable;
	
	var initialize = function(){
		initTable();
	}
	
	var initTable = function () {

        dataTable = new Datatable();		
		
        dataTable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/get_list/",
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + page_class + '/login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false },
                    { "orderable": false }
                ],
                "order": [
                    [1, "ASC"]
                ]
            }
        });
    }
	
	var pageInteraction = function(){
        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
            $("#form-content select").val("refresh").selectpicker("refresh");
        });

        var loadChildMenu = function(kd_grup_menu, selected_val = '') {
            $.ajax({
                type:'POST',
                url:BaseUrl+'menu/get_parent',
                data:{'kode':kd_grup_menu, 'selected':selected_val},
                dataType:'html',
                success:function(html){
                    console.log(html);
                    $('#data-table-form select[name="kd_parent"]').html(html).selectpicker("refresh");
                }
            });
        }

		$('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });

        $(document).on("change", "select[name='kd_grup_menu']", function(e){
            e.preventDefault();
            loadChildMenu($(this).val());
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl+'menu/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    console.log(json);
                    if(! json.no_data){
                        $('#data-table-form input[name="id"]').val(json.kd_menu);
                        $('#data-table-form input[name="nama_menu"]').val(json.nama_menu);
                        $('#data-table-form input[name="deskripsi"]').val(json.deskripsi);
                        $('#data-table-form input[name="url"]').val(json.url);
                        $('#data-table-form select[name="kd_grup_menu"]').val(json.kd_grup_menu).selectpicker("refresh");
                        $('#data-table-form select[name="kd_parent"]').val(json.kd_parent).selectpicker("refresh");
                        loadChildMenu(json.kd_grup_menu, json.kd_parent);
                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Anda yakin akan menghapus data?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'menu/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){
                                toastr['success']("Data berhasil dihapus.","Success");
                                dataTable.getDataTable().ajax.reload(null, false);
                            }else{
                                if(json.no_delete){
                                    toastr['warning']("Anda tidak mempunyai otoritas untuk menghapus data.","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
                        }
                    });
                }
            }); 
        });

		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});
		
		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});
		
		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});
		
		$('#modal-print').on('shown.bs.modal', function () { 
			$('#qty-print').focus();
		});
		
		$('#button-print-qty-barcode').on('click', function(e){
			e.preventDefault();
			
			var id = $('#data-table-form input[name="id"]').val();
			
			showModalPrint(id);
		});
	}

	var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                nama_menu: {
                    required: true
                },
                url: {
                	required: true
                },
                kd_grup_menu: {
                	required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }
                
            },*/
            
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'menu/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Menu data updated","Success");
                            }else{
                                toastr['success']("New menu created","Success");
                            }
                            dataTable.getDataTable().ajax.reload(null, false);
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }
        
    return {

        init: function () {
			initialize();
			pageInteraction();
			handleFormSettings();
        }

    };  
}();

jQuery(document).ready(function() {
    handler.init();
});
var Receiving = function () {
    
    var content_datatable;
	
	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd-mm-yyyy',
			autoclose: true
		});
		
		$('#supplier, #year').select2({
			allowClear: false,
			width: null
		});
	}
	
    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
				if(response.customGroupAction=='OK'){
					toastr['success'](response.customGroupActionMessage,"Success");
				}
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                "language": {
					"lengthMenu": "Display _MENU_ records",
					"info" : ", Display _START_ to _END_ of _TOTAL_ entries"
				},
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl+"receiving/get_list", // ajax source
                },
				"columns": [
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false },
				  ],
                "order": [
                    [1, "desc"]
                ],// set first column as a default sort by asc
            }
        });
    }
	
	var reset = function(){
		$('#po').val('');
		$('#supplier').select2('val', '');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#year').select2('val', 0);
	}
	
	var clickListener = function(){
		$('#reset').on('click', function(){
			reset();
		});
	}
	
	return {

        //main function to initiate the module
        init: function () {
			initialize();
            handleRecords();
			clickListener();
            /*handleBootstrapSelect();
            handleDatePickers();
            pageInteraction();
            handleFormSettings();*/
        }

    };	
}();

jQuery(document).ready(function() {
    Receiving.init();
});
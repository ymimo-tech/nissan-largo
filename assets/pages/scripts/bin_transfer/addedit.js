var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var content_datatable,
		idDestination,idSource, idWarehouse, idDocument;

	var initialize = function(){

		var id = $('input[name="id"]').val();
		if(id.length > 0){
			edit(id);
		}else{
			$.ajax({
				type:'POST',
				url:BaseUrl + page_class + '/getDocumentCode',
				dataType:'json',
				success:function(response){
					$('#bin_transfer_code').val(response.code);
					$('#bin_transfer_code_text').html(response.code);
				},
				error:function(err){
					console.log(err);
				}
			});
		}

	}

	var edit = function(id){

		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/getEdit',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(json.status){

					$('#bin_transfer_code_text').html(json.d.bin_transfer_code);
					$('#bin_transfer_code').val(json.d.bin_transfer_code);
					$('#warehouse').val(json.d.warehouse_id).trigger('change');
					$('#remark').val(json.d.bin_transfer_remark).trigger('change');

					$('#data-table-form #added_rows').append(json.d.items);

					if(json.d.bin_transfer_start){
						$('#save').remove();
					}
										
					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					
					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();
					
					//$(".selectpicker").selectpicker("refresh");
					
					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});

					var j = 0;
					$('.added_row').each(function(){
						console.log('heress');
						$(this).find('select.add_rule').attr('name', 'id_barang['+j+']');
						$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');
						
						j++;
					});
					
				}else{

					toastr['error'](json.message,"Error");

				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    var pageInteraction = function(){

    	$('#warehouse').select2();

		$(document).on('change', '#data-table-form select.item-outbound', function(){
			$(this).closest('.form-group').removeClass('has-error');
		});

		$(document).on('keydown', '.add_rule', function(e){
			return isNumeric(e);
		});

        $(".add_item").click(function(e){
            $.ajax({
                url:BaseUrl + page_class + "/get_add_item_row",
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows").append("<tr id='target_new_row'><td colspan='4'>Please Wait</td></tr>");
                },
                success:function(html){
                    var seq = $('.added_row').length;
					var r = html.replace('id_barang[]', 'id_barang['+seq+']');
						r = html.replace('item_quantity[]', 'item_quantity['+seq+']');

                    $("#target_new_row").replaceWith(r);
                    $(".selectpicker").selectpicker("refresh");
                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();

						var j = 0;
						$('.added_row').each(function(){
							$(this).find('select[name="id_barang"]').attr('name', 'id_barang['+j+']');
							$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

							j++;
						});
                    });

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

                    $('#data-table-form select.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                    $('#data-table-form input.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                }
            });
        });

		$(document).on('change', 'select.item-outbound', function(){

			var ctrl = $(this);

			if(ctrl.val() > 0){

				$.ajax({
					url: BaseUrl + page_class + '/getUomByItem',
					type: 'POST',
					dataType: 'JSON',
					data: {
						id_barang: $(this).val()
					},
					success: function(response){

						var row ='';

						for (var i = 0; i < response.length; i++) {
							row += '<option value="'+response[i].unit_id+'">'+response[i].unit_code+'</option>';
						}

						ctrl.parent().parent().find('select[name="unit[]"]').html(row);
						$('select[name="unit[]"]').select2();
						// ctrl.parent().parent().find('label.uom').html(response.nama_satuan);
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});

			}
		});

    }

    var handleFormSettings = function() {

        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {

				var chk = true;
				var itemId = [];

				if($('#added_rows tr').length <= 0){
					toastr['error']("Please add item first", "ERROR");
					return false;
				}

				$('#data-table-form select.item-outbound').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					itemId.push(val);

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					if(val == 'undefined' || val == '') {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}
				});

				$('#inbound-barang input.add_rule').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					var id = $('input[name="id"]').val();
					if(id.length > 0){
						if(val <= 0) {
							ctrl.closest('.form-group').addClass('has-error');
							chk = false;
						}
					}
				});

				if(!chk){
					toastr['error']("Please check again.","Form Error");
					return false;
				}

				var hasDuplicate = !itemId.every(function(v,i) {
					return itemId.indexOf(v) == i;
				});

				if(hasDuplicate){

					$('#data-table-form select.item-outbound').each(function(index1, item1){

						$.each($('#data-table-form select.item-outbound').not(this), function (index2, item2) {

							if ($(item1).val() == $(item2).val()) {
								$(item1).closest('.form-group').addClass('has-error');
							}

						});

					});

					toastr['error']("Duplicate item. Please fix it first","Form Error");
					return false;
				}

				App.blockUI();

				var disabled = $(form).find(':input:disabled').removeAttr('disabled');
				var serialized = $(form).serialize();
				disabled.attr('disabled','disabled');

				$.ajax({
					type:'POST',
					url: BaseUrl + page_class + '/proses',
					dataType:'json',
					data: serialized,
					beforeSend:function(){
						$('.form-group').removeClass('has-error');
					},
					success:function(json){

						if(json.status){

							toastr['success'](json.message,"Success");

							setTimeout(function(){
								location.href = BaseUrl + page_class;
							}, 500);

						}else{

							toastr['error'](json.message,"Error Occured");

						}

						App.unblockUI();
					},
					error: function(response){
						App.unblockUI();
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});

            }
        });
    }

	return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            handleFormSettings();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

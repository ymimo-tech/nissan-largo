var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable;

	var initialize = function(){
		initTable();
	}

	var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getDetailItem", // ajax source
					"data": {
						bin_transfer_id: $('#data-table-form input[name="id"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
                    { "orderable": true }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1,2,3,4]
					},
                    {
                        "render": function(data, type, row){
                            return '<div align="center"><button class="btn green btn-xs" id="show-detail" type="button">\
                                        Show Detail\
                                    </button></div>';
                        },
                        "targets": [5]
                    }
				]
            }
        });
    }

    var childTableTpl = function(data){
        var tpl = '<div style="width: 90%; margin: 0 auto; text-align: left;">\
                        <h4>Location List</h4>\
                   </div>\
                   <table id="child-table-'+data[0]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
                        <thead>\
                            <tr>\
                                <th><div align="center">No.</div></th>\
                                <th><div align="center">LPN</div></th>\
                                <th><div align="center">GID</div></th>\
                                <th><div align="center">Quantity</div></th>\
                                <th><div align="center">From Location</div></th>\
                                <th><div align="center">To Location</div></th>\
                                <th><div align="center">Time</div></th>\
                                <th><div align="center">Pick By</div></th>\
                            </tr>\
                        </thead>\
                        <tbody>\
                        </tbody>\
                   </table>';

        return tpl;
    }

    var initChildTable = function(data){
        childTable = new Datatable();

        childTable.init({
            src: $("#child-table-"+data[0]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
                "bDestroy": true,
                "processing": true,
                "serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "language": {
                    "infoEmpty": ""
                },
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "initComplete": function(settings, json) {
                    /*
                    var dt = this;
                    setTimeout(function(){
                        dt.fnPageChange( 'first' );
                    }, 300);
                    */
                },
                "ajax": {
                    "url": BaseUrl + page_class + "/getScannedList", // ajax source
                    "data": {
                        item_code: data[1],
                        bin_transfer_id: $('#data-table-form input[name="id"]').val()
                    },
                    error: function(response){
                        if(response.status == 401)
                            location.href = BaseUrl + 'login';
                    }
                },
                "columns": [
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true }
                ],
                "order": [
                    [1, "ASC"]
                ],
                "columnDefs": [
                    {
                        "render": function(data, type, row){
                            return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
                        },
                        "targets": [0,1,2,3,4,5,6,7]
                    }
                ]
            }
        });
    }

    var pageInteraction = function(){

        $('#table-list').on('click', '#show-detail', function(e){
            e.preventDefault();

            var tr = $(this).closest('tr');
            var row = dataTable.getDataTable().row( tr );
            console.log(tr.attr('class'));

            if(tr.hasClass('shown')){
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');

                childTable.getDataTable().destroy();
            }else{
                $('#table-list tbody tr').each(function(){

                    var type = $(this).attr('role');
                    var r = dataTable.getDataTable().row( $(this) );
                    r.child.hide();
                    $(this).removeClass('shown');

                });

                // Open this row
                row.child( childTableTpl(row.data()) ).show();
                tr.addClass('shown');

                initChildTable(row.data());
            }

        });

    }

    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dataTable;

	var initialize = function(){

		$("#kd-outbound")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'outbound/getOutboundCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_outbound,
							text: item.kd_outbound
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#periode, #doc-type, #doc-status').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		setTimeout(function(){
            $('#periode').val(current_filter).trigger('change');
			initTable();
		}, 500);
	}

	var reset = function(){
		$("#do").val(null).trigger("change");
		$('#customer').val(null).trigger('change');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_year');
		$('#doc-type').select2('val', '');
	}

    var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
                current_filter=response.current_filter;
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getList", // ajax source
					"data": {
						do_number: $('#kd-outbound').val(),
						id_customer: $('#customer').val(),
						from: $('#from').val(),
						to: $('#to').val(),
						doc_type: $('#doc-type').val(),
						doc_status: $('#doc-status').val(),
                        periode:$('#periode').val(),
                        warehouse: $('#warehouse-filter').val(),
                        priority: $('#priority').val(),
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
        },
        "columns": [
	          { "orderable": false, "visible": false },
            { "orderable": false },
            { "orderable": false },
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
	          { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": false }
          ],
        "order": [
            [0, "desc"]
        ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4,5,6,8,9,10,11,12]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl+'inbound/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="kd_inbound"]').val(json.kd_inbound);
					$('#data-table-form input[name="tanggal_inbound"]').val(json.tanggal_inbound);
					$('#data-table-form select[name="id_supplier"]').val(json.id_supplier).selectpicker("refresh");
					$('#data-table-form select[name="id_inbound_document"]').val(json.id_inbound_document).selectpicker("refresh");
					$('#data-table-form #added_rows').append(json.inbound_barang_html);
					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});
					$(".remove_row").click(function(){
						$(this).closest("tr").remove();
					});
				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var closeOutbound = function(id){
		App.blockUI();
		$.ajax({
			url: BaseUrl + page_class + '/closeOutbound',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_outbound: id
			},
			success: function(response){
				if(response.status == 'OK'){

					dataTable.getDataTable().ajax.reload();
					toastr['success'](response.message, "SUCCESS");

				}else{
					toastr['error'](response.message, "Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    var pageInteraction = function(){

    	var totalSelected = 0;
    	var totalWeigth = 0;
    	var totalVolume = 0;

    	$(document).on('click','.group-checkable', function(e){

    		if($(this).is(':checked')){

		    	totalSelected = 0;
		    	totalWeigth = 0;
		    	totalVolume = 0;

	            $('#datatable_ajax tbody input[type="checkbox"]').each(function(){
		    		totalSelected = parseFloat(totalSelected + 1);
		    		totalWeigth = parseFloat(totalWeigth + parseInt($(this).attr('data-weight')));
		    		totalVolume = parseFloat(totalVolume + parseInt($(this).attr('data-volume')));
	            })

	        }else{

	            $('#datatable_ajax tbody input[type="checkbox"]').each(function(){
		    		totalSelected = parseFloat(totalSelected - 1);
		    		totalWeigth = parseFloat(totalWeigth - parseInt($(this).attr('data-weight')));
		    		totalVolume = parseFloat(totalVolume - parseInt($(this).attr('data-volume')));
	            })

	        }

    		$('#total-selected').html(totalSelected);
    		$('#total-weight').html(totalWeigth);
    		$('#total-volume').html(totalVolume);

    	});

    	$(document).on('click','input[type="checkbox"]', function(e){

    		if($(this).attr('data-weight')){
	    		if($(this).is(':checked')){
		    		totalSelected = parseFloat(totalSelected + 1);
		    		totalWeigth = parseFloat(totalWeigth + parseInt($(this).attr('data-weight')));
		    		totalVolume = parseFloat(totalVolume + parseInt($(this).attr('data-volume')));
		    	}else{
		    		totalSelected = parseFloat(totalSelected - 1);
		    		totalWeigth = parseFloat(totalWeigth - parseInt($(this).attr('data-weight')));
		    		totalVolume = parseFloat(totalVolume - parseInt($(this).attr('data-volume')));
		    	}

	    		$('#total-selected').html(totalSelected);
	    		$('#total-weight').html(totalWeigth);
	    		$('#total-volume').html(totalVolume);
	    	}
    	})

    	$('#warehouse-filter').change(function(){
    		initTable();
    	});

		$('#export-so').on('click', function(){

			var total = $('.total').html();
				total = total.replace(' ', '');
				total = total.replace('PCS', '');
				total = total.replace(',', '');

			post(BaseUrl + page_class + '/export_so',{
				outbound: $('#kd-outbound').val(),
				destination: $('#customer').val(),
				from: $('#from').val(),
				to: $('#to').val(),
				doc_type: $('#doc-type').val(),
				status: $('#doc-status').val(),
				periode:$('#periode').val(),
				warehouse: $('#warehouse-filter').val()
			});
			
		});

		$("#customer")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'outbound/getCustomerBySearch',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_customer,
							text: item.customer_name
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

		$(document).on('click','.data-table-approve',function(e){

            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			$.ajax({
	            type:'POST',
	            url:BaseUrl + page_class + '/approval',
	            data:{'id_outbound':id, 'status':'approve'},
	            dataType:'json',
	            success:function(json){

		            if(json.response){

						toastr['success'](json.message,"Success");
						dataTable.getDataTable().ajax.reload();

		            }else{

						toastr['error'](json.message, "Error");

		            }

	            },
	            error: function(json){

	            }
			})

		});

		$(document).on('click','.data-table-disapprove',function(e){

            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			$.ajax({
	            type:'POST',
	            url:BaseUrl + page_class + '/approval',
	            data:{'id_outbound':id, 'status':'disapprove'},
	            dataType:'json',
	            success:function(json){

		            if(json.response){

						toastr['success'](json.message,"Success");
						dataTable.getDataTable().ajax.reload();

		            }else{

						toastr['error'](json.message, "Error");

		            }

					App.unblockUI();
	            },
	            error: function(json){

	            }
			})

		});

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            location.href = BaseUrl + page_class + "/edit/" + id;
        });

		$(document).on('click','.data-table-close',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            closeOutbound(id);
        });

        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();

			if($(this).hasClass('disabled-link'))
				return false;

            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want remove this data ?", function(result) {
                if(result){
					App.blockUI();
                    $.ajax({
                        type:'POST',
                        url:BaseUrl + page_class + '/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){

								if(json.status == 'OK'){
									toastr['success'](json.message,"Success");
									dataTable.getDataTable().ajax.reload();
								}else{
									toastr['error'](json.message, "Error");
								}

                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You don't have autority to delete this data","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
							App.unblockUI();
                        },
						error: function(response){
							App.unblockUI();
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
                    });
                }
            });
        });
        $(document).on('click','.btn-sync',function(){

            var bsync = $(this);
            bsync.attr('disabled',true);
            bsync.prop('disabled',true);
            bsync.html('<i class="fa fa-refresh"></i> Please wait');

            $.ajax({
    			url: BaseUrl + 'dashboard/manual_sync',
    			type: 'POST',
    			dataType: 'JSON',
    			success: function(json){
                    if(json.newc.outbound.length>0){
                        $.each(json.newc.outbound, function (i,newc){
                            if(newc.newrec){
                                var title="New Outbound";
                            }else{
                                var title="Outbound Updated";
                            }
                            toastr['info']("<a href='"+BaseUrl+"outbound/detail/"+newc.id+"' target='_blank'>"+newc.nomor+"</a>",title);
                        });
                      dataTable.getDataTable().ajax.reload();
                    }

                    bsync.attr('disabled',false);
                    bsync.prop('disabled',false);
                    bsync.html('<i class="fa fa-refresh"></i> SYNC');

    			}
    		});
        });
        $(document).on('click','.btn-create-picking', function(){

			if(!$('#datatable_ajax tbody input').is(':checked')){
				toastr['error']('Please check at least one row to print', 'ERROR');
				return false;
			}

            var ids=[];
            var temp_diff='';
            var html='';
            var diff=0;
            var status=0;
            $('#datatable_ajax tbody input[type="checkbox"]:checked').each(function(){
                ids.push($(this).val());
                console.log(ids);
                var item=$(this).attr('data-count');
                var st=$(this).attr('data-status');
                var kode=$(this).attr('data-kode');
                html+='- '+kode+'<br/>';
                console.log(item);
                if(temp_diff!=''){
                    if(temp_diff!=item){
                        diff=1;
                    }
                }else{
                    temp_diff=item;
                }
                console.log(temp_diff);
                if(st!='Open'){
                    status=1;
                }
            });

/*            if(diff){
                toastr['error']('Please select only single outbound or multi outbound', 'ERROR');
                return false;
            }

            if(status){
                toastr['error']('Please select only open outbound', 'ERROR');
                return false;
            }

            var ol=$('#datatable_ajax tbody input[type="checkbox"]:checked').length;
            if(ol>5 && temp_diff=='multi'){
                toastr['error']('Max 5 outbound for multi outbound', 'ERROR');
                return false;
            }

            if(ol>10 && temp_diff=='single'){
                toastr['error']('Max 10 outbound for multi outbound', 'ERROR');
                return false;
            }
*/
            $('.ids_outbound').val(ids);
            $('.list_outbound').html(html);

            $('#modal-multi-picking').modal({
				backdrop: 'static',
				keyboard: true
			});
		});

        $(document).on('click','#submit-multi-picking', function(e){
            e.preventDefault();
            $.ajax({
                url: BaseUrl + page_class + '/create_multi_picking',
    			type: 'POST',
    			dataType: 'JSON',
                data:{'ids':$('.ids_outbound').val(), 'priority':$('.priority').val()},
    			success: function(json){
                    if(json.success==1){
                        toastr['success']("Picking Created","Success");
						$('#modal-multi-picking').modal('hide');
						dataTable.getDataTable().ajax.reload();
                    }else if(json.success==2){
						toastr['error']("One or more outbound already have picking","Error");
                        $('#modal-multi-picking').modal('hide');
					}else{
                        toastr['error']("Picking create failed","Error");
                        $('#modal-multi-picking').modal('hide');
                    }
    			}
            })
        });
    }

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_inbound: {
                    required: true
                },
                tanggal_inbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_inbound_document: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'inbound/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            dataTable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    var longpolling = function(){
        function start_longpolling(){
            var evtSource = new EventSource(BaseUrl+'outbound/longpolling');
    		evtSource.addEventListener("sync", function(e) {
    	      var obj = JSON.parse(e.data);
              if(obj.newc.length>0){
        	      $.each(obj.newc, function (i,newc){
         			toastr['info']("<a href='"+BaseUrl+"outbound/detail/"+newc.id+"' target='_blank'>"+newc.nomor+"</a>","New Outbond");
        	      });
                  //content_datatable.getDataTable().ajax.reload();
                  evtSource.close();
                  start_longpolling();
              }
    	    }, false);
        }

        start_longpolling();
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            //longpolling();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

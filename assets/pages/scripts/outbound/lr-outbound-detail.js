var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable;

	var initialize = function(){
		initTable();
		initTablePacking();
		initTablePicking();
		initTableShipping();
	}

	var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getDetailItem", // ajax source
					"data": {
						id_outbound: $('#data-table-form input[name="id"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
          					{ "orderable": true, "visible": false },
          					{ "orderable": false },
          					{ "orderable": true },
                    { "orderable": true },
					          { "orderable": true },
                    { "orderable": false, "visible": false },
					          { "orderable": true }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [2, 5]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [4, 6]
					}
				]
            }
        });
    }
	
	var initTablePacking = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list-pc"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + "outbound/getPackingList", // ajax source
					"data": {
						id_outbound: $('#data-table-form input[name="id"]').val(),
						id_shipping: $('#loading').val(),
						outbound: $('#outbound').val(),
						pccode: $('#pccode').val(),
						from: $('#from').val(),
						to: $('#to').val(),
                        periode:$('#periode').val(),
                        warehouse:$('#warehouse-filter').val()
					},
					"timeout":500000,
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
					{ "orderable": false },
					{ "orderable": true, "visible": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": false },
					// { "orderable": false },
					// { "orderable": true }
                  ],
                "order": [
                    [5, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						// "targets": [1,2,3,4,5,6,7,8,9,10]
						"targets": [1,2,3,4,5,6,7,8]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

	var initTablePicking = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list-p"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
        				"bDestroy": true,
        				"processing": true,
        				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				        "initComplete": function(settings, json) {
				        },
                "ajax": {
                    "url": BaseUrl + page_class + "/getPickingList", // ajax source
      					"data": {
      						id_outbound: $('#data-table-form input[name="id"]').val(),
      						params: 'no_action'
      					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
          { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true }
        ],
                "order": [
                    [0, "DESC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1,2,3,4,5,6,7,8,9,10]
					}
				]
            }
        });
    }

	var initTableShipping = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list-s"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getShippingList", // ajax source
					"data": {
						id_outbound: $('#data-table-form input[name="id"]').val(),
						params: 'no_action'
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}

                },
                "columns": [
					{ "orderable": false, "visible": false },
					{ "orderable": true },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false }
                ],
                "order": [
                    [0, "DESC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1,2,3,4,5,6,7,8]
					}
				]
            }
        });
    }

    var pageInteraction = function(){

        $(document).on('click','#button-add', function(e){
            e.preventDefault();
            $.ajax({
                url: BaseUrl + 'Api_integration/autoCreatePL/'+$(this).attr('data-id'),
                type: 'GET',
                dataType: 'JSON',
                // data:{'ids':$(this).attr('data-id')},
                success: function(json){
                    initTablePicking();
                    // if(json.success==1){
                    //     toastr['success']("Picking Created","Success");
                    //     $('#modal-multi-picking').modal('hide');
                    //     dataTable.getDataTable().ajax.reload();
                    // }else if(json.success==2){

                    //     toastr['error']("One or more outbound already have picking","Error");
                    //     $('#modal-multi-picking').modal('hide');

                    // }else{

                    //     toastr['error']("Picking create failed","Error");
                    //     $('#modal-multi-picking').modal('hide');

                    // }
                }
            })
            initTablePicking();
        });

    }

    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

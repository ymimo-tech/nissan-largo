var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var content_datatable,
		idDestination,idSource, idWarehouse, idDocument;

	var idOutbound = null;

	var initialize = function(){

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: moment().format('DD/MM/YYYY')
		});

		var id = $('input[name="id"]').val();
		if(id.length > 0)
			edit(id);

		$('#data-table-form select[name="id_supplier"], #data-table-form select[name="id_outbound_document"]').select2();

		if($('#data-table-form select[name="id_inbound_status"]') != 'undefined'){
			$('#data-table-form select[name="id_inbound_status"]').select2();
		}

		getOutboundDocType();
	}

	var reset = function(){

		$('#data-table-form input[name="id"]').val('');
		$('#data-table-form input[name="params"]').val('');
		$('#data-table-form input[name="kd_outbound"]').val('');
		$('#data-table-form input[name="tanggal_outbound"]').val(moment().format('DD/MM/YYYY'));
		$('#data-table-form input[name="delivery_date"]').val(moment().format('DD/MM/YYYY'));
		$('#data-table-form select[name="destination"]').val(null).trigger("change");
		$('#data-table-form select[name="id_outbound_document"]').val(null).trigger('change');
    $('.outbound-code').text(' -- ');
		$('.outbound-code').val('-');
		$('input[name="name"]').val('');
		$('input[name="DestinationAddressL1"]').val('');
		$('input[name="DestinationAddressL2"]').val('');
		$('input[name="DestinationAddressL3"]').val('');
		$('input[name="Phone"]').val('');
		$('input[name="shipping_group"]').val('');
		$('input[name="ShippingNote"]').val('');
		$('#addres').val('');
		$('#shipping-note').val('');
		$('#data-table-form #added_rows').html('');

		$('#data-table-form input[name="kd_outbound"]').focus();

	}

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){

                    idOutbound = json.id_outbound;
					idWarehouse = json.warehouse_id;
					idDestination = json.id_destination;
					idDocument = json.id_outbound_document;

					$('.outbound-code').html(json.kd_outbound);
					$('#data-table-form input[name="kd_outbound"]').val(json.kd_outbound);
                    $('#data-table-form input[name="DestinationName"]').val(json.DestinationName);
                    $('#data-table-form input[name="name"]').val(json.DestinationName);
                    $('#data-table-form input[name="DestinationAddressL1"]').val(json.DestinationAddressL1);
                    $('#data-table-form input[name="DestinationAddressL2"]').val(json.DestinationAddressL2);
                    $('#data-table-form input[name="DestinationAddressL3"]').val(json.DestinationAddressL3);
                    $('#data-table-form input[name="Phone"]').val(json.Phone);
                    $('#data-table-form textarea[name="ShippingNote"]').val(json.ShippingNote);
                    $('#data-table-form textarea[name="address"]').val(json.address);
                    $('#data-table-form input[name="shipping_group"]').val(json.shipping_group);

					$('#cost-center').val(json.cost_center_id).trigger("change");

					$('#data-table-form select[name="id_outbound_document"]').val(json.id_outbound_document).trigger("change");
					$('#data-table-form select[name="id_outbound_document"]').attr('disabled', '');
					$('#data-table-form select[name="id_inbound_status"]').val(json.id_status_outbound).trigger("change");
					$('#data-table-form #added_rows').append(json.outbound_barang_html);
					$('#data-table-form select[name="priority"]').val(json.m_priority_id).trigger("change");

					$('#data-table-form input[name="tanggal_outbound"]').datepicker("update", json.tanggal_outbound);

					$('.delivery_date').datepicker('remove');

					$('.delivery_date').datepicker({
						orientation: "left",
						format: 'dd/mm/yyyy',
						autoclose: true,
						startDate: json.tanggal_outbound
					});

					$('.delivery_date').datepicker("update", json.delivery_date);

					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

					//$(".selectpicker").selectpicker("refresh");

					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});

					var j = 0;
					$('.added_row').each(function(){
						$(this).find('select[name="id_barang"]').attr('name', 'id_barang['+j+']');
						$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

						j++;
					});

					if($('#data-table-form input[name="not_used"]').val() == 'true'){

						$(".remove_row").click(function(){
							$(this).closest("tr").remove();

							var j = 0;
							$('.added_row').each(function(){
								$(this).find('select[name="id_barang"]').attr('name', 'id_barang['+j+']');
								$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

								j++;
							});
						});

					}else{

						$('select[name="id_barang[]"]').attr('disabled', '');
						$('input[name="item_quantity[]"]').attr('disabled', '');
						$('.btn-delete').hide();
						$('.add_item').parent('tr').hide();

					}

				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getOutbounDocument = function(id, id_outbound){
		if(id != ""){
			$.ajax({
				url: BaseUrl + page_class + '/getOutDocument',
				type: 'POST',
				dataType: 'JSON',
				data: {
					id:id,
          id_outbound: id_outbound
				},
				success: function(response){
					if(response.status){
						$('input[name="kd_outbound"]').val(response.result);
						$('.outbound-code').text(response.result);
					}else{
						toastr['error'](response.message,"Error");
					}
					App.unblockUI();
				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
		}else{
			$('input[name="kd_outbound"]').val('');
			$('.outbound-code').text(' -- ');
		}
	}

	var getCustomer = function(){
		$.ajax({
			url: BaseUrl + 'inbound/getCustomer',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_customer);
						row = row.replace(/{{ text }}/gi, data[i].customer_name);
					}

					$('#destination').select2('destroy');

					$('#destination').html(row);
					$('#destination').select2();

					$('#destination').val(idDestination).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getDestination = function(){

        $('#destination')
          .select2({
            multiple: false,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            minimumInputLength: 1,
            ajax: {
                url: BaseUrl + page_class +'/select2Options/destinations/destination_id/destination_name',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (res) {
                            return {
                                text: res.text,
                                id: res.id
                            }
                        })
                    };
                }
            }
        });

        // $.ajax({
		// 	url: BaseUrl + page_class + '/getDestination',
		// 	type: 'POST',
		// 	dataType: 'JSON',
		// 	data: {
		// 		document_id:$('#data-table-form select[name="id_outbound_document"]').val()
		// 	},
		// 	success: function(response){
		// 		if(response.status == 'OK'){

		// 			var data = response.data,
		// 				len = data.length,
		// 				tpl = '<option value="{{ id }}">{{ text }}</option>',
		// 				row = '';

		// 			for(var i = 0; i < len; i++){

		// 				row += tpl;
		// 				row = row.replace(/{{ id }}/gi, data[i].id);
		// 				row = row.replace(/{{ text }}/gi, data[i].text);

		// 			}

		// 			$('#destination').select2('destroy');

		// 			$('#destination').html(row);
		// 			$('#destination').select2();

		// 			$('#destination').val(idDestination).trigger('change');

		// 		}else{
		// 			toastr['error']("Get destination list failed","Error");
		// 		}
		// 		App.unblockUI();
		// 	},
		// 	error: function(response){
		// 		App.unblockUI();
		// 		if(response.status == 401)
		// 			location.href = BaseUrl + 'login';
		// 		else
		// 			bootbox.alert(response.responseText);
		// 	}
		// });
	}

	var getDestinationDetail = function(){


		$.ajax({
			url: BaseUrl + page_class + '/getDestinationDetail',
			type: 'POST',
			dataType: 'JSON',
			data: {
				document_id:$('#data-table-form select[name="id_outbound_document"]').val(),
				destination_id:$('#data-table-form select[name="destination"]').val()
			},
			success: function(response){
				console.log(response);
				if(response.status == 'OK'){

					if(response.data.name != ""){
						$('#data-table-form input[name="name"]').val(response.data.name);
					}

					if(response.data.address != ""){
    	                $('#data-table-form textarea[name="address"]').val(response.data.address);
	                }

					if(response.data.phone != ""){
						$('#data-table-form input[name="Phone"]').val(response.data.phone);
					}


				}else{
					toastr['error']("Get destination list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

	var getBatch = function(ctrl){
		$.ajax({
			url: BaseUrl + 'outbound/getBatch',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_barang: ctrl.val()
			},
			success: function(response){

				if(response.is_batch == 'YES'){

					//var html = '<input type="text" name="batchcode['+ctrl.val()+']" value="" class="col-md-12 form-control add_rule" style="width: 100px; float: none; margin: auto; display: table;">';

					var html = '<select name="batchcode['+ctrl.val()+']" class="form-control mbatch add_rule" style="width: 100%;">\
									{{ opt }}\
								</select>',
						opt = '';

					var data = response.data,
						len = data.length;

					for(var i = 0; i < len; i++){
						opt += '<option value="'+data[i].kd_batch+'">'+data[i].kd_batch+'</option>';
					}

					ctrl.parent().parent().find('.batch-column').html(html.replace(/{{ opt }}/gi, opt));

					setTimeout(function(){
						$('.mbatch').select2();
					}, 200);

				}else{

					var html = '-'
					ctrl.parent().parent().find('.batch-column').html(html);

				}
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getOutboundDocType = function(){

		$.ajax({
			url : BaseUrl + page_class + '/getOutboundDocument',
			type: 'POST',
			dataType: 'JSON',
			data:{
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				if(response.response){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}" auto-generate="{{ type }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].document_id);
						row = row.replace(/{{ text }}/gi, data[i].document_name);
						row = row.replace(/{{ type }}/gi, data[i].is_auto_generate);
					}

					$('select[name="id_outbound_document"]').html(row);
					$('select[name="id_outbound_document"]').select2();

					$('select[name="id_outbound_document"]').val(idDocument).trigger('change');

				}else{
					toastr['error'](response.message,"Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

	var getWarehouseFrom = function(){

		var warehouseId = $('#warehouse-filter').val();
		var documentId = $('select[name="id_outbound_document"]').val();

		$.ajax({
			url : BaseUrl + 'warehouse/getWarehouseByDocument/',
			type: 'POST',
			dataType: 'JSON',
			data:{
				document_id	:documentId,
				warehouse_id:warehouseId
			},
			success : function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id);
						row = row.replace(/{{ text }}/gi, data[i].name);
					}

					$('#warehouse-form').html(row);
					$('#warehouse-form').select2();

					$('#warehouse-form').val(idWarehouse).trigger('change');

				}else{
					toastr['error'](response.message,"Error");
				}
			},
			error : function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		})
	}

	var getWarehouseTo = function(){

		var warehouseId = $('#warehouse-form').val();
		var documentId = $('select[name="id_outbound_document"]').val();

		$.ajax({
			url : BaseUrl + 'warehouse/getWarehouseByDocument/',
			type: 'POST',
			dataType: 'JSON',
			data:{
				document_id	:documentId,
				warehouse_id_2:warehouseId
			},
			success : function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id);
						row = row.replace(/{{ text }}/gi, data[i].name);
					}

					$('#destination').html(row);
					$('#destination').select2();

					$('#destination').val(idDestination).trigger('change');

				}else{
					toastr['error'](response.message,"Error");
				}
			},
			error : function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		})
	}

/*
	var getDestination = function(){

		var docId = $('select[name="id_outbound_document"]').val();

		$.ajax({
			url: BaseUrl + 'destination/getDestination',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id: docId
			},
			success: function(response){

				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].idGudang);
						row = row.replace(/{{ text }}/gi, data[i].namaGudang);
					}

					$('#destination').html(row);
					$('#destination').select2();

					$('#destination').val(idDestination).trigger('change');

				}else{
					$('#destination').html('');
					toastr['error'](response.message,"Error");
				}

				App.unblockUI();

			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}
*/

    var pageInteraction = function(){

    	$('select[name="priority"]').select2();
    	$("#destination").change(function(){
    		if($(this).val()){
    			getDestinationDetail();
	    	}
    	});

    	$('#tanggal-outbound').change(function(){

			var outboundDateStr = $("#tanggal-outbound").val();
			var outboundDateArr = outboundDateStr.split("/");
			var outboundDateStrNew = outboundDateArr[1]+"/"+outboundDateArr[0]+"/"+outboundDateArr[2];
			var outboundDate = new Date(outboundDateStrNew);

			var deliveryDateStr = $("#delivery_date").val();
			var deliveryDateArr = deliveryDateStr.split("/");
			var deliveryDateStrNew = deliveryDateArr[1]+"/"+deliveryDateArr[0]+"/"+deliveryDateArr[2];
			var deliveryDate = new Date(deliveryDateStrNew);

			$('.delivery_date').datepicker('remove');

			$('.delivery_date').datepicker({
				orientation: "left",
				format: 'dd/mm/yyyy',
				autoclose: true,
				startDate: outboundDateStr
			});

			if (outboundDate > deliveryDate) {

				$('.delivery_date').datepicker("update", outboundDateStr);

			}else{

				$('.delivery_date').datepicker("update", deliveryDateStr);

			}

    	});

		$('.outbound-code').text(' -- ');

		$('#warehouse-form, #destination, #cost-center').select2();
		// $('#cost-center-box').show();

		$('#warehouse-filter').change(function(){
			getOutboundDocType();
		});

		$('select[name="id_outbound_document"]').change(function(){

			var id = $(this).val();
      var id_outbound = $('#data-table-form input[name="id"]').val();


			$('#warehouse-form').val(null).trigger('change');
			// $('#destination').val(null).trigger('change');
			if(id){

				if($('option:selected', this).attr('auto-generate') == 0){

					if(idOutbound == null){
						$('input[name="kd_outbound"]').val('');
					}

					$('input[name="kd_outbound"]').attr('type','text');
					$('.outbound-code').hide();

				}else{

					$('input[name="kd_outbound"]').attr('type','hidden');
					$('.outbound-code').show();
					// disableForm(false);
					getOutbounDocument(id, id_outbound);

				}

				$('#warehouse-form').attr('data-placeholder','-- Please Select --');
				getWarehouseFrom();

				$('#destination').attr('data-placeholder','-- Please Select --');
				getDestination();

			}

		});

		$('#warehouse-form').change(function(){
			var id = $('select[name="id_outbound_document"]').val();

			if(id == 3 || id == 14){
				$('#destination').val(null).trigger('change');
				getWarehouseTo();
			}

		})

		$(document).on('change', 'select.item-outbound', function(){

			var ctrl = $(this);

			if(ctrl.val() > 0){

				$.ajax({
					url: BaseUrl + page_class + '/getUomByItem',
					type: 'POST',
					dataType: 'JSON',
					data: {
						id_barang: $(this).val()
					},
					success: function(response){

						var row ='';

						for (var i = 0; i < response.length; i++) {
							row += '<option value="'+response[i].unit_id+'">'+response[i].unit_code+'</option>';
						}

						ctrl.parent().parent().find('select[name="unit[]"]').html(row);
						$('select[name="unit[]"]').select2();
						// ctrl.parent().parent().find('label.uom').html(response.nama_satuan);
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});

				getBatch(ctrl);
			}
		});

		$(document).on('change', '#data-table-form select.item-outbound', function(){
			$(this).closest('.form-group').removeClass('has-error');
		});

		$(document).on('keydown', '.add_rule', function(e){
			return isNumeric(e);
		});

		$('#save-new').on('click', function(){
			$('input[name="params"]').val('new');
			$('#data-table-form').submit();
		});

		/*
        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
		*/

        $(".add_item").click(function(e){
			var seq = $('.added_row').length;
            $.ajax({
                url:BaseUrl + page_class + "/get_add_item_row/" + seq,
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows").append("<tr id='target_new_row'><td colspan='4'>Please Wait</td></tr>");
                },
                success:function(html){
                    var seq = $('.added_row').length;
					var r = html.replace('id_barang[]', 'id_barang['+seq+']');
						r = html.replace('item_quantity[]', 'item_quantity['+seq+']');

                    $("#target_new_row").replaceWith(r);
                    $(".selectpicker").selectpicker("refresh");
                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();

						var j = 0;
						$('.added_row').each(function(){
							$(this).find('select[name="id_barang"]').attr('name', 'id_barang['+j+']');
							$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

							j++;
						});
                    });

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

                    $('#data-table-form select.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                    $('#data-table-form input.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                }
            });
        });
    }

	var newOutBoundCode = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getNewOutboundCode',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){
					$('#data-table-form input[name="kd_outbound"]').val(response.outbound_code);
					$('#data-table-form .outbound-code').html(response.outbound_code);
				}else{
					toastr['error']("Generate new outbound code failed. Please refresh page","Form Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

    var handleFormSettings = function() {

        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_outbound: {
                    required: true
                },
                tanggal_outbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_outbound_document: {
                    required: true
                },
                name: {
                    required: true
                },
/*                Phone: {
                    required: true
                },
*/                DestinationAddressL1: {
                    required: true
                },
                DestinationAddressL2: {
                    required: true
                },
                DestinationAddressL3: {
                    required: true
                },
                ShippingNote: {
                    required: true
                },
                id_outbound_document: {
                    required: true
                },
                destination: {
                    required: true
                },
                shipping_group: {
                  required:true
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
            	if(!$('#cost-center').prop('disabled')){

	            	if($('#cost-center').val() == ""){
	            		$('#cost-center').parent().addClass('has-error');
						toastr['error']("Cost Center is required.","Form Error");
	            		return false;
	            	}else{
	            		$('#cost-center').parent().removeClass('has-error');
	            	}
	            }


				var chk = true;
				var itemId = [];

				if($('#data-table-form input[name="tanggal_outbound"]').val().length != 10){
					$('.date-picker').addClass('has-error');
					chk = false;
				}

				if($('#added_rows tr').length <= 0){
					toastr['error']("Please add item first", "ERROR");
					return false;
				}

				$('#data-table-form select.item-outbound').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					itemId.push(val);

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					if(val == 'undefined' || val == '') {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}
				});

				$('#inbound-barang input.add_rule').each(function(){
					var ctrl = $(this),
						val = ctrl.val();
            console.log(val);

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					var id = $('input[name="id"]').val();
					if(id.length > 0){
						if(val <= 0) {
							ctrl.closest('.form-group').addClass('has-error');
							chk = false;
						}
					}
				});

				if(!chk){
					toastr['error']("Please check again.","Form Error");
					return false;
				}

				var hasDuplicate = !itemId.every(function(v,i) {
					return itemId.indexOf(v) == i;
				});

				if(hasDuplicate){

					$('#data-table-form select.item-outbound').each(function(index1, item1){

						$.each($('#data-table-form select.item-outbound').not(this), function (index2, item2) {

							if ($(item1).val() == $(item2).val()) {
								$(item1).closest('.form-group').addClass('has-error');
							}

						});

					});

					toastr['error']("Duplicate item. Please fix it first","Form Error");
					return false;
				}

				App.blockUI();

                $.ajax({
					url: BaseUrl + page_class + '/checkExistDoNumber',
					type: 'POST',
					dataType: 'JSON',
					data: {
						kd_outbound: $('#kd-outbound').val(),
						id_outbound: $('#data-table-form input[name="id"]').val()
					},
					success: function(response){
						if(response.status == 'OK'){

							save(form);

						}else{
							App.unblockUI();
							toastr['error'](response.message, 'ERROR');
						}
					},
					error: function(response){
						App.unblockUI();
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});
            }
        });
    }

	var submitUpdate = function(form){

		var disabled = $(form).find(':input:disabled').removeAttr('disabled');
		var serialized = $(form).serialize();
		disabled.attr('disabled','disabled');

		$.ajax({
			url: BaseUrl + page_class + '/proses',
			type: 'POST',
			dataType: 'JSON',
			data: serialized + '&delete=yes',
			success: function(response){
				if(response.success==1){
					toastr['success']("Save changes success.","Success");

					setTimeout(function(){
						location.href = BaseUrl + page_class;
					}, 200);
				}else{
					if(response.no_add){
						toastr['warning']("You dont have authority to add data.","Warning");
					}else if(response.no_edit){
						toastr['warning']("You dont have authority to change data..","Warning");
					}else{
						toastr['error']("Please refresh the page and try again later.","Error Occured");
					}
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

	var save = function(form){

		var disabled = $(form).find(':input:disabled').removeAttr('disabled');
		var serialized = $(form).serialize();
		disabled.attr('disabled','disabled');

		$.ajax({
			type:'POST',
			url: BaseUrl + page_class + '/proses',
			dataType:'json',
			data: serialized,
			beforeSend:function(){
				//$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
				//$('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
				$('.form-group').removeClass('has-error');
			},
			success:function(json){
				if(json.success==1){
					if(json.edit==1){
						toastr['success']("Save changes success.","Success");
					}else{
						toastr['success']("Save success.","Success");
					}

					var params = $('input[name="params"]').val();

					if(params == 'new'){
						reset();
						//newOutBoundCode();
						$('.detail-item-add').hide();
					}else{
						setTimeout(function(){
							location.href = BaseUrl + page_class;
						}, 500);
					}
					//content_datatable.getDataTable().ajax.reload();
					//$('#form-content').modal('hide');
				}else{
					if(json.no_add){
						toastr['warning']("You dont have authority to add data.","Warning");
					}else if(json.no_edit){
						toastr['warning']("You dont have authority to change data..","Warning");
					}else{

						if(json.status == 'ERR'){

							bootbox.confirm(json.message, function(result) {
								if(result){
									submitUpdate(form);
								}
							});

						}else{

							toastr['error']("Please refresh the page and try again later.","Error Occured");

						}
					}
				}
				App.unblockUI();
				//$('.modal-content').unblock();
				//$('.spinner').remove();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            handleFormSettings();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

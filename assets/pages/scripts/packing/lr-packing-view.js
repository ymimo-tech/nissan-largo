var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
	
	var picking_list = '';

	var initialize = function(){

	}

	var showModalPrint = function(id){

		if(id){

			App.blockUI();
			$.ajax({
				url: BaseUrl + page_class + '/getQtyPrint',
				type: 'POST',
				dataType: 'JSON',
				data: {
					id_receiving: id
				},
				success: function(response){

					var data = response;
					$('#data-modal-form input[name="id"]').val(id);
					$('.qty-print-receiving').html(data.qty);

					$('.qty-print-receiving').show();
					$('#qty-print').hide();
					$('.required').hide();

					$('#modal-print').modal({
						backdrop: 'static',
						keyboard: true
					});

					App.unblockUI();
				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});

		}else{

			$('#data-modal-form input[name="id"]').val('');

			$('#qty-print').val('');

			$('.qty-print-receiving').hide();
			$('#qty-print').show();
			$('.required').show();

			$('#modal-print').modal({
				backdrop: 'static',
				keyboard: true
			});

		}
	}

	var reset = function(){

	}
	
	var initTable = function(){
		rec_list = new Datatable();
		
		rec_list.init({
			src: $("#rec-list"),
			onSuccess: function (grid, response) {
				if(response.status) {
					$('#uom').text(response.uom);
					if(typeof response.destinations !== 'undefined'){
						$("#destination").html("<b>"+response.destinations.name+"</b><br/>"+response.destinations.address);
						$("#destination_code").val(response.destinations.code);
					}
				} else {
					console.log('message');
					console.log(response.message);
					if(typeof response.message !== 'undefined'){
						if(response.message != 'This  is not exist.'){
							toastr['error'](response.message,"Error");
						}
					}
					$("#destination").html("");
					$("#destination_code").val("");
				}
			},
			onError: function (grid) {

			},
			onDataLoad: function(grid) {
				// execute some code on ajax data load
			},

			loadingMessage: 'Loading...',
			dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
				"dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
				"bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"lengthMenu": [
					[10, 20, 50, 100, 150],
					[10, 20, 50, 100, 150] // change per page values here
				],
				"pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
				"ajax": {
					"url": BaseUrl + page_class + "/getRecList", // ajax source
					"data": function(data) { // add request parameters before submit
						data['packing_number'] = $('#pc_code').val();
						data['delivery_note'] = picking_list;
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
				},
				"columns": [
					{ "orderable": false, "visible": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
				  ],
				"order": [
					[0, "desc"]
				],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4,5,6]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
			}
		});
	}

    var pageInteraction = function(){
		$(document).on('click','.new_packing',function(e){
            e.preventDefault();

            $.ajax({
                type:'POST',
                url: BaseUrl + page_class + '/getPackingCode',
                data: {'add':1},
                dataType:'json',
                success:function(response){
					$('#pc_code').val(response.pc_code);
					$('#carton').val('');
					$('#picking_list').val('');
					$('#item_code').val('');
					$('#quantity').val('');
					$('#weight').val('0');
					$('#volume').val('0');
					
					toastr['success']('Successfully request new packing number (' + response.pc_code + ').',"Success");
                },
                error: function(err){
                    console.log(err);
                }
            });
        });
		
		$(document).on('click','#confirm',function(e){
            e.preventDefault();
			
			document.getElementById('confirm').setAttribute('disabled', 'disabled');

            $.ajax({
                type:'POST',
                url: BaseUrl + page_class + '/savePacking',
                data: {
					'packing_number' 	: $('#pc_code').val(),
					'carton' 			: $('#carton').val(),
					// 'picking_list' 		: $('#picking_list').val(),
					'delivery_note'		: $('#picking_list').val(),
					'item_code' 		: $('#item_code').val(),
					'destination_code'	: $('#destination_code').val(),
					'quantity' 			: $('#quantity').val(),
					'weight' 			: $('#weight').val(),
					'volume' 			: $('#volume').val(),
				},
                dataType:'json',
                success:function(response){
					if(response.status) {
						initTable();
						toastr['success'](response.message,"Success");
					} else {
						toastr['error'](response.message,"Error");
					}
					
					document.getElementById('confirm').removeAttribute('disabled')
                },
                error: function(err){
                    console.log(err);
                }
            });
        });
		
		$('#carton').on('keyup', function(e){
			if(e.keyCode === 13) {
				e.preventDefault();
				
				$.ajax({
					type:'POST',
					url: BaseUrl + page_class + '/isCarton',
					data: {
						'carton' : $('#carton').val()
					},
					dataType:'json',
					success:function(response){
						if(!response.status) {
							toastr['error']('This LPN '+$('#carton').val()+' is not carton.',"Error");
							$('#carton').val('');
						}
					},
					error: function(err){
						console.log(err);
					}
				});
			}
		});
		
		$('#data-table-edit').on('click', function(e) {
			console.log('asu asu asu masuk');
		})

		$('#pc_code').on('keyup', function(e){
			if(e.keyCode === 13) {
				e.preventDefault();
				
				$.ajax({
					type:'POST',
					url: BaseUrl + page_class + '/isPCLoaded',
					data: {
						'packing_number' : $('#pc_code').val()
					},
					dataType:'json',
					success:function(response){
						if(!response.status) {
							$('#pc_code').val('');
							toastr['error']('This packing number is already loaded.',"Error");
						}
					},
					error: function(err){
						console.log(err);
					}
				});
				
				initTable();
				
				$('#carton').val('');
				$('#picking_list').val('');
				$('#item_code').val('');
				$('#quantity').val('');
				$('#weight').val('0');
				$('#volume').val('0');
			}
		});
		
		$('#picking_list').on('keyup', function(e){
			if(e.keyCode === 13) {
				e.preventDefault();
				picking_list=$('#picking_list').val();
				$('#item_code').val('');
				$('#quantity').val('');
				
				initTable();
			}
		});
		
		
		
		$('#item_code').on('keyup', function(e){
			if(e.keyCode === 13) {
				e.preventDefault();
				$.ajax({
					type:'POST',
					// url: BaseUrl + page_class + '/getItemQtyByPicking',
					url: BaseUrl + page_class + '/getItemQtyByDN',
					data: {
						'item_code' 	: $('#item_code').val(),
						'delivery_note' : picking_list,
					},
					dataType:'json',
					success:function(response){
						if(response.status) {
							$('#quantity').val(response.qty_picked);
						}
					},
					error: function(err){
						console.log(err);
					}
				});
			}
		});
    }
	
	var getPackingCode = function(){
		if($('#pc_code').val() === ''){
			$.ajax({
				type:'POST',
				url:BaseUrl + page_class + '/getPackingCode',
				data:{'add':0},
				dataType:'json',
				success:function(json){
					$('#pc_code').val(json.pc_code);
					$('#weight').val('0');
					$('#volume').val('0');
				}
			});
		} else {
			$.ajax({
				type:'POST',
				url:BaseUrl + page_class + '/getPackingDetail',
				data:{'pc_code':$('#pc_code').val()},
				dataType:'json',
				success:function(json){
					$('#weight').val(json.weight);
					$('#volume').val(json.volume);
					$("#destination").html("<b>"+json.destination_name+"</b><br/>"+json.destination_address);
					$("#destination_code").val(json.destination_code);
				}
			});
		}
	}

    return {
        init: function () {
			initialize();
            pageInteraction();
            initTable();
			getPackingCode();
        }

    };
}();

jQuery(document).ready(function() {
    Handler.init();
});

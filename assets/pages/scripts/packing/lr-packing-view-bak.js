var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var picking_list,
        outbound_list,
        pick_rec_list,
        pl_id=0,
        id_outbound=0,
        inc,
        pc_code;

	var initialize = function(){

	}

	var reset = function(){

	}

    var initTable = function () {
		$('#weight').val(0);
		$('#volume').val(0);

        picking_list = new Datatable();

        picking_list.init({
            src: $("#picking-list"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
                "bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getList", // ajax source
                    "data":{
                        warehouse: $('#warehouse-filter').val()
                    },
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                  ],
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4,5]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });

        outbound_list = new Datatable();

        outbound_list.init({
            src: $("#outbound-list"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
                "bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getOutboundList", // ajax source
                    "data": function(data) { // add request parameters before submit
    					data['pl_id'] = pl_id;
    				},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                  ],
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });

        rec_list = new Datatable();

        rec_list.init({
            src: $("#rec-list"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
				$('.print_surat_jalan').removeClass('disabled-link');
				$('.print_surat_jalan').attr('href',BaseUrl + page_class + "/print_surat_jalan/"+response.enc);
				$('.print_label').removeClass('disabled-link');
				$('.print_pkb').removeClass('disabled-link');
				$('.print_pkb').attr('href',BaseUrl + page_class + "/print_perintah_keluar_barang/"+response.enc);
				$('#id_outbound').val(response.enc);
                if(response.scanned){
                    $('.print_surat_jalan').removeClass('disabled-link');
                    $('.print_surat_jalan').attr('href',BaseUrl + page_class + "/print_surat_jalan/"+response.enc);
                    $('.print_label').removeClass('disabled-link');
                    $('.print_pkb').removeClass('disabled-link');
                    $('.print_pkb').attr('href',BaseUrl + page_class + "/print_perintah_keluar_barang/"+response.enc);
                    $('#id_outbound').val(response.enc);
                }
                // $('.otb_packing_numbers').html(response.packing_numbers);
                $('#pc_code').html(response.packing_numbers);
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
                "bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getRecList", // ajax source
                    "data": function(data) { // add request parameters before submit
    					data['pl_id'] = pl_id;
                        data['id_outbound'] = id_outbound;
    				},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                  ],
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4,5,6]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

    var pageInteraction = function(){

        $('#warehouse-filter').change(function(){
            initTable();
        });

		$('#search_picking').on('submit', function(e){
			e.preventDefault();
            var pl_name=$('#pl_name').val();
            $.ajax({
                type:'POST',
                url:BaseUrl + page_class + '/get_picking_id',
                data:{'pl_name':pl_name},
                dataType:'json',
                success:function(json){
                    if(json.pl_id!='no_data'){
                        pl_id = json.pl_id;
                        $('#outbound-list').DataTable().ajax.reload();
                        $('.picking_list_name').html('<b>' + pl_name + '</b>');
                        $('.picking_list_container').hide();
                        $('.outbound_list_container').fadeIn();
                    }else{
                        toastr['error']('Picking not found',"Error");
                    }
                }
            });
		});

        $(document).on('click','#picking-list tbody tr',function(e){
            e.preventDefault();
            $('#picking-list tr').removeClass('tr_selected');
            $(this).addClass('tr_selected');
        });

        $(document).on('click','.open_picking',function(){
            if($('#picking-list .tr_selected').length>0){
                var pl_name = $('#picking-list .tr_selected').find('.pl_name').text();
                $('#pl_name').val(pl_name);
                $('#search_picking').submit();
            }else{
                toastr['error']('Please select or input kode picking',"Error");
            }
        });

        $(document).on('click','.open_outbound',function(){
            if($('#outbound-list .tr_selected').length>0){
                id_outbound = $('#outbound-list .tr_selected').find('.kd_outbound').attr('data-id');
                $("#rec-list").DataTable().ajax.reload();
                var kd_outbound = $('#outbound-list .tr_selected').find('.kd_outbound').text();
                var DestinationName = $('#outbound-list .tr_selected').find('.DestinationName').text();
                var DestinationAddressL1 = $('#outbound-list .tr_selected').find('.DestinationAddressL1').text();
                // $('.otb_kd').html('<b>' + kd_outbound + ', ' + $('#picking-list .tr_selected').find('.pl_name').text() + '</b>');
                $('.otb_kd').html('<b>' + kd_outbound + '</b>');
                $('.otb_name').html(DestinationName);
                $('.otb_alamat').html(DestinationAddressL1);
                $('.outbound_list_container').hide();
                $('.rec_list_container').fadeIn();
                $.ajax({
                    type:'POST',
                    url:BaseUrl + page_class + '/get_pc_code',
                    data:{'id_outbound':id_outbound},
                    dataType:'json',
                    success:function(json){
						$('#pc_code').val(json.pc_string);
                        // $('.otb_packing_numbers').html(json.pc_string);
                        inc = json.inc;
                        pc_code = json.pc_code;

						$.ajax({
							type:'POST',
							url:BaseUrl + page_class + '/getDetailPacking',
							data:{'packing_number':json.pc_code,'inc':json.inc},
							dataType:'json',
							success:function(json){
								$('#weight').val(json.weight);
								$('#volume').val(json.volume);
							}
						});
                    }
                });
            }else{
                toastr['error']('Please select outbound',"Error");
            }
        });

        $(document).on('click','#outbound-list tbody tr',function(e){
            e.preventDefault();
            $('#outbound-list tr').removeClass('tr_selected');
            $(this).addClass('tr_selected');
        });

        $(document).on('click','.back_picking',function(e){
            e.preventDefault();
            $('.outbound_list_container').hide();
            $('.picking_list_container').fadeIn();
        });

        $(document).on('click','.back_outbound',function(e){
            e.preventDefault();
            $('.rec_list_container').hide();
            $('.outbound_list_container').fadeIn();
        });

        // $('#search_rec').on('submit', function(e){
        $('#kd_unik').on('keyup', function(e){
			if(e.keyCode === 13) {
				e.preventDefault();
				var kd_unik=$('#kd_unik').val();
				var packing_number=$('#pc_code').val();
				var weight=$('#weight').val();
				var volume=$('#volume').val();
				var qty=$('#qty').val();
				var picking=$('#pl_name').val();

				if(kd_unik.length > 0) {
					$.ajax({
						type:'POST',
						url:BaseUrl + page_class + '/getQtyPicked',
						data:{'kd_unik':kd_unik,'id_outbound':id_outbound,'picking':picking},
						dataType:'json',
						beforeSend:function(){
							$('#kd_unik').parent().after('<img src="'+BaseUrl + 'assets/global/img/loading.gif" style="display:inline-block;margin-top:10px;" class="scan_loader"/>');
						},
						success:function(json){
							if(qty > 0) {
								if(parseInt(qty) > parseInt(json.qty)) {
									console.log('qty:' + parseInt(qty));
									console.log('json.qty:' + parseInt(json.qty));
									toastr['error']("Quantity can't be more than " + json.qty,"Error");
									$('.scan_loader').remove();
								} else if(parseInt(qty) == parseInt(json.qty)) {
									$.ajax({
										type:'POST',
										url:BaseUrl + page_class + '/check_kd_unik',
										data:{'kd_unik':kd_unik,'id_outbound':id_outbound,'pc_code':pc_code,'inc':inc,'packing_number':packing_number,'weight':weight,'volume':volume},
										dataType:'json',
										beforeSend:function(){
											$('#kd_unik').parent().after('<img src="'+BaseUrl + 'assets/global/img/loading.gif" style="display:inline-block;margin-top:10px;" class="scan_loader"/>');
										},
										success:function(json){
											if(json.status=='exist'){
												toastr['success']('Scanned',"Success");
												$("#rec-list").DataTable().ajax.reload();
											}else if(json.status=='scanned'){
												toastr['info']('LPN was already scanned',"Info");
											}else if(json.status=='wrong_packing'){
												toastr['error']('Packing Number is not available for this Outbound Document.',"Error");
											}else{
												toastr['error']('LPN not found',"Error");
											}
											$('#kd_unik').val('').focus();
											$('#qty').val('');
											$('.scan_loader').remove();
										}
									});
								} else if(parseInt(qty) < parseInt(json.qty)) {
									$.ajax({
										type:'POST',
										url:BaseUrl + 'Api_split_serial_number/post',
										data:{
											'new_serial_number':'SPLITBYSYSXX' + kd_unik + 'YY' + new Date().getTime(),
											'old_serial_number':kd_unik,
											'qty':qty,
											'flag':'1',
											'picking':picking,
											'outbound':$('.otb_kd').text()
										},
										dataType:'json',
										beforeSend:function(){
											$('#kd_unik').parent().after('<img src="'+BaseUrl + 'assets/global/img/loading.gif" style="display:inline-block;margin-top:10px;" class="scan_loader"/>');
										},
										success:function(json){
											console.log(json.param);
											$.ajax({
												type:'POST',
												url:BaseUrl + page_class + '/check_kd_unik',
												data:{'kd_unik':json.param,'id_outbound':id_outbound,'pc_code':pc_code,'inc':inc,'packing_number':packing_number,'weight':weight,'volume':volume},
												dataType:'json',
												beforeSend:function(){
													$('#kd_unik').parent().after('<img src="'+BaseUrl + 'assets/global/img/loading.gif" style="display:inline-block;margin-top:10px;" class="scan_loader"/>');
												},
												success:function(json){
													if(json.status=='exist'){
														toastr['success']('Scanned',"Success");
														$("#rec-list").DataTable().ajax.reload();
													}else if(json.status=='scanned'){
														toastr['info']('LPN was already scanned',"Info");
													}else if(json.status=='wrong_packing'){
														toastr['error']('Packing Number is not available for this Outbound Document.',"Error");
													}else{
														toastr['error']('LPN not found',"Error");
													}
													$('#kd_unik').val('').focus();
													$('#qty').val('');
													$('.scan_loader').remove();
												}
											});
										}
									});
								}
							} else {
								$('#qty').val(json.qty);
								$('.scan_loader').remove();
							}
						}
					});
				}
			}
		});

		$('#pc_code').on('keyup', function(e){
			if(e.keyCode === 13) {
				e.preventDefault();
				var packing_number=$('#pc_code').val();

				if(packing_number.length > 0) {
					$.ajax({
						type:'POST',
						url:BaseUrl + page_class + '/getDetailPacking',
						data:{'packing_number':packing_number},
						dataType:'json',
						success:function(json){
							$('#weight').val(json.weight);
							$('#volume').val(json.volume);
							$('#kd_unik').focus();
						}
					});
				}
			}
		});

        $(document).on('click','.print_surat_jalan',function(){

        });

        $(document).on('click','.new_packing',function(e){
            e.preventDefault();

            $.ajax({
                type:'POST',
                url: BaseUrl + page_class + '/check_pc_code',
                data: { 'id_outbound': id_outbound, 'pc_code': pc_code, 'inc': inc },
                dataType:'json',
                success:function(response){

                    if(response.status == 'OK'){

                        // var pc_string='';
                        // inc=parseInt(inc)+1;
                        // for(i=1;i<=inc;i++){
                            // if(i<10){
                                // var pad=0;
                            // }else{
                                // var pad='';
                            // }
                            // if(inc<10){
                                // var pad2=0;
                            // }else{
                                // var pad2='';
                            // }
                            // if(i==inc){
                                // pc_string='- <b>'+pc_code+pad+inc+pad2+inc+'</b><br/>'+pc_string;
                            // }else{
                                // pc_string='&nbsp;&nbsp;- '+pc_code+pad+i+pad2+inc+'<br/>'+pc_string;
                            // }
                        // }

                        // $('.otb_packing_numbers').html(pc_string);

						var pc_string='';
                        inc=parseInt(inc)+1;
						var incString = inc.toString();

						pc_string = pc_code + incString.padStart(3, '0');

                        $('#pc_code').val(pc_string);
                        $('#weight').val('0');
                        $('#volume').val('0');
                        // $('.otb_packing_numbers').html(pc_string);
						toastr['success']('Successfully request new packing number (' + pc_string + ').',"Success");

                    }else{
                        toastr['error'](response.message,"Error");
                    }
                },
                error: function(err){
                    console.log(err);
                }
            });
        });

        $(document).on('click','.print_label',function(e){
			e.preventDefault();
            if($(this).hasClass('disabled-link')){
                toastr['error']('Please scan all item first',"Error");
            }else{
			    printBarcode();
            }
		});
    }

    var printBarcode = function(){

		var $btn = $('.print_label');
			$btn.button('loading');
            id_outbound = $('#id_outbound').val();
            packing_number = $('#pc_code').val();

		$.ajax({
			url: BaseUrl + page_class + '/printZpl',
			type: 'POST',
			dataType: 'JSON',
			data: {
				qty_print: 1,
				id_outbound:id_outbound,
				packing_number:packing_number
			},
			success: function(response){
				if(response.status == 'OK'){
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				}else
					bootbox.alert(response.message);
    				setTimeout(function(){
    					$btn.button('reset');
    				}, 1000);
			},
			error: function(response){
				$btn.button('reset');
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            initTable();
        }

    };
}();

jQuery(document).ready(function() {
    Handler.init();
});

var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var content_datatable;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#supplier, #periode, #doc-type').select2({
			allowClear: false,
			width: null
		});

		var id = $('input[name="id"]').val();
		if(id.length > 0)
			edit(id);

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
	}

	var reset = function(){
		$('#po').val('');
		$('#supplier').select2('val', '');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'today');
	}

    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "language": {
                    "lengthMenu": "Display _MENU_ records",
                    "info" : ", Display _START_ to _END_ of _TOTAL_ entries"
                },
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
				},
                "ajax": {
                    "url": BaseUrl+page_class+"/get_list", // ajax source
					"data": {
						po: $('#po').val(),
						id_supplier: $('#supplier').val(),
						from: $('#from').val(),
						to: $('#to').val(),
						year: $('#year').val()
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": false }
                  ],
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [2,7]
					},
				]
            }
        });
    }

	var edit = function(id){
		//var id=$(this).attr('data-id');
		//App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl+'inbound/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="kd_inbound"]').val(json.kd_inbound);
					$('#data-table-form input[name="tanggal_inbound"]').val(json.tanggal_inbound);
					$('#data-table-form select[name="id_supplier"]').val(json.id_supplier).selectpicker("refresh");
					$('#data-table-form select[name="id_inbound_document"]').val(json.id_inbound_document).selectpicker("refresh");
					$('#data-table-form #added_rows').append(json.inbound_barang_html);
					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});
					$(".remove_row").click(function(){
						$(this).closest("tr").remove();
					});
				}else{
					toastr['error']("Could not load data.","Error");
				}

				//App.unblockUI();
			},
			error: function(response){
				//App.unblockUI();
			}
		});
	}

	var updateStatusReceiving = function(){

		var idInbound = [];
		$('#datatable_ajax tbody input[type="checkbox"]').each(function(){
			var ctrl = $(this);
			if(ctrl.is(':checked'))
				idInbound.push(ctrl.val());
		});

		console.log(idInbound);
		return false;

		$.ajax({
			url: BaseUrl + 'inbound/updateStatusReceiving',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_inbound: idInbound
			},
			success: function(response){

			},
			error: function(response){
				bootbox.mesage(response.responseText);
			}
		});
	}

    var pageInteraction = function(){

		$('.btn-receiving').on('click', function(){
			updateStatusReceiving();
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			handleRecords();
		});

		$('#reset').on('click', function(){
			reset();
		});

        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
            $("#form-content select").val('').selectpicker('refresh');
            $("table#inbound-barang .added_row").remove();
        });
        $('#data-table-filter').submit(function(e){
            e.preventDefault();
            data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
            data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
            data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.hide-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').hide();
            $('.filter-hide-control,.msg-filter').show();
        });
        $('.show-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').show();
            $('.filter-hide-control,.msg-filter').hide();
        });
        $('.reset-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter input[name="nama_data-table"]').val('');
            $('#data-table-filter input[name="kota"]').val('');
            $('#data-table-filter textarea[name="alamat_data-table"]').val('');
            data-table_datatable.clearAjaxParams();
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });
        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

            location.href = BaseUrl + "inbound/edit/" + id;
        });
        $(".add_item").click(function(e){
            $.ajax({
                url:BaseUrl+"inbound/get_add_item_row",
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows").append("<tr id='target_new_row'><td colspan='4'>Please Wait</td></tr>");
                },
                success:function(html){
                    $("#target_new_row").replaceWith(html);
                    $(".selectpicker").selectpicker("refresh");
                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();
                    });
                    $('#data-table-form select.add_rule').each(function() {
                        $(this).rules("add", "required");
                    });
                    $('#data-table-form input.add_rule').each(function() {
                        $(this).rules("add", "required");
                    });
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want to remove data?", function(result) {
                if(result){
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'inbound/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){
                                toastr['success']("Delete data success.","Success");
                                content_datatable.getDataTable().ajax.reload();
                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You dont have authority to delete data.","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
                        }
                    });
                }
            });
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            console.log("datepicker");
            $('.mdatepicker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

    }

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_inbound: {
                    required: true
                },
                tanggal_inbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_inbound_document: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'inbound/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Changes data success.","Success");
                            }else{
                                toastr['success']("Save data success.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You dont have authority to add data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You dont have authority to change data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            handleRecords();
            pageInteraction();
            handleFormSettings();
            handleDatePickers();
            /*handleBootstrapSelect();*/
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

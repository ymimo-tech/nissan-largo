var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var content_datatable,
		idDestination;

	var initialize = function(){

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: moment().format('DD/MM/YYYY')
		});

		if($('#data-table-form select[name="id_inbound_status"]') != 'undefined'){
			$('#data-table-form select[name="id_inbound_status"]').select2();
		}

		$('#id-outbound').select2();
		$('#id-area').select2();

		var id = $('input[name="id"]').val();
		if(id.length > 0){
			edit(id);
			$('#id-outbound').attr('disabled', '');
			//initTablePicked();
		} else {
			var id_outbound = $('input[name="id_outbound"]').val();
			if(id_outbound.length > 0){
				$('#id-outbound').val(id_outbound).trigger('change');
				$('#id-outbound').attr('disabled', '');
			}
			initTable();
		}

	}

	var reset = function(){
		$('#data-table-form input[name="id"]').val('');
		$('#data-table-form input[name="params"]').val('');
		$('#data-table-form input[name="pl_name"]').val('');
		$('#data-table-form input[name="tanggal_picking"]').val(moment().format('DD/MM/YYYY'));
		$('#data-table-form select[name="id_outbound"]').val(null).trigger('change');
		$('#data-table-form #added_rows').html('');

		$('#data-table-form input[name="id_outbound"]').focus();
	}

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/getEdit',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					//$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="pl_name"]').val(json.pl_name);
					$('.pl-name').html(json.pl_name);
					//$('#data-table-form input[name="tanggal_picking"]').val(json.pl_date);
					//$('#data-table-form input[name="tanggal_picking"]').datepicker("update", json.pl_date);
					//$('.date-picker').datepicker("update", json.pl_date);

					$('.date-picker').datepicker('remove');

					$('.date-picker').datepicker({
						orientation: "left",
						format: 'dd/mm/yyyy',
						autoclose: true,
						startDate: json.pl_date
					});

					$('.date-picker').datepicker("update", json.pl_date);

					//$('#id-outbound').val(json.id_outbound).trigger('change');
					initTablePicked();

				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getCustomer = function(){
		$.ajax({
			url: BaseUrl + 'inbound/getCustomer',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_customer);
						row = row.replace(/{{ text }}/gi, data[i].customer_name);
					}

					$('#destination').select2('destroy');

					$('#destination').html(row);
					$('#destination').select2();

					$('#destination').val(idDestination).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getSupplier = function(){
		$.ajax({
			url: BaseUrl + 'inbound/getSupplier',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_supplier);
						row = row.replace(/{{ text }}/gi, data[i].kd_supplier + ' - ' + data[i].nama_supplier);
					}

					$('#destination').select2('destroy');

					$('#destination').html(row);
					$('#destination').select2();
						console.log(idDestination);
					$('#destination').val(idDestination).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var initTablePicked = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getDetailItemPicked',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_outbound: $('#id-outbound').val(),
				id_area: $('#id-area').val(),
				pl_id: $('#data-table-form input[name="id"]').val()
			},
			success: function(response){
				App.unblockUI();

				var tpl = '<tr>\
								<td><div align="center">{{ no }}</div></td>\
								<td><div align="center">{{ item_code }}</div></td>\
								<td>{{ item_name }}</td>\
								<td><div align="right" class="form-group" style="margin: 0; padding: 0;">{{ qty_doc }}</div></td>\
								<td><div align="right" {{ style }}>{{ available_qty }}</div></td>\
								<td><div align="right">{{ previous_pick }}</div></td>\
								<td><div align="right">{{ pick_qty }}</div></td>\
								<td style="display: none;"><div align="right">{{ batch }}</div></td>\
								<td><div align="right">{{ remaining_qty }}</div></td>\
						  </tr>';


				var data = response.data,
					iLen = data.length,
					row;

				for(var i = 0; i < iLen; i++){

					var qtyDoc = data[i].jumlah_barang,
						availableQty = data[i].available_qty,
						previousQty = data[i].previous_pick,
						pickQty = data[i].picked_qty,
						remainingQty = data[i].remaining_qty,
						satuan = data[i].nama_satuan || '',
						limit = 0;

						availableQty = availableQty + ' ' + satuan;

					var style = '';
					if(parseFloat(availableQty) < parseFloat(remainingQty))
						style = 'style="color: red !important;"';

					if(parseFloat(qtyDoc) <= parseFloat(availableQty)){
						limit = qtyDoc;
						qtyDoc = '<strong>'+qtyDoc+' ' + satuan + '</strong>';
					}else if(parseFloat(qtyDoc) > parseFloat(availableQty)){
						limit = availableQty;
						availableQty = '<strong>'+availableQty+'</strong>';
					}

					if(String(qtyDoc).indexOf(' ') < 0)
						qtyDoc = qtyDoc + ' ' + satuan;

					if(String(availableQty).indexOf(' ') < 0)
						availableQty = availableQty + ' ' + satuan;

					if(String(previousQty).indexOf(' ') < 0)
						previousQty = previousQty + ' ' + satuan;

					if(String(remainingQty).indexOf(' ') < 0)
						remainingQty = remainingQty + ' ' + satuan;

					if($('#data-table-form input[name="id"]').val().length <= 0){
						var dis = '';
						if(parseFloat(remainingQty) <= 0)
							dis = 'disabled';

					}

					pickQty = '<input type="text" class="pick-qty form-control input-sm table-input" data-id="'+data[i].id_barang+'" value="'+pickQty+'"" '+dis+' />&nbsp;&nbsp;'+satuan+'';

					var batch = data[i].batchs || '',
						batchInput = '<div align="center">-</div>';

					if(batch.length > 0){

						var html = '<select name="batchcode['+data[i].id_barang+']" data-id="'+data[i].id_barang+'" class="form-control mbatch add_rule" style="">\
										{{ opt }}\
									</select>',
							opt = '';

						var btch = data[i].batchs,
							len = btch.length;

						for(var j = 0; j < len; j++){

							var sel = '';
							if(data[i].batch == btch[j].kd_batch)
								sel = 'selected';

							opt += '<option value="'+btch[j].kd_batch+'" '+sel+'>'+btch[j].kd_batch+'</option>';
						}

						batchInput = html.replace(/{{ opt }}/gi, opt);

					}

					row += tpl;
					row = row.replace(/{{ no }}/gi, i + 1);
					row = row.replace(/{{ style }}/gi, style);
					row = row.replace(/{{ item_code }}/gi, data[i].kd_barang);
					row = row.replace(/{{ item_name }}/gi, data[i].nama_barang);
					row = row.replace(/{{ qty_doc }}/gi, qtyDoc);
					row = row.replace(/{{ available_qty }}/gi, availableQty);
					row = row.replace(/{{ previous_pick }}/gi, previousQty);
					row = row.replace(/{{ pick_qty }}/gi, pickQty);
					row = row.replace(/{{ batch }}/gi, batchInput);
					row = row.replace(/{{ remaining_qty }}/gi, remainingQty);
				}

				if(iLen <= 0){
					row = '<tr><td colspan="9"><div align="center">No data</div></td></tr>';
				}

				$('#table-list tbody').html(row);

				setTimeout(function(){
					//$('.mbatch').attr('disabled','');
					//$('.mbatch').select2();
				}, 200);

				$('.detail-item').show();

			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

    }

	var initTable = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getDetailItem',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_outbound: $('#id-outbound').val(),
				id_area: $('#id-area').val(),
				pl_id: $('#data-table-form input[name="id"]').val()
			},
			success: function(response){
				App.unblockUI();

				var tpl = '<tr>\
								<td><div align="center">{{ no }}</div></td>\
								<td><div align="center">{{ item_code }}</div></td>\
								<td>{{ item_name }}</td>\
								<td>{{ item_area }}</td>\
								<td>{{ outbound_code }}</td>\
								<td><div align="right" class="form-group" style="margin: 0; padding: 0;">{{ qty_doc }}</div></td>\
								<td><div align="right" {{ style }}>{{ available_qty }}</div></td>\
								<td><div align="right">{{ previous_pick }}</div></td>\
								<td><div align="right">{{ pick_qty }}</div></td>\
								<td style="display: none;"><div align="right">{{ batch }}</div></td>\
								<td><div align="right">{{ remaining_qty }}</div></td>\
						  </tr>';


				var data = response.data,
					iLen = data.length,
					row;

				for(var i = 0; i < iLen; i++){

					var qtyDoc = data[i].jumlah_barang,
						availableQty = data[i].available_qty,
						previousQty = data[i].previous_pick,
						remainingQty = data[i].remaining_qty,
						satuan = data[i].nama_satuan || '',
						limit = 0;

						//qtyDoc = qtyDoc + ' ' + satuan;
						//availableQty = availableQty + ' ' + satuan;

					var style = '';
					if(parseFloat(availableQty) < parseFloat(remainingQty))
						style = 'style="color: red !important;"';

					if(parseFloat(qtyDoc) <= parseFloat(availableQty) && parseFloat(previousQty) <= 0){
						limit = qtyDoc;
						qtyDoc = '<strong>'+qtyDoc+' ' + satuan + '</strong>';
					}else if(parseFloat(qtyDoc) > parseFloat(availableQty)){
						limit = availableQty;
						availableQty = '<strong>'+availableQty + ' ' + satuan + '</strong>';
					}else if(parseFloat(previousQty) != 0){
						limit = remainingQty;
						remainingQty = '<strong>'+remainingQty + ' ' + satuan + '</strong>';
					}

					if(String(qtyDoc).indexOf(' ') < 0)
						qtyDoc = qtyDoc + ' ' + satuan;

					if(String(availableQty).indexOf(' ') < 0)
						availableQty = availableQty + ' ' + satuan;

					if(String(previousQty).indexOf(' ') < 0)
						previousQty = previousQty + ' ' + satuan;

					if(String(remainingQty).indexOf(' ') < 0)
						remainingQty = remainingQty + ' ' + satuan;

					var dis = '';
					if(parseFloat(remainingQty) <= 0)
						dis = 'disabled';

					pickQty = '<input type="text" class="pick-qty form-control input-sm table-input" data-id="'+data[i].id_barang+'" value="0" '+dis+' />&nbsp;&nbsp;'+satuan+'';

					var batch = data[i].batch || '',
						batchInput = '<div align="center">-</div>';

					if(batch.length > 0){

						var html = '<select name="batchcode['+data[i].id_barang+']" data-id="'+data[i].id_barang+'" class="form-control mbatch add_rule" style="width: 100%;">\
										{{ opt }}\
									</select>',
							opt = '';

						var btch = data[i].batchs,
							len = btch.length;

						for(var j = 0; j < len; j++){

							var sel = '';
							if(data[i].batch == btch[j].kd_batch)
								sel = 'selected';

							opt += '<option value="'+btch[j].kd_batch+'" '+sel+'>'+btch[j].kd_batch+'</option>';
						}

						batchInput = html.replace(/{{ opt }}/gi, opt);

					}
						//batchInput = '<input type="text" class="batch form-control input-sm table-input" data-id="'+data[i].id_barang+'" value="'+batch+'" />';

					row += tpl;
					row = row.replace(/{{ no }}/gi, i + 1);
					row = row.replace(/{{ style }}/gi, style);
					row = row.replace(/{{ item_code }}/gi, data[i].kd_barang);
					row = row.replace(/{{ item_name }}/gi, data[i].nama_barang);
					row = row.replace(/{{ item_area }}/gi, data[i].item_area);
					row = row.replace(/{{ outbound_code }}/gi, data[i].outbound_code);
					row = row.replace(/{{ qty_doc }}/gi, qtyDoc);
					row = row.replace(/{{ available_qty }}/gi, availableQty);
					row = row.replace(/{{ previous_pick }}/gi, previousQty);
					row = row.replace(/{{ pick_qty }}/gi, pickQty);
					row = row.replace(/{{ batch }}/gi, batchInput);
					row = row.replace(/{{ remaining_qty }}/gi, remainingQty);
				}

				if(iLen <= 0){
					row = '<tr><td colspan="9"><div align="center">No data</div></td></tr>';
				}

				$('#table-list tbody').html(row);

				setTimeout(function(){
					//$('.mbatch').attr('disabled','');
					//$('.mbatch').select2();
				}, 200);

				$('.detail-item').show();

			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

    }

    var pageInteraction = function(){

		$(document).on('change', '.mbatch', function(){

			var ctrl = $(this);

			$.ajax({
				url: BaseUrl + page_class + '/getBatchRow',
				type: 'POST',
				dataType: 'JSON',
				data: {
					id_outbound: $('#id-outbound').val(),
					id_area: $('#id-area').val(),
					id_barang: ctrl.parent().find('select').attr('data-id'),
					kd_batch: ctrl.val()
				},
				success: function(response){

					var data = response.data;

					var qtyDoc = data.jumlah_barang,
						availableQty = data.available_qty,
						previousQty = data.previous_pick,
						remainingQty = data.remaining_qty,
						satuan = data.nama_satuan || '',
						limit = 0;

					var style = '';
					if(parseFloat(availableQty) < parseFloat(remainingQty))
						style = 'style="color: red !important;"';

					if(parseFloat(qtyDoc) <= parseFloat(availableQty) && parseFloat(previousQty) <= 0){
						limit = qtyDoc;
						qtyDoc = '<strong>'+qtyDoc+' ' + satuan + '</strong>';
					}else if(parseFloat(qtyDoc) > parseFloat(availableQty)){
						limit = availableQty;
						availableQty = '<strong>'+availableQty + ' ' + satuan + '</strong>';
					}else if(parseFloat(previousQty) != 0){
						limit = remainingQty;
						remainingQty = '<strong>'+remainingQty + ' ' + satuan + '</strong>';
					}

					if(String(qtyDoc).indexOf(' ') < 0)
						qtyDoc = qtyDoc + ' ' + satuan;

					if(String(availableQty).indexOf(' ') < 0)
						availableQty = availableQty + ' ' + satuan;

					if(String(previousQty).indexOf(' ') < 0)
						previousQty = previousQty + ' ' + satuan;

					if(String(remainingQty).indexOf(' ') < 0)
						remainingQty = remainingQty + ' ' + satuan;

					var dis = '';
					if(parseFloat(remainingQty) <= 0)
						dis = 'disabled';

					pickQty = '<input type="text" class="pick-qty form-control input-sm table-input" data-id="'+data.id_barang+'" value="0" '+dis+' />&nbsp;&nbsp;'+satuan+'';
					console.log(ctrl.parents().closest('tr'));
					ctrl.parents().closest('tr')[0].cells[4].innerHTML = '<div align="right" '+style+'>'+availableQty+'</div>';
					ctrl.parents().closest('tr')[0].cells[5].innerHTML = '<div align="right">'+previousQty+'</div>';
					ctrl.parents().closest('tr')[0].cells[6].innerHTML = '<div align="right">'+pickQty+'</div>';
					ctrl.parents().closest('tr')[0].cells[8].innerHTML = '<div align="right">'+remainingQty+'</div>';

				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
		});

		$(document).on('focusout', '.pick-qty', function(){
			var ctrl = $(this),
				val = ctrl.val();

			if(val.length <= 0){
				ctrl.val('0');
			}
		});

		$(document).on('focus', '.pick-qty', function(){
			var ctrl = $(this),
				val = ctrl.val();

			if(val == '0'){
				ctrl.val('');
			}
		});

		$(document).on('keydown', '.pick-qty', function(e){
			return isNumeric(e);
		});

		$(document).on('keyup', '.pick-qty', function(){
			var tr = $(this).closest('tr'),
				doc = tr[0].cells[3].textContent,
				previous = tr[0].cells[5].textContent,
				pick = $(this).val(),
				remain = tr[0].cells[8];

			if(doc.indexOf(' ') > -1){
				var spl = doc.split(' ');
				doc = spl[0];
			}

			if(previous.indexOf(' ') > -1){
				var spl = previous.split(' ');
				previous = spl[0];
			}

			var newRemain = parseFloat(doc) - (parseFloat(previous) + parseFloat(pick));
			if(isNaN(newRemain))
				newRemain = 0;

			var spl = [];

			if(remain.textContent.indexOf(' ') > -1){
				spl = remain.textContent.split(' ');
				remain = spl[0];
			}

			tr[0].cells[8].innerHTML = '<div align="right">' + newRemain + ' ' + spl[1] + '</div>';
		});

		$(document).on('keyup', '.pick-qty', function(){
			if($(this).val().length > 0){
				$('.pick-qty').parent().removeClass('has-error');
			}
		});

		$('#id-outbound').on('change', function(){
			var id = $('input[name="id"]').val();
			if(id.length <= 0)
				initTable();
		});
		
		$('#id-area').on('change', function(){
			var id = $('input[name="id"]').val();
			if(id.length <= 0)
				initTable();
		});

		$('#save-new').on('click', function(){
			$('input[name="params"]').val('new');
			$('#data-table-form').submit();
		});

    }

	var newPickingListCode = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getNewPickingListCode',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){
					$('#data-table-form input[name="pl_name"]').val(response.outbound_code);
					$('#data-table-form .pl-name').html(response.outbound_code);
				}else{
					toastr['error']("Generate new outbound code failed. Please refresh page","Form Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                tanggal_picking: {
                    required: true
                },
                id_outbound: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {

				var chk = true;
				var itemId = [];

				if($('#data-table-form input[name="tanggal_picking"]').val().length != 10){
					$('.date-picker').addClass('has-error');
					chk = false;
				}

				if(!/\S/.test($('#pl-name').val())) {
					toastr['error']("Picking list number is required","Form Error");
					return false;
				}

				var totalPicked = 0;

				$('#table-list tbody tr').each(function(){
					var tr = $(this);

					var doc = tr[0].cells[3].textContent;
					var avail = tr[0].cells[4].textContent;
					var prev = tr[0].cells[5].textContent;
					var remain = tr[0].cells[8].textContent;
					var limit = 0;

					if(doc.indexOf(' ') > -1){
						var spl = doc.split(' ');
						doc = spl[0];
					}

					if(avail.indexOf(' ') > -1){
						var spl = avail.split(' ');
						avail = spl[0];
					}

					if(prev.indexOf(' ') > -1){
						var spl = prev.split(' ');
						prev = spl[0];
					}

					if(remain.indexOf(' ') > -1){
						var spl = remain.split(' ');
						remain = spl[0];
					}

					if(parseFloat(doc) <= parseFloat(avail) && parseFloat(prev) == 0){
						limit = doc;
					}else if(parseFloat(doc) > parseFloat(avail)){
						limit = avail;
					}else if(parseFloat(prev) != 0){
						limit = (parseFloat(doc) - parseFloat(prev));
					}

					var pickedQty = $(this).find('.pick-qty').val();

					if(!/\S/.test(pickedQty)) {
						$(this).find('.pick-qty').parent().addClass('has-error');
						chk = false;
					}

					if(parseFloat(pickedQty) > limit){
						$(this).find('.pick-qty').parent().addClass('has-error');
						chk = false;
					}

					if(parseFloat(remain) < 0){
						$(this).find('.pick-qty').parent().addClass('has-error');
						chk = false;
					}

					totalPicked += parseFloat(pickedQty);
				});

				if(totalPicked <= 0){
					toastr['error']("Save failed. Total Pick qty must greater than 0","Form Error");
					return false;
				}

				if(!chk){
					toastr['error']("Please check again.","Form Error");
					return false;
				}

				App.blockUI();

				var barang = [];

				$('#table-list .pick-qty').each(function(){
					var ctrl = $(this),
						idBarang = ctrl.attr('data-id'),
						qty = ctrl.val(),
						batch = $('select[name="batchcode['+idBarang+']"]').val() || null;

					barang.push({id_barang: idBarang, qty: qty, batch: batch});
				});

				var postdata = {
					id: $('#data-table-form input[name="id"]').val(),
					params: $('#data-table-form input[name="params"]').val(),
					pl_name: $('#data-table-form input[name="pl_name"]').val(),
					tanggal_picking: $('#data-table-form input[name="tanggal_picking"]').val(),
					id_outbound: $('#id-outbound').val(),
					barang: barang
				};

				$.ajax({
					type:'POST',
					url: BaseUrl + page_class + '/proses',
					dataType:'json',
					data: postdata,
					beforeSend:function(){
						//$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
						//$('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
						$('.form-group').removeClass('has-error');
					},
					success:function(json){
						if(json.success==1){
							if(json.edit==1){
								toastr['success']("Save changes success.","Success");
							}else{
								toastr['success']("Save success.","Success");
							}

							var params = $('input[name="params"]').val();

							if(params == 'new'){
								reset();
								newPickingListCode();
								//$('.detail-item-add').hide();
							}else{
								setTimeout(function(){
									location.href = BaseUrl + page_class;
								}, 500);
							}
							//content_datatable.getDataTable().ajax.reload();
							//$('#form-content').modal('hide');
						}else{
							if(json.no_add){
								toastr['warning']("You dont have autority to add data.","Warning");
							}else if(json.no_edit){
								toastr['warning']("You dont have autority to change data..","Warning");
							}else if(json.picking_exist){
								toastr['warning']("Picking already created.","Warning");
							}else{
								toastr['error']("Please refresh the page and try again later.","Error Occured");
							}
						}
						App.unblockUI();
						//$('.modal-content').unblock();
						//$('.spinner').remove();
					},
					error: function(response){
						App.unblockUI();
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});

            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            handleFormSettings();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

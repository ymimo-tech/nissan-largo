var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable,
		dataTable1,
		childTable;

	var initialize = function(){
		initTable();
		initTablePicking();
		initTableShipping();
	}

	var initTable = function () {

        dataTable1 = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable1.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getDetailItemList", // ajax source
					"data": {
						pl_id: $('#data-table-form input[name="id"]').val(),
						id_outbound: $('#data-table-form input[name="id_outbound"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": true, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": false, "visible": false },
					{ "orderable": true, "visible": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true, "visible": false },
					{ "orderable": false }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [4, 11]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [6, 7, 8, 9, 10, 12]
					},
					{
						"render": function(data, type, row){
							return '<div align="center"><button class="btn green btn-xs detail" type="button">\
										Show Detail\
									</button></div>';
						},
						"targets": [13]
					}
				]
            }
        });
    }

	var initTablePicking = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list-p"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getPickingList", // ajax source
					"data": {
						id_outbound: $('#data-table-form input[name="id"]').val(),
						params: 'no_action'
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
					{ "orderable": true },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true }
                ],
                "order": [
                    [0, "DESC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1,2,3,4,5,6,7]
					},
					{
						"render": function(data, type, row){
							var putaway = row[7];

							if(putaway != '-'){
								if(putaway.indexOf(' ') >= 0){

								}else{

								}
							}else{
								return data;
							}
						},
						"targets": [8]
					}
				]
            }
        });
    }

	var childTableTpl = function(data){
		var tpl = '<div style="width: 90%; margin: 0 auto; text-align: left;">\
						<h4>LPN List</h4>\
				   </div>\
				   <table id="child-table-'+data[0]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
						<thead>\
							<tr>\
								<th><div align="center">No.</div></th>\
								<th><div align="center">LPN</div></th>\
								<th><div align="center">GID</div></th>\
								<th><div align="center">Qty</div></th>\
								<th><div align="center">Expired Date</div></th>\
								<th><div align="center">Pick Date</div></th>\
								<th><div align="center">Location</div></th>\
								<th><div align="center">Picked By</div></th>\
								<th><div align="center">Action</div></th>\
							</tr>\
						</thead>\
						<tbody>\
						</tbody>\
				   </table>';
		return tpl;
	}

	var initChildTable = function(data){
		childTable = new Datatable();

        childTable.init({
            src: $("#child-table-"+data[0]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"language": {
					"infoEmpty": ""
				},
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getScannedList", // ajax source
					"data": {
						pl_id: data[1],
						id_barang: data[2]
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1,2,3,4,5]
					}
				]
            }
        });
	}


	var pageInteraction = function(){

		$('#table-list').on('click', '.detail', function(e){
			e.preventDefault();

			var tr = $(this).closest('tr');
			var row = dataTable1.getDataTable().row( tr );

			if(tr.hasClass('shown')){
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');

				childTable.getDataTable().destroy();
			}else{
				$('#table-list tbody tr').each(function(){
					var type = $(this).attr('role');

					//if(type === undefined){
						//console.log('here');
						//$(this).hide();
						var r = dataTable1.getDataTable().row( $(this) );
						r.child.hide();
						$(this).removeClass('shown');
					//}
				});

				// Open this row
				row.child( childTableTpl(row.data()) ).show();
				tr.addClass('shown');

				initChildTable(row.data());
				initChildTableSub(row.data());
			}
		});

		$('#button-print-picking').on('click', function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_picking_list',
			{
				pl_id: id
			}, '', 'Y');
        });

		$('#button-export-excel').on('click', function(){
			var ctrl = $(this),
				id = ctrl.attr('data-id');
			post(BaseUrl + page_class + '/export_excel',
			{
				picking_id: id,
			}, '', 'Y');
		});

		$(document).on('click','#data-table-cancel',function(e){

			var picking_id = $('#data-table-form input[name="id"]').val(),
			serial_number = $(this).attr('data-id'),
			quantity = $(this).attr('data-qty');

            e.preventDefault();
			var dialog = bootbox.dialog({
			    title: 'Cancel Picked Item',
			    message: "<div><table style='width:100%;'><tr><td>LPN</td><td>:&nbsp;</td><td><b>"+serial_number+"</b></td></tr><tr><td>Quantity</td><td>:</td><td><input type='number' name='quantity' class='form-control' max='"+quantity+"' min='0' value='"+quantity+"' /></td></tr><tr><td></td><td></td><td><span id='errorMessage'></span></td></tr></table></div>",
			    size: 'medium',
			    buttons: {
			        cancel: {
			            label: "Close",
			            className: 'btn-default',
			            callback: function(){
			                //console.log('Custom cancel clicked');
			            }
			        },
			        ok: {
			            label: "Delete",
			            className: 'btn-danger',
			            callback: function(){

			            	var qtyInput = $('input[name="quantity"]').val();
			            	qtyInput = parseInt(qtyInput);

			            	if(qtyInput > quantity){
			            		$('#errorMessage').html('Quantity cannot be more than '+quantity+'.');
			            		return false;
			            	}

			            	if(qtyInput == '' || qtyInput == 0){
			            		$('#errorMessage').html('Quantity cannot be empty.');
			            		return false;
			            	}

			            	if(qtyInput < 0){
			            		$('#errorMessage').html('Quantity cannot be less than 0.');
			            		return false;
			            	}

							$.ajax({
								url:BaseUrl+page_class+'/cancel_item_picked',
								method: "POST",
								data:
								{
									picking_id:picking_id,
									serial_number:serial_number,
									quantity:qtyInput
								},
								dataType:'json',
								success: function(response){

									if(response.status){
										initTable();
					                    toastr['success'](response.message,"Success");

					                    if($('.detail-status').text() != "Picking In Progress"){
						                    location.reload();
						                }
						            }else{
					                    toastr['error'](response.message,"Failed");
						            }

								},
								error: function(err){
									console.log(err);
								}
							});

			            }

			        }
			    }
			});
/*			var r = confirm("Anda yakin ingin menghapus serial number ini ?");
			if (r == true) {

				$.ajax({
					url:BaseUrl+page_class+'/cancel_item_picked',
					method: "POST",
					data:
					{
						picking_id:$('#data-table-form input[name="id"]').val(),
						serial_number:$(this).attr('data-id')
					},
					dataType:'json',
					success: function(response){

						if(response.status){
							initTable();
		                    toastr['success'](response.message,"Success");

		                    if($('.detail-status').text() != "Picking In Progress"){
			                    location.reload();
			                }
			            }else{
		                    toastr['error'](response.message,"Failed");
			            }

					},
					error: function(err){
						console.log(err);
					}
				})

			} else {
				return false;
			}
*/		});

		var pq_id,pq_qty;
		$('#table-list').on('click','.substitute', function(e) {
			var id = $(this).attr('data-id');
			pq_id = $(this).attr('data-pq');
			pq_qty = $(this).attr('data-qty');

			App.blockUI();
			$.ajax({
				url: BaseUrl + page_class + '/getSubstitute',
				type: 'POST',
				dataType: 'JSON',
				data: {
					item_id: id,
					pq_id : pq_id
				},
				success: function(response){
					var html = '';
					var checked = '';
					for(i=0;i<response.length;i++){
						if(!response[i]['qty']){
							checked = 0;
						}else{
							checked = response[i]['qty'];
						}
						html += '<tr>\
										<td>'+response[i]['code']+'-'+response[i]['name']+'</td>\
										<td>'+response[i]['available_qty']+'</td>\
										<td>'+response[i]['loc_name']+'</td>\
										<td>\
											<input type="number" name="qty" min=0 max='+response[i]['available_qty']+' value='+checked+' class="form-control qty">\
											<input type="hidden" name="checked" value='+response[i]['id']+'>\
										</td>\
								</tr>\
								';
						checked = '';
					}
					$('#table-sub-id').html(html);

					$('#modal-sub').modal({
						backdrop: 'static',
						keyboard: true
					});
					App.unblockUI();
				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
		})

		$('#data-modal-form').on('submit', function(e) {
			e.preventDefault();

			var sub = [];
            $.each($("input[name='checked']"), function(){
                sub.push($(this).val());
            });

			var qty = [];
			var temp_qty = 0;
            $.each($("input[name='qty']"), function(){
                qty.push($(this).val());
				temp_qty += Number($(this).val());
            });
			
			if(temp_qty<=pq_qty){
				$.ajax({
					url: BaseUrl + page_class + '/checkSubstitute',
					type: 'POST',
					dataType: 'JSON',
					data: {
						pq_id : pq_id,
						item_subs : sub,
						qty : qty
					},
					success: function(response){
						if(response.status == 'OK'){
							toastr['success']('Item substitute has been updated',"Success");
							$('#modal-sub').modal('hide');
						}else{
							toastr['error']('Please try again!',"Error");
						}
					},
					error: function(response){
						if(response.status == 401){
							location.href = BaseUrl + 'login';
						}else{
							if(response.status == 200){
								toastr['success']('Item substitute has been updated',"Success");
								$('#modal-sub').modal('hide');
							}else{
								toastr['error']('Please try again!',"Error");
							}
						}
					}
				});
			}else{
				toastr['error']('Qty melebihi Qty Picked',"Error");
			}
		});
	}

	var initTableShipping = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list-s"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getShippingList", // ajax source
					"data": {
						id_outbound: $('#data-table-form input[name="id"]').val(),
						params: 'no_action'
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}

                },
                "columns": [
					{ "orderable": false, "visible": false },
					{ "orderable": true },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": false }
                ],
                "order": [
                    [0, "DESC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1,2,3,4,5,6,7]
					},
					{
						"render": function(data, type, row){
							var putaway = row[7];

							if(putaway != '-'){
								if(putaway.indexOf(' ') >= 0){

								}else{

								}
							}else{
								return data;
							}
						},
						"targets": [8]
					}
				]
            }
        });
    }

    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

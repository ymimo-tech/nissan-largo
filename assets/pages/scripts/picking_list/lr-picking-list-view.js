var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dataTable;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#periode, #status').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		setTimeout(function(){
            $('#periode').val(current_filter).trigger('change');
			initTable();
		}, 500);
	}

	var reset = function(){
		$("#pl").val(null).trigger("change");
		$("#do").val(null).trigger("change");
		$('#customer').val(null).trigger('change');
		$('#status').val(null).trigger('change');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_month');
		//$('#doc-status').select2('val', '');
	}

    var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
                current_filter=response.current_filter;
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
				},
                "ajax": {
                    "url": BaseUrl + "outbound/getPickingList", // ajax source
					"data": {
						pl_id: $('#pl').val(),
						id_outbound: $('#do').val(),
						id_customer: $('#customer').val(),
						id_gate: $('#gate').val(),
						area : $('#area').val(),
						from: $('#from').val(),
						to: $('#to').val(),
						status: $('#status').val(),
                        periode:$('#periode').val(),
                        warehouse: $('#warehouse-filter').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
        },
        "columns": [
					{ "orderable": false, "visible": false },
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
          { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
        ],
        "order": [
            [8, "DESC"]
        ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1,2,3,4,6,7,8,9,10,11]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl+'inbound/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="kd_inbound"]').val(json.kd_inbound);
					$('#data-table-form input[name="tanggal_inbound"]').val(json.tanggal_inbound);
					$('#data-table-form select[name="id_supplier"]').val(json.id_supplier).selectpicker("refresh");
					$('#data-table-form select[name="id_inbound_document"]').val(json.id_inbound_document).selectpicker("refresh");
					$('#data-table-form #added_rows').append(json.inbound_barang_html);
					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});
					$(".remove_row").click(function(){
						$(this).closest("tr").remove();
					});
				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var closePicking = function(id){
		App.blockUI();
		$.ajax({
			url: BaseUrl + page_class + '/closePicking',
			type: 'POST',
			dataType: 'JSON',
			data: {
				pl_id: id
			},
			success: function(response){
				if(response.status == 'OK'){

					dataTable.getDataTable().ajax.reload();
					toastr['success'](response.message, "SUCCESS");

				}else{
					toastr['error'](response.message, "Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var printBarcode = function(){

		// var $btn = $('#submit-print');
		// 	$btn.button('loading');

		var qty = $('.qty-print-location').html(),
			check = 0;

		if($('#data-modal-form input[name="id"]').val().length > 0){
			qty = $('.qty-print-location').html();
			check = 1;
		}

		if (!/\S/.test(qty)) {
			if(check == 0){
				$('.qty-print-group').addClass('has-error');
			}

			// $btn.button('reset');
			toastr['error']("Qty print cannot empty and must be greater than 0","Error");
			return false;
		}else{
			if(qty <= 0){

				if(check == 0){
					$('.qty-print-group').addClass('has-error');
				}

				// $btn.button('reset');
				toastr['error']("Qty print cannot empty and must be greater than 0","Error");
				return false;
			}
		}

		var params = 'multiple';
		if(check == 1)
			params = 'single';

		if($('#table-list tbody input[type="checkbox"]').is(':checked'))
			params = 'partial';

		// postdata = {
		// 	dn_id: locId
		// }

		post(BaseUrl + 'picking_list/print_picking_list_bulk',
			{
				pl_id: JSON.stringify(locId)
			}, '', 'Y');

			var $btn = $('#submit-print');
			// $btn.html('print');
			// $btn.removeClass('disabled')
		

		// $.ajax({
		// 	url: BaseUrl + 'tray_list/printTrayListBulk',
		// 	type: 'POST',
		// 	dataType: 'JSON',
		// 	data: postdata,
		// 	success: function(response){
		// 		// if(response.status == 'OK'){

		// 		// }else
		// 		// 	bootbox.alert(response.message);

		// 		// setTimeout(function(){
		// 		// 	$btn.button('reset');
		// 		// }, 1000);
		// 		//App.unblockUI();
		// 		/*
		// 		var zpl = response.zpl;

		// 		var ctrl = $(zpl);

		// 		var postdata = {
		// 			sid: $('#sid').val(),
		// 			pid: $('#pid').val(),
		// 			printerCommands: zpl
		// 		}
		// 		// store user printer settings & commands in server cache
		// 		$.post(BaseUrl + 'webclientprint/ProcessPrint.php',
		// 			postdata,
		// 			function() {
		// 				 // Launch WCPP at the client side for printing...
		// 				 var sessionId = $("#sid").val();
		// 				 jsWebClientPrint.print('sid=' + sessionId);
		// 			}
		// 		);
		// 		*/
		// 	},
		// 	error: function(response){
		// 		$btn.button('reset');
		// 		if(response.status == 401)
		// 			location.href = BaseUrl + 'login';
		// 		else
		// 			bootbox.alert(response.responseText);
		// 	}
		// });
	}

	$('.btn-print').on('click', function(){

		if(!$('#table-list tbody input').is(':checked')){
			toastr['error']('Please check at least one row to print', 'ERROR');
			return false;
		}

		showModalPrint();
	});

	var showModalPrint = function(id, code){

		if(id){

			$('#data-modal-form input[name="id"]').val(id);

			$('#qty-print').val('');
			$('.label-qty').html('Code');

			$('.qty-print-location').show();
			$('.qty-print-location').html(code);
			$('#qty-print').hide();
			$('.required').hide();

			$('#modal-print').modal({
				backdrop: 'static',
				keyboard: true
			});

		}else{

			$('.label-qty').html('Qty DN');

			if($('#table-list tbody input[type="checkbox"]').is(':checked')){

				locId = [];

				$('#table-list tbody input[type="checkbox"]').each(function(){
					var ctrl = $(this);

					if(ctrl.is(':checked'))
						locId.push(ctrl.val());
				});

				App.blockUI();

				$('#data-modal-form input[name="id"]').val('');
				$('.qty-print-location').html(locId.length);
				

				$('.qty-print-location').show();
				$('#qty-print').hide();
				$('.required').hide();

				$('#modal-print').modal({
					backdrop: 'static',
					keyboard: true
				});

				App.unblockUI();

			}else{

				App.blockUI();

				$('#modal-print').modal({
					backdrop: 'static',
					keyboard: true
				});
			
			}
		}
	}

    var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    	});

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});

		$(document).on('click','.data-table-set-complete',function(e){

    		e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			$.ajax({
				url : BaseUrl + page_class + '/setComplete',
				method : "POST",
				dataType : "JSON",
				data : { picking_id : id },
				success : function(response){

	           		if(response.status){
	            		toastr['success'](response.message,"success");
	            	}else{
	            		toastr['error'](response.message,"error");
	            	}

				},
				error : function(err){
            		toastr['error'](err.message,"error");
				}
			})

    	});

		$("#pl")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'picking_list/getPlCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.pl_id,
							text: item.pl_name
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$("#customer")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'outbound/getCustomerBySearch',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_customer,
							text: item.customer_name
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$("#gate")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			ajax: {
				url: BaseUrl + 'outbound/getGateBySearch',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_gate,
							text: item.gate_name
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$("#do")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'outbound/getOutboundCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_outbound,
							text: item.kd_outbound
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

		$(document).on('click','.data-table-print',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_picking_list',
			{
				pl_id: id
			}, '', 'Y');
        });

		$(document).on('click','.data-table-tray',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_tray_list',
			{
				pl_id: id
			}, '', 'Y');
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            location.href = BaseUrl + page_class + "/edit/" + id;
        });

        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');

			if($(this).hasClass('disabled-link'))
				return false;

            bootbox.confirm("Are you sure want remove this data ?", function(result) {
                if(result){
					App.blockUI();
                    $.ajax({
                        type:'POST',
                        url:BaseUrl + page_class + '/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){

								if(json.status == 'OK'){
									toastr['success'](json.message,"Success");
									dataTable.getDataTable().ajax.reload();
								}else{
									toastr['error'](json.message, "Error");
								}

                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You don't have autority to delete this data","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
							App.unblockUI();
                        },
						error: function(response){
							App.unblockUI();
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
                    });
                }
            });
        });

		$(document).on('click','.data-table-cancel',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			$.ajax({
				url : BaseUrl + page_class + '/cancel',
				method : "POST",
				dataType : "JSON",
				data : { picking_id : id },
				success : function(response){

	           		if(response.status){
	            		initTable();
	            		toastr['success'](response.message,"success");
	            	}else{
	            		toastr['error'](response.message,"error");
	            	}

				},
				error : function(err){
            		toastr['error'](err.message,"error");
				}
			})

        });

		$(document).on('click','.data-table-reset',function(e){
            e.preventDefault();

            alert('test');

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			$.ajax({
				url : BaseUrl + page_class + '/resetPicking',
				method : "POST",
				dataType : "JSON",
				data : { picking_id : id },
				success : function(response){

	           		if(response.status){
	            		initTable();
	            		toastr['success'](response.message,"success");
	            	}else{
	            		toastr['error'](response.message,"error");
	            	}

				},
				error : function(err){
            		toastr['error'](err.message,"error");
				}
			})

        });

    }

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_inbound: {
                    required: true
                },
                tanggal_inbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_inbound_document: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'inbound/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            dataTable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

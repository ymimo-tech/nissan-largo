var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable,
		data = [],
		page = 100,
		current = 0;

	var initialize = function(){
		initTable();
	}

	var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-detail"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"bPaginate": false,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				/*
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				*/
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getCheckResult", // ajax source
					"data": {
						cc_id: $('#data-table-form input[name="id"]').val(),
						cc_type: $('#data-table-form input[name="cc_type"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false, "visible": false },
					{ "orderable": false }
                ],
                "order": [
                    [10, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [7,8,9,10,11,-1]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [12,13]
					}
				]
            }
        });
    }

	var save = function(){

		var i = 0,
			valid = true;

		data = [];

		$('#table-detail tbody tr').each(function(){

			var rowdata = dataTable.getDataTable().row(this).data();
			//rowdata[4];

			if(rowdata[11] == 'PENDING REVIEW')
				valid = false;

			var params = '';
			if(rowdata[12] == 'NO' && rowdata[13] == 'YES'){
				params = 'POSITIF';
			}

			if(rowdata[12] == 'YES' && rowdata[13] == 'NO'){
				params = 'NEGATIF';
			}

			if(rowdata[12] == 'YES' && rowdata[13] == 'YES'){
				params = 'RELEASE';
			}

			data.push(
				{
					cc_id: rowdata[1],
					loc_id: rowdata[2],
					id_barang: rowdata[3],
					id_qc: rowdata[4],
					tgl_in: rowdata[5],
					tgl_exp: rowdata[6],
					sn: rowdata[10],
					params: params,
					status: rowdata[11],
					system_qty: rowdata[12],
					scanned_qty: rowdata[13],
					cc_qty: rowdata[14]
				}
			);

		});

		if(!valid){
			toastr["error"]("Save failed, because PENDING REVIEW still exist", "ERROR");
			return false;
		}

		$('#modal-remark').modal({
			backdrop: 'static',
			keyboard: true
		});
	}

	var closeCycleCount = function(){

		var btn = $('#submit-remark');
			btn.button('loading');

		$('.form-close').hide();
			/*
			len = data.length,
			postdata = [];

		if(len > page){
			for(var i = current; i < page; i++){
				postdata.push(data[i]);
			}

			current = postdata.length;
		}
		*/

		console.log(data);

		$.ajax({
			url: BaseUrl + page_class + '/adjustment',
			type: 'POST',
			dataType: 'JSON',
			data: {
				data: JSON.stringify(data),
				remark: $('#remark').val()
			},
			success: function(response){
				if(response.status == 'OK'){

					if(response.sn.length > 0){
						toastr['success'](response.message + '. With warning', 'SUCCESS');

						var len = response.sn.length,
							sg = '';

						for(var i = 0; i < len; i++){
							sg += sn + '<br>';
						}

						bootbox.alert(sg);

						setTimeout(function(){
							location.href = BaseUrl + page_class;
						}, 5000);
					}else{
						toastr['success'](response.message, 'SUCCESS');

						setTimeout(function(){
							location.href = BaseUrl + page_class;
						}, 500);
					}

				}else{
					toastr['error'](response.message, 'ERROR');
					$('.form-close').show();
				}
			},
			error: function(response){
				$('.form-close').show();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var pageInteraction = function(){

		$('#adjust-all').on('click', function(){

			if(!$('#table-detail tbody tr input[type="checkbox"]').is(':checked')){
				toastr['error']('No row checked to adjustment', 'ERROR');
				return false;
			}

			App.blockUI();

			setTimeout(function(){
				//var len = $('#table-detail tbody tr').length;
				var len = $('[name="sn[]"]:checked').length,
					i = 0;

				$('#table-detail tbody tr').each(function(){

					if($(this).find('input[type="checkbox"]').is(':checked')){

						var idx = parseInt($(this).index());
						if(dataTable.getDataTable().cell(idx,12).data() != 'YES' || dataTable.getDataTable().cell(idx,13).data() != 'YES'){
							dataTable.getDataTable().cell(idx,11).data('ADJUSTMENT');
						}

						i++;

						if(i == len){
							App.unblockUI();
						}

					}
				});
			}, 500);
		});

		$('#cancel-all').on('click', function(){

			if(!$('#table-detail tbody tr input[type="checkbox"]').is(':checked')){
				toastr['error']('No row checked to cancel', 'ERROR');
				return false;
			}

			App.blockUI();

			setTimeout(function(){
				//var len = $('#table-detail tbody tr').length;
				var len = $('[name="sn[]"]:checked').length,
					i = 0;

				$('#table-detail tbody tr').each(function(){

					if($(this).find('input[type="checkbox"]').is(':checked')){

						var idx = parseInt($(this).index());
						dataTable.getDataTable().cell(idx,11).data('CANCEL');

						i++;

						if(i == len){
							App.unblockUI();
						}

					}

				});
			}, 500);
		});

		$('#data-modal').on('submit', function(e){
			e.preventDefault();
			closeCycleCount();
		});

		$('#save').on('click', function(){
			save();
		});

		$(document).on('click', '#table-detail .data-table-adjust', function(){
			var ctrl = $(this),
				rowdata = dataTable.getDataTable().row($(this).parents('tr')).data(),
				name = ctrl.attr('data-name'),
				idx = parseInt($('#table-detail tr').index($(this).closest('tr'))) - 1;

			dataTable.getDataTable().cell(idx,11).data(name);
		});

		$(document).on('click', '#table-detail .data-table-cancel', function(){
			var ctrl = $(this),
				rowdata = dataTable.getDataTable().row($(this).parents('tr')).data(),
				name = ctrl.attr('data-name'),
				idx = parseInt($('#table-detail tr').index($(this).closest('tr'))) - 1;

			dataTable.getDataTable().cell(idx,11).data(name);
		});

	}

    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable,
		data = [],
		page = 100,
		current = 0;
	var childTable;

	var initialize = function(){
		initTable();
	}

	var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-detail"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"bPaginate": false,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				/*
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				*/
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getCheckResult", // ajax source
					"data": {
						cc_id: $('#data-table-form input[name="id"]').val(),
						cc_type: $('#data-table-form input[name="cc_type"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false },
					{ "orderable": false }
                ],
                "order": [
                    [2, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [6,7,8]
					}
				]
            }
        });
    }

	var save = function(){

		var i = 0,
			valid = true;

		data = [];

		$('#table-detail tbody tr').each(function(){

			var rowdata = dataTable.getDataTable().row(this).data();

			if(rowdata[8] == 'PENDING REVIEW')
				valid = false;

			data.push(
				{
					cc_id: rowdata[1],
					id_barang: rowdata[2],
					status: rowdata[8]
				}
			);

		});

		if(!valid){
			toastr["error"]("Save failed, because PENDING REVIEW still exist", "ERROR");
			return false;
		}

		$('#modal-remark').modal({
			backdrop: 'static',
			keyboard: true
		});
	}

	var closeCycleCount = function(){

		var btn = $('#submit-remark');
			btn.button('loading');

		$('.form-close').hide();
			/*
			len = data.length,
			postdata = [];

		if(len > page){
			for(var i = current; i < page; i++){
				postdata.push(data[i]);
			}

			current = postdata.length;
		}
		*/

		console.log(data);

		$.ajax({
			url: BaseUrl + page_class + '/adjustment',
			type: 'POST',
			dataType: 'JSON',
			data: {
				data: JSON.stringify(data),
				remark: $('#remark').val()
			},
			success: function(response){
				if(response.status == 'OK'){

					if(response.sn.length > 0){
						toastr['success'](response.message + '. With warning', 'SUCCESS');

						var len = response.sn.length,
							sg = '';

						for(var i = 0; i < len; i++){
							sg += sn + '<br>';
						}

						bootbox.alert(sg);

						setTimeout(function(){
							location.href = BaseUrl + page_class;
						}, 5000);
					}else{
						toastr['success'](response.message, 'SUCCESS');

						setTimeout(function(){
							location.href = BaseUrl + page_class;
						}, 500);
					}

				}else{
					toastr['error'](response.message, 'ERROR');
					$('.form-close').show();
				}
			},
			error: function(response){
				$('.form-close').show();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var childTableTpl = function(data){
		var tpl = '<div>\
					<table id="child-table-'+data[3]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
						<thead>\
							<tr>\
								<th><div align="center">No.</div></th>\
								<th><div align="center">Serial Number</div></th>\
								<th><div align="center">Location</div></th>\
								<th><div align="center">System Qty</div></th>\
								<th><div align="center">Scanned Qty</div></th>\
							</tr>\
						</thead>\
						<tbody>\
						</tbody>\
				   </table>\
				</div>';
				   
		return tpl;
	}
	
	var initChildTable = function(data){
		childTable = new Datatable();		
		
        childTable.init({
            src: $("#child-table-"+data[3]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"language": {
					"infoEmpty": ""
				},
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getCheckResultDetails", // ajax source
					"data": {
						cc_id: data[1],
						cc_type: $('#data-table-form input[name="cc_type"]').val(),
						id_barang: data[2]
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": true }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1,2,3]
					}
				]
            }
        });
	}

	var pageInteraction = function(){

		$('#adjust-all').on('click', function(){

			if(!$('#table-detail tbody tr input[type="checkbox"]').is(':checked')){
				toastr['error']('No row checked to adjustment', 'ERROR');
				return false;
			}

			App.blockUI();

			setTimeout(function(){
				//var len = $('#table-detail tbody tr').length;
				var len = $('[name="sn[]"]:checked').length,
					i = 0;

				$('#table-detail tbody tr').each(function(){

					if($(this).find('input[type="checkbox"]').is(':checked')){

						var idx = parseInt($(this).index());
						if(dataTable.getDataTable().cell(idx,5).data() != dataTable.getDataTable().cell(idx,6).data()){
							dataTable.getDataTable().cell(idx,8).data('ADJUST TO SCANNED QTY');
						}

 							i++;

						if(i == len){
							App.unblockUI();
						}

					}
				});
			}, 500);
		});

		$('#cancel-all').on('click', function(){

			if(!$('#table-detail tbody tr input[type="checkbox"]').is(':checked')){
				toastr['error']('No row checked to cancel', 'ERROR');
				return false;
			}

			App.blockUI();

			setTimeout(function(){
				//var len = $('#table-detail tbody tr').length;
				var len = $('[name="sn[]"]:checked').length,
					i = 0;

				$('#table-detail tbody tr').each(function(){

					if($(this).find('input[type="checkbox"]').is(':checked')){

						var idx = parseInt($(this).index());
						dataTable.getDataTable().cell(idx,8).data('ADJUST TO SYSTEM QTY');

						i++;

						if(i == len){
							App.unblockUI();
						}

					}

				});
			}, 500);
		});

		$('#data-modal').on('submit', function(e){
			e.preventDefault();
			closeCycleCount();
		});

		$('#save').on('click', function(){
			save();
		});

		$(document).on('click', '#table-detail .data-table-adjust', function(){
			if(!$(this).hasClass('disabled-link')){
				var ctrl = $(this),
					rowdata = dataTable.getDataTable().row($(this).parents('tr')).data(),
					name = ctrl.attr('data-name'),
					idx = parseInt($('#table-detail tr').index($(this).closest('tr'))) - 1;

				dataTable.getDataTable().cell(idx,8).data(name);
			}
		});

		$(document).on('click', '#table-detail .data-table-cancel', function(){
			if(!$(this).hasClass('disabled-link')){
				var ctrl = $(this),
					rowdata = dataTable.getDataTable().row($(this).parents('tr')).data(),
					name = ctrl.attr('data-name'),
					idx = parseInt($('#table-detail tr').index($(this).closest('tr'))) - 1;

				if(dataTable.getDataTable().cell(idx,5).data() != dataTable.getDataTable().cell(idx,6).data()){
					dataTable.getDataTable().cell(idx,8).data(name);
				}else{
					toastr["error"]("Can't change status to ADJUSTMENT LOSS", "ERROR");
				}
			}
		});

		$('#table-detail').on('click', '.data-table-detail', function(e){
			e.preventDefault();

			var tr = $(this).closest('tr');
			var row = dataTable.getDataTable().row( tr ); 
			
			if(tr.hasClass('shown')){
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
				
				childTable.getDataTable().destroy();
			}else{
				$('#table-detail tbody tr').each(function(){
					var type = $(this).attr('role');
					
					//if(type === undefined){
						//$(this).hide();
						var r = dataTable.getDataTable().row( $(this) ); 
						r.child.hide();
						$(this).removeClass('shown');
					//}
				});
			
				// Open this row
				row.child( childTableTpl(row.data()) ).show();
				tr.addClass('shown');
			
				initChildTable(row.data());
			}
		});

		$(document).on('click','#pdf',function(e){
            e.preventDefault();

			var ctrl = $(this),
				type = $('#data-table-form input[name="cc_type"]').val(),
				id = $('#data-table-form input[name="id"]').val();

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_pdf/',
			{
				cc_id: id,
				cc_type: type
			}, '', 'Y');
        });
	}

    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

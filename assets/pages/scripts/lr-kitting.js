var Ref_kitting = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

    var content_datatable;
    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
				if(response.customGroupAction=='OK'){
					toastr['success'](response.customGroupActionMessage,"Success");
				}
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				/*
                "language": {
					"lengthMenu": "Display _MENU_ records",
					"info" : ", Display _START_ to _END_ of _TOTAL_ entries"
				},
				*/
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"bDestroy": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl+page_class+"/get_list", // ajax source
					"data":{
						id_kitting: $('#kitting').val(),
					}
                },
				"columns": [
					{ "orderable": false },
					{ "orderable": false },
                    { "orderable": false },
					{ "orderable": false },
				],
				"columnDefs": [
				]
				/*
                "order": [
                    [1, "ASC"]
                ],// set first column as a default sort by asc
				*/
            }
        });
    }

    var pageInteraction = function(){

		$('#min').on('keydown', function(e){
			return isNumeric(e);
		});

		$('#batch').on('change', function(){
			var ctrl = $(this)

			if(ctrl.is(':checked'))
				$('.strategy').hide();
			else
				$('.strategy').show();
		});

		$('.modal select,#kitting').select2();

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			handleRecords();
		});

		$('#reset').on('click', function(){
			$('#kitting').val("").trigger('change');
		});

		$('.data-table-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{

			});
		});

		$("#form-content").on("shown.bs.modal", function () {

		});

        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
            $('.selectpicker').val('').trigger('change');
            $(".kitting_product").html('');
        });

        $('#data-table-filter').submit(function(e){
            e.preventDefault();
            data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
            data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
            data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.hide-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').hide();
            $('.filter-hide-control,.msg-filter').show();
        });
        $('.show-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').show();
            $('.filter-hide-control,.msg-filter').hide();
        });
        $('.reset-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter input[name="nama_data-table"]').val('');
            $('#data-table-filter input[name="kota"]').val('');
            $('#data-table-filter textarea[name="alamat_data-table"]').val('');
            data-table_datatable.clearAjaxParams();
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });
        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl+page_class+'/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    if(! json.no_data){

                        $('#referensi-kitting-form input[name="id"]').val(json.kitting.id_kitting);
                        $('#referensi-kitting-form select[name="id_barang_main"]').val(json.kitting.id_barang).trigger('change');

                        $('.kitting_product').append(json.kitting_product);
                        $('.selectpicker').select2();
                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want to remove data ?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+page_class+'/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){

                            if(json.status == 'OK'){

                                toastr['success'](json.message,"Success");
                                content_datatable.getDataTable().ajax.reload();

                            }else{

                                if(json.no_delete){
                                    toastr['warning']("You dont have authority to delete data.","Warning");
                                }else{
                                    toastr['error'](json.message, "Error Occured");
                                }

                            }

                        }
                    });
                }
            });
        });
        $(".add_item").click(function(e){
            $.ajax({
                url:BaseUrl + page_class + "/add_item",
                dataType:"html",
                beforeSend:function() {
                    $(".kitting_product").append("<tr id='target_new_row'><td colspan='3'>Please Wait</td></tr>");
                },
                success:function(html){
                    $("#target_new_row").replaceWith(html);
                    $('.selectpicker').select2();
                }
            });
        });
        $(document).on('click','.btn-delete',function(e){
            e.preventDefault();
            var me = $(this);
            var id = me.closest('.kitting_item').attr('data-id');
            if(id=='new'){
                me.closest('.kitting_item').remove();
            }else{
                me.closest('.kitting_item').remove();
                $('.for_del_container').append('<input type="hidden" name="for_del[]" value="'+id+'"/>');
            }
        });
    }

    var handleFormSettings = function() {
        $('#referensi-kitting-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                id_barang_main: {
                    required: true
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $('#referensi-kitting-form .form-group').removeClass('has-error');
				// var postdata = {
					// kd_kitting: $('#referensi-kitting-form input[name="kd_kitting"]').val(),
					// nama_kitting: $('#referensi-kitting-form input[name="nama_kitting"]').val(),
					// id_kategori: $('#referensi-kitting-form select[name="id_kategori_1"]').val(),
					// id_kategori_2: $('#referensi-kitting-form select[name="id_kategori_2"]').val(),
					// id_satuan: $('#referensi-kitting-form select[name="id_satuan"]').val(),
					// shipment_type: $('#referensi-kitting-form select[name="shipment_type"]').val(),
					// fifo_period: $('#referensi-kitting-form select[name="fifo_period"]').val(),
					// id_owner: $('#referensi-kitting-form select[name="id_owner"]').val(),
					// base_qty: $('#referensi-kitting-form input[name="base_qty"]').val(),
					// child_satuan: $('#referensi-kitting-form #child_satuan"]').val()
				// }

                var chk = true;
				var itemId = [];

                if($('.kitting_item').length <= 0){
					toastr['error']("Please add item first", "ERROR");
					return false;
				}

				$('.kitting_item select').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					itemId.push(val);

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					if(val == 'undefined' || val == '') {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}
				});

				$('.kitting_item input').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					var id = $('input[name="id"]').val();
					if(id.length > 0){
						if(val <= 0) {
							ctrl.closest('.form-group').addClass('has-error');
							chk = false;
						}
					}
				});

				if(!chk){
					toastr['error']("Please check again.","Form Error");
					return false;
				}

				var hasDuplicate = !itemId.every(function(v,i) {
					return itemId.indexOf(v) == i;
				});

				if(hasDuplicate){

					$('.kitting_item select').each(function(index1, item1){

						$.each($('.kitting_item select').not(this), function (index2, item2) {

							if ($(item1).val() == $(item2).val()) {
								$(item1).closest('.form-group').addClass('has-error');
							}

						});

					});

					toastr['error']("Duplicate item. Please fix it first","Form Error");
					return false;
				}

				var postdata = $(form).serialize();

                $.ajax({
                    type:'POST',
                    url:BaseUrl+page_class+'/proses',
                    dataType:'json',
                    data: postdata,
					//data: postdata,
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#referensi-kitting-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.status == 'OK'){
                            if(json.edit == 1){
                                toastr['success']("Change data success.","Success");
                            }else{
                                toastr['success']("Save data success.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You dont have authority to add data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You dont have authority to change data.","Warning");
                            }else{
								toastr['error'](json.message,"Error Occured");
                                //toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

	return {

        //main function to initiate the module
        init: function () {
            handleRecords();
            pageInteraction();
            handleFormSettings();
            /*handleBootstrapSelect();
            handleDatePickers();

            */
        }

    };
}();

jQuery(document).ready(function() {
    Ref_kitting.init();
});

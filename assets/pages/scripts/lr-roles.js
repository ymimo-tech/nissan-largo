var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
	
	var dataTable;
	
	var initialize = function(){
		initTable();
	}
	
	var initTable = function () {

        dataTable = new Datatable();		
		
        dataTable.init({
            src: $("#table_roles"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/get_list/",
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + page_class + '/login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [1, "ASC"]
                ]
            }
        });
    }
	
	var pageInteraction = function(){

        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $(document).on('click','.data-table-edit',function(e){
        	e.preventDefault();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl + 'roles/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    if(! json.no_data){
                        $('#data-table-form input[name="id"]').val(json.grup_id);
                        $('#data-table-form input[name="grup_name"]').val(json.grup_nama);
                        $('#data-table-form textarea[name="description"]').val(json.grup_deskripsi);
                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });

        $(document).on('click','.data-table-edit-role',function(e){
        	e.preventDefault();
            var id=$(this).attr('data-id');
            $('#form-content2').find("input[name='id']").val(id);
            $('#form-content2').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $(document).on('click','.data-table-edit-role-handheld',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            $('#form-content3').find("input[name='id']").val(id);
            $('#form-content3').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Anda yakin akan menghapus data?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'roles/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){
                                toastr['success']("Data berhasil dihapus.","Success");
                                dataTable.getDataTable().ajax.reload();
                            }else{
                                if(json.no_delete){
                                    toastr['warning']("Anda tidak mempunyai otoritas untuk menghapus data.","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
                        }
                    });
                }
            }); 
        });

        $(document).on('click','#form-content2 button[type="submit"]',function(e){
        	e.preventDefault();
        	$.ajax({
                type:'POST',
                url:BaseUrl+'roles/save_role',
                dataType:'json',
                data: $("#form-content2 #data-table-form").serialize(),
                beforeSend:function(){
                    $("#form-content2 .modal-content").block({message:null,overlayCSS:{'background':'transparent'}});
                    $('#form-content2 #data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                },
                success:function(json){
                	if(json.success==1){
	                	$("#form-content2 #table_roles").html("");
	                    $("#form-content2").modal("hide");
                        toastr['success']("Data saved","OK");
                    }else{
                        if(json.no_add){
                            toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                        }else if(json.no_edit){
                            toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                        }else{
                            toastr['error'](json.message,"Error Occured");
                        }
                    }
                    $("#form-content2").find('.modal-content').unblock();
                    $('#form-content2 .spinner').remove();
                }
            });
        });

        $("#form-content2").on("show.bs.modal", function(e) {
		    var id = $(this).find("input[name='id']").val();
		    $.ajax({
                type:'POST',
                url:BaseUrl+'roles/load_role',
                dataType:'json',
                data: {id:id},
                beforeSend:function(){
                    $("#form-content2 .modal-content").block({message:null,overlayCSS:{'background':'transparent'}});
                    $('#form-content2 #data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                },
                success:function(json){
                	if(json.success==1){
                		$("#form-content2 input[name='grup_name']").val(json.grup.grup_nama);
	                	$("#form-content2 #table_roles").html(json.content);
                    }else{
                        if(json.no_add){
                            toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                        }else if(json.no_edit){
                            toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                        }else{
                            toastr['error'](json.message,"Error Occured");
                        }
                    }
                    $("#form-content2").find('.modal-content').unblock();
                    $('#form-content2 .spinner').remove();
                }
            });
		});

		$("#form-content2").on('hidden.bs.modal', function(){
        	//do something when dialog is closed
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
        	$("#form-content2 #table_roles").html("");
        });

        $(document).on('click','#form-content3 button[type="submit"]',function(e){
            e.preventDefault();
            $.ajax({
                type:'POST',
                url:BaseUrl+'roles/save_role_handheld',
                dataType:'json',
                data: $("#form-content3 #data-table-form").serialize(),
                beforeSend:function(){
                    $("#form-content3 .modal-content").block({message:null,overlayCSS:{'background':'transparent'}});
                    $('#form-content3 #data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                },
                success:function(json){
                    if(json.success==1){
                        $("#form-content3 #table_roles").html("");
                        $("#form-content3").modal("hide");
                        toastr['success']("Data saved","OK");
                    }else{
                        if(json.no_add){
                            toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                        }else if(json.no_edit){
                            toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                        }else{
                            toastr['error'](json.message,"Error Occured");
                        }
                    }
                    $("#form-content3").find('.modal-content').unblock();
                    $('#form-content3 .spinner').remove();
                }
            });
        });

        $("#form-content3").on("show.bs.modal", function(e) {
            var id = $(this).find("input[name='id']").val();
            $.ajax({
                type:'POST',
                url:BaseUrl+'roles/load_role_handheld',
                dataType:'json',
                data: {id:id},
                beforeSend:function(){
                    $("#form-content3 .modal-content").block({message:null,overlayCSS:{'background':'transparent'}});
                    $('#form-content3 #data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                },
                success:function(json){
                    if(json.success==1){
                        $("#form-content3 input[name='grup_name']").val(json.grup.grup_nama);
                        $("#form-content3 #table_roles3").html(json.content);
                    }else{
                        if(json.no_add){
                            toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                        }else if(json.no_edit){
                            toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                        }else{
                            toastr['error'](json.message,"Error Occured");
                        }
                    }
                    $("#form-content3").find('.modal-content').unblock();
                    $('#form-content3 .spinner').remove();
                }
            });
        });

        $("#form-content3").on('hidden.bs.modal', function(){
            //do something when dialog is closed
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
            $("#form-content3 #table_roles3").html("");
        });

        $('.form-close').click(function(e){
            e.preventDefault();
            $(this).closest(".modal").modal('hide');
        });

        $("#form-content").on('hidden.bs.modal', function(){
        	//do something when dialog is closed
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
        });

		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});
		
		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});
		
		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});
		
		$('#modal-print').on('shown.bs.modal', function () { 
			$('#qty-print').focus();
		});
		
		$('#button-print-qty-barcode').on('click', function(e){
			e.preventDefault();
			
			var id = $('#data-table-form input[name="id"]').val();
			
			showModalPrint(id);
		});
	}

	var handleFormSettings = function() {
        $('#form-content #data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
            	grup_name: {
            		required: true
            	}
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }
                
            },*/
            
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'roles/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            dataTable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error'](json.message,"Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });

        $('#data-table-form-customer').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                customer_name: {
                    required: true
                },
                phone: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'outbound/save_new_customer',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('#data-table-form-customer .modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form-customer button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#customer-form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('#data-table-form-customer .modal-content').unblock();
                        $('#data-table-form-customer .spinner').remove();
                        loadCustomers();
                    }
                });
            }
        });
    }
        
    return {

        init: function () {
			initialize();
			pageInteraction();
			handleFormSettings();
        }

    };  
}();

jQuery(document).ready(function() {
    handler.init();
});
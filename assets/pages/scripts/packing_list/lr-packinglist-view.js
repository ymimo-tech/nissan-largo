var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dataTable,
		SHIPPING_ID;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#periode, #loading, #pl, #status').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		setTimeout(function(){
			$('#periode').val('custom').trigger('change');
			$('#from').val('').trigger('change');
			$('#to').val('').trigger('change');
			initTable();
		}, 500);
	}

	var reset = function(){
		$("#loading").val(null).trigger("change");
		$('#pccode').val(null).trigger('change');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_month');
	}

    var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + "packing_list/getPackingList", // ajax source
					"data": {
						id_shipping: $('#loading').val(),
						outbound: $('#outbound').val(),
						pccode: $('#pccode').val(),
						from: $('#from').val(),
						to: $('#to').val(),
                        periode:$('#periode').val(),
                        warehouse:$('#warehouse-filter').val()
					},
					"timeout":500000,
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": false },
					// { "orderable": false },
					{ "orderable": true }
                  ],
                "order": [
                    [5, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						// "targets": [1,2,3,4,5,6,7,8,9,10]
						"targets": [1,2,3,4,5,6,7,8,9]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl+'inbound/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="kd_inbound"]').val(json.kd_inbound);
					$('#data-table-form input[name="tanggal_inbound"]').val(json.tanggal_inbound);
					$('#data-table-form select[name="id_supplier"]').val(json.id_supplier).selectpicker("refresh");
					$('#data-table-form select[name="id_inbound_document"]').val(json.id_inbound_document).selectpicker("refresh");
					$('#data-table-form #added_rows').append(json.inbound_barang_html);
					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});
					$(".remove_row").click(function(){
						$(this).closest("tr").remove();
					});
				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var closeLoading = function(id){
		App.blockUI();
		$.ajax({
			url: BaseUrl + page_class + '/closeLoading',
			type: 'POST',
			dataType: 'JSON',
			data: {
				shipping_id: id
			},
			success: function(response){
				if(response.status == 'OK'){

					dataTable.getDataTable().ajax.reload();
					toastr['success'](response.message, "SUCCESS");

				}else{
					toastr['error'](response.message, "Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var checkDo = function(){

		if (!/\S/.test($('#do-number').val())) {
			$('.do-group').addClass('has-error');
			toastr['error']("Document number required.", "Error");
			return false;
		}

		App.blockUI({
			target: '#modal-do'
		});

		var doNumber = $('#do-number').val();

		$.ajax({
			url: BaseUrl + page_class + '/checkDoNumber',
			type: 'POST',
			dataType: 'JSON',
			data: {
				do_number: doNumber,
				shipping_id: SHIPPING_ID
			},
			success: function(response){
				if(response.status == 'OK'){
					$('#modal-do').modal('hide');
					printDo(doNumber);
					initTable();
					//dataTable.getDataTable().ajax.reload();
				}else{
					App.unblockUI('#modal-do');
					toastr['error'](response.message, "Error");
				}
			},
			error: function(response){
				App.unblockUI('#modal-do');
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var initChildTablePacking = function(data){
		childTable = new Datatable();

        childTable.init({
            src: $("#child-table-"+data[0]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"language": {
					"infoEmpty": ""
				},
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getItemPacking", // ajax source
					"data": {
						packing_id: data[0]
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true , visible: false},
					{ "orderable": false, visible: false }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1,2,3,4,5,6,7,8,9]
					}
				]
            }
        });
	}

	var editDimension = function(){
		var $btn = $('#submit-print');
			$btn.button('loading');
		$.ajax({
			url: BaseUrl + page_class + '/editDimension',
			type: 'POST',
			dataType: 'JSON',
			data: {
				weight: $('#weight_field').val(),
				id: $('#id_packing').val(),
				volume: $('#volume_field').val()
			},
			success: function(response){
				// console.log('MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid));
				if(response == true){
					toastr['success']("Dimension has been updated!","success");
				}

				setTimeout(function(){
					$btn.button('reset');
				}, 1000);
				initTable();
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var showModalPrint = function(id){

		if(id){

			App.blockUI();
			$.ajax({
				url: BaseUrl + page_class + '/getDimension',
				type: 'POST',
				dataType: 'JSON',
				data: {
					id: id
				},
				success: function(response){

					var data = response;
					console.log(data.weight);
					$('#id_packing').val(id);
					$('#weight_field').val(data.weight);
					$('#volume_field').val(data.volume);

					$('#modal-print').modal({
						backdrop: 'static',
						keyboard: true
					});

					App.unblockUI();
				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});

		}else{

			$('#data-modal-form input[name="id"]').val('');

			$('#qty-print').val('');

			$('.qty-print-receiving').hide();
			$('#qty-print').show();
			$('.required').show();

			$('#modal-print').modal({
				backdrop: 'static',
				keyboard: true
			});

		}
	}

    var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    	});

		$('#export-pack').on('click', function(){
			post(BaseUrl + page_class + '/export_pack',{
				id_shipping: $('#loading').val(),
				outbound: $('#outbound').val(),
				pccode: $('#pccode').val(),
				from: $('#from').val(),
				to: $('#to').val(),
				periode:$('#periode').val(),
				warehouse:$('#warehouse-filter').val()
			});
			
		});

		$("#loading")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'packing_list/getLoadingCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.code,
							text: item.code
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$(document).on('click','.delete-packing',function(e){
			e.preventDefault();
			var id = $(this).attr("data-id");
			bootbox.confirm("Are you sure want remove this data ?", function(result) {
                if(result){
					App.blockUI();
                    $.ajax({
                        type:'POST',
                        url:BaseUrl + page_class + '/delete_packing',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){

								if(json.status == 'OK'){
									toastr['success'](json.message,"Success");
									dataTable.getDataTable().ajax.reload();
								}else{
									toastr['error'](json.message, "Error");
								}

                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You don't have autority to delete this data","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
							App.unblockUI();
                        },
						error: function(response){
							App.unblockUI();
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
                    });
                }
            });
		});

		$('#data-modal-form1').on('submit', function(e){
			e.preventDefault();
			editDimension();
		});

		$("#outbound")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'packing_list/getOutboundCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.code,
							text: item.code
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$("#pccode")
		  .select2({
			multiple: true,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'packing_list/getPcCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.packing_number,
							text: item.packing_number
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('#table-list').on('click', '.detail_packing', function(e){
			e.preventDefault();

			var packing_number = $(this).text();

			var tr = $(this).closest('tr');
			var row = dataTable.getDataTable().row( tr );

			if(tr.hasClass('shown')){
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');

				childTable.getDataTable().destroy();
			}else{
				$('#table-list-packing tbody tr').each(function(){
					var type = $(this).attr('role');

					//if(type === undefined){
						//$(this).hide();
						var r = dataTable.getDataTable().row( $(this) );
						r.child.hide();
						$(this).removeClass('shown');
					//}
				});

				// Open this row
				row.child( childTablePacking(row.data()) ).show();
				tr.addClass('shown');

				console.log(row.data());
				initChildTablePacking(row.data());
			}
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			checkDo();
		});

		$(document).on('click','.data-table-print',function(e){
            e.preventDefault();

			var ctrl = $(this),
				outbound_id = ctrl.attr('data-outbound'),
				packing_number = ctrl.attr('data-packing-number');

			printBarcode(packing_number, outbound_id);
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			showModalPrint(id);
			// if(ctrl.hasClass('disabled-link'))
			// 	return false;

            // location.href = BaseUrl + page_class + "/edit/" + id;
        });

		$(document).on('click','.data-table-close',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            closeLoading(id);
        });

        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');

			if($(this).hasClass('disabled-link'))
				return false;

            bootbox.confirm("Are you sure want remove this data ?", function(result) {
                if(result){
					App.blockUI();
                    $.ajax({
                        type:'POST',
                        url:BaseUrl + page_class + '/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){

								if(json.status == 'OK'){
									toastr['success'](json.message,"Success");
									dataTable.getDataTable().ajax.reload();
								}else{
									toastr['error'](json.message, "Error");
								}

                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You don't have autority to delete this data","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
							App.unblockUI();
                        },
						error: function(response){
							App.unblockUI();
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
                    });
                }
            });
        });

		$(document).on('click','.data-table-cancel',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			$.ajax({
				url : BaseUrl + page_class + '/cancel',
				method : "POST",
				dataType : "JSON",
				data : { shipping_id : id },
				success : function(response){

	           		if(response.status){
	            		initTable();
	            		toastr['success'](response.message,"success");
	            	}else{
	            		toastr['error'](response.message,"error");
	            	}

				},
				error : function(err){
            		toastr['error'](err.message,"error");
				}
			})

        });

    }

	var childTablePacking = function(data){
		var tpl = '<div style="width: 90%; margin: 0 auto; text-align: left;">\
						<h4>LPN List</h4>\
				   </div>\
				   <table id="child-table-'+data[0]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
						<thead>\
							<tr>\
								<th><div align="center">No.</div></th>\
								<th><div align="center">Item Code</div></th>\
								<th><div align="center">Item Name</div></th>\
								<th><div align="center">LPN</div></th>\
								<th><div align="center">Outbound Doc.</div></th>\
								<th><div align="center">Supplier</div></th>\
								<th><div align="center">Picking Doc.</div></th>\
								<th><div align="center">Picked Qty</div></th>\
								<th><div align="center">Packed Qty</div></th>\
								<th><div align="center">Action</div></th>\
							</tr>\
						</thead>\
						<tbody>\
						</tbody>\
				   </table>';

		return tpl;
	}

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_inbound: {
                    required: true
                },
                tanggal_inbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_inbound_document: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'inbound/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data is successfully updated.","Success");
                            }else{
                                toastr['success']("Data is successfully added.","Success");
                            }
                            dataTable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You don't have the authority to add this info.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You don't have the authority to modify this info.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

	var printBarcode = function(packing_number, id_outbound){
		$.ajax({
			url: BaseUrl + page_class + '/printZpl',
			type: 'POST',
			dataType: 'JSON',
			data: {
				qty_print: 1,
				id_outbound:id_outbound,
				packing_number:packing_number
			},
			success: function(response){
				if(response.status == 'OK'){
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				}else
					bootbox.alert(response.message);
    				setTimeout(function(){
    				}, 1000);
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

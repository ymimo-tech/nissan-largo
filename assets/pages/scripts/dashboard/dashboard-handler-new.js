var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var initialize = function(){

		initSKUWidget();
		initWarehouseCapacity();
		initItemStatus();
		initWarehousTransaction();
		initInventoryAccuracy();

		qualityControllerChartHandler();
		returnProductChartHandler();
		failedDeliveriesChartHandler();

	}

	var initSKUWidget = function(){

		var data = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		$.post(BaseUrl+'dashboard/getSKUWidget',data,function(response){
			$('#active-sku').html(response.active_SKU);
			$('#hold-sku').html(response.hold_SKU);
			$('#qc-sku').html(response.qc_SKU);
		}, "json");

	}

	var initWarehouseCapacity = function(){

		var data = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		$.post(BaseUrl+'dashboard/getWarehouseCapacity',data,function(response){

			var bin = response.bin_locations;
			var filledBin  = response.filled_bin_location;
			var percentage = (filledBin/bin) * 100;
			percentage = percentage.toFixed(2);

			$('#bin-locations').html(bin);
			$('#filled-bin-location').html(filledBin);

			$('#bin-location-percentage').attr('aria-valuenow',percentage);
			$('#bin-location-percentage').css('width',percentage+'%');
			$('#bin-location-percentage').html(percentage+'%');


			// Percentage General Storage
			$('#general-storage-capt').html(response.general_storage+'%');
			$('#general-storage-bar').attr('aria-valuenow',response.general_storage);
			$('#general-storage-bar').css('width',response.general_storage);
			// End

			// Percentage Cold Room Storage
			$('#cold-room-storage-capt').html(response.cold_room_storage+'%');
			$('#cold-room-storage-bar').attr('aria-valuenow',response.cold_room_storage);
			$('#cold-room-storage-bar').css('width',response.cold_room_storage);
			// End

			// Percentage High Value Storage
			$('#high-value-storage-capt').html(response.high_value_storage+'%');
			$('#high-value-storage-bar').attr('aria-valuenow',response.high_value_storage);
			$('#high-value-storage-bar').css('width',response.high_value_storage);

			// Percentage Damage Storage
			$('#damage-item-storage-capt').html(response.damage_item_storage+'%');
			$('#damage-item-storage-bar').attr('aria-valuenow',response.damage_item_storage);
			$('#damage-item-storage-bar').css('width',response.damage_item_storage);

		}, "json");

	}

	var initItemStatus = function(){

		var data = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		$.post(BaseUrl+'dashboard/getItemStatus',data,function(response){

			$('#replenishment').html(response.replenishment);
			$('#expired').html(response.expired);
			$('#non-moving').html(response.non_moving);
			$('#shelf-life').html(response.shelf_life);

		}, "json");

	}

	var initWarehousTransaction = function(){

		initInbound();
		initOutbound();

	}

	var initInbound = function(){

		var data = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		$.post(BaseUrl+'dashboard/getInbound',data,function(response){

			var data = response.inbound_list;

			if(!data.length){

				var rows = '<tr>\
							<td colspan="3" align="center">No Data</td>\
					   </tr>';


			}else{

      var len = data.length,
				        tpl = '<tr>\
						           <td>{{ inb }}</td>\
            					 <td>{{ src }}</td>\
            				   </tr>',
        				rows = '',
        				row = '';

			for(var i = 0; i < len; i++) {

				row = tpl;
				row = row.replace(/{{ inb }}/gi, data[i].inb_doc);
				row = row.replace(/{{ src }}/gi, data[i].source);

				rows += row;

			}

      // Previous
			// var len = data.length,
			// 	tpl = '<tr>\
			// 			<td>{{ sku }}</td>\
			// 			<td><div align="center">{{ count }}</div></td>\
			// 			<td><div align="right">{{ qty }}</div></td>\
			// 	   </tr>',
			// 	rows = '',
			// 	row = '';

			// for(var i = 0; i < len; i++){

			// 	row = tpl;
			// 	row = row.replace(/{{ sku }}/gi, data[i].sku);
			// 	row = row.replace(/{{ count }}/gi, data[i].count);
			// 	row = row.replace(/{{ qty }}/gi, data[i].qty);

			// 	rows += row;

			//}

			}

			$('#table-inbound tbody').html(rows);

      if (response.new_inbound.length != 0) {
  			$('#new-inbound-total').html(response.new_inbound.total_inbound);
  			$('#new-inbound-total-item').html(response.new_inbound.total_sku + " SKUs");
  			$('#new-inbound-total-item-qty').html(response.new_inbound.total_sku_qty + " EA");
      } else {
        $('#new-inbound-total').html("0");
  			$('#new-inbound-total-item').html("0 SKUs");
  			$('#new-inbound-total-item-qty').html("0 EA");
      }

      if (response.open_inbound.length != 0) {
  			$('#open-inbound-total').html(response.open_inbound.total_inbound);
  			$('#open-inbound-total-item').html(response.open_inbound.total_sku + " SKUs");
  			$('#open-inbound-total-item-qty').html(response.open_inbound.total_sku_qty + " EA");
      } else {
        $('#open-inbound-total').html("0");
        $('#open-inbound-total-item').html("0 SKUs");
        $('#open-inbound-total-item-qty').html("0 EA");
      }

      if (response.on_progress_inbound.length != 0) {
  			$('#on-progress-inbound-total').html(response.on_progress_inbound.total_inbound);
  			$('#on-progress-inbound-total-item').html(response.on_progress_inbound.total_sku + " SKUs");
  			$('#on-progress-inbound-total-item-qty').html(response.on_progress_inbound.total_sku_qty + " EA");
      } else {
        $('#on-progress-inbound-total').html("0");
        $('#on-progress-inbound-total-item').html("0 SKUs");
        $('#on-progress-inbound-total-item-qty').html("0 EA");
      }
      if (response.on_progress_inbound.length != 0) {
  			$('#complete-inbound-total').html(response.complete_inbound.total_inbound);
  			$('#complete-inbound-total-item').html(response.complete_inbound.total_sku + " SKUs");
  			$('#complete-inbound-total-item-qty').html(response.complete_inbound.total_sku_qty + " EA");
      } else {
        $('#complete-inbound-total').html("0");
        $('#complete-inbound-total-item').html("0 SKUs");
        $('#complete-inbound-total-item-qty').html("0 EA");
      }

		}, "json");

		chartInboundHandler();

	}

	var chartInboundHandler = function(){

		var dataPost = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		var dataPoints = [];

        var chart = new CanvasJS.Chart("chartdiv-inbound", {
            animationEnabled: true,
            theme: "none",
            axisX:{
                title: "Hours of the day (by tally)",
                lineThickness: 1
            },
            axisY:{
                title: "",
                tickLength: 0,
                lineThickness:0,
                margin:0,
                valueFormatString:" ",
                gridThickness: 0
            },
            data: [{
                type: "column",
                color: "#00C5DC",
                dataPoints: dataPoints
            }]
        });

		  $.post(BaseUrl+'dashboard/getChartInbound',dataPost,function(response){

			var tallyAverage = response.inbound_processing_time.tally_average;
			var putawayAverage = response.inbound_processing_time.putaway_average;
			var totalAverage = response.inbound_processing_time.total_average;

			$('#tally-average').html(tallyAverage);
			$('#putaway-average').html(putawayAverage);
			$('#total-inbound-average').html(totalAverage);

			var data = response.inbound_chart;

			var isEmpty = true;

		    $.each(data, function(key, value){
            dataPoints.push({x: value.hours, y: parseInt(value.qty), label: value.hours + ' o\'clock'});
		        if(value.qty > 0){
		        	isEmpty = false;
		        }
		    });

		    if(isEmpty){

  				if(!chart.options.subtitles){
  					(chart.options.subtitles = []);
  				}

  				chart.options.subtitles.push({
  					text : 'No Inbound Activity Today',
  					verticalAlign : 'center',
            fontFamily : 'Arial',
  					fontSize : 14
  				});

		    } else {
				(chart.options.subtitles = []);
			}

		}, "json")
		.done(function(){
			chart.render();
		})

	}

	var initOutbound = function(){

		var data = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		$.post(BaseUrl+'dashboard/getOutbound',data,function(response){

			var data = response.outbound_list;

			if(!data.length){

				var rows = '<tr>\
							<td colspan="3" align="center">No Data</td>\
					   </tr>';

			}else{

				var len = data.length,
					tpl = '<tr>\
							<td>{{ outb }}</td>\
							<td>{{ dest }}</td>\
					   </tr>',
					rows = '',
					row = '';

				for(var i = 0; i < len; i++){

					row = tpl;
					row = row.replace(/{{ outb }}/gi, data[i].outb_doc);
					row = row.replace(/{{ dest }}/gi, data[i].destination);

					rows += row;

				}

			}

			$('#table-outbound tbody').html(rows);

      if (response.new_outbound.length != 0) {
  			$('#new-outbound-total').html(response.new_outbound.total_outbound);
  			$('#new-outbound-total-item').html(response.new_outbound.total_sku + " SKUs");
  			$('#new-outbound-total-item-qty').html(response.new_outbound.total_sku_qty + " EA");
      } else {
        $('#new-outbound-total').html("0");
  			$('#new-outbound-total-item').html("0 SKUs");
  			$('#new-outbound-total-item-qty').html("0 EA");
      }

      if (response.open_outbound.length != 0) {
  			$('#open-outbound-total').html(response.open_outbound.total_outbound);
  			$('#open-outbound-total-item').html(response.open_outbound.total_sku + " SKUs");
  			$('#open-outbound-total-item-qty').html(response.open_outbound.total_sku_qty + " EA");
      } else {
        $('#open-outbound-total').html("0");
  			$('#open-outbound-total-item').html("0 SKUs");
  			$('#open-outbound-total-item-qty').html("0 EA");
      }

      if (response.on_progress_outbound.length != 0) {
  			$('#on-progress-outbound-total').html(response.on_progress_outbound.total_outbound);
  			$('#on-progress-outbound-total-item').html(response.on_progress_outbound.total_sku + " SKUs");
  			$('#on-progress-outbound-total-item-qty').html(response.on_progress_outbound.total_sku_qty + " EA");
      } else {
        $('#on-progress-outbound-total').html("0");
        $('#on-progress-outbound-total-item').html("0 SKUs");
        $('#on-progress-outbound-total-item-qty').html("0 EA");
      }

      if (response.complete_outbound.length != 0) {
  			$('#complete-outbound-total').html(response.complete_outbound.total_outbound);
  			$('#complete-outbound-total-item').html(response.complete_outbound.total_sku + " SKUs");
  			$('#complete-outbound-total-item-qty').html(response.complete_outbound.total_sku_qty + " EA");
      } else {
        $('#complete-outbound-total').html("0");
        $('#complete-outbound-total-item').html("0 SKUs");
        $('#complete-outbound-total-item-qty').html("0 EA");
      }

		}, "json");

		chartOutboundHandler();

	}

	var chartOutboundHandler = function(){

		var dataPost = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		var dataPoints = [];

        var chart = new CanvasJS.Chart("chartdiv-outbound", {
            animationEnabled: true,
            theme: "none",
            axisX:{
                title: "Hours of the day (by picking)",
                lineThickness: 1
            },
            axisY:{
                title: "",
                tickLength: 0,
                lineThickness:0,
                margin:0,
                valueFormatString:" ",
                gridThickness: 0
            },
            data: [{
                type: "column",
                color: "#716ACA",
                dataPoints: dataPoints
            }]
        });

		$.post(BaseUrl+'dashboard/getChartOutbound',dataPost,function(response){

			var pickingAverage = response.outbound_processing_time.picking_average;
			var packingAverage = response.outbound_processing_time.packing_average;
			var loadingAverage = response.outbound_processing_time.loading_average;
			var totalAverage = (pickingAverage + packingAverage + loadingAverage) / 3;

			$('#picking-average').html(pickingAverage);
			$('#packing-average').html(packingAverage);
			$('#loading-average').html(loadingAverage);
			$('#total-outbound-average').html(totalAverage);

			var data = response.outbound_chart;

			var isEmpty = true;

		    $.each(data, function(key, value){
		        dataPoints.push({x: value.hours, y: parseInt(value.qty), label: value.hours + ' o\'clock'});
		        if(value.qty > 0){
		        	isEmpty = false;
		        }
		    });

		    if(isEmpty){

				if(!chart.options.subtitles){
					(chart.options.subtitles = []);
				}

				chart.options.subtitles.push({
					text : 'No Outbound Activity Today',
					verticalAlign : 'center',
          fontFamily : 'Arial',
					fontSize : 14
				});

		    }else{
				(chart.options.subtitles = []);
			}

		}, "json")
		.done(function(){
			chart.render();
		})
	}


	var qualityControllerChartHandler = function(){

		var dataPost = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		$.post(BaseUrl+'dashboard/getQCStatus',dataPost,function(response){


		}, "json")
		.done(function(response){

			$('#label-chart-damage').html(response.damage);
			$('#label-chart-good').html(response.good);
			$('#label-chart-qc').html(response.on_process);

			var chart = AmCharts.makeChart( "chart-quality-control", {
			  "type": "pie",
			  "theme": "none",
			  "dataProvider": [ {
			    "title": "Damage",
			    "value": response.damage,
			  }, {
			    "title": "Good",
			    "value": response.good
			  },{
			    "title": "On Progress",
			    "value": response.on_process
			  }],
			  "valueField": "value",
			  "colorField": "color",
			  "labelRadius": 5,
			  "radius": "42%",
			  "innerRadius": "75%",
			  "labelText": "[[title]]",
			  "showBalloon": false
			} );

			chart.dataProvider[0].color = "#00C5DC";
			chart.dataProvider[1].color = "#FFB822";
			chart.dataProvider[2].color = "#716ACA";

		});

	}

	var returnProductChartHandler = function(){
		var chart = AmCharts.makeChart( "chart-return-product", {
		  "type": "pie",
		  "theme": "none",
		  "dataProvider": [ {
		    "title": "Damage",
		    "value": 37,
		  }, {
		    "title": "Good",
		    "value": 42
		  },{
		    "title": "On Progress",
		    "value": 19
		  }],
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": 5,
		  "radius": "42%",
		  "innerRadius": "75%",
		  "labelText": "[[title]]",
		  "showBalloon": false
		} );

		chart.dataProvider[0].color = "#00C5DC";
		chart.dataProvider[1].color = "#FFB822";
		chart.dataProvider[2].color = "#716ACA";
	}

	var failedDeliveriesChartHandler = function(){
		var chart = AmCharts.makeChart( "chart-failed-deliveries", {
		  "type": "pie",
		  "theme": "none",
		  "dataProvider": [ {
		    "title": "Damage",
		    "value": 37,
		  }, {
		    "title": "Good",
		    "value": 42
		  },{
		    "title": "On Progress",
		    "value": 19
		  }],
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": 5,
		  "radius": "42%",
		  "innerRadius": "75%",
		  "labelText": "[[title]]",
		  "showBalloon": false
		} );

		chart.dataProvider[0].color = "#00C5DC";
		chart.dataProvider[1].color = "#FFB822";
		chart.dataProvider[2].color = "#716ACA";
	}

	var initInventoryAccuracy = function(){
		getCycleCount();
	}

	var getCycleCount = function(){

		var data = {
			"warehouse"	:	$('#warehouse-filter').val()
		};

		$.post(BaseUrl+'dashboard/getCycleCount',data,function(response){

			$('#last-cycle-count').html(response.last_cycle_count);

			$('#sku-checked').html(response.sku_checked)
			$('#sku-checked-bar-title').html(response.sku_checked);
			$('#sku-checked-bar-body').attr('aria-valuenow',response.sku_checked);
			$('#sku-checked-bar-body').css('width',response.sku_checked);

			$('#accuracy-level').html(response.accuracy_level);

			$('#error-rate').html(response.error_rate);

		}, "json");

	}


	var pageInteraction = function(){

		$('#warehouse-filter').change(function(){
			initialize();
		})
	}


    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    handler.init();
});

var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
	
	var chart1, chart2, chart3, chart4,
		dataTable;
	
	var initialize = function(){
		initDateRange();
		initWidget();
		initInboundChart();
		initLoadingChart();
		initActiveStockChart();
		initStatusStockChart();
		
		initTopReceived();
		initTopOrdered();
		
		initInboundStatusChart();
		initOutboundStatusChart();
		
		initReplenishment();
		initWarehouseHandler();
		
		//GET PRINTER LIST
		
		//location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
		/*
		location.href = 'MimoPrint:GET_PRINTER_LIST?url=' + base64_encode(BaseUrl + '&sid=' + $('#sessId').val());
		setTimeout(function(){
			$.get(BaseUrl + 'send_to_printer/getPrinterList/' + $('#sessId').val(), function(data){
				console.log(JSON.parse(data));
			});
		}, 1000);
		*/
		/*
		location.href = 'MimoPrint:GET_DEFAULT_PRINTER?url=' + base64_encode(BaseUrl + '&sid=' + $('#sessId').val());
		setTimeout(function(){
			$.get(BaseUrl + 'send_to_printer/getDefaultPrinter/' + $('#sessId').val(), function(data){
				console.log(data);
				console.log(JSON.parse(data));
			});
		}, 3000);
		*/
	}	
	
	var initReplenishment = function () {

        dataTable = new Datatable();
		
        dataTable.init({
            src: $(".table-replenishment"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
				$('#replenishment_loading').hide();
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + "dashboard/getReplenishment", // ajax source
					"data": {
						warehouse: $('#warehouse-filter').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": true },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": false, "visible": false }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="right"><span class="bold theme-font">{{ data }}</span></div>').replace(/{{ data }}/gi, data + ' ' + row[3]);
						},
						"targets": [1, 2]
					}
				]
            }
        });
    }
	
	var initReplenishment1 = function(){
		$.ajax({
			url: BaseUrl + 'dashboard/getReplenishment',
			type: 'POST',
			dataType: 'JSON',
			data: {
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				var data = response.data,
					len = data.length,
					row = '';
					
				var tpl = '<tr>\
								<td>{{ item }}</td>\
								<td style="text-align: right;"><span class="bold theme-font">{{ stock }}</span></td>\
								<td style="text-align: right;"><span class="bold theme-font">{{ min }}</span></td>\
						   </tr>';
				
				if(len > 0 ){
				
					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ item }}/gi, data[i].nama_barang);
						row = row.replace(/{{ stock }}/gi, data[i].stock + ' ' + data[i].nama_satuan);
						row = row.replace(/{{ min }}/gi, data[i].min + ' ' + data[i].nama_satuan);
					}
				
				}else{
					
					row += '<tr><td colspan="3" style="text-align: center;">No data</td></tr>'
					
				}
				
				$('.table-replenishment tbody').html(row);
				
				$('#replenishment_loading').hide();
			},
			error: function(response){
				//App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});
	}
	
	var initTopReceived = function(){
		$.ajax({
			url: BaseUrl + 'dashboard/getTopReceived',
			type: 'POST',
			dataType: 'JSON',
			data: {
				periode: $('#dashboard-report-range span').html(),
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				var data = response.data,
					len = data.length,
					row = '';
					
				var tpl = '<tr>\
								<td>{{ item }}</td>\
								<td style="text-align: right;"><span class="bold theme-font">{{ qty }}</span></td>\
						   </tr>';
				
				if(len > 0 ){
				
					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ item }}/gi, data[i].nama_barang);
						row = row.replace(/{{ qty }}/gi, data[i].qty + ' ' + data[i].nama_satuan);
					}
				
				}else{
					
					row += '<tr><td colspan="2" style="text-align: center;">No data</td></tr>'
					
				}
				
				$('.table-top-received tbody').html(row);
				
				$('#top_received_loading').hide();
			},
			error: function(response){
				//App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});
	}
	
	var initTopOrdered = function(){
		$.ajax({
			url: BaseUrl + 'dashboard/getTopOrdered',
			type: 'POST',
			dataType: 'JSON',
			data: {
				periode: $('#dashboard-report-range span').html(),
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				var data = response.data,
					len = data.length,
					row = '';
					
				var tpl = '<tr>\
								<td>{{ item }}</td>\
								<td style="text-align: right;"><span class="bold theme-font">{{ qty }}</span></td>\
						   </tr>';
				
				if(len > 0 ){
				
					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ item }}/gi, data[i].nama_barang);
						row = row.replace(/{{ qty }}/gi, data[i].qty + ' ' + data[i].nama_satuan);
					}
				
				}else{
					
					row += '<tr><td colspan="2" style="text-align: center;">No data</td></tr>'
					
				}
				
				$('.table-top-ordered tbody').html(row);
				
				$('#top_ordered_loading').hide();
			},
			error: function(response){
				//App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});
	}
	
	var initWidget = function(){
		//App.blockUI();
		
		$.ajax({
			url: BaseUrl + 'dashboard/getWidget',
			type: 'POST',
			dataType: 'JSON',
			data: {
				periode: $('#dashboard-report-range span').html(),
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				//App.unblockUI();
				
				var inbound = response.inbound,
					receiving = response.receiving,
					outbound = response.outbound,
					picking = response.picking;
					
				$('.inbound-item').html(inbound.total_item + ' Items');
				$('.total-inbound').attr('data-value', inbound.total_inbound);
				$('.total-inbound').html(inbound.total_inbound);
				
				$('.receiving-item').html(receiving.total_item + ' Items');
				$('.total-receiving').attr('data-value', receiving.total_receiving);
				$('.total-receiving').html(receiving.total_receiving);
				
				$('.outbound-item').html(outbound.total_item + ' Items');
				$('.total-outbound').attr('data-value', outbound.total_outbound);
				$('.total-outbound').html(outbound.total_outbound);
				
				$('.picking-item').html(picking.total_item + ' Items');
				$('.total-picking').attr('data-value', picking.total_picking);
				$('.total-picking').html(picking.total_picking);
				
				$('.total-inbound').counterUp();
				$('.total-receiving').counterUp();
				$('.total-outbound').counterUp();
				
			},
			error: function(response){
				//App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});
	}
	
	
	var showChartTooltip = function(x, y, xValue, yValue) {
		$('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
			position: 'absolute',
			display: 'none',
			top: y - 40,
			left: x - 40,
			border: '0px solid #ccc',
			padding: '2px 6px',
			'background-color': '#fff'
		}).appendTo("body").fadeIn(200);
	}
	
	var initInboundChart = function(){
		
		$.ajax({
			url: BaseUrl + 'dashboard/getInboundChart',
			type: 'POST',
			dataType: 'JSON',
			data: {
				periode: $('#dashboard-report-range span').html(),
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				console.log(response);
				var visitors = response;

				if ($('#site_statistics').size() != 0) {

					$('#site_statistics_loading').hide();
					$('#site_statistics_content').show();

					var plot_statistics = $.plot($("#site_statistics"), [{
							data: visitors,
							lines: {
								fill: 0.6,
								lineWidth: 0
							},
							color: ['#f89f9f']
						}, {
							data: visitors,
							points: {
								show: true,
								fill: true,
								radius: 5,
								fillColor: "#f89f9f",
								lineWidth: 3
							},
							color: '#fff',
							shadowSize: 0
						}],

						{
							xaxis: {
								tickLength: 0,
								tickDecimals: 0,
								mode: "categories",
								min: 0,
								font: {
									lineHeight: 14,
									style: "normal",
									variant: "small-caps",
									color: "#6F7B8A"
								}
							},
							yaxis: {
								ticks: 5,
								tickDecimals: 0,
								tickColor: "#eee",
								font: {
									lineHeight: 14,
									style: "normal",
									variant: "small-caps",
									color: "#6F7B8A"
								}
							},
							grid: {
								hoverable: true,
								clickable: true,
								tickColor: "#eee",
								borderColor: "#eee",
								borderWidth: 1
							}
						});

					var previousPoint = null;
					$("#site_statistics").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x.toFixed(2));
						$("#y").text(pos.y.toFixed(2));
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;

								$("#tooltip").remove();
								var x = item.datapoint[0].toFixed(2),
									y = item.datapoint[1].toFixed(2);

								showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' ');
							}
						} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
				}
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});

	}
	
	var initLoadingChart = function(){
		
		$.ajax({
			url: BaseUrl + 'dashboard/getLoadingChart',
			type: 'POST',
			dataType: 'JSON',
			data: {
				periode: $('#dashboard-report-range span').html(),
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				console.log(response);
				var visitors = response;

				if ($('#site_activities').size() != 0) {

					$('#site_activities_loading').hide();
					$('#site_activities_content').show();

					var plot_statistics = $.plot($("#site_activities"), [{
							data: visitors,
							lines: {
								fill: 0.6,
								lineWidth: 0
							},
							color: ['#f89f9f']
						}, {
							data: visitors,
							points: {
								show: true,
								fill: true,
								radius: 5,
								fillColor: "#f89f9f",
								lineWidth: 3
							},
							color: '#fff',
							shadowSize: 0
						}],

						{
							xaxis: {
								tickLength: 0,
								tickDecimals: 0,
								mode: "categories",
								min: 0,
								font: {
									lineHeight: 14,
									style: "normal",
									variant: "small-caps",
									color: "#6F7B8A"
								}
							},
							yaxis: {
								ticks: 5,
								tickDecimals: 0,
								tickColor: "#eee",
								font: {
									lineHeight: 14,
									style: "normal",
									variant: "small-caps",
									color: "#6F7B8A"
								}
							},
							grid: {
								hoverable: true,
								clickable: true,
								tickColor: "#eee",
								borderColor: "#eee",
								borderWidth: 1
							}
						});

					var previousPoint = null;
					$("#site_activities").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x.toFixed(2));
						$("#y").text(pos.y.toFixed(2));
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;

								$("#tooltip").remove();
								var x = item.datapoint[0].toFixed(2),
									y = item.datapoint[1].toFixed(2);

								showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' ');
							}
						} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
				}
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});

	}
	
	var initInboundStatusChart = function() {
		
		if(chart1 != undefined){
			chart1.clear();
			chart1 = null;
		}
		
		$.ajax({
			url: BaseUrl + 'dashboard/getInboundStatus',
			type: 'POST',
			dataType: 'JSON',
			data: {
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				var data = response.data,
					len = data.length,
					total = 0;
					
				for(var i = 0; i < len; i++){
					var j = parseFloat(data[i].qty);
					if(isNaN(j))
						j = 0;
				
					total += j;
				}				
					
				if(total > 0){
				
					chart1 = AmCharts.makeChart("chart_inbound", {
						"type": "pie",
						"theme": "light",
						"fontFamily": 'Arial',
						"color":    '#888',
						"marginTop": 0,
						"marginBottom": 0,
						"marginLeft": 15,
						"marginRight": 15,
						"pullOutRadius": 0,
						"dataProvider": data,
						"valueField": "qty",
						"titleField": "label",
						"exportConfig": {
							menuItems: [{
								icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
								format: 'png'
							}]
						}
					});

					$('#chart_inbound').closest('.portlet').find('.fullscreen').click(function() {
						chart1.invalidateSize();
					});
				
				}else{
					
					$('#chart_inbound')
						.html('<div style="vertical-align: middle;text-align: center; top: 50%; position: relative">\
										No data</div>');
					
				}
				
				$('#today_inbound_loading').hide();
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});
    }
	
	var initOutboundStatusChart = function() {
		
		if(chart2 != undefined){
			chart2.clear();
			chart2 = null;
		}
		
		$.ajax({
			url: BaseUrl + 'dashboard/getOutboundStatus',
			type: 'POST',
			dataType: 'JSON',
			data: {
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				var data = response.data,
					len = data.length,
					total = 0;
					
				for(var i = 0; i < len; i++){
					var j = parseFloat(data[i].qty);
					if(isNaN(j))
						j = 0;
				
					total += j;
				}
					
				if(total > 0){
				
					chart2 = AmCharts.makeChart("chart_outbound", {
						"type": "pie",
						"theme": "light",
						"fontFamily": 'Arial',
						"color":    '#888',
						"marginTop": 0,
						"marginBottom": 0,
						"marginLeft": 15,
						"marginRight": 15,
						"pullOutRadius": 0,
						"dataProvider": data,
						"valueField": "qty",
						"titleField": "label",
						"exportConfig": {
							menuItems: [{
								icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
								format: 'png'
							}]
						}
					});

					// $('#chart_outbound').closest('.portlet').find('.fullscreen').click(function() {
					// 	chart2.invalidateSize();
					// });
				
				}else{
					
					$('#chart_outbound')
						.html('<div style="vertical-align: middle;text-align: center; top: 50%; position: relative">\
										No data</div>');
					
				}
				
				$('#today_outbound_loading').hide();
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});
    }
	
	var initActiveStockChart = function() {
		
		if(chart3 != undefined){
			chart3.clear();
			chart3 = null;
		}
		
		$.ajax({
			url: BaseUrl + 'dashboard/getActiveStock',
			type: 'POST',
			dataType: 'JSON',
			data: {
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				var data = response.data,
					len = data.length,
					total = 0;
					
				for(var i = 0; i < len; i++){
					var j = parseFloat(data[i].qty);
					if(isNaN(j))
						j = 0;
				
					total += j;
				}
					
				if(total > 0){
				
					chart3 = AmCharts.makeChart("chart_6", {
						"type": "pie",
						"theme": "light",
						"fontFamily": 'Arial',
						"color":    '#888',
						"marginTop": 0,
						"marginBottom": 0,
						"marginLeft": 15,
						"marginRight": 15,
						"pullOutRadius": 0,
						"dataProvider": data,
						"valueField": "qty",
						"titleField": "label",
						"exportConfig": {
							menuItems: [{
								icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
								format: 'png'
							}]
						}
					});

					$('#chart_6').closest('.portlet').find('.fullscreen').click(function() {
						chart3.invalidateSize();
					});
					
					$('.total-stock').html(response.total);
				
				}else{
					
					$('#chart_6')
						.html('<div style="vertical-align: middle;text-align: center; top: 50%; position: relative">\
										No data</div>');
					
				}
				
				$('#current_stock_loading').hide();
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});
    }
	
	var initStatusStockChart = function() {
		
		if(chart4 != undefined){
			chart4.clear();
			chart4 = null;
		}
		
		$.ajax({
			url: BaseUrl + 'dashboard/getStatusStock',
			type: 'POST',
			dataType: 'JSON',
			data: {
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				
				var data = response.data,
					len = data.length,
					total = 0;
					
				for(var i = 0; i < len; i++){
					var j = parseFloat(data[i].qty);
					if(isNaN(j))
						j = 0;
				
					total += j;
				}
					
				if(total > 0){
				
					chart4 = AmCharts.makeChart("chart_7", {
						"type": "pie",
						"theme": "light",
						"fontFamily": 'Arial',
						"color":    '#888',
						"marginTop": 25,
						"marginBottom": 25,
						"marginLeft": 15,
						"marginRight": 15,
						"pullOutRadius": 0,
						"dataProvider": data,
						"valueField": "qty",
						"titleField": "label",
						"exportConfig": {
							menuItems: [{
								icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
								format: 'png'
							}]
						}
					});

					$('#chart_7').closest('.portlet').find('.fullscreen').click(function() {
						chart4.invalidateSize();
					});
					
					$('.total-status').html(response.total);
				
				}else{
					
					$('#chart_7')
						.html('<div style="vertical-align: middle;text-align: center; top: 50%; position: relative">\
										No data</div>');
					
				}
				
				$('#current_status_loading').hide();
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else 
					bootbox.alert(response.responseText);
			}
		});
    }
	
	var initDateRange = function(){
		if (!jQuery().daterangepicker) {
			return;
		}
		
		/*
		"ranges": {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			'Last 7 Days': [moment().subtract('days', 6), moment()],
			'Last 30 Days': [moment().subtract('days', 29), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		},
		*/

		$('#dashboard-report-range').daterangepicker({
			"ranges": {
				'Today': [moment().format('DD/MM/YYYY'), moment().format('DD/MM/YYYY')],
				'This Week': [moment().isoWeekday(1).format('DD/MM/YYYY'), moment().isoWeekday(1).add(6, 'd').format('DD/MM/YYYY')],
				'This Month': [moment().startOf('month').format('DD/MM/YYYY'), moment().endOf('month').format('DD/MM/YYYY')],
				'This Year': [moment().startOf('year').format('DD/MM/YYYY'), moment().endOf('year').format('DD/MM/YYYY')],
				'Yesterday': [moment().add(-1, 'd').format('DD/MM/YYYY'), moment().add(-1, 'd').format('DD/MM/YYYY')],
				'Last Week': [moment().subtract(1, 'weeks').isoWeekday(1).format('DD/MM/YYYY'), moment().subtract(1, 'weeks').isoWeekday(1).add(6, 'd').format('DD/MM/YYYY')],
				'Last Month': [moment().subtract(1,'months').startOf('month').format('DD/MM/YYYY'), moment().subtract(1, 'months').endOf('month').format('DD/MM/YYYY')],
				'Last Year': [moment().subtract(1,'years').startOf('year').format('DD/MM/YYYY'), moment().subtract(1,'years').endOf('year').format('DD/MM/YYYY')]
			},
			"locale": {
				"format": "DD/MM/YYYY",
				"separator": " - ",
				"applyLabel": "Apply",
				"cancelLabel": "Cancel",
				"fromLabel": "From",
				"toLabel": "To",
				"customRangeLabel": "Custom",
				"daysOfWeek": [
					"Su",
					"Mo",
					"Tu",
					"We",
					"Th",
					"Fr",
					"Sa"
				],
				"monthNames": [
					"January",
					"February",
					"March",
					"April",
					"May",
					"June",
					"July",
					"August",
					"September",
					"October",
					"November",
					"December"
				],
				"firstDay": 1
			},
			"startDate": moment().startOf('month').format('DD/MM/YYYY'),
			"endDate":  moment().endOf('month').format('DD/MM/YYYY'),
			opens: (App.isRTL() ? 'right' : 'left'),
		}, function(start, end, label) {
			$('#dashboard-report-range span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
			
			$('#site_statistics_loading').show();
			$('#site_activities_loading').show();
			$('#top_received_loading').show();
			$('#top_ordered_loading').show();
			
			initWidget();
			initInboundChart();
			initLoadingChart();
			
			initTopReceived();
			initTopOrdered();
			
		});

		//$('#dashboard-report-range span').html(moment().subtract('days', 30).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
		$('#dashboard-report-range span').html(moment().startOf('month').format('DD/MM/YYYY') + ' - ' + moment().endOf('month').format('DD/MM/YYYY'));
		$('#dashboard-report-range').show();
		
		setTimeout(function(){
			$('div.ranges ul li').each(function(){
				$(this).removeClass('active');
			});
			
			$('div.ranges ul li').eq(4).addClass('active');
		},500);
		
	}
	
	var pageInteraction = function(){
		
	}

	var initWarehouseHandler = function(){
		$('#warehouse-filter').select2();


		$('#warehouse-filter').change(function(){

			console.log($(this).val());
			initWidget();
			initInboundChart();
			initLoadingChart();
			initActiveStockChart();
			initStatusStockChart();
			
			initTopReceived();
			initTopOrdered();
			
			initInboundStatusChart();
			initOutboundStatusChart();
		})

	}
        
    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };  
}();

jQuery(document).ready(function() {
    handler.init();
});
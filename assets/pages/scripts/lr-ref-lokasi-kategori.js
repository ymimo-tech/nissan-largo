var Ref_lokasi_kategori = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

    var content_datatable;
	var locId = [];

    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                // "language": {
                    // "lengthMenu": "Display _MENU_ records",
                    // "info" : ", Display _START_ to _END_ of _TOTAL_ entries"
                // },
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl+page_class+"/get_list", // ajax source
                },
                "columns": [
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                ],
                "order": [
                    [2, "asc"]
                ],// set first column as a default sort by asc
            }
        });
    }


	var showModalPrint = function(id, code){

		if(id){

			$('#data-modal-form input[name="id"]').val(id);

			$('#qty-print').val('');
			$('.label-qty').html('Code');

			$('.qty-print-location').show();
			$('.qty-print-location').html(code);
			$('#qty-print').hide();
			$('.required').hide();

			$('#modal-print').modal({
				backdrop: 'static',
				keyboard: true
			});

		}else{

			$('.label-qty').html('Qty');

			if($('#datatable_ajax tbody input[type="checkbox"]').is(':checked')){

				locId = [];

				$('#datatable_ajax tbody input[type="checkbox"]').each(function(){
					var ctrl = $(this);

					if(ctrl.is(':checked'))
						locId.push(ctrl.val());

				});

				App.blockUI();

				$('#data-modal-form input[name="id"]').val('');
				$('.qty-print-location').html(locId.length);

				$('.qty-print-location').show();
				$('#qty-print').hide();
				$('.required').hide();

				$('#modal-print').modal({
					backdrop: 'static',
					keyboard: true
				});

				App.unblockUI();

			}else{

				App.blockUI();
				$.ajax({
					url: BaseUrl + page_class + '/getQtyPrint',
					type: 'POST',
					dataType: 'JSON',
					data: {

					},
					success: function(response){

						var data = response;
						$('#data-modal-form input[name="id"]').val('');
						$('.qty-print-location').html(data.qty);

						$('.qty-print-location').show();
						$('#qty-print').hide();
						$('.required').hide();

						$('#modal-print').modal({
							backdrop: 'static',
							keyboard: true
						});

						App.unblockUI();
					},
					error: function(response){
						App.unblockUI();
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});
			}
		}
	}

	var printBarcode = function(){

		var $btn = $('#submit-print');
			$btn.button('loading');

		var qty = $('.qty-print-location').html(),
			check = 0;

		if($('#data-modal-form input[name="id"]').val().length > 0){
			qty = $('.qty-print-location').html();
			check = 1;
		}

		if (!/\S/.test(qty)) {
			if(check == 0){
				$('.qty-print-group').addClass('has-error');
			}

			$btn.button('reset');
			toastr['error']("Qty print cannot empty and must be greater than 0","Error");
			return false;
		}else{
			if(qty <= 0){

				if(check == 0){
					$('.qty-print-group').addClass('has-error');
				}

				$btn.button('reset');
				toastr['error']("Qty print cannot empty and must be greater than 0","Error");
				return false;
			}
		}

		var params = 'multiple';
		if(check == 1)
			params = 'single';

		if($('#datatable_ajax tbody input[type="checkbox"]').is(':checked'))
			params = 'partial';

		postdata = {
			params: params,
			code: qty,
			loc_category_id: locId
		}

		$.ajax({
			url: BaseUrl + page_class + '/printZpl',
			type: 'POST',
			dataType: 'JSON',
			data: postdata,
			success: function(response){
				if(response.status == 'OK'){
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				}else
					bootbox.alert(response.message);

				setTimeout(function(){
					$btn.button('reset');
				}, 1000);
				//App.unblockUI();
				/*
				var zpl = response.zpl;

				var ctrl = $(zpl);

				var postdata = {
					sid: $('#sid').val(),
					pid: $('#pid').val(),
					printerCommands: zpl
				}
				// store user printer settings & commands in server cache
				$.post(BaseUrl + 'webclientprint/ProcessPrint.php',
					postdata,
					function() {
						 // Launch WCPP at the client side for printing...
						 var sessionId = $("#sid").val();
						 jsWebClientPrint.print('sid=' + sessionId);
					}
				);
				*/
			},
			error: function(response){
				$btn.button('reset');
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}


    var pageInteraction = function(){

		$('.data-table-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{

			});
		});

		$('#loc-type').select2();

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});

		$('.btn-print').on('click', function(){

			if(!$('#datatable_ajax tbody input').is(':checked')){
				toastr['error']('Please check at least one row to print', 'ERROR');
				return false;
			}

			showModalPrint();
		});

        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
        });

        // $('#data-table-filter').submit(function(e){
        //     e.preventDefault();
        //     data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
        //     data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
        //     data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
        //     data-table_datatable.getDataTable().ajax.reload();
        // });
        //
        // $('.hide-filter').click(function(e){
        //     e.preventDefault();
        //     $('#data-table-filter,.filter-show-control').hide();
        //     $('.filter-hide-control,.msg-filter').show();
        // });
        //
        // $('.show-filter').click(function(e){
        //     e.preventDefault();
        //     $('#data-table-filter,.filter-show-control').show();
        //     $('.filter-hide-control,.msg-filter').hide();
        // });
        //
        // $('.reset-filter').click(function(e){
        //     e.preventDefault();
        //     $('#data-table-filter input[name="nama_data-table"]').val('');
        //     $('#data-table-filter input[name="kota"]').val('');
        //     $('#data-table-filter textarea[name="alamat_data-table"]').val('');
        //     data-table_datatable.clearAjaxParams();
        //     data-table_datatable.getDataTable().ajax.reload();
        // });

        $('.data-table-add').click(function(e){
            e.preventDefault();

			$('.type-group').show();
			$('#loc-type').val('BINLOC').trigger('change');

            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });

		$(document).on('click','.data-table-print',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
			var rowdata = content_datatable.getDataTable().row($(this).parents('tr')).data();
            showModalPrint(id, rowdata[2]);
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl+page_class+'/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    console.log(json);
                    if(! json.no_data){
                        $('#referensi-lokasi-form input[name="id"]').val(json.loc_category_id);
                        $('#referensi-lokasi-form input[name="loc_category_name"]').val(json.loc_category_name);
                        $('#referensi-lokasi-form input[name="loc_category_desc"]').val(json.loc_category_desc);

						if(json.loc_type == 'TRANSIT' || json.loc_type == 'SENTOUT' ||
							json.loc_type == 'INBOUND' || json.loc_type == 'OUTBOUND' ||
							json.loc_type == 'QC' || json.loc_type == 'NG')
							$('.type-group').hide();
						else
							$('.type-group').show();

						$('#loc-type').val(json.loc_type).trigger('change');

                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want to remove data ?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+page_class+'/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.status == 'OK'){
                                toastr['success'](json.message,"SUCCESS");
                                content_datatable.getDataTable().ajax.reload();
                            }else{
                                toastr['error'](json.message,"ERROR");
                            }
                        }
                    });
                }
            });
        });
    }

    var handleFormSettings = function() {
        $('#referensi-lokasi-form').validate({
            debug: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                loc_category_name: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+page_class+'/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#referensi-lokasi-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Change data success.","Success");
                            }else{
                                toastr['success']("Save data success.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You dont have authority to add data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You dont have authority to change data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleRecords();
            pageInteraction();
            handleFormSettings();
            /*handleBootstrapSelect();
            handleDatePickers();

            */
        }

    };
}();

jQuery(document).ready(function() {
    Ref_lokasi_kategori.init();
});

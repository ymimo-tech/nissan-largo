
	'use strict';
	
	var Handler = {
		params: function(options){
			var handler = {
				initialize: function(){
					var self = this;
				},
				
				save: function(){
					var self = this;
					
					var pinjaman = 'N';
					if($('#pinjaman').is(':checked'))
						pinjaman = 'A';
					
					$.ajax({
						url: BaseUrl + 'setting/save',
						type: 'POST',
						dataType: 'JSON',
						data: {
							peminjaman: pinjaman
						},
						success: function(response){
							toastr['success'](response.message, 'SUCCESS');
						},
						error: function(response){
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
					});
				},
				
				pageInteraction: function(){
					var self = this;
					
					$('#form-action').on('submit', function(e){
						e.preventDefault();
						
						self.save();
					});
				},
				
				init: function(){
					var self = this;
					
					self.initialize();
					self.pageInteraction();
				}
			}
			
			return handler;
		}
	}
	
	$(document).ready(function(){
		var hdlr = Handler.params();
		hdlr.init();
	});
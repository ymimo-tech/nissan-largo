var setWarehouseHandler = function(){

    var initialize = function(){

        $('.actions').prepend('<a  class="btn default btn-sm data-table-add-warehouse min-width120" style="margin-right:4px;"><i class="fa fa-gear"></i> Add to Warehouse </a>');
        $('.actions').prepend('<a  class="btn default btn-sm data-table-remove-warehouse min-width120" style="margin-right:4px;"><i class="fa fa-gear"></i> Remove from Warehouse </a>');

    }

    var pageInteraction = function(){

        $('.data-table-add-warehouse').click(function(e){
            e.preventDefault();
            $('#form-add-warehouse').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $('.data-table-remove-warehouse').click(function(e){
            e.preventDefault();
            $('#form-remove-warehouse').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $('.form-close-add-warehouse').click(function(e){
            e.preventDefault();
            $('#form-add-warehouse').modal('hide');
        });

        $('.form-close-remove-warehouse').click(function(e){
            e.preventDefault();
            $('#form-remove-warehouse').modal('hide');
        });

        $("#add-warehouse-select, #remove-warehouse-select")
          .select2({
            multiple: true,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            minimumInputLength: 1,
            ajax: {
                url: BaseUrl + page_class +'/select2Options/warehouses/id/name',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (res) {
                            return {
                                text: res.text,
                                id: res.id
                            }
                        })
                    };
                }
            }
        });
    }

    var handleForm = function() {

        $('#data-table-form-add-warehouse').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: true,
            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error,element) {
                return true;
            },

            submitHandler: function(form) {

                var warehouses=$('#add-warehouse-select').val();
                var elements = [];
                $('#datatable_ajax tbody input[type="checkbox"]:checked').each(function(i){
                  elements[i] = $(this).val();
                });

                $('#data-table-form-add-warehouse #submit').button('loading');

                $('#data-table-form-add-warehouse').find('button[id="submit"]').attr('disabled',true);

                setTimeout(function(){
                    $('#data-table-form-add-warehouse').find('button[id="submit"]').removeAttr('disabled');
                }, 500)

                $.ajax({
                    url: BaseUrl + page_class + '/addToWarehouse', // point to server-side controller method
                    dataType: 'json', // what to expect back from the server
                    data: {
                        warehouses:warehouses,
                        elements:elements
                    },
                    type: 'post',
                    success: function(json) {

                        $('#data-table-form-add-warehouse #submit').button('reset');

                        if(json.status=='OK'){

                            toastr['success'](json.message,"Success");
                        }else{
                            toastr['error'](json.message,"Error Occured");
                        }

                        $('#form-add-warehouse').modal('hide');
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    },
                    error: function (err) {
                        console.log(err)
                    }
                }).done(function(json){
                    $('#data-table-form-add-warehouse #submit').button('reset');
                    location.reload();
                })

            }
        });

        $('#data-table-form-remove-warehouse').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: true,
            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error,element) {
                return true;
            },

            submitHandler: function(form) {

                var warehouses=$('#remove-warehouse-select').val();
                var elements = [];
                $('#datatable_ajax tbody input[type="checkbox"]:checked').each(function(i){
                  elements[i] = $(this).val();
                });

                $('#data-table-form-remove-warehouse #submit').button('loading');
                $('#data-table-form-remove-warehouse').find('button[id="submit"]').attr('disabled',true);

                setTimeout(function(){
                    $('#data-table-form-remove-warehouse').find('button[id="submit"]').removeAttr('disabled');
                }, 500)

                $.ajax({
                    url: BaseUrl + page_class + '/removeFromWarehouse', // point to server-side controller method
                    dataType: 'json', // what to expect back from the server
                    data: {
                        warehouses:warehouses,
                        elements:elements
                    },
                    type: 'post',
                    success: function(json) {

                        $('#data-table-form-add-warehouse #submit').button('reset');

                        if(json.status=='OK'){
                            toastr['success'](json.message,"Success");
                        }else{
                            toastr['error'](json.message,"Error Occured");
                        }

                        $('#form-remove-warehouse').modal('hide');
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    },
                    error: function (err) {
                        console.log(err)
                    }
                }).done(function(){
                    $('#data-table-form-remove-warehouse #submit').button('reset');
                    location.reload();
                })

            }
        });

    }

    return {
        init: function(){

            initialize();
            pageInteraction();
            handleForm();

        }
    };
}();

$(document).ready(function(){
    setWarehouseHandler.init()
})
var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dTable;

	var initialize = function(){
		$('#item, #status, #cat, #loc, #loc-type, #uom').select2();

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#groupby, #source, #periode').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		$('#periode').val('today').trigger('change');
	}

	var initTable = function(){

		App.blockUI({
			target: '#portlet-light'
		});

		var sourceType = $("#source").select2().find(":selected").data("type");

		$.ajax({
			url: BaseUrl + page_class + '/getMaterialIn',
			type: 'POST',
			dataType: 'JSON',
			data: {
				group_by: $('#groupby').val(),
				source_id: $('#source').val(),
				source_type: sourceType,
				from: $('#from').val(),
				to: $('#to').val(),
                periode:$('#periode').val(),
                item:$('#item').val(),
                sn:$('#sn').val(),
                warehouse : $('#warehouse-filter').val()
			},
			success: function(response){
				App.unblockUI('#portlet-light');

				if(response.status == 'OK'){

					if($('#groupby').val() == 'DATE')
						$('.group-by-title').html('Period');
					else
						$('.group-by-title').html('Source');

					var data = response.data,
						len = data.length,
						groupTpl = '<tr>\
										<td class="header-report-table"></td>\
										<td class="header-report-table" colspan="10"><strong>{{ group_header }}</strong></td>\
									</tr>',
						tpl = '<tr>\
									<td></td>\
									<td>{{ inbound_date }}</td>\
									<td>{{ inbound_doc }}</td>\
									<td>{{ source }}</td>\
									<td>{{ receiving_doc }}</td>\
									<td>{{ items }}</td>\
									<td><div align="right">{{ doc_qty }}</div></td>\
									<td><div align="right">{{ rec_qty }}</div></td>\
									<td><div align="right">{{ disc }}</div></td>\
									<td>{{ remark }}</td>\
									<td>{{ user }}</td>\
							   </tr>',
						row = '';

					if(len > 0){

						if($('#groupby').val() == 'DATE'){

							for(var i = 0; i < len; i++){

								var header = '',
									detail = '';

								header += groupTpl;
								header = header.replace(/{{ group_header }}/gi, Object.keys(data[i])[0]);

								var detailData = data[i][Object.keys(data[i])[0]],
									iLen = detailData.length;

								for(var j = 0; j < iLen; j++){
									detail += tpl;
									detail = detail.replace(/{{ inbound_date }}/gi, detailData[j].tgl_inbound);
									detail = detail.replace(/{{ inbound_doc }}/gi, detailData[j].kd_inbound);
									detail = detail.replace(/{{ source }}/gi, detailData[j].source);
									detail = detail.replace(/{{ receiving_doc }}/gi, detailData[j].kd_receiving);
									detail = detail.replace(/{{ items }}/gi, detailData[j].item);
									detail = detail.replace(/{{ doc_qty }}/gi, detailData[j].doc_qty + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ rec_qty }}/gi, detailData[j].rec_qty + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ disc }}/gi, Math.abs(detailData[j].disc) + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ remark }}/gi, detailData[j].remark);
									detail = detail.replace(/{{ user }}/gi, detailData[j].nama);
								}

								row += header;
								row += detail;
							}

							$('#table-list tbody').html(row);

						}else{

							for(var i = 0; i < len; i++){

								var header = '',
									detail = '';

								header += groupTpl;
								header = header.replace(/{{ group_header }}/gi, Object.keys(data[i])[0]);

								var detailData = data[i][Object.keys(data[i])[0]],
									iLen = detailData.length;

								for(var j = 0; j < iLen; j++){
									detail += tpl;
									detail = detail.replace(/{{ inbound_date }}/gi, detailData[j].tgl_inbound);
									detail = detail.replace(/{{ inbound_doc }}/gi, detailData[j].kd_inbound);
									detail = detail.replace(/{{ source }}/gi, detailData[j].source);
									detail = detail.replace(/{{ receiving_doc }}/gi, detailData[j].kd_receiving);
									detail = detail.replace(/{{ items }}/gi, detailData[j].item);
									detail = detail.replace(/{{ doc_qty }}/gi, detailData[j].doc_qty + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ rec_qty }}/gi, detailData[j].rec_qty + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ disc }}/gi, Math.abs(detailData[j].disc) + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ remark }}/gi, detailData[j].remark);
									detail = detail.replace(/{{ user }}/gi, detailData[j].nama);
								}

								row += header;
								row += detail;
							}

							$('#table-list tbody').html(row);
						}

					}else{

						$('#table-list tbody').html('<tr><td colspan="11"><div align="center">No data</div></td></tr>');

					}

				}else{
					toastr['error']('Request data error', 'ERROR');
				}
			},
			error: function(error){
				App.unblockUI('#portlet-light');
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var reset = function(){
		$("#groupby").val('DATE').trigger("change");
		$("#source").val(null).trigger("change");
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'today');
	}

    var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    	});

		$('.btn-export').on('click', function(){

			var sourceType = $("#source").select2().find(":selected").data("type");
			var sourceName = $("#source").select2().find(":selected").data("name");

			post(BaseUrl + page_class + '/export_material_in',
			{
				group_by: $('#groupby').val(),
				source_id: $('#source').val(),
				source_type: sourceType || '-- All --',
				source_name: sourceName || '-- All --',
				from: $('#from').val(),
				to: $('#to').val(),
                item:$('#item').val(),
                sn:$('#sn').val(),
                warehouse:$('#warehouse-filter').val()
			}, '', 'Y');
		});

        $('.btn-export-excel').on('click', function(){

			var sourceType = $("#source").select2().find(":selected").data("type");
			var sourceName = $("#source").select2().find(":selected").data("name");

			post(BaseUrl + page_class + '/export_excel',
			{
				group_by: $('#groupby').val(),
				source_id: $('#source').val(),
				source_type: sourceType || '-- All --',
				source_name: sourceName || '-- All --',
				from: $('#from').val(),
				to: $('#to').val(),
                item:$('#item').val(),
                sn:$('#sn').val(),
                warehouse:$('#warehouse-filter').val()
			}, '', 'Y');
		});

        $('.btn-export-detail').on('click', function(){

			var sourceType = $("#source").select2().find(":selected").data("type");
			var sourceName = $("#source").select2().find(":selected").data("name");

			post(BaseUrl + page_class + '/export_excel_detail',
			{
				group_by: $('#groupby').val(),
				source_id: $('#source').val(),
				source_type: sourceType || '-- All --',
				source_name: sourceName || '-- All --',
				from: $('#from').val(),
				to: $('#to').val(),
                item:$('#item').val(),
                sn:$('#sn').val(),
                warehouse:$('#warehouse-filter').val()
			}, '', 'Y');
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

		$("#rcv")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + page_class + '/getReceivingCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_receiving,
							text: item.kd_receiving
						});
					});
					return {
						results: results
					};
				}
			}
		});
    }

    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
	Handler.init();
});

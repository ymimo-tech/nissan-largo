var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dTable, bottom, dLoad;

    var dLength = 50;
    var dStart = 0;

	var initialize = function(){
		$('#item, #status, #cat, #loc, #loc-type, #uom').select2();

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#groupby, #destination, #periode').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		$('#periode').val('today').trigger('change');
	}

	var initTable = function(){

		App.blockUI();

		var dStart = $('#table-list tbody tr').length;

		var destType = $("#destination").select2().find(":selected").data("type");

		$.ajax({
			url: BaseUrl + page_class + '/getMaterialOut',
			type: 'POST',
			dataType: 'JSON',
			data: {
				group_by: $('#groupby').val(),
				destination_id: $('#destination').val(),
				destination_type: destType,
				from: $('#from').val(),
				to: $('#to').val(),
                periode:$('#periode').val(),
                item:$('#item').val(),
                sn:$('#sn').val(),
                warehouse : $('#warehouse-filter').val(),
                length : dLength,
                start  : dStart
			},
			success: function(response){

				if(response.status == 'OK'){

					if($('#groupby').val() == 'DATE')
						$('.group-by-title').html('Period');
					else
						$('.group-by-title').html('Destination');

					var data = response.data,
						len = data.length,
						groupTpl = '<tr>\
										<td class="header-report-table"></td>\
										<td class="header-report-table" colspan="10"><strong>{{ group_header }}</strong></td>\
									</tr>',
						tpl = '<tr>\
									<td></td>\
									<td>{{ outbound_date }}</td>\
									<td>{{ outbound_doc }}</td>\
									<td>{{ destination }}</td>\
									<td>{{ pl_doc }}</td>\
									<td>{{ items }}</td>\
									<td>{{ packing_number }}</td>\
									<td><div align="right">{{ doc_qty }}</div></td>\
									<td><div align="right">{{ load_qty }}</div></td>\
									<td>{{ remark }}</td>\
							   </tr>',
						row = '';

					if(len > 0){

						if($('#groupby').val() == 'DATE'){

							for(var i = 0; i < len; i++){

								var header = '',
									detail = '';

								header += groupTpl;
								header = header.replace(/{{ group_header }}/gi, Object.keys(data[i])[0]);

								var detailData = data[i][Object.keys(data[i])[0]],
									iLen = detailData.length;

								for(var j = 0; j < iLen; j++){
									detail += tpl;
									detail = detail.replace(/{{ outbound_date }}/gi, detailData[j].tgl_outbound);
									detail = detail.replace(/{{ outbound_doc }}/gi, detailData[j].kd_outbound);
									detail = detail.replace(/{{ destination }}/gi, detailData[j].destination);
									detail = detail.replace(/{{ pl_doc }}/gi, detailData[j].pl_name);
									detail = detail.replace(/{{ items }}/gi, detailData[j].item);
									detail = detail.replace(/{{ packing_number }}/gi, detailData[j].packing_number);
									detail = detail.replace(/{{ doc_qty }}/gi, detailData[j].doc_qty + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ load_qty }}/gi, detailData[j].load_qty + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ remark }}/gi, detailData[j].remark);
								}

								row += header;
								row += detail;
							}

							$('#table-list tbody').append(row);

						}else{

							for(var i = 0; i < len; i++){

								var header = '',
									detail = '';

								header += groupTpl;
								header = header.replace(/{{ group_header }}/gi, Object.keys(data[i])[0]);

								var detailData = data[i][Object.keys(data[i])[0]],
									iLen = detailData.length;

								for(var j = 0; j < iLen; j++){
									detail += tpl;
									detail = detail.replace(/{{ outbound_date }}/gi, detailData[j].tgl_outbound);
									detail = detail.replace(/{{ outbound_doc }}/gi, detailData[j].kd_outbound);
									detail = detail.replace(/{{ destination }}/gi, detailData[j].destination);
									detail = detail.replace(/{{ pl_doc }}/gi, detailData[j].pl_name);
									detail = detail.replace(/{{ items }}/gi, detailData[j].item);
									detail = detail.replace(/{{ packing_number }}/gi, detailData[j].packing_number);
									detail = detail.replace(/{{ doc_qty }}/gi, detailData[j].doc_qty + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ load_qty }}/gi, detailData[j].load_qty + ' ' + detailData[j].nama_satuan);
									detail = detail.replace(/{{ remark }}/gi, detailData[j].remark);
									detail = detail.replace(/{{ user }}/gi, detailData[j].nama);
								}

								row += header;
								row += detail;
							}

							$('#table-list tbody').append(row);
						}

						dLoad = true;
	
						App.unblockUI();

					}else{

						App.unblockUI();

						if(dStart == 0){
							$('#table-list tbody').append('<tr><td colspan="10"><div align="center">No data</div></td></tr>');
						}

						dLoad = false;

					}

				}else{
					toastr['error']('Request data error', 'ERROR');
				}
			},
			error: function(error){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var reset = function(){
		$("#groupby").val('DATE').trigger("change");
		$("#destination").val(null).trigger("change");
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'today');
	}

    var pageInteraction = function(){

		$(window).scroll(function() {
		   if(($(window).scrollTop() + $(window).height()) > ($(document).height() - 10)) {
		   		if(bottom==false){
		   			if(dLoad){
			   			initTable();
					   	bottom=true;
					}
		   		}
		   }else{
			   	bottom=false;
		   }
		});


    	$('#warehouse-filter').change(function(){
    		initTable();
    	});

		$('.btn-export').on('click', function(){

			var destType = $("#destination").select2().find(":selected").data("type");
			var destName = $("#destination").select2().find(":selected").data("name");

			post(BaseUrl + page_class + '/export_material_out',
			{
				group_by: $('#groupby').val(),
				destination_id: $('#destination').val(),
				destination_type: destType || '-- All --',
				destination_name: destName || '-- All --',
				from: $('#from').val(),
				to: $('#to').val(),
                item:$('#item').val(),
                sn:$('#sn').val(),
                warehouse:$('#warehouse-filter').val()
			}, '', 'Y');
		});

        $('.btn-export-excel').on('click', function(){

			var destType = $("#destination").select2().find(":selected").data("type");
			var destName = $("#destination").select2().find(":selected").data("name");

			post(BaseUrl + page_class + '/export_excel',
			{
				group_by: $('#groupby').val(),
				destination_id: $('#destination').val(),
				destination_type: destType || '-- All --',
				destination_name: destName || '-- All --',
				from: $('#from').val(),
				to: $('#to').val(),
                item:$('#item').val(),
                sn:$('#sn').val(),
                warehouse:$('#warehouse-filter').val()
			}, '', 'Y');
		});


        $('.btn-export-detail').on('click', function(){

			var destType = $("#destination").select2().find(":selected").data("type");
			var destName = $("#destination").select2().find(":selected").data("name");

			post(BaseUrl + page_class + '/export_excel_detail',
			{
				group_by: $('#groupby').val(),
				destination_id: $('#destination').val(),
				destination_type: destType || '-- All --',
				destination_name: destName || '-- All --',
				from: $('#from').val(),
				to: $('#to').val(),
                item:$('#item').val(),
                sn:$('#sn').val(),
                warehouse:$('#warehouse-filter').val()
			}, '', 'Y');
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

		$("#rcv")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + page_class + '/getReceivingCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_receiving,
							text: item.kd_receiving
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$("#item")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'material_in/getItemCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id,
							text: item.code
						});
					});
					return {
						results: results
					};
				}
			}
		});
    }

    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
	Handler.init();
});

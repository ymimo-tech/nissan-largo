var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
    
    var dataTable,param;

	var initialize = function(){
		$('#item').select2();	
		$('#location').select2();	
		initTable();
		initQty();
	}

	var initQty = function(){
		$.ajax({
			url:  BaseUrl + page_class + "/getWidget",
			type: 'POST',
			dataType: 'JSON',
			data:{
			},
			success: function(response){
				
				var rec = parseFloat(response.data['one_month']);
				if(rec > 0){
					rec = '<a href="javascript:;" data-params="receiving" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'+rec.toLocaleString()+'</a>';
				}
				
				var on = parseFloat(response.data['two_month']);
				if(on > 0){
					on = '<a href="javascript:;" data-params="onhand" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'+on.toLocaleString()+'</a>';
				}
				
				var spn = parseFloat(response.data['three_month']);
				if(spn > 0){
					spn = '<a href="javascript:;" data-params="allocated" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'+spn.toLocaleString()+'</a>';
				}
				
				var all = parseFloat(response.data['expired']);
				if(all > 0){
					all = '<a href="javascript:;" data-params="suspend" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'+all.toLocaleString()+'</a>';
				}

				$('.30d h1').html(rec);
				$('.30d span').html('30 Days');
				
				$('.60d h1').html(on);
				$('.60d span').html('60 Days');

				$('.120d h1').html(spn);
				$('.120d span').html('120 Days');

				$('.expired h1').html(all);
				$('.expired span').html('Expired');

			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}
	
	var initTable = function (params='') {

        dataTable = new Datatable();
		
        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
				$('#replenishment_loading').hide();
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" param) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + "expired_report/getList", // ajax source
					"data": {
						id_barang: $('#item').val(),
						loc_id: $('#location').val(),
						warehouse: $('#warehouse-filter').val(),
						params: params
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
                    // { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [5, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">'+data+'</div>');
						},
						"targets": [0]
					},
					{
						"render": function(data, type, row){
							return '<div align="center"><button class="btn green btn-xs detail" type="button">\
										Show Detail\
									</button></div>';
						},
						"targets": [6]
					}
/*					{
						"render": function(data, type, row){
							return ('<div align="right">'+data+'</div>');
						},
						"targets": [5,7]
					}*/
				]
            }
        });
    }

	var childTableTpl = function(data){
		var tpl = '<div style="width: 90%; margin: 0 auto; text-align: left;">\
				   </div>\
				   <table id="child-table-'+data[0]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
						<thead>\
							<tr>\
								<th><div align="center">No.</div></th>\
								<th><div align="center">Serial Number</div></th>\
								<th><div align="center">Qty</div></th>\
								<th><div align="center">Expired Date</div></th>\
							</tr>\
						</thead>\
						<tbody>\
						</tbody>\
				   </table>';
				   
		return tpl;
	}
	
	var initChildTable = function(data){
		childTable = new Datatable();		
		
        childTable.init({
            src: $("#child-table-"+data[0]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" param) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"language": {
					"infoEmpty": ""
				},
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getExpiredList", // ajax source
					"data": {
						kd_barang: data[1],
						tgl_exp: data[5]
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1,2,3]
					}
				]
            }
        });
	}
	
	var reset = function(){
		$("#item").val(null).trigger("change");
		$("#location").val(null).trigger("change");
	}
	
    var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    	})

		$('.30d').on('click', function(){
			param=30;
			initTable(param);
		});

		$('.60d').on('click', function(){
			param=60;
			initTable(param);
		});

		$('.120d').on('click', function(){
			param=120;
			initTable(param);
		});

		$('.expired').on('click', function(){
			param=0;
			initTable(param);
		});

		$('#table-list').on('click', '.detail', function(e){
			e.preventDefault();
			
			var tr = $(this).closest('tr');
			var row = dataTable.getDataTable().row( tr ); 
			
			if(tr.hasClass('shown')){
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
				
				childTable.getDataTable().destroy();
			}else{
				$('#table-list tbody tr').each(function(){
					var type = $(this).attr('role');
					
					//if(type === undefined){
						console.log('here');
						//$(this).hide();
						var r = dataTable.getDataTable().row( $(this) ); 
						r.child.hide();
						$(this).removeClass('shown');
					//}
				});
			
				// Open this row
				row.child( childTableTpl(row.data()) ).show();
				tr.addClass('shown');
			
				initChildTable(row.data());
			}
		});
		
		$('.btn-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{
				params: param,
				id_barang: $('#item').val(),
				loc_id: $('#location').val()
			}, '', 'Y');
		});

		$('.btn-export-excel').on('click', function(){
			post(BaseUrl + page_class + '/export_excel',
			{
				params: param,
				id_barang: $('#item').val(),
				loc_id: $('#location').val()
			}, '', 'Y');
		});
		
		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});
		
		$('#reset').on('click', function(){
			reset();
		});		
    }
        
    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };  
}();

jQuery(document).ready(function() {
	Handler.init();
});
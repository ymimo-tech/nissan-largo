var Ref_barang = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

    var content_datatable;
    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
				if(response.customGroupAction=='OK'){
					toastr['success'](response.customGroupActionMessage,"Success");
				}
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				/*
                "language": {
					"lengthMenu": "Display _MENU_ records",
					"info" : ", Display _START_ to _END_ of _TOTAL_ entries"
				},
				*/
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"bDestroy": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl+page_class+"/get_list", // ajax source
					"data":{
						id_barang: $('#item').val(),
						id_kategori_1: $('#cat-1').val(),
						id_kategori_2: $('#cat-2').val(),
						picking_strategy: $('#ship').val(),
						period: $('#period').val(),
                        preorder: $('#preorder').val(),
                        status: $('#status').val()
					}
                },
				"columns": [
					{ "orderable": false },
                    { "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    // { "orderable": true },
					{ "orderable": false }
				],
                "order": [
                    [1, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function(data, type, row){
							if(row[6] == 1){
								return 'BATCH';
							}else{
								return data
							}
						},
						"targets": [7]
					},
					{
						"render": function(data, type, row){
							if(row[6] == 1){
								return '<div align="center">-</div>';
							}else{
								return data
							}
						},
						"targets": [8]
					}
				]
				/*
                "order": [
                    [1, "ASC"]
                ],// set first column as a default sort by asc
				*/
            }
        });
    }

    var pageInteraction = function(){

      $('.modal select, #item, #item_subtitute, #parent_child, #cat-1, #cat-2, #ship, #period').select2();

      $("#locations")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'receiving/getLocationsRef',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_inbound,
							text: item.kd_inbound
						});
					});
					return {
						results: results
					};
				}
			}
		});

        $("#item")
          .select2({
            multiple: false,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            minimumInputLength: 3,
            ajax: {
                url: BaseUrl + 'referensi_barang/option',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });


        $("#item_subtitute")
          .select2({
            multiple: false,
            allowClear: true,
            width: null,
            placeholder: "-- All --",
            dropdownParent: $("#form-content"),
            minimumInputLength: 3,
            ajax: {
                url: BaseUrl + 'referensi_barang/option',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        var length = $('#length').val();
        var height = $('#height').val();
        var width = $('#width').val();

        volume = length*height*width;
        $('#volume').val(volume);

        $('#weight, #length, #height, #width').on('keydown', function(e){

            if(isNumeric(e)){

                return isNumeric(e);

            }else{
                if ( e.keyCode == 190 ) {
                    return e.key;
                }
            }

            return false;
        });

        $('#length, #height, #width').on("keyup", function() {

            var volume = 0;
            var length = $('#length').val();
            var height = $('#height').val();
            var width = $('#width').val();

            volume = length*height*width;
            $('#volume').val(volume);

        });

		$('#min').on('keydown', function(e){

            return isNumeric(e);

		});

        $('#min_reorder').on('keydown', function(e){
            return isNumeric(e);
        });

        $("#preorder")
            .select2({
                placeholder: "-- All --",
                allowClear:true,
                data: [
                    {id:0,text:'No'},
                    {id:1,text:'Yes'}
                ]
            });
		
		$("#status")
            .select2({
                placeholder: "-- All --",
                allowClear:true,
                data: [
                    {id:'ACTIVE',text:'ACTIVE'},
                    {id:'DEACTIVE',text:'DEACTIVE'}
                ]
            });

        $('#preorder').val(null).trigger('change');
        $('#status').val(null).trigger('change');

		// $('#batch').on('change', function(){
		// 	var ctrl = $(this)

		// 	if(ctrl.is(':checked'))
		// 		$('.strategy').hide();
		// 	else
		// 		$('.strategy').show();
		// });

		// $('.modal select, #item, #item_subtitute, #parent_child, #cat-1, #cat-2, #ship, #period').select2();

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			handleRecords();
		});

		$('#reset').on('click', function(){
            $('#preorder').val(null).trigger('change');
            $('#status').val(null).trigger('change');
			$('#item').val(null).trigger('change');
			$('#cat-1').val(null).trigger('change');
			$('#cat-2').val(null).trigger('change');
			$('#ship').val(null).trigger('change');
			$('#period').val(null).trigger('change');
		});

		$('.data-table-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{

			});
		});

		$("#form-content").on("shown.bs.modal", function () {

		});

        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
        });

        $('#data-table-filter').submit(function(e){
            e.preventDefault();
            data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
            data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
            data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.hide-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').hide();
            $('.filter-hide-control,.msg-filter').show();
        });
        $('.show-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').show();
            $('.filter-hide-control,.msg-filter').hide();
        });
        $('.reset-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter input[name="nama_data-table"]').val('');
            $('#data-table-filter input[name="kota"]').val('');
            $('#data-table-filter textarea[name="alamat_data-table"]').val('');
            data-table_datatable.clearAjaxParams();
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.data-table-add').click(function(e){
            e.preventDefault();


            resetForm();

            $('#warehouse').val(null).trigger('change');

            $('input[name="convert"]').val(1);

            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });


        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            $.ajax({
                type:'POST',
                url:BaseUrl+'referensi_barang/get_data',
                data:{'id':id},
                dataType:'json',
                success:function(json){
                    console.log(json);
                    if(! json.no_data){

                        $('#referensi-barang-form input[name="id"]').val(json.id_barang);
                        $('#referensi-barang-form input[name="sku"]').val(json.sku);
                        $('#referensi-barang-form input[name="kd_barang"]').val(json.kd_barang);
                        $('#referensi-barang-form input[name="ori_kd_barang"]').val(json.kd_barang);
                        $('#referensi-barang-form input[name="nama_barang"]').val(json.nama_barang);
                        $('#referensi-barang-form select[name="id_kategori_1"]').val(json.id_kategori).trigger('change');
						$('#referensi-barang-form select[name="id_kategori_2"]').val(json.id_kategori_2).trigger('change');
                        $('#referensi-barang-form select[name="id_satuan"]').val(json.id_satuan).trigger('change');
                        $('#referensi-barang-form input[name="tipe_barang"]').val(json.tipe_barang);
                        $('#referensi-barang-form select[name="shipment_type"]').val(json.shipment_type_code).trigger('change');
                        $('#referensi-barang-form select[name="fifo_period"]').val(json.fifo_period).trigger('change');
                        $('#referensi-barang-form select[name="id_supplier"]').val(json.id_owner).trigger('change');
                        //$('#referensi-barang-form input[name="base_qty"]').val(json.base_qty);
						$('#referensi-barang-form input[name="DefQty"]').val(json.def_qty);
                        $('#referensi-barang-form #child_satuan').val(json.child_satuan).trigger('change');
						$('#referensi-barang-form #min').val(json.minimum_stock);
                        $('#referensi-barang-form #min_reorder').val(json.min_reorder);

                        $('#warehouse').val(null).trigger('change');
                        $('#warehouse').val(json.warehouses).trigger('change');
						
                        $('#item_area').val(json.item_area);

                        $('#parent_child').val(json.parent_child).trigger('change');
                        // $('#item_subtitute').val(null).trigger('change');
                        // $('#item_subtitute').val(json.id_item_subtitute).trigger('change');
                        $('#item_subtitute').select2('data', {id : json.id_item_subtitute});

                        //
                            $('input[name="convert"]').val(1);
                            $('input[name="convert_2"]').val(json.convert_qty_1);
                            $('select[name="unit_id_2"]').val(json.convert_qty_unit_id_1).trigger('change');
                            $('input[name="convert_3"]').val(json.convert_qty_2);
                            $('select[name="unit_id_3"]').val(json.convert_qty_unit_id_2).trigger('change');
                        //


                        $('#weight').val(json.weight);
                        $('#length').val(json.length);
                        $('#width').val(json.width);
                        $('#height').val(json.height);

                        var length = $('#length').val();
                        var height = $('#height').val();
                        var width = $('#width').val();

                        volume = length*height*width;
                        $('#volume').val(volume);


                        if(json.preorder == 1){
                            $('#preorder').prop('checked', true);
                            $('#preorder').parent().addClass('checked');
                        }else{
                            $('#preorder').removeAttr('checked');
                            $('#preorder').parent().removeClass('checked');
                        }

                        if(json.has_batch == 1){
                            $('#batch').prop('checked', true);
                            $('#batch').parent().addClass('checked');

                            //$('.strategy').hide();
                        }else{
                            $('#batch').removeAttr('checked');
                            $('#batch').parent().removeClass('checked');

                            //$('.strategy').show();
                        }

                        if(json.has_expdate == 1){
                            $('#expdate').prop('checked', true);
                            $('#expdate').parent().addClass('checked');
                        }else{
                            $('#expdate').removeAttr('checked');
                            $('#expdate').parent().removeClass('checked');
                        }

                        if(json.has_qty == 1){
                            $('#multiQty').prop('checked', true);
                            $('#multiQty').parent().addClass('checked');
                        }else{
                            $('#multiQty').removeAttr('checked');
                            $('#multiQty').parent().removeClass('checked');
                        }

                        if (json.autorelease == 1) {
                            $('#autorelease').prop('checked', true);
                            $('#autorelease').parent().addClass('checked');
                        } else {
                            $('#autorelease').removeAttr('checked', true);
                            $('#autorelease').parent().removeClass('checked');
                        }

                        if (json.quarantine == 1) {
                            $('#quarantine').prop('checked', true);
                            $('#quarantine').parent().addClass('checked');
                            $('#referensi-barang-form input[name="quarantineduration"]').val(json.quarantineduration);
                            $('#referensi-barang-form #duration').attr("style", "display:block");
                        } else {
                            $('#quarantine').removeAttr('checked', true);
                            $('#quarantine').parent().removeClass('checked');
                            $('#referensi-barang-form input[name="quarantineduration"]').val();
                            $('#referensi-barang-form #duration').attr("style", "display:none");
                        }

                        $('#form-content').modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                    }else{
                        toastr['error']("Could not load data.","Error");
                    }
                }
            });
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want to remove data ?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'referensi_barang/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){

                            if(json.status == 'OK'){

                                toastr['success'](json.message,"Success");
                                content_datatable.getDataTable().ajax.reload();

                            }else{

                                if(json.no_delete){
                                    toastr['warning']("You dont have authority to delete data.","Warning");
                                }else{
                                    toastr['error'](json.message, "Error Occured");
                                }

                            }

                        }
                    });
                }
            });
        });
        $(document).on('click','.btn-sync',function(){

            var bsync = $(this);
            bsync.attr('disabled',true);
            bsync.prop('disabled',true);
            bsync.html('<i class="fa fa-refresh"></i> Please wait');

            $.ajax({
    			url: BaseUrl + 'dashboard/manual_sync',
    			type: 'POST',
    			dataType: 'JSON',
    			success: function(json){
                    if(json.newc.item.length>0){
                        $.each(json.newc.item, function (i,newc){
                            if(newc.newrec){
                                var title="New Item";
                            }else{
                                var title="Item Updated";
                            }
                            toastr['info']("<a href='"+BaseUrl+"referensi_barang' target='_blank'>"+newc.sku+"</a><br/>"+newc.nama,title);
                        });
                        content_datatable.getDataTable().ajax.reload();
                    }

                    bsync.attr('disabled',false);
                    bsync.prop('disabled',false);
                    bsync.html('<i class="fa fa-refresh"></i> SYNC');

    			}
    		});
        });
    }

    $.validator.addMethod("positive", function(value, element) {
		return this.optional(element) || $('#multiQty').is(':checked')?parseInt(value) > 0:true;
	});

    var handleFormSettings = function() {
        $('#referensi-barang-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                kd_barang: {
                    required: true
                },
                nama_barang: {
                    required: true
                },
				/*
                tipe_barang: {
                    required: true
                },
				*/
				base_qty: {
                    required: true
                },
               /* owner_name: {
                    required: true
                }*/
                DefQty:{
                    number:true,
                    positive:true
                },
                min:{
                    number:true
                    // positive:true
                },
                min_reorder:{
                    number:true
                    // positive:true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {

				// var postdata = {
					// kd_barang: $('#referensi-barang-form input[name="kd_barang"]').val(),
					// nama_barang: $('#referensi-barang-form input[name="nama_barang"]').val(),
					// id_kategori: $('#referensi-barang-form select[name="id_kategori_1"]').val(),
					// id_kategori_2: $('#referensi-barang-form select[name="id_kategori_2"]').val(),
					// id_satuan: $('#referensi-barang-form select[name="id_satuan"]').val(),
					// shipment_type: $('#referensi-barang-form select[name="shipment_type"]').val(),
					// fifo_period: $('#referensi-barang-form select[name="fifo_period"]').val(),
					// id_owner: $('#referensi-barang-form select[name="id_owner"]').val(),
					// base_qty: $('#referensi-barang-form input[name="base_qty"]').val(),
					// child_satuan: $('#referensi-barang-form #child_satuan"]').val()
				// }

				var postdata = $(form).serialize();

                if($('#preorder').is(':checked'))
                    postdata = postdata + '&preorder=1';

				if($('#batch').is(':checked'))
                    postdata = postdata + '&batch=1';

                if ($('#quarantine').is(':checked'))
                    postdata = postdata + '&quarantine=1';

                if ($('#autorelease').is(':checked'))
                    postdata = postdata + '&autorelease=1';

				if($('#expdate').is(':checked'))
					postdata = postdata + '&expdate=1';

                if($('#multiQty').is(':checked')){
                    postdata = postdata + '&multiQty=1';
                    //alert('test');
                    //$('#DefQty').attr('disabled', false);
				}
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'referensi_barang/proses',
                    dataType:'json',
                    data: postdata,
					//data: postdata,
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#referensi-barang-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.status == 'OK'){
                            if(json.edit == 1){
                                toastr['success']("Change data success.","Success");
                            }else{
                                toastr['success']("Save data success.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You dont have authority to add data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You dont have authority to change data.","Warning");
                            }else if(json.no_location){
                                toastr['warning']("The location is not registered!","Warning");
                            }else{
								toastr['error'](json.message,"Error Occured");
                                //toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    var longpolling = function(){
        function start_longpolling(){
            var evtSource = new EventSource(BaseUrl+'referensi_barang/longpolling');
    		evtSource.addEventListener("sync", function(e) {
    	      var obj = JSON.parse(e.data);
              if(obj.newc.length>0){
        	      $.each(obj.newc, function (i,newc){
         			toastr['info']("<a href='"+BaseUrl+"referensi_barang' target='_blank'>"+newc.sku+"</a><br/>"+newc.nama,"New Item");
        	      });
                  //content_datatable.getDataTable().ajax.reload();
                  evtSource.close();
                  start_longpolling();
              }
    	    }, false);
        }

        start_longpolling();
    }

    var warehouseOptionHandler = function(){

        var row = '<option value="all">Select All</option>';
        row = row + $('#warehouse').html();

        $('#warehouse').html(row);

        $('#warehouse').select2();

        $('#warehouse').on('change', function(e){

            var ctrl = $(this);

            var allOption = $('#warehouse option').length;
            var selectedOption = $('#warehouse option:selected').length;

            if($.inArray('all',ctrl.val()) != -1){
                if(allOption != selectedOption){
                    $('#warehouse option').each(function(){
                        $(this).prop('selected',true);
                    })
                    $('#warehouse option[value="all"]').text('Deselect All');
                    $('#warehouse option[value="all"]').prop('selected',false);

                }else{

                    $('#warehouse option').each(function(){
                        $(this).prop('selected',false);
                    })
                    $('#warehouse option[value="all"]').text('Select All');

                }

            }
            else
            {

                if((allOption-1) == selectedOption){

                    $('#warehouse option[value="all"]').text('Deselect All');

                }else{

                    $('#warehouse option[value="all"]').text('Select All');

                }

            }
            $(this).trigger('change.select2');

        });

        $('#warehouse').on('select2:open', function(e){

            e.preventDefault();

            var allOption = $('#warehouse option').length;
            var selectedOption = $('#warehouse option:selected').length;
            if((allOption-1) == selectedOption){
                $('#select2-warehouse-results li[id$="all"]').text('Deselect All');
            }else{
                $('#select2-warehouse-results li[id$="all"]').text('Select All');
            }
        });

    }

    var convertionHandler = function(){

        $('input[name="convert"]').val(1);

        $('select[name="id_satuan"]').change(function(){
            $('select[name="unit_id"]').val($(this).val()).trigger('change');
        })

    }

    var resetForm = function(){

        $('input[name="convert"]').val(1);
        $('input[name="convert_2"]').val('');
        $('select[name="unit_id_2"]').val(null).trigger('change');
        $('input[name="convert_3"]').val('');
        $('select[name="unit_id_3"]').val(null).trigger('change');

    }

	return {

        //main function to initiate the module
        init: function () {
            handleRecords();
            pageInteraction();
            handleFormSettings();
            warehouseOptionHandler();
            convertionHandler();
            /*handleBootstrapSelect();
            handleDatePickers();

            */
            //longpolling();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

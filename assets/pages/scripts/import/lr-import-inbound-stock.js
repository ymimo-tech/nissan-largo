var importHandlerInboundStock = function(){

    var pageInteraction = function(){

        $('.data-table-import-inbound-stock').click(function(e){
            e.preventDefault();
            $('#form-import-inbound-stock').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $('.form-close-import-inbound-stock').click(function(e){
            e.preventDefault();
            $('#form-import-inbound-stock').modal('hide');
        });

        $('#download-template-inbound-stock').click(function(e){

            e.preventDefault();
            $.get(BaseUrl+page_class+"/download_template_inbound_stock", function(response){
                if(response.status){
                    window.location = BaseUrl+"assets/template/"+response.filename;
                }else{
                    toastr['error'](response.message,"Error !");
                }
            },"json").fail(function(){
                toastr['error']("Please refresh this page !","Error !");
            });

        });

        $('#warehouse-inbound-stock').select2();


    }

    var handleForm = function() {

        $('.actions').prepend('<a  class="btn red btn-sm data-table-import-inbound-stock min-width120" style="margin-right:4px;"><i class="fa fa-upload"></i> IMPORT INBOUND STOCK </a>');

        $('#data-table-form-import-inbound-stock').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: true,
            rules: {
                inb_doc_file: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            
            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },
            
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error,element) {
                return true;
            },

            submitHandler: function(form) {

                $('#data-table-form-import-inbound-stock').find('button[id="submit"]').attr('disabled',true);
                setTimeout(function(){
                    $('#data-table-form-import-inbound-stock').find('button[id="submit"]').removeAttr('disabled');
                }, 500);

                var file_data = $('#file-inbound-stock').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('warehouse', $('#warehouse-inbound-stock').val());
                $.ajax({
                    url: BaseUrl + page_class + '/importFromExcelInboundStock', // point to server-side controller method
                    dataType: 'json', // what to expect back from the server
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(json) {

                        if(json.status){

                            toastr['success'](json.message,"Success");
                            setTimeout(function(){
                                location.reload();
                            }, 1000);

                            $('#form-import-inbound-stock').modal('hide');
                        }else{
                            toastr['error'](json.message,"Error Occured");
                        }

                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    },
                    error: function (err) {
                        console.log(err)
                    }
                });

            }
        });
    }



    return {
        init: function(){

            handleForm();
            pageInteraction();

        }
    };
}();

$(document).ready(function(){
    importHandlerInboundStock.init()
})
var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dTable;

	var initialize = function(){
		initTable();
	}

	var initTable = function () {

        dTable = new Datatable();

        dTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
                current_filter=response.current_filter;
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl+page_class+"/getSapLog", // ajax source
					"data": {
						docRef: $('#doc-reference').val(),
						docType: $('#doc-type').val(),
                        warehouse:$('#warehouse-filter').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true }
                  ],
                "order": [
                    [0, "desc"]
                ]// set first column as a default sort by asc
			}
		});
	}

	var pageInteraction = function(){
		// Page Interaction

        $(document).on('click','.data-table-retry',function(e){
            e.preventDefault();

            var ctrl = $(this),
                refId = ctrl.attr('data-id'),
                refCode = ctrl.attr('data-code');

            if(ctrl.hasClass('disabled-link'))
                return false;

            retry(refId,refCode);

        })
	}

    var retry = function(refId,refCode){
        $.ajax({
            url:BaseUrl+page_class+"/retry",
            type: 'POST',
            dataType: 'JSON',
            data:{
                refId:refId,
                refCode:refCode
            },
            success: function(response){
                initTable();
            },
            error: function(err){
                initTable();
            }
        })

    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

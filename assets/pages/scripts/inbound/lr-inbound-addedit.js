var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var content_datatable,
		idSource,
		idWarehouse,
		idDocument;
	var idInbound = null;

	var initialize = function(){

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: moment().format('DD/MM/YYYY')
		});

		var id = $('input[name="id"]').val();
		if(id.length > 0)
			edit(id);

		$('#data-table-form select[name="id_supplier"], #data-table-form select[name="id_inbound_document"], #warehouse-form').select2();

		if($('#data-table-form select[name="id_inbound_status"]') != 'undefined'){
			$('#data-table-form select[name="id_inbound_status"]').select2();
		}

		$('#source').select2();

		if($('#outbound-peminjaman').length > 0)
			$('#outbound-peminjaman').select2();
	}

	var reset = function(){
		$('#data-table-form input[name="id"]').val('');
		$('#data-table-form input[name="params"]').val('');
		$('#data-table-form input[name="kd_inbound"]').val('');
    	$('.inbound-code').text(' -- ');
		$('#data-table-form input[name="tanggal_inbound"]').val(moment().format('DD/MM/YYYY'));
		$('#remark').val('');
		$('#data-table-form select[name="source"]').val(null).trigger("change");
		$('#data-table-form select[name="id_inbound_document"]').val(null).trigger("change");
		$('#data-table-form #added_rows').html('');

		$('#data-table-form input[name="kd_inbound"]').focus();
	}

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					idInbound = json.id_inbound;
					idWarehouse = json.warehouse_id;
					idDocument = json.id_inbound_document;
					idSource = json.id_supplier;

					$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('input[name="kd_inbound"]').val(json.kd_inbound);
					$('.inbound-code').html(json.kd_inbound);
					//$('#data-table-form input[name="tanggal_inbound"]').val(json.tanggal_inbound);
					//$('#data-table-form input[name="tanggal_inbound"]').datepicker("update", json.tanggal_inbound);
					//$('.date-picker').datepicker("update", json.tanggal_inbound);
					//$('#data-table-form select[name="id_supplier"]').val(json.id_supplier);
					$('#data-table-form select[name="id_inbound_document"]').val(json.id_inbound_document).trigger("change");
					$('#data-table-form select[name="id_inbound_document"]').attr('disabled', '');
					$('#data-table-form select[name="id_inbound_status"]').val(json.id_status_inbound).trigger("change");
					$('#data-table-form textarea[name="remark"]').val(json.remark).trigger("change");

					$('#data-table-form #added_rows').append(json.inbound_barang_html);

					if(json.id_status_inbound == 2){
						$('#save').remove();
					}

					if(json.id_outbound != null)
						$('#outbound-peminjaman').val(json.id_outbound).trigger('change');

					$('.date-picker').datepicker('remove');

					$('.date-picker').datepicker({
						orientation: "left",
						format: 'dd/mm/yyyy',
						autoclose: true,
						startDate: json.tanggal_inbound
					});

					$('.date-picker').datepicker("update", json.tanggal_inbound);

					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

					//$(".selectpicker").selectpicker("refresh");

					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});

					var j = 0;
					$('.added_row').each(function(){
						console.log('heress');
						$(this).find('select.add_rule').attr('name', 'id_barang['+j+']');
						$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

						j++;
					});

					if($('#data-table-form input[name="not_used"]').val() == 'true'){

						$(".remove_row").click(function(){
							$(this).closest("tr").remove();

							var j = 0;
							$('.added_row').each(function(){

								$(this).find('select.add_rule').attr('name', 'id_barang['+j+']');
								$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

								j++;
							});

						});

					}else{
						$('select[name="id_barang[]"]').attr('disabled', '');
						$('input[name="item_quantity[]"]').attr('disabled', '');
						$('.btn-delete').hide();
						$('.add_item').parent('tr').hide();
					}

				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getSource = function(destination_id=''){
		$.ajax({
			url: BaseUrl + page_class + '/getSource',
			type: 'POST',
			dataType: 'JSON',
			data: {
				document_id:$('#data-table-form select[name="id_inbound_document"]').val(),
				destination_id:destination_id
			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id);
						row = row.replace(/{{ text }}/gi, data[i].text);
					}

					$('#source').select2('destroy');

					$('#source').html(row);
					$('#source').select2();

					$('#source').val(idSource).trigger('change');

				}else{
					toastr['error']("Get source list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getCustomer = function(){
		$.ajax({
			url: BaseUrl + page_class + '/getCustomer',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_customer);
						row = row.replace(/{{ text }}/gi, data[i].customer_code + ' - ' +data[i].customer_name);
					}

					$('#source').select2('destroy');

					$('#source').html(row);
					$('#source').select2();

					$('#source').val(idSource).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getSupplier = function(){
		$.ajax({
			url: BaseUrl + page_class + '/getSupplier',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_supplier);
						row = row.replace(/{{ text }}/gi, data[i].kd_supplier + ' - ' + data[i].nama_supplier);
					}

					$('#source').select2('destroy');

					$('#source').html(row);
					$('#source').select2();

					$('#source').val(idSource).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getDetailOutbound = function(id){

		$.ajax({
			url: BaseUrl + page_class + '/getDetailOutbound',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_outbound: id
			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response,
						len = data.brg.length;

					$('#source').val(data.id_customer).trigger('change');

					$('.added_row').remove();

					for(var i = 0; i < len; i++){
						$('.add_item').trigger('click');
					}

					setTimeout(function(){
						var i = 0;

						$('.added_row').each(function(){

							$(this).find('select').val(data.brg[i].id_barang).trigger('change');
							$(this).find('input').val(data.brg[i].jumlah_barang);

							i++;
						});
					}, 500);

				}else{
					toastr['error']("Get outbound detail failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    var pageInteraction = function(){

		$('.inbound-code').text(' -- ');

		$('#outbound-peminjaman').on('change', function(){
			var ctrl = $(this),
				val = ctrl.val();

			if($('#data-table-form select[name="id_inbound_document"]').val() == 7 && $('input[name="id"]').val().length <= 0)
				getDetailOutbound(val);
		});

		$('#data-table-form'). keydown(function (e) {
			if (e. keyCode == 13) {
				e. preventDefault();
				return false;
			}
		});

		getInboundDocType();
		$('#warehouse-filter').change(function(){
			getInboundDocType();
		});

		$(document).on('change', 'select.add_rule', function(){

			var ctrl = $(this);

			if(ctrl.val() > 0){
				$.ajax({
					url: BaseUrl + page_class + '/getUomByItem',
					type: 'POST',
					dataType: 'JSON',
					data: {
						id_barang: $(this).val()
					},
					success: function(response){
						ctrl.parent().parent().find('label.uom').html(response.nama_satuan);
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});
			}
		});

		$(document).on('change', '#data-table-form select.add_rule', function(){
			$(this).closest('.form-group').removeClass('has-error');
		});

		$(document).on('keydown', '.add_rule', function(e){
			return isNumeric(e);
		});

		$('#save-new').on('click', function(){
			$('input[name="params"]').val('new');
			$('#data-table-form').submit();
		});

		/*
        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
		*/

        $(".add_item").click(function(e){
			var seq = $('.added_row').length;
            $.ajax({
                url:BaseUrl + page_class + "/get_add_item_row/" + seq,
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows").append("<tr id='target_new_row'><td colspan='4'>Please Wait</td></tr>");
                },
                success:function(html){

					var seq = $('.added_row').length;
					var r = html.replace('id_barang[]', 'id_barang['+seq+']');
						r = html.replace('item_quantity[]', 'item_quantity['+seq+']');

                    $("#target_new_row").replaceWith(r);
                    $(".selectpicker").selectpicker("refresh");
                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();

						var j = 0;
						$('.added_row').each(function(){
							$(this).find('select.add_rule').attr('name', 'id_barang['+j+']');
							$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

							j++;
						});

                    });

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

                    $('#data-table-form select.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                    $('#data-table-form input.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                }
            });
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {

            $('.mdatepicker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

    }

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_inbound: {
                    required: true
                },
                tanggal_inbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_inbound_document: {
                    required: true
                },
				outbound_peminjaman: {
                    required: true
                },
				source: {
                    required: true
                },
                warehouse:{
                	required:true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
				submitFormHandler(form);
            }
        });
    }

    var submitFormHandler = function(form){

		var chk = true;
		var itemId = [];

		if($('#tanggal-inbound').val().length != 10){
			$('.date-picker').addClass('has-error');
			chk = false;
		}

		if($('#added_rows tr').length <= 0){
			toastr['error']("Please add item first", "ERROR");
			return false;
		}

		$('#data-table-form select.add_rule').each(function(){
			var ctrl = $(this),
				val = ctrl.val();

			itemId.push(val);

			if(!/\S/.test(val)) {
				ctrl.closest('.form-group').addClass('has-error');
				chk = false;
			}

			if(val == 'undefined' || val == '') {
				ctrl.closest('.form-group').addClass('has-error');
				chk = false;
			}
		});

		$('#inbound-barang input.add_rule').each(function(){
			var ctrl = $(this),
				val = ctrl.val();
        console.log(val);

			if(!/\S/.test(val)) {
				ctrl.closest('.form-group').addClass('has-error');
				chk = false;
			}

			var id = $('input[name="id"]').val();
			if(id.length > 0){
				if(val <= 0) {
					ctrl.closest('.form-group').addClass('has-error');
					chk = false;
				}
			}
		});

		// if($('#data-table-form select[name="id_inbound_document"]').val() != 1){
		// 	$('.cost_center').each(function(){
		// 		var ctrl = $(this),
		// 			val = ctrl.val();

		// 		if(!/\S/.test(val)) {
		// 			ctrl.closest('.form-group').addClass('has-error');
		// 			chk = false;
		// 		}

		// 		if(val == "") {
		// 			chk = false;
		// 		}

		// 	})
		// }

		if(!chk){
			toastr['error']("Please check again.","Form Error");
			return false;
		}

		var hasDuplicate = !itemId.every(function(v,i) {
			return itemId.indexOf(v) == i;
		});

		if(hasDuplicate){

			$('#data-table-form select.add_rule').each(function(index1, item1){

				$.each($('#data-table-form select.add_rule').not(this), function (index2, item2) {

					if ($(item1).val() == $(item2).val()) {
						$(item1).closest('.form-group').addClass('has-error');
					}

				});

			});

			toastr['error']("Duplicate item. Please fix it first","Form Error");
			return false;
		}

		App.blockUI();

        $.ajax({
			url: BaseUrl + page_class + '/checkInboundDocNumberExist',
			type: 'POST',
			dataType: 'JSON',
			data: {
				po: $('#data-table-form input[name="kd_inbound"]').val(),
				id_inbound: $('#data-table-form input[name="id"]').val()
			},
			success: function(response){
				if(response.status == 'OK'){

					save(form);

				}else{
					App.unblockUI();
					toastr['error'](response.message, 'ERROR');

					updateInboundDocumentNumber();
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
    }

	var save = function(form){

		var disabled = $(form).find(':input:disabled').removeAttr('disabled');
		var serialize = $(form).serialize();
		disabled.attr('disabled','disabled');

		$.ajax({
			type:'POST',
			url: BaseUrl + page_class + '/proses',
			dataType:'json',
			data: serialize,
			beforeSend:function(){
				//$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
				//$('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
				$('.form-group').removeClass('has-error');
			},
			success:function(json){
				if(json.success==1){
					if(json.edit==1){
						toastr['success']("Save changes success.","Success");
					}else{
						toastr['success']("Save success.","Success");
					}

					var params = $('input[name="params"]').val();

					if(params == 'new'){
						reset();
					}else{
						setTimeout(function(){
							location.href = BaseUrl + page_class;
						}, 500);
					}
					//content_datatable.getDataTable().ajax.reload();
					//$('#form-content').modal('hide');
				}else{
					if(json.no_add){
						toastr['warning']("You dont have autority to add data.","Warning");
					}else if(json.no_edit){
						toastr['warning']("You dont have autority to change data..","Warning");
					}else{
						toastr['error']("Please refresh the page and try again later.","Error Occured");
					}
				}
				App.unblockUI();
				//$('.modal-content').unblock();
				//$('.spinner').remove();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getInboundDocument = function(id){
		if(id !== ""){
			$.ajax({
				url: BaseUrl + page_class + '/getInDocument',
				type: 'POST',
				dataType: 'JSON',
				data: {
					id:id
				},
				success: function(response){
					if(response.status){
						$('input[name="kd_inbound"]').val(response.result);
						$('.inbound-code').text(response.result);
					}else{
						toastr['error'](response.message,"Error");
					}
					App.unblockUI();
				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401){
						location.href = BaseUrl + 'login';
					}
				}
			});
		}else{
			$('input[name="kd_inbound"]').val('');
			$('.inbound-code').text(' -- ');
		}
	}

	var getInboundDocType = function(){

		$.ajax({
			url : BaseUrl + page_class +'/getInboundDocument',
			type: 'POST',
			dataType: 'JSON',
			data:{
				warehouse: $('#warehouse-filter').val()
			},
			success: function(response){
				if(response.response){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}" auto-generate="{{ type }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].document_id);
						row = row.replace(/{{ text }}/gi, data[i].document_name);
						row = row.replace(/{{ type }}/gi, data[i].is_auto_generate);
					}

					$('select[name="id_inbound_document"]').attr('data-placeholder'," -- Please Select --");
					$('select[name="id_inbound_document"]').html(row);
					$('select[name="id_inbound_document"]').select2();

					$('select[name="id_inbound_document"]').val(idDocument).trigger('change');

				}else{
					toastr['error'](response.message,"Error");
				}
			}
		});

	}

	var getWarehouse = function(){

		var warehouseId = $('#warehouse-filter').val();
		var documentId = $('select[name="id_inbound_document"]').val();

		$.ajax({
			url : BaseUrl + 'warehouse/getWarehouseByDocument/',
			type: 'POST',
			dataType: 'JSON',
			data:{
				document_id	:documentId,
				warehouse_id:warehouseId
			},
			success : function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id);
						row = row.replace(/{{ text }}/gi, data[i].name);
					}

					$('#warehouse-form').html(row);
					$('#warehouse-form').select2();

					$('#warehouse-form').val(idWarehouse).trigger('change');

				}else{
					toastr['error'](response.message,"Error");
				}
			},
			error : function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		})
	}

    var disableForm = function(status){

    	if(status){

			$('.date-picker #tanggal-inbound').prop('disabled',true);
			$('.date-picker button').prop('disabled',true);
			$('.add-item').hide();
			$('.add_item').hide();
			$('.remove_row button').hide();
			$('select[name="id_barang[]"]').prop('disabled',true);
			$('input[name="item_quantity[]"]').prop('disabled',true);
			$('input[name="po_item[]"]').prop('disabled',true);
			$('select#source').prop('disabled',true);

		}else{

			$('.date-picker #tanggal-inbound').prop('disabled',false);
			$('.date-picker button').prop('disabled',false);
			$('.add-item').show();
			$('.add_item').show();
			$('.remove_row button').show();
			$('select[name="id_barang[]"]').prop('disabled',false);
			$('input[name="item_quantity[]"]').prop('disabled',false);
			$('input[name="po_item[]"]').prop('disabled',false);
			$('select#source').prop('disabled',false);

		}

    }

    var inboundDocumentHandler = function(){

		$('select[name="id_inbound_document"]').on('change', function(){
			var ctrl = $(this),
				id = ctrl.val();

			App.blockUI();

			if($('option:selected', this).attr('auto-generate') == 0){

				if(idInbound == null){
					$('input[name="kd_inbound"]').val('');
				}

				$('input[name="kd_inbound"]').attr('type','text');
				$('.inbound-code').hide();

			}else{

				$('input[name="kd_inbound"]').attr('type','hidden');
				$('.inbound-code').show();
				// disableForm(false);
				getInboundDocument(id);

			}

			if(!id){
				App.unblockUI();
				$('#source').select2('destroy');
				$('#source').html('');
				$('#source').select2();
			}else{
				getSource();
			}

			getWarehouse();

			/*
						if($('#outbound-peminjaman').length > 0){
							if(id == 7)
								$('.outbound-group').show();
							else
								$('.outbound-group').hide();
						}
			*/

		});

    }

    var updateInboundDocumentNumber = function(){

    	var documentId = $('select[name="id_inbound_document"]').val();

		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/updateInboundDocumentNumber',
			data:{'document_id':documentId},
			dataType:'json',
    		success:function(response){

    			if(response.status == 'OK'){
					$('input[name="kd_inbound"]').val(response.result);
					$('.inbound-code').text(response.result);
    			}else{
					toastr['error'](response.message,"Error");
    			}

    		},
    		error:function(err){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
    		}
    	})
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            handleFormSettings();
            handleDatePickers();
            inboundDocumentHandler();
            /*handleBootstrapSelect();*/
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

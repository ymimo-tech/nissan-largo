var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable;

	var initialize = function(){
		initTable();
		initTableReceiving();
	}

	var initTableTest = function(){

			var table = $('#table-list');

		 table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [6, 15, 20, -1],
                [6, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 6,
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });
	}

	var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl+page_class+"/getDetailItem", // ajax source
					"data": {
						id_inbound: $('#data-table-form input[name="id"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
					/*
					dataSrc: function(json) {
						totalItem = json.recordsFiltered;
						totalQty = json.totalQty;

						$('.t-item').html(totalItem);
						$('.t-qty').html(totalQty);

						return json.data;
					}
					*/
                },
                "columns": [
          					{ "orderable": true, "visible": false },
          					{ "orderable": false },
          					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [2]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [4, 5]
					}
				]
            }
        });
    }

	var initTableReceiving = function () {

        dataTable = new Datatable();

		    var totalItem = 0,
			  totalQty = 0;

        dataTable.init({
            src: $("#table-list-r"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
        				"bDestroy": true,
        				"processing": true,
        				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
        				"initComplete": function(settings, json) {
        					/*
        					var dt = this;
        					setTimeout(function(){
        						dt.fnPageChange( 'first' );
        					}, 300);
        					*/
				        },
                "ajax": {
                    "url": BaseUrl + "receiving/getList", // ajax source
          					"data": {
          						id_po: $('#data-table-form input[name="id"]').val(),
          						// params: 'no_action'
          					},
          					error: function(response){
          						if(response.status == 401)
          							location.href = BaseUrl + 'login';
          					}
					/*
					dataSrc: function(json) {
						totalItem = json.recordsFiltered;
						totalQty = json.totalQty;

						$('.t-item').html(totalItem);
						$('.t-qty').html(totalQty);

						return json.data;
					}
					*/
                },
                "columns": [
          					{ "orderable": false, "visible": false },
          					{ "orderable": false },
          					{ "orderable": true},
                    { "orderable": false, "visible": false },
          					{ "orderable": true },
          					{ "orderable": false },
          					{ "orderable": false },
          					{ "orderable": false },
                    { "orderable": false },
                    { "orderable": false, "visible": false },
                    { "orderable": false }
                ],
                "order": [
                    [0, "DESC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1,2,3,4,5,6,7]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">' + data + '</div>';
						},
						"targets": [8,10]
					}
				]
            }
        });

    }

    var pageInteraction = function(){

        $('.inbound-item-export').on('click', function(){
            var me=$(this);
            post(BaseUrl + page_class + '/export',
            {
                'id_inbound':me.attr('id_inbound')
            });
        });
    }

    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

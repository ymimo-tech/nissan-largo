var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var content_datatable;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#supplier, #periode, #doc-type, #doc-status, #warehouse-filter').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		setTimeout(function(){
            $('#periode').val(current_filter).trigger('change');
			initTable();
		}, 500);
	}

	var reset = function(){
		$("#po").val(null).trigger("change");
		$('#supplier').select2('val', '');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_month');
		$('#doc-type').select2('val', '');
	}

  var initTable = function () {

    content_datatable = new Datatable();

	  var totalItem = 0,
		totalQty = 0;

      content_datatable.init({
          src: $("#datatable_ajax"),
          onSuccess: function (grid, response) {
              // grid:        grid object
              // response:    json object of server side ajax response
              // execute some code after table records loaded
              if(response.customGroupAction=='OK'){
                  toastr['success'](response.customGroupActionMessage,"Success");
              }
              current_filter=response.current_filter;
          },
          onError: function (grid) {

          },
          onDataLoad: function(grid) {
              // execute some code on ajax data load
          },

          loadingMessage: 'Loading...',
          dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
        			"bDestroy": true,
        			"processing": true,
        			"serverSide": true,
              // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
              // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
              // So when dropdowns used the scrollable div should be removed.
              //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
              "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
              "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
              "lengthMenu": [
                  [10, 20, 50, 100, 150],
                  [10, 20, 50, 100, 150] // change per page values here
              ],
              "pageLength": 10, // default record count per page
			        "initComplete": function(settings, json) {

			  },
        "ajax": {
            "url": BaseUrl+page_class+"/get_list", // ajax source
    				"data": {
    					po: $('#po').val(),
    					id_supplier: $('#supplier').val(),
    					from: $('#from').val(),
    					to: $('#to').val(),
    					year: $('#year').val(),
    					doc_type: $('#doc-type').val(),
    					doc_status: $('#doc-status').val(),
              periode:$('#periode').val(),
              warehouse:$('#warehouse-filter').val()
    				},
    				error: function(response){
    					if(response.status == 401)
    						location.href = BaseUrl + 'login';
				    }
    				/*
    				dataSrc: function(json) {
    					totalItem = json.total_item;
    					totalQty = json.total_qty;

    					return json.data;
    				}
    				*/
        },
        "columns": [
	          { "orderable": false, "visible": false },
            { "orderable": false },
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": false },
	          { "orderable": true },
            { "orderable": false }
          ],
        "order": [
            [0, "desc"]
        ],// set first column as a default sort by asc
			  "columnDefs": [
        /*
				{
					"render": function ( data, type, row ) {
						return '<div align="right">'+formatMoney(data,0)+'</div>';
					},
					"targets": [6]
				},*/
				{
					"render": function ( data, type, row ) {
						return '<div align="center">'+data+'</div>';
					},
					"targets": [1,2,3,4,6,7,8]
				}
        ,
				{
					"render": function (data, type, row){
						return '<div align="center">'+data+'</div>';
					},
					"targets": -1
				}
			]
			/*
			"footerCallback": function ( row, data, start, end, display ) {
				var api = this.api(), data;
				var pageTotal,
					total;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};

				// Total over this page
				pageTotal = api
					.column( 6, { page: 'current'} )
					.data()
					.reduce( function (a, b) {
						return parseFloat(a) + parseFloat(b);
					}, 0 );

				// Update footer
				$( api.column( 6 ).footer() ).html(
					''+formatMoney(pageTotal,0) +'<br>'+ formatMoney(totalItem,0)
				);

				// Total over this page
				pageTotal = api
					.column( 7, { page: 'current'} )
					.data()
					.reduce( function (a, b) {
						return parseFloat(a) + parseFloat(b);
					}, 0 );

				// Update footer
				$( api.column( 7 ).footer() ).html(
					'<span style="font-weight: normal">Total /Page :</span> '+formatMoney(pageTotal,0) +'<br><span style="font-weight: normal">Total All :</span> '+ formatMoney(totalQty,0)
				);
			}
			*/
          }
      });
  }

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl+'inbound/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="kd_inbound"]').val(json.kd_inbound);
					$('#data-table-form input[name="tanggal_inbound"]').val(json.tanggal_inbound);
					$('#data-table-form select[name="id_supplier"]').val(json.id_supplier).selectpicker("refresh");
					$('#data-table-form select[name="id_inbound_document"]').val(json.id_inbound_document).selectpicker("refresh");
					$('#data-table-form #added_rows').append(json.inbound_barang_html);
					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});
					$(".remove_row").click(function(){
						$(this).closest("tr").remove();
					});
				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var closePo = function(id){
		App.blockUI();
		$.ajax({
			url: BaseUrl + page_class + '/closePo',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_inbound: id
			},
			success: function(response){
				if(response.status == 'OK'){

					content_datatable.getDataTable().ajax.reload();
					toastr['success'](response.message, "SUCCESS");

				}else{
					toastr['error'](response.message, "Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    var pageInteraction = function(){

    	$('#warehouse-filter').change(function(){
    		initTable();
    	})

		$("#po")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'receiving/getInboundCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_inbound,
							text: item.kd_inbound
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

    $("#form-content").on('hidden.bs.modal', function(){
        $('.form-group').removeClass('has-error');
        $('#form-content input,#form-content textarea').val('');
        $("#form-content select").val('').selectpicker('refresh');
        $("table#inbound-barang .added_row").remove();
    });

		/*
        $('#data-table-filter').submit(function(e){
            e.preventDefault();
            data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
            data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
            data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
            data-table_datatable.getDataTable().ajax.reload();
        });

        $('.hide-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').hide();
            $('.filter-hide-control,.msg-filter').show();
        });

        $('.show-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').show();
            $('.filter-hide-control,.msg-filter').hide();
        });

        $('.reset-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter input[name="nama_data-table"]').val('');
            $('#data-table-filter input[name="kota"]').val('');
            $('#data-table-filter textarea[name="alamat_data-table"]').val('');
            data-table_datatable.clearAjaxParams();
            data-table_datatable.getDataTable().ajax.reload();
        });
		*/
    $('.data-table-add').click(function(e){
        e.preventDefault();
        $('#form-content').modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true
        });
    });

    $('.form-close').click(function(e){
        e.preventDefault();
        $('#form-content').modal('hide');
    });

    $(document).on('click','.data-table-edit',function(e){
        e.preventDefault();

		var ctrl = $(this),
			id = ctrl.attr('data-id');

		if(ctrl.hasClass('disabled-link'))
			return false;

          location.href = BaseUrl + page_class + "/edit/" + id;
      });

		$(document).on('click','.data-table-close',function(e){
        e.preventDefault();

  			var ctrl = $(this),
  				id = ctrl.attr('data-id');

  			if(ctrl.hasClass('disabled-link'))
  				return false;

        closePo(id);
    });

    $(document).on('click','.data-table-delete',function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');

  			if($(this).hasClass('disabled-link'))
  				return false;

              bootbox.confirm("Are you sure want remove this data ?", function(result) {
                  if(result){
  					          App.blockUI();
                      $.ajax({
                          type:'POST',
                          url:BaseUrl+'inbound/delete',
                          data:{'id':id},
                          dataType:'json',
                          success:function(json){
                              if(json.success){

                  								if(json.status == 'OK'){
                  									toastr['success'](json.message,"Success");
                  									content_datatable.getDataTable().ajax.reload();
                  								}else{
                  									toastr['error'](json.message, "Error");
                  								}

                              }else{
                                  if(json.no_delete){
                                      toastr['warning']("You don't have authority to delete this data","Warning");
                                  }else{
                                      toastr['error']("Please refresh the page and try again later.","Error Occured");
                                  }
                              }
  							              App.unblockUI();
                          },
  						            error: function(response){
                							App.unblockUI();
                							if(response.status == 401)
                								location.href = BaseUrl + 'login';
                							else
                								bootbox.alert(response.responseText);
  						            }
                      });
                  }
              });
          });

    $(document).on('click','.btn-sync',function(){

			var bsync = $(this);
            bsync.attr('disabled',true);
            bsync.prop('disabled',true);
            bsync.html('<i class="fa fa-refresh"></i> Please wait');

            $.ajax({
    			url: BaseUrl + 'dashboard/manual_sync',
    			type: 'POST',
    			dataType: 'JSON',
    			success: function(json){
                    if(json.newc.inbound.length>0){
                        $.each(json.newc.inbound, function (i,newc){
                            if(newc.newrec){
                                var title="New Inbound";
                            }else{
                                var title="Inbound Updated";
                            }

                            toastr['info']("<a href='"+BaseUrl+"inbound/detail/"+newc.id+"' target='_blank'>"+newc.nomor+"</a>",title);
                        });
                        content_datatable.getDataTable().ajax.reload();
                    }

		    		bsync.attr('disabled',false);
		            bsync.prop('disabled',false);
		            bsync.html('<i class="fa fa-refresh"></i> SYNC');

    			}
    		});

	    });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.mdatepicker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

    }

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_inbound: {
                    required: true
                },
                tanggal_inbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_inbound_document: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check your input again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'inbound/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data has been successfully updated.","Success");
                            }else{
                                toastr['success']("Data has been successfully added.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You don't have authority to add this data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You don't have authority to change this data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    var longpolling = function(){
        function start_longpolling(){
            var evtSource = new EventSource(BaseUrl+'inbound/longpolling');
    		    evtSource.addEventListener("sync", function(e) {

            var obj = JSON.parse(e.data);
            if(obj.newc.length>0){
      	      $.each(obj.newc, function (i,newc){
       			toastr['info']("<a href='"+BaseUrl+"inbound/detail/"+newc.id+"' target='_blank'>"+newc.nomor+"</a>","New Inbound");
      	      });
                //content_datatable.getDataTable().ajax.reload();
                evtSource.close();
                start_longpolling();
              }
      	    }, false);
        }

        start_longpolling();
    }

    return {

        //main function to initiate the module
        init: function () {
			      initialize();
            pageInteraction();
            handleFormSettings();
            handleDatePickers();
            //longpolling();
            /*handleBootstrapSelect();*/
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

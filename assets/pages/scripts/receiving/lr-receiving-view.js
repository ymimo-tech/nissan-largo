var Receiving = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dTable,
		RCV_ID;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#supplier, #periode').select2({
			allowClear: false,
			width: null
		});

		$('#status').select2();

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		setTimeout(function(){
            $('#periode').val(current_filter).trigger('change');
			initTable();
		}, 500);
	}

	var initTable = function () {

        dTable = new Datatable();

        dTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
                current_filter=response.current_filter;
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl+page_class+"/getList", // ajax source
					"data": {
						id_receiving: $('#rcv').val(),
						id_po: $('#inbound').val(),
						id_supplier: $('#supplier').val(),
						from: $('#from').val(),
						to: $('#to').val(),
						status: $('#status').val(),
                        periode:$('#periode').val(),
                        warehouse:$('#warehouse-filter').val(),
                        has_preorder:$('#has-preorder').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
					/*
					dataSrc: function(json) {
						totalItem = json.total_item;
						totalQty = json.total_qty;

						return json.data;
					}
					*/
                },
                "columns": [
					          { "orderable": false, "visible": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": false },
                    { "orderable": false },
          					{ "orderable": false },
          					{ "orderable": false },
          					{ "orderable": false },
                    { "orderable": false },
                    // { "orderable": false },
					          { "orderable": false }
                  ],
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,5,6,7,8]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
			}
		});
	}

	var reset = function(){
		$("#rcv").val(null).trigger("change");
		$("#inbound").val(null).trigger("change");
		$('#status').val(null).trigger('change');
		$('#has-preorder').val(null).trigger('change');
		$('#supplier').select2('val', '');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_month');
	}

	var showModalPrint = function(id){

		if(id){

			App.blockUI();
			$.ajax({
				url: BaseUrl + page_class + '/getQtyPrint',
				type: 'POST',
				dataType: 'JSON',
				data: {
					id_receiving: id
				},
				success: function(response){

					var data = response;
					$('#data-modal-form input[name="id"]').val(id);
					$('.qty-print-receiving').html(data.qty);

					$('.qty-print-receiving').show();
					$('#qty-print').hide();
					$('.required').hide();

					$('#modal-print').modal({
						backdrop: 'static',
						keyboard: true
					});

					App.unblockUI();
				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});

		}else{

			$('#data-modal-form input[name="id"]').val('');

			$('#qty-print').val('');

			$('.qty-print-receiving').hide();
			$('#qty-print').show();
			$('.required').show();

			$('#modal-print').modal({
				backdrop: 'static',
				keyboard: true
			});

		}
	}

	var printBarcode = function(){

		var $btn = $('#submit-print');
			$btn.button('loading');

		var type = $('#type-print').val();
		var warehouse = $('#warehouse-filter').val();
		var qty = $('#qty-print').val(),
			check = 0;
		if($('#data-modal-form input[name="id"]').val().length > 0){
			qty = $('.qty-print-receiving').html();
			check = 1;
		}

		if (!/\S/.test(qty)) {
			if(check == 0){
				$('.qty-print-group').addClass('has-error');
			}
			$btn.button('reset');
			toastr['error']("Qty print cannot empty and must be greater than 0","Error");
			return false;
		}else{
			if(qty <= 0){

				if(check == 0){
					$('.qty-print-group').addClass('has-error');
				}
				$btn.button('reset');
				toastr['error']("Qty print cannot empty and must be greater than 0","Error");
				return false;
			}
		}

		if(warehouse != "" || type == "serial-number"){

			$.ajax({
				url: BaseUrl + page_class + '/printZpl',
				type: 'POST',
				dataType: 'JSON',
				data: {
					type_print: type,
					qty_print: qty,
					item: $('#item').val(),
					warehouse: warehouse
				},
				success: function(response){
					if(response.status == 'OK'){
						//UNCOMMENT SCRIPT BELOW FOR PRINT TO SPECIFIC PRINTER. GET DEFAULT PRINTER FIRST.
						/*
						location.href = 'MimoPrint:GET_DEFAULT_PRINTER?url=' + base64_encode(BaseUrl + '&sid=' + response.sid);
						setTimeout(function(){
							$.get(BaseUrl + 'send_to_printer/getDefaultPrinter/' + response.sid, function(data){
								var prntr = JSON.parse(data);
								console.log(prntr);
								location.href = 'MimoPrint:CLIENT_PRINT_DEFAULT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid + '&printer=' + prntr.printer);
							});
						}, 1500);
						*/
						location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
					}else
						bootbox.alert(response.message);
					setTimeout(function(){
						$btn.button('reset');
					}, 1000);
					//App.unblockUI();
					/*
					var zpl = response.zpl;

					var ctrl = $(zpl);

					var postdata = {
						sid: $('#sid').val(),
						pid: $('#pid').val(),
						printerCommands: zpl
					}
					// store user printer settings & commands in server cache
					$.post(BaseUrl + 'webclientprint/ProcessPrint.php',
						postdata,
						function() {
							 // Launch WCPP at the client side for printing...
							 var sessionId = $("#sid").val();
							 jsWebClientPrint.print('sid=' + sessionId);
						}
					);
					*/
				},
				error: function(response){
					$btn.button('reset');
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
		}else{
			toastr['warning']("Please select warehouse","Warning!");
			$btn.button('reset');
			return false;
		}
	}

	var finishTally = function(id){

		var d = $.Deferred();

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/finishTally',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_receiving: id,
				remark: $('#remark').val()
			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){
					d.resolve(response);
					toastr['success'](response.message, "Success");
				}else
					toastr['error'](response.message, "Error");

				$('#modal-remark').modal('hide');
			},
			error: function(response){
				App.unblockUI();

				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

		return d.promise();
	}

    var pageInteraction = function(){

    	$('#item').select2();

		$(document).on('click','#set_complete',function(e){
            e.preventDefault();

			var id = $(this).attr('data-id');

            $.ajax({
            	url : BaseUrl + page_class + "/setComplete/" + id,
            	method : "GET",
            	dataType : "JSON",
            	success : function(response){

	           		if(response.status){

	            		initTable();
	            		toastr['success'](response.message,"success");

	            	}else{

	            		toastr['error'](response.message,"error");

	            	}

            	},
            	error : function(err){
            		toastr['error'](err.message,"error");
            	}

            })
        });

		$(document).on('click','.data-table-cancel',function(e){
            e.preventDefault();

			if($(this).hasClass('disabled-link'))
				return false;


			var id = $(this).attr('data-id');


            $.ajax({
            	url : BaseUrl + page_class + "/cancel_receiving/",
            	method : "POST",
            	data: {receiving_id:id},
            	dataType : "JSON",
            	success : function(response){

	           		if(response.status){

	            		initTable();
	            		toastr['success'](response.message,"success");

	            	}else{

	            		toastr['error'](response.message,"error");

	            	}

            	},
            	error : function(err){
            		toastr['error'](err.message,"error");
            	}

            })
        });

		$(document).on('click','#back_to_tally',function(e){
            e.preventDefault();

			var id = $(this).attr('data-id');

            $.ajax({
            	url : BaseUrl + page_class + "/backToTally/" + id,
            	method : "GET",
            	dataType : "JSON",
            	success : function(response){

	           		if(response.status){

	            		initTable();
	            		toastr['success'](response.message,"success");

	            	}else{

	            		toastr['error'](response.message,"error");

	            	}

            	},
            	error : function(err){
            		toastr['error'](err.message,"error");
            	}

            })
        });

		$(document).on('click','#reset_receiving',function(e){
            e.preventDefault();

			var id = $(this).attr('data-id');

            $.ajax({
            	url : BaseUrl + page_class + "/resetReceiving/" + id,
            	method : "GET",
            	dataType : "JSON",
            	success : function(response){

	           		if(response.status){

	            		initTable();
	            		toastr['success'](response.message,"success");

	            	}else{

	            		toastr['error'](response.message,"error");

	            	}

            	},
            	error : function(err){
            		toastr['error'](err.message,"error");
            	}

            })
        });

		$("#has-preorder")
			.select2({
			 	placeholder: "-- All --",
			 	allowClear:true,
				data: [
					{id:0,text:'No'},
					{id:1,text:'Yes'}
		        ]
		    });

		$('#has-preorder').val(null).trigger('change');

    	$('#warehouse-filter').change(function(){
    		initTable();
    	})

		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});

		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});

		$('#modal-print').on('shown.bs.modal', function () {
			$('#qty-print').focus();
		});

		$('#button-print-qty-barcode-lp').on('click', function(e){
			e.preventDefault();
			$('#type-print').val('license-plate');
	    	$('.item-print-group').hide();
			showModalPrint();
		});

		$('#button-print-qty-barcode-sn').on('click', function(e){
			e.preventDefault();
			$('#type-print').val('serial-number');
	    	$('.item-print-group').show();
			showModalPrint();
		});

		$('#button-receiv').on('click', function(e){
			e.preventDefault();

			var idRcv = [];
			$('#table-list tbody input[type="checkbox"]').each(function(){
				var ctrl = $(this),
					id = ctrl.val();

				if(ctrl.is(':checked'))
					idRcv.push(id);
			});

			if(idRcv.length <= 0){
				toastr['error']("No record selected.","Error");
				return false;
			}

			post(BaseUrl + page_class + '/process',
			{
				id_receiving: idRcv
			});
		});

		$(document).on('click','.data-table-barcode',function(e){
            e.preventDefault();

        });

		$(document).on('click','.data-table-tally',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_tally',
			{
				id_receiving: id
			}, '', 'Y');
        });

		$(document).on('click','.data-table-print-grn',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_grn',
			{
				id_receiving: id
			}, '', 'Y');
        });

		$(document).on('click','.data-table-barcode',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			$('#type-print').val('serial-number');

            showModalPrint(id);
        });

		$('#data-modal').on('submit', function(e){
			e.preventDefault();

			$('#submit-remark').button('loading');

			var tally = finishTally(RCV_ID);
			tally.done(function(response){

				dTable.getDataTable().ajax.reload();

				$('#submit-remark').button('reset');

			});
		});

		$(document).on('click','.data-table-finish-tally',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			RCV_ID = id;

			if(ctrl.hasClass('disabled-link'))
				return false;

			$('#remark').val('');

			$('#modal-remark').modal({
				keyboard: true,
				backdrop: 'static'
			});

            //finishTally(id);
        });

		$(document).on('click','.data-table-edit',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            location.href = BaseUrl + page_class + "/edit/" + id;
        });

		$(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id = $(this).attr('data-id');

			if($(this).hasClass('disabled-link'))
				return false;

            bootbox.confirm("Are you sure want remove this data ?", function(result) {
                if(result){
					App.blockUI();
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'receiving/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){

								if(json.status == 'OK'){
									toastr['success'](json.message,"Success");
									dTable.getDataTable().ajax.reload();
								}else{
									toastr['error'](json.message, "Error");
								}

                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You don't have autority to delete this data","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
							App.unblockUI();
                        },
						error: function(response){
							App.unblockUI();
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
                    });
                }
            });
        });

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

		$("#rcv")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + page_class + '/getReceivingCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_receiving,
							text: item.kd_receiving
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$("#inbound")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + page_class + '/getInboundCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_inbound,
							text: item.kd_inbound
						});
					});
					return {
						results: results
					};
				}
			}
		});
    }

    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Receiving.init();
});

var Receiving = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dTable,
		isTally = false;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: moment().format('DD/MM/YYYY')
		});

		handleFormSettings();

		$('#inbound').select2();

		var id = $('input[name="id"]').val();
		var id_inbound = $('input[name="id_inbound"]').val();

		if(id.length > 0){
			edit(id);
			$('#inbound').attr('disabled', '');
			setTimeout(function(){
				initTableReceiving();
			}, 500);
		}else{
			setTimeout(function(){
				initTable();
			}, 500);
		}

		if(id_inbound.length > 0){
			$('#inbound').val(id_inbound).trigger('change');
			$('#inbound').attr('disabled', '');
			// initTableReceiving();
			setTimeout(function(){
				initTable();
			}, 500);
		}

	}

	var initTable = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getDetailItem',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_inbound: $('#inbound').val(),
				id_receiving: $('#data-table-form input[name="id"]').val()
			},
			success: function(response){
				App.unblockUI();

				var tpl = '<tr>\
								<td><div align="center">{{ no }}</div></td>\
								<td><div align="center">{{ item_code }}</div></td>\
								<td>{{ item_name }}</td>\
								<td style="width:30%">{{ inbound_list }}</td>\
								<td><div align="right">{{ inb_qty }}</div></td>\
								<td><div align="right" class="form-group" style="margin: 0; padding: 0;">{{ qty_doc }}</div></td>\
								<td><div align="right">{{ qty_received }}</div></td>\
								<td><div align="right">{{ discrapency }}</div></td>\
						  </tr>';


				var data = response.data,
					iLen = data.length,
					row;

				for(var i = 0; i < iLen; i++){

					var qtyDoc = data[i].qty_doc,
						qtyReceived = data[i].qty_received,
						discrapency = 0,
						satuan = data[i].nama_satuan,
						inb_qty = data[i].inb_qty,
						inboundList = '';


					if($('input[name="id"]').val().length > 0){

						if(parseInt(qtyDoc) > 0 && qtyDoc != '' && qtyDoc != null){
							discrapency = qtyDoc - qtyReceived;
							qtyDoc = '<input type="text" name="qty_doc" class="qty-doc input-sm table-input form-control" data-inbound-id="'+data[i].id_inbound+'"" data-id="'+data[i].id_barang+'" value="'+qtyDoc+'" aria-required="true" />&nbsp;&nbsp;'+satuan+'';
						}else{
							qtyDoc = '<input type="text" name="qty_doc" class="qty-doc input-sm table-input form-control" data-inbound-id="'+data[i].id_inbound+'"" data-id="'+data[i].id_barang+'" value="'+qtyDoc+'" aria-required="true" />&nbsp;&nbsp;'+satuan+'';
						}

					}else{
						qtyDoc = '<input type="text" name="qty_doc" class="qty-doc input-sm table-input form-control" data-inbound-id="'+data[i].id_inbound+'"" data-id="'+data[i].id_barang+'" value="'+qtyDoc+'" aria-required="true" />&nbsp;&nbsp;'+satuan+'';
					}

					if(parseInt(qtyReceived) > 0 && qtyReceived != '' && qtyReceived != null){
						qtyReceived = qtyReceived;
					}

/*
					inboundList +='<select name="inbound-id" id="inbound-id-'+data[i].id_barang+'" class="inbound-id">';

					$.each(data[i].kd_inbound, function(i,v){
						inboundList += '<option value="'+i+'">'+v+'</option>';
					});

					inboundList += '</select>';
*/

					row += tpl;
					row = row.replace(/{{ no }}/gi, i + 1);
					row = row.replace(/{{ item_code }}/gi, data[i].kd_barang);
					row = row.replace(/{{ item_name }}/gi, data[i].nama_barang);
					row = row.replace(/{{ inb_qty }}/gi, inb_qty + ' ' + satuan);
					row = row.replace(/{{ qty_doc }}/gi, qtyDoc);
					row = row.replace(/{{ qty_received }}/gi, qtyReceived + ' ' + satuan);
					row = row.replace(/{{ discrapency }}/gi, Math.abs(discrapency) + ' ' + satuan);
					row = row.replace(/{{ inbound_list }}/gi, data[i].kd_inbound);

				}

				if(iLen <= 0){
					row = '<tr><td colspan="6"><div align="center">No data</div></td></tr>';
				}

				$('#table-list tbody').html(row);
				// $('select[name="inbound-id"]').select2();
				$('.detail-item').show();

			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		})

    }

	var initTableReceiving = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getDetailItemReceiving',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_inbound: $('#inbound').val(),
				id_receiving: $('#data-table-form input[name="id"]').val()
			},
			success: function(response){
				App.unblockUI();

				var tpl = '<tr>\
								<td><div align="center">{{ no }}</div></td>\
								<td><div align="center">{{ item_code }}</div></td>\
								<td>{{ item_name }}</td>\
								<td>{{ inbound_list }}</td>\
								<td><div align="right" class="form-group" style="margin: 0; padding: 0;">{{ qty_doc }}</div></td>\
								<td><div align="right">{{ qty_received }}</div></td>\
								<td><div align="right">{{ discrapency }}</div></td>\
						  </tr>';


				var data = response.data,
					iLen = data.length,
					row;

				for(var i = 0; i < iLen; i++){

					var qtyDoc = data[i].qty_doc,
						qtyReceived = data[i].qty_received,
						discrapency = 0,
						satuan = data[i].nama_satuan || ''
						inboundList = '';

					if($('input[name="id"]').val().length > 0){

						if(!isTally){

							if(parseInt(qtyDoc) > 0 && qtyDoc != '' && qtyDoc != null){
								discrapency = qtyDoc - qtyReceived;
								qtyDoc = '<input type="text" name="qty_doc" class="qty-doc input-sm table-input form-control" data-inbound-id="'+data[i].id_inbound+'"" data-id="'+data[i].id_barang+'" value="'+qtyDoc+'" aria-required="true" />&nbsp;&nbsp;'+satuan+'';
							}else{
								qtyDoc = '<input type="text" name="qty_doc" class="qty-doc input-sm table-input form-control" data-inbound-id="'+data[i].id_inbound+'"" data-id="'+data[i].id_barang+'" value="'+qtyDoc+'" aria-required="true" />&nbsp;&nbsp;'+satuan+'';
							}

						}else{

							if(parseInt(qtyDoc) > 0 && qtyDoc != '' && qtyDoc != null)
								discrapency = qtyDoc - qtyReceived;

						}

					}else{
						qtyDoc = '<input type="text" name="qty_doc" class="qty-doc input-sm table-input form-control" data-inbound-id="'+data[i].id_inbound+'"" data-id="'+data[i].id_barang+'" value="'+qtyDoc+'" aria-required="true" />&nbsp;&nbsp;'+satuan+'';
					}

/*					inboundList +='<select name="inbound-id" id="inbound-id-'+data[i].id_barang+'" class="inbound-id">';

					$.each(data[i].kd_inbound, function(i,v){
						inboundList += '<option value="'+i+'">'+v+'</option>';
					});

					inboundList += '</select>';
*/
					row += tpl;
					row = row.replace(/{{ no }}/gi, i + 1);
					row = row.replace(/{{ item_code }}/gi, data[i].kd_barang);
					row = row.replace(/{{ item_name }}/gi, data[i].nama_barang);
					row = row.replace(/{{ qty_doc }}/gi, qtyDoc);
					row = row.replace(/{{ qty_received }}/gi, qtyReceived + ' ' + satuan );
					row = row.replace(/{{ discrapency }}/gi, Math.abs(discrapency) + ' ' + satuan);
					row = row.replace(/{{ inbound_list }}/gi, inboundList);

				}

				if(iLen <= 0){
					row = '<tr><td colspan="6"><div align="center">No data</div></td></tr>';
				}

				$('#table-list tbody').html(row);

/*				$('select[name="inbound-id"]').select2();
				for(var i = 0; i < iLen; i++){
					$('select[id="inbound-id-'+data[i].id_barang+'"]').val(data[i].inbound_id).trigger('change');
				}
*/
				$('.detail-item').show();

			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}

		});

    }

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/getEdit',
			data:{'id':id},
			dataType:'json',
			success:function(json){

				if(! json.no_data){
					$('#data-table-form input[name="id"]').val(json.id_receiving);
					$('#data-table-form input[name="kd_receiving"]').val(json.kd_receiving);
					$('#data-table-form .rcv-code').html(json.kd_receiving);
					//$('#data-table-form input[name="rcv_date"]').val(json.tanggal_receiving);
					//$('.date-picker').datepicker("update", json.tanggal_receiving);
					//$('#data-table-form input[name="rcv_date"]').datepicker("update", json.tanggal_receiving);

					$('.date-picker').datepicker('remove');

					$('.date-picker').datepicker({
						orientation: "left",
						format: 'dd/mm/yyyy',
						autoclose: true,
						startDate: json.tanggal_receiving
					});

					$('.date-picker').datepicker("update", json.tanggal_receiving);

					//$('#inbound').select2('data', {id: json.id_inbound, text: json.kd_inbound});
					//$('#inbound').html('<option value="'+json.id_inbound+'">'+json.kd_inbound+'</option>');
					//$('#inbound').val(json.id_inbound).trigger('change');
					$('#inbound').val(json.id_inbound).trigger('change');

					$('#data-table-form input[name="plate"]').val(json.vehicle_plate);
					$('#data-table-form input[name="driver"]').val(json.dock);
					$('#data-table-form input[name="doc"]').val(json.delivery_doc);

					if(json.is_cross_doc == 1){
                        $('#crossdoc').prop('checked', true);
                        $('#crossdoc').parent().addClass('checked');
					}

					if(json.start_tally != null)
						isTally = true;

					if(isTally){

						$('#rcv-date').attr('disabled', '');
						$('.date-picker').datepicker('remove');

						$('#data-table-form input[name="plate"]').attr('disabled', '');
						$('#data-table-form input[name="driver"]').attr('disabled', '');
						$('#data-table-form input[name="doc"]').attr('disabled', '');
					}

				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var reset = function(){
		$('#data-table-form input[name="id"]').val('');
		$('#data-table-form input[name="params"]').val('');
		$('#data-table-form input[name="kd_receiving"]').val('');
		$('#data-table-form .rcv-code').html('');
		$('#data-table-form input[name="rcv_date"]').val(moment().format('DD/MM/YYYY'));
		$("#data-table-form #inbound").val(null).trigger("change");
		$('#data-table-form input[name="plate"]').val('');
		$('#data-table-form input[name="driver"]').val('');
		$('#data-table-form input[name="doc"]').val('');
		$('#table-list tbody').html('');

		// $('#data-table-form #inbound').select2('open');
		// $('#data-table-form #inbound').on('open', function () {
		//   self.$search.attr('tabindex', 0);
		//   //self.$search.focus(); remove this line
		//   setTimeout(function () { self.$search.focus(); }, 10);//add this line

		// });
	}

	var newRcvCode = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getNewRcvCode',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){
					$('#data-table-form input[name="kd_receiving"]').val(response.rcv_code);
					$('#data-table-form .rcv-code').html(response.rcv_code);
				}else{
					toastr['error']("Generate new receiving code failed. Please refresh page","Form Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

	var getInboundDetail = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getInboundDetail',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_inbound: $('#inbound').val()
			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){

					var tpl = '<tr>\
									<td>{{ no }}</td>\
									<td><div align="center">{{ code }}</div></td>\
									<td>{{ name }}</td>\
									<td>\
										<div align="center">\
											<input type="text" name="qty_doc" class="form-control input-sm qty-doc" data-id="{{ id }}" value="" />\
										</div>\
									</td>\
							  </tr>',
						row = '',
						data = response.data,
						len = data.length;

					if(len > 0){
						for(var i = 0; i < len; i++){
							row += tpl;
							row = row.replace(/{{ no }}/gi, (i + 1));
							row = row.replace(/{{ id }}/gi, data[i].id_barang);
							row = row.replace(/{{ code }}/gi, data[i].kd_barang);
							row = row.replace(/{{ name }}/gi, data[i].nama_barang);
						}
					}else{
						row += '<tr><td colspan="4"><div align="center">No item in Inbound Document</div></td></tr>';
					}

					$('.detail-item-add table tbody').html(row);

					if($('#inbound').val() !== null)
						$('.detail-item-add').show();
					else
						$('.detail-item-add').hide();

				}else{
					toastr['error']("Get detail item failed","Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

	var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                rcv_date: {
                    required: true
                },
				inbound: {
                    required: true
                },
                doc: {
                	required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {

				if($('.rcv-code').html().length <= 0){
					toastr['error']("Receiving Code is required","Error");
					return false;
				}

				var chk = true;
				$('.qty-doc').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					if(!/\S/.test(val)) {
						ctrl.parent().addClass('has-error');
						chk = false;
					}

					if(val < 0) {
						ctrl.parent().addClass('has-error');
						chk = false;
					}
				});

				if($('#rcv-date').val().length != 10){
					$('.rcv-date-group').addClass('has-error');
					chk = false;
				}

				if(!chk){
					toastr['error']("Please check again.","Form Error");
					return false;
				}

				App.blockUI();

				var barang = [];

				var idTable = 'table-list'; //TODO FIXED LATER
				if($('#data-table-form input[name="id"]').val().length > 0)
					idTable = 'table-list';

				$('#' + idTable + ' .qty-doc').each(function(){

					var ctrl = $(this),
						idBarang = ctrl.attr('data-id'),
						qty = ctrl.val(),
						inboundId = ctrl.attr('data-inbound-id');
						// inboundId = $('#inbound-id-'+idBarang).val();

					barang.push({id_barang: idBarang, qty: qty, inbound_id: inboundId});

				});

				var crossdoc = 0;
				if($('#data-table-form input[name="crossdoc"]').prop('checked')){
					crossdoc = 1;
				}

				var postdata = {
					id: $('#data-table-form input[name="id"]').val(),
					params: $('#data-table-form input[name="params"]').val(),
					kd_receiving: $('#data-table-form input[name="kd_receiving"]').val(),
					rcv_date: $('#data-table-form input[name="rcv_date"]').val(),
					inbound: $('#inbound').val(),
					plate: $('#data-table-form input[name="plate"]').val(),
					driver: $('#data-table-form input[name="driver"]').val(),
					doc: $('#data-table-form input[name="doc"]').val(),
					barang: barang,
					crossdoc : crossdoc
				};

                $.ajax({
                    type:'POST',
                    url:BaseUrl+'receiving/proses',
                    dataType:'json',
                    data: postdata,
                    beforeSend:function(){
                        //$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        //$('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Save changes success.","Success");
                            }else{
                                toastr['success']("Save success.","Success");
                            }

							var params = $('input[name="params"]').val();

							if(params == 'new'){
								reset();
								newRcvCode();
								$('.detail-item-add').hide();
							}else{
								setTimeout(function(){
									location.href = BaseUrl + page_class;
								}, 500);
							}
                            //content_datatable.getDataTable().ajax.reload();
                            //$('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You dont have autority to add data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You dont have autority to change data..","Warning");
                            }else if(!json.response){
                                toastr['warning'](json.message,"Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
	                    }
						App.unblockUI();
                        //$('.modal-content').unblock();
                        //$('.spinner').remove();
                    },
					error: function(response){
						App.unblockUI();
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
                });
            }
        });
    }

    var pageInteraction = function(){

		$(document).on('keydown', '.qty-doc', function(e){
			return isNumeric(e);
		});

/*		$('#inbound').on('change', function(){
			//if($('#data-table-form input[name="id"]').val() === null || $('#data-table-form input[name="id"]').val() == '')
			//getInboundDetail();

			var id = $('input[name="id"]').val();
			if(id.length <= 0)
				initTable();
		});
*/
		$('#plate').on('keyup', function(){
			var ctrl = $(this);
			ctrl.val(ctrl.val().toUpperCase());
		});

		$('#save-new').on('click', function(){
			$('input[name="params"]').val('new');
			$('#data-table-form').submit();
		});
		/*
		$("#inbound")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "Type here...",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + page_class + '/getInboundCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term,
						params: 'open'
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_inbound,
							text: item.kd_inbound
						});
					});
					return {
						results: results
					};
				}
			}
		});
		*/
    }

    var multipleInboundHandler = function(){

		var val = '';
    	$('#inbound').change(function(){

    		if($(this).val() != null){

	    		if($(this).val() != ''){

					$('#inbound option:selected').each(function(){

						if($(this).val() != ''){

							if(val == ''){
							    val = $(this).attr('source-id');
							}

							if($(this).attr('source-id') != val){

								$(this).prop('selected',false);
								$('li[title="'+$(this).text()+'"]').remove();

								toastr['error']("Tidak boleh lebih dari 1 sumber.","Error");

								return false;

							}

						}

					})

					var id = $('input[name="id"]').val();
					if(id.length <= 0){
						initTable();
					}

				}

			}

    	})

    }

    return {

        init: function () {
			initialize();
            pageInteraction();
            multipleInboundHandler();
        }

    };
}();

jQuery(document).ready(function() {
    Receiving.init();
});

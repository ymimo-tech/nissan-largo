var Ref_supplier = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

    var content_datatable;
    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                // "language": {
                    // "lengthMenu": "Display _MENU_ records",
                    // "info" : ", Display _START_ to _END_ of _TOTAL_ entries"
                // },
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl+page_class+"/get_list", // ajax source
                },
                "columns": [
                    // { "orderable": false },
                    { "orderable": false },
                    // { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": false },
                  ],
				
                "order": [
                    [1, "desc"]
                ],// set first column as a default sort by asc
				
				"columnDefs": [
/*					{
						"render": function ( data, type, row ) {
							return data.substr(0,20) + '...';
						},
						"targets": [4]
					},
					{
						"render": function ( data, type, row ) {
							return data.substr(0,20) + '...';
						},
						"targets": [5]
					}
*/				]
            }
        });
    }

	var getEdit = function(id){
		 $.ajax({
			type:'POST',
			url:BaseUrl+'referensi_supplier/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){

				if(! json.no_data){
					$('#referensi-supplier-form input[name="id"]').val(json.id_supplier);
					$('#referensi-supplier-form input[name="kd_supplier"]').val(json.kd_supplier);
					$('#referensi-supplier-form input[name="nama_supplier"]').val(json.nama_supplier);
					$('#referensi-supplier-form input[name="alamat_supplier"]').val(json.alamat_supplier);
					$('#referensi-supplier-form input[name="telepon_supplier"]').val(json.telepon_supplier);
					$('#referensi-supplier-form input[name="cp_supplier"]').val(json.cp_supplier);
					$('#referensi-supplier-form input[name="email_supplier"]').val(json.email_supplier);

                    $('#warehouse').val(null).trigger('change');
                    $('#warehouse').val(json.warehouses).trigger('change');

					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});
				}else{
					toastr['error']("Could not load data.","Error");
				}
			}
		});
	}

    var pageInteraction = function(){

		$('.data-table-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{

			});
		});

		var id = window.location.hash;
		if(id != ''){
			getEdit(id.replace(/\#/gi, ''));
			parent.location.hash = '';
		}

        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
        });
        $('#data-table-filter').submit(function(e){
            e.preventDefault();
            data-table_datatable.setAjaxParam("nama_data-table", $('input[name="nama_data-table"]').val());
            data-table_datatable.setAjaxParam("kota", $('input[name="kota"]').val());
            data-table_datatable.setAjaxParam("alamat_data-table", $('input[name="alamat_data-table"]').val());
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.hide-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').hide();
            $('.filter-hide-control,.msg-filter').show();
        });
        $('.show-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter,.filter-show-control').show();
            $('.filter-hide-control,.msg-filter').hide();
        });
        $('.reset-filter').click(function(e){
            e.preventDefault();
            $('#data-table-filter input[name="nama_data-table"]').val('');
            $('#data-table-filter input[name="kota"]').val('');
            $('#data-table-filter textarea[name="alamat_data-table"]').val('');
            data-table_datatable.clearAjaxParams();
            data-table_datatable.getDataTable().ajax.reload();
        });
        $('.data-table-add').click(function(e){
            e.preventDefault();

            $('#warehouse').val(null).trigger('change');
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });
        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
			getEdit(id);
        });
        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want to remove data ?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+'referensi_supplier/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.status == 'OK'){
                                toastr['success'](json.message,"Success");
                                content_datatable.getDataTable().ajax.reload();
                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You dont have authority to delete data.","Warning");
                                }else{
                                    toastr['error'](json.message,"Error Occured");
                                }
                            }
                        }
                    });
                }
            });
        });

        $(document).on('click','.btn-sync',function(){

            var bsync = $(this);
            bsync.attr('disabled',true);
            bsync.prop('disabled',true);
            bsync.html('<i class="fa fa-refresh"></i> Please wait');

            $.ajax({
    			url: BaseUrl + 'dashboard/manual_sync',
    			type: 'POST',
    			dataType: 'JSON',
                success: function(json){
                    if(json.newc.supplier.length>0){
                        $.each(json.newc.supplier, function (i,newc){
                            if(newc.newrec){
                                var title="New Supplier";
                            }else{
                                var title="Supplier Updated";
                            }
                            toastr['info']("<a href='"+BaseUrl+"referensi_supplier' target='_blank'>"+newc.kd_supplier+"</a><br/>"+newc.nama,title);
                        });
                        content_datatable.getDataTable().ajax.reload();
                    }

                    bsync.attr('disabled',false);
                    bsync.prop('disabled',false);
                    bsync.html('<i class="fa fa-refresh"></i> SYNC');

    			}
    		});
        });
    }

    var handleFormSettings = function() {
        $('#referensi-supplier-form').validate({
            debug: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                kd_supplier: {
                    required: true
                },
                nama_supplier: {
                    required: true
                },
                alamat_supplier: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'referensi_supplier/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#referensi-supplier-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        console.log(json);
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Change data success.","Success");
                            }else{
                                toastr['success']("Save data success.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You dont have authority to add data","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You dont have authority to change data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    var warehouseOptionHandler = function(){

        var row = '<option value="all">Select All</option>';
        row = row + $('#warehouse').html();

        $('#warehouse').html(row);

        $('#warehouse').select2();

        $('#warehouse').on('change', function(e){

            var ctrl = $(this);

            var allOption = $('#warehouse option').length;
            var selectedOption = $('#warehouse option:selected').length;

            if($.inArray('all',ctrl.val()) != -1){
                if(allOption != selectedOption){
                    $('#warehouse option').each(function(){
                        $(this).prop('selected',true);
                    })
                    $('#warehouse option[value="all"]').text('Deselect All');
                    $('#warehouse option[value="all"]').prop('selected',false);

                }else{

                    $('#warehouse option').each(function(){
                        $(this).prop('selected',false);
                    })
                    $('#warehouse option[value="all"]').text('Select All');

                }

            }
            else
            {

                if((allOption-1) == selectedOption){

                    $('#warehouse option[value="all"]').text('Deselect All');

                }else{

                    $('#warehouse option[value="all"]').text('Select All');

                }

            }
            $(this).trigger('change.select2');

        });

        $('#warehouse').on('select2:open', function(e){

            e.preventDefault();

            var allOption = $('#warehouse option').length;
            var selectedOption = $('#warehouse option:selected').length;
            if((allOption-1) == selectedOption){
                $('#select2-warehouse-results li[id$="all"]').text('Deselect All');
            }else{
                $('#select2-warehouse-results li[id$="all"]').text('Select All');
            }
        })

    }

    return {

        //main function to initiate the module
        init: function () {
            handleRecords();
            pageInteraction();
            handleFormSettings();
            warehouseOptionHandler();
            /*handleBootstrapSelect();
            handleDatePickers();

            */
        }

    };
}();

jQuery(document).ready(function() {
    Ref_supplier.init();
});


	'use strict';
	
	var ChangePasswordHandler = {
		params: function(options){
			
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "1000",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
			
			var handler = {
				_initialize: function(){
					var self = this;
					
					$('.change-password').on('click', function(){
						self._showModal();
					});
					
					$(document).on('submit', '#change-password-modal-form', function(e){
						e.preventDefault();
						self._checkOldPassword();
					});
					
					$(document).on('keyup', '#old', function(){
						var ctrl = $(this);
						
						if(ctrl.val().length > 0){
							$('.old-group').removeClass('has-error');
							$('.old-msg').html('');
						}
					});
					
					$(document).on('keyup', '#new', function(){
						var ctrl = $(this);
						
						if(ctrl.val().length > 0){
							$('.new-group').removeClass('has-error');
							$('.new-msg').html('');
						}
					});
					
					$(document).on('keyup', '#confirm', function(){
						var ctrl = $(this);
						
						if(ctrl.val().length > 0){
							$('.confirm-group').removeClass('has-error');
							$('.confirm-msg').html('');
						}
					});
				},
				
				_validate: function(){
					var self = this;
					
					var bCheck = true;
					
					if(!/\S/.test($('#old').val())){
						$('.old-group').addClass('has-error');
						$('.old-msg').html('Old Password is required');
						
						bCheck = false;
					}
					
					if(!/\S/.test($('#new').val())){
						$('.new-group').addClass('has-error');
						$('.new-msg').html('New Password is required');
						
						bCheck = false;
					}
					
					if(!/\S/.test($('#confirm').val())){
						$('.confirm-group').addClass('has-error');
						$('.confirm-msg').html('Confirm Password is required');
						
						bCheck = false;
					}else{
						
						if($('#new').val() != $('#confirm').val()){
							$('.confirm-group').addClass('has-error');
							$('.confirm-msg').html('Password not match');
							
							bCheck = false;
						}
						
					}
					
					return bCheck;
				},
				
				_checkOldPassword: function(){
					var self = this;
					
					var valid = self._validate();
					if(!valid)
						return false;
					
					App.blockUI({
						target: '#change-password-modal-form'
					});
					$.ajax({
						url: options.baseUrl + 'user/checkOldPassword',
						type: 'POST',
						dataType: 'JSON',
						data: {
							user_id: $('#change-password-modal-form input[name="id"]').val(),
							old: $('#old').val()
						},
						success: function(response){
							if(response.status == 'OK'){
								
								self._changePassword();
								
							}else{
								App.unblockUI('#change-password-modal-form');
								toastr['error'](response.message, "ERROR");
							}
						},
						error: function(response){
							App.unblockUI('#change-password-modal-form');
							
							if(response.status == 401)
								location.href = baseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
					});
					
				},
				
				_changePassword: function(){
					var self = this;
					
					$.ajax({
						url: options.baseUrl + 'user/saveNewPassword',
						type: 'POST',
						dataType: 'JSON',
						data: {
							user_id: $('#change-password-modal-form input[name="id"]').val(),
							new_password: $('#new').val()
						},
						success: function(response){
							if(response.status == 'OK'){
								
								$('#modal-change-password').modal('hide');
								toastr['success'](response.message, "Success");
								
							}else{
								toastr['error'](response.message, "ERROR");
							}
							
							App.unblockUI('#change-password-modal-form');
						},
						error: function(response){
							App.unblockUI('#change-password-modal-form');
							
							if(response.status == 401)
								location.href = baseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
					});
				},
				
				_showModal: function(){
					var self = this;
					console.log('here');
					$('#modal-change-password').remove();
					
					var tpl = '<div id="modal-change-password" class="modal fade" role="basic">\
									<div class="modal-dialog">\
										<div class="modal-content">\
											<form id="change-password-modal-form" class="form-horizontal">\
												<input type="hidden" name="id" value="' + options.userId + '" />\
												<div class="modal-header">\
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\
													<h4 class="modal-title">Change Password</h4>\
												</div>\
												<div class="modal-body">\
													<div class="row">\
														<div class="col-md-12">\
														   <div class="form-group old-group" style="margin-bottom: 5px;">\
																<label class="col-md-4 control-label">Old Password<span class="required">*</span></label>\
																<div class="controls col-md-6">\
																	<input type="password" id="old" name="old" placeholder="Old Password" class="form-control" />\
																	<span class="help-block old-msg"></span>\
																</div>\
															</div>\
															<div class="form-group new-group" style="margin-bottom: 5px;">\
																<label class="col-md-4 control-label">New Password<span class="required">*</span></label>\
																<div class="controls col-md-6">\
																	<input type="password" id="new" name="new" placeholder="New Password" class="form-control" />\
																	<span class="help-block new-msg"></span>\
																</div>\
															</div>\
															<div class="form-group confirm-group" style="margin-bottom: 5px;">\
																<label class="col-md-4 control-label">Confirm Password<span class="required">*</span></label>\
																<div class="controls col-md-6">\
																	<input type="password" id="confirm" name="confirm" placeholder="Confirm Password" class="form-control" />\
																	<span class="help-block confirm-msg"></span>\
																</div>\
															</div>\
														</div>\
													</div>\
												</div>\
												<div class="modal-footer">\
													<div class="form-actions">\
														<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>\
														<button id="submit" type="submit" class="btn blue min-width120"><i class="fa fa-check"></i> &nbsp;SAVE CHANGES</button>\
													</div>\
												</div>\
											</form>\
										</div>\
									</div>\
								</div>';
								
					$('body').append(tpl);
					
					$('#modal-change-password').modal({
						backdrop: 'static',
						keyboard: 'true'
					});
				},
				
				init: function(){
					var self = this;
					self._initialize();
				}
			}
			
			return handler;
		}
	}
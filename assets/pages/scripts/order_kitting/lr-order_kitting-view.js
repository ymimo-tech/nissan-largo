var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var dataTable;

	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$('#periode, #doc-type, #doc-status').select2({
			allowClear: false,
			width: null
		});

		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});

		dateFilter.init();
		setTimeout(function(){
            $('#periode').val(current_filter).trigger('change');
			initTable();
		}, 500);
	}

	var reset = function(){
		$("#po").val(null).trigger("change");
		$('#customer').val(null).trigger('change');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_month');
		$('#doc-type').select2('val', '');
	}

    var initTable = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
                current_filter=response.current_filter;
            },
            onError: function (grid) {

            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getList", // ajax source
					"data": {
						do_number: $('#do').val(),
						id_customer: $('#customer').val(),
						from: $('#from').val(),
						to: $('#to').val(),
						doc_type: $('#doc-type').val(),
						doc_status: $('#doc-status').val(),
                        periode:$('#periode').val(),
                        warehouse : $('#warehouse').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false, "visible": false },
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": false }
                  ],
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
				"columnDefs": [
					{
						"render": function ( data, type, row ) {
							return '<div align="center">'+data+'</div>';
						},
						"targets": [1,2,3,4]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl+'inbound/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="kd_inbound"]').val(json.kd_inbound);
					$('#data-table-form input[name="tanggal_inbound"]').val(json.tanggal_inbound);
					$('#data-table-form select[name="id_supplier"]').val(json.id_supplier).selectpicker("refresh");
					$('#data-table-form select[name="id_inbound_document"]').val(json.id_inbound_document).selectpicker("refresh");
					$('#data-table-form #added_rows').append(json.inbound_barang_html);
					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});
					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});
					$(".remove_row").click(function(){
						$(this).closest("tr").remove();
					});
				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var closeOrderKitting = function(id){
		App.blockUI();
		$.ajax({
			url: BaseUrl + page_class + '/closeOrderKitting',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_order_kitting: id
			},
			success: function(response){
				if(response.status == 'OK'){

					dataTable.getDataTable().ajax.reload();
					toastr['success'](response.message, "SUCCESS");

				}else{
					toastr['error'](response.message, "Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var finishTallyOrderKitting = function(id){
		App.blockUI();
		$.ajax({
			url: BaseUrl + page_class + '/finishTallyOrderKitting',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_order_kitting: id
			},
			success: function(response){
				if(response.status == 'OK'){

					dataTable.getDataTable().ajax.reload();
					toastr['success'](response.message, "SUCCESS");

				}else{
					toastr['error'](response.message, "Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    var pageInteraction = function(){

    	$('#warehouse').change(function(){
    		initTable();
    	});

		$("#customer")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'order_kitting/getCustomerBySearch',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_customer,
							text: item.customer_name
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$("#do")
		  .select2({
			multiple: false,
			allowClear: true,
			width: null,
			placeholder: "-- All --",
			minimumInputLength: 3,
			ajax: {
				url: BaseUrl + 'order_kitting/getOrderKittingCode',
				dataType: 'json',
				type: 'POST',
				data: function (q, page) {
					return {
						query: q.term
					};
				},
				processResults: function (data, params) {
					var results = [];

					$.each(data.result, function(index, item){
						results.push({
							id: item.id_order_kitting,
							text: item.kd_order_kitting
						});
					});
					return {
						results: results
					};
				}
			}
		});

		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});

		$('#reset').on('click', function(){
			reset();
		});

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            location.href = BaseUrl + page_class + "/edit/" + id;
        });

		$(document).on('click','.data-table-close',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            closeOrderKitting(id);
        });

		$(document).on('click','.data-table-finish',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            finishTallyOrderKitting(id);
        });

        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();

			if($(this).hasClass('disabled-link'))
				return false;

            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want remove this data ?", function(result) {
                if(result){
					App.blockUI();
                    $.ajax({
                        type:'POST',
                        url:BaseUrl + page_class + '/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.success){

								if(json.status == 'OK'){
									toastr['success'](json.message,"Success");
									dataTable.getDataTable().ajax.reload();
								}else{
									toastr['error'](json.message, "Error");
								}

                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You don't have autority to delete this data","Warning");
                                }else{
                                    toastr['error']("Please refresh the page and try again later.","Error Occured");
                                }
                            }
							App.unblockUI();
                        },
						error: function(response){
							App.unblockUI();
							if(response.status == 401)
								location.href = BaseUrl + 'login';
							else
								bootbox.alert(response.responseText);
						}
                    });
                }
            });
        });
    }

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_inbound: {
                    required: true
                },
                tanggal_inbound: {
                    required: true
                },
                id_supplier: {
                    required: true
                },
                id_inbound_document: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Periksa kembali form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+'inbound/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Data barang berhasil diperbaharui.","Success");
                            }else{
                                toastr['success']("Data barang berhasil ditambahkan.","Success");
                            }
                            dataTable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk menambah data.","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("Anda tidak mempunyai otoritas untuk merubah data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

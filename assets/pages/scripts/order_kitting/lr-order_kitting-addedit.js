var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var content_datatable,
		idDestination;

	var initialize = function(){

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: moment().format('DD/MM/YYYY')
		});

		var id = $('input[name="id"]').val();
		if(id.length > 0)
			edit(id);

		$('#data-table-form select[name="id_supplier"], #data-table-form select[name="id_order_kitting_document"], #data-table-form select[name="type"]').select2();

		if($('#data-table-form select[name="id_inbound_status"]') != 'undefined'){
			$('#data-table-form select[name="id_inbound_status"]').select2();
		}

		$('#destination, #warehouse').select2();
	}

	var reset = function(){
		$('#data-table-form input[name="id"]').val('');
		$('#data-table-form input[name="params"]').val('');
		$('#data-table-form input[name="kd_order_kitting"]').val('');
		$('#data-table-form input[name="tanggal_order_kitting"]').val(moment().format('DD/MM/YYYY'));
		$('#data-table-form select[name="destination"]').val(null).trigger("change");
		$('#data-table-form select[name="id_order_kitting_document"]').val(null).trigger('change');
		$('#data-table-form #added_rows').html('');

		$('#data-table-form input[name="kd_order_kitting"]').focus();
	}

	var edit = function(id){
		//var id=$(this).attr('data-id');
		App.blockUI();

		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				if(! json.no_data){
					//$('#data-table-form input[name="id"]').val(json.id_inbound);
					$('#data-table-form input[name="kd_order_kitting"]').val(json.kd_order_kitting);
					$('.order_kitting-code').html(json.kd_order_kitting);
					//$('#data-table-form input[name="tanggal_order_kitting"]').val(json.tanggal_order_kitting);
					//$('#data-table-form input[name="tanggal_order_kitting"]').datepicker("update", json.tanggal_order_kitting);

					//$('#data-table-form select[name="id_supplier"]').val(json.id_supplier);
					$('#data-table-form select[name="id_order_kitting_document"]').val(json.id_order_kitting_document).trigger("change");
					$('#data-table-form select[name="id_inbound_status"]').val(json.id_status_order_kitting).trigger("change");
					$('#data-table-form select[name="type"]').val(json.order_kitting_type_id).trigger("change");
					$('#warehouse').val(json.warehouse_id).trigger("change");
					$('#data-table-form #added_rows').append(json.order_kitting_barang_html);

					idDestination = json.id_customer;

					$('.date-picker').datepicker('remove');

					$('.date-picker').datepicker({
						orientation: "left",
						format: 'dd/mm/yyyy',
						autoclose: true,
						startDate: json.tanggal_order_kitting
					});

					$('.date-picker').datepicker("update", json.tanggal_order_kitting);

					$('#data-table-form select.add_rule').each(function() {
						$(this).rules("add", "required");
					});

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

					//$(".selectpicker").selectpicker("refresh");

					$('#data-table-form input.add_rule').each(function() {
						$(this).rules("add", "required");
					});

					var j = 0;
					$('.added_row').each(function(){
						console.log('heress');
						$(this).find('select.add_rule').attr('name', 'id_kitting['+j+']');
						$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

						j++;
					});

					// if($('#data-table-form input[name="not_used"]').val() == 'true'){
                    //
						$(".remove_row").click(function(){
							$(this).closest("tr").remove();

							var j = 0;
							$('.added_row').each(function(){
								$(this).find('select.add_rule').attr('name', 'id_kitting['+j+']');
								$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

								j++;
							});
						});
                    //
					// }else{
					// 	$('select[name="id_barang[]"]').attr('disabled', '');
					// 	$('input[name="item_quantity[]"]').attr('disabled', '');
					// 	$('.btn-delete').hide();
					// 	$('.add_item').parent('tr').hide();
					// }
				}else{
					toastr['error']("Could not load data.","Error");
				}

				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getCustomer = function(){
		$.ajax({
			url: BaseUrl + 'inbound/getCustomer',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_customer);
						row = row.replace(/{{ text }}/gi, data[i].customer_name);
					}

					$('#destination').select2('destroy');

					$('#destination').html(row);
					$('#destination').select2();

					$('#destination').val(idDestination).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getSupplier = function(){
		$.ajax({
			url: BaseUrl + 'inbound/getSupplier',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_supplier);
						row = row.replace(/{{ text }}/gi, data[i].kd_supplier + ' - ' + data[i].nama_supplier);
					}

					$('#destination').select2('destroy');

					$('#destination').html(row);
					$('#destination').select2();
						console.log(idDestination);
					$('#destination').val(idDestination).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	// var getBatch = function(ctrl){
	// 	$.ajax({
	// 		url: BaseUrl + 'order_kitting/getBatch',
	// 		type: 'POST',
	// 		dataType: 'JSON',
	// 		data: {
	// 			id_barang: ctrl.val()
	// 		},
	// 		success: function(response){
    //
	// 			if(response.is_batch == 'YES'){
    //
	// 				//var html = '<input type="text" name="batchcode['+ctrl.val()+']" value="" class="col-md-12 form-control add_rule" style="width: 100px; float: none; margin: auto; display: table;">';
    //
	// 				var html = '<select name="batchcode['+ctrl.val()+']" class="form-control mbatch add_rule" style="width: 100%;">\
	// 								{{ opt }}\
	// 							</select>',
	// 					opt = '';
    //
	// 				var data = response.data,
	// 					len = data.length;
    //
	// 				for(var i = 0; i < len; i++){
	// 					opt += '<option value="'+data[i].kd_batch+'">'+data[i].kd_batch+'</option>';
	// 				}
    //
	// 				ctrl.parent().parent().find('.batch-column').html(html.replace(/{{ opt }}/gi, opt));
    //
	// 				setTimeout(function(){
	// 					$('.mbatch').select2();
	// 				}, 200);
    //
	// 			}else{
    //
	// 				var html = '-'
	// 				ctrl.parent().parent().find('.batch-column').html(html);
    //
	// 			}
	// 		},
	// 		error: function(response){
	// 			if(response.status == 401)
	// 				location.href = BaseUrl + 'login';
	// 			else
	// 				bootbox.alert(response.responseText);
	// 		}
	// 	});
	// }

    var pageInteraction = function(){

		// $(document).on('change', 'select.item-order_kitting', function(){
        //
		// 	var ctrl = $(this);
        //
		// 	$.ajax({
		// 		url: BaseUrl + 'inbound/getUomByItem',
		// 		type: 'POST',
		// 		dataType: 'JSON',
		// 		data: {
		// 			id_barang: $(this).val()
		// 		},
		// 		success: function(response){
		// 			ctrl.parent().parent().find('label.uom').html(response.nama_satuan);
		// 		},
		// 		error: function(response){
		// 			if(response.status == 401)
		// 				location.href = BaseUrl + 'login';
		// 			else
		// 				bootbox.alert(response.responseText);
		// 		}
		// 	});
		// });

		$(document).on('change', '#data-table-form select.item-order_kitting', function(){
			$(this).closest('.form-group').removeClass('has-error');
		});

		$(document).on('keydown', '.add_rule', function(e){
			return isNumeric(e);
		});

		$('#save-new').on('click', function(){
			$('input[name="params"]').val('new');
			$('#data-table-form').submit();
		});

		/*
        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
		*/

        $(".add_item").click(function(e){
            $.ajax({
                url:BaseUrl + page_class + "/get_add_item_row",
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows").append("<tr id='target_new_row'><td colspan='4'>Please Wait</td></tr>");
                },
                success:function(html){
                    var seq = $('.added_row').length;
					var r = html.replace('id_kitting[]', 'id_kitting['+seq+']');
						r = html.replace('item_quantity[]', 'item_quantity['+seq+']');

                    $("#target_new_row").replaceWith(r);
                    $(".selectpicker").selectpicker("refresh");
                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();

						var j = 0;
						$('.added_row').each(function(){
							$(this).find('select.add_rule').attr('name', 'id_kitting['+j+']');
							$(this).find('input.add_rule').attr('name', 'item_quantity['+j+']');

							j++;
						});
                    });

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

                    $('#data-table-form select.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                    $('#data-table-form input.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                }
            });
        });
    }

	var newOrderKittingCode = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getNewOrderKittingCode',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){
					$('#data-table-form input[name="kd_order_kitting"]').val(response.order_kitting_code);
					$('#data-table-form .order_kitting-code').html(response.order_kitting_code);
				}else{
					toastr['error']("Generate new order_kitting code failed. Please refresh page","Form Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                kd_order_kitting: {
                    required: true
                },
                tanggal_order_kitting: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {

				var chk = true;
				var itemId = [];

				if($('#data-table-form input[name="tanggal_order_kitting"]').val().length != 10){
					$('.date-picker').addClass('has-error');
					chk = false;
				}

				if($('#added_rows tr').length <= 0){
					toastr['error']("Please add item first", "ERROR");
					return false;
				}

				$('#data-table-form select.item-order_kitting').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					itemId.push(val);

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					if(val == 'undefined' || val == '') {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}
				});

				$('#inbound-barang input.add_rule').each(function(){
					var ctrl = $(this),
						val = ctrl.val();

					if(!/\S/.test(val)) {
						ctrl.closest('.form-group').addClass('has-error');
						chk = false;
					}

					var id = $('input[name="id"]').val();
					if(id.length > 0){
						if(val <= 0) {
							ctrl.closest('.form-group').addClass('has-error');
							chk = false;
						}
					}
				});

				if(!chk){
					toastr['error']("Please check again.","Form Error");
					return false;
				}

				var hasDuplicate = !itemId.every(function(v,i) {
					return itemId.indexOf(v) == i;
				});

				if(hasDuplicate){

					$('#data-table-form select.item-order_kitting').each(function(index1, item1){

						$.each($('#data-table-form select.item-order_kitting').not(this), function (index2, item2) {

							if ($(item1).val() == $(item2).val()) {
								$(item1).closest('.form-group').addClass('has-error');
							}

						});

					});

					toastr['error']("Duplicate item. Please fix it first","Form Error");
					return false;
				}

				App.blockUI();

                $.ajax({
					url: BaseUrl + page_class + '/checkExistDoNumber',
					type: 'POST',
					dataType: 'JSON',
					data: {
						kd_order_kitting: $('#kd-order_kitting').val(),
						id_order_kitting: $('#data-table-form input[name="id"]').val()
					},
					success: function(response){
						if(response.status == 'OK'){

							save(form);

						}else{
							App.unblockUI();
							toastr['error'](response.message, 'ERROR');
						}
					},
					error: function(response){
						App.unblockUI();
						if(response.status == 401)
							location.href = BaseUrl + 'login';
						else
							bootbox.alert(response.responseText);
					}
				});
            }
        });
    }

	var submitUpdate = function(form){

		$.ajax({
			url: BaseUrl + page_class + '/proses',
			type: 'POST',
			dataType: 'JSON',
			data: $(form).serialize() + '&delete=yes',
			success: function(response){
				if(response.success==1){
					toastr['success']("Save changes success.","Success");

					setTimeout(function(){
						location.href = BaseUrl + page_class;
					}, 200);
				}else{
					if(response.no_add){
						toastr['warning']("You dont have authority to add data.","Warning");
					}else if(response.no_edit){
						toastr['warning']("You dont have authority to change data..","Warning");
					}else{
						toastr['error']("Please refresh the page and try again later.","Error Occured");
					}
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

	var save = function(form){

		$.ajax({
			type:'POST',
			url: BaseUrl + page_class + '/proses',
			dataType:'json',
			data: $(form).serialize(),
			beforeSend:function(){
				//$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
				//$('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
				$('.form-group').removeClass('has-error');
			},
			success:function(json){
				if(json.success==1){
					if(json.edit==1){
						toastr['success']("Save changes success.","Success");
					}else{
						toastr['success']("Save success.","Success");
					}

					var params = $('input[name="params"]').val();

					if(params == 'new'){
						reset();
						//newOrderKittingCode();
						$('.detail-item-add').hide();
					}else{
						setTimeout(function(){
							location.href = BaseUrl + page_class;
						}, 500);
					}
					//content_datatable.getDataTable().ajax.reload();
					//$('#form-content').modal('hide');
				}else{
					if(json.no_add){
						toastr['warning']("You dont have authority to add data.","Warning");
					}else if(json.no_edit){
						toastr['warning']("You dont have authority to change data..","Warning");
					}else{

						if(json.status == 'ERR'){

							toastr['error'](json.message,"Error");

							// bootbox.confirm(json.message, function(result) {
							// 	if(result){
							// 		submitUpdate(form);
							// 	}
							// });

						}else{

							toastr['error']("Please refresh the page and try again later.","Error Occured");

						}
					}
				}
				App.unblockUI();
				//$('.modal-content').unblock();
				//$('.spinner').remove();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            handleFormSettings();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

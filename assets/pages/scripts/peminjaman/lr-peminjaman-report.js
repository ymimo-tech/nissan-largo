var Handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
    
    var dataTable;
	
	var initialize = function(){
		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true
		});
		
		$('#customer, #periode').select2({
			allowClear: false,
			width: null
		});
		
		var dateFilter = MimoPeriode.params({
			selector :{
				periode: '#periode',
				from: '#from',
				to: '#to',
				clear: '#clear-date'
			}
		});
		
		dateFilter.init();
		setTimeout(function(){
			initTable();
		}, 500);
	}
	
	var reset = function(){
		$('#customer').val('').trigger('change');
		$('#from, #to').val(moment().format('DD-MM-YYYY'));
		$('#periode').select2('val', 'this_month');
	}
	
	var initTable = function () {
		
		var btn = '<div align="center"><button class="btn green btn-xs detail" type="button">Show Detail</button></div>';
		
        dataTable = new Datatable();
		
        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
				$('#replenishment_loading').hide();
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
			
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {

				},
                "ajax": {
                    "url": BaseUrl + "peminjaman_report/getList", // ajax source
					"data": {
						id_customer: $('#customer').val(),
						from: $('#from').val(),
						to: $('#to').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">'+data+'</div>');
						},
						"targets": [0]
					},
					{
						"render": function(data, type, row){
							return '<div align="right">' + data + '</div>';
						},
						"targets": [4, 5]
					},
					{
						"targets": -1,
						"data": null,
						"defaultContent": btn,
						"orderable": false
					}
				]
            }
        });
    }
	
    var pageInteraction = function(){
		
		$('.btn-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{
				id_barang: $('#item').val()
			}, '', 'Y');
		});
		
		$('#form-search').on('submit', function(e){
			e.preventDefault();
			initTable();
		});
		
		$('#reset').on('click', function(){
			reset();
		});		
    }
        
    return {

        init: function () {
			initialize();
            pageInteraction();
        }

    };  
}();

jQuery(document).ready(function() {
	Handler.init();
});
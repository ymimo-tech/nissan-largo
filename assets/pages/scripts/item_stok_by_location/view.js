var ItemStok = function () {

    var content_datatable,
		PARAMS = '';

	var initialize = function(){
		$('#item, #status, #cat, #loc, #loc-type, #uom').select2();
	}

    var handleRecords = function (params) {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
        				if(response.customGroupAction=='OK'){
        					toastr['success'](response.customGroupActionMessage,"Success");
        				}
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: {
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false,
        				"bDestroy": true,
        				"autoWidth": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": BaseUrl + page_class + "/getList", // ajax source
          					"data":{
          						id_barang: $('#item').val(),
          						id_kategori: $('#cat').val(),
          						loc_id: $('#loc').val(),
          						loc_type: $('#loc-type').val(),
                      warehouse: $('#warehouse-filter').val(),
          						params: params
          					}
                },
        				"columns": [
          					{ "orderable": false },
          					{ "orderable": true },
          					{ "orderable": true },
                    { "orderable": true },
          					{ "orderable": false },
                    { "orderable": true }
        			  ],
                "order": [
                    [5, "DESC"]
                ],
        				"columnDefs": [
                    {"className": "dt-center", "targets": [0,1,2,3,5]}
                ]
            }
        });
    }

    var pageInteraction = function(){

        $('#warehouse-filter').change(function(){
            handleRecords();
        });

  		$('#export').on('click', function(){
  			post(BaseUrl + page_class + '/export',
  			{
  				id_barang: $('#item').val(),
  				id_kategori: $('#cat').val(),
  				loc_id: $('#loc').val(),
  				loc_type: $('#loc-type').val(),
          warehouse: $('#warehouse-filter').val(),
  				params: PARAMS
  			});
  		});

  		$('#form-search').on('submit', function(e){
  			e.preventDefault();

  			PARAMS = '';

  			handleRecords();
  		});

  		$('#reset').on('click', function(){
  			$('#item, #status, #cat, #loc, #loc-type, #uom').val(null).trigger('change');
  		});

      $('#loc').select2({
            ajax: {
                url: BaseUrl + "item_stok/get_location_list",
                dataType: 'json',
                method: 'POST',
                delay: 250,
                cache: false,
                data: function (params) {
                    return {
                        term: params.term,
                        page: params.page
                    };
                },
                processResults: function(data) {
                    var result = $.map(data, function (loc) { return { id: loc.id, text: loc.name }});
                    return { results: result };
                }
            }
        });

      $("#item")
        .select2({
          multiple: false,
          allowClear: true,
          width: null,
          placeholder: "-- All --",
          minimumInputLength: 3,
          ajax: {
              url: BaseUrl + 'item_stok_by_location/option',
              dataType: 'json',
              type: 'POST',
              data: function (q, page) {
                  return {
                      query: q.term
                  };
              },
              processResults: function (data) {
                  return {
                      results: $.map(data.results, function (item) {
                          return {
                              text: item.name,
                              id: item.id
                          }
                      })
                  };
              }
          }
      });
    }

    return {

      //main function to initiate the module
      init: function () {
  			initialize();
        handleRecords();
        pageInteraction();
      }
    };
}();

jQuery(document).ready(function() {
    ItemStok.init();
});

var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable,
		dataTable2,
		reprint = [];

	var initialize = function(){
		initTable();
		itemListHandler();
	}

	var initChildTable = function(data){
		childTable = new Datatable();
		console.log(data);
		console.log('succes');
		console.log($('#warehouse-filter').val());
		console.log($('input[name="id"]').val());

        childTable.init({
            src: $("#child-table-"+data[0]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"language": {
					"infoEmpty": ""
				},
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/get_list_detail/", // ajax source
					"data": {
						location_id: $('input[name="id"]').val(),
						warehouse: $('#warehouse-filter').val(),
						item_id : data[5]
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					// { "orderable": false },
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true }
                ],
                "order": [
                    [2, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1, 2, 3, 4, 5, 6]
					}
				]
            }
        });
	}

	var initTable = function (params) {

        dataTable = new Datatable();

        dataTable.init({
            src: $("#table_receiving"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/get_list_detail/", // ajax source
					"data": {
						location_id: $('input[name="id"]').val(),
						warehouse: $('#warehouse-filter').val(),
						params: params
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true }
                ],
                "order": [
                    [2, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [1, 2, 3, 4, 5, 6]
					}
				]
            }
        });
    }


	var showModalPrint = function(){

		var c = 0;

		reprint = [];

		$('#table_receiving tbody input[type="checkbox"]').each(function(){
			if($(this).is(':checked')){
				reprint.push($(this).val());
				c++;
			}
		});

		$('.qty-print').html(c);

		$('#modal-print').modal({
			backdrop: 'static',
			keyboard: true
		});

	}

	var printBarcode = function(){

		var $btn = $('#submit-print');
			$btn.button('loading');

		$.ajax({
			url: BaseUrl + page_class + '/printZpl',
			type: 'POST',
			dataType: 'JSON',
			data: {
				sn: reprint
			},
			success: function(response){

				if(response.status == 'OK'){
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				}else
					bootbox.alert(response.message);

				$('#modal-print').modal('hide');
				$btn.button('reset');

			},
			error: function(response){
				$btn.button('reset');
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var itemListHandler = function (params) {

        dataTable2 = new Datatable();

        dataTable2.init({
            src: $("#table-item-list"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
            },
            onDataLoad: function(grid) {
            },

            loadingMessage: 'Loading...',
            dataTable: {
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false,
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150]
                ],
                "pageLength": 10,
				"initComplete": function(settings, json) {
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/get_list_detail_item/", // ajax source
					"data": {
						location_id: $('input[name="id"]').val(),
						warehouse: $('#warehouse-filter').val(),
						params: params
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return '<div align="center"><button class="btn green btn-xs location" type="button">\
										Show Detail\
									</button></div>';
						},
						"targets": [4]
					},

					{"className": "dt-center", "targets": [1,2,3]}
				]
            }
        });
	}

	var childTableTpl = function(data){
		var tpl = '<div style="width: 90%; margin: 0 auto; text-align: left;">\
						<h4>Serial Number List</h4>\
				   </div>\
				   <table id="child-table-'+data[0]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
						<thead>\
							<tr>\
								<th><div align="center">No.</div></th>\
								<th><div align="center">LPN</div></th>\
								<th><div align="center">GID</div></th>\
								<th><div align="center">Qty</div></th>\
								<th><div align="center">Doc.Reference #</div></th>\
								<th><div align="center">Expired Date</div></th>\
								<th><div align="center">Date In</div></th>\
								<th><div align="center">Status</div></th>\
							</tr>\
						</thead>\
						<tbody>\
						</tbody>\
				   </table>';

		return tpl;
	}


	var pageInteraction = function(){

		$('#table-item-list').on('click', '.location', function(e){
			e.preventDefault();

			var tr = $(this).closest('tr');
			var row = dataTable2.getDataTable().row( tr );


			console.log(tr);
			console.log(row.data());

			if(tr.hasClass('shown')){
				console.log('here');
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');

				childTable.getDataTable().destroy();
			}else{
				console.log('hello');
				$('#table-item-list tbody tr').each(function(){
					var type = $(this).attr('role');

					//if(type === undefined){
						//$(this).hide();
						var r = dataTable.getDataTable().row( $(this) );
						r.child.hide();
						$(this).removeClass('shown');
					//}
				});

				console.log(row.data());

				// Open this row
				row.child( childTableTpl(row.data()) ).show();
				tr.addClass('shown');

				initChildTable(row.data());
			}

		});

    	$('#warehouse-filter').change(function(){
    		initialize();
    	})

		$('#reload').on('click', function(){
			initTable();
		});

		$('#reprint').on('click', function(){
			if(!$('#table_receiving tbody input[type="checkbox"]').is(':checked')){
				toastr['error']('Please check row at least one for reprint', 'ERROR');
				return false;
			}

			showModalPrint();
		});

		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});

		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});

		$('#modal-print').on('shown.bs.modal', function () {
			$('#qty-print').focus();
		});

		$('#button-print-qty-barcode').on('click', function(e){
			e.preventDefault();

			var id = $('#data-table-form input[name="id"]').val();

			showModalPrint(id);
		});

		$('#export-detail').click(function(){
			post(BaseUrl + page_class + '/get_list_detail_export',{
				location_id: $('input[name="id"]').val(),
				warehouse: $('#warehouse-filter').val()
			});
		})

		$('#export-detail-item').click(function(){
			post(BaseUrl + page_class + '/get_list_detail_item_export',{
				location_id: $('input[name="id"]').val(),
				warehouse: $('#warehouse-filter').val()
			});
		})
	}

    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    handler.init();
});

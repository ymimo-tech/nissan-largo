var drivers = function () {

    toastr.options = {
        "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

    var content_datatable;
    var handleRecords = function () {

        content_datatable = new Datatable();

        content_datatable.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
            },
            onDataLoad: function(grid) {
            },
            loadingMessage: 'Loading...',
            dataTable: {

                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false,
                "bDestroy":true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150]
                ],
                "pageLength": 10,
                "ajax": {
                    "url": BaseUrl+page_class+"/get_list",
                    "data":{
                        source_code_filter: $('input[name="source_code_filter"]').val(),
                        source_name_filter: $('input[name="source_name_filter"]').val(),
                        source_contact_person_filter: $('input[name="source_contact_person_filter"]').val(),
                        source_city_filter: $('input[name="source_city_filter"]').val(),
                        source_type_filter: $('select[name="source_type_filter"]').val()
                    }
                },
                "columns": [
                    { "orderable": false },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    { "orderable": false }
                  ],
				
                "order": [
                    [1, "desc"]
                ],
				
				"columnDefs": [
				]
            }
        });
    }

	var getEdit = function(id){
		 $.ajax({
			type:'POST',
			url:BaseUrl+page_class+'/get_data',
			data:{'id':id},
			dataType:'json',
			success:function(json){

				if(! json.no_data){

					$('#referensi-supplier-form input[name="id"]').val(json.driver_id);
					$('#referensi-supplier-form input[name="driver_name"]').val(json.driver_name);
                    $('#referensi-supplier-form input[name="driver_type"]').val(json.driver_type);
                    $('#referensi-supplier-form input[name="vehicle_plate"]').val(json.vehicle_plate);
                    $('#referensi-supplier-form input[name="description"]').val(json.description);
                    $('#referensi-supplier-form input[name="length"]').val(json.length);
                    $('#referensi-supplier-form input[name="height"]').val(json.height);
                    $('#referensi-supplier-form input[name="width"]').val(json.width);
                    $('#referensi-supplier-form input[name="production_date"]').val(json.production_date);
                    $('#referensi-supplier-form input[name="vehicle_type"]').val(json.vehicle_type);

					$('#form-content').modal({
						"backdrop": "static",
						"keyboard": true,
						"show": true
					});

				}else{
					toastr['error']("Could not load data.","Error");
				}
			}
		});
	}

    var pageInteraction = function(){

        $('#source_category_id, select[name="source_type_filter"]').select2();

        /* Filter Handler */

        $('#reset').on('click', function(){
            $('input[name="source_code_filter"]').val('');
            $('input[name="source_name_filter"]').val('');
            $('input[name="source_contact_person_filter"]').val('');
            $('input[name="source_city_filter"]').val('');
            $('select[name="source_type_filter"]').val('').trigger('change');
        });

        $('#form-search').on('submit', function(e){
            e.preventDefault();
            handleRecords();
        });

        /* End - Filter Handler */

		$('.data-table-export').on('click', function(){
			post(BaseUrl + page_class + '/export',
			{

			});
		});

		var id = window.location.hash;
		if(id != ''){
			getEdit(id.replace(/\#/gi, ''));
			parent.location.hash = '';
		}

        $("#form-content").on('hidden.bs.modal', function(){
            $('.form-group').removeClass('has-error');
            $('#form-content input,#form-content textarea').val('');
        });

        $('.data-table-add').click(function(e){
            e.preventDefault();

            $('#warehouse').val(null).trigger('change');
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });

        $('.form-close').click(function(e){
            e.preventDefault();
            $('#form-content').modal('hide');
        });

        $(document).on('click','.data-table-edit',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
			getEdit(id);
        });

        $(document).on('click','.data-table-delete',function(e){
            e.preventDefault();
            var id=$(this).attr('data-id');
            bootbox.confirm("Are you sure want to remove data ?", function(result) {
                if(result){
                    console.log(result);
                    $.ajax({
                        type:'POST',
                        url:BaseUrl+page_class+'/delete',
                        data:{'id':id},
                        dataType:'json',
                        success:function(json){
                            if(json.status == 'OK'){
                                toastr['success'](json.message,"Success");
                                content_datatable.getDataTable().ajax.reload();
                            }else{
                                if(json.no_delete){
                                    toastr['warning']("You dont have authority to delete data.","Warning");
                                }else{
                                    toastr['error'](json.message,"Error Occured");
                                }
                            }
                        }
                    });
                }
            });
        });

        $(document).on('click','.btn-sync',function(){

            var bsync = $(this);
            bsync.attr('disabled',true);
            bsync.prop('disabled',true);
            bsync.html('<i class="fa fa-refresh"></i> Please wait');

            $.ajax({
    			url: BaseUrl + 'dashboard/manual_sync',
    			type: 'POST',
    			dataType: 'JSON',
                success: function(json){
                    if(json.newc.supplier.length>0){
                        $.each(json.newc.supplier, function (i,newc){
                            if(newc.newrec){
                                var title="New Supplier";
                            }else{
                                var title="Supplier Updated";
                            }
                            toastr['info']("<a href='"+BaseUrl+"referensi_supplier' target='_blank'>"+newc.kd_supplier+"</a><br/>"+newc.nama,title);
                        });
                        content_datatable.getDataTable().ajax.reload();
                    }

                    bsync.attr('disabled',false);
                    bsync.prop('disabled',false);
                    bsync.html('<i class="fa fa-refresh"></i> SYNC');

    			}
    		});
        });
    }

    var handleFormSettings = function() {
        $('#referensi-supplier-form').validate({
            debug: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                source_code: {
                    required: true
                },
                source_name: {
                    required: true
                },
                source_address: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check form input.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {
                $.ajax({
                    type:'POST',
                    url:BaseUrl+page_class+'/proses',
                    dataType:'json',
                    data: $(form).serialize(),
                    beforeSend:function(){
                        $('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
                        $('#referensi-supplier-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
                        $('.form-group').removeClass('has-error');
                    },
                    success:function(json){
                        console.log(json);
                        if(json.success==1){
                            if(json.edit==1){
                                toastr['success']("Change data success.","Success");
                            }else{
                                toastr['success']("Save data success.","Success");
                            }
                            content_datatable.getDataTable().ajax.reload();
                            $('#form-content').modal('hide');
                        }else{
                            if(json.no_add){
                                toastr['warning']("You dont have authority to add data","Warning");
                            }else if(json.no_edit){
                                toastr['warning']("You dont have authority to change data.","Warning");
                            }else{
                                toastr['error']("Please refresh the page and try again later.","Error Occured");
                            }
                        }
                        $('.modal-content').unblock();
                        $('.spinner').remove();
                    }
                });
            }
        });
    }

    var warehouseOptionHandler = function(){

        var row = '<option value="all">Select All</option>';
        row = row + $('#warehouse').html();

        $('#warehouse').html(row);

        $('#warehouse').select2();

        $('#warehouse').on('change', function(e){

            var ctrl = $(this);

            var allOption = $('#warehouse option').length;
            var selectedOption = $('#warehouse option:selected').length;

            if($.inArray('all',ctrl.val()) != -1){
                if(allOption != selectedOption){
                    $('#warehouse option').each(function(){
                        $(this).prop('selected',true);
                    })
                    $('#warehouse option[value="all"]').text('Deselect All');
                    $('#warehouse option[value="all"]').prop('selected',false);

                }else{

                    $('#warehouse option').each(function(){
                        $(this).prop('selected',false);
                    })
                    $('#warehouse option[value="all"]').text('Select All');

                }

            }
            else
            {

                if((allOption-1) == selectedOption){

                    $('#warehouse option[value="all"]').text('Deselect All');

                }else{

                    $('#warehouse option[value="all"]').text('Select All');

                }

            }
            $(this).trigger('change.select2');

        });

        $('#warehouse').on('select2:open', function(e){

            e.preventDefault();

            var allOption = $('#warehouse option').length;
            var selectedOption = $('#warehouse option:selected').length;
            if((allOption-1) == selectedOption){
                $('#select2-warehouse-results li[id$="all"]').text('Deselect All');
            }else{
                $('#select2-warehouse-results li[id$="all"]').text('Select All');
            }
        })

    }

    return {

        //main function to initiate the module
        init: function () {
            handleRecords();
            pageInteraction();
            handleFormSettings();
            warehouseOptionHandler();
        }

    };
}();

jQuery(document).ready(function() {
    drivers.init();
});

var Ref_barang = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

    var content_datatable,
		idDestination,
		dataTable;

    var shippingGroup = $('#gates_id').val();

	// console.log(shippingGroup);

	var initialize = function(){

		$('.date-picker').datepicker({
			orientation: "left",
			format: 'dd/mm/yyyy',
			autoclose: true,
			startDate: moment().format('DD/MM/YYYY')
		});

		var id = $('input[name="id"]').val();
		if(id.length > 0){
			// edit(id);
			$('#picking').attr('disabled','');
		}

		$('#picking, #shippingGroup, #outbound_document').select2();
	}

	var reset = function(){
		$('#data-table-form input[name="id"]').val('');
		$('#data-table-form input[name="params"]').val('');
		$('#data-table-form input[name="loading_code"]').val('');
		$('#data-table-form input[name="tanggal_loading"]').val(moment().format('DD/MM/YYYY'));
		$('#data-table-form input[name="driver"]').val('');
		$('#data-table-form input[name="plate"]').val('');
		//$('#data-table-form input[name="outbound"]').val('').trigger('change');
		$('#data-table-form input[name="picking"]').val('').trigger('change');
		$('#data-table-form #added_rows').html('');
		$('#table-list tbody').html('');

		$('#data-table-form input[name="driver"]').focus();
	}

	var getCustomer = function(){
		$.ajax({
			url: BaseUrl + 'inbound/getCustomer',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_customer);
						row = row.replace(/{{ text }}/gi, data[i].customer_name);
					}

					$('#destination').select2('destroy');

					$('#destination').html(row);
					$('#destination').select2();

					$('#destination').val(idDestination).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var getSupplier = function(){
		$.ajax({
			url: BaseUrl + 'inbound/getSupplier',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				if(response.status == 'OK'){

					var data = response.data,
						len = data.length,
						tpl = '<option value="{{ id }}">{{ text }}</option>',
						row = '';

					for(var i = 0; i < len; i++){
						row += tpl;
						row = row.replace(/{{ id }}/gi, data[i].id_supplier);
						row = row.replace(/{{ text }}/gi, data[i].kd_supplier + ' - ' + data[i].nama_supplier);
					}

					$('#destination').select2('destroy');

					$('#destination').html(row);
					$('#destination').select2();
						console.log(idDestination);
					$('#destination').val(idDestination).trigger('change');

				}else{
					toastr['error']("Get customer list failed","Error");
				}
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var initTable = function () {

        dataTable = new Datatable();

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
				},
                "ajax": {
					'url': BaseUrl + 'packing/getPackingList',

					"data": {
						packing_list: $('#outbound_document').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					// { "orderable": true, "visible": false },
					// { "orderable": false, "visible": false },
					// { "orderable": false, "visible": false },
					{ "orderable": false },
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
                    // { "orderable": true, "visible": false },
					// { "orderable": false, "visible": false },
					// { "orderable": false, "visible": false },
					// { "orderable": false, "visible": false },
					// { "orderable": true, "visible": false },
					// { "orderable": true, "visible": false },
					{ "orderable": false },
					{ "orderable": false }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1,2,3,4,5,6,7,8]
					},
					{
						"render": function(data, type, row){
							return '<div align="center">'+data+'</div>';
						},
						"targets": -1
					}
				]
            }
        });
    }

    var pageInteraction = function(){


		$('#outbound_document').on('change', function(){
			console.log('succes');
			console.log($('#outbound_document').val());
			if($('#outbound_document').val().length > 0) {
				initTable();
			}
		});

		$('#gates_id').on('change', function(){
			shippingGroup = $('#gates_id').val();
			$('#outbound_document').val('');
			console.log(shippingGroup);
		});

		$('#outbound').on('change', function(){
			var id = $('input[name="id"]').val();
			if(id.length <= 0)
				$('#data-table-form #added_rows').html('');
		});

		$('#plate').on('keyup', function(e){
			var ctrl = $(this);
			setTimeout(function(){
				ctrl.val(ctrl.val().toUpperCase());
			}, 100);
		});

		$(document).on('change', '.dnumber', function(){
			var data = $(this).select2('data')[0];
			var date = $(data.element).attr('data-date'),
				name = $(data.element).attr('data-name');

			$(this).closest('tr').find('.ddate').html('<div align="center">' + date + '</div>');
			$(this).closest('tr').find('.dcust').html(name);
		});

		$('select[name="id_outbound_document"]').on('change', function(){
			var ctrl = $(this),
				id = ctrl.val();

			App.blockUI();

			if(id == 2)
				getSupplier();
			else if(id == ''){
				App.unblockUI();
				$('#destination').select2('destroy');
				$('#destination').html('');
				$('#destination').select2();
			}else
				getCustomer();
		});

		$(document).on('change', '#data-table-form select.add_rule', function(){
			$(this).closest('.form-group').removeClass('has-error');
		});

		$(document).on('keydown', '.add_rule', function(e){
			return isNumeric(e);
		});

		$('#save-new').on('click', function(){
			$('input[name="params"]').val('new');
			$('#data-table-form').submit();
		});

		/*
        $('.data-table-add').click(function(e){
            e.preventDefault();
            $('#form-content').modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
        });
		*/

		$('#data-table-form').on('submit',function(e){
			e.preventDefault();
		})



        $(".add_item").click(function(e){
            $.ajax({
                url:BaseUrl + page_class + "/get_add_item_row",
				type: 'post',
                dataType:"html",
                beforeSend:function() {
                    $("#added_rows").append("<tr id='target_new_row'><td colspan='5'>Please Wait</td></tr>");
                },
				data: {
					id_outbound: $('#outbound').val()
				},
                success:function(html){
                    $("#target_new_row").replaceWith(html);
                    $(".selectpicker").selectpicker("refresh");
                    $(".remove_row").click(function(){
                        $(this).closest("tr").remove();
                    });

					$('#data-table-form select.add_rule').css('width', '100%');
					$('#data-table-form select.add_rule').select2();

                    $('#data-table-form select.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                    $('#data-table-form input.add_rule').each(function() {
                        //$(this).rules("add", "required");
                    });
                }
            });
		});


		$('#outbound_document').select2({
			multiple: true,
			allowClear: true,
			width: null,
			placeholder: "------------------------------------------ Packing Number ------------------------------------------",
			minimumInputLength: 1,
			ajax: {
				url: BaseUrl + page_class + '/shippingGroup',
				dataType: 'JSON',
				type: 'POST',
				data: function (q, page){
					return {
						query: q.term,
						shippingGroup: shippingGroup
					};
				},
				processResults: function (data) {
					return {
						results: $.map(data.results, function(res){
							return {
								text: res.code,
								id: res.id
							}
						})
					};
				}
			}
		});

        /*$('#plate')
          .select2({
            multiple: false,
            allowClear: true,
            width: null,
            placeholder: "Plate Number",
            minimumInputLength: 1,
            ajax: {
                url: BaseUrl + page_class +'/select2Options/vehicles/vehicle_plate/vehicle_plate',
                dataType: 'json',
                type: 'POST',
                data: function (q, page) {
                    return {
                        query: q.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (res) {
                            return {
                                text: res.text,
                                id: res.id
                            }
                        })
                    };
                }
            }
        });*/
    }

	var newLoadingCode = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getLoadingCodeIncrement',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){
					$('#data-table-form input[name="loading_code"]').val(response.loading_code);
					$('#data-table-form .loading-code').html(response.loading_code);
				}else{
					toastr['error']("Generate new outbound code failed. Please refresh page","Form Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

	var getPickingNumber = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getPickingNumber',
			type: 'POST',
			dataType: 'JSON',
			data: {

			},
			success: function(response){
				App.unblockUI();

				if(response.status == 'OK'){

					var data = response.data;
					var len = data.length,
						tpl = '<option value="{{ value }}">{{ text }}</option>',
						row = '';

					if(len > 0){
						row += '<option value="">-- Choose picking document --</option>';
						for(var i = 0; i < len; i++){
							row += tpl;
							row = row.replace(/{{ value }}/gi, data[i].pl_id);
							row = row.replace(/{{ text }}/gi, data[i].pl_name);
						}

						$('#picking').html(row);
						$('#picking').val(null).trigger('change');
					}else{
						row += '<option value="">-- Choose picking document --</option>';
						$('#picking').html(row);
						$('#picking').val(null).trigger('change');
					}

				}else{
					toastr['error']("Refresh picking number failed","Form Error");
				}
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

	}

    var handleFormSettings = function() {
        $('#data-table-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                driver:
                {
                  required: true
                },
                plate:
                {
                  required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                toastr['error']("Please check again.","Form Error");
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight:function(element){
                 $(element)
                    .closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            /*errorPlacement: function(error, element) {
                if(element.next('span').hasClass('help-inline')){
                    error.insertAfter(element.next('span.help-inline'));
                }else{
                    error.insertAfter(element);
                }

            },*/

            errorPlacement: function(error,element) {
                return true;
            },
            submitHandler: function(form) {

				App.blockUI();

                $.ajax({
					type:'POST',
					url: BaseUrl + page_class + '/update',
					dataType:'json',
					data: {
						id: $('#data-table-form input[name="id"]').val(),
						driver: $('#driver').val(),
						plate: $('#plate').val(),
						outbound_document: $('#outbound_document').val(),
					},
					beforeSend:function(){
						//$('.modal-content').block({message:null,overlayCSS:{'background':'transparent'}});
						//$('#data-table-form button[type="submit"]').before('<img src="'+BaseUrl+'assets/global/img/loading-spinner-grey.gif" class="spinner" style="margin-right:10px"/>');
						// $('.form-group').removeClass('has-error');
					},
					success: function(response){
						if(response.status == 'OK'){
							toastr['success']("Save changes success.","Success");
							setTimeout(function(){
								location.href = BaseUrl + page_class;
							}, 500);
						}else{
							toastr['error']("Please refresh the page and try again later.","Error Occured");
						}
						App.unblockUI();
						//$('.modal-content').unblock();
						//$('.spinner').remove();
					},
					error: function(response){
						App.unblockUI();
						if(response.status == 401){
							location.href = BaseUrl + 'login';

						}else{
							if(response.status == 200){
								toastr['success']("Save changes success.","Success");
								setTimeout(function(){
									location.href = BaseUrl + page_class;
								}, 500);
							}else{
								toastr['error']("Please refresh the page and try again later.","Error Occured");
							}
						}
					}
				});
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
			initialize();
            pageInteraction();
            handleFormSettings();
        }

    };
}();

jQuery(document).ready(function() {
    Ref_barang.init();
});

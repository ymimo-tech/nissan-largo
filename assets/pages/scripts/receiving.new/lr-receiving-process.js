var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }
	
	var dataTable;
	
	var initialize = function(){
		
		$('#rcv-code').select2();
		initRcvCode();
	}	
	
	var initRcvCode = function(){
		var val = $('#rcv').val();
			
		App.blockUI();
		
		$.ajax({
			url: BaseUrl + page_class + '/getRcvCode',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_receiving: val
			},
			success: function(response){
				$('#rcv-code').select2('data', null);
				$('#rcv-code').select2('destroy');
				
				//setTimeout(function(){
					$('#rcv-code').select2({
						data: response,
						templateResult: function (d) { 
							return $(d.text); 
						},
						templateSelection: function (d) { 
							return $(d.text); 
						}
					});
				//}, 300);
				
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}
	
	var initTable = function(id){
		
		App.blockUI();
		
		$.ajax({
			url: BaseUrl + page_class + '/getDetailItem',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_receiving: id
			},
			success: function(response){
				App.unblockUI();
				
				var tpl = '<tr>\
								<td><div align="center">{{ no }}</div></td>\
								<td><div align="center">{{ item_code }}</div></td>\
								<td>{{ item_name }}</td>\
								<td><div align="right">{{ qty_doc }}</div></td>\
								<td><div align="right">{{ qty_received }}</div></td>\
								<td><div align="right">{{ discrapency }}</div></td>\
						  </tr>';
						  
						  
				var data = response.data,
					iLen = data.length,
					row;
					
				for(var i = 0; i < iLen; i++){
					
					var qtyDoc = data[i].qty_doc,
						qtyReceived = data[i].qty_received,
						discrapency = 0,
						satuan = data[i].nama_satuan || '';
					
					if(parseInt(qtyDoc) > 0 && qtyDoc != '' && qtyDoc != null){
						discrapency = qtyDoc - qtyReceived;
						qtyDoc = '<input type="text" name="qty_doc" class="qty-doc input-xs table-input" data-id="'+data[i].id_barang+'" value="'+qtyDoc+'" />';
					}else{
						qtyDoc = '<input type="text" name="qty_doc" class="qty-doc input-xs table-input" data-id="'+data[i].id_barang+'" value="'+qtyDoc+'" />';
					}
					/*
					if(parseInt(qtyDoc) > 0 && qtyDoc != '' && qtyDoc != null){
						discrapency = qtyDoc - qtyReceived;
						qtyDoc = qtyDoc + ' ' + satuan;
					}
					*/
					if(parseInt(qtyReceived) > 0 && qtyReceived != '' && qtyReceived != null)
						qtyReceived = qtyReceived + ' ' + satuan;
					
					
					row += tpl;
					row = row.replace(/{{ no }}/gi, i + 1);
					row = row.replace(/{{ item_code }}/gi, data[i].kd_barang);
					row = row.replace(/{{ item_name }}/gi, data[i].nama_barang);
					row = row.replace(/{{ qty_doc }}/gi, qtyDoc);
					row = row.replace(/{{ qty_received }}/gi, qtyReceived);
					row = row.replace(/{{ discrapency }}/gi, discrapency);
				}
				
				if(iLen <= 0){
					row = '<tr><td colspan="6"><div align="center">No data</div></td></tr>';
				}
				
				$('#table-list tbody').html(row);
				
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
		
    }
	
	var save = function(){
		
		var barang = [];
		$('#table-list .qty-doc').each(function(){
			var ctrl = $(this),
				idBarang = ctrl.attr('data-id'),
				qty = ctrl.val();
				
			barang.push({id_barang: idBarang, qty: qty});
		});
		
		$.ajax({
			url: BaseUrl + page_class + '/saveReceiving',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_receiving: $('#rcv-code').val(),
				barang: barang
			},
			success: function(response){
				App.unblockUI();
				toastr['success'](response.message,"Success");
				
				initRcvCode();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}
	
	var showModalPrint = function(id){
		
		if(id){
			
			App.blockUI();
			$.ajax({
				url: BaseUrl + page_class + '/getQtyPrint',
				type: 'POST',
				dataType: 'JSON',
				data: {
					id_receiving: id
				},
				success: function(response){
					
					var data = response;
					$('#data-modal-form input[name="id"]').val(id);
					$('.qty-print-receiving').html(data.qty);
					
					$('.qty-print-receiving').show();
					$('#qty-print').hide();
					$('.required').hide();
					
					$('#modal-print').modal({
						backdrop: 'static',
						keyboard: true
					});
			
					App.unblockUI();
				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
			
		}else{
			
			$('#data-modal-form input[name="id"]').val('');
			
			$('#qty-print').val('');
			
			$('.qty-print-receiving').hide();
			$('#qty-print').show();
			$('.required').show();
			
			$('#modal-print').modal({
				backdrop: 'static',
				keyboard: true
			});
			
		}
	}
	
	var printBarcode = function(){
		
		var qty = $('#qty-print').val(),
			check = 0;
		if($('#data-modal-form input[name="id"]').val().length > 0){
			qty = $('.qty-print-receiving').html();
			check = 1;
		}
		
		if (!/\S/.test(qty)) {
			if(check == 0){
				$('.qty-print-group').addClass('has-error');
			}
			
			toastr['error']("Qty print cannot empty and must be greater than 0","Error");
			return false;
		}else{
			if(qty <= 0){
				
				if(check == 0){
					$('.qty-print-group').addClass('has-error');
				}
				
				toastr['error']("Qty print cannot empty and must be greater than 0","Error");
				return false;
			}
		}
		
		$.ajax({
			url: BaseUrl + page_class + '/printZpl',
			type: 'POST',
			dataType: 'JSON',
			data: {
				qty_print: qty
			},
			success: function(response){
				var zpl = response.zpl;
				
				var ctrl = $(zpl);
				
				var postdata = {
					sid: $('#sid').val(),
					pid: $('#pid').val(),
					printerCommands: zpl
				}
				// store user printer settings & commands in server cache
				$.post(BaseUrl + 'webclientprint/ProcessPrint.php',
					postdata,
					function() {
						 // Launch WCPP at the client side for printing...
						 var sessionId = $("#sid").val();
						 jsWebClientPrint.print('sid=' + sessionId);
					}
				);
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}
	
	var pageInteraction = function(){
		
		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});
		
		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});
		
		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});
		
		$('#modal-print').on('shown.bs.modal', function () { 
			$('#qty-print').focus();
		});
		
		$('#button-print-qty-barcode').on('click', function(e){
			e.preventDefault();
			
			var id = $('#rcv-code').val();
			
			showModalPrint(id);
		});
		
		$('#save').on('click', function(){
			save();
		});
		
		$('#rcv-code').on('change', function(){
			setTimeout(function(){
				var id = $('#rcv-code').val();
				
				getDetail(id);
				initTable(id);
			}, 300);
			
		}).change();
	}
	
	var getDetail = function(id){
		$.ajax({
			type:'POST',
			url:BaseUrl + page_class + '/getEdit',
			data:{'id':id},
			dataType:'json',
			success:function(json){
				
				if(! json.no_data){
					//$('#data-table-form ').val(json.id_receiving);
					//$('#data-table-form ').val(json.kd_receiving);
					$('#data-table-form .rcv-code').html(json.kd_receiving);
					$('#data-table-form .rcv-date').html(json.tanggal_receiving);
					$('#data-table-form .inb-code').html(json.kd_inbound);
					$('#data-table-form .plate').html(json.vehicle_plate);
					$('#data-table-form .driver').html(json.dock);
					$('#data-table-form .doc').html(json.delivery_doc);
					
				}else{
					toastr['error']("Could not load data.","Error");
				}
				
				App.unblockUI();
			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}
        
    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };  
}();

jQuery(document).ready(function() {
    handler.init();
});
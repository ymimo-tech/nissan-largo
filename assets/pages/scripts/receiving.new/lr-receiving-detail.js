var handler = function () {

    toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
    }

	var dataTable,
		childTable;

	var initialize = function(){
		initTableInbound();
		//initTable();
	}

	var initTableInbound = function () {

        dataTable = new Datatable();

		var totalItem = 0,
			totalQty = 0;

        dataTable.init({
            src: $("#table-list-inb"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getDetailItemInboundReceiving", // ajax source
					"data": {
						id_receiving: $('#data-table-form input[name="id"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
					/*
					dataSrc: function(json) {
						totalItem = json.recordsFiltered;
						totalQty = json.totalQty;

						$('.t-item').html(totalItem);
						$('.t-qty').html(totalQty);

						return json.data;
					}
					*/
                },
                "columns": [
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": false, "visible": false },
					{ "orderable": true },
                    { "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [0, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [3]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [6, 7, 8]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [9]
					},
					{
						"render": function(data, type, row){
							return '<div align="center"><button class="btn green btn-xs location" type="button">\
										Show Detail\
									</button></div>';
						},
						"targets": [13]
					}
				]
            }
        });
    }

	var initTable = function () {

        dataTable = new Datatable();

        dataTable.init({
            src: $("#table-list"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getDetailItemDataTable", // ajax source
					"data": {
						id_inbound: $('#data-table-form input[name="id_inbound"]').val(),
						id_receiving: $('#data-table-form input[name="id"]').val()
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1]
					},
					{
						"render": function(data, type, row){
							return ('<div align="right">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [3, 4, 5]
					}
				]
            }
        });
    }

	var initTable1 = function(){

		App.blockUI();

		$.ajax({
			url: BaseUrl + page_class + '/getDetailItem',
			type: 'POST',
			dataType: 'JSON',
			data: {
				id_inbound: $('#data-table-form input[name="id_inbound"]').val()
			},
			success: function(response){
				App.unblockUI();

				var tpl = '<tr>\
								<td><div align="center">{{ no }}</div></td>\
								<td><div align="center">{{ item_code }}</div></td>\
								<td>{{ item_name }}</td>\
								<td><div align="right">{{ qty_doc }}</div></td>\
								<td><div align="right">{{ qty_received }}</div></td>\
								<td><div align="right">{{ discrapency }}</div></td>\
						  </tr>';


				var data = response.data,
					iLen = data.length,
					row;

				for(var i = 0; i < iLen; i++){

					var qtyDoc = data[i].qty_doc,
						qtyReceived = data[i].qty_received,
						discrapency = 0,
						satuan = data[i].nama_satuan || '';

					if(parseInt(qtyDoc) > 0 && qtyDoc != '' && qtyDoc != null){
						discrapency = qtyDoc - qtyReceived;
						qtyDoc = qtyDoc + ' ' + satuan;
					}

					if(parseInt(qtyReceived) > 0 && qtyReceived != '' && qtyReceived != null)
						qtyReceived = qtyDoc + ' ' + satuan;


					row += tpl;
					row = row.replace(/{{ no }}/gi, i + 1);
					row = row.replace(/{{ item_code }}/gi, data[i].kd_barang);
					row = row.replace(/{{ item_name }}/gi, data[i].nama_barang);
					row = row.replace(/{{ qty_doc }}/gi, qtyDoc);
					row = row.replace(/{{ qty_received }}/gi, qtyReceived);
					row = row.replace(/{{ discrapency }}/gi, discrapency);
				}

				if(iLen <= 0){
					row = '<tr><td colspan="6"><div align="center">No data</div></td></tr>';
				}

				$('#table-list tbody').html(row);

			},
			error: function(response){
				App.unblockUI();
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});

    }

	var showModalPrint = function(id){

		if(id){

			App.blockUI();
			$.ajax({
				url: BaseUrl + page_class + '/getQtyPrint',
				type: 'POST',
				dataType: 'JSON',
				data: {
					id_receiving: id
				},
				success: function(response){

					var data = response;
					$('#data-modal-form input[name="id"]').val(id);
					$('.qty-print-receiving').html(data.qty);

					$('.qty-print-receiving').show();
					$('#qty-print').hide();
					$('.required').hide();

					$('#modal-print').modal({
						backdrop: 'static',
						keyboard: true
					});

					App.unblockUI();
				},
				error: function(response){
					App.unblockUI();
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});

		}else{

			$('#data-modal-form input[name="id"]').val('');

			$('#qty-print').val('');

			$('.qty-print-receiving').hide();
			$('#qty-print').show();
			$('.required').show();

			$('#modal-print').modal({
				backdrop: 'static',
				keyboard: true
			});

		}
	}

	var printBarcode = function(){

		var $btn = $('#submit-print');
			$btn.button('loading');

		var qty = $('#qty-print').val(),
			check = 0;
		if($('#data-modal-form input[name="id"]').val().length > 0){
			qty = $('.qty-print-receiving').html();
			check = 1;
		}

		if (!/\S/.test(qty)) {
			if(check == 0){
				$('.qty-print-group').addClass('has-error');
			}
			$btn.button('reset');
			toastr['error']("Qty print cannot empty and must be greater than 0","Error");
			return false;
		}else{
			if(qty <= 0){

				if(check == 0){
					$('.qty-print-group').addClass('has-error');
				}
				$btn.button('reset');
				toastr['error']("Qty print cannot empty and must be greater than 0","Error");
				return false;
			}
		}

		$.ajax({
			url: BaseUrl + page_class + '/printZpl',
			type: 'POST',
			dataType: 'JSON',
			data: {
				qty_print: qty
			},
			success: function(response){
				if(response.status == 'OK')
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				else
					bootbox.alert(response.message);

				setTimeout(function(){
					$btn.button('reset');
				}, 1000);
				/*
				var zpl = response.zpl;

				var ctrl = $(zpl);

				var postdata = {
					sid: $('#sid').val(),
					pid: $('#pid').val(),
					printerCommands: zpl
				}
				// store user printer settings & commands in server cache
				$.post(BaseUrl + 'webclientprint/ProcessPrint.php',
					postdata,
					function() {
						 // Launch WCPP at the client side for printing...
						 var sessionId = $("#sid").val();
						 jsWebClientPrint.print('sid=' + sessionId);
					}
				);
				*/
			},
			error: function(response){
				if(response.status == 401)
					location.href = BaseUrl + 'login';
				else
					bootbox.alert(response.responseText);
			}
		});
	}

	var childTableTpl = function(data){
		var tpl = '<div style="width: 90%; margin: 0 auto; text-align: left;">\
						<h4>Location List</h4>\
				   </div>\
				   <table id="child-table-'+data[0]+'" type="child" class="table child-table table-striped table-bordered table-hover table-checkable order-column">\
						<thead>\
							<tr>\
								<th><div align="center">No.</div></th>\
								<th><div align="center">LPN</div></th>\
								<th><div align="center">GID</div></th>\
								<th><div align="center">Qty</div></th>\
								<th><div align="center">Batch</div></th>\
								<th><div align="center">Expired Date</div></th>\
								<th><div align="center">Date In</div></th>\
								<th><div align="center">Location</div></th>\
								<th><div align="center">Status</div></th>\
								<th><div align="center">Received By</div></th>\
								<th><div align="center">Action</div></th>\
							</tr>\
						</thead>\
						<tbody>\
						</tbody>\
				   </table>';

		return tpl;
	}

	var initChildTable = function(data){
		childTable = new Datatable();

        childTable.init({
            src: $("#child-table-"+data[0]),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
                if(response.customGroupAction=='OK'){
                    toastr['success'](response.customGroupActionMessage,"Success");
                }
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },

            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
				"bDestroy": true,
				"processing": true,
				"serverSide": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "dom": '<"table-group-actions pull-right"><rt><"bottom"pi<"pull-right"l>><"clear">',
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
				"language": {
					"infoEmpty": ""
				},
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
				"initComplete": function(settings, json) {
					/*
					var dt = this;
					setTimeout(function(){
						dt.fnPageChange( 'first' );
					}, 300);
					*/
				},
                "ajax": {
                    "url": BaseUrl + page_class + "/getReceivedList", // ajax source
					"data": {
						id_barang: data[1],
						id_receiving: data[2]
					},
					error: function(response){
						if(response.status == 401)
							location.href = BaseUrl + 'login';
					}
                },
                "columns": [
					{ "orderable": false },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
                    { "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": true },
					{ "orderable": false }
                ],
                "order": [
                    [1, "ASC"]
                ],
				"columnDefs": [
					{
						"render": function(data, type, row){
							return ('<div align="center">{{ data }}</div>').replace(/{{ data }}/gi, data);
						},
						"targets": [0,1,2,3,4,5,6]
					}
				]
            }
        });
	}

	var pageInteraction = function(){

		$(document).on('click','.data-table-cancel-item',function(e){
            e.preventDefault();

			var id = $('#data-table-form input[name="id"]').val();
			var serial_number = $(this).attr('data-id');

            $.ajax({
            	url : BaseUrl + page_class + "/cancel_receiving_item/",
            	method : "POST",
            	data: {
            		receiving_id:id,
            		serial_number:serial_number
            	},
            	dataType : "JSON",
            	success : function(response){

	           		if(response.status){

						var button = $(document).find('.shown').find('button');

						button.trigger('click');
						button.trigger('click');

	            		toastr['success'](response.message,"success");

	            	}else{

	            		toastr['error'](response.message,"error");

	            	}

            	},
            	error : function(err){
            		toastr['error'](err.message,"error");
            	}

            })
        });

		$('#table-list-inb').on('click', '.location', function(e){
			e.preventDefault();

			var tr = $(this).closest('tr');
			var row = dataTable.getDataTable().row( tr );
			console.log(tr.attr('class'));

			if(tr.hasClass('shown')){
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');

				childTable.getDataTable().destroy();
			}else{
				$('#table-list-inb tbody tr').each(function(){
					var type = $(this).attr('role');

					//if(type === undefined){
						console.log('here');
						//$(this).hide();
						var r = dataTable.getDataTable().row( $(this) );
						r.child.hide();
						$(this).removeClass('shown');
					//}
				});

				// Open this row
				row.child( childTableTpl(row.data()) ).show();
				tr.addClass('shown');

				initChildTable(row.data());
			}


			// if(row.child.isShown()){

				// // This row is already open - close it
				// row.child.hide();
				// tr.removeClass('shown');

				// childTable.getDataTable().destroy();

			// }else{

				// $('#table-list-inb tbody tr').each(function(){
					// var type = $(this).attr('role');

					// if(type === undefined){
						// console.log('here');
						// $(this).hide();
						// $(this).removeClass('shown');
					// }
				// });

				// // Open this row
				// row.child( childTableTpl(row.data()) ).show();
				// tr.addClass('shown');

				// initChildTable(row.data());
			// }
		});


		$(document).on('click','#print-ol',function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            $.ajax({
				url: BaseUrl + page_class + '/printOuterLabel',
				type: 'POST',
				dataType: 'JSON',
				data: {receiving_id:$(this).attr('data-id')},
				success: function(response){
					location.href = 'MimoPrint:CLIENT_PRINT?url=' + base64_encode(BaseUrl + 'send_to_printer/send/' + response.sid);
				},
				error: function(response){
					if(response.status == 401)
						location.href = BaseUrl + 'login';
					else
						bootbox.alert(response.responseText);
				}
			});
        });

		$('#qty-print').on('keydown', function(e){
			return isNumeric(e);
		});

		$('#qty-print').on('keyup', function(){
			$('.qty-print-group').removeClass('has-error');
		});

		$('#data-modal-form').on('submit', function(e){
			e.preventDefault();
			printBarcode();
		});

		$('#modal-print').on('shown.bs.modal', function () {
			$('#qty-print').focus();
		});

		$('#button-print-qty-barcode').on('click', function(e){
			e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

			var id = $('#data-table-form input[name="id"]').val();

			showModalPrint(id);
		});

		$('#button-print-tally').on('click', function(e){
            e.preventDefault();

			var ctrl = $(this),
				id = ctrl.attr('data-id');

			if(ctrl.hasClass('disabled-link'))
				return false;

            post(BaseUrl + page_class + '/print_tally',
			{
				id_receiving: id
			}, '', 'Y');
		});

		$('#button-export-excel').on('click', function(){
			var ctrl = $(this),
				id = ctrl.attr('data-id');
			post(BaseUrl + page_class + '/export_excel',
			{
				id_receiving: id,
			}, '', 'Y');
		});

		$(document).on('click','.btn-export-detail', function(){
			var ctrl = $(this),
				id_receiving = ctrl.attr('data-id'),
				id_barang = ctrl.attr('data-idbarang');
			post(BaseUrl + page_class + '/export_excel2',
			{
				id_receiving: id_receiving,
				id_barang: id_barang,
			}, '', 'Y');
		});
	}

    return {

        init: function () {
			initialize();
			pageInteraction();
        }

    };
}();

jQuery(document).ready(function() {
    handler.init();
});

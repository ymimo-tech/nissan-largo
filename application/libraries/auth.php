<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class auth {

	private $CI;

	function __construct(){

		$this->CI = &get_instance();
	}

	private function load_model($modelName){
		if(is_array($modelName)){
			foreach ($modelName as $val) {
				$this->CI->load->model($this->CI->db->dbdriver.'/'.$val);
			}
		}else{
			$this->CI->load->model($this->CI->db->dbdriver.'/'.$modelName);
		}
	}

	public function user_authentication(){

        $uname 		= $this->CI->input->get_request_header('User', true);
        $session    = $this->CI->input->get_request_header('Authorization', true);

        if(!empty($uname) && !empty($session)){

			$this->load_model('api_setting_model');
			$auth = $this->CI->api_setting_model->user_authentication();
			if($auth){
				return true;
			}

        }

        $this->response();

	}

	public function static_auth($username,$token){

        $uname 		= $this->CI->input->get_request_header('User', true);
        $session    = $this->CI->input->get_request_header('Authorization', true);

        if($username == $uname || $token == $session){
        	return true;
        }

    	$this->response();
	}

	private function response(){

		$data['param']		= null;
		$data['status']		= 401;
		$data['message']	= 'Unauthorized user.';
		$data['response']	= false;

		$this->CI->output
	        ->set_content_type('application/json')
	        ->set_status_header(401)
	        ->set_output(json_encode($data))
	        ->_display();
	        
	    exit;
	}

}

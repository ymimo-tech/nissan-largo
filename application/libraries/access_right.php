<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class access_right {

    public function __construct() {
        
    }

    private $role;

    protected function load_model($modelName){
        $CI = &get_instance();
        if(is_array($modelName)){
            foreach ($modelName as $val) {
                $CI->load->model($CI->db->dbdriver.'/'.$val);
            }
        }else{
            $CI->load->model($CI->db->dbdriver.'/'.$modelName);
        }
    }

    public function warehouse_filter(){

        $CI = &get_instance();
        $this->load_model('warehouse_model');

        $warehouseFilter = $CI->warehouse_model->options('All Warehouse', NULL);

        return $warehouseFilter;

    }

    public function menu() {
        $CI = &get_instance();
        $this->load_model('roles_model');
        $this->load_model('menu_group_model');

        $kd_roles = $CI->session->userdata('kd_roles');
        $url = $CI->uri->segment(1);
        $active_group = '';

        $temp_menu = array();
        $roles = $CI->roles_model->get_data_roles(array('a.grup_id' => $kd_roles));
        foreach ($roles->result() as $value) {
            $pos = '';
            if ($url == $value->url) {
                $active_group = $value->kd_grup_menu;
                $pos = 'border-color:#0072c6';
            }

            $parent = !empty($value->kd_parent) ? $value->kd_parent : 0;
            $temp_menu[$value->kd_grup_menu][$parent][] = array(
                'kd_menu' => $value->kd_menu,
                'kd_parent' => $value->kd_parent,
                'kd_grup_menu' => $value->kd_grup_menu,
                'nama_menu' => $value->nama_menu,
                'url' => $value->url,
                'pos' => $pos
            );
        }

        //$menu = '<ul class="main">';
        $menu_group = $CI->menu_group_model->get_data();
        $menu = '
        <div class="hor-menu  ">
                <ul class="nav navbar-nav">
        ';
        $menu .= '<li class="menu-dropdown classic-menu-dropdown">
                        <a href="'.base_url().'"> Home
                                
                            </a>
                    </li>';

        foreach ($menu_group->result() as $group) {
            if (isset($temp_menu[$group->kd_grup_menu])) {

                $expand = '';
                $actived = '';
                if ($active_group == $group->kd_grup_menu) {
                    /*$expand = ' class="expand" id="current" ';
                    $actived = 'class="active navAct"';*/
                }

                $menu .= '<li class="menu-dropdown classic-menu-dropdown">';

                $menu .= '<a href="javascript:;"> '.$group->nama_grup_menu.'
                            <i class="fa fa-angle-down"></i>
                        </a>';
                
                $menu .= '
                        <ul class="dropdown-menu pull-left">';
                /*$menu .= '<li ' . $actived . '>';
                $menu .= '  <a ' . $expand . ' style="cursor:pointer;"><i class="' . $group->icon . '"></i> ' . $group->nama_grup_menu . '</a>';*/

                //$menu .= '  <ul class="sub_main">';
                if (isset($temp_menu[$group->kd_grup_menu][0])) {
                    foreach ($temp_menu[$group->kd_grup_menu][0] as $level1) {
                        $kd_menu1 = $level1['kd_menu'];

                        if (isset($temp_menu[$group->kd_grup_menu][$level1['kd_menu']])) {
                            $menu .= '
                            <li class="divider"></li>
                            <li class="dropdown-submenu ">
                                <a href="javascript:;"> ' . $level1['nama_menu'] . '
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu">';

                            foreach ($temp_menu[$group->kd_grup_menu][$kd_menu1] as $level2) {
                                $kd_menu2 = $level2['kd_menu'];
                                if (isset($temp_menu[$group->kd_grup_menu][$kd_menu2])) {
                                    $menu .= '
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <!--<i class="icon-settings"></i>--> ' . $level2['nama_menu'] . '
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">';

                                    foreach ($temp_menu[$group->kd_grup_menu][$kd_menu2] as $level3) {
										$kd_menu3 = $level3['kd_menu'];
										
										if (isset($temp_menu[$group->kd_grup_menu][$kd_menu3])) {
										
											$menu .= '
												<li class="dropdown-submenu ">
													<a href="' . $this->set_url($level3['url']) . '" class="nav-link nav-toggle"> ' . $level3['nama_menu'] . '
														<span class="arrow"></span>
													</a>
												</li>';
												
										}else{
											
											$menu .= '
											<li class=" ">
												<a href="' . $this->set_url($level3['url']) . '" class="nav-link nav-toggle ">
													<!--<i class="icon-settings"></i>--> ' . $level3['nama_menu'] . '
													<span class="arrow"></span>
												</a>
											</li>';
											
										}
                                    }
                                    $menu .= '</ul>';
                                } else {
                                    $menu .= '
                                    <li class=" ">
                                        <a href="' . $this->set_url($level2['url']) . '" class="nav-link nav-toggle ">
                                            <!--<i class="icon-settings"></i>--> ' . $level2['nama_menu'] . '
                                            <span class="arrow"></span>
                                        </a>
                                    </li>';

                                }
                                
                            }
                            $menu .= '</ul>';
                        } else {
                            $menu .= '
                            <li class="">
                                <a href="' . $this->set_url($level1['url']) . '"> ' . $level1['nama_menu'] . '
                                    <span class="arrow"></span>
                                </a>
                            </li>';
                        }
                    }
                    $menu .= '</ul>';
                }
            }
        }
        $menu .= '</ul>
        </div>';

        return $menu;
    }

    private function set_url($url) {

        switch ($url) {
            case 'moodle':
                $full_url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $url;
                break;
            default:
                $full_url = base_url($url);
                break;
        }
        return $full_url;
    }

    public function check($list_modul = array()) {
        $CI = &get_instance();
        $this->load_model('roles_model');
        $this->load_model('menu_group_model');

        $user_id = $CI->session->userdata('kd_roles');

        if (count($list_modul) == 0) {
            $url = $CI->uri->segment(1);

            $roles = $CI->roles_model->get_data_roles(array('a.grup_id' => $user_id, "lower(b.url) = '" . strtolower($url) . "'" => null));
            if ($roles->num_rows() > 0) {
                $otoritas = $roles->row();
                $this->role[$url] = array(
                    'view' => $otoritas->is_view,
                    'add' => $otoritas->is_add,
                    'edit' => $otoritas->is_edit,
                    'delete' => $otoritas->is_delete,
                    'approve' => $otoritas->is_approve,
                    'import' => $otoritas->is_import,
                    'print' => $otoritas->is_print
                );
            } else {
                // redirect('dashboard');
                // exit;
            }
        } else {
            foreach ($list_modul as $list) {
                $url = $list;

                $roles = $CI->roles_model->get_data_roles(array('a.grup_id' => $user_id, 'b.url' => $url));
                if ($roles->num_rows() > 0) {
                    $otoritas = $roles->row();
                    $this->role[$url] = array(
                        'view' => $otoritas->is_view,
                        'add' => $otoritas->is_add,
                        'edit' => $otoritas->is_edit,
                        'delete' => $otoritas->is_delete,
                        'approve' => $otoritas->is_approve,
                        'import' => $otoritas->is_import,
                        'print' => $otoritas->is_print
                    );
                }
            }
        }
    }

    public function otoritas($id = '', $redirect = false, $modul = '') {
        $CI = &get_instance();

        return true;

        if (!empty($id)) {

            if (empty($modul)) {
                $url = $CI->uri->segment(1);
            } else {
                $url = $modul;
            }

            if (isset($this->role[$url][$id]) && $this->role[$url][$id] == 't') {
                $otoritas = true;
            } else {
                $otoritas = false;
            }
        } else {
            $otoritas = false;
        }

        if ($otoritas) {
            return true;
        } else {
            if ($redirect) {
                $this->redirect();
            } else {
                return false;
            }
        }
    }

    public function redirect() {
        redirect('denied');
    }

    public function activity_logs($activity, $description) {
        $CI = &get_instance();

        if (!isset($CI->log_activity_model)) {
            $this->load_model('log_activity_model');
        }

        $message = '';
        switch ($activity) {
            case 'add':
                $message = 'Menambahkan data ' . $description;
                break;
            case 'view':
                $message = 'Menampilkan halaman ' . $description;
                break;
            case 'edit':
                $message = 'Mengubah data ' . $description;
                break;
            case 'delete':
                $message = 'Menghapus data ' . $description;
                break;
            case 'approve':
                $message = 'Menyetujui proses ' . $description;
                break;
            case 'not_approve':
                $message = 'Tidak menyetujui proses ' . $description;
                break;
            default:
                $message = $description;
                break;
        }

        $nik = $CI->session->userdata('nik_session');
        $username = $CI->session->userdata('username');
        $log = array(
            'nik' => $nik,
            'username' => $username,
            'activity' => $message
        );
        $CI->log_activity_model->create($log);
    }


    public function expiredDate(){
        $ci = get_instance();

        $data= $ci->license->data();
        if(!empty($data->expd)){

            $date = new \DateTime();
            $date->setTimestamp($data->expd);
            $subscribe_date = $date->format("Y-m-d");

        }else{

            $subscribe_date = "Unlimited";
        }

        echo "Expired Date : ".$subscribe_date;
    }

    public function is_expired(){
        $ci =& get_instance();
        $ci->load->library('license');
        $data = $ci->license->data();

        if(!empty($data->expd)){

            $today = new \DateTime();
            $exp = new DateTime();
            $exp->setTimestamp($data->expd);
            $diff = $today->diff($exp);
            $diffDay = $diff->format('%a');

            if($diffDay <= 30){

                if($data->expd < $today->getTimestamp()){

                    $class="danger";
                    $message="Masa aktif license anda telah habis";

                }else{

                    $class="warning";
                    $message = "Masa aktif license anda tinggal ".$diffDay." hari lagi.";

                }

                $view = "<div class='alert alert-".$class."' style='color:black;'><center><b>".$message."</b></center></div>";
                echo $view;

            }
        }
    }

}

<?php

session_start();

include FCPATH.'webclientprint\WebClientPrint.php';
use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\Utils;

class Wcpp {
	
	public function generateScript(){
		$script = WebClientPrint::createScript(get_instance()->config->base_url().'webclientprint/ProcessPrint.php');
		
		$xpath = new DOMXPath(@DOMDocument::loadHTML($script));
		$src = $xpath->evaluate("string(//script/@src)");
		
		return str_replace(base_url(), '', $src);
	}
	
}
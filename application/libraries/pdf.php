<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class pdf {

    function pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }

    function load($param='')
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';

        // if ($params == NULL)
        // {
        //    $param = '"en-GB-x","A4","","",10,10,10,10,6,3';
        // }

        // return new mPDF($param);
	if ($param == 'horizontal'){
            return new mPDF('', 'LETTER');
        }else{
            // $param = '"en-GB-x","A4-L","","",10,10,10,10,6,3';
            // return new mPDF($param);
            return new mPDF('c', 'A4-L');
        }
    }
}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



class import {

    var $ci;

    public function __construct() {

        $method = $_SERVER['REQUEST_METHOD'];
        // if($method !== 'POST'){
        //     show_404();
        // }

        $this->ci =& get_instance();
    }

    public function fromExcel($path,$table,$field){

        $path = 'uploads/excel/'. $path .'/';
        $upload = $this->upload($path);

        if($upload['status']){

            $arr = $this->excelToArray($path.$upload['data']['file_name']);

            if(!$arr){

                $result = ['status'=> 0 , 'message'=>'Wrong Template'];

            }else{

                $validate = $this->toFieldTable($field, $arr);

                if(!$validate){

                    $result = ['status'=> 0 , 'message'=>'Wrong Template'];

                }else{

                    $insert = $this->ci->db->insert_batch($table, $validate);

                    if($insert){

                        $result = ['status'=> 1, 'message'=>'Import Successfully'];

                    }else{

                        $result = ['status'=> 0 , 'message'=>"Can't Insert to Database "];

                    }

                }
            }

        }else{

            $result = ['status'=> 0 , 'message'=> $upload['data']];

        }

        return $result;

    }

    public function upload($path=''){

        $config['upload_path'] = $path;
        $config['allowed_types']='xlsx';
        $config['encrypt_name'] = TRUE;

        if(!is_dir($config['upload_path'])){
            mkdir($config['upload_path'], 0755);
        }
         
        $this->ci->load->library('upload',$config);

        if($this->ci->upload->do_upload("file")){

            $data = $this->ci->upload->data();

            return [
                "status" => 1,
                "data" => $data
            ];
        }

        return [
            "status" =>0,
            "data" => $this->ci->upload->display_errors('','')
        ];

    }

    public function excelToArray($file=''){

        $this->ci->load->library('PHPExcel');

        $objPHPExcel = PHPExcel_IOFactory::load($file);
        
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $cellCollection =  $objWorksheet->getCellCollection();

        if(count($cellCollection) > 0){

            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();

            $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
            $headingsArray = $headingsArray[1];
            $r = 0;

            $namedDataArray = array();
            for ($row = 2; $row <= $highestRow; ++$row) {
                $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
                foreach($headingsArray as $columnKey => $columnHeading) {

                    if(!empty($columnHeading)){
                        $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                    }

                }
                $r++;
            }

    
            return $namedDataArray;

        }

        return false;

    }

    public function toFieldTable($field=[],$data=[]){

        foreach ($field as $key => $value) {

            for ($i=0; $i < count($data); $i++) { 

                if(!array_key_exists($key, $data[$i])){
                    return false;
                }

                if($key !== $data[$i][$key]){
                    $data[$i][$value] = $data[$i][$key];
                    unset($data[$i][$key]);
                }

            }

        }

        return $data;

    }

    public function excelValidate($field=[],$data=[]){

        for ($i=0; $i < count($data); $i++) { 

            foreach ($data[$i] as $key => $value) {
                if(!in_array($key, $field)){

                    return false;

                }
            }

        }

        return true;

    }

    public function errorMessage($message=""){
        $payload['status'] = 0;
        $payload['message'] = $message;

        return $payload;
    }

    public function downloadTemplate($filename){

        $this->ci->load->helper('download');

        $payload = array();

        if($filename){
            $file = realpath('assets'). '/template/' . $filename;
            if (file_exists ( $file )) {

                $payload['status']  = 1;
                $payload['filename'] = $filename;

            }else{

                $payload['status']  = 0;
                $payload['message'] = "Template not available !";

            }
        }

        return $payload;
    }

}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class buttons {

    public function __construct() {
        
    }

    private $role;

   public function actions($data = array()){
        $CI = &get_instance();
        $action='';
        if ($CI->access_right->otoritas('edit') OR $CI->access_right->otoritas('delete')) {
			/*
            $action = '<div class="btn-group">
                    <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-flash"></i>Action <i class="fa fa-angle-down"></i>
                    </button>';
			*/
			
			$action = '<div class="btn-group">
                    <button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                    &nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
                    </button>';
			

            $action .= '<ul class="dropdown-menu" role="menu"> ';
            if(!empty($data)){
                foreach ($data as $key => $value) {
                    $checkKey = $key;
                    $explodedKey = explode("-", $key);
                    if(sizeof($explodedKey) > 1) {
                        $checkKey = $explodedKey[0];
                    }

                    if ($CI->access_right->otoritas($checkKey)) {
                        $action .= '<li>';
                        if($checkKey=='edit')
                            $action .= '<a class="data-table-'.$key.'" data-id="'.$value.'"><i class="fa fa-'.$checkKey.'"></i>'.ucfirst(str_replace("-", " ", $key)).'</a>';
                        if($checkKey=='delete')
                            $action .= '<a class="data-table-'.$key.'" data-id="'.$value.'"><i class="fa fa-trash"></i>'.ucfirst($checkKey).'</a>';
                        $action .= '</li>';
                    }
                }
            }
            $action .= '</ul>
                    </div>';
        }
        return $action;
   }

    private function set_url($url) {

        switch ($url) {
            case 'moodle':
                $full_url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $url;
                break;
            default:
                $full_url = base_url($url);
                break;
        }
        return $full_url;
    }

    public function check($list_modul = array()) {
        $CI = &get_instance();
        $CI->load->model('roles_model');
        $CI->load->model('menu_group_model');

        $user_id = $CI->session->userdata('kd_roles');

        if (count($list_modul) == 0) {
            $url = $CI->uri->segment(1);

            $roles = $CI->roles_model->get_data_roles(array('a.grup_id' => $user_id, "lower(b.url) = '" . strtolower($url) . "'" => null));
            if ($roles->num_rows() > 0) {
                $otoritas = $roles->row();
                $this->role[$url] = array(
                    'view' => $otoritas->is_view,
                    'add' => $otoritas->is_add,
                    'edit' => $otoritas->is_edit,
                    'delete' => $otoritas->is_delete,
                    'approve' => $otoritas->is_approve,
                    'import' => $otoritas->is_import,
                    'print' => $otoritas->is_print
                );
            } else {
                redirect('dashboard');
                exit;
            }
        } else {
            foreach ($list_modul as $list) {
                $url = $list;

                $roles = $CI->roles_model->get_data_roles(array('a.grup_id' => $user_id, 'b.url' => $url));
                if ($roles->num_rows() > 0) {
                    $otoritas = $roles->row();
                    $this->role[$url] = array(
                        'view' => $otoritas->is_view,
                        'add' => $otoritas->is_add,
                        'edit' => $otoritas->is_edit,
                        'delete' => $otoritas->is_delete,
                        'approve' => $otoritas->is_approve,
                        'import' => $otoritas->is_import,
                        'print' => $otoritas->is_print
                    );
                }
            }
        }
    }

    public function otoritas($id = '', $redirect = false, $modul = '') {
        $CI = &get_instance();

        if (!empty($id)) {

            if (empty($modul)) {
                $url = $CI->uri->segment(1);
            } else {
                $url = $modul;
            }

            if (isset($this->role[$url][$id]) && $this->role[$url][$id] == 't') {
                $otoritas = true;
            } else {
                $otoritas = false;
            }
        } else {
            $otoritas = false;
        }

        if ($otoritas) {
            return true;
        } else {
            if ($redirect) {
                $this->redirect();
            } else {
                return false;
            }
        }
    }

    public function redirect() {
        redirect('denied');
    }

    public function activity_logs($activity, $description) {
        $CI = &get_instance();

        if (!isset($CI->log_activity_model)) {
            $CI->load->model('log_activity_model');
        }

        $message = '';
        switch ($activity) {
            case 'add':
                $message = 'Menambahkan data ' . $description;
                break;
            case 'view':
                $message = 'Menampilkan halaman ' . $description;
                break;
            case 'edit':
                $message = 'Mengubah data ' . $description;
                break;
            case 'delete':
                $message = 'Menghapus data ' . $description;
                break;
            case 'approve':
                $message = 'Menyetujui proses ' . $description;
                break;
            case 'not_approve':
                $message = 'Tidak menyetujui proses ' . $description;
                break;
            default:
                $message = $description;
                break;
        }

        $nik = $CI->session->userdata('nik_session');
        $username = $CI->session->userdata('username');
        $log = array(
            'nik' => $nik,
            'username' => $username,
            'activity' => $message
        );
        $CI->log_activity_model->create($log);
    }

}

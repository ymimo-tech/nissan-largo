<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pgpdo {
	
	private $conn;
	private $CI;
	
	public function __construct(){
		
		$this->CI = &get_instance();
		$this->CI->load->helper('pgconnect');
		
		$this->conn = pgconnect();
	}
	
	public function fetch($sql, $params = false){
		try{
			$q = $this->conn->prepare($sql);
			
			if($params){
				foreach($params as $k => $v){
					$q->bindParam($k, $params[$k]);
				}
			}
			
			$q->execute();
			
			return $q->fetch(PDO::FETCH_ASSOC);
			
		}catch(Exception $e){
			echo 'ERROR : ' . $e->getMessage() . '  IN QUERY : ' . $sql;
			file_put_contents('log/pdo.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ERROR : ' . $e->getMessage() . ' IN QUERY : ' . $sql . PHP_EOL, FILE_APPEND);
		}
	}
	
	public function fetchAll($sql, $params = false){
		try{
			$q = $this->conn->prepare($sql);
			
			if($params){
				foreach($params as $k => $v){
					$q->bindParam($k, $params[$k]);
				}
			}
			
			$q->execute();
			
			return $q->fetchall(PDO::FETCH_ASSOC);
			
		}catch(Exception $e){
			echo 'ERROR : ' . $e->getMessage() . '  IN QUERY : ' . $sql;
			file_put_contents('log/pdo.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ERROR : ' . $e->getMessage() . ' IN QUERY : ' . $sql . PHP_EOL, FILE_APPEND);
		}
	}
	
	public function exec($sql, $params = false){
		try{
			$q = $this->conn->prepare($sql);
			
			if($params){
				foreach($params as $k => $v){
					$q->bindParam($k, $params[$k]);
				}
			}
			
			return $q->execute();
			
		}catch(Exception $e){
			echo 'ERROR : ' . $e->getMessage() . '  IN QUERY : ' . $sql;
			file_put_contents('log/pdo.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ERROR : ' . $e->getMessage() . ' IN QUERY : ' . $sql . PHP_EOL, FILE_APPEND);
		}
	}
}
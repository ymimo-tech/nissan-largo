<?php
	if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	
	class MimoClientPrint{
		
		public function __construct(){
			
		}
		
		public function getDefaultPrinter($post = array()){
			$result = array();
			
			$printers = file_get_contents(FCPATH.'mcpcache/'.$post['sid'].'.mcpcache');
			
			$result['printer'] = $printers;
			if(file_exists(FCPATH.'mcpcache/'.$post['sid'].'.mcpcache'))
				unlink(FCPATH.'mcpcache/'.$post['sid'].'.mcpcache');
			
			return $result;
		}
		
		public function getPrinterList($post = array()){
			$result = array();
			
			$printers = file_get_contents(FCPATH.'mcpcache/'.$post['sid'].'.mcpcache');
			$exp = explode(',', $printers);
			
			$result['printers'] = $exp;
			if(file_exists(FCPATH.'mcpcache/'.$post['sid'].'.mcpcache'))
				unlink(FCPATH.'mcpcache/'.$post['sid'].'.mcpcache');
			
			return $result;
		}
		
		public function addCache($post = array()){
			
			if(!is_dir(FCPATH.'mcpcache/')){
				
				mkdir(FCPATH.'mcpcache/', 0777, true);
				
			}else{
				if(!is_writable(FCPATH.'mcpcache/')){
					echo  'Cannot write in mcpcache directory';
					exit();
				}
				
				file_put_contents(FCPATH.'mcpcache/'.base64_decode($post['sid']).'.mcpcache', base64_decode($post['printers']));
			}
		}
		
		public function printIt($post = array()){
			$result = array();
			
			$zpl = $post['printerCommands'];
			
			if(!is_dir(FCPATH.'mcpcache/')){
				
				mkdir(FCPATH.'mcpcache/', 0777, true);
				
				//$result['status'] = 'ERR';
				//$result['message'] = 'Directory mcpcache doesn\'t exist';
				
				//return $result;
			}else{
				if(!is_writable(FCPATH.'mcpcache/')){
					$result['status'] = 'ERR';
					$result['message'] = 'Cannot write in mcpcache directory';
					
					return $result;
				}
			}
			
			$sId = session_id();
			
			$name = FCPATH . 'mcpcache/' . $sId . '.ini';
			$content = 'command=' . base64_encode($zpl);
			
			if(file_put_contents($name, $content)){
				$result['status'] = 'OK';
				$result['sid'] = $sId;
				$result['message'] = 'Generate print cache success';
			}else{
				$result['status'] = 'ERR';
				$result['sid'] = 0;
				$result['message'] = 'Generate print cache failed';
			}
			
			return $result;
		}
		
		public function generateScript(){
			
			$script = '<script>';
			$script .= 'function sendToPrinter(sid){
							setTimeout(function(){
								location.href="MimoPrint:CLIENT_PRINT?url='.base64_encode(base_url().'send_to_printer/"+sid+"').'";
							}, 300);
						}';
			$script .= '</script>';
			
			return $script;
		}
		
		public function send($sid){
			$file = FCPATH.'mcpcache/'.$sid.'.ini';
			
			if(!file_exists(FCPATH.'mcpcache/'.$sid.'.ini')){
				echo 'Invalid print command';
				exit();
			}
			
			$content = file_get_contents($file);
			unlink(FCPATH.'mcpcache/'.$sid.'.ini');
			echo $content;
		}
		
	}
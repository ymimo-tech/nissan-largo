<?php

class send_to_printer extends CI_Controller {
	
    public function __construct() {
        // Declaration
        parent::__construct();		
		$this->load->helper('download');
        $this->load->library('mimoclientprint');
    }
	
    public function index(){
		
	}
	
	public function send($sid){
			
		$this->mimoclientprint->send($sid);
		
	}
	
	public function printer_list(){
			
		$printers = $_GET['PRINTERS'];
		$sId = $_GET['sid'];
		
		$params = array(
			'printers'	=> $printers,
			'sid'		=> $sId
		);
		
		$this->mimoclientprint->addCache($params);
		
	}
	
	public function getPrinterList($sid){
		
		$result = array();
		
		$params = array(
			'sid'	=> $sid
		);
		
		$result = $this->mimoclientprint->getPrinterList($params);
		
		echo json_encode($result);
		
	}
	
	public function getDefaultPrinter($sid){
		
		$result = array();
		
		$params = array(
			'sid'	=> $sid
		);
		
		$result = $this->mimoclientprint->getDefaultPrinter($params);
		
		echo json_encode($result);
		
	}
}
		
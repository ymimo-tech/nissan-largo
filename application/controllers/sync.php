<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class Sync extends CI_Controller {

    private $class_name;
    private $limit = 50;
    private $offset = 0;
    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

    }

    function _curl_process($type='',$table='',$data=array()){
        $curl_handle = curl_init ();

		//supported format ['json','csv','jsonp','serialized','xml']
		//change url suffix ex : .json to .xml for change response format
        $clientid='test';
        $local_ip='';
        $staging_ip='localhost';
        $data2 = array (
                'table' => $table,
				'clientid' => $clientid,
				'hash' => sha1 ( $table . $clientid . $local_ip ),
				'xkey' => '0b5581b7ec815ad66a916b4fc5716d2ec84cd45e',
		);

        $data = array_merge($data,$data2);
		curl_setopt ( $curl_handle, CURLOPT_URL, 'http://'.$staging_ip.'/salubritas-staging/api/'.$type.'.json' );
		curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $curl_handle, CURLOPT_POST, 1 );
		curl_setopt ( $curl_handle, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
		curl_setopt ( $curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST );
		curl_setopt ( $curl_handle, CURLOPT_USERPWD, 'test:123456' );

		$buffer = curl_exec ( $curl_handle );
		curl_close ( $curl_handle );

		$data = json_decode($buffer);
        return $data;
    }

    function sync_all(){
        $new_otb=array();
        $new_itb=array();
        $new_itm=array();
        $new_supp=array();

        $this->load->model('api_putaway_model');
        $this->api_putaway_model->send_staging();


        //INBOUND
        $this->load->model(array('inbound_model'));
        $this->db->trans_start();
        //$new_inbounds =  $this->inbound_model->get_new_inbound();
        $new_inbounds = $this->_curl_process('get','f_t_inbound_h',array('where'=>array('SyncStatus'=>'N')));
        if($new_inbounds->status=='001'){
            foreach($new_inbounds->data as $inbound){
                $new=0;
                $supplier =  $this->inbound_model->get_supplier($inbound->Supplier);
                $id_supplier = $supplier->num_rows()?$supplier->row()->id_supplier:0;
                $doc =  $this->inbound_model->get_inbound_document($inbound->InbType);
                $id_doc = $doc->num_rows()?$doc->row()->id_inbound_document:0;

                $data_inbound=array(
                    'kd_inbound'=>$inbound->InbNo,
                    'tanggal_inbound'=>$inbound->DocDate,
					'BaseCreatedDate' => $inbound->BaseCreatedDate,
                    'id_supplier'=>$id_supplier,
                    'id_inbound_document'=>$id_doc,
                    'id_status_inbound'=>1,
                    'id_staging'=>$inbound->DocEntry,
					'WhsFrom'=>$inbound->WhsFrom,
                );

                $exist = $this->inbound_model->check_inbound($inbound->InbNo);
                if($exist->num_rows()>0){
                    $new_inbound_id = $exist->row()->id_inbound;
                    $this->inbound_model->update_inbound($data_inbound,$new_inbound_id);
                }else{
                    $new_inbound_id = $this->inbound_model->create_inbound($data_inbound);
                    $new=1;
                }
                // $inbound_items = $this->inbound_model->get_inbound_item($inbound->DocEntry);
                $inbound_items = $this->_curl_process('get','f_t_inbound_l1',array('where'=>array('DocEntry'=>$inbound->DocEntry)));
                if($inbound_items->status=='001'){
                    foreach($inbound_items->data as $item){
                        $barang =  $this->inbound_model->get_barang($item->SKU);
                        $id_barang = $barang->num_rows()?$barang->row()->id_barang:0;
                        $data_item = array(
                            'id_inbound'=>$new_inbound_id,
                            'id_barang'=>$id_barang,
                            'jumlah_barang'=>$item->DocQty,
                            'id_staging'=>$item->LineNum,
                            'FinaCode' => $item->FinaCode
                        );

                        $exist_item = $this->inbound_model->check_inbound_barang(array('id_staging'=>$item->LineNum,'id_inbound'=>$new_inbound_id));
                        if($exist_item->num_rows()>0){
                            $new_inbound_barang_id = $exist_item->row()->id_inbound_barang;
                            $this->inbound_model->update_inbound_barang($data_item,$new_inbound_barang_id);
                        }else{
                            $new_inbound_barang_id = $this->inbound_model->create_inbound_barang($data_item);
                        }
                    }
                }
                $SyncDate=date('Y-m-d H:i:s');
                //$this->inbound_model->set_inbound_status($inbound->DocEntry,$SyncDate);
                $this->_curl_process('update','f_t_inbound_h',array(
                    'where'=>array('DocEntry'=>$inbound->DocEntry),
                    'data'=>array(
                        'SyncStatus'=>'Y',
                        'SyncDate'=>$SyncDate,
                        'InbStatus'=>'INPROCESS'
                    )
                ));
                $new_itb[]=array('nomor'=>$inbound->InbNo,'id'=>$new_inbound_id,'newrec'=>$new);
            }
        }
        $this->db->trans_complete();

        //OUTBOUND
        $this->load->model(array('outbound_model'));
        $this->db->trans_start();
        // $new_outbounds =  $this->outbound_model->get_new_outbound();
        $new_outbounds = $this->_curl_process('get','f_t_outbound_h',array('where'=>array('SyncStatus'=>'N')));
        if($new_outbounds->status=='001'){
            foreach($new_outbounds->data as $outbound){
                $new=0;
                $doc =  $this->outbound_model->get_outbound_document($outbound->OutbondType);
                $id_doc = $doc->num_rows()?$doc->row()->id_outbound_document:0;
                $data_outbound=array(
                    'kd_outbound'=>$outbound->OutbondNo,
                    'tanggal_outbound'=>$outbound->DocDate,
                    'id_outbound_document'=>$id_doc,
                    'id_status_outbound'=>1,
                    'id_staging'=>$outbound->DocEntry,
                    'DestinationName'=>$outbound->DestinationName?$outbound->DestinationName:'',
                    'DestinationAddressL1'=>$outbound->DestinationAddressL1?$outbound->DestinationAddressL1:'',
                    'DestinationAddressL2'=>$outbound->DestinationAddressL2?$outbound->DestinationAddressL2:'',
                    'DestinationAddressL3'=>$outbound->DestinationAddressL3?$outbound->DestinationAddressL3:'',
                    'Phone'=>$outbound->PhoneNo?$outbound->PhoneNo:'',
                    'ShippingNote'=>$outbound->ShippingNote?$outbound->ShippingNote:'',
                );
                $exist = $this->outbound_model->check_outbound($outbound->OutbondNo);
                if($exist->num_rows()>0){
                    $new_outbound_id = $exist->row()->id_outbound;
                    $this->outbound_model->update_outbound($data_outbound,$new_outbound_id);
                }else{
                    $new_outbound_id = $this->outbound_model->create_outbound($data_outbound);
                    $new=1;
                }
                // $outbound_items = $this->outbound_model->get_outbound_item($outbound->DocEntry);
                $outbound_items = $this->_curl_process('get','f_t_outbound_l1',array('where'=>array('DocEntry'=>$outbound->DocEntry)));
                if($outbound_items->status=='001'){
                    foreach($outbound_items->data as $item){
                        $barang =  $this->outbound_model->get_barang($item->SKU);
                        $id_barang = $barang->num_rows()?$barang->row()->id_barang:0;
                        $data_item = array(
                            'id_outbound'=>$new_outbound_id,
                            'id_barang'=>$id_barang,
                            'jumlah_barang'=>$item->DocQty,
                            'id_staging'=>$item->LineNum
                        );
                        $exist_item = $this->outbound_model->check_outbound_barang(array('id_staging'=>$item->LineNum,'id_outbound'=>$new_outbound_id));
                        if($exist_item->num_rows()>0){
                            $new_outbound_barang_id = $exist_item->row()->id;
                            $this->outbound_model->update_outbound_barang($data_item,$new_outbound_barang_id);
                        }else{
                            $new_outbound_barang_id = $this->outbound_model->create_outbound_barang($data_item);
                        }
                    }
                }
                $SyncDate=date('Y-m-d H:i:s');
                // $this->outbound_model->set_outbound_status($outbound->DocEntry,$SyncDate);
                $this->_curl_process('update','f_t_outbound_h',array(
                    'where'=>array('DocEntry'=>$outbound->DocEntry),
                    'data'=>array(
                        'SyncStatus'=>'Y',
                        'SyncDate'=>$SyncDate
                    )
                ));
                $new_otb[]=array('nomor'=>$outbound->OutbondNo,'id'=>$new_outbound_id,'newrec'=>$new);
            }
        }
        $this->db->trans_complete();

        //BARANG
        $this->load->model(array('referensi_barang_model'));
        $this->db->trans_start();
        // $new_barangs =  $this->referensi_barang_model->get_new_barang();
        $new_barangs = $this->_curl_process('get','m_t_item',array('where'=>array('SyncStatus'=>'N')));
        if($new_barangs->status=='001'){
            foreach($new_barangs->data as $barang){
                $new = 0;
                $id_category = $this->referensi_barang_model->get_barang_category($barang->Category);
                $id_satuan = $this->referensi_barang_model->get_barang_satuan($barang->UomCode);
                $data_barang=array(
                    'kd_barang'=>$barang->SKU,
                    'nama_barang'=>$barang->ItemName,
                    'detail_barang'=>$barang->ItemDesc,
                    'id_kategori'=>$id_category,
                    'has_expdate'=>$barang->HasExpDate,
                    'shipment_type'=>$barang->ShipmentType,
                    'fifo_period'=>$barang->FifoPeriod,
                    'def_qty'=>$barang->DefQty,
                    'id_satuan'=>$id_satuan,
                    'minimum_stock'=>$barang->MinStok
                );
                $exist = $this->referensi_barang_model->check_barang($barang->SKU);
                if($exist->num_rows()>0){
                    $new_barang_id = $exist->row()->id_barang;
                    $this->referensi_barang_model->update_barang($data_barang,$new_barang_id);
                }else{
                    $new_barang_id = $this->referensi_barang_model->create_barang($data_barang);
                    $new=1;
                }
                $SyncDate=date('Y-m-d H:i:s');
                // $this->referensi_barang_model->set_barang_status($barang->SKU,$SyncDate);
                $this->_curl_process('update','m_t_item',array(
                    'where'=>array('SKU'=>$barang->SKU),
                    'data'=>array(
                        'SyncStatus'=>'Y',
                        'SyncDate'=>$SyncDate
                    )
                ));
                $new_itm[]=array('sku'=>$barang->SKU,'nama'=>$barang->ItemName,'id'=>$new_barang_id,'newrec'=>$new);
            }
        }
        $this->db->trans_complete();

        $this->load->model(array('referensi_supplier_model'));
        $this->db->trans_start();
        // $new_suppliers =  $this->referensi_supplier_model->get_new_supplier();
        // $new=array();
        $new_suppliers = $this->_curl_process('get','m_t_supplier',array('where'=>array('SyncStatus'=>'N')));
        if($new_suppliers->status=='001'){
            foreach($new_suppliers->data as $supplier){
                $new = 0;
                $data_supplier=array(
                    'kd_supplier'=>$supplier->SupplierCode,
                    'nama_supplier'=>$supplier->SupplierName,
                    'alamat_supplier'=>$supplier->AddressL1.' '.$supplier->AddressL2.' '.$supplier->AddressL3,
                    'telepon_supplier'=>$supplier->Phone,
                    'cp_supplier'=>$supplier->ContactPerson,
                    'email_supplier'=>''
                );
                $exist = $this->referensi_supplier_model->check_supplier($supplier->SupplierCode);
                if($exist->num_rows()>0){
                    $new_supplier_id = $exist->row()->id_supplier;
                    $this->referensi_supplier_model->update_supplier($data_supplier,$new_supplier_id);
                }else{
                    $new_supplier_id = $this->referensi_supplier_model->create_supplier($data_supplier);
                    $new=1;
                }
                $SyncDate=date('Y-m-d H:i:s');
                // $this->referensi_supplier_model->set_supplier_status($supplier->SupplierCode,$SyncDate);
                $this->_curl_process('update','m_t_supplier',array(
                    'where'=>array('SupplierCode'=>$supplier->SupplierCode),
                    'data'=>array(
                        'SyncStatus'=>'Y',
                        'SyncDate'=>$SyncDate
                    )
                ));
                $new_supp[]=array('kd_supplier'=>$supplier->SupplierCode,'nama'=>$supplier->SupplierName,'id'=>$new_supplier_id,'newrec'=>$new);
            }
        }
        $this->db->trans_complete();

        return array('inbound'=>$new_itb,'outbound'=>$new_otb,'item'=>$new_itm,'supplier'=>$new_supp);
    }

    function all(){
        date_default_timezone_set("Asia/Jakarta");
        $new = $this->sync_all();
        $json = array('newc'=>$new);
        echo json_encode($json);
    }


    public function send_to_staging_stok(){

        $this->load->model('api_sync_model');
        $this->api_sync_model->sync_qty();

        $results = array(
            "response" => true,
            "message" => "Berhasil Sync"
        );

        echo json_encode($results);

    }

}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

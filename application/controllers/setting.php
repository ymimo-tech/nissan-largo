<?php 
class setting extends CI_Controller {

    
    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);
		
		$this->load->model('setting_model');
    }
	
	public function index(){
		$data['title'] = '<i class="icon-plus-sign-alt"></i> Setting';
        $data['page'] = $this->class_name . '/index';
        $data['data_source'] = base_url($this->class_name . '/load');
		
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
		$data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
		
		$data['jsPage'][] = 'assets/custom/js/share.js';
		$data['jsPage'][] = 'assets/pages/scripts/setting/setting-handler.js';
		
		$data['setting'] = $this->setting_model->getData();
         
        /* INSERT LOG */
        $this->access_right->activity_logs('view','Setting');
        /* END OF INSERT LOG */
        
        $this->load->view('template', $data);
	}
	
	public function save(){
		$result = array();
		
		$params = array(
			'peminjaman'	=> filter_var(trim($this->input->post('peminjaman')), FILTER_SANITIZE_STRING)
		);
		
		$result = $this->setting_model->save($params);
		
		echo json_encode($result);
	}
	
}
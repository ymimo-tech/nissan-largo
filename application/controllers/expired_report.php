<?php

class expired_report extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Expired';
        $data['page'] = $this->class_name . '/index';
		
		$this->include_required_file($data);
		
		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/report/lr-expired-report.js";
		$data['today'] = date('d/m/Y');
		
		$this->load_model('inv_transfer_model');
		$data['item'] = $this->expired_report_model->getItem();
		$data['location'] = $this->inv_transfer_model->getLocation();
		 
		/* INSERT LOG */
		$this->access_right->activity_logs('view','Expired Report');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }
	
	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}
	
	//REQUEST	
	public function export(){
		$data = array();
		
		$params = array(
			'id_barang'	=> $this->input->post('id_barang'),
			'loc_id'	=> $this->input->post('loc_id'),
			'params' 	=> $this->input->post('params')
		);
		
		$data['page'] = $this->class_name . '/expired';
		$data['params'] = $params;
		$data['data'] = $this->expired_report_model->getListRaw($params);	
		
        $html = $this->load->view($data['page'], $data, true);
        $path = "expired_report.pdf";
 
        ini_set('memory_limit', $this->config->item('allocated_memory')); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            10, // margin bottom
            10, // margin header
            10
		); // margin footer

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can  
		
	}
	
	public function export_excel(){
		$data = array();

		$params = array(
			'id_barang'	=> $this->input->post('id_barang'),
			'loc_id'	=> $this->input->post('loc_id'),
			'params' 	=> $this->input->post('params')
		);

		$data = $this->expired_report_model->getListRaw($params);

        $this->load->library('PHPExcel');

        $objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Expired")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);

       	$sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'SKU')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Product Name')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Type')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Location')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Quantity')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Uom')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'Expired Date')->getStyle('H1')->getFont()->setBold(true);

        $len = count($data);
        if($len > 0){
            $row = 2;
            for($i = 0; $i < $len; $i++){
				$exp_date = DateTime::createFromFormat('Y-m-d', $data[$i]['tgl_exp']);
                $sheet->setCellValueByColumnAndRow(0, $row, ($i + 1));
                $sheet->setCellValueByColumnAndRow(1, $row, $data[$i]['sku']);
                $sheet->setCellValueByColumnAndRow(2, $row, $data[$i]['nama_barang']);
                $sheet->setCellValueByColumnAndRow(3, $row, 'TYPE');
                $sheet->setCellValueByColumnAndRow(4, $row, $data[$i]['loc_name']);
                $sheet->setCellValueByColumnAndRow(5, $row, $data[$i]['stock']);
                $sheet->setCellValueByColumnAndRow(6, $row, $data[$i]['nama_satuan']);
                $sheet->setCellValueByColumnAndRow(7, $row, $exp_date->format('d M Y'));
                $row++;
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Expired'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

    }
	
	public function getList(){
		$result = array();
		
		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
			'loc_id'		=> $this->input->post('loc_id'),
			'params'		=> $this->input->post('params')
		);
		
		$result = $this->expired_report_model->getList($params);
		
		echo json_encode($result);
	}

	public function getExpiredList(){
		$result = array();

		$params = array(
			'kd_barang'	=> $this->input->post('kd_barang'),
			'tgl_exp'	=> $this->input->post('tgl_exp')
		);

		$result = $this->expired_report_model->getExpiredList($params);

		echo json_encode($result);
	}

	public function getWidget(){

		$result = array();

		$result = $this->expired_report_model->getWidget();

		echo json_encode($result);

	}
}
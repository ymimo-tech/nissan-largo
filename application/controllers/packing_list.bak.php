<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class packing_list extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");

		$this->load_model('api_integration_model');
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Packing List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/packing_list/lr-packinglist-view.js?1";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

        //$this->include_other_view_data_filter($data);
		$data['today'] = date('d/m/Y');

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Outbound');
        /* END OF INSERT LOG */

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	public function getPackingList($id = false){
		$result = array();

		$params = array(
			'id_shipping'		=> filter_var(trim($this->input->post('id_shipping')), FILTER_SANITIZE_STRING),
			'outbound'			=> filter_var(trim($this->input->post('outbound')), FILTER_SANITIZE_STRING),
			'pccode'			=> filter_var(trim($this->input->post('pccode')), FILTER_SANITIZE_STRING),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'status'			=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_NUMBER_INT),
			'warehouse'			=> $this->input->post('warehouse'),
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->packing_list_model->getPackingList($params);

		echo json_encode($result);
	}
	
	public function getItemPacking(){
		$result = array();

		$params = array(
					'packing_id' => $this->input->post('packing_id')
		);

		$result = $this->packing_list_model->getDetailPacking($params);

		echo json_encode($result);
	}
	
	public function printZpl(){
        date_default_timezone_set('Asia/Jakarta');
        $id_outbound = $this->input->post('id_outbound');
        $packing_number = $this->input->post('packing_number');
		$pc_code = substr($packing_number, 0, 12);
		$inc = substr($packing_number, 12, 14);
		
        $this->load->library('enc');
        $data = array();

        $params = array(
            'id_outbound'   => $id_outbound,
            'packing_number'   => $this->enc->decode($packing_number),
            'pc_code'   => $this->enc->decode($pc_code),
            'inc'   => $this->enc->decode($inc),
        );
        $id_outbound = $this->enc->decode($id_outbound);
		
		$result = array();
		$this->load->library('mimoclientprint');
        $this->load_model('receiving_model');

        $label = $this->packing_list_model->get_data_label($params);

        $delivery_date = date('d/m/Y',strtotime($label->delivery_date));

		$cmd = '';

		$sn = $packing_number;
		$pack = 'Colly '.$inc;
		//$snInfo = substr($sn, 0, 6) . " " . substr($sn, 6);

		$tpl = file_get_contents(APPPATH . 'views/label/packing_label.tpl');

		// $tpl = str_replace('{outbound_code}', $label->kd_outbound, $tpl);
		$tpl = str_replace('{{barcode}}', $sn, $tpl);
		$tpl = str_replace('{{text}}', $sn, $tpl);
		$tpl = str_replace('{{address}}', $label->DestinationAddress, $tpl);
		$tpl = str_replace('{{pack}}', $pack, $tpl);
		$tpl = str_replace('{{date}}', $delivery_date, $tpl);
		$tpl = str_replace('{{remark}}', $label->note, $tpl);
		$tpl = str_replace('{{ekspedisi}}', $label->ekspedisi, $tpl);
		$tpl = str_replace('{{destination}}', $label->DestinationName, $tpl);
		$tpl = str_replace('{{picking_code}}', $label->picking_code, $tpl);

			$cmd .= $tpl;

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);
        // $this->packing_model->update_staging_outbound_by_id_outbound($id_outbound);
		echo json_encode($result);
	}

	public function getPcCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->packing_list_model->getPcCode($params);

		echo json_encode($result);
	}
	
	public function getLoadingCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->packing_list_model->getLoadingCode($params);

		echo json_encode($result);
	}
	
	public function getOutboundCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->packing_list_model->getOutboundCode($params);

		echo json_encode($result);
	}

    public function cancel(){

		$result = array();

		$params = array(
			'shipping_id' => $this->input->post('shipping_id'),
		);

		$cancel = $this->loading_model->cancel($params);

		if($cancel){

			$result['status'] = "success";
			$result['message'] = "Cancel loading success.";

		}else{

			$result['status'] = "failed";
			$result['message'] = "Cancel loading failed.";

		}

		echo json_encode($result);

    }

    public function cancel_item(){

		$result = array();

		$params = array(
			'shipping_id'		=> filter_var(trim($this->input->post('shipping_id')), FILTER_SANITIZE_NUMBER_INT),
			'serial_number'		=> $this->input->post('serial_number')
		);

		$cancel = $this->loading_model->cancel_item($params);

		if($cancel){

			$result['status'] = "success";
			$result['message'] = "Cancel loading success.";

		}else{

			$result['status'] = "failed";
			$result['message'] = "Cancel loading failed.";

		}


		echo json_encode($result);

    }
}

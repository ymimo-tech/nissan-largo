<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class search extends MY_Controller {

    private $class_name;
	
    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);
		
        // Protection
        hprotection::login();
        //$this->access_right->check();
        //$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load_model(array('search_model'));
    }

    public function index() {
		$data = array();
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Search Result';
        $data['page'] = $this->class_name . '/index';
        
		$this->include_required_file($data);
		
		/* INSERT LOG */
		$this->access_right->activity_logs('view','Search');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }
	
	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
		
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}
	
	public function result($type, $id){
		$data = array();
		if($type == 'sn')
			$type = 'Serial Number';
		
        $data['title'] = '<i class="icon-plus-sign-alt"></i> ' . ucfirst($type) . ' Detail';
        $data['page'] = $this->class_name . '/index';
        
		$this->include_required_file($data);
		
		$data['type'] = $type;
		
		if($type == 'supplier'){
			$this->load->model('referensi_supplier_model');
			$data['supplier'] =  $this->referensi_supplier_model->get_by_id($id)->row_array();
		}
		
		if($type == 'item'){
			$this->load->model('referensi_barang_model');
			$data['items'] =  $this->referensi_barang_model->get_by_id($id)->row_array();
			$data['inbound'] = $this->search_model->getLastInbound($id);
			$data['receiving'] = $this->search_model->getLastReceived($id);
			$data['outbound'] = $this->search_model->getLastOutbound($id);
		}
		
		if($type == 'Serial Number'){
			$data['sn'] = $id;
			$data['items'] =  $this->search_model->getItemBySn($id);
			$data['inbound'] = $this->search_model->getInboundBySn($id);
			$data['receiving'] = $this->search_model->getReceivedBySn($id);
			$data['outbound'] = $this->search_model->getOutboundBySn($id);
			$data['picking'] = $this->search_model->getPickingBySn($id);
			$data['loading'] = $this->search_model->getLoadingBySn($id);
		}
			
		/* INSERT LOG */
		$this->access_right->activity_logs('view','Search');
        /* END OF INSERT LOG */

        if($data['items']){
    	    $this->load->view('template', $data);
	    }else{
	    	show_404();
	    }
	}
	
	//REQUEST
	public function getResult(){
		$result = array();
		
		$params = array(
			'keyword'	=> filter_var(trim(strtolower($this->input->post('query'))), FILTER_SANITIZE_STRING)
		);
		
		$result = $this->search_model->getResult($params);
		
		echo json_encode($result);
	}
	
	public function getReceivedList(){
		$result = array();
		
		$params = array(
			'id_receiving'	=> filter_var(trim($this->input->post('id_receiving')), FILTER_SANITIZE_NUMBER_INT),
			'id_barang'		=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT)
		);
		
		$result = $this->search_model->getReceivedList($params);
		
		echo json_encode($result);
	}
}
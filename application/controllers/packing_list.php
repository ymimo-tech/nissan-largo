<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class packing_list extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");

		$this->load_model('api_integration_model');
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Packing List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/packing_list/lr-packinglist-view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

        //$this->include_other_view_data_filter($data);
		$data['today'] = date('d/m/Y');

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Outbound');
        /* END OF INSERT LOG */

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $this->load->view('template', $data);
    }

	public function export_pack(){
		ini_set('memory_limit', '6143M');
		set_time_limit(60000);
        $this->load->library('PHPExcel');

		$params = array(
			'id_shipping'		=> filter_var(trim($this->input->post('id_shipping')), FILTER_SANITIZE_STRING),
			'outbound'		=> filter_var(trim($this->input->post('outbound')), FILTER_SANITIZE_STRING),
			'pccode'		=> filter_var(trim($this->input->post('pccode')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'status'		=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_STRING)
		);

		$result = $this->packing_list_model->export_pack($params);
        // dd($result);
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Pack-Export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
		$sheet->getColumnDimension('M')->setAutoSize(true);
		$sheet->getColumnDimension('N')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Packing Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Item Sub')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Item Ori')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'LPN')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Packing Doc Time')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'DN Doc')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Weight')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'Volume')->getStyle('H1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(8, 1, 'Packing Time')->getStyle('I1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(9, 1, 'Pack Qty')->getStyle('J1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(10, 1, 'User Pack')->getStyle('K1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(11, 1, 'Priority')->getStyle('L1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(12, 1, 'Sales Order')->getStyle('M1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(13, 1, 'Customer')->getStyle('N1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['packing_number']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['item_sub']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['item_ori']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['lpn']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['packing_date']);
			$sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['dn_name']);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['weight']);
			$sheet->setCellValueByColumnAndRow(7, $row, $result[$i]['volume']);
			$sheet->setCellValueByColumnAndRow(8, $row, $result[$i]['pack_time']);
			$sheet->setCellValueByColumnAndRow(9, $row, $result[$i]['pack_qty']);
			$sheet->setCellValueByColumnAndRow(10, $row, $result[$i]['user_pack']);
			$sheet->setCellValueByColumnAndRow(11, $row, $result[$i]['m_priority_name']);
			$sheet->setCellValueByColumnAndRow(12, $row, $result[$i]['sale_order']);
			$sheet->setCellValueByColumnAndRow(13, $row, $result[$i]['destination_name']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Pack-Export'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }


	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	public function getPackingList($id = false){
		$result = array();
		
		$params = array(
			'id_shipping'		=> filter_var(trim($this->input->post('id_shipping')), FILTER_SANITIZE_STRING),
			'outbound'			=> filter_var(trim($this->input->post('outbound')), FILTER_SANITIZE_STRING),
			'pccode'			=> $this->input->post('pccode'),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'status'			=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_NUMBER_INT),
			'warehouse'			=> $this->input->post('warehouse'),
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->packing_list_model->getPackingList($params);

		echo json_encode($result);
	}
	
	public function getItemPacking(){
		$result = array();

		$params = array(
					'packing_id' => $this->input->post('packing_id')
		);

		$result = $this->packing_list_model->getDetailPacking($params);

		echo json_encode($result);
	}

	public function delete_packing(){
		$results = array();
		$params = array(
			'oird_id' => $this->input->post('id')
		);

		$result = $this->packing_list_model->delete_packing($params);

		if($result){
			$results['status'] = "OK";
			$results['success'] = "true";
			$results['message'] = "delete success.";
		}else{
			$results['status'] = "failed";
			$results['success'] = "false";
			$results['message'] = "delete failed.";
		}

		echo json_encode($results);
	}
	
	public function printZpl(){
        date_default_timezone_set('Asia/Jakarta');
        $id_outbound = $this->input->post('id_outbound');
        $packing_number = $this->input->post('packing_number');
		$pc_code = substr($packing_number, 0, 12);
		$inc = substr($packing_number, 12, 14);
		
        $this->load->library('enc');
        $data = array();

        $params = array(
            'id_outbound'   => $id_outbound,
            'pc'   => $packing_number,
            'packing_number'   => $this->enc->decode($packing_number),
            'pc_code'   => $this->enc->decode($pc_code),
            'inc'   => $this->enc->decode($inc),
        );
		
        $id_outbound = $this->enc->decode($id_outbound);
		
		$result = array();
		$this->load->library('mimoclientprint');
        $this->load_model('receiving_model');

        // $label = $this->packing_list_model->get_data_label($params);
		$sql = "select ho.packing_number, d.destination_address daddress, d.destination_code dcode, d.destination_name dname, o.note note, o.ekspedisi ekspedisi, string_agg(distinct(p.name),', ') picking_code, g.gate_name gate_name, g.gate_code gate_code, ho.weight weight, ho.volume volume, ho.time delivery_date from header_oird ho
		left join destinations d on d.destination_id = ho.destination_id
		left join gate g on g.id = d.gate
		left join outbound_item_receiving_details oird on oird.packing_number = ho.id
		left join picking_recomendation pr on pr.id = oird.pr_id
		left join outbound o on pr.outbound_id = o.id
		left join pickings p on p.pl_id = pr.picking_id 
		where ho.packing_number = '".$packing_number."'
		group by ho.packing_number, d.destination_address, d.destination_code, d.destination_name, o.note, o.ekspedisi, g.gate_name, g.gate_code, ho.weight, ho.volume, ho.time, p.date";

		$label = $this->db->query($sql)->row();

        $delivery_date = date('d/m/Y',strtotime($label->delivery_date));

		$cmd = '';

		$sn = $packing_number;
		$pack = 'Colly '.$inc;
		//$snInfo = substr($sn, 0, 6) . " " . substr($sn, 6);

		$tpl = file_get_contents(APPPATH . 'views/label/packing_label.tpl');

		// $tpl = str_replace('{outbound_code}', $label->kd_outbound, $tpl);
		$tpl = str_replace('{{barcode}}', $sn, $tpl);
		$tpl = str_replace('{{Packing_no}}', $sn, $tpl);
		$tpl = str_replace('{{Destination Address}}', $label->daddress, $tpl);
		$tpl = str_replace('{{Destination Code}}', $label->dcode, $tpl);
		$tpl = str_replace('{{Destination Name}}', $label->dname, $tpl);
		$tpl = str_replace('{{pack}}', $pack, $tpl);
		$tpl = str_replace('{{date}}', $delivery_date, $tpl);
		$tpl = str_replace('{{remark}}', $label->note, $tpl);
		$tpl = str_replace('{{ekspedisi}}', $label->ekspedisi, $tpl);
		$tpl = str_replace('{{destination}}', $label->dname, $tpl);
		$tpl = str_replace('{{picking_code}}', $label->picking_code, $tpl);
		$tpl = str_replace('{{weight}}', $label->weight, $tpl);
		$tpl = str_replace('{{volume}}', $label->volume, $tpl);
		$tpl = str_replace('{{gate_name}}', $label->gate_name, $tpl);
		$tpl = str_replace('{{gate_code}}', $label->gate_code, $tpl);
		$tpl = str_replace('{{picking_code}}', $label->picking_code, $tpl);

			$cmd .= $tpl;

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);
        // $this->packing_model->update_staging_outbound_by_id_outbound($id_outbound);
		echo json_encode($result);
	}

	public function getPcCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->packing_list_model->getPcCode($params);

		echo json_encode($result);
	}

	public function getDimension(){
		$result = array();
		$params = array(
			'id' => $this->input->post('id')
		);

		$result = $this->packing_list_model->getDimension($params);

		echo json_encode($result);
	}

	public function editDimension(){
		$result = array();
		$params = array(
			'id' => $this->input->post('id'),
			'weight' => $this->input->post('weight'),
			'volume' => $this->input->post('volume')
		);

		$result = $this->packing_list_model->editDimension($params);

		echo json_encode($result);
	}
	
	public function getLoadingCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->packing_list_model->getLoadingCode($params);

		echo json_encode($result);
	}
	
	public function getOutboundCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->packing_list_model->getOutboundCode($params);

		echo json_encode($result);
	}

    public function cancel(){

		$result = array();

		$params = array(
			'shipping_id' => $this->input->post('shipping_id'),
		);

		$cancel = $this->loading_model->cancel($params);

		if($cancel){

			$result['status'] = "success";
			$result['message'] = "Cancel loading success.";

		}else{

			$result['status'] = "failed";
			$result['message'] = "Cancel loading failed.";

		}

		echo json_encode($result);

    }

    public function cancel_item(){

		$result = array();

		$params = array(
			'shipping_id'		=> filter_var(trim($this->input->post('shipping_id')), FILTER_SANITIZE_NUMBER_INT),
			'serial_number'		=> $this->input->post('serial_number')
		);

		$cancel = $this->loading_model->cancel_item($params);

		if($cancel){

			$result['status'] = "success";
			$result['message'] = "Cancel loading success.";

		}else{

			$result['status'] = "failed";
			$result['message'] = "Cancel loading failed.";

		}


		echo json_encode($result);

    }

}
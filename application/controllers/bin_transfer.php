<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class bin_transfer extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();

        // Check Otoritas
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> BIN TRANSFER JOBS';
        $data['page'] = $this->class_name . '/index';

		    $this->load->helper('diff_helper');

		    $this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		    $data['jsPage'][] = "assets/pages/scripts/bin_transfer/view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

        $this->include_other_view_data_filter($data);

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Bin Trasfer');
        /* END OF INSERT LOG */

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

    private function include_other_view_data_filter(&$data) {

    	$this->load_model('outbound_document_model');
        $data['outbound_docs'] = $this->outbound_document_model->options();

		$data['today'] = date('d/m/Y');
		$data['status']	= $this->bin_transfer_model->getStatus();

    }

	//ADD
	public function add(){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> NEW BIN TRANSFER';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/bin_transfer/addedit.js";

            $this->load_model('warehouse_model');
            $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

			/* INSERT LOG */
			$this->access_right->activity_logs('edit','Add Bin Transfer Document');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);
		}
	}

	public function edit($binTransferId=''){

		if ($this->access_right->otoritas('edit') && !empty($binTransferId)) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Bin Transfer';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);
			$data['jsPage'][] = "assets/pages/scripts/bin_transfer/addedit.js";

            $this->load_model('warehouse_model');
            $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

            $data['id'] = $binTransferId;

			/* INSERT LOG */
			$this->access_right->activity_logs('edit','Edit Bin Transfer Document');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);
		}else{
			show_404();
		}
	}

    public function get_add_item_row() {
    	$this->load_model('referensi_barang_model');
        $data['items'] = $this->referensi_barang_model->options();
        $data['random_string'] = $this->randomidgenerator->generateRandomString(4);
        echo $this->load->view($this->class_name."/add_item_row", $data, true);
    }

	public function getList(){

		$result = array();

		$params = array(
			'bin_transfer_doc'	=> $this->input->post('bin_transfer_doc'),
			'status'			=> $this->input->post('status')
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->bin_transfer_model->getList($params);

		echo json_encode($result);

	}

    public function getDocumentCode(){

        $result = array();

        $docInc = $this->db->get('m_increment')->row_array();

        $result['code'] = $docInc['bin_transfer_code'].$docInc['bin_transfer_inc'];

        echo json_encode($result);

    }

    public function getUomByItem(){

        $result = array();

        $params= array(
            "id_barang" => $this->input->post('id_barang')
        );

        $this->db
            ->select('unt.id as unit_id, unt.code as unit_code')
            ->from('items itm')
            ->join('units unt','unt.id=itm.unit_id or unt.id=itm.convert_qty_unit_id_1 or unt.id=itm.convert_qty_unit_id_2')
            ->where('itm.id',$params['id_barang']);

        $result = $this->db->get()->result_array();

        echo json_encode($result);

    }

    public function proses(){

        $result = array();

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('bin_transfer_code', 'Bin Transfer Code', 'required|trim');
            $this->form_validation->set_rules('warehouse', 'Warehouse of Bin Transfer', 'required|trim');

            if ($this->form_validation->run()) {

                $id = $this->input->post('id');

            	$params = array(
            		"bin_transfer_code"		=> $this->input->post('bin_transfer_code'),
            		"bin_transfer_remark"	=> $this->input->post("remark"),
            		"warehouse_id"			=> $this->input->post('warehouse'),
            		"status_id"				=> 3,
                    'user_id'               => $this->session->userdata('user_id')
            	);

            	$paramsItem = array(
            		"items"	=> $this->input->post("id_barang"),
            		"qty"	=> $this->input->post('item_quantity'),
            		"units"	=> $this->input->post('unit')
            	);

              // dd([$params,$paramsItem]);

				$binTransfer = false;

				if(empty($id)){
					$binTransfer = $this->bin_transfer_model->create($params,$paramsItem);
				}else{
					$binTransfer = $this->bin_transfer_model->update($params,$paramsItem,$id);
				}

				if($binTransfer){

					$result = array(
						"status"	=> 1,
						"message"	=> "Save Success."
					);

				}else{

					$result = array(
						"status"	=> 0,
						"message"	=> "Failed, Please try again !."
					);

				}

            }else{

            	$result = array(
            		"status"	=> 0,
            		"message"	=> validation_errors()
            	);

            }

        }

        echo json_encode($result);

    }

	public function getEdit() {

    	$result = array(
    		"status"	=> 0,
    		"message"	=> "Data not found."
    	);

        if ($this->access_right->otoritas('edit')) {

            $data = $this->db->get_where('bin_transfer',array('bin_transfer_id' => $this->input->post("id")))->row_array();

            if($data){

            	$result = array(
            		"status"	=> 1,
            		"message"	=> "",
            		"d"			=> $data
            	);

	            $binTransferItem = $this->bin_transfer_model->getBinTransferItem($this->input->post("id"));

	            $rows = "";

		    	$this->load_model('referensi_barang_model');
		        $dataBinTransfer['items'] = $this->referensi_barang_model->options();

	            foreach ($binTransferItem as $binItem) {
	                $dataBinTransfer['data'] = $binItem;
	                $rows .= $this->load->view("bin_transfer/add_item_row", $dataBinTransfer, true);
	            }

	            $result['d']['items'] = $rows;

            }

        }

        echo json_encode($result);

    }

    public function delete() {

        $result = array();

        if ($this->access_right->otoritas('delete', true)) {

            $id = $this->input->post("id");
            $delete = $this->bin_transfer_model->delete($id);

            if($delete){

                $result = array(
                    "status"    => 1,
                    "message"   => "Delete success."
                );

            }else{

                $result = array(
                    "status"    => 0,
                    "message"   => "Can't delete this data, because has been scanned."
                );

            }

        } else {

            $result = array(
                "status"    => 0,
                "message"   => "You do not have permission."
            );

        }

        echo json_encode($result);
    }

    public function detail($binTransferId=""){

        if ($this->access_right->otoritas('view')) {

            $data['title'] = '<i class="icon-plus-sign-alt"></i> Bin Transfer Document';
            $data['page'] = $this->class_name . '/detail';

            $this->include_required_file($data);

            $data['jsPage'][] = "assets/pages/scripts/bin_transfer/detail.js";

            $data['id'] = $binTransferId;
            $data['data'] = $this->bin_transfer_model->getDetail($binTransferId);

            /* INSERT LOG */
            $this->access_right->activity_logs('view','Bin Transfer Document Detail');
            /* END OF INSERT LOG */

            $this->load->view('template', $data);

        }

    }

    public function getDetailItem($id = false){

        $result = array();

        $params = array(
            'bin_transfer_id'    => filter_var(trim($this->input->post('bin_transfer_id')), FILTER_SANITIZE_NUMBER_INT)
        );

        $result = $this->bin_transfer_model->getDetailItem($params);

        echo json_encode($result);
    }

    public function getScannedList(){

        $result = array();

        $params = array(
            'bin_transfer_id'    => filter_var(trim($this->input->post('bin_transfer_id')), FILTER_SANITIZE_NUMBER_INT),
            'item_code'          => $this->input->post('item_code')
        );

        $result = $this->bin_transfer_model->getscannedList($params);

        echo json_encode($result);

    }

}

/* End of file referensi_barang.php */
/* Location: ./application/controllers/referensi_barang.php */

<?php

/**
 * Description of referensi_lokasi
 *
 * @author SANGGRA HANDI
 */
class referensi_lokasi extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Bin Info';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        //$data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-lokasi.js";

        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

		$data['location_type'] = array(
			'BINLOC'		=> 'BINLOC',
			'INBOUND'		=> 'INBOUND',
			'OUTBOUND'		=> 'OUTBOUND',
			'QC'			=> 'QC',
			'NG'			=> 'NG'
		);

        $this->load_model('referensi_lokasi_area_model');
        $this->load_model('referensi_lokasi_type_model');
        $this->load_model('referensi_kategori_model');

        $data['loc_category'] = $this->referensi_kategori_model->options();
        $data['loc_area'] = $this->referensi_lokasi_area_model->options();
        $data['loc_type'] = $this->referensi_lokasi_type_model->options();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Referensi Lokasi');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

    private function include_other_view_data_filter(&$data) {
        $data['suppliers'] = $this->inbound_model->create_options("supplier", "id_supplier", "kd_supplier,nama_supplier", "-- All --");
        $data['inbound_docs'] = $this->inbound_model->create_options("m_inbound_document", "id_inbound_document", "inbound_document_name", "-- All --");
        $data['inbound_status'] = $this->inbound_model->create_options("m_status_inbound", "id_status_inbound", "nama_status", "-- All --");
    }

    public function options(){

        $query = $this->input->post('query');

        if(!empty($query)){
            $this->db->like('lower(a.name)',strtolower($query));
        }

        $locations = $this->referensi_lokasi_model->get_data()->result_array();

        $data= array();

        foreach ($locations as $loc) {
            $data['results'][] = array(
                'id' => $loc['loc_id'],
                'name' => $loc['loc_name']
            );
        }

        echo json_encode($data);
    }

	public function export(){
		$this->load->library('PHPExcel');

		$params = array();

		$result = $this->referensi_lokasi_model->getLocation($params);
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Master-Location")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Bin Name')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Bin Description')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Bin Type')->getStyle('C1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['loc_name']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['loc_desc']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['loc_type']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Master-Bin'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

	public function getQtyPrint(){
		$result = array();

		$result = $this->referensi_lokasi_model->getQtyLocation();

		echo json_encode($result);
	}

	public function printZpl(){
		$result = array();
		$this->load->library('mimoclientprint');
		$this->load->helper('print_helper');

		$params = $this->input->post('params');
		$cmd = '';
		$i = 1;

		if($params == 'multiple'){

			$location = $this->referensi_lokasi_model->getLocation();
			$len = count($location);

			foreach($location as $row){

				$tpl = file_get_contents(APPPATH . 'views/label/rack_label.tpl');

				$tpl = str_replace('{{level}}', $row['level'], $tpl);
				$tpl = str_replace('{{rack}}', $row['rack'], $tpl);
				$tpl = str_replace('{{row}}', $row['row'], $tpl);
				$tpl = str_replace('{{barcode}}', $row['loc_name'], $tpl);
                $tpl = str_replace('{{text}}', $row['loc_name'], $tpl);
                $tpl = str_replace('{{bin}}', $row['bin'], $tpl);
                $tpl = str_replace('{{color_code_bin}}', $row['color_code_bin'], $tpl);
                $tpl = str_replace('{{color_code_level}}', $row['color_code_level'], $tpl);

				$cmd .= $tpl;

            }

		}else if($params == 'partial'){

			$params = array();
			$params['loc_id'] = $this->input->post('loc_id');

			$location = $this->referensi_lokasi_model->getLocationById($params);

			foreach($location as $row){

                $defaultLocation = array(100,101,102,103,104,105,106,107);
                if(in_array($row['loc_id'], $defaultLocation)){

                    $tpl = file_get_contents(APPPATH . 'views/label/rack_default_label.tpl');

                    $tpl = str_replace('{barcode}', $row['loc_name'], $tpl);

                    $cmd .= $tpl;

                }else{

    				$tpl = file_get_contents(APPPATH . 'views/label/rack_label.tpl');

                    $tpl = str_replace('{{level}}', $row['level'], $tpl);
                    $tpl = str_replace('{{rack}}', $row['rack'], $tpl);
                    $tpl = str_replace('{{row}}', $row['row'], $tpl);
                    $tpl = str_replace('{{barcode}}', $row['loc_name'], $tpl);
                    $tpl = str_replace('{{text}}', $row['loc_name'], $tpl);
                    $tpl = str_replace('{{bin}}', $row['bin'], $tpl);
                    $tpl = str_replace('{{color_code_bin}}', $row['color_code_bin'], $tpl);
                    $tpl = str_replace('{{color_code_level}}', $row['color_code_level'], $tpl);

    				$cmd .= $tpl;

                }

            }

		}else{

			$params = array();
			$params['name'] = $this->input->post('code');

			$location = $this->referensi_lokasi_model->getLocationByName($params);

			foreach($location as $row){

				$tpl = file_get_contents(APPPATH . 'views/label/rack_label.tpl');

                $tpl = str_replace('{{level}}', $row['level'], $tpl);
                $tpl = str_replace('{{rack}}', $row['rack'], $tpl);
                $tpl = str_replace('{{row}}', $row['row'], $tpl);
                $tpl = str_replace('{{barcode}}', $row['loc_name'], $tpl);
                $tpl = str_replace('{{text}}', $row['loc_name'], $tpl);
                $tpl = str_replace('{{bin}}', $row['bin'], $tpl);
                $tpl = str_replace('{{color_code_bin}}', $row['color_code_bin'], $tpl);
                $tpl = str_replace('{{color_code_level}}', $row['color_code_level'], $tpl);

				$cmd .= $tpl;

            }

		}

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);

		echo json_encode($result);
	}

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $lokasi = $this->referensi_lokasi_model->get_data(array('loc_id' => $id))->row_array();
            $process_result = $lokasi;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){

        $columns = array(
            2 => 'loc_name',
            3 => 'loc_desc',
            4 => 'nama_kategori',
            5 => 'loc_type',
            6 => 'loc_area_name',
            7 => 'a.status'
        );

        $param = array();

		$param['a.loc_name IS NOT NULL'] = null;
        $post =$_REQUEST;

        $params = array();

        if(!empty($post['bin_name'])){
            $params['a.loc_name LIKE "%'.$post['bin_name'].'%"'] = null;
        }

        if(!empty($post['bin_desc'])){
            $params['a.loc_desc LIKE "%'.$post['bin_desc'].'%"'] = null;
        }

        if(!empty($post['category'])){
            $params['a.loc_category_id '] = $post['category'];
        }

        if(!empty($post['type'])){
            $params['a.loc_type_id '] = $post['type'];
        }

        if(!empty($post['area'])){
            $params['a.loc_area_id '] = $post['area'];
        }
		
		if(!empty($post['status'])){
            $params['a.status '] = $post['status'];
        }

        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $lokasi = $this->referensi_lokasi_model->data($params)->get();
        $iTotalRecords = $this->referensi_lokasi_model->data($params)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();

        $records["data"] = array();

        $i=1;
        foreach($lokasi->result() as $value){
            //---------awal button action---------
            $action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print" data-id="'.$value->loc_id.'"><i class="fa fa-print"></i> Print</a>';
				$action .= '</li>';
            }

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit" data-id="'.$value->loc_id.'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete" data-id="'.$value->loc_id.'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';
            //$action = $this->buttons->actions(array('edit'=>$value->loc_id,'delete' => $value->loc_id));
			
			$statusText = '';
			if(strtoupper($value->status) == 'ACTIVE'){
				$statusText = "<span class='label label-sm label-default' style='background:green;'>".strtoupper($value->status)."</span>";
			} else {
				$statusText = "<span class='label label-sm label-danger'>".strtoupper($value->status)."</span>";
			}

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="'.$value->loc_id.'">',
                $_REQUEST['start']+$i,
                $value->loc_name,

                $value->loc_desc,
                $value->nama_kategori,
                $value->loc_type_name,
                $value->loc_area_name,
                $statusText,
                $action
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function add($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load_model('referensi_lokasi_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->referensi_lokasi_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;



            $this->load->view($this->class_name . '/form2', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
		$this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function proses() {
        $proses_result['success'] = 1;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('loc_name', 'Nama Lokasi', 'required|trim');
            $this->form_validation->set_rules('loc_category_id', 'Bin Category', 'required');
            $this->form_validation->set_rules('loc_area_id', 'Bin Area', 'required');
            $this->form_validation->set_rules('loc_type_id', 'Bin Type', 'required');
            //$this->form_validation->set_rules('loc_desc', 'Deskripsi Lokasi', 'required|trim');


            if ($this->form_validation->run()) {
                $id = $this->input->post('id');

                $data = array(
                    'name'	=> $this->input->post('loc_name'),
                    'description' 	=> $this->input->post('loc_desc'),
                    'location_category_id' 	=> $this->input->post('loc_category_id'),
                    'location_area_id' 	=> $this->input->post('loc_area_id'),
                    'location_type_id' 	=> $this->input->post('loc_type_id'),
                    'rack'  => $this->input->post('rack'),
                    'row'  => $this->input->post('row'),
                    'level'  => $this->input->post('level'),
                    'bin'  => $this->input->post('bin'),
                    'color_code_bin' => $this->input->post('color_code_bin'),
                    'color_code_level' => $this->input->post('color_code_level')
                );

				$locType = trim($this->input->post('loc_type'));
				if(!empty($locType))
					$data['loc_type'] = $locType;

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->referensi_lokasi_model->create($data)) {
                        $proses_result['add'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Referensi Lokasi');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->referensi_lokasi_model->update($data, $id)) {
                        $proses_result['edit'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Referensi Lokasi');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $processResult = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            $processResult = $this->referensi_lokasi_model->delete($id);
        } else {
            $processResult['status'] = 'ERR';
			$processResult['message'] = 'You dont have authority to delete data';
        }
        echo json_encode($processResult);
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Bin Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        'Bin Name'          => 'location_name',
                        'Bin Description'   => 'location_description',
						'Bin Type'          => 'location_type',
                        'Bin Category'      => 'location_category',
                        'Bin Area'          => 'location_area',
						'Bin Rack'          => 'location_rack',
                        'Bin Row'           => 'location_row',
                        'Bin Level'         => 'location_level',
                        'Bin Bin'           => 'location_bin',
                        'Bin Color Code Bin'    => 'location_color_code_bin',
                        'Bin Color Code Level'  => 'location_color_code_level',
                        'Warehouse'  		=> 'warehouse'
                    ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->referensi_lokasi_model->importFromExcel($data);

                        if($process['status']){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage($process['message']);

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }

        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }

}

/* End of file referensi_lokasi.php */
/* Location: ./application/controllers/referensi_lokasi.php */

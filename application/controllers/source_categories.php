<?php

/**
 * Description of referensi_supplier
 *
 * @author Hady Pratama
 */
class source_categories extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {

        $data['title'] = '<i class="icon-plus-sign-alt"></i> Source / Destination Categories Master';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;

        /* JS Page*/
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = "assets/pages/scripts/source_categories/source_categories.js";
		$data['jsPage'][] = "assets/custom/js/share.js";
        
        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

		/* Otoritas Tombol tambah */
    	$data['button_group'] = array();
    	if ($this->access_right->otoritas('add')) {
    		$data['button_group'] = array(
    			anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
    		);
    	}

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Source Categories Master');
        /* END OF INSERT LOG */

        $this->load->view('template', $data);

    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $data = $this->source_categories_model->get_data(array('source_category_id' => $id))->row_array();

            $process_result = $data;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){

        $columns = array(
            0 => '',
            1 => 'source_category_name',
            2 => 'source_category_code'
        );
        
        $param = '';

        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $supplier = $this->source_categories_model->data($param)->get();
        $iTotalRecords = $this->source_categories_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($supplier->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->source_category_id,'delete' => $value->source_category_id));

            $records["data"][] = array(
 
                $_REQUEST['start']+$i,
                $value->source_category_code,
 				$value->source_category_name,
                $action
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function add($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load_model('source_categories_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->source_categories_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;



            $this->load->view($this->class_name . '/form2', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
		$this->access_right->otoritas('edit', true);
        $this->add($id);
    }

	public function export(){
		$this->load->library('PHPExcel');

		$params = array();

		$result = $this->source_categories_model->get_data($params)->result_array();
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Master-Supplier")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Source Category Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Source Category Name')->getStyle('B1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['source_category_code']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['source_category_name']);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Source-Category-Master'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

    public function proses() {
        $proses_result['success'] = 1;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('source_category_code', 'Source Category Code', 'required|trim');
            $this->form_validation->set_rules('source_category_name', 'Source Category Name', 'required|trim');

            if ($this->form_validation->run()) {
                $id = $this->input->post('id');

                $data = array(
                    'source_category_code' => $this->input->post('source_category_code'),
                    'source_category_name' => $this->input->post('source_category_name'),
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $create = $this->source_categories_model->create($data);
                    if ($create) {
                        // $id = $this->db->insert_id();
                        $proses_result['add'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Source Categories Master');
						/* END OF INSERT LOG */

                    }
                } else {
                    if ($this->source_categories_model->update($data, $id)) {
                        $proses_result['edit'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Source Categories Master');
						/* END OF INSERT LOG */
                    }
                }


            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();

        if ($this->access_right->otoritas('delete', true)) {

            $id = $this->input->post("id");
            $result = $this->source_categories_model->delete($id);

        } else {

            $result['status'] = 'ERR';
			$result['no_delete'] = 1;

        }

        echo json_encode($result);
    }

    function sync_supplier(){
        $this->db->trans_start();
        $new_suppliers =  $this->source_categories_model->get_new_supplier();
        $new=array();
        foreach($new_suppliers->result() as $supplier){
            // SupplierCode
            // SupplierName
            // AddressL1
            // AddressL2
            // AddressL3
            // Phone
            // ContactPerson
            // SyncStatus
            // SyncDate

            $data_supplier=array(
                'kd_supplier'=>$supplier->SupplierCode,
                'nama_supplier'=>$supplier->SupplierName,
                'alamat_supplier'=>$supplier->AddressL1.' '.$supplier->AddressL2.' '.$supplier->AddressL3,
                'telepon_supplier'=>$supplier->Phone,
                'cp_supplier'=>$supplier->ContactPerson,
                'email_supplier'=>''
            );
            $new_supplier_id = $this->source_categories_model->create_supplier($data_supplier);
            $SyncDate=date('Y-m-d H:i:s');
            $this->source_categories_model->set_supplier_status($supplier->SupplierCode,$SyncDate);
            $new[]=array('kd_supplier'=>$supplier->SupplierCode,'nama'=>$supplier->SupplierName,'id'=>$new_supplier_id);
        }
        $this->db->trans_complete();
        return $new;
    }

    function longpolling(){
	    date_default_timezone_set("Asia/Jakarta");
	    header('Cache-Control: no-cache');
	    header("Content-Type: text/event-stream\n\n");
	    while (1) {
            //check new supplier
            $new = $this->sync_supplier();
            //sleep(10);
            //$new = array(array('nomor'=>'PO170411-001','id'=>48),array('nomor'=>'PO170425-001','id'=>49));
	        $output = array('newc'=>$new);
	        echo "\nevent: sync\n";
	        echo 'data: '.json_encode($output);
	        echo "\n\n";
	        ob_flush();
	        flush();
            session_write_close();
	        sleep(30);
	    }
	}

    function manual_sync_supplier(){
        date_default_timezone_set("Asia/Jakarta");
        $new = $this->sync_supplier();
        $json = array('newc'=>$new);
        echo json_encode($json);
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Source Category Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        'Source Category Code'         =>  'source_category_code',
                        'Source Category Name'         =>  'source_category_name'
                    ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->source_categories_model->importFromExcel($data);

                        if($process['status']){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage($process['message']);

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }
 
        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }    
}

/* End of file referensi_supplier.php */
/* Location: ./application/controllers/referensi_supplier.php */

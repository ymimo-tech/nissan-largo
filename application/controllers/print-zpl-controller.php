<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class packing extends CI_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);
    }

    public function printZpl(){
        date_default_timezone_set('Asia/Jakarta');
        $id_outbound = $this->input->post('id_outbound');
        $this->load->library('enc');
        $data = array();

        $params = array(
            'id_outbound'   => $this->enc->decode($id_outbound)
        );

        $qty_print = $this->packing_model->get_qty_print($params);
		$result = array();
		$this->load->library('mimoclientprint');
        $this->load->model('receiving_model');

		$qty = $qty_print;
        $label = $this->packing_model->get_data_label($params);
		$cmd = '';

		for($i = 1; $i <= $qty; $i++) {
            if($i<10){
                $pad=0;
            }else{
                $pad='';
            }
            if($qty<10){
                $pad2=0;
            }else{
                $pad2='';
            }

            $sn = $label->packing_number.$pad.$i.$pad2.$qty;
            $pack = 'Pack '.$i.'/'.$qty;
            
			//$snInfo = substr($sn, 0, 6) . " " . substr($sn, 6);

			$tpl = file_get_contents(APPPATH . 'views/label/outer_label2.tpl');

			$tpl = str_replace('{{ sn }}', $sn, $tpl);
			$tpl = str_replace('{{ tanggal_outbound }}', $label->tanggal_outbound, $tpl);
            $tpl = str_replace('{{ name }}', $label->DestinationName, $tpl);
            $tpl = str_replace('{{ phone }}', $label->Phone, $tpl);
            $tpl = str_replace('{{ shipping_note }}', $label->ShippingNote, $tpl);
            $tpl = str_replace('{{ alamat_1 }}', $label->DestinationAddressL1, $tpl);
            $tpl = str_replace('{{ alamat_2 }}', $label->DestinationAddressL2, $tpl);
            $tpl = str_replace('{{ alamat_3 }}', $label->DestinationAddressL3, $tpl);
            $tpl = str_replace('{{ pack }}', $pack, $tpl);

			$cmd .= $tpl;
		}

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);
        $this->packing_model->update_staging_outbound();
		echo json_encode($result);
	}
}

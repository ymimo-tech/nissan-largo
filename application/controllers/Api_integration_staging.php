<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require "api/Api.php";
class api_integration_staging extends Api {

	function __construct() {

		parent::__construct();

		$this->load_model('api_integration_model');
		$this->load_model('item_stok_model');
		
	}
	
	public function getInboundDoc(){
		$data = $this->api_integration_model->getInboundList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['DOCENTRY'];
				$param['po_number'] = $data[$i]['BASETYPE'];
				$param['st_staging'] = $data[$i]['BASELINE'];
				$param['code'] = $data[$i]['DOCNUM'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['id_supplier'] = $data[$i]['CARDCODE'];
				$param['document_code'] = 'PO';
				$param['user'] = 'admin';
				$param['remark'] = '';
				$param['id_warehouse'] = isset($data[$i]['WAREHOUSE']) ? $data[$i]['WAREHOUSE'] : 'SA1';
				if($param['id_warehouse'] == 'KAB' || $param['id_warehouse'] == 'WIT') {
					$param['id_warehouse'] = 'SA1';
				}
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_inbound = $this->api_integration_model->insert_inbound($param);
				
				$itemList = $this->api_integration_model->getItemListByInboundCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_inbound_item($param);
				}
			}
		}
	}
	
	public function getOutboundDoc(){
		$data = $this->api_integration_model->getOutboundList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_outbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['DOCENTRY'];
				$param['code'] = $data[$i]['DOCNUM'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['shipping_group'] = '';
				$param['customer_id'] = $data[$i]['CARDCODE'];
				$param['document_code'] = 'SO';
				$param['user'] = 'admin';
				$param['note'] = ($data[$i]['REMARKS'] != NULL) ? $data[$i]['REMARKS'] : '';
				$param['ekspedisi'] = ($data[$i]['EKSPEDISI'] != NULL) ? $data[$i]['EKSPEDISI'] : '';
				$param['m_priority_id'] = ($data[$i]['PRIORITY'] == 'NORMAL') ? 1 : 2;
				$param['id_warehouse'] = $data[$i]['WAREHOUSE'];
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				$insert_outbound = $this->api_integration_model->insert_outbound($param);
				
				if($insert_outbound) {
					$itemList = $this->api_integration_model->getItemListByOutboundCode($param);
					
					for($j = 0; $j < count($itemList); $j++) {
						$param['item_id'] = $itemList[$j]['ITEMCODE'];
						$param['qty'] = $itemList[$j]['QTY'];
						
						$insert_outbound_item = $this->api_integration_model->insert_outbound_item($param);
					}
				}
			}
		}
	}
	
	public function getGRDoc(){
		$data = $this->api_integration_model->getGRDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['po_number'] = $data[$i]['BASETYPE'];
				$param['st_staging'] = $data[$i]['BASELINE'];
				$param['code'] = $data[$i]['DOCNUM'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['id_supplier'] = 'INTERNAL';
				$param['document_code'] = $data[$i]['GRTYPE'];
				$param['user'] = 'admin';
				$param['remark'] = '';
				$param['id_warehouse'] = isset($data[$i]['WAREHOUSE']) ? $data[$i]['WAREHOUSE'] : 'SA1';
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_inbound = $this->api_integration_model->insert_inbound($param);
				
				$itemList = $this->api_integration_model->getItemListByGRDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_inbound_item($param);
				}
			}
		}
	}
	
	public function getGIDoc(){
		$data = $this->api_integration_model->getGIDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_outbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['code'] = $data[$i]['DOCNUM'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['shipping_group'] = '';
				$param['customer_id'] = 'INTERNAL';
				$param['document_code'] = $data[$i]['GITYPE'];
				$param['user'] = 'admin';
				$param['note'] = '';
				$param['m_priority_id'] = 1;
				$param['id_warehouse'] = $data[$i]['WAREHOUSE'];
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				$insert_outbound = $this->api_integration_model->insert_outbound($param);
				
				$itemList = $this->api_integration_model->getItemListByGIDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_outbound_item($param);
				}
			}
		}
	}
	
	public function getSalesReturnDoc(){ // Sales Return - Inbound
		$data = $this->api_integration_model->getSalesReturnDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['po_number'] = $data[$i]['BASETYPE'];
				$param['st_staging'] = $data[$i]['BASELINE'];
				$param['code'] = isset($data[$i]['DOCNUM']) ? $data[$i]['DOCNUM'] : 'SRTN-' . time() . $i;
				$param['document_code'] = 'SALES RETURN';
				$param['id_supplier'] = $data[$i]['CARDCODE'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['user'] = 'admin';
				$param['remark'] = '';
				$param['id_warehouse'] = isset($data[$i]['WAREHOUSE']) ? $data[$i]['WAREHOUSE'] : 'SA1';
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_inbound = $this->api_integration_model->insert_inbound($param);
				
				$itemList = $this->api_integration_model->getItemListBySalesReturnDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_inbound_item($param);
				}
			}
		}
	}
	
	public function getGoodsReturnDoc(){ // Goods Return - Outbound
		$data = $this->api_integration_model->getGoodsReturnDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['po_number'] = $data[$i]['BASETYPE'];
				$param['st_staging'] = $data[$i]['BASELINE'];
				$param['shipping_group'] = '';
				$param['code'] = isset($data[$i]['DOCNUM']) ? $data[$i]['DOCNUM'] : 'GRTN-' . time() . $i;
				$param['document_code'] = 'GOODS RETURN';
				$param['customer_id'] = $data[$i]['CARDCODE'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['user'] = 'admin';
				$param['note'] = '';
				$param['m_priority_id'] = 1;
				$param['id_warehouse'] = isset($data[$i]['WAREHOUSE']) ? $data[$i]['WAREHOUSE'] : 'SA1';
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_outbound = $this->api_integration_model->insert_outbound($param);
				
				$itemList = $this->api_integration_model->getItemListByGoodsReturnDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_outbound_item = $this->api_integration_model->insert_outbound_item($param);
				}
			}
		}
	}
	
	public function getAPCMDoc(){ // AP Credit Memo - Outbound
		$data = $this->api_integration_model->getAPCMDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['po_number'] = $data[$i]['BASETYPE'];
				$param['st_staging'] = $data[$i]['BASELINE'];
				$param['code'] = isset($data[$i]['DOCNUM']) ? $data[$i]['DOCNUM'] : 'APCM-' . time() . $i;
				$param['document_code'] = 'AP CREDIT MEMO';
				$param['shipping_group'] = '';
				$param['note'] = '';
				$param['m_priority_id'] = 1;
				$param['customer_id'] = $data[$i]['CARDCODE'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['user'] = 'admin';
				$param['remark'] = '';
				$param['id_warehouse'] = isset($data[$i]['WAREHOUSE']) ? $data[$i]['WAREHOUSE'] : 'SA1';
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_inbound = $this->api_integration_model->insert_outbound($param);
				
				$itemList = $this->api_integration_model->getItemListByAPCMDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					// $param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_outbound_item($param);
				}
			}
		}
	}
	
	public function getARCMDoc(){ // AR Credit Memo - Inbound
		$data = $this->api_integration_model->getARCMDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['po_number'] = $data[$i]['BASETYPE'];
				$param['st_staging'] = $data[$i]['BASELINE'];
				$param['code'] = isset($data[$i]['DOCNUM']) ? $data[$i]['DOCNUM'] : 'ARCM-' . time() . $i;
				$param['document_code'] = 'AR CREDIT MEMO';
				$param['id_supplier'] = $data[$i]['CARDCODE'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['user'] = 'admin';
				$param['remark'] = '';
				$param['id_warehouse'] = isset($data[$i]['WAREHOUSE']) ? $data[$i]['WAREHOUSE'] : 'SA1';
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_inbound = $this->api_integration_model->insert_inbound($param);
				
				$itemList = $this->api_integration_model->getItemListByARCMDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_inbound_item($param);
				}
			}
		}
	}
	
	/* BACKUP --
	public function getAPCMDoc(){ // AP Credit Memo - Inbound
		$data = $this->api_integration_model->getAPCMDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['po_number'] = $data[$i]['BASETYPE'];
				$param['st_staging'] = $data[$i]['BASELINE'];
				$param['code'] = isset($data[$i]['DOCNUM']) ? 'APCM-' . $data[$i]['DOCNUM'] : 'APCM-' . time() . $i;
				$param['document_code'] = 'AP CREDIT MEMO';
				$param['id_supplier'] = $data[$i]['CARDCODE'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['user'] = 'admin';
				$param['remark'] = '';
				$param['id_warehouse'] = isset($data[$i]['WAREHOUSE']) ? $data[$i]['WAREHOUSE'] : 'SA1';
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_inbound = $this->api_integration_model->insert_inbound($param);
				
				$itemList = $this->api_integration_model->getItemListByAPCMDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					// $param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_inbound_item($param);
				}
			}
		}
	}
	
	public function getARCMDoc(){ // AR Credit Memo - Outbound
		$data = $this->api_integration_model->getARCMDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['po_number'] = $data[$i]['BASETYPE'];
				$param['st_staging'] = $data[$i]['BASELINE'];
				$param['code'] = isset($data[$i]['DOCNUM']) ? 'ARCM-' . $data[$i]['DOCNUM'] : 'ARCM-' . time() . $i;
				$param['document_code'] = 'AR CREDIT MEMO';
				$param['shipping_group'] = '';
				$param['note'] = '';
				$param['customer_id'] = $data[$i]['CARDCODE'];
				$param['date'] = $data[$i]['DOCDATE'];
				$param['user'] = 'admin';
				$param['id_warehouse'] = isset($data[$i]['WAREHOUSE']) ? $data[$i]['WAREHOUSE'] : 'SA1';
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_inbound = $this->api_integration_model->insert_outbound($param);
				
				$itemList = $this->api_integration_model->getItemListByARCMDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_outbound_item($param);
				}
			}
		}
	}
	--*/
	
	public function getBinTransferDoc() {
		$data = $this->api_integration_model->getBinTransferDocList();
		
		if(count($data) > 0) {
			for($i = 0; $i < count($data); $i++) {
				// $param['id_inbound'] = $data[$i]['DOCENTRY'];
				$param['id_staging'] = $data[$i]['BASEENTRY'];
				$param['code'] = isset($data[$i]['DOCNUM']) ? $data[$i]['DOCNUM'] : 'BT-' . time() . $i;
				$param['user'] = 'admin';
				$param['id_warehouse'] = isset($data[$i]['FROM_WAREHOUSE']) ? $data[$i]['FROM_WAREHOUSE'] : 'SA1';
				$param['status_id'] = ($data[$i]['DOCSTATUS'] == 'Y' || $data[$i]['DOCSTATUS'] == 'O') ? 3 : 17;
				
				$insert_bin_transfer = $this->api_integration_model->insert_bin_transfer($param);
				$param['bin_transfer_id'] = $insert_bin_transfer;
				
				$itemList = $this->api_integration_model->getItemListByBinTransferDocCode($param);
				
				for($j = 0; $j < count($itemList); $j++) {
					$param['item_id'] = $itemList[$j]['ITEMCODE'];
					$param['qty'] = $itemList[$j]['QTY'];
					
					$insert_inbound_item = $this->api_integration_model->insert_bin_transfer_item($param);
				}
			}
		}
	}
	
	public function test_post_bt($a, $b) {
		$this->api_integration_model->post_bin_transfer($a, $b);
	}
	
	public function test_post_gr($a, $b) {
		$this->api_integration_model->post_receipt($a, $b);
	}
	
	public function test_post_gi($a, $b) {
		$this->api_integration_model->post_loading($a, $b);
	}
	
	public function repost_gr($rcv_code) {
		$this->api_integration_model->repost_gr($rcv_code);
	}
	
	public function repost_gi($picking) {
		$this->api_integration_model->repost_gi($picking);
	}
	
	public function repost_loading($picking) {
		$this->api_integration_model->repost_loading($picking);
	}
	
	public function reinsert_picking() {
		$this->api_integration_model->reinsert_picking();
	}
	
	public function getItemStock(){

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$param = array(
			'id_barang'		=> $params['item_id'],
			// 'id_satuan'		=> $this->input->post('id_satuan'),
			// 'id_qc'			=> $this->input->post('id_qc'),
			// 'id_kategori'	=> $this->input->post('id_kategori'),
			// 'loc_id'		=> $this->input->post('loc_id'),
			// 'loc_type'		=> $this->input->post('loc_type'),
      		// 'warehouse'     => $this->input->post('warehouse'),
			// 'params'		=> $this->input->post('params')
		);


		$data_getItem = $this->api_integration_model->getItemStock($param);

		$data['status']		= 200;
		$data['message']	= 'Success';
		$data['response']	= true;
		$data['results']	= $data_getItem;
		
		echo json_encode($data);
	}

	function generateRandomString($length = 18) {
		return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
	}
	
	
}

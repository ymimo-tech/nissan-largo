<?php

class menu extends MY_Controller {

    private $class_name;
    private $limit = 10;
    private $title = 'Menu Aplikasi';

    public function __construct() {
        parent::__construct();

        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        /*$this->access_right->check();
        $this->access_right->otoritas('view', true);*/

        // Global Model
        $this->load_model($this->class_name . '_model');
    }

    public function index() {
        $data['title'] = 'MENU MANAGEMENT';
        $data['subTitle'] = '<i class="fa fa-cogs"></i> List of Largo Menu';
        $data['page'] = $this->class_name . '/index';

        $this->include_required_file($data);
        $data['jsPage'][] = "assets/pages/scripts/lr-menu.js";

        $this->include_other_view_data($data);
        $this->access_right->activity_logs('view','Daftar Menu');
        $this->load->view('template', $data);
    }

    public function index2() {
        // Activity log
        $this->access_right->activity_logs('view', $this->title);

        $data['title'] = '<i class="icon-list"></i> ' . $this->title;
        $data['page'] = $this->class_name . '/index';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-barang.js";

        $data['button_group'] = array();
        if ($this->access_right->otoritas('add')) {
            $data['button_group'] = array(
                anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
            );
        }
        $this->load_model('menu_group_model');
        $this->load_model('menu_model');

        $data['menu_group'] = $this->menu_group_model->get_data();
        $data['menu'] = $this->menu_model->parsing_data();
        $data['content_page'] = $this->class_name . '/table';
        $this->load->view('template', $data);
    }

    private function include_other_view_data(&$data) {
        $data['menu_group'] = $this->menu_model->create_options("hr_grup_menu", "kd_grup_menu", "nama_grup_menu", "-- Select grup menu --");
    }

    private function include_required_file(&$data){
        $data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
    }

    public function get_list(){
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $menu = $this->menu_model->get_all();
        $iTotalRecords = $this->menu_model->get_all()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($menu->result() as $value){
            $records["data"][] = array(
                $_REQUEST['start']+$i,
                $value->nama_menu,
                $value->nama_grup_menu,"",
                $value->url, "",
                $action = $this->buttons->actions(array('edit'=> $value->kd_menu, 'delete' => $value->kd_menu))
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $menu = $this->menu_model->get_by_id($id)->row_array();
            $process_result = $menu;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function delete() {
        $process_result = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            if ($this->menu_model->delete($id)) {
                $this->access_right->activity_logs('delete','Daftar Menu');
                $process_result['success'] = 1;
            }
        } else {
            $process_result['no_delete'] = 1;
        }
        echo json_encode($process_result);
    }

    public function proses() {
        $process_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post('id');
            $data = array(
                'kd_grup_menu' => $this->input->post('kd_grup_menu'),
                'nama_menu' => $this->input->post('nama_menu'),
                'deskripsi' => $this->input->post('deskripsi'),
                'url' => $this->input->post('url'),
                'kd_parent' => $this->input->post('kd_parent')
            );

            /*
             * If : $id == '', lakukan proses create data
             * Else : lakukan proses update
             */
             
            if ($id == '') {
                if ($this->menu_model->create($data)) {
                    $this->access_right->activity_logs('add','Daftar Menu');
                    $process_result['success'] = 1;
                } else {
                    $process_result['success'] = $this->db->_error_message();
                }
            } else {
                if ($this->menu_model->update($data, $id)) {
                    $this->access_right->activity_logs('edit','Daftar Menu');
                    $process_result['success'] = 1;
                } else {
                    $process_result['success'] = 0;
                }
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $process_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $process_result['no_edit'] = 1;
            }
        }

        echo json_encode($process_result);
    }

    public function proses_delete($id) {
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->menu_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'laod_table_menu()');
            }
            echo json_encode($message);
        }
    }

    public function get_parent() {
        $kode = $this->input->post('kode');
        $selected = $this->input->post('selected');

        $options = array('' => 'None');

        if (!empty($kode)) {
            //$data = $this->menu_model->get_data(array('kd_grup_menu' => $kode, 'kd_parent' => null));
            $data = $this->menu_model->get_data(array('a.kd_grup_menu' => $kode));
            foreach ($data->result() as $value) {
                $options[$value->kd_menu] = $value->nama_menu;
            }
        }

        echo hgenerator::array_to_option($options, $selected);
    }

}

?>

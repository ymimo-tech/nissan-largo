<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class dashboard extends MY_Controller {

    private $class_name;
    private $limit = 50;
    private $offset = 0;
    public function __construct() {

        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();

        /* Otoritas */
        //$this->access_right->check();
        //$this->access_right->otoritas('view', true);

        $this->load_model($this->class_name.'_model');

    }


    public function  v2() {

		    $this->load->library('mimoclientprint');

        if($this->session->userdata('st_toko'))
            redirect('receiving_store');

		    $data['title'] = '<i class="icon-plus-sign-alt"></i> Dashboard';
        $data['page'] = $this->class_name . '/index';
        $data['data_source'] = base_url($this->class_name . '/load');

        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
    		$data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
    		$data['jsPage'][] = 'assets/global/plugins/amcharts/amcharts/amcharts.js';
        $data['jsPage'][] = 'assets/global/plugins/amcharts/amcharts/serial.js';
        $data['jsPage'][] = 'assets/global/plugins/amcharts/amcharts/pie.js';
        $data['jsPage'][] = 'assets/global/plugins/amcharts/amcharts/themes/light.js';
        $data['jsPage'][] = 'assets/global/plugins/canvasjs/canvasjs.js';
    		$data['jsPage'][] = 'assets/custom/js/share.js';
    		$data['jsPage'][] = 'assets/pages/scripts/dashboard/dashboard-handler.js';

        /* Get Data Select*/
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options('-- ALL WAREHOUSE --', NULL); // Get Data Warehoues

		/*
        $data['open_po'] = 5;//$this->dashboard_model->get_open_po();
        $data['open_do'] = 7;//$this->dashboard_model->get_open_do();
        $data['total_stok'] = $this->dashboard_model->get_total_stok();
        $data['graph_inbound'] = $this->dashboard_model->get_graph_inbound();
        $data['graph_outbound'] = $this->dashboard_model->get_graph_outbound();
        $data['total_inbound'] = $this->dashboard_model->get_total_inbound();
        $data['total_outbound'] = $this->dashboard_model->get_total_outbond();
		*/

        //========== Cek SN dengan loc kosong, set jadi 100=============
        $this->dashboard_model->cek_null_loc();

        /* INSERT LOG */
        $this->access_right->activity_logs('view','Dashboard');
        /* END OF INSERT LOG */


		//$this->load->view('template_dashboard', $data);
        $this->load->view('template', $data);
    }

    public function index(){

        $this->load->library('mimoclientprint');

        if($this->session->userdata('st_toko'))
            redirect('receiving_store');

        $data['title'] = '<i class="icon-plus-sign-alt"></i> Dashboard';
        $data['page'] = $this->class_name . '/index-new';
        $data['data_source'] = base_url($this->class_name . '/load');

        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/amcharts/amcharts/amcharts.js';
        $data['jsPage'][] = 'assets/global/plugins/amcharts/amcharts/serial.js';
        $data['jsPage'][] = 'assets/global/plugins/amcharts/amcharts/pie.js';
        $data['jsPage'][] = 'assets/global/plugins/amcharts/amcharts/themes/light.js';
        $data['jsPage'][] = 'assets/global/plugins/canvasjs/canvasjs.js';
        $data['jsPage'][] = 'assets/custom/js/share.js';
        $data['jsPage'][] = 'assets/pages/scripts/dashboard/dashboard-handler-new.js';

        /* Get Data Select*/
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options('-- ALL WAREHOUSE --', NULL); // Get Data Warehoues

        /* INSERT LOG */
        $this->access_right->activity_logs('view','Dashboard');
        /* END OF INSERT LOG */


        //$this->load->view('template_dashboard', $data);

        $this->load->view('template', $data);

        require_once(APPPATH.'controllers/quarantine.php');
        $quarantine = new Quarantine();
        $quarantine->QuarantineDetailUpdate();
    }

	public function getReplenishment(){
		$result = array();

        $params = array(
            'warehouse' => $this->input->post('warehouse')
        );
		$result = $this->dashboard_model->getReplenishment($params);

		echo json_encode($result);
	}

	public function getInboundStatus(){
		$result = array();
		$this->load_model('receiving_model');

		$params = array(
            'warehouse' => $this->input->post('warehouse')
        );
		$result = $this->dashboard_model->getInboundStatus($params);

		echo json_encode($result);
	}

	public function getOutboundStatus(){
		$result = array();

		$params = array();
		$result = $this->dashboard_model->getOutboundStatus($params);

		echo json_encode($result);
	}

	public function getTopReceived(){
		$result = array();

		$post = trim($this->input->post('periode'));
		$post = explode(' - ', $post);

		$start = str_replace('/','-',trim($post[0]));
		$end = str_replace('/','-',trim($post[1]));

		$params = array(
			'start'	=> date('Y-m-d', strtotime($start)),
			'end'	=> date('Y-m-d', strtotime($end)),
      'warehouse' => $this->input->post('warehouse')
		);

		$result = $this->dashboard_model->getTopReceived($params);

		echo json_encode($result);
	}

	public function getTopOrdered(){
		$result = array();

		$post = trim($this->input->post('periode'));
		$post = explode(' - ', $post);

		$start = str_replace('/','-',trim($post[0]));
		$end = str_replace('/','-',trim($post[1]));

		$params = array(
			'start'	=> date('Y-m-d', strtotime($start)),
			'end'	=> date('Y-m-d', strtotime($end))
		);

		$result = $this->dashboard_model->getTopOrdered($params);

		echo json_encode($result);
	}

	public function getStatusStock(){
		$result = array();

		$params = array(
            'warehouse' => $this->input->post('warehouse')
        );
		$result = $this->dashboard_model->getStatusStock($params);

		echo json_encode($result);
	}

	public function getActiveStock(){
		$result = array();

		$params = array(
                'warehouse' => $this->input->post('warehouse')
              );
		$result = $this->dashboard_model->getActiveStock($params);

		echo json_encode($result);
	}

	public function getLoadingChart(){
		$result = array();

		$post = trim($this->input->post('periode'));
		$post = explode(' - ', $post);

		$start = str_replace('/','-',trim($post[0]));
		$end = str_replace('/','-',trim($post[1]));

		$params = array(
			'start'	=> date('Y-m-d', strtotime($start)),
			'end'	=> date('Y-m-d', strtotime($end))
		);

		$result = $this->dashboard_model->getLoadingChart($params);

		echo json_encode($result);
	}

	public function getInboundChart(){
		$result = array();

		$post = trim($this->input->post('periode'));
		$post = explode(' - ', $post);

		$start = str_replace('/','-',trim($post[0]));
		$end = str_replace('/','-',trim($post[1]));

		$params = array(
			'start'	=> date('Y-m-d', strtotime($start)),
			'end'	=> date('Y-m-d', strtotime($end)),
            'warehouse' => $this->input->post('warehouse')
		);

		$result = $this->dashboard_model->getInboundChart($params);

		echo json_encode($result);
	}

	public function getWidget(){
		$result = array();

		$post = trim($this->input->post('periode'));
		$post = explode(' - ', $post);

		$start = str_replace('/','-',trim($post[0]));
		$end = str_replace('/','-',trim($post[1]));

		$params = array(
			'start'	=> date('Y-m-d', strtotime($start)),
			'end'	=> date('Y-m-d', strtotime($end)),
            'warehouse' => $this->input->post('warehouse')
		);

		$inbound = $this->dashboard_model->getInbound($params);
		$receiving = $this->dashboard_model->getReceiving($params);
		$outbound = $this->dashboard_model->getOutbound($params);
		$picking = $this->dashboard_model->getPicking($params);

		$result = array(
			'inbound'	=> $inbound,
			'receiving'	=> $receiving,
			'outbound'	=> $outbound,
			'picking'	=> $picking
		);

		echo json_encode($result);
	}

    public function data_graph_inbound(){
        $hasil = $this->dashboard_model->get_graph_inbound();
        foreach($hasil->result() as $row){
            $data[] = array($row->bulan,$row->qty);
        }
        $out = array_values($data);
        echo json_encode($out);
    }

    public function data_graph_outbound(){
        $hasil = $this->dashboard_model->get_graph_outbound();
        foreach($hasil->result() as $row){
            $data[] = array($row->bulan,$row->qty);
        }
        $out = array_values($data);
        echo json_encode($out);
    }

    public function change_password($data = '') {
        $this->load_model('user_model');
        if($this->session->userdata('st_toko')){
            $data['data_user'] = $this->user_model->get_user_toko();
            $data['title']="Ganti Username / Password";
            $data['page'] = 'user/edit_user_toko';
        }else{
            $kd_roles = $this->session->userdata('kd_roles');
            $nik = $this->session->userdata('nik_session');
            $q = $this->db->query("select user_id from hr_user where nik='$nik'");
            foreach ($q->result_array() as $row) {
                $userid = $row['user_id'];
            }
            $data['data_user'] = $this->user_model->data_user_mdl($userid);
            $data['title'] = "Ganti Username / Password";
            $data['page'] = 'user/edit_user';
        }
        $this->load->view('template', $data);
    }

    function check_old_pass() {
        $this->load_model('user_model');
        return $this->user_model->check_old_pass();
    }

    function update_user() {
        $this->load_model('user_model');
        $nik = $this->input->post('txtnik');
        $username_lama = $this->input->post('txtulama');
        $password_lama = $this->input->post('txtplama');
        $username_baru = $this->input->post('txtubaru');
        $password_baru = $this->input->post('txtpbaru');
        $chek_old_pass = $this->check_old_pass();
        if ($chek_old_pass == '0') {
            $data['status'] = "error";
            $data['message'] = "Password Salah / Tidak Boleh Kosong";
            $this->change_password($data);
        } else if ($username_baru == '') {
            $data['status'] = "error";
            $data['message'] = "Username Baru Tidak Boleh Kosong";
            $this->change_password($data);
            //$this->user_model->update_user();
        } else if ($password_baru == '') {
            $data['status'] = "error";
            $data['message'] = "Password Baru Tidak Boleh Kosong";
            $this->change_password($data);
            //$this->user_model->update_user();
        } else {
            $this->user_model->update_user();
            $data['status'] = "success";
            $data['message'] = "Sukses Update Data Password";
            $this->change_password($data);
        }
    }

    function update_user_toko() {
        $this->load_model('user_model');
        $nik = $this->input->post('txtnik');
        $username_lama = $this->input->post('txtulama');
        $password_lama = $this->input->post('txtplama');
        $username_baru = $this->input->post('txtubaru');
        $password_baru = $this->input->post('txtpbaru');
        $chek_old_pass = $this->check_old_pass();
        if ($chek_old_pass == '0') {
            $data['status'] = "error";
            $data['message'] = "Password Salah / Tidak Boleh Kosong";
            $this->change_password($data);
        } else if ($username_baru == '') {
            $data['status'] = "error";
            $data['message'] = "Username Baru Tidak Boleh Kosong";
            $this->change_password($data);
            //$this->user_model->update_user();
        } else if ($password_baru == '') {
            $data['status'] = "error";
            $data['message'] = "Password Baru Tidak Boleh Kosong";
            $this->change_password($data);
            //$this->user_model->update_user();
        } else {
            $this->user_model->update_user();
            $data['status'] = "success";
            $data['message'] = "Sukses Update Data Password";
            $this->change_password($data);
        }
    }

    function _curl_process($type='',$table='',$data=array()){
        $curl_handle = curl_init ();

		//supported format ['json','csv','jsonp','serialized','xml']
		//change url suffix ex : .json to .xml for change response format
        $clientid='test';
        $local_ip='127.0.0.1';
        $staging_ip='127.0.0.1';
        /*$data2 = array (
                'table' => $table,
				'clientid' => $clientid,
				'hash' => sha1 ( $table . $clientid . $local_ip ),
				'xkey' => '0b5581b7ec815ad66a916b4fc5716d2ec84cd45e',
		);*/

        $data2 = array (
                'table' => $table,
                'clientid' => $clientid,
                'hash' => sha1 ( $table . $clientid  ),
                'xkey' => '0b5581b7ec815ad66a916b4fc5716d2ec84cd45e',
        );

        $data = array_merge($data,$data2);
		curl_setopt ( $curl_handle, CURLOPT_URL, 'http://'.$staging_ip.'/prod/largo_api/api/'.$type.'.json' );
		curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $curl_handle, CURLOPT_POST, 1 );
		curl_setopt ( $curl_handle, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
		curl_setopt ( $curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST );
		curl_setopt ( $curl_handle, CURLOPT_USERPWD, 'test:123456' );

		$buffer = curl_exec ( $curl_handle );
		curl_close ( $curl_handle );

		$data = json_decode($buffer);
        return $data;
    }


    public function get_data() {
        $curl_handle = curl_init ();

        //supported format ['json','csv','jsonp','serialized','xml']
        //change url suffix ex : .json to .xml for change response format
        curl_setopt ( $curl_handle, CURLOPT_URL, 'http://192.168.115.222/largo_api/api/get.json' );

        curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $curl_handle, CURLOPT_POST, 1 );
        curl_setopt ( $curl_handle, CURLOPT_POSTFIELDS, http_build_query ( array (
                'table' => 'f_t_inbound_l2',
                'where' => array('SyncStatus'=>'N'),
                'clientid' => 'test',
                'hash' => sha1 ( 'f_t_inbound_l2' . 'test' . '' ),
                'xkey' => '0b5581b7ec815ad66a916b4fc5716d2ec84cd45e',
        ) ) );
        curl_setopt ( $curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST );
        curl_setopt ( $curl_handle, CURLOPT_USERPWD, 'test:123456' );

        $buffer = curl_exec ( $curl_handle );
        curl_close ( $curl_handle );

        echo $buffer;
    }

    function sync_all(){
        $new_otb=array();
        $new_itb=array();
        $new_itm=array();
        $new_supp=array();

        $this->load_model('api_putaway_model');
        $this->api_putaway_model->send_staging();

        $this->access_right->check(array('inbound','outbound','referensi_barang','referensi_supplier'));
        if ($this->access_right->otoritas('view',false,'inbound')) {
            $this->load_model(array('inbound_model'));
            $this->db->trans_start();
            //$new_inbounds =  $this->inbound_model->get_new_inbound();
            $new_inbounds = $this->_curl_process('get','f_t_inbound_h',array('where'=>array('SyncStatus'=>'N')));
            if($new_inbounds->status=='001'){
                foreach($new_inbounds->data as $inbound){
                    $new=0;
                    $supplier =  $this->inbound_model->get_supplier($inbound->Supplier);
                    $id_supplier = $supplier->num_rows()?$supplier->row()->id_supplier:0;
                    $doc =  $this->inbound_model->get_inbound_document($inbound->InbType);
                    $id_doc = $doc->num_rows()?$doc->row()->id_inbound_document:0;

                    $data_inbound=array(
                        'kd_inbound'=>$inbound->InbNo,
                        'tanggal_inbound'=>$inbound->DocDate,
    					          'BaseCreatedDate' => $inbound->BaseCreatedDate,
                        'id_supplier'=>$id_supplier,
                        'id_inbound_document'=>$id_doc,
                        'id_status_inbound'=>1,
                        'id_staging'=>$inbound->DocEntry,
    					          'WhsFrom'=>$inbound->WhsFrom,
                    );

                    $exist = $this->inbound_model->check_inbound($inbound->InbNo);
                    if($exist->num_rows()>0){
                        $new_inbound_id = $exist->row()->id_inbound;
                        $this->inbound_model->update_inbound($data_inbound,$new_inbound_id);
                    }else{
                        $new_inbound_id = $this->inbound_model->create_inbound($data_inbound);
                        $new=1;
                    }
                    // $inbound_items = $this->inbound_model->get_inbound_item($inbound->DocEntry);
                    $inbound_items = $this->_curl_process('get','f_t_inbound_l1',array('where'=>array('DocEntry'=>$inbound->DocEntry)));
                    $this->load_model('referensi_barang_model');
                    if($inbound_items->status=='001'){
                        foreach($inbound_items->data as $item){
                            $barang =  $this->inbound_model->get_barang($item->SKU);
                            //$id_barang = $barang->num_rows()?$barang->row()->id_barang:0;
                            if($barang->num_rows()>0){
                                $id_barang = $barang->row()->id_barang;
                            }else{
                                $data_barang_insert = array(
                                    'kd_barang' => $item->SKU );
                                $id_barang = $this->referensi_barang_model->create_barang($data_barang_insert);
                            }
                            $data_item = array(
                                'id_inbound'=>$new_inbound_id,
                                'id_barang'=>$id_barang,
                                'jumlah_barang'=>$item->DocQty,
                                'id_staging'=>$item->LineNum,
                                'FinaCode' => $item->FinaCode
                            );

                            $exist_item = $this->inbound_model->check_inbound_barang(array('id_staging'=>$item->LineNum,'id_inbound'=>$new_inbound_id));
                            if($exist_item->num_rows()>0){
                                $new_inbound_barang_id = $exist_item->row()->id_inbound_barang;
                                $this->inbound_model->update_inbound_barang($data_item,$new_inbound_barang_id);
                            }else{
                                $new_inbound_barang_id = $this->inbound_model->create_inbound_barang($data_item);
                            }
                        }
                    }
                    $SyncDate=date('Y-m-d H:i:s');
                    //$this->inbound_model->set_inbound_status($inbound->DocEntry,$SyncDate);
                    $this->_curl_process('update','f_t_inbound_h',array(
                        'where'=>array('DocEntry'=>$inbound->DocEntry),
                        'data'=>array(
                            'SyncStatus'=>'Y',
                            'SyncDate'=>$SyncDate,
                            'InbStatus'=>'INPROCESS'
                        )
                    ));
                    $new_itb[]=array('nomor'=>$inbound->InbNo,'id'=>$new_inbound_id,'newrec'=>$new);
                }
            }
            $this->db->trans_complete();
        }

        if ($this->access_right->otoritas('view',false,'outbound')) {
            $this->load_model(array('outbound_model'));
            $this->db->trans_start();
            // $new_outbounds =  $this->outbound_model->get_new_outbound();
            $new_outbounds = $this->_curl_process('get','f_t_outbound_h',array('where'=>array('SyncStatus'=>'N')));
            if($new_outbounds->status=='001'){
                foreach($new_outbounds->data as $outbound){
                    $new=0;
                    $doc =  $this->outbound_model->get_outbound_document($outbound->OutbondType);
                    $id_doc = $doc->num_rows()?$doc->row()->id_outbound_document:0;
                    $data_outbound=array(
                        'kd_outbound'=>$outbound->OutbondNo,
                        'tanggal_outbound'=>$outbound->DocDate,
                        'id_outbound_document'=>$id_doc,
                        'id_status_outbound'=>1,
                        'id_staging'=>$outbound->DocEntry,
                        'DestinationName'=>$outbound->DestinationName?$outbound->DestinationName:'',
                        'DestinationAddressL1'=>$outbound->DestinationAddressL1?$outbound->DestinationAddressL1:'',
                        'DestinationAddressL2'=>$outbound->DestinationAddressL2?$outbound->DestinationAddressL2:'',
                        'DestinationAddressL3'=>$outbound->DestinationAddressL3?$outbound->DestinationAddressL3:'',
                        'Phone'=>$outbound->PhoneNo?$outbound->PhoneNo:'',
                        'ShippingNote'=>$outbound->ShippingNote?$outbound->ShippingNote:'',
                    );
                    $exist = $this->outbound_model->check_outbound($outbound->OutbondNo);
                    if($exist->num_rows()>0){
                        $new_outbound_id = $exist->row()->id_outbound;
                        $this->outbound_model->update_outbound($data_outbound,$new_outbound_id);
                    }else{
                        $new_outbound_id = $this->outbound_model->create_outbound($data_outbound);
                        $new=1;
                    }
                    // $outbound_items = $this->outbound_model->get_outbound_item($outbound->DocEntry);
                    $outbound_items = $this->_curl_process('get','f_t_outbound_l1',array('where'=>array('DocEntry'=>$outbound->DocEntry)));
                    if($outbound_items->status=='001'){
                        foreach($outbound_items->data as $item){
                            $barang =  $this->outbound_model->get_barang($item->SKU);
                            $id_barang = $barang->num_rows()?$barang->row()->id_barang:0;
                            $data_item = array(
                                'id_outbound'=>$new_outbound_id,
                                'id_barang'=>$id_barang,
                                'jumlah_barang'=>$item->DocQty,
                                'id_staging'=>$item->LineNum
                            );
                            $exist_item = $this->outbound_model->check_outbound_barang(array('id_staging'=>$item->LineNum,'id_outbound'=>$new_outbound_id));
                            if($exist_item->num_rows()>0){
                                $new_outbound_barang_id = $exist_item->row()->id;
                                $this->outbound_model->update_outbound_barang($data_item,$new_outbound_barang_id);
                            }else{
                                $new_outbound_barang_id = $this->outbound_model->create_outbound_barang($data_item);
                            }
                        }
                    }
                    $SyncDate=date('Y-m-d H:i:s');
                    // $this->outbound_model->set_outbound_status($outbound->DocEntry,$SyncDate);
                    $this->_curl_process('update','f_t_outbound_h',array(
                        'where'=>array('DocEntry'=>$outbound->DocEntry),
                        'data'=>array(
                            'SyncStatus'=>'Y',
                            'SyncDate'=>$SyncDate
                        )
                    ));
                    $new_otb[]=array('nomor'=>$outbound->OutbondNo,'id'=>$new_outbound_id,'newrec'=>$new);
                }
            }
            $this->db->trans_complete();
        }

        if ($this->access_right->otoritas('view',false,'referensi_barang')) {
            $this->load_model(array('referensi_barang_model'));
            $this->db->trans_start();
            // $new_barangs =  $this->referensi_barang_model->get_new_barang();
            $new_barangs = $this->_curl_process('get','m_t_item',array('where'=>array('SyncStatus'=>'N')));
            if($new_barangs->status=='001'){
                foreach($new_barangs->data as $barang){
                    $new = 0;
                    $id_category = $this->referensi_barang_model->get_barang_category($barang->Category);
                    $id_satuan = $this->referensi_barang_model->get_barang_satuan($barang->UomCode);
                    $data_barang=array(
                        'kd_barang'=>$barang->SKU,
                        'nama_barang'=>$barang->ItemName,
                        'detail_barang'=>$barang->ItemDesc,
                        'id_kategori'=>$id_category,
                        'has_expdate'=>$barang->HasExpDate,
                        'shipment_type'=>$barang->ShipmentType,
                        'fifo_period'=>$barang->FifoPeriod,
                        'def_qty'=>$barang->DefQty,
                        'id_satuan'=>$id_satuan,
                        'minimum_stock'=>$barang->MinStok
                    );
                    $exist = $this->referensi_barang_model->check_barang($barang->SKU);
                    if($exist->num_rows()>0){
                        $new_barang_id = $exist->row()->id_barang;
                        $this->referensi_barang_model->update_barang($data_barang,$new_barang_id);
                    }else{
                        $new_barang_id = $this->referensi_barang_model->create_barang($data_barang);
                        $new=1;
                    }
                    $SyncDate=date('Y-m-d H:i:s');
                    // $this->referensi_barang_model->set_barang_status($barang->SKU,$SyncDate);
                    $this->_curl_process('update','m_t_item',array(
                        'where'=>array('SKU'=>$barang->SKU),
                        'data'=>array(
                            'SyncStatus'=>'Y',
                            'SyncDate'=>$SyncDate
                        )
                    ));
                    $new_itm[]=array('sku'=>$barang->SKU,'nama'=>$barang->ItemName,'id'=>$new_barang_id,'newrec'=>$new);
                }
            }
            $this->db->trans_complete();
        }

        if ($this->access_right->otoritas('view',false,'referensi_supplier')) {
            $this->load_model(array('referensi_supplier_model'));
            $this->db->trans_start();
            // $new_suppliers =  $this->referensi_supplier_model->get_new_supplier();
            // $new=array();
            $new_suppliers = $this->_curl_process('get','m_t_supplier',array('where'=>array('SyncStatus'=>'N')));
            if($new_suppliers->status=='001'){
                foreach($new_suppliers->data as $supplier){
                    $new = 0;
                    $data_supplier=array(
                        'kd_supplier'=>$supplier->SupplierCode,
                        'nama_supplier'=>$supplier->SupplierName,
                        'alamat_supplier'=>$supplier->AddressL1.' '.$supplier->AddressL2.' '.$supplier->AddressL3,
                        'telepon_supplier'=>$supplier->Phone,
                        'cp_supplier'=>$supplier->ContactPerson,
                        'email_supplier'=>''
                    );
                    $exist = $this->referensi_supplier_model->check_supplier($supplier->SupplierCode);
                    if($exist->num_rows()>0){
                        $new_supplier_id = $exist->row()->id_supplier;
                        $this->referensi_supplier_model->update_supplier($data_supplier,$new_supplier_id);
                    }else{
                        $new_supplier_id = $this->referensi_supplier_model->create_supplier($data_supplier);
                        $new=1;
                    }
                    $SyncDate=date('Y-m-d H:i:s');
                    // $this->referensi_supplier_model->set_supplier_status($supplier->SupplierCode,$SyncDate);
                    $this->_curl_process('update','m_t_supplier',array(
                        'where'=>array('SupplierCode'=>$supplier->SupplierCode),
                        'data'=>array(
                            'SyncStatus'=>'Y',
                            'SyncDate'=>$SyncDate
                        )
                    ));
                    $new_supp[]=array('kd_supplier'=>$supplier->SupplierCode,'nama'=>$supplier->SupplierName,'id'=>$new_supplier_id,'newrec'=>$new);
                }
            }
            $this->db->trans_complete();
        }

        return array('inbound'=>$new_itb,'outbound'=>$new_otb,'item'=>$new_itm,'supplier'=>$new_supp);
    }

    function longpolling(){
	    date_default_timezone_set("Asia/Jakarta");
	    header('Cache-Control: no-cache');
	    header("Content-Type: text/event-stream\n\n");
	    while (1) {
            //sleep first, sync after 30s on page to prevent heavy load on open page
            sleep(250);
            $new = $this->sync_all();
	        $output = array('newc'=>$new);
	        echo "\nevent: sync\n";
	        echo 'data: '.json_encode($output);
	        echo "\n\n";
	        ob_flush();
	        flush();
            session_write_close();
	    }
	}

    function manual_sync(){
        date_default_timezone_set("Asia/Jakarta");
        $new = $this->sync_all();
        $json = array('newc'=>$new);
        echo json_encode($json);
    }

    public function getSKUWidget(){

    	$params = array(
    		"warehouse" => $this->input->post('warehouse')
    	);

    	$data = array(
    		"active_SKU"	=> $this->dashboard_model->getActiveSKU($params),
    		"hold_SKU"		=> $this->dashboard_model->getHoldSKU($params),
    		"qc_SKU"		=> $this->dashboard_model->getQcSKU($params)
    	);

    	echo json_encode($data);

    }

    public function getWarehouseCapacity(){

    	$params = array(
    		"warehouse" => $this->input->post('warehouse')
    	);

    	$data = array(
    		"bin_locations"			=> $this->dashboard_model->getBinLocations($params),
    		"filled_bin_location"	=> $this->dashboard_model->getFilledBinLocations($params),
    		"general_storage"	    => $this->dashboard_model->getBinStorageByCategory('GN'),
    		"cold_room_storage"		=> $this->dashboard_model->getBinStorageByCategory('CR'),
    		"high_value_storage"	=> $this->dashboard_model->getBinStorageByCategory('HV'),
    		"damage_item_storage"	=> $this->dashboard_model->getBinStorageByType('NG')
    	);

    	echo json_encode($data);

    }

    public function getItemStatus(){

    	$params = array(
    		"warehouse" => $this->input->post('warehouse')
    	);

    	$data = array(
    		"replenishment" 	=> $this->dashboard_model->getReplenishmentSKU($params),
    		"expired"			=> $this->dashboard_model->getExpiredSKU($params),
        "non_moving"        => 0, // definisi non-moving - sku yg tdk pernah ada transaksi dalam 6 bulan terakhir
    		// "non_moving"		=> $this->dashboard_model->getNonMovingSKU($params),
    		"shelf_life"		=> $this->dashboard_model->getShelfLifeSKU($params)
    	);

    	echo json_encode($data);

    }

    public function getInbound(){

    	$params = array(
    		"warehouse" => $this->input->post('warehouse')
    	);

    	$data = array(
    		//"inbound_list"			=> $this->dashboard_model->getInboundTopList($params),
        "inbound_list"			=> $this->dashboard_model->getReceivingInProgress($params),
        "new_inbound"			=> $this->dashboard_model->getNewInboundStatus($params),
    		"open_inbound"			=> $this->dashboard_model->getOpenInboundStatus($params),
    		"on_progress_inbound"	=> $this->dashboard_model->getOnProgressInboundStatus($params),
    		"complete_inbound"		=> $this->dashboard_model->getCompleteInboundStatus($params)
    	);

    	echo json_encode($data);

    }

    public function getChartInbound(){

        $params = array(
            "warehouse" => $this->input->post('warehouse')
        );

        $data = array(
            "inbound_chart"  => $this->dashboard_model->getInboundChartTime($params),
            "inbound_processing_time"=> $this->dashboard_model->getInboundTimeAverage($params)
        );

        echo json_encode($data);

    }

    public function getOutbound(){

    	$params = array(
    		"warehouse" => $this->input->post('warehouse')
    	);

    	$data = array(
    		//"outbound_list"			=> $this->dashboard_model->getOutboundTopList($params),
        "outbound_list"			=> $this->dashboard_model->getPickingInProgress($params),
    		"new_outbound"			=> $this->dashboard_model->getNewOutboundStatus($params),
    		"open_outbound"			=> $this->dashboard_model->getOpenOutboundStatus($params),
    		"on_progress_outbound"	=> $this->dashboard_model->getOnProgressOutboundStatus($params),
    		"complete_outbound"		=> $this->dashboard_model->getCompleteOutboundStatus($params)
    	);
        

    	echo json_encode($data);

    }

    public function getChartOutbound(){

        $params = array(
            "warehouse" => $this->input->post('warehouse')
        );

        $data = array(
            "outbound_chart"  => $this->dashboard_model->getOutboundChartTime($params),
            "outbound_processing_time"=> $this->dashboard_model->getOutboundTimeAverage($params)
        );

        echo json_encode($data);

    }

    public function getCycleCount(){

    	$params = array(
    		"warehouse" => $this->input->post('warehouse')
    	);

    	$data = array(
    		"last_cycle_count"	=> 0,
    		"sku_checked"		=> 0,
    		"accuracy_leve"		=> 0,
    		"error_rate"		=> 0
    	);

    	echo json_encode($data);

    }

    public function getQCStatus(){

        $params = array(
            "warehouse" => $this->input->post('warehouse')
        );

        $qc = $this->dashboard_model->getQCItem($params);

        $data = array(
            "good"          => ($qc['good_item']/$qc['total_item']) * 100,
            "on_process"    => ($qc['qc_item']/$qc['total_item']) * 100,
            "damage"        => ($qc['damage_item']/$qc['total_item']) * 100
        );

        echo json_encode($data);

    }

}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

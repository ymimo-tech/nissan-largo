<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_putaway extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_putaway_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function checkserial()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_unik']		= $this->input->post('serial_number');
				$params['time']			= $this->input->post('time');
				$params['user_name']	= $this->input->post('user');

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_unik']		= $this->input->post('serial_number');
				$params['time']			= $this->input->post('time');
				$params['user_name']	= $this->input->post('user');
				$cek_lp = $this->api_putaway_model->get_lp($params['kd_unik'])->result_array();
				$serialized = $this->api_putaway_model->get($params['kd_unik'])->row_array();
				$check_routing = $this->api_putaway_model->check_routing($params['kd_unik']);
				if($check_routing == TRUE){
							if($serialized){
								// if(($serialized['loc_id'] == 100||$serialized['loc_id'] == 103||$serialized['loc_id'] == 107) && $serialized['st_receiving'] == 1){
								if(($serialized['loc_id'] == 100||$serialized['loc_id'] == 103||$serialized['loc_id'] == 107)){
									$data['param']			= $params['kd_unik'];
									$data['status']			= 200;
									$data['message']		= $params['kd_unik'] . ' is available.';
									$data['response']		= true;
									$data['results']		= $this->api_putaway_model->retrieve($params['kd_unik'])->result_array();
									$data['last_location']		= $this->api_putaway_model->get_last_location($data['results'][0]['kd_barang']);
									$params['loc_id_old']	= $serialized['loc_id'];
									// $this->api_putaway_model->set($params);
									$this->api_putaway_model->start($serialized);
								} else{
									if($serialized['st_receiving'] != 1 ){
										$data['param']		= $params['kd_unik'];
										$data['status']		= 200;
										$data['message']	= $params['kd_unik'] . ' in Tally Process .';
										$data['response']	= false;
									}else if( $serialized['loc_id'] != 100 && $serialized['loc_id'] != 103 && $serialized['loc_id'] != 107){
										$data['param']		= $params['kd_unik'];
										$data['status']		= 200;
										$data['message']	= $params['kd_unik'] . ' has been putaway.';
										$data['response']	= false;
									}else{
										$data['param']		= $params['kd_unik'];
										$data['status']		= 200;
										$data['message']	= $params['kd_unik'] . ' is not available.';
										$data['response']	= false;
									}
								}
							}elseif($cek_lp){
								foreach ($cek_lp as $lp) {
									if(($lp['loc_id'] == 100||$lp['loc_id'] == 103||$lp['loc_id'] == 107) && $lp['st_receiving'] == 1){
										$data['param']			= $params['kd_unik'];
										$data['status']			= 200;
										$data['message']		= $params['kd_unik'] . ' is available.';
										$data['response']		= true;
										$data['results']		= $this->api_putaway_model->retrieve_lp($params['kd_unik'])->result_array();
										$params['loc_id_old']	= $lp['loc_id'];
										// $this->api_putaway_model->set_lp($params);
										$this->api_putaway_model->start_lp($lp);
										break;
									}else{

										if($lp['st_receiving'] != 1 ){
											$data['param']		= $params['kd_unik'];
											$data['status']		= 200;
											$data['message']	= $params['kd_unik'] . ' in Tally Process .';
											$data['response']	= false;
										}else if( ($lp['loc_id'] != 100 && $lp['loc_id'] != 103 && $lp['loc_id'] != 107)){
											$data['param']		= $params['kd_unik'];
											$data['status']		= 200;
											$data['message']	= $params['kd_unik'] . ' has been putaway.';
											$data['response']	= false;
										}else{
											$data['param']		= $params['kd_unik'];
											$data['status']		= 200;
											$data['message']	= $params['kd_unik'] . ' is not available.';
											$data['response']	= false;
										}
									}
								}
							}else{
								$data['param']		= $params['kd_unik'];
								$data['status']		= 401;
								$data['message']	= $params['kd_unik'] . ' is not available.';
								$data['response']	= false;
							}
					}else{
						$data['param']		= $params['kd_unik'];
						$data['status']		= 401;
						$data['message']	= $params['kd_unik'] . ' Must Be Routing Before.';
						$data['response']	= false;
					}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_location($location = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $location == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$location=urldecode($location);

			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_putaway_model->get_location($location)->row_array();
				if($result){
					$data['param']			= $location;
					$data['status']			= 200;
					$data['message']		= $location . ' is available.';
					$data['response']		= true;
				} else{
					$data['param']		= $location;
					$data['status']		= 401;
					$data['message']	= $location . ' is not available.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_location_serial($location = '', $serial_number = '', $part_number = '')
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			if($method == 'POST'){

				$location 		= $this->input->post('location');
				$serial_number 	= $this->input->post('serial_number');

			}
		}

		if($location == '' || $serial_number == ''){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		}else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$location 		= urldecode($location);

				// $result = $this->api_putaway_model->get_location_serial($location,$serial_number);
				// $result_lp = $this->api_putaway_model->get_location_serial_lp($location,$serial_number);
				// if($result){

/*				$serialized = $this->api_putaway_model->get($serial_number)->row_array();
				if($serialized){
*/
					$cek_kategori = $this->api_putaway_model->cek_kategori($location,$serial_number);
					if($cek_kategori){

						$validSNLocation = $this->api_putaway_model->getSerialLocation($location,$serial_number);
						if($validSNLocation){

							$data['param']			= $location;
							$data['status']			= 400;
							$data['message']		= "This item should be located in ". $validSNLocation['name'];
							$data['response']		= false;

						}else{

							$data['param']			= $location;
							$data['status']			= 200;
							$data['message']		= $location . ' is available.';
							$data['response']		= true;

						}

					}else{

						$data['param']		= $location;
						$data['status']		= 401;
						$data['message']	= 'Invalid Location/Item Category.';
						$data['response']	= false;

					}

/*				}else{

					$data['param']		= $serial_number;
					$data['status']		= 400;
					$data['message']	= $serial_number . ' is not available.';
					$data['response']	= false;

				}
*/
				// }else{
				// 	$data['param']		= $location;
				// 	$data['status']		= 401;
				// 	$data['message']	= 'Wrong location type scanned.';
				// 	$data['response']	= false;
				// }
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}

		}

		echo json_encode($data);
	}

	public function post()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		/*$data['param']		= $params['kd_unik'];
						$data['status']		= 200;
						$data['message']	= $params['kd_unik'] . ' has been stored to ' . $params['loc_name'] . '.';
						$data['response']	= true;
						*/
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['loc_name']			= $this->input->post('location');
				// $params['putaway_time']		= $this->input->post('time');
				$params['putaway_time']		= "NOW()";
				$params['user_name']		= $this->input->post('user');
				$params['pick_time']		= $this->input->post('pick_time');
				$params['user_pick']		= $this->input->post('user_pick');

				if($params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{
					$defLocName = $this->db->select(['loc.name as name'])
											->from('item_receiving_details ird')
											->join('items i','i.id = ird.item_id','left')
											->join('locations loc','loc.id = i.def_loc_id','left')
											->where('ird.unique_code',$params['kd_barang'])
											->get()
											->row_array()['name'];
											
					$defLocid = $this->db->select(['i.id as id'],['loc.id as loc_id'])
											->from('item_receiving_details ird')
											->join('items i','i.id = ird.item_id','left')
											->join('locations loc','loc.id = i.def_loc_id','left')
											->where('ird.unique_code',$params['kd_barang'])
											->get()
											->row_array();
					
					$locId = $this->db->select(['loc.id'])->from('locations loc')->where('loc.name',$params['loc_name'])->get()->row_array();
											
					if($defLocName == $params['loc_name']){
						$params['loc_name'] = urldecode($params['loc_name']);

						$this->db->trans_start();

						$process = $this->api_putaway_model->post($params);
						
						if($process['status']){

							$params['item_receiving_id'] = $process['item_receiving_id'];

							$data['param']		= $params['kd_unik'];
							$data['status']		= 200;
							$data['message']	= $params['kd_unik'] . ' has been stored to ' . $params['loc_name'] . '.';
							$data['response']	= true;

						$this->db->set("start_putaway", "COALESCE(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);

							$isFinish		= $this->api_putaway_model->isFinish($params);
							if(!empty($isFinish)){
								if(isset($isFinish['status'])){
									if($isFinish['status']){
										$this->api_putaway_model->finish($isFinish['kd_receiving']);
									}
								}
							}
							//$this->api_putaway_model->send_staging($count_putaway['kd_receiving']);
						}else{

							$data['param']		= $params['kd_unik'];
							$data['status']		= 400;
							$data['message']	= $process['message'];
							$data['response']	= false;

						}
						$this->db->trans_complete();
					}else if($defLocName == ''){
						if(isset($locId)){
							$this->db->trans_start();
							$this->db->set('def_loc_id',$locId['id']);
							$this->db->set('item_area',substr($params['loc_name'],0,1));
							$this->db->where('id',$defLocid['id']);
							$up = $this->db->update('items');
							
							$this->db->trans_complete();
							if($up){
								$data['param']		= $params['loc_name'];
								$data['status']		= 400;
								$data['message']	= 'new Default Location';
								$data['response']	= false;
							}
						}else{
							$data['param']		= $params['loc_name'];
							$data['status']		= 401;
							$data['message']	= 'Item can\'t be stored in this location!';
							$data['response']	= false;
						}
					}else{
						$data['param']		= $params['loc_name'];
						$data['status']		= 401;
						$data['message']	= 'Item can\'t be stored in this location!';
						$data['response']	= false;
					}
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post_cancel()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');

				if($params['kd_barang'] == '' || $params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{
					$this->api_putaway_model->post_cancel($params);

					$data['param']		= $params['kd_unik'];
					$data['status']		= 200;
					$data['message']	= "Cancelation has been success.";
					$data['response']	= true;

					$count_putaway		= $this->api_putaway_model->count_putaway($params)->row_array();
					if($count_putaway['qty'] == $count_putaway['putted']){
						$this->api_putaway_model->finish($count_putaway['kd_receiving']);
					}
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function getOuterLabelItem(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$outerLabel		= $this->input->post('outer_label');

				$datas = $this->api_putaway_model->getOuterLabelItem($outerLabel);
				if($datas){
					$data['param']		= $outerLabel;
					$data['status']		= 200;
					$data['response']	= true;
					$data['message']	= $datas;
				} else{
					$data['param']		= $outerLabel;
					$data['status']		= 400;
					$data['response']	= false;
					$data['message']	= 'Outer Label not available';
				}

			}else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		echo json_encode($data);

	}

	public function putawayByReceiving($receiving='',$location=''){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $receiving == '' || $location==''){
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{

			$params['receiving'] = $receiving;
			$params['location'] = urldecode($location);

			$process = $this->api_putaway_model->putawayByReceiving($params);
			if($process['status']){

				$data['response']	= true;
				$data['message']	= "Success";

				$this->api_putaway_model->finish($receiving);

			} else{
				$data['response']	= false;
				$data['message']	= $process['message'];
			}

		}

		header("Content-Type:application/json");
		echo json_encode($data);

	}

	public function finish($serialNumber="")
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serialNumber == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$params['kd_unik'] = strtoupper($serialNumber);

			$isFinish		= $this->api_putaway_model->isFinish($params);
			if($isFinish){
				$this->api_putaway_model->finish($isFinish['kd_receiving']);
			}

			$data['status']		= 200;
			$data['message']	= 'Success';
			$data['response']	= true;

		}

		echo json_encode($data);

	}

	public function get_sku_serial(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$sku = $this->input->post('sku');
			$qr = $this->input->post('qr');

			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_putaway_model->get_sku_serial($sku, $qr)->row_array();
				// var_dump($result);exit;
				if(count($result)>0){
					$data['param']			= $sku;
					$data['status']			= 200;
					$data['message']		= $sku . ' is match.';
					$data['response']		= true;
				} else{
					$data['param']		= $sku;
					$data['status']		= 401;
					$data['message']	= $sku . ' is not match.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_sku()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$sku= $this->input->post('sku');;
			// var_dump($sku);exit;
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_putaway_model->get_sku($sku)->row_array();
				// var_dump($result);exit;
				if($result){
					$data['param']			= $sku;
					$data['status']			= 200;
					$data['message']		= $sku . ' is available.';
					$data['response']		= true;
				} else{
					$data['param']		= $sku;
					$data['status']		= 401;
					$data['message']	= $sku . ' is not available.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
/*
	public function get_suggest_location(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$post = json_decode($this->input->post('data'));
				$unique_code = $post->serial_number;

				// $unique_code = $this->input->post('serial_number');

				$this->db
					->select('unique_code, item_id')
					->from('item_receiving_details ird')
					->where_in('unique_code',$unique_code)
					->group_by('unique_code, item_id')
					->limit(5);

				$items = $this->db->get()->result_array();

				$results = array();

				if($items){

					$i=0;
					foreach ($items as $item) {

						$limit = 5;

						$results[$i]['unique_code'] = $item['unique_code'];

						$this->db
							->select('loc.name')
							->from('item_receiving_details ird')
							->join('locations loc','loc.id=ird.location_id')
							->where('item_id',$item['item_id'])
							->where('loc_type IS NULL',NULL)
							->group_by('loc.name')
							->limit(5);

						$currentLocations = $this->db->get()->result_array();

						foreach ($currentLocations as $cl) {
							$results[$i]['location'][] = $cl['name'];
						}

						$limit = $limit - count($currentLocations);

						if($limit){

							$this->db
								->from('locations')
								->where('loc_type IS NULL',NULL)
								->limit($limit);

							$availableLocation = $this->db->get()->result_array();

							foreach ($availableLocation as $al) {
								$results[$i]['location'][] = $al['name'];
							}

						}

						$i++;

					}

				}

				$data['status']		= 200;
				$data['message']	= 'Success';
				$data['response']	= true;
				$data['message']	= true;
				$data['results']	= $results;

			}else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}

		}

		echo json_encode($data);

	}*/

	public function get_recommend_location_by_item_code(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$item_code = json_decode($this->input->post('item_code'), true);

				$results = array();

				if($item_code){

					foreach($item_code as $item){

						$this->db
							->select('loc.name as location_name')
							->from('item_receiving_details ird')
							->join('items itm','itm.id=ird.item_id')
							->join('locations loc','loc.id=ird.location_id')
							->where('itm.code', $item)
							->where_not_in('loc.id',[100,101,102,103,104,105,106,107])
							->where_in('loc.location_type_id',[2,3])
							->group_by('loc.name')
							->limit(2);

						$locations = $this->db->get()->result_array();
						$locLen = count($locations);

						$location = array();


						if(!$locations){

							$limit = 1;

							$this->db
								->select('loc.name as location_name')
								->from('locations loc')
								->where("id NOT IN (SELECT location_id FROM item_receiving_details ird JOIN items itm ON itm.id=ird.item_id WHERE itm.code='$item')",NULL)
								->where_not_in('loc.id',[100,101,102,103,104,105,106,107])
								->where_in('loc.location_type_id',[2,3])
								->limit($limit);

							$locations = $this->db->get()->result_array();


						}else{

							$limit = 1;
							if($locLen < 1){
								$limit = 1 - $locLen;
							}

							$this->db
								->select('loc.name as location_name')
								->from('locations loc')
								->where("id NOT IN (SELECT location_id FROM item_receiving_details ird JOIN items itm ON itm.id=ird.item_id WHERE itm.code='$item')",NULL)
								->where_not_in('loc.id',[100,101,102,103,104,105,106,107])
								->where_in('loc.location_type_id',[2,3])
								->limit($limit);

							$emptyLocations = $this->db->get()->result_array();


							foreach ($emptyLocations as $loc) {
								$location[] = $loc['location_name'];
							}

						}

						if(!$locations){
						foreach ($locations as $loc) {
							$location[] = $loc['location_name'];
							}
						}


						$this->db
							->select(["CONCAT(itm.code, ' - ', itm.name) as item_name"])
							->from('items itm')
							->where('code',$item);

						$itemIdent = $this->db->get()->row_array();

						$results[] = array(
							"item_code"	=> $itemIdent['item_name'],
							"location"	=> implode(", ", $location)
						);

					}

				}
				if($results[0]['location'] == ''){
					$results[0]['location'] = 'Temporary Bin';
				}

				$data['status']		= 200;
				$data['message']	= 'Success';
				$data['response']	= true;
				$data['message']	= true;
				$data['results']	= $results;

			}else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}

		}



		echo json_encode($data);

	}
}

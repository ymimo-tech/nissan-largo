<?php

/**
 * Description of referensi_kitting
 *
 * @author SANGGRA HANDI
 */
class referensi_kitting extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Repack List';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
		$data['jsPage'][] = "assets/custom/js/share.js";
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-kitting.js";

        /* Get Data Select*/
        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        /* Import */
        // if($this->access_right->otoritas('import')){
        //     $data['page'] = [$data['page'],'components/form-import'];
        //     $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        // }

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

        //$this->include_other_view_params($data);
        $data['kitting'] = $this->referensi_kitting_model->options();

        $this->load_model('referensi_barang_model');
        $data['item'] = $this->referensi_barang_model->options();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Referensi Kitting');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

	public function export(){
		$this->load->library('PHPExcel');

		$params = array();

		$result = $this->referensi_kitting_model->export($params)->result_array();
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Master-Kitting")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Serial Number')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Kitting Name')->getStyle('B1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['sn_kitting']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['nama_kitting']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Master-Kitting'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $kitting = $this->referensi_kitting_model->get_data(array('id_kitting' => $id))->row_array();
            $kitting_product = $this->referensi_kitting_model->get_data_kitting_product($id);
            $this->load_model('referensi_barang_model');
            $data['item'] = $this->referensi_barang_model->options();
            $data['kitting_product'] = $kitting_product;
            $html = $this->load->view('referensi_kitting/add_item_edit',$data,TRUE);
            $process_result = array('kitting'=>$kitting,'kitting_product'=>$html);
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    private function include_other_view_params(&$data) {
		$this->load_model('inbound_model');

        //$data['categories'] = $this->referensi_kitting_model->create_options("kategori", "id_kategori", "kd_kategori");
		$data['categories'] = $this->inbound_model->create_options("kategori", "id_kategori", "kd_kategori, nama_kategori");
		$data['categories2'] = $this->inbound_model->create_options("kategori_2", "id_kategori_2", "kd_kategori_2, nama_kategori_2");
        $data['satuan'] = $this->referensi_kitting_model->create_options("satuan", "id_satuan", "nama_satuan");
		$data['shipment_types'] = $this->referensi_kitting_model->create_options_code("m_shipment_type", "shipment_type_code", "shipment_type");
		$data['fifo_periods'] = $this->referensi_kitting_model->create_options_code("m_shipment_periode", "periode_code", "periode");
       // $data['shipment_types'] = array('FEFOY' => "FEFOY");
        //$data['fifo_periods'] = array('MONTHLY' => "MONTHLY");
        $data['owners'] = $this->referensi_kitting_model->create_options("owner", "id_owner", "owner_name");
    }

    public function get_list(){
        $param = array();

		$idKitting = $this->input->post('id_kitting');

		if(!empty($idKitting))
			$param['a.id_kitting'] = $idKitting;

        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $kitting = $this->referensi_kitting_model->data($param)->get();
        $iTotalRecords = $this->referensi_kitting_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($kitting->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_kitting,'delete' => $value->id_kitting));

            $records["data"][] = array(
                // '<input type="checkbox" name="id[]" value="'.$value->id_kitting.'">',
                $_REQUEST['start']+$i,
                $value->kd_barang,
                $value->nama_barang,
                $action
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function add($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load_model('referensi_kitting_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->referensi_kitting_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;



            $this->load->view($this->class_name . '/form2', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
		$this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function proses() {
        $proses_result['status'] = 'OK';

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('id_barang_main', 'Item', 'required');

            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $id_barang = $this->input->post('id_barang_main');
                $items = $this->input->post('id_barang');
                $qty = $this->input->post('qty_barang');
                $for_del = $this->input->post('for_del');
                $data = array(
                    'id_barang' => $id_barang,
             	);
                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $id = $this->referensi_kitting_model->create($data);
                    if ($id) {
                        $proses_result['add'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Referensi Kitting');
						/* END OF INSERT LOG */
                        if(count($items)>0 && is_array($items)){
                            foreach($items as $key => $item){
                                $kit=array(
                                    'id_kitting'=>$id,
                                    'id_barang'=>$item,
                                    'qty'=>$qty[$key]
                                );
                                $this->referensi_kitting_model->insert_kitting_item($kit);
                            }
                        }
                    }
                } else {
                    if ($this->referensi_kitting_model->update($data, $id)) {
                        //$this->referensi_kitting_model->update_barang($data_barang);
                        $proses_result['edit'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Referensi Kitting');
						/* END OF INSERT LOG */
                        if(count($items)>0 && is_array($items)){
                            foreach($items as $key => $item){
                                $ide = explode('_',$key);
                                if($ide[0]=='new'){
                                    $kit=array(
                                        'id_kitting'=>$id,
                                        'id_barang'=>$item,
                                        'qty'=>$qty[$key]
                                    );
                                    $this->referensi_kitting_model->insert_kitting_item($kit);
                                }else{
                                    $kit=array(
                                        'id_kitting'=>$id,
                                        'id_barang'=>$item,
                                        'qty'=>$qty[$key]
                                    );
                                    $this->referensi_kitting_model->update_kitting_item($kit,$key);
                                }
                            }
                        }
                    }
                }

                if(count($for_del)>0 && is_array($for_del)){
                    foreach($for_del as $del){
                        $this->referensi_kitting_model->delete_kitting_item($del);
                    }
                }

            } else {
				$proses_result['status'] = 'ERR';
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
				$proses_result['status'] = 'ERR';
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
				$proses_result['status'] = 'ERR';
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();

        if ($this->access_right->otoritas('delete', true)) {

			$id = $this->input->post("id");
            $result = $this->referensi_kitting_model->delete($id);

        } else {

			$result['status'] = 'ERR';
			$result['no_delete'] = 1;
        }

        echo json_encode($result);
    }

    public function add_item(){
        $this->load_model('referensi_barang_model');
        $data['item'] = $this->referensi_barang_model->options();
        $html = $this->load->view('referensi_kitting/add_item',$data,TRUE);
        echo $html;
    }
}

/* End of file referensi_kitting.php */
/* Location: ./application/controllers/referensi_kitting.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_split_serial_number extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load_model('api_setting_model');
        $this->load_model('api_split_serial_number_model');
    }

    public function index()
    {
        $data['param']      = null;
        $data['status']     = 400;
        $data['message']    = 'Bad request.';
        $data['response']   = false;

        echo json_encode($data);
    }

    public function get($serial_number = '')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $serial_number == ''){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $serialized = $this->api_split_serial_number_model->get($serial_number);
                if($serialized){
                    $data['param']          = $serial_number;
                    $data['status']         = 200;
                    $data['message']        = $serial_number . ' is available.';
                    $data['response']       = true;
                } else{
                    $data['param']      = $serial_number;
                    $data['status']     = 401;
                    $data['message']    = $serial_number . ' is not available.';
                    $data['response']   = false;
                }
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function post(){

        $data = array();

        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            //$auth = $this->api_setting_model->user_authentication();
            //if($auth){
				$flag = ($this->input->post('flag') != null) ? 1 : 0;
				$picking = ($this->input->post('picking') != null) ? $this->input->post('picking') : 0;
				$outbound = ($this->input->post('outbound') != null) ? $this->input->post('outbound') : 0;
				
                $params = array(
                    'serial_number_new' => $this->input->post('new_serial_number'),
                    'serial_number_old' => $this->input->post('old_serial_number'),
                    'qty'               => $this->input->post('qty'),
                    'user'              => $this->input->post('user'),
					'flag'				=> $flag,
					'picking'			=> $picking,
					'outbound'			=> $outbound,
                );

                if($params['serial_number_new'] == '' || $params['serial_number_old'] == '' || $params['qty'] == '' || $params['qty'] == 0 || $params['qty'] < 0){

                    if($params['serial_number_new'] == ''){
                        $data['message']    = 'Serial number baru tidak boleh kosong.';
                    }else if($params['serial_number_old'] == ''){
                        $data['message']    = 'Serial number lama tidak boleh kosong.';
                    }else{
                        $data['message']    = 'Quantity tidak boleh kosong.';
                    }

                    $data['param']      = '';
                    $data['status']     = 400;
                    $data['response']   = false;

                }else{

                    $checkQty = $this->api_split_serial_number_model->checkQty($params);
                    if($checkQty){

                        $checkSerialNumber = $this->api_split_serial_number_model->checkSerialNumber($params['serial_number_new']);
                        if($checkSerialNumber){

                            $process = $this->api_split_serial_number_model->post($params);
                            if($process){

                                $data['param']          = $params['serial_number_new'];
                                $data['status']         = 200;
                                $data['message']        = $params['serial_number_new'] . ' berhasil dibuat.';
                                $data['response']       = true;
								
								$user_id = $this->db->select('id')->from('users')->where('user_name',$params['user'])->get()->row_array()['id'];
								$query_insert = "INSERT INTO split_history (old_lpn, new_lpn, qty, user_id) VALUES ('".$params['serial_number_new']."','".$params['serial_number_old']."',".$params['qty'].",".$user_id.")";
								$this->db->query($query_insert);

                            } else{

                                $data['param']      = $params['serial_number_new'];
                                $data['status']     = 400;
                                $data['message']    = 'Proses gagal, silahkan coba kembali.';
                                $data['response']   = false;

                            }

                        }else{

                            $data['param']      = $params['serial_number_new'];
                            $data['status']     = 400;
                            $data['message']    = 'Serial number yg anda masukkan sudah tersedia di sistem, silahkan menggunakan serial number lain.';
                            $data['response']   = false;

                        }

                    }else{

                        $data['param']      = $params['serial_number_new'];
                        $data['status']     = 400;
                        $data['message']    = 'Quantity SN baru tidak boleh melebihi SN lama.';
                        $data['response']   = false;

                    }

                }

            /*} else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }*/
        }
        echo json_encode($data);

    }

}

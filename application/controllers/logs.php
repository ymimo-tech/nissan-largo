<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class logs extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        /* Otoritas */
        //$this->access_right->check();
        //$this->access_right->otoritas('view', true);

        // Global Model
        // $this->load_model(array($this->class_name . '_model'));
    }


    public function index() {

        //$this->load->library('wcpp'); //LOAD WEBCLIENTPRINT

        $data['title'] = '<i class="icon-plus-sign-alt"></i> Log List';
        $data['page'] = $this->class_name . '/index';

        $this->load->helper('diff_helper');

        $this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
        $data['jsPage'][] = "assets/pages/scripts/logs/sap_logs.js";

        /* INSERT LOG */
        $this->access_right->activity_logs('view','SAP Log');
        /* END OF INSERT LOG */

        $this->load->view('template', $data);

    }

    public function getSAPLog(){

        $result = array();
        $requestData = $_REQUEST;

        $columns = array(
            0 => 'sync_date',
            1 => '',
            2 => 'stg.refCode',
            3 => 'stg.status',
            4 => ''
        );

        $this->db->start_cache();

        $this->db
            ->select('stg.refId, stg.refCode, stg.status, ali.response')
            ->select(["COALESCE(stg.sync_date,ali.created_at) as sync_date"])
            ->from('staging stg')
            ->join('api_log_integration ali','ali.body=stg.body','left')
            ->order_by('stg.id','desc');

        $this->db->stop_cache();

        $row = $this->db->get();
        $totalData = $row->num_rows();

        $this->db
            ->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
            ->limit($requestData['length'],$requestData['start']);

        $row = $this->db->get()->result_array();
        $totalFiltered = count($row);

        $data = array();
        $action = '';

        for($i=0;$i<$totalFiltered;$i++){

            if(!$row[$i]['status']){

                $class="";
                $status="Failed";

                $response = json_decode($row[$i]['response'], TRUE);

                if(!empty($response['d'])){
                    $resp = $response['d']['Message'];
                }else{
                    $resp = $response['error']['message']['value'];
                }

            }else{

                $class = "disabled-link";
                $status = "Success";
                $resp = " - ";

            }

            $action = '<div class="btn-group">
                        <button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                            &nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a class="data-table-retry '.$class.'" data-id="'.$row[$i]['refId'].'" data-code="'.$row[$i]['refCode'].'">Retry</a>
                            </li>
                        </ul>
                            ';

            $nested = array();
            $nested[] = $row[$i]['refId'];
            $nested[] = $i+1;
            $nested[] = $row[$i]['refCode'];
            $nested[] = $status;
            $nested[] = $resp;
            $nested[] = $row[$i]['sync_date'];
            $nested[] = $action;

            $data[] = $nested;
        }

        $result = array(
            "draw"            => intval( $requestData['draw'] ),
            "recordsTotal"    => intval( $totalFiltered ),
            "recordsFiltered" => intval( $totalData ),
            "data"            => $data
        );

        header("Content-type:application/json");
        echo json_encode($result);
    }

    private function include_required_file(&$data){

        $data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";

        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
        $data['jsPage'][] = 'assets/custom/js/share.js';

    }

    public function retry(){

    	$params = array(
            'refId' => $this->input->post('refId'),
    		'refCode' => $this->input->post('refCode')
    	);

        $this->db
            ->from('staging')
            ->where('refCode',$params['refCode']);
        $staging = $this->db->get()->row_array();

        if($staging){

        	if($staging['refCode'] === "PL-"){

                $this->retry_GI($staging['refCode']);

        	}else{

                $check = explode("-",$staging['refCode']);

                if(count($check) > 2){

                    $this->retry_TP($staging['refId']);

                }else{

                    $this->retry_GR($staging['refId']);

                }

        	}

        }

    }

    public function retry_GI($docCode=""){
		$this->load_model('api_picking_model');
		$this->api_picking_model->sendToStaging($docCode);
    }

    public function retry_GR($docId=""){
		$this->load_model('receiving_model');
		$this->receiving_model->sendToStaging($docId);
    }

	public function retry_TP($docId=""){
		$this->load_model('api_putaway_model');
		$this->api_putaway_model->sendToStaging($docId);
	}

    public function retry_AP($transactionCode){
        $this->load_model('api_agent_app_model');
        $this->api_agent_app_model->sendToStaging($transactionCode);    
    }

    public function retry_QC($logId){
        $this->load_model('api_qc_model');
        $this->api_qc_model->sendToStaging($logId);
    }

	public function activity(){

        $this->db
        	// ->select('')
            // ->select('body')
            ->from('api_log_integration')
            ->limit(20)
            ->order_by('id','desc');

		$data = $this->db->get()->result_array();


        header("Content-Type:application/json");
        echo json_encode($data);

	}

}

/* End of file logs.php */
/* Location: ./application/controllers/logs.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_sync extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_setting_model');
		$this->load->model('api_loading_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function sync_qty(){
		$this->load->model('api_sync_model');
		$this->api_sync_model->sync_qty();
	}

	public function __first_sync(){
		$this->load->model('api_sync_model');
		$this->api_sync_model->first_sync();
	}

	public function first_sync_inbound(){
		$this->load->model('api_sync_model');
		$this->api_sync_model->first_sync_inbound();
	}

	public function cek_inb_l2(){
		$this->load->model('api_sync_model');
		$this->api_sync_model->cek_inb_l2();
	}

	public function sync_inb_l2(){
		$this->load->model('api_sync_model');
		$this->api_sync_model->sync_staging_inb_l2();
	}

}

<?php

/**
 * Description of referensi_kategori
 *
 * @author SANGGRA HANDI
 */
class referensi_kategori_2 extends CI_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

		// Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Protection
       /* hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
			*/
        // Global Model
        $this->load->model(array($this->class_name . '_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Category Info';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-kategori-2.js";
		$data['jsPage'][] = "assets/custom/js/share.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Referensi Kategori 2');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $kategori = $this->referensi_kategori_2_model->get_data(array('id_kategori_2' => $id))->row_array();
            $process_result = $kategori;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

	public function export(){
		$this->load->library('PHPExcel');

		$params = array();

		$result = $this->referensi_kategori_2_model->get_data($params)->result_array();
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Master-Category-2")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Category 2 Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Category 2 Name')->getStyle('B1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['kd_kategori_2']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['nama_kategori_2']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Master-Category-2'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

    public function get_list(){
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $kategori = $this->referensi_kategori_2_model->data($param)->get();
        $iTotalRecords = $this->referensi_kategori_2_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($kategori->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_kategori_2,'delete' => $value->id_kategori_2));

            $records["data"][] = array(
                // '<input type="checkbox" name="id[]" value="'.$value->id_kategori_2.'">',
                $_REQUEST['start']+$i,
                // $value->kd_kategori_2,
                $value->nama_kategori_2,
                $action
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function add($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('referensi_kategori_2_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->referensi_kategori_2_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;



            $this->load->view($this->class_name . '/form2', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
		$this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function proses() {
        $proses_result['success'] = 1;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_kategori_2', 'Category Code', 'required|trim');
            $this->form_validation->set_rules('nama_kategori_2', 'Category Name', 'required|trim');


            if ($this->form_validation->run()) {
                $id = $this->input->post('id');

                $data = array(
                    'kd_kategori_2' => $this->input->post('kd_kategori_2'),
                    'nama_kategori_2' => $this->input->post('nama_kategori_2'),
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->referensi_kategori_2_model->create($data)) {
                        $proses_result['add'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Referensi Kategori 2');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->referensi_kategori_2_model->update($data, $id)) {
                        $proses_result['edit'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Referensi Kategori 2');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();

        if ($this->access_right->otoritas('delete', true)) {

            $id = $this->input->post("id");
            $result = $this->referensi_kategori_2_model->delete($id);

        } else {

            $result['status'] = 'ERR';
			$result['no_delete'] = 1;

        }

        echo json_encode($result);
    }



}

/* End of file referensi_kategori.php */
/* Location: ./application/controllers/referensi_kategori.php */

<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class destination extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Destination';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;

        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-destination.js";

        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }
 
		/* Otoritas Tombol tambah */
	 
		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
			);
		}


        $this->load_model('outbound_document_model');
        $data['outbound_doc'] = $this->outbound_document_model->get_data()->result_array();
		 
		/* INSERT LOG */
		$this->access_right->activity_logs('view','Destination');

        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $destination = $this->destination_model->get_data(array('idGudang' => $id))->row_array();
            $destination['docType'] = $this->destination_model->get_doc_gudang($id);
            $process_result = $destination;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){

        $columns = array(
            1 => 'destination_name',
        );

        $param = '';

        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $destinations = $this->destination_model->data($param)->get();
        $iTotalRecords = $destinations->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($destinations->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->idGudang,'delete' => $value->idGudang));
            
            $records["data"][] = array(
                // '<input type="checkbox" name="id[]" value="1">',
                $_REQUEST['start']+$i,
                $value->namaGudang,
                $action
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('destination_name', 'Destination Name', 'required|trim');
            $this->form_validation->set_rules('destination_code', 'Destination Initial', 'required|trim');


            if ($this->form_validation->run()) {

                $proses_result['success'] = 1;

                $id = $this->input->post('id');

                $data = array(
                    'namaGudang' => $this->input->post('destination_name'),
                    'kodeGudang' => $this->input->post('destination_code')
             	);

                $data2 = $this->input->post('outbound_doc');

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->destination_model->create($data,$data2)) {
                        $proses_result['add'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Destination');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->destination_model->update($id, $data, $data2)) {
                        $proses_result['edit'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Destination');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();
		
        if ($this->access_right->otoritas('delete', true)) {
			
            $id = $this->input->post("id");
            $result = $this->destination_model->delete($id);
			
        } else {
			
            $result['status'] = 'ERR';
			$result['no_delete'] = 1;
			
        }
		
        echo json_encode($result);
    }

    public function getDestination(){

        $params = array(
            "id" => $this->input->post('id')
        );

        $results = $this->destination_model->getDestination($params);
        echo json_encode($results);
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Destination Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        'Destination Name'     => 'dest_name',
                        'Destination Initial'  => 'dest_initial'
                    ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->destination_model->importFromExcel($data);

                        if($process){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage('Please check again your data !');

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }
 
        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }
}

/* End of file referensi_barang.php */
/* Location: ./application/controllers/referensi_barang.php */

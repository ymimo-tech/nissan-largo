<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_bin_transfer extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load_model('api_setting_model');
        $this->load_model('api_bin_transfer_model');
    }

    public function index()
    {
        $data['param']      = null;
        $data['status']     = 400;
        $data['message']    = 'Bad request.';
        $data['response']   = false;

        echo json_encode($data);
    }

    public function get(){

        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET'){

            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;

        } else{

            $auth = $this->api_setting_model->user_authentication();
            if($auth){

                $data['param']      = '';
                $data['status']     = 200;
                $data['message']    = '';
                $data['results']	= $this->db->select('bin_transfer_code')->get_where('bin_transfer',['status_id <>'=>4])->result_array();

            }else{

                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;

            }

        }

        echo json_encode($data);

    }

    public function get_item(){

        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){

            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;

        }else{

            $auth = $this->api_setting_model->user_authentication();
            if($auth){

            	$binTransferCode = $this->input->post('bin_transfer_code');

                $data['param']      = $binTransferCode;
                $data['status']     = 200;
                $data['message']    = '';
                $data['results']	= $this->api_bin_transfer_model->getItem($binTransferCode)->result_array();

            }else{

                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;

            }

        }

        echo json_encode($data);

    }

    public function get_serial()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){

                $params = array(
                    "serial_number"     => $this->input->post('serial_number'),
                    "item_code"         => $this->input->post('item_code'),
                    "bin_transfer_code" => $this->input->post('bin_transfer_code')
                );

                $checkParam = $this->api_bin_transfer_model->checkParam($params);
                if($checkParam['status']){

                    $validateSerial = $this->api_bin_transfer_model->validateSerial($params);
                    if($validateSerial){

                        if($validateSerial['bin_transfer_code']){

                            $validateQty = $this->api_bin_transfer_model->validateQty($params);
                            if($validateQty){

                                if((($validateQty['document']['order_qty']-$validateQty['document']['scanned_qty']) - $validateQty['stock']) >= 0){

                                    $getRecomendation = $this->api_bin_transfer_model->getPickingRecomendation($params);
                                    if($getRecomendation['status']){

                                        $params['data'] = $validateSerial;

                                        $post = $this->api_bin_transfer_model->pickItem($params);

                                        unset($params['data']);

                                        if($post){

                                            $data['param']      = $params;
                                            $data['status']     = 200;
                                            $data['message']    = $post['message'];
                                            $data['response']   = true;
                                            $data['old_location'] = $validateSerial['location_name'];

                                            if(empty($validateSerial['bin_transfer_start'])){
                                                $this->api_bin_transfer_model->start($params['bin_transfer_code']);
                                            }

                                        }else{

                                            $data['param']      = $params;
                                            $data['status']     = 400;
                                            $data['message']    = $post['message'];
                                            $data['response']   = false;

                                        }

                                    }else{

                                        $data['param']      = $params;
                                        $data['status']     = 400;
                                        $data['message']    = 'Silahkan ambil ';

                                        foreach ($getRecomendation['data'] as $cr) {
                                            $data['message'] .= $cr['unique_code'].' - '.$cr['location_name'].'<br/>';
                                        }

                                        $data['response']   = false;

                                    }

                                }else{

                                    $data['param']      = $params;
                                    $data['status']     = 400;
                                    $data['message']    = 'Quantity cannot be more than '.($validateQty['document']['order_qty']-$validateQty['document']['scanned_qty']).'.';
                                    $data['response']   = false;                                

                                }

                            }else{

                                $data['param']      = $params;
                                $data['status']     = 400;
                                $data['message']    = 'Maximum quantity has been reached. Please return last scanned item. Quantity cannot be more than '.$validateQty[''].'.';
                                $data['response']   = false;                                

                            }

                        }else{

                            $data['param']      = $params;
                            $data['status']     = 400;
                            $data['message']    = 'This item is not on the list.';
                            $data['response']   = false;

                        }

                    }else{

                        $data['param']      = $params;
                        $data['status']     = 400;
                        $data['message']    = $params['serial_number'] . ' is not available.';
                        $data['response']   = false;

                    }
                }else{

                    $data['param']      = $params;
                    $data['status']     = 400;
                    $data['message']    = "Parameter ".$checkParam['key']." cannot be empty.";
                    $data['response']   = false;

                }

            } else{

                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;

            }

        }
        echo json_encode($data);
    }

    public function get_item_scanned_detail(){

        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){

            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;

        }else{

            $auth = $this->api_setting_model->user_authentication();
            if($auth){

                $binTransferCode = $this->input->post('bin_transfer_code');
                $itemCode = $this->input->post('item_code');

                $data['param']      = array('bin_transfer_code'=>$binTransferCode,'item_code'=>$itemCode);
                $data['status']     = 200;
                $data['message']    = '';
                $data['results']    = $this->api_bin_transfer_model->getItemScannedDetail($binTransferCode,$itemCode);

            }else{

                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;

            }

        }

        echo json_encode($data);

    }

    public function get_location()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){

            	$location = $this->input->post('location_code');
                $result = $this->api_bin_transfer_model->get_location($location)->row_array();
                if($result){
                    $data['param']          = $location;
                    $data['status']         = 200;
                    $data['message']        = $location . ' is available.';
                    $data['response']       = true;
                } else{
                    $data['param']      = $location;
                    $data['status']     = 401;
                    $data['message']    = $location . ' is not available.';
                    $data['response']   = false;
                }
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function post()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){

            	$params = array(
            		"serial_number"		=> $this->input->post('serial_number'),
            		"item_code"			=> $this->input->post('item_code'),
            		"bin_transfer_code"	=> $this->input->post('bin_transfer_code'),
            		"location_code"		=> $this->input->post('location_code')
            	);

                $checkParam = $this->api_bin_transfer_model->checkParam($params);
                if($checkParam['status']){

                    $validateSerial = $this->api_bin_transfer_model->validateSerial($params);
                    if($validateSerial){

                    	if($validateSerial['bin_transfer_code']){

                          //   $validateQty = $this->api_bin_transfer_model->validateQty($params);
                          //   if($validateQty){

        	                	// $getRecomendation = $this->api_bin_transfer_model->getPickingRecomendation($params);
        	                	// if($getRecomendation['status']){

                                    $validateLocation = $this->api_bin_transfer_model->get_location($params['location_code'])->row_array();
                                    if($validateLocation){

                                        $params['data'] = $validateSerial;
                                        $params['location_id'] = $validateLocation['loc_id'];

        	                			$post = $this->api_bin_transfer_model->post($params);

                                        unset($params['data']);
                                        unset($params['location_id']);

        	                			if($post){

        				                    $data['param']      = $params;
        				                    $data['status']     = 200;
        				                    $data['message']    = 'Data has been update.';
        				                    $data['response']   = true;

                                            // if(empty($validateSerial['bin_transfer_start'])){
                                            //     $this->api_bin_transfer_model->start($params['bin_transfer_code']);
                                            // }
											
											$this->load_model('api_integration_model');
											$this->api_integration_model->post_bin_transfer($params['bin_transfer_code'], $params['serial_number']);

                                            $checkFinish = $this->api_bin_transfer_model->checkFinish($params['bin_transfer_code']);
                                            if($checkFinish){
                                                $this->api_bin_transfer_model->finish($params['bin_transfer_code']);
                                            }

        	                			}else{

        				                    $data['param']      = $params;
        				                    $data['status']     = 400;
        				                    $data['message']    = $post['message'];
        				                    $data['response']   = false;

        	                			}

                                    }else{

                                        $data['param']      = $params;
                                        $data['status']     = 400;
                                        $data['message']    = "Location '".$params['location_code']."' doesn't exists";
                                        $data['response']   = false;

                                    }

        		     //            }else{

        							// $data['param']		= $params;
        							// $data['status']		= 400;
        							// $data['message']	= 'Silahkan ambil ';

        							// foreach ($checkRecomendation['data'] as $cr) {
        							// 	$data['message'] .= $cr['unique_code'].' - '.$cr['location_name'].'<br/>';
        							// }

        							// $data['response']	= false;

        		     //            }

               //              }else{

               //                  $data['param']      = $params;
               //                  $data['status']     = 400;
               //                  $data['message']    = 'Maximum quantity has been reached. Please return last scanned item.';
               //                  $data['response']   = false;                                

               //              }

                    	}else{

    	                    $data['param']      = $params;
    	                    $data['status']     = 400;
    	                    $data['message']    = 'This item is not on the list.';
    	                    $data['response']   = false;

                    	}

                    }else{

                        $data['param']      = $params;
                        $data['status']     = 400;
                        $data['message']    = $params['serial_number'] . ' is not available.';
                        $data['response']   = false;

                    }
                }else{

                    $data['param']      = $params;
                    $data['status']     = 400;
                    $data['message']    = "Parameter ".$checkParam['key']." cannot be empty.";
                    $data['response']   = false;

                }

            } else{

                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;

            }

        }
        echo json_encode($data);
    }

    public function cancel()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){

                $serialNumber = json_decode($this->input->post('serial_number'),TRUE);
                $params = array(
                    "serial_number"     => $serialNumber['serial_number'],
                    "bin_transfer_code" => $this->input->post('bin_transfer_code')
                );

                $checkParam = $this->api_bin_transfer_model->checkParam($params);
                if($checkParam['status']){

                    $cancel = $this->api_bin_transfer_model->cancel($params);
                    if($cancel){

                        $data['param']      = $params;
                        $data['status']     = 200;
                        $data['message']    = 'Data has been canceled.';
                        $data['response']   = true;

                    }else{

                        $data['param']      = $params;
                        $data['status']     = 400;
                        $data['message']    = 'Failed canceled data.';
                        $data['response']   = false;

                    }

                }else{

                    $data['param']      = $params;
                    $data['status']     = 400;
                    $data['message']    = "Parameter ".$checkParam['key']." cannot be empty.";
                    $data['response']   = false;

                }

            }else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

}

<?php

class inv_transfer extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Inventory Transfer List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/inv_transfer/lr-inv-transfer-view.js";
		$data['today'] = date('d/m/Y');

		$this->load_model('inbound_model');
		$this->load_model('referensi_barang_model');

		$data['items'] = $this->referensi_barang_model->options();
		$data['location'] = $this->inv_transfer_model->getLocation();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Inventory Transfer');
        /* END OF INSERT LOG */

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	//REQUEST
	public function getSerialNumber(){
		$result = array();

		$params = array(
			'query' 	=> filter_var(trim($this->input->post('query')), FILTER_SANITIZE_STRING),
			'params' 	=> $this->input->post('params')
		);

		$result = $this->inv_transfer_model->getSerialNumber($params);

		echo json_encode($result);
	}

	public function export(){
		$data = array();

		$itemName = '-- All --';
		$idBarang = $this->input->post('id_barang');

		if(!empty($idBarang)){
			$itemName = $this->inv_transfer_model->getItemNameById(filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT));
		}

		$locName = '-- All --';
		$locId = $this->input->post('loc_id');
		if(!empty($locId)){
			$locName = $this->inv_transfer_model->getLocNameById(filter_var(trim($this->input->post('loc_id')), FILTER_SANITIZE_NUMBER_INT));
		}

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
			'loc_id'	=> filter_var(trim($this->input->post('loc_id')), FILTER_SANITIZE_NUMBER_INT),
			'item_name'	=> $itemName,
			'loc_name'	=> $locName,
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'warehouse'     => $this->input->post('warehouse')
		);

		$data['page'] = $this->class_name . '/inventory_transfer';
		$data['params'] = $params;
		$data['data'] = $this->inv_transfer_model->getListRaw($params);

		$html = $this->load->view($data['page'], $data, true);

        $path = "inventory_transfer.pdf";

        ini_set('memory_limit','256M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            10, // margin bottom
            10, // margin header
            10
		); // margin footer

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

	}
	
	public function export_excel(){
		$data = array();

		$itemName = '-- All --';
		$idBarang = $this->input->post('id_barang');

		if(!empty($idBarang)){
			$itemName = $this->inv_transfer_model->getItemNameById(filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT));
		}

		$locName = '-- All --';
		$locId = $this->input->post('loc_id');
		if(!empty($locId)){
			$locName = $this->inv_transfer_model->getItemNameById(filter_var(trim($this->input->post('loc_id')), FILTER_SANITIZE_NUMBER_INT));
		}

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
			'loc_id'	=> filter_var(trim($this->input->post('loc_id')), FILTER_SANITIZE_NUMBER_INT),
			'item_name'	=> $itemName,
			'loc_name'	=> $locName,
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'warehouse'     => $this->input->post('warehouse')
		);

		$data = $this->inv_transfer_model->getListRaw($params);

        $this->load->library('PHPExcel');

        $objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Inventory Transfer")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		
        $sheet->setCellValueByColumnAndRow(0, 1, 'Items')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, ucfirst(strtolower($params['item_name'])))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 2, 'Location')->getStyle('A2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 2, ucfirst($params['loc_name']))->getStyle('B2')->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(3, 1, 'From')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, $params['from'])->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 2, 'To')->getStyle('D2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 2, $params['from'])->getStyle('E2')->getFont()->setBold(true);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:I5');
        $objPHPExcel->getActiveSheet()->getStyle('G5:I5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //->setVERTICAL(PHPExcel_Style_Alignment::PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:L5');
        $objPHPExcel->getActiveSheet()->getStyle('J5:L5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
        $objPHPExcel->getActiveSheet()->getStyle('A5:A6')->getAlignment()->setVERTICAL(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:B6');
        $objPHPExcel->getActiveSheet()->getStyle('B5:B6')->getAlignment()->setVERTICAL(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
        $objPHPExcel->getActiveSheet()->getStyle('C5:C6')->getAlignment()->setVERTICAL(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
        $objPHPExcel->getActiveSheet()->getStyle('D5:D6')->getAlignment()->setVERTICAL(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
		$sheet->setCellValueByColumnAndRow(0, 5, 'No')->getStyle('A5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 5, 'Bin Transfer Document')->getStyle('B5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 5, 'Item Code')->getStyle('C5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 5, 'Item Name')->getStyle('D5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 5, 'Serial Number')->getStyle('E5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 5, 'Quantity')->getStyle('F5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 5, 'From')->getStyle('G5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(9, 5, 'To')->getStyle('J5')->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(6, 6, 'Location')->getStyle('G6')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(7, 6, 'By')->getStyle('H6')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(8, 6, 'Time')->getStyle('I6')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(9, 6, 'Location')->getStyle('J6')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(10, 6, 'By')->getStyle('K6')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(11, 6, 'Time')->getStyle('L6')->getFont()->setBold(true);



        $len = count($data);
        if($len > 0){
            $row = 7;
            for($i = 0; $i < $len; $i++){
                $sheet->setCellValueByColumnAndRow(0, $row, ($i + 1));
                $sheet->setCellValueByColumnAndRow(1, $row, $data[$i]['bin_transfer_code']);
                $sheet->setCellValueByColumnAndRow(2, $row, $data[$i]['kd_barang']);
                $sheet->setCellValueByColumnAndRow(3, $row, $data[$i]['nama_barang']);
                $sheet->setCellValueByColumnAndRow(4, $row, $data[$i]['kd_unik']);
                $sheet->setCellValueByColumnAndRow(5, $row, $data[$i]['quantity']);
                $sheet->setCellValueByColumnAndRow(6, $row, $data[$i]['loc_from']);
                $sheet->setCellValueByColumnAndRow(7, $row, $data[$i]['pick_by']);
                $sheet->setCellValueByColumnAndRow(8, $row, $data[$i]['pick_time']);
                $sheet->setCellValueByColumnAndRow(9, $row, $data[$i]['loc_to']);
                $sheet->setCellValueByColumnAndRow(10, $row, $data[$i]['put_by']);
                $sheet->setCellValueByColumnAndRow(11, $row, $data[$i]['put_time']);
                $row++;
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Inventory Transfer'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

    }

	public function getList(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
			'loc_id'	=> filter_var(trim($this->input->post('loc_id')), FILTER_SANITIZE_NUMBER_INT),
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'sn'		=> filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'warehouse'     => $this->input->post('warehouse')

		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->inv_transfer_model->getList($params);

		echo json_encode($result);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_packing extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_packing_model');
		$this->load_model('packing_model');
		$this->load_model('api_integration_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function load()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= 'PICKING';
				$data['status']		= 200;
				$data['message']	= 'Load data for available picking list.';
				$data['response']	= true;
				$data['results']	= $this->api_packing_model->load()->result_array();
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_picking(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= 'PACKING';
				$data['status']		= 200;
				$data['message']	= 'Load data for available picking list.';
				$data['response']	= true;
				$data['results']	= $this->api_packing_model->get_picking();

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_outbound($picking_code=''){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' && $picking_code === ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$data['param']		= 'PACKING';
				$data['status']		= 200;
				$data['message']	= 'Load data for available picking list.';
				$data['response']	= true;
				$data['results']	= $this->api_packing_model->get_outbound($picking_code);

			} else {

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}
		echo json_encode($data);
	}

	public function get_items(){

		$method = $_SERVER['REQUEST_METHOD'];
		$outbound_code = $this->input->post("outbound_code");
		$picking_code = $this->input->post('picking_code');

		if($method != 'POST' || $outbound_code === '' || $picking_code === ''){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$data['param']		= 'PACKING';
				$data['status']		= 200;
				$data['message']	= 'Load data for available picking list.';
				$data['response']	= true;
				$data['results']	= $this->api_packing_model->get_items($outbound_code,$picking_code);

			} else {

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}
		echo json_encode($data);
	}

	public function check_pc_code(){
		
		$id_outbound = $this->api_packing_model->get_outbound_id($this->input->post('id_outbound'),'');

        $params = array(
            "pc_code"   => $this->input->post('pc_code'),
            "inc"       => $this->input->post('inc'),
            'id_outbound' => $id_outbound
        );

        $data = $this->packing_model->check_pc_code($params);

        $result = array();

        if($data['total'] > 0){
            $result['status'] = 'OK';
        }else{
            $result['status'] = 'ERR';
            $result['message'] = 'Please scan item before create new packing';
        }

        echo json_encode($result);
    }

    public function get_pc_code($outboundCode=''){

    	$id_outbound = $this->api_packing_model->get_outbound_id($outboundCode,'');

        $pn = $this->packing_model->get_packing_number($id_outbound);
        if($pn->num_rows()>0){
            $pc_code=$pn->row()->packing_number;
            //$pc_string_tmp = $pn->row()->pc_string;
            $inc=$pn->row()->inc;
            $pc_string_tmp = '';
            for($i=1;$i<=$inc;$i++){
                if($inc<10){
                    $pad=0;
                }else{
                    $pad='';
                }
                if($i==$inc){
                    $pc_string_tmp = '- <b>'.$pc_code.$pad.$i.$pad.$inc.'</b><br/> '.$pc_string_tmp;
                }else{
                    $pc_string_tmp = '&nbsp;&nbsp;- '.$pc_code.$pad.$i.$pad.$inc.'<br/> '.$pc_string_tmp;
                }
            }
            //$pc_string = str_replace('x',$inc,$pc_string_tmp);
        }else{
            $this->packing_model->add_pc_code();
            $pc_code = $this->packing_model->get_pc_code();
            $pc_string_tmp= '<b>'.$pc_code.'0101</b>';
            $inc=1;
        }

		echo json_encode(array('pc_code'=>$pc_code,'inc'=>$inc,'pc_string'=>$pc_string_tmp));
        //return array('pc_code'=>$pc_code,'inc'=>$inc,'pc_string'=>$pc_string_tmp);
    }

	public function get_packing_number(){

		$method = $_SERVER['REQUEST_METHOD'];
		$outboundCode = $this->input->post('outbound_code');

		if($method != 'POST' || $outboundCode === ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$data['param']		= 'PACKING';
				$data['status']		= 200;
				$data['message']	= 'Load data for available picking list.';
				$data['response']	= true;
				$data['results']	= $this->get_pc_code($outboundCode);

			} else {

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}
		echo json_encode($data);
	}
	
	public function post(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

		        $kd_unik = $this->input->post('serial_number');
				$outboundCode = $this->input->post('outbound_code');
				// $pcCode = $this->get_pc_code($outboundCode);
				$pcCode = $this->input->post('packing_code');
				
				$isTray = $this->api_packing_model->isTray($kd_unik);
				if(count($isTray) > 0) {
					if($isTray['name'] == 'TRAY') {
						$getSnByTray = $this->api_packing_model->getSnByTray($kd_unik);
						if(count($getSnByTray) > 0) {
							for($i=0;$i<count($getSnByTray);$i++) {
								ob_start();
								$result = $this->post_proses($getSnByTray[$i]['unique_code'], $outboundCode, $pcCode);
								ob_get_clean();
							}
							$data['param']		= null;
							$data['status']		= 200;
							$data['message']	= 'Tray scanned';
							$data['response']	= true;
							echo json_encode($data);
						} else {
							$data['param']		= $kd_unik;
							$data['status']		= 400;
							$data['message']	= 'Tray is empty.';
							$data['response']	= false;
							echo json_encode($data);
						}
					} else {
						$data['param']		= $kd_unik;
						$data['status']		= 400;
						$data['message']	= $kd_unik . ' is not a Tray.';
						$data['response']	= false;
						echo json_encode($data);
					}
				} else {
					$this->post_proses($kd_unik, $outboundCode, $pcCode);
				}
			} else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				echo json_encode($data);
			}
		}
	}

	public function post_proses($serial_number, $outbound_code, $packing_code){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

		        $kd_unik = $serial_number;
				$outboundCode = $outbound_code;
				// $pcCode = $this->get_pc_code($outboundCode);
				$pcCode = json_decode($packing_code,true);

		        $id_outbound = $this->api_packing_model->get_outbound_id($outboundCode, $kd_unik);

		        $pc_code = $pcCode['pc_code'];
		        $inc = $pcCode['inc'];

		        $id_picking = $this->packing_model->get_picking($id_outbound);
		        $exist = $this->packing_model->get_avail_qty($kd_unik,$id_picking);
		        if($exist){

		            $cek_barang = $this->packing_model->get_avail_qty($kd_unik,$id_picking);
		            
		            if($cek_barang!=0){

				        // $id_barang = $this->api_packing_model->get_receiving_barang($kd_unik)->id_barang;
		                $checkQty = $this->packing_model->checkQty($id_outbound,$id_picking,$kd_unik);
				        if($checkQty){

			                $id_receiving_barang = $this->packing_model->get_receiving_barang($kd_unik);

			                $this->db->trans_start();

			                $this->packing_model->update_picking_recomendation($kd_unik,$id_outbound,$id_picking);
		                    $this->packing_model->insert_outbound_receiving($id_outbound,$id_picking,$pc_code,$inc,$kd_unik);

			                // $this->packing_model->update_receiving_barang($kd_unik);
			                // $this->packing_model->update_location_outbound($kd_unik);
			                // $this->packing_model->insert_outbound_receiving(array('id_outbound'=>$id_outbound,'id_receiving_barang'=>$id_receiving_barang,'packing_number'=>$pc_code,'inc'=>$inc,'kd_unik'=>$));
			                // $this->packing_model->closePacking($id_picking);

			               	$this->db->trans_complete();

			               	if($this->db->trans_status()){

								$data['param']		= $kd_unik;
								$data['status']		= 200;
								$data['message']	= 'Serial number scanned';
								$data['response']	= true;
								
								// $this->api_integration_model->post_loading($id_picking,$kd_unik);

			               	}else{

								$data['param']		= null;
								$data['status']		= 400;
								$data['message']	= "Please Try Again !";
								$data['response']	= false;

			               	}

						}else{

							$data['param']		= $kd_unik;
							$data['status']		= 400;
							$data['message']	= 'Quantity already complete';
							$data['response']	= false;

						}

		            }else{

						$data['param']		= $kd_unik;
						$data['status']		= 400;
						$data['message']	= 'Serial number has been scanned';
						$data['response']	= false;

		            }

		        }else{

					$data['param']		= $kd_unik;
					$data['status']		= 400;
					$data['message']	= 'Serial number not exist';
					$data['response']	= false;

		        }
			} else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}
		echo json_encode($data);
	}

	public function generate()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['packing_code']	= $this->input->post('packing_code');
				$params['packing_date']	= $this->input->post('date');
				$params['user_name']	= $this->input->post('user');
				$packed 				= $this->api_packing_model->get($params['packing_code'])->row_array();
				if($packed){
					$params['packing_code']	= $this->api_packing_model->add($params['packing_code']);
					$this->api_packing_model->generate($params);
					$data['param']		= $params['packing_code'];
					$data['status']		= 200;
					$data['message']	= $params['packing_code'] . ' has been generated.';
					$data['response']	= true;
				} else{
					$this->api_packing_model->generate($params);
					$data['param']		= $params['packing_code'];
					$data['status']		= 200;
					$data['message']	= $params['packing_code'] . ' has been generated.';
					$data['response']	= true;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_serial($serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$serialize = $this->api_packing_model->get_serial($serial_number)->row_array();
				if($serialize){
					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= $serial_number . ' is valid.';
					$data['response']	= true;
				} else{
					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function checkQty($id_outbound='',$id_picking='',$id_barang=''){
	    $checkQty = $this->api_packing_model->checkQty($id_outbound,$id_picking,$id_barang);
	}

	public function close(){

		$this->packing_model->closePacking($this->input->post('id_picking'));

	}

	public function get_serial_number_by_picking($pickingCode="",$itemCode=''){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $pickingCode == '' || $itemCode==''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$serialize = $this->api_packing_model->getSerialNumberbyPicking($pickingCode,str_replace('|','/',$itemCode));
				if($serialize){
					$data['param']		= $pickingCode;
					$data['status']		= 200;
					$data['message']	= $pickingCode . ' is valid.';
					$data['response']	= $serialize;
				} else{
					$data['param']		= $pickingCode;
					$data['status']		= 400;
					$data['message']	= $pickingCode . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		echo json_encode($data);

	}

	public function get_sn_by_tray($tray=""){

		$data = array();

		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'GET'){
			
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
			
		} else{
			
			$auth = $this->api_setting_model->user_authentication();
			
			if($auth){

				if($tray == ""){

					$data['param']		= $tray;
					$data['status']		= 400;
					$data['message']	= '';
					$data['response']	= false;

				} else{

					$data['param']		= $tray;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getSnByTray($tray);

				}				
				
			} else{
				
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				
			}
		}

		echo json_encode($data);		
	}

}


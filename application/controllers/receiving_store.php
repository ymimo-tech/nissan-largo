<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class receiving_store extends MY_Controller {

	public function index()
	{
		$data['title'] = "";
		$data['error']['title']	= "Oops!";
		$data['error']['message'] = "This Module Not Active";
		$data['error']['description']	= "Please contact support team LARGO !";

		$this->load->view('components/errors/index', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
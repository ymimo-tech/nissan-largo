<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load_model('user_model');
		//$this->output->enable_profiler(TRUE);
	}

	public function index(){
		hprotection::login(false);
		if($this->session->userdata('logged_in')){
			redirect('dashboard');
		}else{
			$this->load->view('login_view');
		}
	}

	public function auth(){
		//$this->load_model('user_model');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user = $this->user_model->login($username,$password);
		if($user){

			/*$lm=FALSE;
			$res=$this->user_model->check_lm(array('manager_id'=>$user->id));
			if($res->num_rows()){
				$lm=TRUE;
			}*/
			$userdata = array(
				'user_id'=>$user->user_id,
				'username'=>$user->user_name,
				'name'=>$user->nama,
				'logged_in'=>TRUE,
				'grup_id' => $user->grup_id,
				'nik' =>$user->nik,
				'status_login' => TRUE,
				'kd_roles' => $user->grup_id,

			);

			// $this->load_model('setting_model');
			// $data_temp = $this->setting_model->getData();
			// $userdata = array_merge($userdata,$data_temp);

			$this->session->set_userdata($userdata);
			$json = array('logged'=>1);

		}else{

			$json = array('logged'=>0);

		}

		echo json_encode($json);
	}

	public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class putaway extends MY_Controller {

    private $class_name;
    private $limit = 50;
    private $offset = 0;
	
    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		//$this->access_right->check();
		//$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load_model(array('receiving_model'));
    }

    public function index() {
		
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Put Away List';
        $data['page'] = $this->class_name . '/index';
        
		$this->include_required_file($data);
		
		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
        $data['jsPage'][] = "assets/pages/scripts/putaway/lr-putaway-view.js";
 
		$this->include_other_view_data($data);
		$data['today'] = date('d/m/Y');
		$data['id'] = '';

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Put Away');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

	public function download_gi(){
		$this->load->library('PHPExcel');

		$params = [
			'to' => $this->input->post('to'),
			'from' => $this->input->post('from'),
			'status' => $this->input->post('status')
		];

		$where = '';

		if(!empty($params['to']) && !empty($params['from'])){
			$where .= "and create_at between '".$params['from']."' and '".$params['to']."'";
		}

		if(!empty($params['status'])){
			if($params['status']=='1'){
				$where .= " and vgs.case = 'SUCCESS'";
			}else if($params['status']=='2'){
				$where .= " and vgs.case = 'FAILED'";
				$where .= " or vgs.case = 'NO RESPONSE'";
			}else if($params['status']=='3'){
			}
		}else{
			$where .= " and case = 'NULL'";	
		}

		$sql = "select * from v_gi_status vgs where 1=1 ".$where;

		$result = $this->db->query($sql)->result_array();

		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Report-GI")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Manifest Number')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Packing Number ')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Create At')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Status')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Reasons')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'HIT COUNT')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Finish')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'SKU')->getStyle('H1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(8, 1, 'Outbound Doc')->getStyle('I1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{
            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['manifest_number']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['packing_number']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['create_at']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['case']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['string_agg']);
			$sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['count']);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['finish']);
			$sheet->setCellValueByColumnAndRow(7, $row, $result[$i]['sku']);
			$sheet->setCellValueByColumnAndRow(8, $row, $result[$i]['code']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Report-GI'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

	}

	public function download_gr(){
		$this->load->library('PHPExcel');

		$params = [
			'to' => $this->input->post('to'),
			'from' => $this->input->post('from'),
			'status' => $this->input->post('status')
		];

		$where = '';

		if(!empty($params['to']) && !empty($params['from'])){
			$where .= "and start_putaway between '".$params['from']." 00:00:00' and '".$params['to']." 23:59:00'";
		}

		if(!empty($params['status'])){
			if($params['status']=='1'){
				$where .= " and vgs.case = 'SUCCESS'";
			}else if($params['status']=='2'){
				$where .= " and vgs.case = 'FAILED'";
				$where .= " or vgs.case = 'NO RESPONSE'";
			}else if($params['status']=='3'){
			}
		}else{
			$where .= " and vgs.case = 'NULL'";	
		}

		$sql = "select * from v_gr_status vgs where 1=1 ".$where;

		$result = $this->db->query($sql)->result_array();

		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Report-GR")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'SKU')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'SPBN')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Status')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'HIT COUNT')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Reasons')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Putaway Time')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Inbound Doc')->getStyle('G1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{
            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['sku']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['code']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['case']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['count']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['string_agg']);
			$sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['start_putaway']);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['inbound_code']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Report-GR'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

	}
	
	public function by_receiving_id($id = false) {
		
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Put Away List';
        $data['page'] = $this->class_name . '/index';
        
		$this->include_required_file($data);
		
		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
        $data['jsPage'][] = "assets/pages/scripts/putaway/lr-putaway-view.js";
 
		$this->include_other_view_data($data);
		$data['today'] = date('d/m/Y');
		$data['id'] = $id;

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Put Away');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }
	
	private function include_other_view_data(&$data) {
		$this->load_model('referensi_supplier_model');
        $data['suppliers'] = $this->referensi_supplier_model->options();

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues
    }
	
	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
		
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}
	
	//REQUEST
	public function getList(){
		$result = array();
		
		$params = array(
			'id_receiving'	 	=> filter_var(trim($this->input->post('id_receiving')), FILTER_SANITIZE_STRING),
			'id_po' 			=> filter_var(trim($this->input->post('id_po')), FILTER_SANITIZE_STRING),
			'id_supplier' 		=> filter_var(trim($this->input->post('id_supplier')), FILTER_SANITIZE_NUMBER_INT),
			'from' 				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to' 				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'warehouse'			=> $this->input->post('warehouse'),
			'gr'				=> $this->input->post('gr'),
			'location'			=> $this->input->post('location'),
			'className'			=> $this->class_name
		);
		
		$result = $this->receiving_model->getPutAwayList($params);
		
		echo json_encode($result);
	}

	public function getLocation(){

		if(!empty($this->input->post('query'))){

			$this->db->like('lower(name)',strtolower($this->input->post('query')));

		}

		$this->db
			->select('id as location_id, name as location_code')
			->from('locations');

		$data['result'] = $this->db->get()->result_array();

		echo json_encode($data);

	}

	public function export_excel(){

		if(!empty($post['location'])){
			$this->db->where_in('ird.location_id', [103]);
		}

		$this->db
			->select([
				"COALESCE(NULL,0) as no",
				"inb.code as kd_inbound", "rcv.code as kd_receiving",
				"TO_CHAR(rcv.date, 'DD/MM/YYYY') AS tanggal_receiving",
				"ird.unique_code as kd_unik", "itm.code as kd_barang", "itm.name as nama_barang", "loc.name as loc_name"
			])
			->from('inbound inb')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('receivings rcv','rcv.po_id=inb.id_inbound')
			->join('suppliers spl','spl.id=inb.supplier_id')
			->join('item_receiving ir','ir.receiving_id=rcv.id')
			->join('item_receiving_details ird','ird.item_receiving_id=ir.id')
			->join('items itm','itm.id=ir.item_id')
			->join('locations loc','loc.id=ird.location_id');

		$data = $this->db->get()->result_array();

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="Putaway Report.csv"');
        $fp = fopen('php://output', 'wb');

        $head = ['No','Inbound Doc.','Rec. Doc #','Serial Number','Item Code','Item Name','Location Name'];
        fputcsv($fp, $head);

        $i=1;
        foreach ( $data as $d ) {

            $d['no'] = $i;
            fputcsv($fp, $d);
            $i++;
        }

        fclose($fp);

	}

}
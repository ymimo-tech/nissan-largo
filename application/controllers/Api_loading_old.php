<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_loading extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_loading_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function load()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= 'LOADING';
				$data['status']		= 200;
				$data['message']	= 'Load data for available picking list.';
				$data['response']	= true;
				$data['results']	= $this->api_loading_model->load()->result_array();
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_picking($picking_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $picking_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$picked = $this->api_loading_model->get_shipping($picking_code)->row_array();
				if($picked){
					if($picked['st_shipping'] == 0){
						$data['param']		= $picking_code;
						$data['status']		= 200;
						$data['message']	= $picking_code . ' is available.';
						$data['response']	= true;
						$data['results']	= $this->api_loading_model->retrieve($picking_code)->result_array();
						// $this->api_loading_model->start($picking_code);
					} else{
						$data['param']		= $picking_code;
						$data['status']		= 401;
						$data['message']	= $picking_code . ' is never picked anymore.';
						$data['response']	= false;
					}
				} else{
					$data['param']		= $picking_code;
					$data['status']		= 401;
					$data['message']	= $picking_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get($loading_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $loading_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$shipped = $this->api_loading_model->get($loading_code)->row_array();
				if($shipped){
					if($shipped['st_shipping'] != 1){
						$data['param']		= $loading_code;
						$data['status']		= 200;
						$data['message']	= $loading_code . ' is available.';
						$data['response']	= true;
					} else{
						$data['param']		= $loading_code;
						$data['status']		= 401;
						$data['message']	= $loading_code . ' has been shipped before.';
						$data['response']	= false;
					}
				} else{
					$data['param']		= $loading_code;
					$data['status']		= 401;
					$data['message']	= $loading_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_serial($picking_code = '', $serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $picking_code == '' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$outbound_id = $this->api_loading_model->getOutboundId($picking_code);
				$check_packing = $this->api_loading_model->checkPacking($outbound_id, $serial_number);
				if($check_packing){
					$getPicking_outboundId = $this->api_loading_model->getPicking_outboundId($serial_number);
					$addData = $this->api_loading_model->update_loadingPacking($getPicking_outboundId, $serial_number);

					$result = $this->api_loading_model->get_serial($picking_code, $serial_number)->row_array();
					if($result){
						if($result['st_shipping'] == 0){
							$data['param']		= $serial_number;
							$data['status']		= 200;
							$data['message']	= $serial_number . ' is available.';
							$data['response']	= true;
						} else{
							$data['param']		= $serial_number;
							$data['status']		= 401;
							$data['message']	= $serial_number . ' is not available.';
							$data['response']	= false;
						}
					} else{
						$data['param']		= $serial_number;
						$data['status']		= 401;
						$data['message']	= $serial_number . ' is not valid.';
						$data['response']	= false;
					}

				}else{
					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' is not available.';
					$data['response']	= false;
				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post($params=array())
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST' && !$params){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				if(!$params){
					$params['pl_name']			= $this->input->post('picking_code');
					$params['license_plate']	= $this->input->post('vehicle_police');
					$params['kd_packing']		= $this->input->post('packing_code');
					$params['kd_barang']		= $this->input->post('item_code');
					$params['kd_unik']			= $this->input->post('serial_number');
					$params['loading_time']		= $this->input->post('time');
					$params['uname_pick']		= $this->input->post('user');
					$params['put_time']			= $this->input->post('put_time');
				}


				if($params['pl_name'] == '' || $params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{

					$getPicking_outboundId = $this->api_loading_model->getPicking_outboundId($params['kd_unik']);

					if($this->api_loading_model->post($params)){

						$data['param']		= $params['kd_unik'];
						$data['status']		= 200;
						$data['message']	= 'Data has been updated.';
						$data['response']	= true;

						// $checkFinish = $this->api_loading_model->checkFinish($params['pl_name']);
						$checkFinish = $this->api_loading_model->checkFinish($getPicking_outboundId['code']);
						$this->api_loading_model->start($getPicking_outboundId['code']);
						if($checkFinish){
							$this->api_loading_model->finish($getPicking_outboundId['code']);
							// $data['message']	= 'Loading completed.';
						}


						$allFinish = $this->api_loading_model->allFinish($params['pl_name']);

						if($allFinish == TRUE){
							$data['message']	= 'Loading completed.';
						}

					}else{

						$data['param']		= $params['kd_unik'];
						$data['status']		= 400;
						$data['message']	= $params['kd_unik']. ' not available';
						$data['response']	= false;

					}
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post_serial()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['pl_name']			= $this->input->post('picking_code');
				$params['license_plate']	= $this->input->post('vehicle_police');
				$params['kd_packing']		= $this->input->post('packing_code');
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['loading_time']		= $this->input->post('time');
				$params['uname_pick']		= $this->input->post('user');
				$params['put_time']			= $this->input->post('put_time');

				if($params['pl_name'] == '' || $params['kd_barang'] == '' || $params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{
					$this->api_loading_model->post_serial($params);

					$data['param']		= $params['kd_unik'];
					$data['status']		= 200;
					$data['message']	= 'Data has been updated.';
					$data['response']	= true;

					$this->api_loading_model->start($params['pl_name']);
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_time()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= 'LOADING TIME';
				$data['status']		= 200;
				$data['message']	= 'Get loading time';
				$data['response']	= true;
				$data['time']		= date("Y-m-d H:i:s");
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				$data['time']		= date("Y-m-d H:i:s");
			}
		}
		echo json_encode($data);
	}

	public function getOuterLabel($loadingCode=""){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $loadingCode == ""){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$getOL = $this->api_loading_model->getOuterLabelByPL($loadingCode)->result_array();

				if($getOL){

					$data['param']		= $loadingCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results'] 	= $getOL;

				}else{

					$data['param']		= $loadingCode;
					$data['status']		= 400;
					$data['message']	= '';
					$data['response']	= false;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function postPlateNumber(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$params['plate_number'] = $this->input->post('plate_number');
				$params['loading_code'] = $this->input->post('loading_code');

				if($params['plate_number'] == "" || $params['loading_code'] == ""){

					$data['param']		= null;
					$data['status']		= 400;
					$data['message']	= 'Bad request.';
					$data['response']	= false;

				}else{

					$getPlateNumber = $this->api_loading_model->getPlateNumber($params)->row_array();
					if($getPlateNumber){

						$data['param']		= $params['plate_number'];
						$data['status']		= 200;
						$data['response']	= true;
						$data['message']	= 'Plate Number is available!';

					}else{

						$data['param']		= $params['plate_number'];
						$data['status']		= 400;
						$data['response']	= false;
						$data['message']	= 'Plate Number not available!';

					}

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);

	}

	public function finish($shippingCode){
		$checkFinish = $this->api_loading_model->checkFinish($shippingCode);
		if($checkFinish){
			$this->api_loading_model->finish($shippingCode);
		}
	}

	public function LoadingFromWeb($shippingCode){

		if(!empty($shippingCode)){

			$shipping = $this->db->get_where('shippings',['code'=>$shippingCode,'st_shipping'=>0])->row_array();

			print_r($shipping);
			exit;

			if($shipping){

				$serialNumber = $this->db->get_where('item_receiving_details',['shipping_id'=>$shipping['shipping_id']])->result_array();

				if($serialNumber){

					foreach ($serialNumber as $sn) {

						$params['pl_name']			= $shipping['code'];
						$params['license_plate']	= $shipping['license_plate'];
						$params['kd_packing']		= NULL;
						$params['kd_barang']		= NULL;
						$params['kd_unik']			= $this->input->post('serial_number');
						$params['loading_time']		= date('Y-m-d h:i:s');
						$params['uname_pick']		= 'will';
						$params['put_time']			= date('Y-m-d h:i:s');

					}

					print_r($params);
					exit;

					$this->post($params);

					$data['param']		= $shippingCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['message']	= 'Success!';


				}else{

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['response']	= false;
					$data['message']	= 'Empty Serial Number';

				}


			}else{

				$data['param']		= NULL;
				$data['status']		= 400;
				$data['response']	= false;
				$data['message']	= 'Shipping Doc not available';

			}

		}else{

			$data['param']		= NULL;
			$data['status']		= 400;
			$data['response']	= false;
			$data['message']	= 'Empty Shipping Code';

		}

		echo json_encode($data);

	}

	public function get_item()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$loadingCode = $this->input->post('loading_code');

				if($loadingCode){

					$data['param']		= 'LOADING';
					$data['status']		= 200;
					$data['message']	= '';
					$data['response']	= true;
					$data['results']	= $this->api_loading_model->getItem($loadingCode);

				}else{

					$data['param']		= $loadingCode;
					$data['status']		= 400;
					$data['message']	= 'Parameter loading_code cannot be empty.';
					$data['response']	= false;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

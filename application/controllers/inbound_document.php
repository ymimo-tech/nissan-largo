<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class inbound_document extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        // $this->access_right->check();
        // $this->access_right->otoritas('view', true);

		/* Otoritas */
		// $this->access_right->check();
		// $this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Inbound Document';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-inbound-document.js";

        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

        if($this->access_right->otoritas('import')){
            $data['page'][] = 'components/form-set-warehouse';
            $data['jsPage'][] = "assets/pages/scripts/lr-set-warehouse.js";
        }

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

    /* Get Data Select */
    $this->load_model('warehouse_model');
    $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

    $this->load_model('cost_center_model');
    $data['cost_center'] = $this->cost_center_model->options(); // Get Data Warehoues

/*        $source_type = array();
    $mSourceType = $this->db->get('m_source_type')->result_array();
    if($mSourceType){
        foreach ($mSourceType as $mSource) {
            $source_type[$mSource['m_source_type_id']] = $mSource['m_source_type_name'];
        }
    }
*/
    $data['source_type'] = $this->options('source_categories','source_category_id','source_category_name');

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Referensi Barang');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $inbound_document = $this->inbound_document_model->get_data(array('id_inbound_document' => $id))->row_array();

            $status = $this->inbound_document_model->get_inbound_num($id);
            if($status > 0){
                $inbound_document['status'] = 1;
            }else{
                $inbound_document['status'] = 0;
            }

            $warehouses = $this->inbound_document_model->getDocumentWarehouse($id);
            $inbound_document['warehouses'] = array();
            foreach ($warehouses as $wh) {
            	$inbound_document['warehouses'][] = $wh['warehouse_id'];
            }

            $costCenter = $this->inbound_document_model->getDocumentCostCenter($id);
            $inbound_document['cost_center'] = array();
            foreach ($costCenter as $cost) {
                $inbound_document['cost_center'][] = $cost['cost_center_id'];
            }

            $process_result = $inbound_document;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){

        $columns = array(
            2 => 'name'
        );

        $param = '';

        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $inbound_documents = $this->inbound_document_model->data($param)->get();
        $iTotalRecords = $this->inbound_document_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($inbound_documents->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_inbound_document,'delete' => $value->id_inbound_document));

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="1">',
                $_REQUEST['start']+$i,
                $value->inbound_document_name,
                $action
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function proses() {

        $proses_result['success'] = 1;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('inbound_document_name', 'Document Name', 'required|trim');


            if ($this->form_validation->run()) {
                $id = $this->input->post('id');

                $data = array(
                    'code'              => $this->input->post('inbound_document_code'),
                    'name'              => $this->input->post('inbound_document_name'),
                    'separator'         => $this->input->post('separator'),
                    'seq_length'        => $this->input->post('sequence'),
                    'year_type'         => $this->input->post('year'),
                    'month_type'        => $this->input->post('month'),
                    'is_active'         => $this->input->post('status'),
                    // 'm_source_type_id'  => $this->input->post('source_type')
                    'source_category_id' => $this->input->post('source_type'),
                    'is_auto_generate'  => $this->input->post('auto_generate')
             	);

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                $this->db->trans_start();

                if ($id == '') {
                    if ($this->inbound_document_model->create($data)) {

                        $proses_result['add'] = 1;

                        $docId = $this->db->insert_id();

    					/* INSERT LOG */
						$this->access_right->activity_logs('add','Inbound Document');
						/* END OF INSERT LOG */

                    }
                } else {
                    if ($this->inbound_document_model->update($data, $id)) {
                        $proses_result['edit'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Inbound Document');
						/* END OF INSERT LOG */
                    }
                }


                /*
                    Add or Update Warehouse & Cost Center
                */


                    // Create relation of document & warehouse
                    $warehouses     = $this->input->post('warehouse');
                    if(!empty($warehouses)){
                        if($id == ""){
                            $this->inbound_document_model->addDocumentWarehouse($docId, $warehouses);
                        }else{
                            $this->inbound_document_model->updateDocumentWarehouse($id,$warehouses);
                        }
                    }

                    // $costCenters    = $this->input->post('cost_center');
                    // if(!empty($cost_center)){
                    //     if($id == ""){
                    //         $this->inbound_document_model->addDocumentCostCenter($docId, $cost_center);
                    //     }else{
                    //         $this->inbound_document_model->updateDocumentCostCenter($id,$cost_center);
                    //     }
                    // }

                /*
                    End - Add or Update Warehouse & Cost Center
                */

                $this->db->trans_complete();


            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();

        if ($this->access_right->otoritas('delete', true)) {

            $id = $this->input->post("id");
            $result = $this->inbound_document_model->delete($id);

        } else {

            $result['status'] = 'ERR';
			$result['no_delete'] = 1;

        }

        echo json_encode($result);
    }

    public function getInboundDocument(){

        $results = array();

        $params = array(
            'warehouse' => $this->input->post('warehouse')
        );

        $data = $this->inbound_document_model->getDocumentByWarehouse($params);

        if($data->num_rows() > 0){

            $results['data'] = $data->result_array();
            $results['response'] = true;

        }else{

            $results['response'] = false;
            $results['message'] = 'Please create document type first!';

        }

        echo json_encode($results);

    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Inbound Document Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

	    	$this->load->library('import');

	        $path = 'uploads/excel/'. $this->class_name .'/';
	        $upload = $this->import->upload($path);

	        if($upload['status']){

	            $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

	            if(!$arr){

	                $result = $this->import->errorMessage('Wrong Template');

	            }else{

			        $field = [
		                'Document Name'  	=> 'doc_name',
		                'Document Initial'  => 'doc_initial',
		                'Separator'         => 'separator',
		                'Sequence'          => 'sequence',
		                'Year Type'         => 'year',
		                'Month Type'        => 'month',
		                'Status'            => 'status'
			        ];

	                $data = $this->import->toFieldTable($field, $arr);

	                if(!$data){

	                    $result = $this->import->errorMessage('Wrong Template');

	                }else{

	                	$process = $this->inbound_document_model->importFromExcel($data);

	                	if($process){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

	                	}else{

                            $result = $this->import->errorMessage('Please check again your data !');

	                	}

	                }

	            }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }

        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }
}

/* End of file referensi_barang.php */
/* Location: ./application/controllers/referensi_barang.php */

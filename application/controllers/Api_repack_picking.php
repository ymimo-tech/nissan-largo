<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_repack_picking extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_repack_picking_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function loadDocument()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$results = $this->api_repack_picking_model->loadDocument();
				if(count($results) > 0){
					$data['param']				= '-';
					$data['status']				= 200;
					$data['message']			= '-';
					$data['response']			= true;
					$data['resultsDocument']	= $results;
					$data['resultsDetail']		= $this->api_repack_picking_model->loadDetail($results);
				}else{
					$data['param']		= '-';
					$data['status']		= 401;
					$data['message']	= 'There are document to be procesed.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		echo json_encode($data);
	}
	
	public function validSN()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_unik']		= $this->input->post('kd_unik');
				$params['id_kitting']	= $this->input->post('id_kitting');
				$params['kd_barang']	= $this->input->post('kd_barang');

				$results = $this->api_repack_picking_model->validSN($params);
				if(count($results) > 0){
					$data['param']				= '-';
					$data['status']				= 200;
					$data['message']			= 'Serial number is valid.';
					$data['response']			= true;
					$data['resultsSerial']		= $results;
				}else{
					$data['param']		= '-';
					$data['status']		= 401;
					$data['message']	= 'Invalid serial number.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	
	public function postSN()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['id_order_kitting']	= $this->input->post('id_order_kitting');
				$params['id_barang']		= $this->input->post('id_barang');
				$params['kd_unik']			= $this->input->post('kd_unik');
				$params['qty']				= $this->input->post('qty');
				
				$docQty = $this->api_repack_picking_model->getDocQty($params);
				$pickQty = $this->api_repack_picking_model->getPickQty($params);

				$validQty = $docQty - $pickQty;

				if($validQty >= 0){

					if($validQty >= $params['qty']){

						$this->api_repack_picking_model->postSN($params);
						
						$data['param']				= '-';
						$data['status']				= 200;
						$data['message']			= 'Serial number is valid.';
						$data['response']			= true;

						$this->api_repack_picking_model->addTallyQty($params);

					}else{

						$data['param']		= '-';
						$data['status']		= 400;
						$data['message']	= "invalid Qty";
						$data['response']	= false;

					}

				}else{

					$data['param']		= '-';
					$data['status']		= 400;
					$data['message']	= "this material item has been picked.";
					$data['response']	= false;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}

		}

		echo json_encode($data);

	}
	
	public function deleteSN()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['id_order_kitting']	= $this->input->post('id_order_kitting');
				$params['id_barang']		= $this->input->post('id_barang');
				$params['kd_unik']			= $this->input->post('kd_unik');
				$params['qty']				= $this->input->post('qty');
				
				$this->api_repack_picking_model->deleteSN($params);
				
				$data['param']				= '-';
				$data['status']				= 200;
				$data['message']			= 'Serial number has been deleted.';
				$data['response']			= true;
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}


	public function getItemPicked(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['id_order_kitting']	= $this->input->post('id_order_kitting');
				$params['kd_barang']	= $this->input->post('kd_barang');
				$results = $this->api_repack_picking_model->itemPicked($params);
				if(count($results) > 0){

					$data['param']				= '-';
					$data['status']				= 200;
					$data['message']			= 'Serial number is valid.';
					$data['response']			= true;
					$data['resultsSerial']		= $results;

				}else{

					$data['param']		= '-';
					$data['status']		= 401;
					$data['message']	= 'Invalid serial number.';
					$data['response']	= false;

				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
}

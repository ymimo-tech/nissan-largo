<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_active_stock extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_active_stock_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function get($serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$serialize = $this->api_active_stock_model->get($serial_number)->result_array();
				$serialize_pl = $this->api_active_stock_model->get_pl($serial_number)->result_array();
				if($serialize||$serialize_pl){
					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= 'Data has been retrieved.';
					$data['response']	= true;
					if($serialize){
						$data['results']	= $serialize;
					}else{
						$data['results']	= $serialize_pl;
					}
				} else{
					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' unrecognized.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_item($item_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $item_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$serialize = $this->api_active_stock_model->get_item($item_code)->result_array();
				if($serialize){
					$data['param']		= $item_code;
					$data['status']		= 200;
					$data['message']	= 'Data has been retrieved.';
					$data['response']	= true;
					$data['quantity']	= $this->api_active_stock_model->retrieve_item($item_code)->result_array();
				} else{
					$data['param']		= $item_code;
					$data['status']		= 401;
					$data['message']	= $item_code . ' unrecognized.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_history($serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$serialize = $this->api_active_stock_model->get($serial_number)->result_array();
				$serialize_pl = $this->api_active_stock_model->get_pl($serial_number)->result_array();
				if($serialize||$serialize_pl){
					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= 'Data has been retrieved.';
					$data['response']	= true;
					$data['history']	= $this->api_active_stock_model->history($serial_number)->result_array();
				} else{
					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' unrecognized.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

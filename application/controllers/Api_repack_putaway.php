<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_repack_putaway extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_repack_putaway_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function checkserial()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_unik']		= $this->input->post('serial_number');
				$params['time']			= $this->input->post('time');
				$params['user_name']	= $this->input->post('user');
				
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_unik']		= $this->input->post('serial_number');
				$params['time']			= $this->input->post('time');
				$params['user_name']	= $this->input->post('user');
				$cek_lp = $this->api_repack_putaway_model->get_lp($params['kd_unik'])->row_array();
				$serialized = $this->api_repack_putaway_model->get($params['kd_unik'])->row_array();
				if($serialized){
					if(($serialized['loc_id'] == 100||$serialized['loc_id'] == 103)){
						$data['param']			= $params['kd_unik'];
						$data['status']			= 200;
						$data['message']		= $params['kd_unik'] . ' is available.';
						$data['response']		= true;
						$data['results']		= $this->api_repack_putaway_model->retrieve($params['kd_unik'])->result_array();
						$params['loc_id_old']	= $serialized['loc_id'];
					} else{
						$data['param']		= $params['kd_unik'];
						$data['status']		= 401;
						$data['message']	= $params['kd_unik'] . ' is not available.';
						$data['response']	= false;
					}
				}elseif($cek_lp){
					if($cek_lp['st_receiving'] == 1){
						$data['param']			= $params['kd_unik'];
						$data['status']			= 200;
						$data['message']		= $params['kd_unik'] . ' is available.';
						$data['response']		= true;
						$data['results']		= $this->api_repack_putaway_model->retrieve_lp($params['kd_unik'])->result_array();
						$params['loc_id_old']	= $cek_lp['loc_id'];
					}else{
						$data['param']		= $params['kd_unik'];
						$data['status']		= 401;
						$data['message']	= $params['kd_unik'] . ' is not available.';
						$data['response']	= false;
					}
				}else{
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= $params['kd_unik'] . ' is not available.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_location($location = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $location == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_repack_putaway_model->get_location($location)->row_array();
				if($result){
					$data['param']			= $location;
					$data['status']			= 200;
					$data['message']		= $location . ' is available.';
					$data['response']		= true;
				} else{
					$data['param']		= $location;
					$data['status']		= 401;
					$data['message']	= $location . ' is not available.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_location_serial($location = '', $serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $location == '' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_repack_putaway_model->get_location_serial($location,$serial_number);
				//$result_lp = $this->api_repack_putaway_model->get_location_serial_lp($location,$serial_number);
				if($result){
					$cek_kategori = $this->api_repack_putaway_model->cek_kategori($location,$serial_number);
					if($cek_kategori){
						$data['param']			= $location;
						$data['status']			= 200;
						$data['message']		= $location . ' is available.';
						$data['response']		= true;
					}else{
						$data['param']		= $location;
						$data['status']		= 401;
						$data['message']	= 'Invalid Location/Item Category.';
						$data['response']	= false;
					}
				}else{
					$data['param']		= $location;
					$data['status']		= 401;
					$data['message']	= 'Wrong location type scanned.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['loc_name']			= $this->input->post('location');
				$params['putaway_time']		= $this->input->post('time');
				$params['user_name']		= $this->input->post('user');
				$params['pick_time']		= $this->input->post('pick_time');
				$params['user_pick']		= $this->input->post('user_pick');

				if($params['kd_barang'] == '' || $params['kd_unik'] == ''){

					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;

				}else{
					
					$this->api_repack_putaway_model->post($params);

					$data['param']		= $params['kd_unik'];
					$data['status']		= 200;
					$data['message']	= $params['kd_unik'] . ' has been stored to ' . $params['loc_name'] . '.';
					$data['response']	= true;
					
					$count_putaway		= $this->api_repack_putaway_model->count_putaway($params);
					if($count_putaway){

						$this->api_repack_putaway_model->finish($count_putaway['id_order_kitting']);

						// Send Stock to Staging
				        // $this->load_model('api_sync_model');
				        // $this->api_sync_model->sync_qty();

					}

				}			
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post_cancel()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');

				if($params['kd_barang'] == '' || $params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{
					$this->api_repack_putaway_model->post_cancel($params);

					$data['param']		= $params['kd_unik'];
					$data['status']		= 200;
					$data['message']	= "Cancelation has been success.";
					$data['response']	= true;
					
				}			
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

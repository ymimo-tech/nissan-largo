<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class tray_list extends MY_Controller {
    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        // hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array('picking_list_model','tray_list_model','api_setting_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        hprotection::login();

        $data['title'] = '<i class="icon-plus-sign-alt"></i> Delivery Note';
        $data['page'] = $this->class_name . '/index';

		$this->load->helper('diff_helper');

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/tray_list/lr-picking-list-view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			/*$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);*/
		}

        //$this->include_other_view_data_filter($data);
		$data['today'] = date('d/m/Y');
		$data['widget'] = $this->picking_list_model->getWidget();
		$data['status']	= $this->picking_list_model->getStatus();

		/* Begin Andri */
		// var_dump(secsToTime($data['widget']['average']));
		$data['widget']['average'] = secsToTime($data['widget']['average']);
		/* End Andri */

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Outbound');
        /* END OF INSERT LOG */

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

    public function export_dn(){
        $this->load->library('PHPExcel');

		$params = array(
			'pl_id'		=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_STRING),
			'destination'		=> filter_var(trim($this->input->post('destination')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'status'		=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_STRING)
		);

		$result = $this->tray_list_model->export_dn($params);
        // dd($result);
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("DN-Export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'DN Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'DN Date')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Picking Code')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'DN Qty')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Priority')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Sales Order')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Customer')->getStyle('F1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['dn_name']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['created_at']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['name']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['qty']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['m_priority_name']);
			$sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['code']);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['destination_name']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="DN-Export'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

    public function getTrayLocation(){
        $result = array();
		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);
		$result = $this->tray_list_model->getTrayLocation($params);
		echo json_encode($result);
    }

    public function printTrayList(){
        $data = array();

		$params = array(
			'dn_id'	=> $this->input->post('dn_id')
		);

        // dd($params);
		
        
		$data['page'] = $this->class_name . '/picking_list_report';
		// $data['items'] = $this->picking_list_model->getItems($params);
		$data['pick'] = $this->tray_list_model->printhead($params);
        $data['tray'] = $this->tray_list_model->printitem($params);
		$tray = $this->load->view($data['page'].'_tray', $data, true);
        $path = "tray_list.pdf";
        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load('picking_list');
        $pdf->addPage('P');
        $pdf->writeHTML($tray);
		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)
		$pdf->Output($path, 'I'); // save to file because we can
    }

    public function printTrayListBulk(){
        $data = array();

        $dn_id = json_decode($this->input->post('dn_id'));
        // dd(json_decode($dn_id));

        $this->load->library('pdf');
        $pdf = $this->pdf->load('picking_list');

        for($i=0;$i<count($dn_id);$i++){
            $params = array(); 
            $params = array(
                'dn_id' => $dn_id[$i]
            );

            $data['page'] = $this->class_name . '/picking_list_report';
            // $data['items'] = $this->picking_list_model->getItems($params);
            $data['pick'] = $this->tray_list_model->printhead($params);
            $data['tray'] = $this->tray_list_model->printitem($params);
            $tray = $this->load->view($data['page'].'_tray', $data, true);
            ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)
            
            $pdf->addPage('P');
            $pdf->writeHTML($tray);
        }
        
        $path = "tray_list.pdf";
        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)
		$pdf->Output($path, 'I'); // save to file because we can
    }

    public function create(){
        $data = array();

        $params = array(
            'location_id' => $this->input->post('location_id')
        );

        $tray = $this->tray_list_model->create($params);
		echo json_encode($tray);
    }

    public function add($id_dn){
    }
	
	public function complete(){
		$data = array();

		$params = array(
			'dn_id'	=> $this->input->post('dn_id')
		);
		
		$status = $this->tray_list_model->complete($params);
		
		echo json_encode($status);
	}

    public function completeTrayName(){
		$data = array();

		$params = array(
			'dn_name'	=> $this->input->post('dn_name')
		);
		
		$status = $this->tray_list_model->complete_tray_name($params);
		
		echo json_encode($status);
	}

    public function getDNList(){
        $result = [];
        $params = [
            'pl_id' => $this->input->post('pl_id'),
            'status' => $this->input->post('status'),
            'dn' => $this->input->post('dn'),
            'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING)
        ];
        $result = $this->tray_list_model->getDNList($params);

		echo json_encode($result);
    }

    public function getStatus(){
        $data = [
            array('pl_id'=>0,'pl_name'=>'OPEN'),
            array('pl_id'=>1,'pl_name'=>'COMPLETE')
        ];
        echo json_encode($data);
    }

    public function get_document(){

		$data['param']		= 'DN';
		$data['status']		= 200;
		$data['response']	= true;
		$data['message']	= 'Get Document for DN.';
		$data['results']	= $this->tray_list_model->get_document();

        echo json_encode($data);
    }

    public function get_document_Item($params = []){
        $params = [
            'name' => $this->input->post('dn_name')
        ];

        $data['param']		= 'DN';
		$data['status']		= 200;
		$data['response']	= true;
		$data['message']	= 'Get Item for DN.';
		$data['results']	= $this->tray_list_model->get_document_item($params);

        echo json_encode($data);
    }
	
	public function delete(){
		$params = [
			'dn_id' => $this->input->post('dn_id')
		];
		
		$delete = $this->tray_list_model->delete($params);
		
		echo json_encode($delete);
	}

    public function free_tray($params = []){
        $params = [
            'tray_name' => $this->input->post('tray_name')
        ];

        $status = $this->tray_list_model->free_tray($params);
        if($status['status'] == '200'){
            $data['param']		= 'DN';
            $data['status']		= 200;
            $data['response']	= true;
            $data['message']	= 'Tray has been free';
            $data['results']	= $status;
        }else{
            $data['param']		= 'DN';
            $data['status']		= 401;
            $data['response']	= false;
            $data['message']	= 'Please Check Again!';
            $data['results']	= $status;
        }

        echo json_encode($data);
    }
}
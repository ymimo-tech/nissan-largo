<?php

/**
 * Description of referensi_lokasi_type
 *
 * @author SANGGRA HANDI
 */
class referensi_lokasi_type extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Location Type Info';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        //$data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-lokasi-type.js";

        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

		$data['location_type'] = array(
			'BINLOC'		=> 'BINLOC',
			'INBOUND'		=> 'INBOUND',
			'OUTBOUND'		=> 'OUTBOUND',
			'QC'			=> 'QC',
			'NG'			=> 'NG'
		);

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Referensi Lokasi');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

	public function export(){
		$this->load->library('PHPExcel');

		$params = array();

		$result = $this->referensi_lokasi_type_model->getLocation($params);
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Master-Location Area")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Location Area Name')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Location Area Description')->getStyle('B1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['loc_type_name']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['loc_type_desc']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Master-Location Area'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

	public function getQtyPrint(){
		$result = array();

		$result = $this->referensi_lokasi_type_model->getQtyLocation();

		echo json_encode($result);
	}

	public function printZpl(){
		$result = array();
		$this->load->library('mimoclientprint');
		$this->load->helper('print_helper');

		$params = $this->input->post('params');
		$cmd = '';
		$i = 1;

		if($params == 'multiple'){

			$location = $this->referensi_lokasi_type_model->getLocation();
			$len = count($location);

			foreach($location as $row){
				$cmd .= getPrintTemplate($row['loc_type_name']);
				/*
				$cmd .= '^XA
						^MMT
						^PW318
						^LL0239
						^LS0
						^BY4,3,133^FT48,159^BCN,,N,N
						^FD>:'.$row['loc_type_name'].'^FS
						^FT147,203^A0N,39,38^FH\^FD'.$row['loc_type_name'].'^FS
						^PQ1,0,1,Y^XZ';
				*/
				/*
				$cmd .= "^XA";
                if($i < $len){
					$cmd .= "^XB";
                }
                $cmd .= "^MMA
						^PW264
						^LL0144
						^LS0
						^BY3,3,66^FT58,84^BCN,,Y,N
						^FD>:".$row['loc_type_name']."^FS
						^PQ1,0,1,Y
						~JSB
						^XZ";
                $i++;
				*/
            }

		}else if($params == 'partial'){

			$params = array();
			$params['loc_type_id'] = $this->input->post('loc_type_id');

			$location = $this->referensi_lokasi_type_model->getLocationById($params);

			foreach($location as $row){
				$cmd .= getPrintTemplate($row['loc_type_name']);
            }

		}else{

			$code = trim($this->input->post('code'));

			$cmd .= getPrintTemplate($code);

			/*
			$cmd .= '^XA
					^MMT
					^PW318
					^LL0239
					^LS0
					^BY4,3,133^FT48,159^BCN,,N,N
					^FD>:'.$code.'^FS
					^FT147,203^A0N,39,38^FH\^FD'.$code.'^FS
					^PQ1,0,1,Y^XZ';
			*/
			/*
			$cmd .= '^XA
					^XB
					^MMA
					^PW264
					^LL0144
					^LS0
					^BY3,3,66^FT58,84^BCN,,Y,N
					^FD>:'.$code.'^FS
					^PQ1,0,1,Y
					~JSB
					^XZ';
			*/
		}

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);

		echo json_encode($result);
	}

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $lokasi = $this->referensi_lokasi_type_model->get_data(array('a.id' => $id))->row_array();
            $process_result = $lokasi;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){

        $columns = array(
            2 => 'name',
            3 => 'description'
        );

        $param = array();

        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $lokasi = $this->referensi_lokasi_type_model->data($param)->get();
        $iTotalRecords = $this->referensi_lokasi_type_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($lokasi->result() as $value){
            //---------awal button action---------
            $action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			/*if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print" data-id="'.$value->loc_type_id.'"><i class="fa fa-print"></i> Print</a>';
				$action .= '</li>';
            }*/

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit" data-id="'.$value->loc_type_id.'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete" data-id="'.$value->loc_type_id.'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';
            //$action = $this->buttons->actions(array('edit'=>$value->loc_type_id,'delete' => $value->loc_type_id));

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="'.$value->loc_type_id.'">',
                $_REQUEST['start']+$i,
                $value->loc_type_name,
                $value->loc_type_desc,
                $action
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function proses() {
        $proses_result['success'] = 1;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('loc_type_name', 'Nama Lokasi', 'required|trim');
            //$this->form_validation->set_rules('loc_type_desc', 'Deskripsi Lokasi', 'required|trim');


            if ($this->form_validation->run()) {
                $id = $this->input->post('id');

                $data = array(
                    'loc_type_name'	=> $this->input->post('loc_type_name'),
                    'loc_type_desc' 	=> $this->input->post('loc_type_desc'),
                );

				$locType = trim($this->input->post('loc_type'));
				if(!empty($locType))
					$data['loc_type'] = $locType;

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->referensi_lokasi_type_model->create($data)) {
                        $proses_result['add'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Referensi Lokasi');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->referensi_lokasi_type_model->update($data, $id)) {
                        $proses_result['edit'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Referensi Lokasi');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $processResult = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            $processResult = $this->referensi_lokasi_type_model->delete($id);
        } else {
            $processResult['status'] = 'ERR';
			$processResult['message'] = 'You dont have authority to delete data';
        }
        echo json_encode($processResult);
    }



}

/* End of file referensi_lokasi_type.php */
/* Location type: ./application/controllers/referensi_lokasi_type.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_license_plating extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_license_plating_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function validateSN()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_unik']	= $this->input->post('kd_unik');
				$isValid			= $this->api_license_plating_model->validateSN($params['kd_unik']);
				if($isValid){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 200;
					$data['message']	= $params['kd_unik'] . ' is valid serial number.';
					$data['response']	= true;
				}else{
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= $params['kd_unik'] . ' is not valid serial number.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	
	public function validatePC()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_parent']	= $this->input->post('kd_parent');
				$isValid				= $this->api_license_plating_model->validatePC($params['kd_parent']);
				if($isValid){
					$data['param']		= $params['kd_parent'];
					$data['status']		= 200;
					$data['message']	= $params['kd_parent'] . ' is valid parent code.';
					$data['response']	= true;
				}else{
					$data['param']		= $params['kd_parent'];
					$data['status']		= 401;
					$data['message']	= $params['kd_parent'] . ' is not valid parent code.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	
	public function submit()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$params = array(
									'kd_unik'	=> $this->input->post('kd_unik'),
									'kd_parent'	=> $this->input->post('kd_parent')
								);
				$process = $this->api_license_plating_model->submit($params);
				
				if($process['status']){

					$data['param']		= '-';
					$data['status']		= 200;
					$data['message']	= 'Data has been submited.';
					$data['response']	= true;

				}else{

					$data['param']		= '-';
					$data['status']		= 200;
					$data['message']	= 'Data has been submited.';
					$data['response']	= true;

				}

			} else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}

		}

		echo json_encode($data);
	}

	public function checkparentcode($parent_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $parent_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_license_plating_model->checkparentcode($parent_code);
				if(count($result) > 0){
					$data['param']		= $parent_code;
					$data['status']		= 401;
					$data['message']	= 'Invalid parent code.';
					$data['response']	= false;
					$data['pc']			= '';
				} else{
					$data['param']		= $parent_code;
					$data['status']		= 200;
					$data['message']	= 'OK.';
					$data['response']	= true;
					$data['pc']			= $parent_code;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

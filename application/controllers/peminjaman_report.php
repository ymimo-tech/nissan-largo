<?php 
class peminjaman_report extends CI_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);
		
		// Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load->model(array('peminjaman_model'));
        $this->load->library("randomidgenerator");
    }
	
	public function index(){
		$data['title'] = '<i class="icon-plus-sign-alt"></i> Peminjaman Report';
        $data['page'] = 'peminjaman/peminjaman_report';
		
		$this->include_required_file($data);
		
        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/peminjaman/lr-peminjaman-report.js";
		$data['today'] = date('d/m/Y');
		
		$data['customer'] = $this->peminjaman_model->getCustomer();
         
        /* INSERT LOG */
        $this->access_right->activity_logs('view','Peminjaman Report');
        /* END OF INSERT LOG */
        
        $this->load->view('template', $data);
	}
	
	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}
	
	//REQUEST
	public function getList(){
		$result = array();
		
		$params = array(
			'id_customer'		=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_NUMBER_INT),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING)
		);
		
		$result = $this->peminjaman_model->getReportList($params);
		
		echo json_encode($result);
	}
}
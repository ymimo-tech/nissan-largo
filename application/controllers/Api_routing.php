<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Api_routing extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load_model('api_setting_model');
		$this->load_model('api_routing_model');
  }

  public function get_serial($serial_number = '')
  {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$check_serial_number = $this->api_routing_model->check_serial_number($serial_number);
				if($check_serial_number){
							$data['param']		= $serial_number;
							$data['status']		= 200;
							$data['message']	= $serial_number . ' is available.';
							$data['response']	= true;
				}else{
					    $data['param']		= $serial_number;
				      $data['status']		= 401;
					    $data['message']	= $serial_number . ' is not available.';
					    $data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

  public function post($params=array())
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST' && !$params){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				if(!$params){
					$params['routing_code']	= $this->input->post('routing_code');
					$params['kd_unik']			= $this->input->post('serial_number');
					$params['uname_pick']		= $this->input->post('user');
				}


				if($params['routing_code'] == '' || $params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{

          $check_true_routing = $this->api_routing_model->check_serial_number($params['kd_unik']);
          if($check_true_routing){
            $post_data = $this->api_routing_model->post($params);
            if($post_data){
                $data['param']		= $params['kd_unik'];
                $data['status']		= 200;
                $data['message']	= 'Data has been updated.';
                $data['response']	= true;
              }else{
				$data['param']		= $params['kd_unik'];
				$data['status']		= 401;
				$data['message']	= 'Failed to update data.';
				$data['response']	= false;
			  }

          }else{

					  	$data['param']		= $params['kd_unik'];
					  	$data['status']		= 400;
					  	$data['message']	= $params['kd_unik']. ' not available';
					  	$data['response']	= false;

					  }
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

  public function get_item()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
					$data['param']		= 'ROUTING';
					$data['status']		= 200;
					$data['message']	= '';
					$data['response']	= true;
					$data['results']	= $this->api_routing_model->getItem();
			}else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		echo json_encode($data);
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_transfer extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load_model('api_setting_model');
        $this->load_model('api_transfer_model');
    }

    public function index()
    {
        $data['param']      = null;
        $data['status']     = 400;
        $data['message']    = 'Bad request.';
        $data['response']   = false;

        echo json_encode($data);
    }

    public function get()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $params['kd_unik']      = $this->input->post('serial_number');
                $params['time']         = $this->input->post('time');
                $params['user_name']    = $this->input->post('user');
                $serialized = $this->api_transfer_model->get($params['kd_unik'])->row_array();
                $serialized_pl = $this->api_transfer_model->get_pl($params['kd_unik'])->row_array();
                if($serialized||$serialized_pl){
                    if(!empty($serialized['loc_id'])||!empty($serialized_pl['loc_id'])){                        

                        if(!empty($serialized['loc_id'])){

                            $data['results']        = $this->api_transfer_model->retrieve($params['kd_unik'])->result_array();
                            $params['loc_id_old']   = $serialized['loc_id'];
                            $process = $this->api_transfer_model->set($params,$serialized);

                        }else{

                            $data['results']        = $this->api_transfer_model->retrieve_pl($params['kd_unik'])->result_array();
                            $params['loc_id_old']   = $serialized_pl['loc_id'];
                            $process = $this->api_transfer_model->set_pl($params,$serialized_pl);

                        }

                        if($process){
    
                            $data['param']          = $params['kd_unik'];
                            $data['status']         = 200;
                            $data['message']        = $params['kd_unik'] . ' is available.';
                            $data['response']       = true;

                        }else{

                            $data['param']          = $params['kd_unik'];
                            $data['status']         = 200;
                            $data['message']        = $params['kd_unik'] . ' is available.';
                            $data['response']       = true;

                        }

                    } else{

                        $data['param']      = $params['kd_unik'];
                        $data['status']     = 401;
                        $data['message']    = $params['kd_unik'] . ' is not available.';
                        $data['response']   = false;

                    }
                } else{
                    $data['param']      = $params['kd_unik'];
                    $data['status']     = 401;
                    $data['message']    = $params['kd_unik'] . ' is not available.';
                    $data['response']   = false;
                }
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function get_location($location = '')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $location == ''){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $result = $this->api_transfer_model->get_location($location)->row_array();
                if($result){
                    $data['param']          = $location;
                    $data['status']         = 200;
                    $data['message']        = $location . ' is available.';
                    $data['response']       = true;
                } else{
                    $data['param']      = $location;
                    $data['status']     = 401;
                    $data['message']    = $location . ' is not available.';
                    $data['response']   = false;
                }
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function post()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $params['kd_unik']          = $this->input->post('serial_number');
                $params['loc_name_old']     = $this->input->post('loc_name_old');
                $params['user_pick']        = $this->input->post('user_pick');
                $params['pick_time']        = $this->input->post('pick_time');
                $params['loc_name_new']     = $this->input->post('loc_name_new');
                $params['user_put']         = $this->input->post('user_put');
                $params['put_time']         = $this->input->post('put_time');

                if($params['kd_unik'] == ''){

                    $data['param']      = $params['kd_unik'];
                    $data['status']     = 401;
                    $data['message']    = 'Failed to update data.';
                    $data['response']   = false;

                } else{

                    $process = $this->api_transfer_model->post($params);

                    if($process['status']){

                        $data['param']      = $params['kd_unik'];
                        $data['status']     = 200;
                        $data['message']    = $params['kd_unik'] . ' has been stored to ' . $params['loc_name_new'] . '.';
                        $data['response']   = true;

                    }else{

                        $data['param']      = $params['kd_unik'];
                        $data['status']     = 400;
                        $data['message']    = $process['message'];
                        $data['response']   = false;

                    }

                }           
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function cancel()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $params['kd_unik']          = $this->input->post('serial_number');
                $params['loc_name_old']     = $this->input->post('loc_name_old');
                $params['user_pick']        = $this->input->post('user_pick');
                $params['pick_time']        = $this->input->post('pick_time');
                $params['loc_name_new']     = $this->input->post('loc_name_new');
                $params['user_put']         = $this->input->post('user_put');
                $params['put_time']         = $this->input->post('put_time');

                if($params['kd_unik'] == ''){
                    $data['param']      = $params['kd_unik'];
                    $data['status']     = 401;
                    $data['message']    = 'Failed to update data.';
                    $data['response']   = false;
                } else{
                    $this->api_transfer_model->cancel($params);
                    $data['param']      = $params['kd_unik'];
                    $data['status']     = 200;
                    $data['message']    = $params['kd_unik'] . ' has been canceled.';
                    $data['response']   = true;
                }           
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function getOuterLabelItem()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $params['parent_code']     = $this->input->post('outer_label');

                if($params['parent_code'] == ''){
                    $data['param']      = $params['parent_code'];
                    $data['status']     = 400;
                    $data['message']    = 'Outer Label not available';
                    $data['response']   = false;
                } else{

					$datas = $this->api_transfer_model->getOuterLabelItem($params);
					if($datas){
						$data['param']		= $params['parent_code'];
						$data['status']		= 200;
						$data['response']	= true;
						$data['message']	= $datas;
					} else{
						$data['param']		= $params['parent_code'];
						$data['status']		= 400;
						$data['response']	= false;
						$data['message']	= 'Outer Label not available';
					}

                }           
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

}

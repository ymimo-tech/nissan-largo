<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require "api/Api.php";
class api_integration extends Api {


	/* Static Auth */

	private $username 		= "largodms";
	private $authorization 	= '5m!9-fr99$e$$m#g';

	/* End - Static Auth */

	private $payload = array(
		"params"	=> NULL,
		"response"	=> FALSE,
		"message"	=> "",
		"d"			=> array(),
		"error"		=> array()
	);

	/* ------------------------------------------------------------------------------------- */

	function __construct() {

		parent::__construct();

		//$this->auth->static_auth($this->username,$this->authorization);

		$this->load_model('api_integration_model');
		$this->load_model('api_setting_model');

	}

	public function test(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		var_dump($auth);
	}

	public function syncProduct(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'syncProduct';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncProduct';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['id_item_erp'] = !empty($data[$i]['Id']) ? $data[$i]['Id'] : 0;
					$param['name'] = $data[$i]['Item Name'];
					$param['sku'] = $data[$i]['Item Code'];
					$param['code'] = str_replace('-','',$data[$i]['Item Code']);
					$param['oem']	= $data[$i]['OEM'];
					$param['packing_description'] = $data[$i]['Packing Description'];
					$param['has_batch'] = strtolower($data[$i]['Have Batch']) == 'no' ? 0 : 0;
					$param['category_id'] = $data[$i]['Item Category'];
					$param['has_qty']	= strtolower($data[$i]['Multi Qty']) == 'yes' ? 1 : 0;
					$param['def_qty']	= $data[$i]['Default Qty'];
					$param['weight'] = !empty($data[$i]['Weight']) ? $data[$i]['Weight'] : 0;
					$param['width'] = !empty($data[$i]['Width']) ? $data[$i]['Width'] : 0;
					$param['height'] = !empty($data[$i]['Height']) ? $data[$i]['Height'] : 0;
					$param['length'] = !empty($data[$i]['Length']) ? $data[$i]['Length'] : 0;
					$param['unit_id'] = $data[$i]['Unit Of Measurement'];
					$param['has_expdate'] = strtolower($data[$i]['Have Expired Date']) == 'no' ? 0 : 0;
					$param['shipment_id'] = strtolower($data[$i]['Picking Strategy']) == 'fifo' ? 1 : 2;
					$param['fifo_period'] = $data[$i]['Picking Period'];
					$param['minimum_stock'] = !empty($data[$i]['Minimum Replenishment Stock']) ? $data[$i]['Minimum Replenishment Stock'] : 0 ;
					$param['min_reorder'] = !empty($data[$i]['Minimum Reorder Stock']) ? $data[$i]['Minimum Reorder Stock'] : 0;
					$param['warehouse'] = $data[$i]['Warehouse'];
					$param['quarantine'] = strtolower($data[$i]['Quarantine']) == 'yes' ? 1 : 0;
					$param['quarantineduration'] = !empty($data[$i]['Quarantine Duration']) ? $data[$i]['Quarantine Duration'] : 0;
					$param['autorelease'] = strtolower($data[$i]['Autorelease']) == 'yes' ? 1 : 0;
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';
					$param['routing'] = !empty($data[$i]['Special Movement Code']) ? $data[$i]['Special Movement Code'] : '';


					// $param['uom_id'] = $data[$i]['uom_id'][0];
					// $param['uom'] = $data[$i]['uom_id'][1];
					// $param['convert_qty_1'] = '';
					// $param['convert_qty_unit_id_1'] = '';

					// if($data[$i]['uom_ids']) {
					// 	//if($data[$i]['uom_ids']['uom_type'] == 'bigger') {
					// 		$param['convert_qty_1'] = $data[$i]['uom_ids'][0]['factor_product'];
					// 		$param['convert_qty_unit_id_1'] = $data[$i]['uom_ids'][0]['name'];
					// 	//}
					// }

					$insert = $this->api_integration_model->insert_product($param);


				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncProductSubstitute(){
		ini_set('MAX_EXECUTION_TIME', '-1');
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'syncProductSubstitute';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncProductSubstitute';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['item_code'] = $data[$i]['item_code'];
					$param['subs_item_code'] = $data[$i]['subs_item_code'];
					$param['status'] = !empty($data[$i]['status']) ? $data[$i]['status'] : 'ACTIVE';

					$insert = $this->api_integration_model->insert_product_substitute($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncVendor(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'syncVendor';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncVendor';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['id_source_erp'] = !empty($data[$i]['Source Id']) ? $data[$i]['Source Id'] : 0;
					$param['source_code'] = $data[$i]['Source Code'];
					$param['source_name'] = $data[$i]['Source Name'];
					$param['source_address'] = $data[$i]['Source Address'];
					$param['source_city'] = $data[$i]['Source City'];
					$param['source_phone'] =  $data[$i]['Source Phone'];
					$param['source_email'] = $data[$i]['Source Email'];
					$param['source_contact_person'] = $data[$i]['Source Contact Person'];
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					$insert = $this->api_integration_model->insert_vendor($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncDocument(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'syncDocument';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncUser';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['type'] = $data[$i]['document'];
					$param['name'] = $data[$i]['name'];
					// $param['nama'] = $data[$i]['Full Name'];
					// $param['user_password'] = $data[$i]['Password'];
					// $param['grup_id'] = $data[$i]['Role'];
					// $param['warehouse'] = $data[$i]['Warehouse'];

					$insert = $this->api_integration_model->insert_doc($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}


	public function syncUser(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'syncUser';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncUser';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);
		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					// $param['id_users_erp'] = $data[$i]['User Id'];
					$param['nik'] = $data[$i]['User Id'];
					$param['user_name'] = $data[$i]['User Name'];
					$param['nama'] = $data[$i]['Full Name'];
					$param['user_password'] = $data[$i]['Password'];
					$param['grup_id'] = $data[$i]['Role'];
					$param['warehouse'] = $data[$i]['Warehouse'];
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					$insert = $this->api_integration_model->insert_user($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncCustomer(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$log_param['name'] = 'syncCustomer';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncCustomer';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();
		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['id_destination_erp'] = !empty($data[$i]['Destination Id']) ? $data[$i]['Destination Id'] : 0;
					$param['destination_code'] = $data[$i]['Destination Code'];
					$param['destination_name'] = $data[$i]['Destination Name'];
					$param['destination_address'] = $data[$i]['Destination Address'];
					$param['destination_city'] = $data[$i]['Destination City'];
					$param['destination_phone'] =  $data[$i]['Destination Phone'];
					$param['destination_email'] = $data[$i]['Destination Email'];
					$param['destination_contact_person'] = $data[$i]['Destination Contact Person'];
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					$insert = $this->api_integration_model->insert_customer($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncWarehouse(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'syncWarehouse';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncWarehouse';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params['result'][0];

				for($i = 0; $i < count($data); $i++) {
					$param['id'] = $data[$i]['id'];
					$param['name'] = $data[$i]['name'];

					$insert = $this->api_integration_model->insert_warehouse($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncLocation(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'syncLocation';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncLocation';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['id_location_erp'] = !empty($data[$i]['Bin Id']) ? $data[$i]['Bin Id'] : 0;
					$param['name'] = $data[$i]['Bin Name'];
					$param['description'] = $data[$i]['Bin Description'];
					$param['location_type_id'] = $data[$i]['Bin Type'];
					$param['location_category_id'] = $data[$i]['Bin Category'];
					$param['location_area_id'] = $data[$i]['Bin Area'];
					$param['rack'] = $data[$i]['Bin Rack'];
					$param['row'] = $data[$i]['Bin Row'];
					$param['level'] = $data[$i]['Bin Level'];
					$param['bin'] = $data[$i]['Bin Bin'];
					$param['color_code_bin'] = $data[$i]['Bin Color Code Bin'];
					$param['color_code_level'] = $data[$i]['Bin Color Code Level'];
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					$insert = $this->api_integration_model->insert_location($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function postInboundDoc(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$error = array();

		$log_param['name'] = 'postInboundDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/postInboundDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				for($xCount=0;$xCount<count($params);$xCount++) {
					$data = $params[$xCount];
					$param = array();
					// var_dump($params[$i]);

					// $param['id_inbound'] = $data['id'];
					$param['id_inbound_erp'] = !empty($data['id']) ? $data['id'] : 0;
					$param['code'] = $data['number'];
					//$param['date'] = $data['date'];
					$param['date'] = 'NOW()';
					$param['supplier_id'] = $data['source'];
					$param['document_id'] = $data['inbound_type'];
					$param['status_id'] = $data['status'] == 'OPEN' ? 3 : 5;
					$param['user_id'] = $data['user'];
					$param['remark'] = $data['remark'];
					$param['warehouse_id'] = $data['warehouse'];

					// $param['no_pallet_original'] = $data['no_container'];

					for($i = 0; $i < count($data['items']); $i++) {
						$item['item_name'] = $data['items'][$i]['item_code'];
						$check_item_exist = $this->api_integration_model->check_product_by_name($item);
						if($check_item_exist->num_rows() == 0) {
							$this->payload = array(
								"params"	=> "",
								"response"	=> FALSE,
								"message"	=> "Item product code (".$data['items'][$i]['item_code'].") is not exist."
							);
							$error[] = "Item product code (".$data['items'][$i]['item_code'].") is not exist.";
							break;
						}
					}

					if($check_item_exist->num_rows() > 0) {
						$insert_inbound = $this->api_integration_model->insert_inbound($param);
						$inbound_id = $this->db->select('id_inbound')->from('inbound')->where('code', $data['number'])->get()->row_array()['id_inbound'];

						for($i = 0; $i < count($data['items']); $i++) {
							$item_id = $this->db->select('id')->from('items')->where('sku', $data['items'][$i]['item_code'])->get()->row_array()['id'];
							$param['item_id'] = $item_id;
							$param['qty'] = $data['items'][$i]['qty'];
							$param['inbound_id'] = $inbound_id;
							$param['po_line_no'] = $data['items'][$i]['po_line_no'];

							$insert_inbound_item = $this->api_integration_model->insert_inbound_item($param);
						}

						if($insert_inbound) {
							$this->payload = array(
										"params"	=> "",
										"response"	=> TRUE,
										"message"	=> "Insert Inbound Document Success."
									);
						} else {
							$this->payload = array(
									"params"	=> "",
									"response"	=> FALSE,
									"message"	=> "Update Inbound Document Success."
								);
						}
					}
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$this->payload['error'] = $error;

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function postASNDoc(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$error = array();

		$log_param['name'] = 'postASNDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/postASNDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				for($xCount=0;$xCount<count($params);$xCount++) {
					$data = $params[$xCount];
					$param = array();

					// $param['supplier_id'] = $data['COMPANY_CODE']; // SOURCE/VENDOR/SUPPLIER
					$param['id_erp'] = (!empty($data['id'])) ? $data['id'] : 0; // ID ERP
					$param['code'] = $data['BIN_DOC_NO']; // RECEIVING DOC CODE
					
					// $param['date'] = $data['BIN_DOC_DATE']; // RECEIVING DOC DATE
					// $date=date_create($data['BIN_DOC_DATE']);
					$date = date("Y-m-d H:i:s");
					$param['date'] = date("Y-m-d H:i:s"); // RECEIVING DOC DATE
					
					// $param['inbound_doc'] = $data['PO_DOC_NO']; // INBOUND DOC CODE
					// $param['status_id'] = 3;
					// $param['status_id'] = '';

					$param['user_id'] = (!empty($data['user'])) ? $data['user'] : $this->input->get_request_header('User', true); // USER
					$param['case_no'] = $data['SUPPLIER_CASE_NO']; // CASE NO/DELIVERY DOC/NO CONTAINER
					$param['delivery_doc'] = $data['SUPPLIER_INV_NO']; // INVOICE NO
					$param['company_code_erp'] = $data['COMPANY_CODE']; // COMPANY CODE
					$param['warehouse_id'] = $data['WHS_CODE']; // WAREHOUSE CODE

					for($i = 0; $i < count($data['ITEMS']); $i++) {
						$item['item_name'] = $data['ITEMS'][$i]['ITEM_CODE']; // ITEM CODE
						$check_item_exist = $this->api_integration_model->check_product_by_name($item);
						if($data['ITEMS'][$i]['ITEM_CODE'] == $data['ITEMS'][$i]['ORDERED_ITEM_CODE']){
							$item['item_name'] = $data['ITEMS'][$i]['ITEM_CODE']; // ITEM CODE
							$check_item_exist = $this->api_integration_model->check_product_by_name($item);
							if($check_item_exist->num_rows() == 0) {
								$this->payload = array(
									"params"	=> "",
									"response"	=> FALSE,
									"message"	=> "Item product code (".$item['item_name'].") is not exist."
								);
								$error[] = "Item product code (".$item['item_name'].") is not exist.";
								break;
							}
						}else{
							$item['item_name'] = $data['ITEMS'][$i]['ITEM_CODE']; // ITEM CODE
							$check_item_exist = $this->api_integration_model->check_product_by_name($item);
							if($check_item_exist->num_rows() == 0) {
								$this->payload = array(
									"params"	=> "",
									"response"	=> FALSE,
									"message"	=> "Item product code (".$item['item_name'].") is not exist."
								);
								$error[] = "Item product code (".$item['item_name'].") is not exist.";
								break;
							}

							$item['item_name'] = $data['ITEMS'][$i]['ORDERED_ITEM_CODE']; // ITEM CODE
							$check_order_item_exist = $this->api_integration_model->check_product_by_name($item);
							if($check_order_item_exist->num_rows() == 0) {
								$this->payload = array(
									"params"	=> "",
									"response"	=> FALSE,
									"message"	=> "Item product code (".$item['item_name'].") is not exist."
								);
								$error[] = "Item product code (".$item['item_name'].") is not exist.";
								break;
							}
						}
					}

					$inbound_detail = $this->db->get_where('inbound',['code'=>$data['PO_DOC_NO']]);
					if($inbound_detail->num_rows() > 0) {
						if($check_item_exist->num_rows() > 0) {
							$id_inbound = $inbound_detail->row_array()['id_inbound'];

							$insert_receiving = $this->api_integration_model->insert_receiving($param, $id_inbound);
							$id_receiving = $this->db->get_where('receivings',['code'=>$param['code']])->row_array()['id'];

							for($i = 0; $i < count($data['ITEMS']); $i++) {
								$item_id = $this->db->get_where('items',['sku'=>$data['ITEMS'][$i]['ITEM_CODE']])->row_array()['id'];
								$order_item_id = $this->db->get_where('items',['sku'=>$data['ITEMS'][$i]['ORDERED_ITEM_CODE']])->row_array()['id'];
								$check_inbound_item_by_rcv = $this->db->select('id')->from('inbound_item')->where('item_id', $item_id)->where('inbound_id', $id_inbound)->get()->row_array();
								
								if(isset($check_inbound_item_by_rcv['id'])){
									$check_inbound_item_by_rcv = $check_inbound_item_by_rcv['id'];
								}else{
									$check_inbound_item_by_rcv = $this->db->select('id')->from('inbound_item')->where('item_id', $order_item_id)->where('inbound_id', $id_inbound)->get()->row_array();
								}
								
								if(!empty($check_inbound_item_by_rcv)){
									if($data['ITEMS'][$i]['ITEM_CODE'] == $data['ITEMS'][$i]['ORDERED_ITEM_CODE']){
										//dd('masuk '.$data['ITEMS'][$i]['ITEM_CODE'].'    '.$data['ITEMS'][$i]['ORDERED_ITEM_CODE']);
										$item_id = $this->db->get_where('items',['sku'=>$data['ITEMS'][$i]['ITEM_CODE']])->row_array()['id'];
										$param['item_id'] = $item_id;
										$param['qty'] = $data['ITEMS'][$i]['QTY_DO']; // QTY
										$param['rcv_line_no'] = $data['ITEMS'][$i]['BIN_LINE_NO']; // RCV_LINE_NO
										$param['id_receiving'] = $id_receiving;
										$param['id_inbound'] = $id_inbound;
										$param['substitute_id'] = '';

										$insert_asn_item = $this->api_integration_model->insert_asn_item($param);
									}else{
										$is_sub = $this->api_integration_model->is_sub($data['ITEMS'][$i]['ITEM_CODE'],$data['ITEMS'][$i]['ORDERED_ITEM_CODE']);
										//dd($is_sub);
										//dd('masuk '.$data['ITEMS'][$i]['ITEM_CODE'].'    '.$data['ITEMS'][$i]['ORDERED_ITEM_CODE']);
										if($is_sub['id']){
											//$item_id = $this->db->get_where('items',['sku'=>$data['ITEMS'][$i]['ORDERED_ITEM_CODE']])->row_array()['id'];
											$param['item_id'] = $item_id;
											$param['qty'] = $data['ITEMS'][$i]['QTY_DO']; // QTY
											$param['rcv_line_no'] = $data['ITEMS'][$i]['BIN_LINE_NO']; // RCV_LINE_NO
											$param['id_receiving'] = $id_receiving;
											$param['id_inbound'] = $id_inbound;
											$param['substitute_id'] = $is_sub['id'];
											$insert_asn_item = $this->api_integration_model->insert_asn_item($param);
										}else{
											$this->payload = array(
												"params"	=> "",
												"response"	=> FALSE,
												"message"	=> "Item subtitute isn't assigned for product code (".$item['ORDERED_ITEM_CODE'].")"
											);
											$error[] = "Item subtitute isn't assigned for this product code (".$item['ORDERED_ITEM_CODE'].")";
											break;
										}
									}
								}else{
									$this->payload = array(
											"params"	=> "",
											"response"	=> FALSE,
											"message"	=> "Item Receiving (".$data['ITEMS'][$i]['ITEM_CODE'].") is not exist on Inbound Doc (".$data['PO_DOC_NO'].")."
										);
										$error[] = "Item Receiving (".$data['ITEMS'][$i]['ITEM_CODE'].") is not exist on Inbound Doc (".$data['PO_DOC_NO'].").";
										$log_param['id'] = $insert_log;
										$log_param['response'] = json_encode($this->payload);
										$this->api_integration_model->update_log($log_param);
										echo json_encode($this->payload);
										break;
								}
							}

							if($insert_receiving) {
								$this->payload = array(
											"params"	=> "",
											"response"	=> TRUE,
											"message"	=> "Insert ASN Success."
										);
							} else {
								$this->payload = array(
										"params"	=> "",
										"response"	=> TRUE,
										"message"	=> "Update ASN Success."
									);
							}
						}
					} else {
						$this->payload = array(
								"params"	=> "",
								"response"	=> FALSE,
								"message"	=> "Inbound Document (".$data['PO_DOC_NO'].") is not exist. Please insert inbound document first."
							);
							$error[] = "Inbound Document (".$data['PO_DOC_NO'].") is not exist. Please insert inbound document first.";
					}
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}
		$this->payload['error'] = $error;

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function postOutboundDoc($params = array()) {
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'postOutboundDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/postOutboundDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				for($xCount=0;$xCount<count($params);$xCount++) {
					$data = $params[$xCount];
					$param = array();

					if($data['order_type'] == 'E - Emergency Order Type'){
						$prio = 2;
					}else if($data['order_type'] == 'R - Regular Order Type'){
						$prio = 1;
					}

					$param['id_outbound_erp'] = !empty($data['id']) ? $data['id'] : 0;
					$param['code'] = $data['number'];
					$param['date'] = $data['date'];
					$param['shipping_group'] = $data['shipping_group'];
					$param['customer_id'] = 0;
					$param['document_id'] = $data['outbound_type'];
					$param['m_priority_id'] = $prio;
					$param['user_id'] = $data['user'];
					$param['note'] = $data['note'];
					$param['destination_id'] = $data['destination_code'];
					$param['id_warehouse'] = $data['warehouse'];
					$param['ekspedisi'] = $data['no_container'];
					$param['status_id'] = $data['status'] == 'OPEN' ? 3 : 5;
					$param['company_code'] = $data['company_code'];

					for($i = 0; $i < count($data['items']); $i++) {
						$item['item_name'] = $data['items'][$i]['item_code'];
						$check_order_item_exist = $this->api_integration_model->check_product_by_name($item);
						if($check_order_item_exist->num_rows() == 0) {
							$this->payload = array(
								"params"	=> "",
								"response"	=> FALSE,
								"message"	=> "Item product (".$data['items'][$i]['item_code'].") is not exist."
							);
							break;
						}
					}

					if($check_order_item_exist->num_rows() > 0) {
						$insert_outbound = $this->api_integration_model->insert_outbound($param);
						$outbound_id = $this->db->select('id')->from('outbound')->where('code', $data['number'])->get()->row_array()['id'];

						for($i = 0; $i < count($data['items']); $i++) {
							$item_id = $this->db->select('id')->from('items')->where('sku', $data['items'][$i]['item_code'])->get()->row_array()['id'];
							$uom_id = $this->db->select('id')->from('units')->where('code', $data['items'][$i]['uom_id'])->get()->row_array();
							if(!isset($uom_id['id'])){
								$uom_id = $this->db->select('unit_id')->from('items')->where('sku', $data['items'][$i]['item_code'])->get()->row_array()['unit_id'];
							}

							$param['item_id'] = $item_id;
							$param['qty'] = $data['items'][$i]['qty'];
							$param['unit_id'] = $uom_id;
							$param['line_no'] = !empty($data['items'][$i]['so_line_no']) ? $data['items'][$i]['so_line_no'] : $i;
							$param['id_outbound'] = $outbound_id;

							$insert_outbound_item = $this->api_integration_model->insert_outbound_item($param);
						}

						if($insert_outbound) {
							$this->payload = array(
										"params"	=> "",
										"response"	=> TRUE,
										"message"	=> "Insert Outbound Document Success."
									);
						} else {
							$this->payload = array(
									"params"	=> "",
									"response"	=> TRUE,
									"message"	=> "Update Outbound Document Success."
								);
						}
					}
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function postGoodsReceipt($params = array()) {
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);

		$this->api_integration_model->post_receipt($params['kd_receiving'],$params['unique_code']);
	}

	public function postNewGoodsReceipt($params = array()) {
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);

		$this->api_integration_model->post_new_goods_receipt($params['kd_receiving']);
	}

	public function postNewGoodsIssue($params = array()) {
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);

		$this->api_integration_model->post_new_goods_issue($params['manifest_number']);
	}

	public function postGoodsIssue($params = array()){
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);

		$this->api_integration_model->post_picking($params['pl_name'],$params['unique_code']);
	}

	public function cancelInboundDoc(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'cancelInboundDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/cancelInboundDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				$param['id_staging'] = $data['id'];
				$param['code'] = $data['number'];

				$cancel_inbound = $this->api_integration_model->cancel_inbound($param);

				if($cancel_inbound) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Successfuly cancel inbound document (".$data['number'].")."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Cancel PO Failed."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function cancelOutboundDoc(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'cancelOutboundDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/cancelOutboundDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				$param['id_staging'] = $data['id'];
				$param['code'] = $data['number'];

				$cancel_inbound = $this->api_integration_model->cancel_outbound($param);

				if($cancel_inbound) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Successfuly cancel outbound document (".$data['number'].")."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Cancel PO Failed."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_model->update_log($log_param);
		echo json_encode($this->payload);
	}
	
	public function autoCreatePL_old($outbound_id=''){
		$where = '';
		if($outbound_id != ''){
			$checkdest = "select d.destination_id from outbound o
						left join destinations d on d.destination_id = o.destination_id
						where o.id =".$outbound_id;
			$dest = $this->db->query($checkdest)->row_array()['destination_id'];
			if($dest=='10822'){
				$where = " AND outbound_id = ".$outbound_id." and destination_id = 10822";
			}else{
				$where = " AND outbound_id = ".$outbound_id;
			}
		}else{
			$where = " and destination_id != 10822";
		}
		
		$getOutboundItemArea_q = "
			SELECT * FROM (
			SELECT o.id AS outbound_id, o.code AS outbound_code, destination_id, m_priority_id, date, LEFT(i.item_area,1) AS item_area
			FROM outbound o
			JOIN outbound_item oi ON o.id = oi.id_outbound
			JOIN items i ON i.id = oi.item_id
			) a
			WHERE item_area IS NOT NULL ".$where."
			GROUP BY outbound_id, outbound_code, item_area, m_priority_id, destination_id, date
			order by m_priority_id desc, date desc
			;";
		$getOutboundItemArea = $this->db->query($getOutboundItemArea_q);
		$getOutboundItemArea_r = $getOutboundItemArea->result_array();
		
		for($i=0;$i<$getOutboundItemArea->num_rows();$i++){
			$getPL_q = "
				SELECT p.*
				FROM pickings p
				JOIN picking_qty pq ON p.pl_id = pq.picking_id
				JOIN items i ON i.id = pq.item_id
				JOIN picking_list_outbound plo ON plo.pl_id = p.pl_id
				WHERE plo.id_outbound = ".$getOutboundItemArea_r[$i]['outbound_id']." AND LEFT(i.item_area,1) = '".$getOutboundItemArea_r[$i]['item_area']."'
				;";
			$getPL = $this->db->query($getPL_q);
			$getPL_r = $getPL->result_array();
			
			if($getPL->num_rows() == 0){

				$getOutboundItem_q = "
					SELECT o.id AS outbound_id, o.code AS outbound_code, o.date AS outbound_date, o.m_priority_id AS outbound_priority, i.id AS item_id, i.code AS item_code, oi.qty AS item_qty, i.unit_id AS item_unit, LEFT(i.item_area,1) AS item_area
					FROM outbound o
					JOIN outbound_item oi ON o.id = oi.id_outbound
					JOIN items i ON i.id = oi.item_id
					WHERE o.id = ".$getOutboundItemArea_r[$i]['outbound_id']." AND LEFT(i.item_area,1) = '".$getOutboundItemArea_r[$i]['item_area']."'
					;";
				$getOutboundItem = $this->db->query($getOutboundItem_q);
				$getOutboundItem_r = $getOutboundItem->result_array();
				// print('<pre>'.print_r($getOutboundItem_r,true).'</pre>');exit;
				

				$this->db->trans_start();

					if($getOutboundItem_r[0]['outbound_priority'] == null){
						$getOutboundItem_r[0]['outbound_priority'] = 1;
					}
					$insert_picking_q = "
						INSERT INTO pickings (name, date, status_id, remark, user_id, created_at, pl_status, priority)
						VALUES
						('PL-".date('Ymd').$getOutboundItem_r[0]['outbound_id']."-".$getOutboundItem_r[0]['item_area']."', NOW(), 6, '', ".$this->session->userdata('user_id').", NOW(), 0, ".$getOutboundItem_r[0]['outbound_priority'].")
						;";

					$this->db->query($insert_picking_q);
					$pl_id = $this->db->select('pl_id')->from('pickings')->order_by('pl_id','desc')->limit(1)->get()->row_array()['pl_id'];
					$insert_plo_q = "
						INSERT INTO picking_list_outbound(pl_id, id_outbound) VALUES
						(".$pl_id.", ".$getOutboundItem_r[0]['outbound_id'].")
						;";
					$insert_plo = $this->db->query($insert_plo_q);
					
					$update_so = "Update outbound set status_id = 5 where id = ".$getOutboundItem_r[0]['outbound_id'];
					$update_so = $this->db->query($update_so);
					
					for($j=0;$j<$getOutboundItem->num_rows();$j++){
						$checkQty = $this->db->query('
									SELECT
										ird.item_id, sum(ird.last_qty) AS available_qty
									FROM
										item_receiving_details ird
									JOIN locations loc
										ON loc.id=ird.location_id
									WHERE
										ird.item_id ='.$getOutboundItem_r[$j]['item_id'].'
									and 
										ird.qc_id = \'1\'
									and 
										ird.location_id not in (100,101,102,103,104,105,106)
									GROUP BY
										ird.item_id
									')->result_array();

						$item_sub = $this->db->query(
							"select item_id_subs from item_substitute is2
							where item_id =".$getOutboundItem_r[$j]['item_id']
						)->result_array();

						$booked_qty = $this->db->query('
								select itm.name,sum(pq.qty) as booked_qty from picking_qty pq
								left join pickings p on p.pl_id = pq.picking_id 
								left join items itm on itm.id = pq.item_id 
								where p.end_time is null
								and itm.id = '.$getOutboundItem_r[$j]['item_id'].'
								group by itm.id, itm.name;
						')->result_array();

						// $checkBtchQty = $this->db->query('
						// 			SELECT
						// 				ird.item_id, ird.kd_batch, sum(ird.last_qty) AS available_qty
						// 			FROM
						// 				item_receiving_details ird
						// 			JOIN locations loc
						// 				ON loc.id=ird.location_id
						// 			WHERE
						// 				loc.loc_type NOT IN('.$this->config->item('hardcode_location').')
						// 			AND
						// 				qc_id = \'1\'
						// 			AND
						// 				ird.item_id ='.$getOutboundItem_r[$j]['item_id'].'
						// 			GROUP BY
						// 				ird.item_id, ird.kd_batch
						// 			')->result_array();

						if(isset($checkQty[0]['available_qty'])){
							$available = $checkQty[0]['available_qty'];
							$is_sub = 0;

							//* check item subtitute
							if($available < $getOutboundItem_r[$j]['item_qty']){
								if(count($item_sub)>0){
									for($is = 0; $is < count($item_sub); $is++){
										$sub_qty = $this->db->query('
													SELECT
														ird.item_id, sum(ird.last_qty) AS available_qty
													FROM
														item_receiving_details ird
													JOIN locations loc
														ON loc.id=ird.location_id
													WHERE
														ird.item_id ='.$item_sub[$is]['item_id_subs'].'
													and 
														ird.qc_id = \'1\'
													and 
														ird.location_id not in (100,101,102,103,104,105,106)
													GROUP BY
														ird.item_id
													')->result_array();
										$sub_booked_qty = $this->db->query('
															select itm.name,sum(pq.qty) as booked_qty from picking_qty pq
															left join pickings p on p.pl_id = pq.picking_id 
															left join items itm on itm.id = pq.item_id 
															where p.end_time is null
															and itm.id = '.$item_sub[$is]['item_id_subs'].'
															group by itm.id, itm.name;
													')->result_array();
										if(isset($sub_qty[0]['available_qty'])){
											if($sub_qty[0]['available_qty']>0){
												$available += $sub_qty[0]['available_qty'];
											}
										}
										if(isset($sub_booked_qty[0]['booked_qty'])){
											$available -= $sub_booked_qty[0]['booked_qty'];
										}

										if($available>0){
											$is_sub = 1;
											$this->db->set('status_id',3);
											$this->db->where('pl_id',$pl_id);
											$this->db->update('pickings');
										}else{
											$is_sub = 0;
										}
									}
								}
							}


							if(isset($booked_qty[0]['booked_qty'])){
								$available -= $booked_qty[0]['booked_qty'];
							}
							if($available>0){
								if($available<$getOutboundItem_r[$j]['item_qty']){
									$qty = $available;
								}else if($available>=$getOutboundItem_r[$j]['item_qty']){
									$qty = $getOutboundItem_r[$j]['item_qty'];
								}
								$insert_picking_qty_q = "
									INSERT INTO picking_qty (picking_id, item_id, qty, created_at, unit_id, is_sub)
									VALUES
									(".$pl_id.", ".$getOutboundItem_r[$j]['item_id'].", ".$qty.", NOW(), ".$getOutboundItem_r[$j]['item_unit'].", ".$is_sub.")
									;";
								$this->db->query($insert_picking_qty_q);
							}else{
								echo 'item tidak mencukupi<br>';
							}
						}
					}

				$sumOfItem = $this->db->query('
						SELECT count(id)
						from picking_qty
						where picking_id ='.$pl_id.'
						group by picking_id
				');

				if($sumOfItem->num_rows()==0){
					echo 'rollback';
					$this->db->trans_rollback();
				}else{
					echo 'commit';
					$this->db->trans_complete();
					$this->db->trans_commit();
				}
			}else{
				$getOutboundItem_q = "
					SELECT o.id AS outbound_id, o.code AS outbound_code, o.date AS outbound_date, o.m_priority_id AS outbound_priority, i.id AS item_id, i.code AS item_code, oi.qty AS item_qty, i.unit_id AS item_unit, LEFT(i.item_area,1) AS item_area
					FROM outbound o
					JOIN outbound_item oi ON o.id = oi.id_outbound
					JOIN items i ON i.id = oi.item_id
					WHERE o.id = ".$getOutboundItemArea_r[$i]['outbound_id']." AND LEFT(i.item_area,1) = '".$getOutboundItemArea_r[$i]['item_area']."'
					;";
				$getOutboundItem = $this->db->query($getOutboundItem_q);
				$getOutboundItem_r = $getOutboundItem->result_array();
				for($b = 0; $b < count($getOutboundItem_r); $b++){
					$getPickedItem_q = "
						select item_id, sum(qty) as qty, p.status_id, itm.unit_id from picking_qty pq
						left join picking_list_outbound plo on plo.pl_id = pq.picking_id 
						left join pickings p on pq.picking_id = p.pl_id
						left join items itm on itm.id = pq.item_id
						where plo.id_outbound =".$getOutboundItemArea_r[$i]['outbound_id']."
						group by item_id, qty, p.status_id, itm.unit_id
					";
					$getPickedItem = $this->db->query($getPickedItem_q);
					$getPickedItem_r = $getPickedItem->result_array();
					if(isset($getPickedItem_r[$b]['status_id'])){
						if($getPickedItem_r[$b]['status_id']>7){
							//TODO: this code will checked, is there any qty that dont picked yet
							$comparing_sql = "
                                select oi.qty AS item_qty
                                from outbound o
                                join outbound_item oi on o.id = oi.id_outbound
                                join items i on i.id = oi.item_id
                                WHERE o.id = ".$getOutboundItemArea_r[$i]['outbound_id']." AND i.id = ".$getPickedItem_r[$b]['item_id'];
                            $compare_oi = $this->db->query($comparing_sql)->row_array();
							$decision = $compare_oi['item_qty'] - $getPickedItem_r[$b]['qty'];
							//$decision = $getOutboundItem_r[$b]['item_qty'] - $getPickedItem_r[$b]['qty'];
	
							$this->db->trans_start();
	
							if($getOutboundItem_r[0]['outbound_priority'] == null){
								$getOutboundItem_r[0]['outbound_priority'] = 1;
							}
							$insert_picking_q = "
								INSERT INTO pickings (name, date, status_id, remark, user_id, created_at, pl_status, priority)
								VALUES
								('PL-".date('Ymd').$getOutboundItem_r[0]['outbound_id']."-".$getOutboundItem_r[0]['item_area']."-BO-".rand(1000,9999)."', NOW(), 6, '', ".$this->session->userdata('user_id').", NOW(), 0, ".$getOutboundItem_r[0]['outbound_priority'].")
								;";
	
							$this->db->query($insert_picking_q);
							$pl_id = $this->db->select('pl_id')->from('pickings')->order_by('pl_id','desc')->limit(1)->get()->row_array()['pl_id'];
							$insert_plo_q = "
								INSERT INTO picking_list_outbound(pl_id, id_outbound) VALUES
								(".$pl_id.", ".$getOutboundItem_r[0]['outbound_id'].")
								;";
							$insert_plo = $this->db->query($insert_plo_q);
	
							if($decision > 0){
								$checkQty = $this->db->query('
											SELECT
												ird.item_id, sum(ird.last_qty) AS available_qty
											FROM
												item_receiving_details ird
											JOIN locations loc
												ON loc.id=ird.location_id
											WHERE
												ird.item_id ='.$getOutboundItem_r[$b]['item_id'].'
											and 
												ird.qc_id = \'1\'
											and 
												ird.location_id not in (100,101,102,103,104,105,106)
											GROUP BY
												ird.item_id
											')->result_array();

								$item_sub = $this->db->query(
									"select item_id_subs from item_substitute is2
									where item_id =".$getOutboundItem_r[$b]['item_id']
								)->result_array();
	
								$booked_qty = $this->db->query('
										select itm.name,sum(pq.qty) as booked_qty from picking_qty pq
										left join pickings p on p.pl_id = pq.picking_id 
										left join items itm on itm.id = pq.item_id 
										where p.end_time is null
										and itm.id = '.$getOutboundItem_r[$b]['item_id'].'
										group by itm.id, itm.name;
								')->result_array();
	
								if(isset($checkQty[0]['available_qty'])){
									$available = $checkQty[0]['available_qty'];

									$is_sub = 0;

									//* check item subtitute
									if($available < $decision){
										if(count($item_sub)>0){
											for($is = 0; $is < count($item_sub); $is++){
												$sub_qty = $this->db->query('
															SELECT
																ird.item_id, sum(ird.last_qty) AS available_qty
															FROM
																item_receiving_details ird
															JOIN locations loc
																ON loc.id=ird.location_id
															WHERE
																ird.item_id ='.$item_sub[$is]['item_id_subs'].'
															and 
																ird.qc_id = \'1\'
															and 
																ird.location_id not in (100,101,102,103,104,105,106)
															GROUP BY
																ird.item_id
															')->result_array();
												$sub_booked_qty = $this->db->query('
																	select itm.name,sum(pq.qty) as booked_qty from picking_qty pq
																	left join pickings p on p.pl_id = pq.picking_id 
																	left join items itm on itm.id = pq.item_id 
																	where p.end_time is null
																	and itm.id = '.$item_sub[$is]['item_id_subs'].'
																	group by itm.id, itm.name;
															')->result_array();
												if(isset($sub_qty[0]['available_qty'])){
													if($sub_qty[0]['available_qty']>0){
														$available += $sub_qty[0]['available_qty'];
													}
												}
												if(isset($sub_booked_qty[0]['booked_qty'])){
													$available -= $sub_booked_qty[0]['booked_qty'];
												}

												if($available>0){
													$is_sub = 1;
													$this->db->set('status_id',3);
													$this->db->where('pl_id',$pl_id);
													$this->db->update('pickings');
												}else{
													$is_sub = 0;
												}
											}
										}
									}

									if(isset($booked_qty[0]['booked_qty'])){
										$available -= $booked_qty[0]['booked_qty'];
									}
									if($available>0){
										if($available<$decision){
											$qty = $available;
										}else if($available>=$decision){
											$qty = $decision;
										}
										$insert_picking_qty_q = "
											INSERT INTO picking_qty (picking_id, item_id, qty, created_at, unit_id, is_sub)
											VALUES
											(".$pl_id.", ".$getPickedItem_r[$b]['item_id'].", ".$qty.", NOW(), ".$getPickedItem_r[$b]['unit_id'].", ".$is_sub.")
											;";
										$this->db->query($insert_picking_qty_q);
										echo 'SO: '.$getOutboundItem_r[0]['outbound_id'].'item '.$getOutboundItem_r[$b]['item_id'].' mencukupi booking order=>'.$decision.'<br>';
									}else{
										echo 'SO: '.$getOutboundItem_r[0]['outbound_id'].'item tidak mencukupi booking order=>'.$decision.'<br>';
									}
								}
	
							}else{
								echo $getOutboundItemArea_r[$i]['outbound_id']." tidak membentuk Backorder karena Outbound Item sudah already have pick qty <br>";
							}
	
							$sumOfItem = $this->db->query('
								SELECT count(id)
								from picking_qty
								where picking_id ='.$pl_id.'
								group by picking_id
							');
	
							if($sumOfItem->num_rows()==0){
								echo 'rollback BO ';
								$this->db->trans_rollback();
							}else{
								echo 'commit BO ';
								$this->db->trans_complete();
								$this->db->trans_commit();
							}
						}
					}
				}
			}
		}
	}

	public function emergencyPl(){
		$getOutboundItemArea_q = "
			SELECT * FROM (
			SELECT o.id AS outbound_id, o.code AS outbound_code, m_priority_id, date, LEFT(i.item_area,1) AS item_area
			FROM outbound o
			JOIN outbound_item oi ON o.id = oi.id_outbound
			JOIN items i ON i.id = oi.item_id
			) a
			WHERE item_area IS NOT NULL and m_priority_id = 2
			GROUP BY outbound_id, outbound_code, item_area, m_priority_id, date
			order by m_priority_id desc, date desc
			;";
		$getOutboundItemArea = $this->db->query($getOutboundItemArea_q);
		$getOutboundItemArea_r = $getOutboundItemArea->result_array();
		dd($getOutboundItemArea_r);
	}
	
	public function autoCreatePL($outbound_id=''){
		$where = '';
		if($outbound_id !== ''){
			$checkdest = "select d.destination_id from outbound o
						left join destinations d on d.destination_id = o.destination_id
						where o.id =".$outbound_id;
			$dest = $this->db->query($checkdest)->row_array()['destination_id'];
			if($dest=='10822'){
				$where = " AND outbound_id = ".$outbound_id." and destination_id = 10822";
			}else{
				$where = " AND outbound_id = ".$outbound_id;
			}
		}else{
			$where = " and destination_id != 10822";
		}
		
		$getOutboundItemArea_q = "
			SELECT * FROM (
			SELECT o.id AS outbound_id, o.code AS outbound_code, destination_id, m_priority_id, date, LEFT(i.item_area,1) AS item_area, o.status_id
			FROM outbound o
			JOIN outbound_item oi ON o.id = oi.id_outbound
			JOIN items i ON i.id = oi.item_id
			) a
			WHERE a.status_id not in (4,17) and item_area IS NOT NULL ".$where."
			GROUP BY outbound_id, outbound_code, item_area, m_priority_id, destination_id, date, status_id
			order by m_priority_id desc, date ASC
			;";
		$getOutboundItemArea = $this->db->query($getOutboundItemArea_q);
		$getOutboundItemArea_r = $getOutboundItemArea->result_array();
		//dd(json_encode($getOutboundItemArea_r));
		for($i=0;$i<$getOutboundItemArea->num_rows();$i++){
			$getPL_q = "
				SELECT p.*
				FROM pickings p
				JOIN picking_qty pq ON p.pl_id = pq.picking_id
				JOIN items i ON i.id = pq.item_id
				JOIN picking_list_outbound plo ON plo.pl_id = p.pl_id
				WHERE plo.id_outbound = ".$getOutboundItemArea_r[$i]['outbound_id']." AND LEFT(i.item_area,1) = '".$getOutboundItemArea_r[$i]['item_area']."'
				;";
			$getPL = $this->db->query($getPL_q);
			$getPL_r = $getPL->result_array();
			
			if($getPL->num_rows() == 0){
	
				$getOutboundItem_q = "
					SELECT o.id AS outbound_id, o.code AS outbound_code, o.date AS outbound_date, o.m_priority_id AS outbound_priority, i.id AS item_id, i.code AS item_code, oi.qty AS item_qty, i.unit_id AS item_unit, LEFT(i.item_area,1) AS item_area
					FROM outbound o
					JOIN outbound_item oi ON o.id = oi.id_outbound
					JOIN items i ON i.id = oi.item_id
					WHERE o.id = ".$getOutboundItemArea_r[$i]['outbound_id']." AND LEFT(i.item_area,1) = '".$getOutboundItemArea_r[$i]['item_area']."'
					;";
				$getOutboundItem = $this->db->query($getOutboundItem_q);
				$getOutboundItem_r = $getOutboundItem->result_array();
				//print('<pre>'.print_r($getOutboundItem_r,true).'</pre>');exit;
				
	
				$this->db->trans_start();
	
					if($getOutboundItem_r[0]['outbound_priority'] == null){
						$getOutboundItem_r[0]['outbound_priority'] = 1;
					}
					$insert_picking_q = "
						INSERT INTO pickings (name, date, status_id, remark, user_id, created_at, pl_status, priority)
						VALUES
						('PL-".date('Ymd').$getOutboundItem_r[0]['outbound_id']."-".$getOutboundItem_r[0]['item_area']."', NOW(), 6, '', ".$this->session->userdata('user_id').", NOW(), 0, ".$getOutboundItem_r[0]['outbound_priority'].")
						;";
	
					$this->db->query($insert_picking_q);
					$pl_id = $this->db->select('pl_id')->from('pickings')->order_by('pl_id','desc')->limit(1)->get()->row_array()['pl_id'];
					$insert_plo_q = "
						INSERT INTO picking_list_outbound(pl_id, id_outbound) VALUES
						(".$pl_id.", ".$getOutboundItem_r[0]['outbound_id'].")
						;";
					$insert_plo = $this->db->query($insert_plo_q);
					
					$update_so = "Update outbound set status_id = 5 where id = ".$getOutboundItem_r[0]['outbound_id'];
					$update_so = $this->db->query($update_so);
					
					for($j=0;$j<$getOutboundItem->num_rows();$j++){
						$checkQty = $this->db->query('
									SELECT
										ird.item_id, sum(ird.last_qty) AS available_qty
									FROM
										item_receiving_details ird
									JOIN locations loc
										ON loc.id=ird.location_id
									WHERE
										ird.item_id ='.$getOutboundItem_r[$j]['item_id'].'
									and 
										ird.qc_id = \'1\'
									and 
										ird.location_id not in (100,101,102,103,104,105,106)
									GROUP BY
										ird.item_id
									')->result_array();
						
						if(count($checkQty)==0){
							$checkQty[0]['available_qty'] = 0;
							$checkQty[0]['item_id'] = $getOutboundItem_r[$j]['item_id'];
						}
							
						$item_sub = $this->db->query(
							"select item_id_subs from item_substitute is2
							where item_id =".$getOutboundItem_r[$j]['item_id']
						)->result_array();
	
						$booked_qty = $this->db->query('
								select itm.name,sum(pq.qty) as booked_qty from picking_qty pq
								left join pickings p on p.pl_id = pq.picking_id 
								left join items itm on itm.id = pq.item_id 
								where p.end_time is null
								and itm.id = '.$getOutboundItem_r[$j]['item_id'].'
								group by itm.id, itm.name;
						')->result_array();

						$min_booked = $this->db->query('select pr.item_id ,sum(qty) as qty from picking_recomendation pr 
						left join pickings p on p.pl_id = pr.picking_id 
						where item_id = '.$getOutboundItem_r[$j]['item_id'].' and p.end_time is null group by pr.item_id')->result_array();
						// dd($getOutboundItem_r[$j]['item_id']);
						
						// if(!isset($checkQty[0]['available_qty'])){
						// 	$checkQty[0]['available_qty'] = 0;
						// }
						if(isset($checkQty[0]['available_qty'])){


							$available = $checkQty[0]['available_qty'];
							$is_sub = 0;
	
							//* check item subtitute
							if($available < $getOutboundItem_r[$j]['item_qty']){
								if(count($item_sub)>0){
									for($is = 0; $is < count($item_sub); $is++){
										$sub_qty = $this->db->query('
													SELECT
														ird.item_id, sum(ird.last_qty) AS available_qty
													FROM
														item_receiving_details ird
													JOIN locations loc
														ON loc.id=ird.location_id
													WHERE
														ird.item_id ='.$item_sub[$is]['item_id_subs'].'
													and 
														ird.qc_id = \'1\'
													and 
														ird.location_id not in (100,101,102,103,104,105,106)
													GROUP BY
														ird.item_id
													')->result_array();
										$sub_booked_qty = $this->db->query('
															select itm.name,sum(pq.qty) as booked_qty from picking_qty pq
															left join pickings p on p.pl_id = pq.picking_id 
															left join items itm on itm.id = pq.item_id 
															where p.end_time is null
															and itm.id = '.$item_sub[$is]['item_id_subs'].'
															group by itm.id, itm.name;
													')->result_array();
										if(isset($sub_qty[0]['available_qty'])){
											if($sub_qty[0]['available_qty']>0){
												$available += $sub_qty[0]['available_qty'];
											}
										}
										if(isset($sub_booked_qty[0]['booked_qty'])){
											$available -= $sub_booked_qty[0]['booked_qty'];
										}
	
										if($available>0){
											$is_sub = 1;
											$this->db->set('status_id',3);
											$this->db->where('pl_id',$pl_id);
											$this->db->update('pickings');
										}else{
											$is_sub = 0;
										}
									}
								}
							}
	
							if(isset($booked_qty[0]['booked_qty'])){
								$book = $booked_qty[0]['booked_qty']-$min_booked[0]['qty'];
								$available -= $book;
							}

							if($available>0){
								if($available<$getOutboundItem_r[$j]['item_qty']){
									$qty = $available;
								}else if($available>=$getOutboundItem_r[$j]['item_qty']){
									$qty = $getOutboundItem_r[$j]['item_qty'];
								}
								$insert_picking_qty_q = "
									INSERT INTO picking_qty (picking_id, item_id, qty, created_at, unit_id, is_sub)
									VALUES
									(".$pl_id.", ".$getOutboundItem_r[$j]['item_id'].", ".$qty.", NOW(), ".$getOutboundItem_r[$j]['item_unit'].", ".$is_sub.")
									;";
								$this->db->query($insert_picking_qty_q);
							}else{
								echo 'item tidak mencukupi<br>';
							}
						}
					}
	
				$sumOfItem = $this->db->query('
						SELECT count(id)
						from picking_qty
						where picking_id ='.$pl_id.'
						group by picking_id
				');
	
				if($sumOfItem->num_rows()==0){
					echo 'rollback';
					$this->db->trans_rollback();
				}else{
					echo 'commit';
					$this->db->trans_complete();
					$this->db->trans_commit();
				}
			}else{
				$getOutboundItem_q = "
					SELECT o.id AS outbound_id, o.code AS outbound_code, o.date AS outbound_date, o.m_priority_id AS outbound_priority, i.id AS item_id, i.code AS item_code, oi.qty AS item_qty, i.unit_id AS item_unit, LEFT(i.item_area,1) AS item_area
					FROM outbound o
					JOIN outbound_item oi ON o.id = oi.id_outbound
					JOIN items i ON i.id = oi.item_id
					WHERE o.id = ".$getOutboundItemArea_r[$i]['outbound_id']." AND LEFT(i.item_area,1) = '".$getOutboundItemArea_r[$i]['item_area']."'
					;";
				$getOutboundItem = $this->db->query($getOutboundItem_q);
				$getOutboundItem_r = $getOutboundItem->result_array();
				//dd(json_encode($getOutboundItemArea_r));
				for($b = 0; $b < count($getOutboundItem_r); $b++){
					$getPickedItem_q = "
						select item_id, sum(qty) as qty, itm.unit_id from picking_qty pq
						left join picking_list_outbound plo on plo.pl_id = pq.picking_id 
						left join pickings p on pq.picking_id = p.pl_id
						left join items itm on itm.id = pq.item_id
						where plo.id_outbound =".$getOutboundItemArea_r[$i]['outbound_id']."
						and itm.id = ".$getOutboundItem_r[$b]['item_id']."
						and p.status_id < 11
						group by item_id, itm.unit_id
					";
					$getPickedItem = $this->db->query($getPickedItem_q);
					$getPickedItem_r = $getPickedItem->result_array();
					//dd($getPickedItem_r);
					
					if(count($getPickedItem_r)==0){
						$getPickedItem_r[0]['status_id'] = 8;
						$getPickedItem_r[0]['item_id'] = $getOutboundItem_r[$b]['item_id'];
						$getPickedItem_r[0]['qty'] = 0;
						//$getOutboundItem_r[0]['outbound_id'] = $getOutboundItemArea_r[$i]['outbound_id'];
						$getPickedItem_r[0]['unit_id'] = $this->db->select('unit_id')->from('items')->where('id',$getOutboundItem_r[$b]['item_id'])->get()->row_array()['unit_id'];
					}else{
						$getPickedItem_r[0]['status_id'] = 8;
					}
					
					//dd(json_encode($getOutboundItem_r));
					
					
					if(isset($getPickedItem_r[0]['status_id'])){
						if($getPickedItem_r[0]['status_id']>7){
							//TODO: this code will checked, is there any qty that dont picked yet
							$comparing_sql = "
								select oi.qty AS item_qty
								from outbound o
								join outbound_item oi on o.id = oi.id_outbound
								join items i on i.id = oi.item_id
								WHERE o.id = ".$getOutboundItemArea_r[$i]['outbound_id']." AND i.id = ".$getPickedItem_r[0]['item_id'];
							$compare_oi = $this->db->query($comparing_sql)->row_array();
							$decision = $compare_oi['item_qty'] - $getPickedItem_r[0]['qty'];
							//$decision = $getOutboundItem_r[$b]['item_qty'] - $getPickedItem_r[$b]['qty'];
	
							$this->db->trans_start();
	
							if($getOutboundItem_r[0]['outbound_priority'] == null){
								$getOutboundItem_r[0]['outbound_priority'] = 1;
							}
							$insert_picking_q = "
								INSERT INTO pickings (name, date, status_id, remark, user_id, created_at, pl_status, priority)
								VALUES
								('PL-".date('Ymd').$getOutboundItem_r[0]['outbound_id']."-".$getOutboundItem_r[0]['item_area']."-BO-".rand(1000,9999)."', NOW(), 6, '', ".$this->session->userdata('user_id').", NOW(), 0, ".$getOutboundItem_r[0]['outbound_priority'].")
								;";
	
							$this->db->query($insert_picking_q);
							$pl_id = $this->db->select('pl_id')->from('pickings')->order_by('pl_id','desc')->limit(1)->get()->row_array()['pl_id'];
							$insert_plo_q = "
								INSERT INTO picking_list_outbound(pl_id, id_outbound) VALUES
								(".$pl_id.", ".$getOutboundItem_r[0]['outbound_id'].")
								;";
							$insert_plo = $this->db->query($insert_plo_q);
	
							if($decision > 0){
								$checkQty = $this->db->query('
											SELECT
												ird.item_id, sum(ird.last_qty) AS available_qty
											FROM
												item_receiving_details ird
											JOIN locations loc
												ON loc.id=ird.location_id
											WHERE
												ird.item_id ='.$getPickedItem_r[0]['item_id'].'
											and 
												ird.qc_id = \'1\'
											and 
												ird.location_id not in (100,101,102,103,104,105,106)
											GROUP BY
												ird.item_id
											')->result_array();
	
								$item_sub = $this->db->query(
									"select item_id_subs from item_substitute is2
									where item_id =".$getPickedItem_r[0]['item_id']
								)->result_array();
	
								$booked_qty = $this->db->query('
										select itm.name,sum(pq.qty) as booked_qty from picking_qty pq
										left join pickings p on p.pl_id = pq.picking_id 
										left join items itm on itm.id = pq.item_id 
										where p.end_time is null
										and itm.id = '.$getPickedItem_r[0]['item_id'].'
										group by itm.id, itm.name
								')->result_array();

								if(!isset($checkQty[0]['available_qty'])){
									$checkQty[0]['available_qty'] = 0;
								}
								
								if(isset($checkQty[0]['available_qty'])){
									$available = $checkQty[0]['available_qty'];
	
									$is_sub = 0;
	
									//* check item subtitute
									if($available < $decision){
										if(count($item_sub)>0){
											for($is = 0; $is < count($item_sub); $is++){
												$sub_qty = $this->db->query('
															SELECT
																ird.item_id, sum(ird.last_qty) AS available_qty
															FROM
																item_receiving_details ird
															JOIN locations loc
																ON loc.id=ird.location_id
															WHERE
																ird.item_id ='.$item_sub[$is]['item_id_subs'].'
															and 
																ird.qc_id = \'1\'
															and 
																ird.location_id not in (100,101,102,103,104,105,106)
															GROUP BY
																ird.item_id
															')->result_array();
												$sub_booked_qty = $this->db->query('
																	select itm.name,sum(pq.qty) as booked_qty from picking_qty pq
																	left join pickings p on p.pl_id = pq.picking_id 
																	left join items itm on itm.id = pq.item_id 
																	where p.end_time is null
																	and itm.id = '.$item_sub[$is]['item_id_subs'].'
																	group by itm.id, itm.name;
															')->result_array();
												if(isset($sub_qty[0]['available_qty'])){
													if($sub_qty[0]['available_qty']>0){
														$available += $sub_qty[0]['available_qty'];
													}
												}
												if(isset($sub_booked_qty[0]['booked_qty'])){
													$available -= $sub_booked_qty[0]['booked_qty'];
												}
	
												if($available>0){
													$is_sub = 1;
													$this->db->set('status_id',3);
													$this->db->where('pl_id',$pl_id);
													$this->db->update('pickings');
												}else{
													$is_sub = 0;
												}
											}
										}
									}
	
									if(isset($booked_qty[0]['booked_qty'])){
										$available -= $booked_qty[0]['booked_qty'];
									}

									if($available>0){
										if($available<$decision){
											$qty = $available;
										}else if($available>=$decision){
											$qty = $decision;
										}
										$insert_picking_qty_q = "
											INSERT INTO picking_qty (picking_id, item_id, qty, created_at, unit_id, is_sub)
											VALUES
											(".$pl_id.", ".$getPickedItem_r[0]['item_id'].", ".$qty.", NOW(), ".$getPickedItem_r[0]['unit_id'].", ".$is_sub.")
											;";
										$this->db->query($insert_picking_qty_q);
										echo 'SO: '.$getOutboundItem_r[0]['outbound_id'].'item '.$getOutboundItem_r[$b]['item_id'].' mencukupi booking order=>'.$decision.' | '.$getPickedItem_r[0]['item_id'].'<br>';
									}else{
										echo 'SO: '.$getOutboundItem_r[0]['outbound_id'].'item tidak mencukupi booking order=>'.$decision.' | '.$getPickedItem_r[0]['item_id'].'<br>';
									}
								}
	
							}else{
								echo $getOutboundItemArea_r[$i]['outbound_id']." tidak membentuk Backorder karena Outbound Item sudah already have pick qty <br>";
							}
	
							$sumOfItem = $this->db->query('
								SELECT count(id)
								from picking_qty
								where picking_id ='.$pl_id.'
								group by picking_id
							');
	
							if($sumOfItem->num_rows()==0){
								echo 'rollback BO ';
								$this->db->trans_rollback();
							}else{
								echo 'commit BO ';
								$this->db->trans_complete();
								$this->db->trans_commit();
							}
						}
					}
				}
			}
		}
	}
}

<?php

/**
 * Description of user
 *
 * @author Warman Suganda
 */
class user extends MY_Controller {

    private $class_name;
    private $limit = 10;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
		
        if ($this->uri->segment(2) != 'change_password') {
            $this->access_right->check();
            $this->access_right->otoritas('view', true);
        }

        // Global Model
        $this->load_model($this->class_name . '_model');
    }

    private function include_required_file(&$data){

        $data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery.cookie.js';
        $data['jsPage'][] = 'assets/custom/js/share.js';

    }

    public function index() {
        $data['title'] = 'Users';
        $data['subTitle'] = '<i class="fa fa-th-list"></i> User List';
        $data['page'] = $this->class_name . '/index';

        $this->include_required_file($data);
        $data['jsPage'][] = "assets/pages/scripts/lr-user.js";

        $this->include_other_view_data($data);

        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

        /* Import */
        if($this->access_right->otoritas('edit')){
            $data['page'][] = 'components/form-set-warehouse';
            $data['jsPage'][] = "assets/pages/scripts/lr-set-warehouse.js";
        }

        /* Get Data Select*/
        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options('',NULL,TRUE); // Get Data Warehoues
        $data['zones'] = $this->user_model->zone();// Get Data Zone
        $this->access_right->activity_logs('view','Daftar User');
        $this->load->view('template', $data);
    }

/*    public function get_list(){

        $post =$_REQUEST;

        $params = array();

        if(!empty($post['name'])){
            $params[]['a.id']=$post['name'];
        }

        if(!empty($post['username'])){
            $params[]['a.id']=$post['username'];
        }

        if(!empty($post['roles'])){
            $params[]['a.grup_id'] = $post['roles'];
        }

        // if(!empty($post[''])){
        //     $params['a.nama LIKE "%'.$post['name'].'%"'] = null;
        // }

        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $users = $this->user_model->get_data($params);
        $iTotalRecords = $this->user_model->get_data($params)->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($users->result() as $value){

            $onlineUser = "<span class='label label-sm label-danger'> Offline </span>";

            if($value->handheld_session_code !== NULL){
                $onlineUser = "<span class='label label-sm label-default' style='background:green;'> Online </span>";
            }

            $records["data"][] = array(
                $_REQUEST['start']+$i,
                $value->nama,
                $value->user_name,
                " * * * ",
                $value->grup_nama,
                $onlineUser,
                $action = $this->buttons->actions(array('edit'=> $value->user_id, 'delete' => $value->user_id))
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }
*/
    public function get_list(){

        $post =$_REQUEST;

        $columns = array(
            0 => '',
            1 => 'nama',
            2 => 'user_name',
            3 => '',
            4 => 'grup_nama',
            5 => 'handheld_session_code'
        );

        $params = array();

        if(!empty($post['name'])){
            $params[]['a.id']=$post['name'];
        }

        if(!empty($post['username'])){
            $params[]['a.id']=$post['username'];
        }

        if(!empty($post['roles'])){
            $params[]['a.grup_id'] = $post['roles'];
        }

        $this->db->order_by($columns[$post['order'][0]['column']],$post['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $users = $this->user_model->get_data($params);
        $iTotalRecords = $this->user_model->get_data($params)->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($users->result() as $value){

            $onlineUser = "<span class='label label-sm label-danger'> Offline </span>";

            if($value->handheld_session_code !== NULL){
                $onlineUser = "<span class='label label-sm label-default' style='background:green;'> Online </span>";
            }

            $records["data"][] = array(
                $_REQUEST['start']+$i,
                $value->nama,
                $value->user_name,
                " * * * ",
                $value->grup_nama,
                $onlineUser,
                $action = $this->buttons->actions(array('edit'=> $value->user_id, 'delete' => $value->user_id))
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }
    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $user = $this->user_model->get_user_with_id($id)->row_array();

            $warehouses = $this->user_model->getUserWarehouse($id);
            $user['warehouses'] = array();
            foreach ($warehouses as $wh) {
                $user['warehouses'][] = $wh['warehouse_id'];
            }

            $zones = $this->user_model->getUserZones($id);
            $user['zones'] = array();
            foreach ($zones as $zone) {
                $user['zones'][] = $zone['zone_id'];
            }

            $process_result = $user;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    private function include_other_view_data(&$data) {
        $data['grups'] = $this->user_model->create_options("hr_grup", "grup_id", "grup_nama", "-- Choose grup --");
        $data['statuses'] = array(
                '' => "-- Choose status --",
                'f' => "Not Active",
                't' => "Active"
            );
    }

    public function proses() {
        $process_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post('id');

            $data = array(
                'grup_id' => $this->input->post('grup'),
                'nik' => $this->input->post('nik'),
                'nama' => $this->input->post("nama"),
                'user_name' => $this->input->post('user_name'),
                //'user_password' => md5($this->input->post("password")),
                'is_active' => $this->input->post('status'),
                'warehouse' => $this->input->post('warehouse'),
                'zone'      => $this->input->post('zone')
            );
			$pass = $this->input->post("password");
			if(!empty($pass))
				$data['user_password'] = md5($pass);
			
            /*
             * If : $id == '', lakukan proses create data
             * Else : lakukan proses update
             */

            if ($id == '') {
                if ($this->user_model->create($data)) {
					$this->access_right->activity_logs('add','Daftar User');
                    $process_result['add'] = 1;
                }
            } else {
                if ($this->user_model->update($data, $id)) {
					$this->access_right->activity_logs('edit','Daftar User');
                    $process_result['edit'] = 1;
                }
            }
            $process_result['success'] = 1;
        } else {
            if(!$this->access_right->otoritas('add')) {
                $process_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $process_result['no_edit'] = 1;
            }
        }
        echo json_encode($process_result);
    }

    public function delete() {
        $process_result = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            if ($this->user_model->delete($id)) {
				$this->access_right->activity_logs('delete','Daftar User');
                $process_result['success'] = 1;
            }
        } else {
            $process_result['no_delete'] = 1;
        }
        echo json_encode($process_result);
    }

    public function search_pegawai() {
        $this->load_model('master_pegawai_model');

        $query = $this->input->get('query');

        $condition = array();
        $condition['OR']["LOWER(a.nik) LIKE " . " '%" . strtolower($query) . "%'"] = null;
        $condition['OR']["LOWER(a.nama) LIKE " . " '%" . strtolower($query) . "%'"] = null;

        $data = array();
        $source = $this->master_pegawai_model->get_data($condition);

        foreach ($source->result() as $value) {
            $data[] = array(
                'nik' => $value->nik,
                'nama' => $value->nik . ' - ' . $value->nama
            );
        }

        echo json_encode($data);
    }

    public function roles_setting() {
        $id = $this->input->post('id');
        $user_id = $this->input->post('user_id');

        $this->load_model('menu_group_model');
        $this->load_model('menu_model');
        $this->load_model('roles_model');

        $data['menu_group'] = $this->menu_group_model->get_data();
        $data['menu'] = $this->menu_model->parsing_data();

        $data['roles'] = $this->roles_model->parsing_roles(array('grup_id' => $id));
        $data['user_akses'] = $this->user_model->parsing_akses(array('userid' => $user_id));

        echo json_encode($this->load->view($this->class_name . '/roles', $data, true));
    }

    public function reset_password($id) {
        if ($this->access_right->otoritas('edit', true)) {
            $message = array(false, 'Proses gagal', 'Proses reset password gagal.', '');

            if ($this->user_model->reset_password($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses reset password berhasil.', '#content_table');
            }
            echo json_encode($message);
        }
    }

    public function pdf() {
        if ($this->access_right->otoritas('print', true)) {
            $this->load->library("custom_table");
            $this->load->helper('hpdf');

            $pdf = new hpdf('A4-L');

            $pdf->judul('DAFTAR USER');

            $table = new stdClass();
            $header[0] = array("NPP", 1, 1, "Nama", 1, 1, "Lokasi Kerja", 1, 1, "Username", 1, 1, "Ket. Password", 1, 1, "Status Password", 1, 1, "Role", 1, 1, "Status", 1, 1);
            $table->header = $header;
            $table->align = array('nik' => 'center', 'username' => 'center', 'ket_password' => 'center', 'status_password' => 'center', 'status' => 'center');
            $table->style = "table";
            $table->numbering = false;
            $table->kolom = 9;

            $table->model = $this->class_name . '_model->data_table_report';
            $data = $this->custom_table->generate($table);

            $pdf->html($data['table']);
            $pdf->cetak();
        }
    }

    public function excel() {
        if ($this->access_right->otoritas('print', true)) {
            $this->load->helper('hexcel');
            $excel = new hexcel();

            // Defult Border
            $defaultBorder = array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('rgb' => '000000')
            );

            $excel->getDefaultStyle()->getFont()->setName('Arial');
            $excel->getDefaultStyle()->getFont()->setSize(8);
            $excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

            // Set Active Sheet 1
            $excel->setActiveSheetIndex(0);
            $sheet = $excel->getActiveSheet();

            // Seting Halaman
            $excel->page_setup($sheet, PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4, PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $excel->margin($sheet, 0.3, 0.3, 0.5, 0.5, 0.5, 0.5);

            // Judul
            $sheet->setCellValue('A1', 'DAFTAR USER');
            $sheet->mergeCells('A1:H1');
            $sheet->getRowDimension(1)->setRowHeight(27);
            $sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A1')->applyFromArray(array('font' => array('bold' => true, 'size' => 11)));

            // Set Lebar Kolom
            $sheet->getColumnDimension('A')->setWidth(17);
            $sheet->getColumnDimension('B')->setWidth(35);
            $sheet->getColumnDimension('C')->setWidth(25);
            $sheet->getColumnDimension('D')->setWidth(17);
            $sheet->getColumnDimension('E')->setWidth(17);
            $sheet->getColumnDimension('F')->setWidth(17);
            $sheet->getColumnDimension('G')->setWidth(33);
            $sheet->getColumnDimension('H')->setWidth(14);

            // Set Header Table
            $sheet->setCellValue('A2', 'NPP');
            $sheet->setCellValue('B2', 'Nama');
            $sheet->setCellValue('C2', 'Lokasi Kerja');
            $sheet->setCellValue('D2', 'Username');
            $sheet->setCellValue('E2', 'Ket. Password');
            $sheet->setCellValue('F2', 'Status Password');
            $sheet->setCellValue('G2', 'Role');
            $sheet->setCellValue('H2', 'Status');
            $sheet->getStyle('A2:H2')->applyFromArray(array('font' => array('bold' => true), 'borders' => array('inside' => $defaultBorder, 'outline' => $defaultBorder)));
            $sheet->getStyle('A2:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2, 2);

            // Set Content Table
            $data = $this->user_model->data_table_report();

            $start_row = 3;
            $idx = $start_row - 1;
            if ($data['total'] > 0) {
                foreach ($data['rows'] as $value) {
                    $idx++;
                    $sheet->setCellValueExplicit('A' . $idx, $value['nik'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue('B' . $idx, $value['nama']);
                    $sheet->setCellValue('C' . $idx, $value['nama_cabang']);
                    $sheet->setCellValueExplicit('D' . $idx, $value['username'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue('E' . $idx, $value['ket_password']);
                    $sheet->setCellValue('F' . $idx, $value['status_password']);
                    $sheet->setCellValue('G' . $idx, $value['grup_nama']);
                    $sheet->setCellValue('H' . $idx, $value['status']);
                }

                $sheet->getStyle('A' . $start_row . ':H' . $idx)->applyFromArray(array('borders' => array('inside' => $defaultBorder, 'outline' => $defaultBorder)));
                $sheet->getStyle('A' . $start_row . ':A' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D' . $start_row . ':F' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('H' . $start_row . ':H' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            } else {
                $sheet->setCellValue('A' . $start_row, 'Data tidak ditemukan');
                $sheet->mergeCells('A' . $start_row . ':H' . $start_row);
                $sheet->getStyle('A' . $start_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A' . $start_row . ':H' . $start_row)->applyFromArray(array('borders' => array('inside' => $defaultBorder, 'outline' => $defaultBorder)));
            }

            // Proses Generate Excel
            hexcel::simpan('daftar_user', $excel, 'Excel2007');
        }
    }

    public function change_password($data='') {
        $this->load_model('user_model');
        if($this->session->userdata('st_toko')){
            $data['data_user'] = $this->user_model->get_user_toko();
            $data['title']="Ganti Username / Password";
            $data['page'] = 'user/edit_user_toko';
        }else{
            $kd_roles = $this->session->userdata('kd_roles');
            $nik = $this->session->userdata('nik_session');
            $q = $this->db->query("select user_id from hr_user where nik='$nik'");
            foreach ($q->result_array() as $row) {
                $userid = $row['user_id'];
            }
    		
            $data['data_user'] = $this->user_model->data_user_mdl($userid);
            $data['title']="Ganti Username / Password";
            $data['page'] = 'user/edit_user';
        }
        $this->load->view('template', $data);
    }
	
	public function checkOldPassword(){
		$result = array();
		
		$params = array(
			'user_id'	=> filter_var(trim($this->input->post('user_id')), FILTER_SANITIZE_NUMBER_INT),
			'old'		=> filter_var(trim($this->input->post('old')), FILTER_SANITIZE_STRING)
		);
		
		$result = $this->user_model->checkOldPassword($params);
		
		echo json_encode($result);
	}
	
	public function saveNewPassword(){
		$result = array();
		
		$params = array(
			'user_id'			=> filter_var(trim($this->input->post('user_id')), FILTER_SANITIZE_NUMBER_INT),
			'new_password'		=> filter_var(trim($this->input->post('new_password')), FILTER_SANITIZE_STRING)
		);
		
		$result = $this->user_model->saveNewPassword($params);
		
		echo json_encode($result);
	}

    public function getOnlineUser(){
        $this->db->where('handheld_session_code IS NOT NULL', null, false);

        $data['status'] = 'OK';
        $data['result']['online_user'] = $this->user_model->get_all()->num_rows();
        $data['result']['limit_user'] = $this->license->data()->limit_user;

        echo json_encode($data);
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('User Master Import Template.xlsx');

            echo json_encode($data);
        }
    }


    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        'NIK'       => 'nik',
                        'Name'      => 'name',
                        'Username'  => 'user_name',
                        'Password'  => 'user_password',
                        'Role'      => 'role',
                        'Warehouse' => 'warehouse'
                    ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->user_model->importFromExcel($data);

                        if($process['status']){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage($process['message']);

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }
 
        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }


    public function option($table,$col1,$col2){

        $query = strtolower($this->input->post('query'));

        $sql = "SELECT $col1 as id, $col2 as name FROM $table WHERE 1=1 ";

        if(!empty($query)){
            $sql .= "AND lower($col2) LIKE '%$query%'";
        }

        $data['results'] = $this->db->query($sql)->result_array();

        echo json_encode($data);

    }

}

/* End of file user.php */
    /* Location: ./application/controllers/user.php */
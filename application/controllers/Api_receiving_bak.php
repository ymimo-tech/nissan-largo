<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_receiving extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_receiving_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
		exit;
	}

	public function load()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= 'RECEIVING';
				$data['status']		= 200;
				$data['message']	= 'Load data for receiving.';
				$data['response']	= true;
				$data['results']	= $this->api_receiving_model->load()->result_array();
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get($receiving_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $receiving_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$received = $this->api_receiving_model->get($receiving_code)->row_array();
				if($received){
					if($received['st_receiving'] != 1){
						$data['param']		= $receiving_code;
						$data['status']		= 200;
						$data['message']	= $receiving_code . ' is available.';
						$data['response']	= true;
					} else{
						$data['param']		= $receiving_code;
						$data['status']		= 401;
						$data['message']	= $receiving_code . ' has been received before.';
						$data['response']	= false;
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $receiving_code;
					$data['message']	= $receiving_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post($param=array())
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST' && empty($param)){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth || !empty($param)){

				if(empty($param)){

					$params['kd_receiving']		= $this->input->post('receiving_code');
					$params['kd_barang']		= $this->input->post('item_code');
					$params['kd_unik']			= $this->input->post('serial_number');
					$params['kd_batch']			= $this->input->post('batch_code');
					$params['tgl_exp']			= $this->input->post('tgl_exp');
					$params['qty']				= $this->input->post('qty');
					$params['kd_qc']			= $this->input->post('qc_code');
					$params['tgl_in']			= $this->input->post('tgl_in');
					$params['user_name']		= $this->input->post('uname');
					$params['kd_parent']		= str_replace("\n", "", $this->input->post('parent_code'));
					$params['kd_parent']		= ($params['kd_parent'] != "OL1") ? $params['kd_parent'] : "";
					// $params['production_date']  = $this->input->post('production_date');

				}else{

					$params['kd_receiving']		= $param['receiving_code'];
					$params['kd_barang']		= $param['item_code'];
					$params['kd_unik']			= $param['unique_code'];
					$params['kd_batch']			= $param['batch_code'];
					$params['tgl_exp']			= $param['tgl_exp'];
					$params['qty']				= $param['qty'];
					$params['kd_qc']			= $param['qc_code'];
					$params['tgl_in']			= $param['tgl_in'];
					$params['user_name']		= $param['uname'];
					$params['kd_parent']		= str_replace("\n", "", $param['parent_code']);
					$params['kd_parent']		= ($params['kd_parent'] != "OL1") ? $params['kd_parent'] : "";
					// $params['production_date']  = $param['production_date'];

				}

				if($params['kd_receiving'] == '' || $params['kd_barang'] == '' || $params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{

					$WTvalidation = $this->api_receiving_model->wtValidation($params);
					if($WTvalidation){

					// $this->db
					// 	->from('item_receiving_details')
					// 	->where('unique_code',$params['kd_unik']);

					// $check = $this->db->get()->row_array();

					// if(!$check){

						$validSN = $this->api_receiving_model->validSN($params['kd_unik'],$params['kd_receiving']);
						if(!$validSN){

							$dataValidSerial = array(
								"kd_receiving"	=>	$params['kd_receiving'],
								"kd_barang"		=>	$params['kd_barang'],
								"qty"			=>	$params['qty']
							);

							$this->db->trans_start();

							$serialized = $this->api_receiving_model->get_serial($params)->row_array();
							if($serialized){

								if($serialized['item_code'] == $params['kd_barang']){

									$this->api_receiving_model->update($params);
									$data['message']	= 'Data is returned because ' . $params['kd_unik'] . ' has an item code.';


									$this->db->trans_complete();
									$status = $this->db->trans_status();

									if($status === FALSE){

										$data['param']		= $params['kd_unik'];
										$data['status']		= 400;
										$data['message']	= 'Failed to update data.';
										$data['response']	= false;

									}else{

										$data['param']		= $params['kd_unik'];
										$data['status']		= 200;
										$data['response']	= true;

									}

								}else{

									$data['param']		= $params['kd_unik'];
									$data['status']		= 400;
									$data['response']	= false;
									$data['message']	= "Serial number ini sudah di scanned pada item ".$serialized['item_code'];

								}

							} else{

								$validSerial = $this->api_receiving_model->validQty($dataValidSerial);
								if($validSerial){

									$this->api_receiving_model->start_receiving($params['kd_receiving']);
	
									$action = $this->api_receiving_model->insert($params);
									if($action['status']){
										$data['message']	= 'Data has been inserted.';
			
										$this->db->trans_complete();
										$status = $this->db->trans_status();

										if($status === FALSE){

											$data['param']		= $params['kd_unik'];
											$data['status']		= 400;
											$data['message']	= 'Failed to update data.';
											$data['response']	= false;

										}else{

											$data['param']		= $params['kd_unik'];
											$data['status']		= 200;
											$data['response']	= true;

										}
									}else{

										$this->db->trans_complete();

										$data['param']		= $params['kd_unik'];
										$data['status']		= 400;
										$data['message']	= $action['message'];
										$data['response']	= false;

									}


								}else{

									$data['param']		= $params['kd_unik'];
									$data['status']		= 400;
									$data['message']	= 'Quantity is full';
									$data['response']	= false;

								}
							}

						}else{

							$data['param']		= $params['kd_unik'];
							$data['status']		= 400;
							$data['message']	= "This serial number '".$params['kd_unik']."' already exist.";
							$data['response']	= true;

						}

					}else{

						$data['param']		= $params['kd_unik'];
						$data['status']		= 400;
						$data['message']	= 'Barang ini tidak tersedia di '.$params['kd_receiving'];
						$data['response']	= true;

					}

					// }else{

					// 	$data['param']		= $params['kd_unik'];
					// 	$data['status']		= 400;
					// 	$data['message']	= 'Data has been scanned';
					// 	$data['response']	= false;

					// }

				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		if(empty($param)){
			echo json_encode($data);
		}else{
			return $data;
		}
	}

	public function post_json()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$posts = json_decode($this->input->post('data'));

				if(!empty($posts) && count($posts) > 0){

					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= array();

					foreach($posts as $post){

						$params['receiving_code']	= $post->receiving_code;
						$params['item_code']		= $post->item_code;
						$params['unique_code']		= $post->serial_number;
						$params['batch_code']		= $post->batch_code;
						$params['tgl_exp']			= $post->tgl_exp;
						$params['qty']				= $post->qty;
						$params['qc_code']		 	= $post->qc_code;
						$params['tgl_in']			= $post->tgl_in;
						$params['uname']			= $post->uname;
						$params['parent_code']		= $post->parent_code;
						$params['parent_code']		= ($params['parent_code'] != "OL1") ? $params['parent_code'] : "";
						// $params['production_date']	= $post->production_date;

						$data['results'][] = $this->post($params);
					}

				}else{

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Empty Parameter';
					$data['response']	= false;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_item($receiving_code = '', $item_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $receiving_code == '' || $item_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_receiving_model->get_item($receiving_code, $item_code)->row_array();
				if($result){
					if($result['st_receive'] == 0){
						$data['param']		= $item_code;
						$data['status']		= 200;
						$data['message']	= $item_code . ' is registered and available.';
						$data['response']	= true;
					} else{
						$data['param']		= $item_code;
						$data['status']		= 401;
						$data['message']	= $item_code . ' has been scanned before.';
						$data['response']	= true;
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $item_code;
					$data['message']	= $item_code . ' is not registered.';
					$data['response']	= true;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function lock_item()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_receiving']		= $this->input->post('receiving_code');
				$params['kd_barang']		= $this->input->post('item_code');

				if($params['kd_receiving'] == '' || $params['kd_barang'] == ''){
					$data['param']		= $params['kd_barang'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{
					$result = $this->api_receiving_model->get_item($receiving_code, $item_code)->row_array();
					if($result){
						$this->api_receiving_model->lock_item($receiving_code, $item_code);
						$data['param']		= $params['kd_barang'];
						$data['status']		= 200;
						$data['message']	= 'Data has been updated.';
						$data['response']	= true;
					} else{
						$data['param']		= $params['kd_barang'];
						$data['status']		= 401;
						$data['message']	= 'Failed to update data.';
						$data['response']	= true;
					}
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_serial($receiving_code = '', $serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $receiving_code == '' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$serialized = $this->api_receiving_model->get_serial_lp($receiving_code, $serial_number)->row_array();
				if($serialized){
					if($serialized['id_barang'] == null){
						$data['param']		= $serial_number;
						$data['status']		= 'AVAILABLE';
						$data['message']	= $serial_number . ' is registered and available.';
						$data['response']	= true;
						$this->api_receiving_model->start_receiving($receiving_code);
					} else{
						$data['param']		= $serial_number;
						$data['status']		= 'REGISTERED';
						$data['message']	= $serial_number . ' has been registered before.';
						$data['response']	= false;
					}
				} else{
					$data['status']		= 'AVAILABLE';
					$data['param']		= $serial_number;
					$data['message']	= $serial_number . ' is not registered.';
					$data['response']	= true;
					$this->api_receiving_model->start_receiving($receiving_code);
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_qc()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= 'QC';
				$data['status']		= 200;
				$data['message']	= 'Getting QC list.';
				$data['response']	= true;
				$data['results']	= $this->api_receiving_model->get_qc()->result_array();
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_valid_serial($params=array())
	{

		$method = $_SERVER['REQUEST_METHOD'];

		if(empty($params)){

			$receiving_code = $this->input->post('receiving_code');
			$serial_number = $this->input->post('serial_number');
			$item_code = $this->input->post('item_code');
			$qty = $this->input->post('qty');
			$qty2 = $this->input->post('qty2');

		}else{

			$receiving_code = $params['receiving_code'];
			$serial_number 	= $params['serial_number'];
			$item_code 		= $params['item_code'];
			$qty 			= $params['qty'];
			$qty2 			= 0;

		}

		if($receiving_code == '' || $serial_number == '' || $item_code == '' || $qty == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$params = array(
					"kd_receiving"	=>	$receiving_code,
					"kd_barang"		=>	$item_code,
					"qty"			=>	($qty+$qty2)
				);

				$serialized = $this->api_receiving_model->validQty($params);
				if($serialized){
					$data['status']		= 'Available';
					$data['param']		= $serial_number;
					$data['message']	= 'available';
					$data['response']	= true;
				} else{
					$data['status']		= 'Unavailable';
					$data['param']		= $serial_number;
					$data['message']	= 'quantity is too much';
					$data['response']	= false;
				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		if(!empty($params)){
			return $data;
		}else{
			echo json_encode($data);
		}
	}

	public function outer_label_item(){

		$method = $_SERVER['REQUEST_METHOD'];

		$params['receiving_code'] = $this->input->post('receiving_code');
		$params['parent_code'] = $this->input->post('outer_label');

		if($method != 'POST' || $params['parent_code'] == ''){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$datas = $this->api_receiving_model->getOuterLabelItem($params);
				if($datas){
					$data['param']		= $params['parent_code'];
					$data['status']		= 200;
					$data['response']	= true;
					$data['message']	= $datas;
				} else{
					$data['param']		= $params['parent_code'];
					$data['status']		= 400;
					$data['response']	= false;
					$data['message']	= 'Outer Label not available';
				}

			} else{
				$data['param']		= $params['parent_code'];
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);

	}

	public function get_doc_type($receiving_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $receiving_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$received = $this->api_receiving_model->getDocType($receiving_code);
				if($received){
					$data['param']		= $receiving_code;
					$data['status']		= 200;
					$data['message']	= $received['document_id'];
					$data['response']	= true;
				} else{
					$data['status']		= 401;
					$data['param']		= $receiving_code;
					$data['message']	= $receiving_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post_bulk()
	{

		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			// $auth = $this->api_setting_model->user_authentication();
			// if($auth){

				$serialNumber = $this->input->post('serial_number');

				if(strpos($serialNumber, ",")){
					$serialNumber = explode(",", $serialNumber);
				}

				if(!empty($serialNumber)){

					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= array();

					if(is_array($serialNumber)){
						foreach ($serialNumber as $sn) {


							$params['receiving_code']	= $this->input->post('receiving_code');
							$params['item_code']		= $this->input->post('item_code');
							$params['unique_code']		= $sn;
							$params['batch_code']		= "";
							$params['tgl_exp']			= "0000-00-00";
							$params['qty']				= 1;
							$params['qc_code']		 	= "GOOD";
							$params['tgl_in']			= date('Y-m-d');
							$params['uname']			= "will";
							$params['parent_code']		= $this->input->post('parent_code');
							$params['parent_code']		= ($params['parent_code'] != "OL1") ? $params['parent_code'] : "";

							$data['results'][] = $this->post($params);
						}

					}else{

							$params['receiving_code']	= $this->input->post('receiving_code');
							$params['item_code']		= $this->input->post('item_code');
							$params['unique_code']		= $serialNumber;
							$params['batch_code']		= "";
							$params['tgl_exp']			= "0000-00-00";
							$params['qty']				= 1;
							$params['qc_code']		 	= "GOOD";
							$params['tgl_in']			= date('Y-m-d');
							$params['uname']			= "will";
							$params['parent_code']		= $this->input->post('parent_code');
							$params['parent_code']		= ($params['parent_code'] != "OL1") ? $params['parent_code'] : "";

							$data['results'][] = $this->post($params);
					}

				}else{

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Empty Parameter';
					$data['response']	= false;

				}

			// } else{
			// 	$data['param']		= null;
			// 	$data['status']		= 401;
			// 	$data['message']	= 'Unauthorized user.';
			// 	$data['response']	= false;
			// }
		}
		echo json_encode($data);
	}

	public function postReceivingByPicking($receivingCode="",$pickingCode="",$receivingCode2=""){

		if($pickingCode != ""){

			$receivingCondition="";
			$receivingCondition2="";

			$receivingCondition=" AND unique_code NOT IN (SELECT unique_code FROM item_receiving_details ird JOIN item_receiving ir ON ird.item_receiving_id=ir.id JOIN receivings r ON r.id=ir.receiving_id WHERE r.code='$receivingCode')";

			if($receivingCode2 != ""){
				$receivingCondition2 = " AND unique_code NOT IN (SELECT unique_code FROM item_receiving_details ird JOIN item_receiving ir ON ird.item_receiving_id=ir.id JOIN receivings r ON r.id=ir.receiving_id WHERE r.code='$receivingCode2')";
			}

			$this->db
				->select(["itm.code as item_code","pr.unique_code","ird.parent_code"])
				->from('pickings p')
				->join('picking_recomendation pr',"pr.picking_id=p.pl_id ".$receivingCondition.$receivingCondition2)
				->join('item_receiving_details ird','ird.picking_id=pr.picking_id and  ird.unique_code=pr.unique_code','left')
				->join('items itm','itm.id=pr.item_id','left')
				->where('p.name',$pickingCode);

			$datas= $this->db->get()->result_array();

			if($datas){

				$data['status']		= 200;
				$data['response']	= true;
				$data['results']	= array();

				foreach ($datas as $d) {

					$params['receiving_code']	= $receivingCode;
					$params['item_code']		= $d['item_code'];
					$params['unique_code']		= $d['unique_code'];
					$params['batch_code']		= "";
					$params['tgl_exp']			= "0000-00-00";
					$params['qty']				= 1;
					$params['qc_code']		 	= "GOOD";
					$params['tgl_in']			= date('Y-m-d');
					$params['uname']			= "largo";
					$params['parent_code']		= $d['parent_code'];
					$params['parent_code']		= ($params['parent_code'] != "OL1") ? $params['parent_code'] : "";

					$data['results'][] = $this->post($params);

				}

				echo json_encode($data);

			}

		}

	}

	public function get_exp_date_by_batch($batch=""){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();
			if(!$auth){

				if(!empty($batch)){

					$data['status']		= 200;
					$data['message']	= 'Success';
					$data['response']	= true;
					$data['message']	= true;
					$data['results']	= NULL;

					$this->db
						->select('kd_batch as batch, tgl_exp as exp_date')
						->from('item_receiving_details')
						->where('kd_batch',$batch)
						->group_by('batch, exp_date')
						->limit(1);

					$d = $this->db->get()->row_array();

					if($d){
						$data['results']	= $d;
					}

				}else{

					$data['params']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Batch is empty.';
					$data['response']	= false;

				}

			}else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}

		}

		echo json_encode($data);

	}

	public function getValidOuterLabel(){

		$method = $_SERVER['REQUEST_METHOD'];

		$params['receiving_code'] = $this->input->post('receiving_code');
		$params['parent_code'] = $this->input->post('outer_label');
		$params['item_code'] = $this->input->post('item_code');

		if($method != 'POST' || $params['parent_code'] == '' || $params['receiving_code'] == '' || $params['item_code'] == ''){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$datas = $this->api_receiving_model->getValidOuterLabel($params['receiving_code'],$params['parent_code'],$params['item_code']);
				if($datas){
					$data['param']		= $params['parent_code'];
					$data['status']		= 200;
					$data['response']	= true;
					$data['message']	= 'Outer Label is available.';
				} else{
					$data['param']		= $params['parent_code'];
					$data['status']		= 400;
					$data['response']	= false;
					$data['message']	= 'Outer Label not available.';
				}

			} else{
				$data['param']		= $params['parent_code'];
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);

	}

}

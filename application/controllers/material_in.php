<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class material_in extends MY_Controller {

    private $class_name;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        /* Otoritas */
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array('report_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Material In Report';
        $data['page'] = $this->class_name . '/index';

        $this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
        $data['jsPage'][] = "assets/pages/scripts/report/lr-material-in.js";

        $data['today'] = date('d/m/Y');
        $data['source'] = $this->report_model->getSource('in');

        $this->load_model('item_stok_model');
        $data['item'] = $this->item_stok_model->getItem();

        /* INSERT LOG */
        $this->access_right->activity_logs('view','Report Material In');
        /* END OF INSERT LOG */

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

    public function getItemCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->report_model->getItemCode($params);

		echo json_encode($result);
	}

    private function include_required_file(&$data){
        $data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
        $data['jsPage'][] = 'assets/custom/js/share.js';
    }

    public function export_material_in(){
        $data = array();

        $params = array(
            'group_by'      => filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
            'source_id'     => filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
            'source_type'   => filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
            'source_name'   => filter_var(trim($this->input->post('source_name')), FILTER_SANITIZE_STRING),
            'from'          => filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
            'to'            => filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'                => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'                => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
        );

        $data['page'] = $this->class_name . '/material_in_export';
        $data['params'] = $params;
        $data['data'] = $this->report_model->getMaterialIn($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "material_in.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            10, // margin bottom
            10, // margin header
            10
        ); // margin footer

        $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->Output($path, 'I'); // save to file because we can

    }

    public function export_excel_old(){

        ini_set('display_errors', false);

        error_reporting(-1);

        register_shutdown_function(function(){
                $error = error_get_last();
                if(null !== $error)
                {
                    header('location:error_limit');
                }
            });

        try
        {

        $params = array(
            'group_by'      => filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
            'source_id'     => filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
            'source_type'   => filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
            'source_name'   => filter_var(trim($this->input->post('source_name')), FILTER_SANITIZE_STRING),
            'from'          => filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
            'to'            => filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'                => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'                => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
        );

        $this->load->library('PHPExcel');
        $data = $this->report_model->getMaterialIn($params);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Material In Report")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);
        $sheet->getColumnDimension('P')->setAutoSize(true);
        $sheet->getColumnDimension('Q')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'Group By')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, ucfirst(strtolower($params['group_by'])))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 2, 'Source Type')->getStyle('A2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 2, ucfirst($params['source_type']))->getStyle('B2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 3, 'Source Name')->getStyle('A3')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 3, ucfirst($params['source_name']))->getStyle('B3')->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(3, 1, 'From')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, $params['from'])->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 2, 'To')->getStyle('D2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 2, $params['to'])->getStyle('E2')->getFont()->setBold(true);

        if(strtolower($params['group_by']) == 'date')
            $gr='Periode';
        else
            $gr='Source';
        $sheet->setCellValueByColumnAndRow(0, 5, 'No')->getStyle('A5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 5, 'Group By '.$gr)->getStyle('B5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 5, 'Inbound Doc. #')->getStyle('C5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 5, 'Source')->getStyle('D5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 5, 'Rec. Doc. #')->getStyle('E5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 5, 'Items Code')->getStyle('F5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(6, 5, 'LPN Number')->getStyle('G5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(7, 5, 'Items Name')->getStyle('H5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(8, 5, 'Doc. Qty')->getStyle('I5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(9, 5, 'Rec. Qty')->getStyle('J5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(10, 5, 'Disc')->getStyle('K5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(11, 5, 'UoM')->getStyle('L5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(12, 5, 'Remark')->getStyle('M5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(13, 5, 'User')->getStyle('N5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(14, 5, 'Start Tally')->getStyle('O5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(15, 5, 'Finish Tally')->getStyle('P5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(16, 5, 'Start Putaway')->getStyle('Q5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(17, 5, 'Finish Putaway')->getStyle('R5')->getFont()->setBold(true);

        $row = 6;

        $data = $data['data'];

        foreach($data as $k => $v){

            $dt = key($data[$k]);
            $sheet->setCellValueByColumnAndRow(0, $row, $dt)->getStyle('A'.$row)->getFont()->setBold(true);

            $row++;

            $detailData = $v[$dt];
            $iLen = count($detailData);

            for($j = 0; $j < $iLen; $j++){
                $sheet->setCellValueByColumnAndRow(0, $row, $detailData[$j]['tgl_inbound']);
                $sheet->setCellValueByColumnAndRow(1, $row, '');
                $sheet->setCellValueByColumnAndRow(2, $row, $detailData[$j]['kd_inbound']);
                $sheet->setCellValueByColumnAndRow(3, $row, $detailData[$j]['source']);
                $sheet->setCellValueByColumnAndRow(4, $row, $detailData[$j]['kd_receiving']);
                $sheet->setCellValueByColumnAndRow(5, $row, $detailData[$j]['item_code']);
                $sheet->setCellValueByColumnAndRow(6, $row, $detailData[$j]['lpn']);
                $sheet->setCellValueByColumnAndRow(7, $row, $detailData[$j]['item_name']);
                $sheet->setCellValueByColumnAndRow(8, $row, $detailData[$j]['doc_qty']);
                $sheet->setCellValueByColumnAndRow(9, $row, $detailData[$j]['rec_qty']);
                $sheet->setCellValueByColumnAndRow(10, $row, abs($detailData[$j]['disc']));
                $sheet->setCellValueByColumnAndRow(11, $row, $detailData[$j]['nama_satuan']);
                $sheet->setCellValueByColumnAndRow(12, $row, $detailData[$j]['remark']);
                $sheet->setCellValueByColumnAndRow(13, $row, $detailData[$j]['nama']);
                $sheet->setCellValueByColumnAndRow(14, $row, $detailData[$j]['start_tally']);
                $sheet->setCellValueByColumnAndRow(15, $row, $detailData[$j]['finish_tally']);
                $sheet->setCellValueByColumnAndRow(16, $row, $detailData[$j]['start_putaway']);
                $sheet->setCellValueByColumnAndRow(17, $row, $detailData[$j]['finish_putaway']);

                $row++;
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Material In Report'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

        }
        catch(\Exception $exception)
        {

            $this->load->view('components/error_memory_limit');

        }
    }

    public function export_excel(){

        ini_set('display_errors', false);

        error_reporting(-1);

        register_shutdown_function(function(){
                $error = error_get_last();
                if(null !== $error)
                {
                    header('location:error_limit');
                }
            });

        try
        {

        $params = array(
            'group_by'      => filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
            'source_id'     => filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
            'source_type'   => filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
            'source_name'   => filter_var(trim($this->input->post('source_name')), FILTER_SANITIZE_STRING),
            'from'          => filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
            'to'            => filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'                => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'                => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
        );

        $this->load->library('PHPExcel');
        $data = $this->report_model->getMaterialIn($params);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Material In Report")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);
        $sheet->getColumnDimension('P')->setAutoSize(true);
        $sheet->getColumnDimension('Q')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'Group By')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, ucfirst(strtolower($params['group_by'])))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 2, 'Source Type')->getStyle('A2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 2, ucfirst($params['source_type']))->getStyle('B2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 3, 'Source Name')->getStyle('A3')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 3, ucfirst($params['source_name']))->getStyle('B3')->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(3, 1, 'From')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, $params['from'])->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 2, 'To')->getStyle('D2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 2, $params['to'])->getStyle('E2')->getFont()->setBold(true);

        if(strtolower($params['group_by']) == 'date')
            $gr='Periode';
        else
            $gr='Source';
        $sheet->setCellValueByColumnAndRow(0, 5, 'No')->getStyle('A5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 5, 'Group By '.$gr)->getStyle('B5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 5, 'Inbound Doc. #')->getStyle('C5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 5, 'Source')->getStyle('D5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 5, 'Rec. Doc. #')->getStyle('E5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 5, 'Items Code')->getStyle('F5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(6, 5, 'LPN Number')->getStyle('G5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(7, 5, 'Items Name')->getStyle('H5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(8, 5, 'Doc. Qty')->getStyle('I5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(9, 5, 'Rec. Qty')->getStyle('J5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(10, 5, 'Disc')->getStyle('K5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(11, 5, 'UoM')->getStyle('L5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(12, 5, 'Remark')->getStyle('M5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(13, 5, 'User')->getStyle('N5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(14, 5, 'Start Tally')->getStyle('O5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(15, 5, 'Finish Tally')->getStyle('P5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(16, 5, 'Start Putaway')->getStyle('Q5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(17, 5, 'Finish Putaway')->getStyle('R5')->getFont()->setBold(true);

        $row = 6;

        $data = $data['data'];

        foreach($data as $k => $v){

            $dt = key($data[$k]);
            $sheet->setCellValueByColumnAndRow(0, $row, $dt)->getStyle('A'.$row)->getFont()->setBold(true);

            $row++;

            $detailData = $v[$dt];
            $iLen = count($detailData);

            for($j = 0; $j < $iLen; $j++){
                $sheet->setCellValueByColumnAndRow(0, $row, $detailData[$j]['tgl_inbound']);
                $sheet->setCellValueByColumnAndRow(1, $row, '');
                $sheet->setCellValueByColumnAndRow(2, $row, $detailData[$j]['kd_inbound']);
                $sheet->setCellValueByColumnAndRow(3, $row, $detailData[$j]['source']);
                $sheet->setCellValueByColumnAndRow(4, $row, $detailData[$j]['kd_receiving']);
                $sheet->setCellValueByColumnAndRow(5, $row, $detailData[$j]['item_code']);
                $sheet->setCellValueByColumnAndRow(6, $row, $detailData[$j]['lpn']);
                $sheet->setCellValueByColumnAndRow(7, $row, $detailData[$j]['item_name']);
                $sheet->setCellValueByColumnAndRow(8, $row, $detailData[$j]['doc_qty']);
                $sheet->setCellValueByColumnAndRow(9, $row, $detailData[$j]['rec_qty']);
                $sheet->setCellValueByColumnAndRow(10, $row, abs($detailData[$j]['disc']));
                $sheet->setCellValueByColumnAndRow(11, $row, $detailData[$j]['nama_satuan']);
                $sheet->setCellValueByColumnAndRow(12, $row, $detailData[$j]['remark']);
                $sheet->setCellValueByColumnAndRow(13, $row, $detailData[$j]['nama']);
                $sheet->setCellValueByColumnAndRow(14, $row, $detailData[$j]['start_tally']);
                $sheet->setCellValueByColumnAndRow(15, $row, $detailData[$j]['finish_tally']);
                $sheet->setCellValueByColumnAndRow(16, $row, $detailData[$j]['start_putaway']);
                $sheet->setCellValueByColumnAndRow(17, $row, $detailData[$j]['finish_putaway']);

                $row++;
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Material In Report'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

        }
        catch(\Exception $exception)
        {

            $this->load->view('components/error_memory_limit');

        }
    }

    //REQUEST
    public function getMaterialIn(){
        $result = array();

        $params = array(
            'group_by'      => filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
            'source_id'     => filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
            'source_type'   => filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
            'from'          => filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
            'to'            => filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'            => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'          => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
        );

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

        $result = $this->report_model->getMaterialIn($params);

        echo json_encode($result);
    }


    public function export_excel_detail(){

        ini_set('display_errors', false);

        error_reporting(-1);

        register_shutdown_function(function(){
                $error = error_get_last();
                if(null !== $error)
                {
                    header('location:error_limit');
                }
            });

        try
        {

        $params = array(
            'group_by'      => filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
            'source_id'     => filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
            'source_type'   => filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
            'source_name'   => filter_var(trim($this->input->post('source_name')), FILTER_SANITIZE_STRING),
            'from'          => filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
            'to'            => filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'            => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'          => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
        );

        $this->load->library('PHPExcel');
        $data = $this->report_model->getMaterialInDetail($params);

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="Materil In Report.csv"');
        $fp = fopen('php://output', 'wb');

        $head = ['No','Inbound Doc.','Rec. Doc #','Serial Number','Serial Number Visual','Outer Label','Item Code','Item Name','Qty','UoM','User'];
        fputcsv($fp, $head);

        $i=1;
        foreach ( $data as $d ) {

            $d['no'] = $i;
            fputcsv($fp, $d);
            $i++;
        }

        fclose($fp);

        }
        catch(\Exception $exception)
        {

            $this->load->view('components/error_memory_limit');

        }

    }

    public function error_limit(){
        $this->load->view('components/error_memory_limit');
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		$this->toJson($data);
	}

	protected function toJson($payload,$stat=200){

		header('Content-Type:application/json');
		set_status_header($stat);
		echo json_encode($payload);
		exit;

	}

	protected function errorMessage($message="Error.",$param="",$stat=400){

		$payload = array(
			'param' 	=> (!empty($param)) ? $param : NULL,
			'status' 	=> $stat,
			'message'	=> $message,
			'response'	=> false
		);

		header('Content-Type:application/json');
		set_status_header($stat);
		echo json_encode($payload);
		exit;

	}

	protected function method($method=""){

		if($method !== $_SERVER['REQUEST_METHOD']){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

			$this->output
		        ->set_content_type('application/json')
		        ->set_status_header(400)
		        ->set_output(json_encode($data))
		        ->_display();

		    exit;

		}

	}

	protected function log($body,$response){
		
		$data = array(
			"body"		=> $body,
			"response"	=> $response,
			"url"		=> $this->uri->uri_string(),
			"username"	=> $this->CI->input->get_request_header('User', true),
			"token"		=> $this->CI->input->get_request_header('Authorization', true),
			"method"	=> $_GET['REQUEST_METHOD']
		);

		$this->db->insert('api_log',$data);

	}

}

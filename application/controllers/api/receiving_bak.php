<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require "Api.php";
class receiving extends Api {

	function __construct() {

		parent::__construct();
		$this->auth->user_authentication();
		$this->load_model('api_receiving_model');
		$this->load_model('api_putaway_model');

	}

	public function get_document()
	{

		$this->method("GET");

		$data['param']		= 'RECEIVING';
		$data['status']		= 200;
		$data['response']	= true;
		$data['message']	= 'Get Document for Receiving.';
		$data['results']	= $this->api_receiving_model->get_document();

		$this->toJson($data);

	}

	public function get_document_item($receivingCode='', $itemCode='')
	{

		$this->method("GET");

		if(empty($receivingCode)){
			$this->index();
		}

		$results = $this->api_receiving_model->get_document_item($receivingCode,$itemCode);

		if($results){

			if(!$results[0]['status']){

				foreach($results as &$r) {
					unset($r['status']);
				}

				$data['param']		= 'RECEIVING';
				$data['status']		= 200;
				$data['response']	= true;
				$data['message']	= 'Get Document Item for Receiving.';
				$data['results']	= $results;


			}else{

				$data['param']		= 'RECEIVING';
				$data['status']		= 400;
				$data['response']	= false;
				$data['message']	= 'Receiving Document has been received !';

			}

		}else{

			$data['param']		= 'RECEIVING';
			$data['status']		= 404;
			$data['response']	= false;
			$data['message']	= 'Receiving Document Not Found !';

		}

		$this->toJson($data);

	}


	public function post_putaway_serial(){

		$this->method("POST");

		$serialNumber = $this->input->post('serial_number');

		if(empty($serialNumber)){
			$this->index();
		}


		$results = $this->api_putaway_model->putaway_serial($serialNumber);

		if($results){

			if($results['status']){

				unset($results['status']);

				$data['param']		= 'RECEIVING';
				$data['status']		= 200;
				$data['response']	= true;
				$data['message']	= 'Serial Number Ready for Putaway.';
				$data['results']	= $results;

			}else{

				$data['param']		= 'RECEIVING';
				$data['status']		= 400;
				$data['response']	= false;
				$data['message']	= 'Serial Number belum selesai di tally.';

			}

		}else{

			$data['param']		= 'RECEIVING';
			$data['status']		= 401;
			$data['response']	= false;
			$data['message']	= 'Serial Number Unvailable !';

		}

		$this->toJson($data);

	}

}

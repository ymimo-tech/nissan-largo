<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require "Api.php";
class picking extends Api {

	function __construct() {

		parent::__construct();
		$this->auth->user_authentication();
		$this->load_model('api_picking_model');

	}

	public function get_document($username = '')
	{

		$this->method("GET");

		$data['param']		= 'PICKING';
		$data['status']		= 200;
		$data['response']	= true;
		$data['message']	= 'Get Document for Picking.';
		$data['results']	= $this->api_picking_model->get_document($username);
		$this->toJson($data);

	}

	public function get_document_item($pickingCode='', $item_area='')
	{

		// $this->method("POST");

		$pickingCode = $this->input->post('pickingCode');
		// dd(json_decode($pickingCode));
		$item_area = $this->input->post('item_area');

		if(empty($pickingCode)){
			$this->index();
		}

		$results = $this->api_picking_model->get_document_item(json_decode($pickingCode),$item_area);

		if($results){

			if(!$results[0]['status']){

				foreach($results as &$r) {
					unset($r['status']);
				}

				$data['param']		= 'Picking';
				$data['status']		= 200;
				$data['response']	= true;
				$data['message']	= 'Get Document Item for Picking.';
				$data['results']	= $results;


			}else{

				$data['param']		= 'Picking';
				$data['status']		= 400;
				$data['response']	= false;
				$data['message']	= 'Picking Document has been received !';

			}

		}else{

			$data['param']		= 'Picking';
			$data['status']		= 404;
			$data['response']	= false;
			$data['message']	= 'Picking Document Not Found !';

		}

		$this->toJson($data);

	}

}

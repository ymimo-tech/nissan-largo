<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_picking extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_picking_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function load()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= 'PICKING';
				$data['status']		= 200;
				$data['message']	= 'Load data for available picking list.';
				$data['response']	= true;
				$data['results']	= $this->api_picking_model->load()->result_array();
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get($picking_code = '', $outbound_code= '', $item_code = '')
	{
		$item_code = str_replace('%7C','*',$item_code);
		$outbound_code = str_replace('%7C','/',$outbound_code);
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $picking_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$picked = $this->api_picking_model->get($picking_code)->row_array();
				if($picked){
					$data['param']		= $picking_code;
					$data['status']		= 200;
					$data['message']	= $picking_code . ' is available.';
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->retrieve($picking_code,$outbound_code, $item_code)->result_array();
					$data['locations']	= $this->api_picking_model->retrieve_location($picking_code,$outbound_code, $item_code);
					$data['results']	= array_merge($data['results'],$data['locations']);
					// dd($data);
				} else{
					$data['param']		= $picking_code;
					$data['status']		= 401;
					$data['message']	= $picking_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		// dd($data);
		echo json_encode($data);
	}

	public function get_picked($picking_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $picking_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$picked = $this->api_picking_model->get($picking_code)->row_array();
				if($picked){
					// if($picked['pl_status'] != 1){
						$data['param']		= $picking_code;
						$data['status']		= 200;
						$data['message']	= $picking_code . ' is available.';
						$data['response']	= true;
						$data['results']	= $this->api_picking_model->get_picked($picking_code)->result_array();
					// } else{
					// 	$data['param']		= $picking_code;
					// 	$data['status']		= 401;
					// 	$data['message']	= $picking_code . ' has been picked before.';
					// 	$data['response']	= false;
					// }
				} else{
					$data['param']		= $picking_code;
					$data['status']		= 401;
					$data['message']	= $picking_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_batch($item_code = '', $batch_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $item_code == '' || $batch_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_picking_model->get_batch($item_code, $batch_code)->row_array();
				if($result){
					$data['param']		= $batch_code;
					$data['status']		= 200;
					$data['message']	= $batch_code . ' is valid.';
					$data['response']	= true;
				} else{
					$data['param']		= $batch_code;
					$data['status']		= 401;
					$data['message']	= $batch_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_serial($picking_code = '', $serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $picking_code == '' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_picking_model->get_serial($serial_number)->row_array();
				if($result){

					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= $serial_number . ' is valid.';
					$data['response']	= true;
					$data['time']		= date("Y-m-d H:i:s");
					$data['locations']	= $this->api_picking_model->retrieve_serial($serial_number,$picking_code)->result_array();
					$this->api_picking_model->start($picking_code);

				} else{

					$result = $this->api_picking_model->get_serial_lp($serial_number)->row_array();
					if($result){

						$data['param']		= $serial_number;
						$data['status']		= 200;
						$data['message']	= $serial_number . ' is valid.';
						$data['response']	= true;
						$data['time']		= date("Y-m-d H:i:s");
						$data['locations']	= $this->api_picking_model->retrieve_serial_lp($serial_number)->result_array();
						$this->api_picking_model->start($picking_code);

					}else{

						$data['param']		= $serial_number;
						$data['status']		= 401;
						$data['message']	= $serial_number . ' is not valid.';
						$data['response']	= false;
						$data['time']		= date("Y-m-d H:i:s");
					}

				}

			} else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}
		echo json_encode($data);
	}

	private function _get_serial($params){

		$data = array();

		$result = $this->api_picking_model->get_serial($params['kd_unik'])->row_array();
		if($result){

			$serial = $this->api_picking_model->retrieve_serial($params['kd_unik'], $params['pl_name'])->row_array();
			if($serial){
				if($serial['last_qty'] >= $params['qty']){

					return true;

				}else{

					$data['param']		= $params['kd_unik'];
					$data['status']		= 400;
					$data['message']	= $params['kd_unik'].' stock not available.';
					$data['response']	= false;

				}

			}else{

				$data['param']		= $params['kd_unik'];
				$data['status']		= 400;
				$data['message']	= $params['kd_unik'].' stock not available.';
				$data['response']	= false;

			}

		} else{

			$result = $this->api_picking_model->get_serial_lp($params['kd_unik'])->row_array();
			if($result){

				$serial = $this->api_picking_model->retrieve_serial_lp($params);
				if($serial){

					return true;

				}else{

					$data['param']		= $params['kd_unik'];
					$data['status']		= 400;
					$data['message']	= $params['kd_unik']. ' stock not available.';
					$data['response']	= false;

				}

			}else{

				$serial = $this->api_picking_model->retrieve_serial_batch($params);
				if($serial){

					return true;

				}else{

					$data['param']		= $params['kd_unik'];
					$data['status']		= 400;
					$data['message']	= $params['kd_unik']. ' not available';
					$data['response']	= false;

				}

			}

		}

		echo json_encode($data);
		exit;
	}

	public function post_serial()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['pl_name']		= $this->input->post('picking_code');
				$params['kd_unik']		= strtoupper($this->input->post('serial_number'));
				$params['qty']			= $this->input->post('qty');
				$params['user_name']	= $this->input->post('user');
				$params['kit_time']		= $this->input->post('kit_time');
				$params['pick_time']	= $this->input->post('pick_time');
				$params['code']			= $this->input->post('serial_code');
				$params['tray_location'] = $this->input->post('tray_location');
				$params['outbound_code'] = $this->input->post('outbound_code');
				$params['unit_code']	= $this->input->post('unit_code');
				$params['sku']			= $this->input->post('sku');
				$params['item_id'] = $this->input->post('item_code');

				if($params['kd_unik'] == ''){

					$data['param']		= $params['kd_unik'];
					$data['status']		= 400;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;

				} else{

					// $this->_get_serial($params);

					$checkOrderItem = $this->api_picking_model->checkOrderItem($params);
					if($checkOrderItem){

						/* Get Parameter type */
						$getType = $this->api_picking_model->getType($params['kd_unik']);
						if($getType){

							/*Get UoM*/
							$params['unit_id']	= $this->api_picking_model->getUnitBySerial($params);
							if($params['unit_id']){

								$params['unit_id'] = $params['unit_id']->unit_id;

								/* Parameter Validation */
								switch ($getType) {
									case 'serial_number':
										$checkRecomendation = $this->api_picking_model->getPickingRecomendationBySerialNumber($params);
										$checkStock			= ($checkRecomendation['status']) ? $this->api_picking_model->checkStockSerialNumber($params) : false;
										$checkOrderQty		= ($checkStock) ? $this->api_picking_model->checkOrderQtySerialNumber($params) : false;
										$pickProcess		= ($checkOrderQty) ? $this->api_picking_model->post_serial($params) : false;
										break;

									case 'outer_label':
										$checkRecomendation = $this->api_picking_model->getPickingRecomendationByOuterLabel($params);
										$checkStock			= ($checkRecomendation['status']) ? $this->api_picking_model->checkStockOuterLabel($params) : false;
										$checkOrderQty		= $this->api_picking_model->checkOrderQtyOuterLabel($params);
										$pickProcess		= ($checkOrderQty) ? $this->api_picking_model->post_serial_by_outerlabel($params) : false;
										break;

									case 'batch':
										$checkRecomendation = $this->api_picking_model->getPickingRecomendationByOuterLabel($params);
										$checkStock			= ($checkRecomendation['status']) ? $this->api_picking_model->checkStockOuterLabel($params) : false;
										$checkOrderQty		= ($checkStock) ? $this->api_picking_model->checkOrderQtyOuterLabel($params) : false;
										$pickProcess		= ($checkOrderQty) ? $this->api_picking_model->post_serial_by_batch($params) : false;
										break;

									default:
										# code...
										break;
								}
								/* End - Parameter Validation */

								// dd($checkOrderQty);

								if($checkRecomendation['status']){
									if($checkStock){
										if($checkOrderQty){

											if($pickProcess['status']){

												$data['param']		= $params['kd_unik'];
												$data['status']		= 200;
												$data['message']	= 'Data has been updated.';
												$data['response']	= true;
												$data['item_code'] = $params['item_id'];

											}else{

												$data['param']		= $params['kd_unik'];
												$data['status']		= 400;
												$data['message']	= $process['message'];
												$data['response']	= false;

											}

											$this->api_picking_model->start($params['pl_name']);

											$this->load_model('api_putaway_model');
											$isFinish = $this->api_putaway_model->isFinish($params);
											if($isFinish){
												$this->api_putaway_model->finish($isFinish['kd_receiving']);
											}

										}else{

											$data['param']		= $params['kd_unik'];
											$data['status']		= 400;
											$data['message']	= 'Item Melebihi Kapasitas Dalam List.';
											// $data['message']	= 'Item tersebut tidak ada di '.$params['outbound_code'].'.';
											$data['response']	= false;

										}

									}else{

										$data['param']		= $params['kd_unik'];
										$data['status']		= 400;
										$data['message']	= "Stock not available";
										$data['response']	= false;

									}

								}else{

									$data['param']		= $params['kd_unik'];
									$data['status']		= 400;
									$data['message']	= 'Silahkan ambil ';

									foreach ($checkRecomendation['data'] as $cr) {
										$data['message'] .= $cr['unique_code'].' - '.$cr['location_name'].'<br/>';
									}

									$data['response']	= false;

								}

							}else{

								$data['param']		= $params['kd_unik'];
								$data['status']		= 400;
								$data['message']	= "Unit code doesn't exists.";
								$data['response']	= false;

							}

						}else{

							$data['param']		= $params['kd_unik'];
							$data['status']		= 400;
							$data['message']	= $params['kd_unik'].' is not available.';
							$data['response']	= false;

						}

					}else{

						$data['param']		= $params['kd_unik'];
						$data['status']		= 400;
						$data['message']	= "Item ini tidak ada dalam list.";
						$data['response']	= false;


					}

				}

			} else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function post_json(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$posts = json_decode($this->input->post('data'));
				// dd($posts);

				if(!empty($posts) && count($posts) > 0){

					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= array();

					foreach($posts as $post){

						$params['picking_code']		= $post->picking_name;
						$params['kd_barang']		= $post->item_code;
						$params['kd_unik']			= $post->serial_number;
						$params['old_loc']			= $post->old_loc;
						$params['qty']				= $post->qty;
						$params['uname_pick']		= $post->user_pick;
						$params['pick_time']		= $post->pick_time;
						$params['new_loc']			= $post->outbound_loc;
						$params['uname_put']		= $post->user_put;
						$params['put_time']			= $post->put_time;


						$data['results'][] = $this->post($params);

					}

					if($params){

						$this->db
							->from('pickings')
							->where('name',$params['picking_code'])
							->where('pl_status',0);

						$picking = $this->db->get()->row_array();
						if($picking){

							$equalizer = $this->api_picking_model->get_picked($params['picking_code']);
							$sum_qty = 0;
							$sum_single = 0;
							$sum_multi = 0;

							foreach ($equalizer->result() as $row) {
								$sum_qty += $row->qty;
								$sum_single += $row->single_picked;
								$sum_multi += $row->multiple_picked;
							}

							if($sum_qty == $sum_single || $sum_qty == $sum_multi){
								$finish = $this->api_picking_model->finish($params['picking_code']);
								if($finish){
									$this->api_picking_model->sendToStaging($params['picking_code']);
								}
							}

						}

					}


				}else{

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Empty Parameter';
					$data['results'] 	= 'nothing';
					$data['response']	= false;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);

	}

	public function post($param=array())
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				if(empty($param)){

					$params['picking_code']		= $this->input->post('picking_name');
					$params['kd_barang']		= $this->input->post('item_code');
					$params['kd_unik']			= $this->input->post('serial_number');
					$params['old_loc']			= $this->input->post('old_loc');
					$params['qty']				= $this->input->post('qty');
					$params['uname_pick']		= $this->input->post('user_pick');
					$params['pick_time']		= $this->input->post('pick_time');
					$params['new_loc']			= $this->input->post('outbound_loc');
					$params['uname_put']		= $this->input->post('user_put');
					$params['put_time']			= $this->input->post('put_time');

				}else{

					$params = $param;
				}

				if($params['picking_code'] == '' || $params['kd_unik'] == ''){

					$data['param']		= $params['kd_unik'];
					$data['status']		= 400;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;

				} else{

                    $this->api_picking_model->post($params);

					$data['param']		= $params['kd_unik'];
					$data['status']		= 200;
					$data['message']	= 'Data has been updated.';
					$data['response']	= true;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		if(empty($param)){
			echo json_encode($data);
		}else{
			return $data;
		}

	}

	public function get_location($location = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $location == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_picking_model->get_location($location)->row_array();
				if($result){
					$data['param']			= $location;
					$data['status']			= 200;
					$data['message']		= $location . ' is available.';
					$data['response']		= true;
				} else{
					$data['param']		= $location;
					$data['status']		= 401;
					$data['message']	= $location . ' is not available.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post_array()
	{

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'POST'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();

			if($auth){

				$params['picking_code']		= $this->input->post('picking_name');
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['old_loc']			= $this->input->post('old_loc');
				$params['qty']				= $this->input->post('qty');
				$params['uname_pick']		= $this->input->post('user_pick');
				$params['pick_time']		= $this->input->post('pick_time');
				$params['new_loc']			= $this->input->post('outbound_loc');
				$params['uname_put']		= $this->input->post('user_put');
				$params['put_time']			= $this->input->post('put_time');

				$this->api_picking_model->update_location($params['picking_code'][0],$params);

				$equalizer = $this->api_picking_model->get_picked($params['picking_code'][0]);
				$sum_qty = 0;
				$sum_single = 0;
				$sum_multi = 0;

				foreach ($equalizer->result() as $row) {
					$sum_qty += $row->qty;
					$sum_single += $row->single_picked;
					$sum_multi += $row->multiple_picked;
				}

				if($sum_qty == $sum_single || $sum_qty == $sum_multi){
					$this->api_picking_model->finish($params['picking_code'][0]);
				}

				$data['param']		= $params['picking_code'][0];
				$data['status']		= 200;
				$data['message']	= 'Data has been updated. For picking code ' . $params['picking_code'][0];
				$data['response']	= true;


			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function get_sn_by_pl($userName='',$pickingCode='')
	{

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();

			if($auth){

				if($userName == "" || $pickingCode == ""){

					$data['param']		= $pickingCode;
					$data['status']		= 400;
					$data['message']	= '';
					$data['response']	= false;

				} else{

					$data['param']		= $pickingCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getSnByPl($userName,$pickingCode)->result_array();

				}

			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function get_picking_recomendation($pickingCode=''){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();

			if($auth){

				if($pickingCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Picking code is empty';
					$data['response']	= false;

				} else{

					$data['param']		= $pickingCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getPickingRecomendation($pickingCode);

				}

			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function get_location_by_item($itemCode=''){

		$itemCode = str_replace("%7C", "*", $itemCode);

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();

			if($auth){

				if($itemCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Item code is empty';
					$data['response']	= false;

				} else{

					$data['param']		= $itemCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getLocationByItem($itemCode);

				}

			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function get_sn_by_location($locationCode='',$itemCode=''){

		// $itemCode = str_replace("%7", "*", $itemCode);
		$itemCode = str_replace('%7C','*',$itemCode);

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();

			if($auth){

				if($locationCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Location code is empty';
					$data['response']	= false;

				} else{

					$data['param']		= $locationCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getSerialNumberByLocation($locationCode,$itemCode);

				}

			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}
		// dd($data);

		echo json_encode($data);

	}

	public function get_tray_location($locationCode="",$outboundCode="", $username=""){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();

			if($auth){

				if($locationCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Location code is empty.';
					$data['response']	= false;

				} else{

					$result = $this->api_picking_model->getTrayLocation($locationCode);
					$user_id = $this->db
							->select('zone_id')
							->from('users_zone uz')
							->join('users u', 'u.id = uz.user_id')
							->where('u.user_name', $username)
							->get()
							->result_array();

					$location_area_id = $this->db
								->select('location_area_id')
								->from('locations')
								->where('name', $locationCode)
								->get()
								->row_array();

					//old location access
					// $location_access = in_array($location_area_id['location_area_id'], array_column($user_id, 'zone_id'));
					$location_access = true;
				if($location_access == false){
					$data['param']		= $locationCode;
					$data['status']		= 401;
					$data['message']	= 'Unauthorized user.';
					$data['response']	= false;
				}else{
					if($result){

						if($outboundCode){

							$getOutbound = $this->api_picking_model->getOutboundByTray($locationCode);
							if($getOutbound){

								if($getOutbound['outbound_code'] == $outboundCode){

									$data['param']		= array("location_code"=>$locationCode, "outbound_code"=>$outbound_code);
									$data['status']		= 200;
									$data['response']	= true;

								}else{

									$data['param']		= array("location_code"=>$locationCode, "outbound_code"=>$outbound_code);
									$data['status']		= 400;
									$data['response']	= false;
									$data['message']	= "Tray ini sedang dipakai oleh document outbound '".$getOutbound['outbound_code']."'.";

								}

							}else{

								$data['param']		= $locationCode;
								$data['status']		= 200;
								$data['response']	= true;

							}

						}else{

							$data['param']		= $locationCode;
							$data['status']		= 200;
							$data['response']	= true;

						}

					}else{

						$data['param']		= $locationCode;
						$data['status']		= 400;
						$data['message']	= "Location doesn't exists or Inactive.";

					}
				    }



				}

			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function get_uom_by_serial_number(){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'POST'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();

			if($auth){

				$params = array(
					"serial_number" => $this->input->post('serial_number'),
					"picking_code"	=> $this->input->post("picking_code"),
					"item_code" => $this->input->post("item_code")
				);

				if(empty($params['serial_number']) || empty($params['picking_code'])){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Serial number is empty.';
					$data['response']	= false;

				} else{

					$result = $this->api_picking_model->getUomBySerialNumber($params);

					if($result){

						$data['param']		= $params['serial_number'];
						$data['status']		= 200;
						$data['response']	= true;
						$data['results']	= $result;

					}else{

						$data['param']		= $params['serial_number'];
						$data['status']		= 400;
						$data['message']	= "Serial number doesn't exists.";

					}



				}

			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function get_picking_by_area($pickingCode){
		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				if($pickingCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Picking code is empty.';
					$data['response']	= false;

				} else{

					$results = $this->api_picking_model->getPickingByArea($pickingCode);

					if($results){

						$data['param']		= $pickingCode;
						$data['status']		= 200;
						$data['response']	= true;
						$data['results']	= $results;

					}else{

						$data['param']		= $pickingCode;
						$data['status']		= 400;
						$data['message']	= "Picking doesn't exists.";

					}

				}
			}else{
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		// dd($data);
		echo json_encode($data);
	}

	public function get_outbound_by_picking($pickingCode=""){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				if($pickingCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Picking code is empty.';
					$data['response']	= false;

				} else{

					$results = $this->api_picking_model->getOutboundByPicking($pickingCode);

					if($results){

						$data['param']		= $pickingCode;
						$data['status']		= 200;
						$data['response']	= true;
						$data['results']	= $results;

					}else{

						$data['param']		= $pickingCode;
						$data['status']		= 400;
						$data['message']	= "Picking doesn't exists.";

					}

				}

			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function get_tray_by_outbound($outboundCode=""){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();

			if($auth){

				if(empty($outboundCode)){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Outbound code is empty.';
					$data['response']	= false;

				} else{

					$data['param']		= $outboundCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getTrayByOutbound($outboundCode);

				}

			} else{

				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);

	}

	public function check_part_number($unique = '', $part_number = ''){
		$params = [
			'unique' => $unique,
			'part_number' => $part_number
		];

		// dd($params);

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];


		if($method !== 'GET'){

			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				if($unique !== '' || $pl !== ''){
					$data['param']		= $unique;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->check_part_number($params);
				}else{
					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Check PL and Unique Code';
					$data['response']	= false;
				}
			}else{
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

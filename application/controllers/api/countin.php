<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require "Api.php";
class countin extends Api {

	function __construct() {

		parent::__construct();
		$this->auth->user_authentication();
		$this->load_model('api_countin_model');

	}

	public function get_items()
	{

		$this->method("GET");

		$data['param']		= '';
		$data['status']		= 200;
		$data['response']	= true;
		$data['message']	= 'Get Item List for Count In.';
		$data['results']	= $this->api_countin_model->get_items();

		$this->toJson($data);

	}

}

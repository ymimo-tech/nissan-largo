<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class warehouse_transfer_report extends MY_Controller {

    private $class_name;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array('warehouse_transfer_report_model'));
    }

	public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Warehouse Transfer Report';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/report/lr-warehouse-transfer-report.js";

		$data['today'] = date('d/m/Y');

        $this->load_model('item_stok_model');
        $data['item'] = $this->item_stok_model->getItem();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Warehouse Transfer Report');
        /* END OF INSERT LOG */

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

    public function export_excel(){

        ini_set('display_errors', false);

        error_reporting(-1);

        register_shutdown_function(function(){
                $error = error_get_last();
                if(null !== $error)
                {
                    header('location:error_limit');
                }
            });

        try
        {

            $params = array(
                'from'          => filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
                'to'            => filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
                'sn'            => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
                'item'          => $this->input->post('item'),
                'warehouse'     => $this->input->post('warehouse')
            );

            $this->session->set_userdata('current_filter',$this->input->post('periode'));

            $data = $this->warehouse_transfer_report_model->export_excel();

            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="In Transit Report.csv"');
            $fp = fopen('php://output', 'wb');

            $header = ["Outbound Document","Warehouse From","Warehouse Destination","Serial Number","Item Name","Status"];
            fputcsv($fp, $header);
            foreach ( $data as $d ) {
                fputcsv($fp, $d);
            }

            fclose($fp);

        }
        catch(\Exception $exception)
        {

            $this->load->view('components/error_memory_limit');

        }
	}

	//REQUEST
	public function getList(){

		$result = array();

		$params = array(
			'from'			=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'			=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'            => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'          => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->warehouse_transfer_report_model->getList($params);

		echo json_encode($result);

	}

    public function error_limit(){
        $this->load->view('components/error_memory_limit');
    }

}

<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class referensi_barang extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function update_kategori(){
        return $this->referensi_barang_model->update_kategori();
    }

    public function getItem(){
        dd($_POST);
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Item Info';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
		$data['jsPage'][] = "assets/custom/js/share.js";
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-barang.js";

        /* Get Data Select*/
        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues
        $data['locations'] = $this->warehouse_model->locations(); // Get Data Warehoues

        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

        /* Edit */
        if($this->access_right->otoritas('edit')){
            $data['page'][] = 'components/form-set-warehouse';
            $data['jsPage'][] = "assets/pages/scripts/lr-set-warehouse.js";
        }

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

        $this->include_other_view_params($data);

		$this->load_model('item_stok_model');

		$data['item'] = $this->item_stok_model->getItem();
		// $data['categories'] = $this->referensi_kategori_model->options();
		// $data['categories2'] = $this->inbound_model->create_options("kategori_2", "id_kategori_2", "kd_kategori_2, nama_kategori_2", "-- All --");
		$data['strategy'] = $this->referensi_barang_model->getPickingStrategy();
		$data['period'] = $this->referensi_barang_model->getPickingPeriod();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Referensi Barang');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

	public function export(){
		$this->load->library('PHPExcel');

		$params = array();

		$result = $this->referensi_barang_model->get_data($params)->result_array();
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Master-Item")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Item Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Item Name')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Category Code')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Category Name')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'UoM')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Multi Qty')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Default Qty')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'Minimal Qty')->getStyle('H1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(8, 1, 'Picking Strategy')->getStyle('I1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(9, 1, 'Picking Period')->getStyle('J1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{
            $multiQty = $result[$i]['has_qty']==1?'Yes':'No' ;
			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['kd_barang']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['nama_barang']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['kd_kategori']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['nama_kategori']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['kd_satuan']);
			$sheet->setCellValueByColumnAndRow(5, $row, $multiQty);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['def_qty']);
			$sheet->setCellValueByColumnAndRow(7, $row, $result[$i]['minimum_stock']);
			$sheet->setCellValueByColumnAndRow(8, $row, $result[$i]['shipment_type']);
			$sheet->setCellValueByColumnAndRow(9, $row, $result[$i]['fifo_period']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Master-Item'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $barang = $this->referensi_barang_model->get_data(array('id_barang' => $id))->row_array();
            $id_item_subtitute = $barang['id_item_subtitute'];
            // $barang['id_item_subtitute'] = array();
            //
            // $barang['id_item_subtitute'][0] = $id_item_subtitute;
            $warehouses = $this->referensi_barang_model->getItemWarehouse($id);
            $barang['warehouses'] = array();
            foreach ($warehouses as $wh) {
                $barang['warehouses'][] = $wh['warehouse_id'];
            }

            $process_result = $barang;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    private function include_other_view_params(&$data) {
		$this->load_model('inbound_model');
        $this->load_model('referensi_kategori_model');
        $this->load_model('referensi_satuan_model');

        //$data['categories'] = $this->referensi_barang_model->create_options("kategori", "id_kategori", "kd_kategori");
		$data['categories'] = $this->referensi_kategori_model->options();
		// $data['categories2'] = $this->inbound_model->create_options("kategori_2", "id_kategori_2", "kd_kategori_2, nama_kategori_2");
        $data['satuan'] = $this->referensi_satuan_model->options();
		$data['shipment_types'] = $this->referensi_barang_model->shipmentTypeOptions();
		$data['fifo_periods'] = $this->referensi_barang_model->shipmentPeriodOptions();
        // $data['shipment_types'] = array('FEFOY' => "FEFOY");
        //$data['fifo_periods'] = array('MONTHLY' => "MONTHLY");
        // $data['owners'] = $this->referensi_barang_model->create_options("owner", "id_owner", "owner_name");
    }

    public function get_list(){

        $columns = array(
            0 => '',
            1 => 'kd_barang',
            2 => 'nama_barang',
            3 => 'nama_kategori',
            4 => 'nama_satuan',
            5 => 'has_qty',
            6 => 'def_qty',
            7 => 'minimum_stock',
            9 => 'shipment_type',
            10 => 'fifo_period',
			11 => 'a.status',
			12 => 'a.status',
        );

        $param = array();

		$idBarang = $this->input->post('id_barang');
		$cat1 = $this->input->post('id_kategori_1');
		$cat2 = $this->input->post('id_kategori_2');
		$str = $this->input->post('picking_strategy');
		$period = $this->input->post('period');
        $preorder = $this->input->post('preorder');
        $status = $this->input->post('status');

		if(!empty($idBarang))
			$param['a.id_barang'] = $idBarang;
		
		if(!empty($status))
			$param['a.status'] = $status;

		if(!empty($cat1))
			$param['a.id_kategori'] = $cat1;

		if(!empty($cat2))
			$param['a.id_kategori_2'] = $cat2;

		if(!empty($str))
			$param['a.shipment_id'] = $str;

		if(!empty($period))
			$param['a.fifo_period'] = $period;

        if($preorder != ""){
            $param['a.preorder'] = $preorder;
        }

        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $barang = $this->referensi_barang_model->data($param)->get();
        $iTotalRecords = $this->referensi_barang_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($barang->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_barang,'delete' => $value->id_barang));
            $MultiQty = $value->has_qty == 1 ? 'YES':'NO';
            $preorder = ($value->preorder) ? "YES" : "NO";
			
			$statusText = '';
			if(strtoupper($value->status) == 'ACTIVE'){
				$statusText = "<span class='label label-sm label-default' style='background:green;'>".strtoupper($value->status)."</span>";
			} else {
				$statusText = "<span class='label label-sm label-danger'>".strtoupper($value->status)."</span>";
			}
			
            $records["data"][] = array(
                '<input type="checkbox" name="id[]" id="id" value="'.$value->id_barang.'">',
                $_REQUEST['start']+$i,
                $value->kd_barang,
                $value->nama_barang,
                $value->nama_kategori,
                $value->nama_satuan,
                $MultiQty,
                $value->def_qty,
                $value->minimum_stock,
                $value->shipment_type,
                $value->fifo_period,
				$statusText,
                // $preorder,
                $action
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function add($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load_model('referensi_barang_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->referensi_barang_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;



            $this->load->view($this->class_name . '/form2', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
		$this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function option(){

        $query = $this->input->post('query');

        $locations = $this->db
                        ->select('id, code, name')
                        ->from('items')
                        // ->where('lower(name) like', strtolower($query))
                        ->like('lower(code)',strtolower($query))
                        ->get()
                        ->result_array();
        $data= array();

        foreach ($locations as $loc) {
            $data['results'][] = array(
                'id' => $loc['id'],
                'name' => $loc['code'].' - '.$loc['name'],
                'code'  => $loc['code']
            );
        }

        echo json_encode($data);
    }

    public function proses() {
        $proses_result['status'] = 'OK';

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $kd_barang_rule = 'required|trim';

            $this->form_validation->set_rules('kd_barang', 'Kode Barang', $kd_barang_rule);
            $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required|trim');
            $this->form_validation->set_rules('id_kategori_1', 'Kode Kategori', 'required|trim');
			//$this->form_validation->set_rules('id_kategori_2', 'Kode Kategori 2', 'required|trim');
            $this->form_validation->set_rules('id_satuan', 'Base UoM', 'required|trim');
            //$this->form_validation->set_rules('tipe_barang', 'Kode Satuan', 'required|trim');
            //$this->form_validation->set_rules('shipment_type', 'Tipe Shipment', 'required|trim');
            //$this->form_validation->set_rules('fifo_period', 'FIFO Period', 'required|trim');
            //$this->form_validation->set_rules('id_owner', 'Nama Owner', 'required|trim');
            //$this->form_validation->set_rules('base_qty', 'Base QTY', 'required|trim');
            //$this->form_validation->set_rules('child_satuan', 'Child UoM', 'required|trim');


            $kdBarangValidate = 1;
            if($this->input->post('kd_barang')!=$this->input->post('ori_kd_barang')){
                $kdBarangValidate =  $this->referensi_barang_model->is_unique($this->input->post('kd_barang'));
            }


            if($kdBarangValidate){
                if ($this->form_validation->run()) {
                    $id = $this->input->post('id');

                    $is_area = $this->db->select('*')->from('locations')->where('name',$this->input->post('item_area'))->get()->row_array();
                    // dd($is_area);
                    // $area = $this->db->select('name')->from('locations')->where('name',$this->input->post('item_area'))->get()->row_array()['name'];
                    // $area_id = $this->db->select('id')->from('locations')->where('name',$this->input->post('item_area'))->get()->row_array()['id'];
                    
                    if(isset($is_area['name'])&&isset($is_area['id'])){
                        $area = substr($is_area['name'],0,1);
                        $area_id = $is_area['id'];
                    }else{
                        // dd('masuk sini');
                        $proses_result['status'] = 'ERR';
                        $proses_result['no_location'] = 1;
                        echo json_encode($proses_result);
                        exit;
                        $area = null;
                        $area_id = null;
                    }

                    $data = array(
                        'sku'       => $this->input->post('sku'),
                        'kd_barang' => $this->input->post('kd_barang'),
                        'nama_barang' => $this->input->post('nama_barang'),
                        'id_kategori' => $this->input->post('id_kategori_1'),
    					'id_kategori_2' => $this->input->post('id_kategori_2'),
                        'id_satuan' => $this->input->post('id_satuan'),
                        'tipe_barang' => $this->input->post('tipe_barang'),
                        'shipment_type' => $this->input->post('shipment_type'),
                        'fifo_period' => $this->input->post('fifo_period'),
                        'id_owner' => $this->input->post('id_owner'),
                        'def_qty' => $this->input->post('DefQty'),
                        'child_satuan' => $this->input->post('child_satuan'),
                        'weight'    => $this->input->post('weight'),
                        'width'     => $this->input->post('width'),
                        'height'    => $this->input->post('height'),
                        'length'    => $this->input->post('length'),
                        'item_area' => $area,
                        'def_loc_id'=> $area_id,
                        'parent_child' => $this->input->post('parent_child'),
                        'item_subtitute' => $this->input->post('item_subtitute'),
                        'convert_2' => (empty($this->input->post('convert_2'))) ? 0 : $this->input->post('convert_2'),
                        'unit_id_2' => (empty($this->input->post('unit_id_2'))) ? 0 : $this->input->post('unit_id_2'),
                        'convert_3' => (empty($this->input->post('convert_3'))) ? 0 : $this->input->post('convert_3'),
                        'unit_id_3' => (empty($this->input->post('unit_id_3'))) ? 0 : $this->input->post('unit_id_3'),
                        'quarantineduration' => ($this->input->post('quarantineduration') != "") ? $this->input->post('quarantineduration') : 0
                 	);

					$data['minimum_stock']	= (empty($this->input->post('min'))) ? 0 : $this->input->post('min');
					$data['min_reorder']	= (empty($this->input->post('min_reorder'))) ? 0 : $this->input->post('min_reorder');

                    /*
                     * If : $id == '', lakukan proses create data
                     * Else : lakukan proses update
                     */

                    $warehouse = $this->input->post('warehouse');
    				$batch = $this->input->post('batch');
    				$expDate = $this->input->post('expdate');
                    $MultiQty = $this->input->post('multiQty');
                    $preorder = $this->input->post('preorder');
                    $quarantine = $this->input->post('quarantine');
                    $autorelease = $this->input->post('autorelease');


                    if(isset($quarantine)){
                        if($quarantine){
                            $data['quarantine'] = 1;
                        }else{
                            $data['quarantine'] = 0;
                        }
                    }else{
                        $data['quarantine'] = 0;
                    }


                    if(isset($autorelease)){
                        if($autorelease){
                            $data['autorelease'] = 1;
                        }else{
                            $data['autorelease'] = 0;
                        }
                    }else{
                        $data['autorelease'] = 0;
                    }

                    if(isset($preorder)){
                        if($preorder)
                            $data['preorder'] = 1;
                        else
                            $data['preorder'] = 0;
                    }else{
                        $data['preorder'] = 0;
                    }

                    if(isset($MultiQty)){
                        if($MultiQty)
                            $data['has_qty'] = 1;
                        else
                            $data['has_qty'] = 0;
                    }else{
                        $data['has_qty'] = 0;
                    }

    				if(isset($batch)){
    					if($batch)
    						$data['has_batch'] = 1;
    					else
    						$data['has_batch'] = 0;
    				}else{
    					$data['has_batch'] = 0;
    				}

    				if(isset($expDate)){
    					if($expDate)
    						$data['has_expdate'] = 1;
    					else
    						$data['has_expdate'] = 0;
    				}else{
    					$data['has_expdate'] = 0;
    				}

                    if ($id == '') {
                        if ($this->referensi_barang_model->create($data)) {
                            $id=$this->db->insert_id();
                            $proses_result['add'] = 1;
    						/* INSERT LOG */
    						$this->access_right->activity_logs('add','Referensi Barang');
    						/* END OF INSERT LOG */
                        }
                    } else {
                        if ($this->referensi_barang_model->update($data, $id)) {
                            $proses_result['edit'] = 1;
    						/* INSERT LOG */
    						$this->access_right->activity_logs('edit','Referensi Barang');
    						/* END OF INSERT LOG */
                        }
                    }
                    $this->referensi_barang_model->addItemWarehouse($id,$warehouse);
                } else {
    				$proses_result['status'] = 'ERR';
                    $proses_result['message'] = validation_errors();
                }
            }else{
                $proses_result['status'] = 'ERR';
                $proses_result['message'] = 'Item code already used!';
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
				$proses_result['status'] = 'ERR';
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
				$proses_result['status'] = 'ERR';
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();

        if ($this->access_right->otoritas('delete', true)) {

			$id = $this->input->post("id");
            $result = $this->referensi_barang_model->delete($id);

        } else {

			$result['status'] = 'ERR';
			$result['no_delete'] = 1;
        }

        echo json_encode($result);
    }

    function sync_barang(){
        $this->db->trans_start();
        $new_barangs =  $this->referensi_barang_model->get_new_barang();
        $new=array();
        foreach($new_barangs->result() as $barang){
            // SKU
            // ItemName
            // ItemDesc
            // UpdateDate
            // Category
            // HasExpDate
            // ShipmentType
            // FifoPeriod
            // MultiQty
            // DefQty
            // UomCode
            // MinStok
            // SyncStatus
            // SyncDate

            $id_category = $this->referensi_barang_model->get_barang_category($barang->Category);
            $id_satuan = $this->referensi_barang_model->get_barang_satuan($barang->UomCode);
            $data_barang=array(
                'kd_barang'=>$barang->SKU,
                'nama_barang'=>$barang->ItemName,
                'detail_barang'=>$barang->ItemDesc,
                'id_kategori'=>$id_category,
                'has_expdate'=>$barang->HasExpDate,
                'shipment_type'=>$barang->ShipmentType,
                'fifo_period'=>$barang->FifoPeriod,
                'def_qty'=>$barang->DefQty,
                'id_satuan'=>$id_satuan,
                'minimum_stock'=>$barang->MinStok
            );
            $new_barang_id = $this->referensi_barang_model->create_barang($data_barang);
            $SyncDate=date('Y-m-d H:i:s');
            $this->referensi_barang_model->set_barang_status($barang->SKU,$SyncDate);
            $new[]=array('sku'=>$barang->SKU,'nama'=>$barang->ItemName,'id'=>$new_barang_id);
        }
        $this->db->trans_complete();
        return $new;
    }

    function longpolling(){
	    date_default_timezone_set("Asia/Jakarta");
	    header('Cache-Control: no-cache');
	    header("Content-Type: text/event-stream\n\n");
	    while (1) {
            //check new barang
            $new = $this->sync_barang();
            //sleep(10);
            //$new = array(array('nomor'=>'PO170411-001','id'=>48),array('nomor'=>'PO170425-001','id'=>49));
	        $output = array('newc'=>$new);
	        echo "\nevent: sync\n";
	        echo 'data: '.json_encode($output);
	        echo "\n\n";
	        ob_flush();
	        flush();
            session_write_close();
	        sleep(30);
	    }
	}

    function manual_sync_barang(){
        date_default_timezone_set("Asia/Jakarta");
        $new = $this->sync_barang();
        $json = array('newc'=>$new);
        echo json_encode($json);
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('New Item Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        // 'Material SAP' => 'sku',
                        // 'Material Reject SAP' => 'sap_reject_code',
                        'Item Code'=> 'item_code',
                        'Item Name' => 'item_name',
                        'Brand' => 'brand',
                        'OEM' => 'oem',
                        'Packing Description' => 'packing_description',
                        'Item Category' => 'category',
                        'Multi Qty' => 'has_qty',
                        'Default Qty' => 'def_qty',
                        'Unit Of Measurement' => 'unit',
                        'Have Expired Date' => 'has_expdate',
                        'Picking Strategy' => 'shipment_type',
                        'Picking Period' => 'fifo_period',
                        'Minimum Replenishment Stock' => 'minimum_stock',
                        'Minimum Reorder Stock' => 'min_reorder',
                        'Warehouse'	=> 'warehouse',
                        'Height'	=> 'height',
                        'Weight'	=> 'weight',
                        'Length'	=> 'length',
                        'Width'		=> 'width'
                    ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->referensi_barang_model->importFromExcel($data);

                        if($process['status']){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage($process['message']);

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }

        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }

    public function addToWarehouse(){

        $result = array();

        $params = array(
            'warehouses' => $this->input->post('warehouses'),
            'elements'   => $this->input->post('elements')
        );

        $process = $this->referensi_barang_model->addToWarehouse($params);

        if($process){

            $result = array(
                "status"    => "OK",
                "message"   => "Succes"
            );

        }else{

            $result = array(
                "status"    => "ERR",
                "message"   => "Failed"
            );

        }

        echo json_encode($result);

    }

    public function removeFromWarehouse(){

        $result = array();

        $params = array(
            'warehouses' => $this->input->post('warehouses'),
            'elements'   => $this->input->post('elements')
        );

        if(!empty($params['warehouses']) && !empty($params['elements'])){

            $process = $this->referensi_barang_model->removeFromWarehouse($params);

            if($process){

                $result = array(
                    "status"    => "OK",
                    "message"   => "Succes"
                );

            }else{

                $result = array(
                    "status"    => "ERR",
                    "message"   => "Failed"
                );

            }

        }else{

            if(empty($params['warehouses'])){

                $result = array(
                    "status"    => "ERR",
                    "message"   => "Warehouse tidak boleh kosong"
                );

            }else{

                $result = array(
                    "status"    => "ERR",
                    "message"   => "Item tidak boleh kosong"
                );

            }

        }

        echo json_encode($result);

    }

}

/* End of file referensi_barang.php */
/* Location: ./application/controllers/referensi_barang.php */

<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class material_out extends MY_Controller {

    private $class_name;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array('report_model'));
    }

    public function test() {

        $result = $this->report_model->getMaterialOutDetail();

        header('Content-Type:Application/json');
        echo json_encode($result);
        exit;

    }

	public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Material Out Report';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/report/lr-material-out.js";

		$data['today'] = date('d/m/Y');
		$data['destination'] = $this->report_model->getSource('out');

        $this->load_model('item_stok_model');
        $data['item'] = $this->item_stok_model->getItem();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Report Material Out');
        /* END OF INSERT LOG */

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	public function export_material_out(){
		$data = array();

		$params = array(
			'group_by'			=> filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
			'destination_id'	=> filter_var(trim($this->input->post('destination_id')), FILTER_SANITIZE_NUMBER_INT),
			'destination_type'	=> filter_var(trim($this->input->post('destination_type')), FILTER_SANITIZE_STRING),
			'destination_name'	=> filter_var(trim($this->input->post('destination_name')), FILTER_SANITIZE_STRING),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'                => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'              => $this->input->post('item'),
            'warehouse'         => $this->input->post('warehouse')
		);

		$data['page'] = $this->class_name . '/material_out';
		$data['params'] = $params;
		$data['data'] = $this->report_model->getMaterialOut($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "material_out.pdf";

        ini_set('memory_limit','256M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            10, // margin bottom
            10, // margin header
            10
		); // margin footer

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

    public function export_excel_old(){

        ini_set('display_errors', false);
        ini_set('memory_limit','6143M'); // boost the memory limit if it's low ;)
        ini_set('max_execution_time', 4000);
        error_reporting(-1);

        register_shutdown_function(function(){
                $error = error_get_last();
                if(null !== $error)
                {
                    header('location:error_limit');
                }
            });

        try
        {

        $params = array(
			'group_by'		=> filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
			'source_id'		=> filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
			'source_type'	=> filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
			'source_name'	=> filter_var(trim($this->input->post('source_name')), FILTER_SANITIZE_STRING),
			'from'			=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'			=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'            => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'          => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
		);

		$this->load->library('PHPExcel');
        $data = $this->report_model->getMaterialOut($params);

        $warehouse = "ALL";
        $this->load_model('warehouse_model');
        if(!empty($params['warehouse'])){
            $warehouse = $this->warehouse_model->getById($params['warehouse'])->row_array();
        }

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Material Out Report - ".$warehouse)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'Group By')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, ucfirst(strtolower($params['group_by'])))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 2, 'Source Type')->getStyle('A2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 2, ucfirst($params['source_type']))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 3, 'Source Name')->getStyle('A3')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 3, ucfirst($params['source_name']))->getStyle('B3')->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(3, 1, 'From')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, $params['from'])->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 2, 'To')->getStyle('D2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 2, $params['from'])->getStyle('E2')->getFont()->setBold(true);

        if(strtolower($params['group_by']) == 'date')
            $gr='Periode';
        else
            $gr='Source';

		$sheet->setCellValueByColumnAndRow(0, 5, 'No')->getStyle('A5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 5, 'Group By '.$gr)->getStyle('B5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 5, 'Outbound Doc. #')->getStyle('C5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 5, 'Destination')->getStyle('D5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 5, 'Order Qty')->getStyle('E5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 5, 'Picking Doc. #')->getStyle('F5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 5, 'Print Picking Status')->getStyle('G5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 5, 'Items')->getStyle('H5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(8, 5, 'Picking Qty')->getStyle('I5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(9, 5, 'Start Pick')->getStyle('J5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(10, 5, 'Finish Pick')->getStyle('K5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(11, 5, 'User Picking')->getStyle('L5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(12, 5, 'Packing Number')->getStyle('M5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(13, 5, 'Packing Qty')->getStyle('N5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(14, 5, 'Packing Volume')->getStyle('O5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(15, 5, 'Packing Weight')->getStyle('P5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(16, 5, 'Start Packing')->getStyle('Q5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(17, 5, 'User Packing')->getStyle('R5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(18, 5, 'Delivery Note')->getStyle('S5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(19, 5, 'Manifest')->getStyle('T5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(20, 5, 'Surat Jalan')->getStyle('U5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(21, 5, 'Load. Qty')->getStyle('V5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(22, 5, 'Start Load')->getStyle('W5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(23, 5, 'Finish Load')->getStyle('X5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(24, 5, 'User Loading')->getStyle('Y5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(25, 5, 'Remark')->getStyle('Z5')->getFont()->setBold(true);

        $row = 6;

        $data = $data['data'];

        foreach($data as $k => $v){

            $dt = key($data[$k]);
            $sheet->setCellValueByColumnAndRow(0, $row, $dt)->getStyle('A'.$row)->getFont()->setBold(true);

            $row++;

            $detailData = $v[$dt];
            $iLen = count($detailData);

            for($j = 0; $j < $iLen; $j++){
                $sheet->setCellValueByColumnAndRow(0, $row, $detailData[$j]['tgl_outbound']);
                $sheet->setCellValueByColumnAndRow(1, $row, $gr);
                $sheet->setCellValueByColumnAndRow(2, $row, $detailData[$j]['kd_outbound']);
                $sheet->setCellValueByColumnAndRow(3, $row, $detailData[$j]['destination']);
                $sheet->setCellValueByColumnAndRow(4, $row, $detailData[$j]['doc_qty']);
                $sheet->setCellValueByColumnAndRow(5, $row, $detailData[$j]['pl_name']);
                $sheet->setCellValueByColumnAndRow(6, $row, $detailData[$j]['printed']);
                $sheet->setCellValueByColumnAndRow(7, $row, $detailData[$j]['item']);
                $sheet->setCellValueByColumnAndRow(8, $row, $detailData[$j]['picking_qty']);
                $sheet->setCellValueByColumnAndRow(9, $row, $detailData[$j]['start_picking']);
                $sheet->setCellValueByColumnAndRow(10, $row, $detailData[$j]['finish_picking']);
                $sheet->setCellValueByColumnAndRow(11, $row, $detailData[$j]['user_picking']);
                $sheet->setCellValueByColumnAndRow(12, $row, $detailData[$j]['packing_number']);
                $sheet->setCellValueByColumnAndRow(13, $row, $detailData[$j]['packing_qty']);
                $sheet->setCellValueByColumnAndRow(14, $row, $detailData[$j]['packing_volume']);
                $sheet->setCellValueByColumnAndRow(15, $row, $detailData[$j]['packing_weight']);
                $sheet->setCellValueByColumnAndRow(16, $row, $detailData[$j]['packing_start']);
                $sheet->setCellValueByColumnAndRow(17, $row, $detailData[$j]['user_packing']);
                $sheet->setCellValueByColumnAndRow(18, $row, $detailData[$j]['dn']);
                $sheet->setCellValueByColumnAndRow(19, $row, $detailData[$j]['manifest_number']);
                $sheet->setCellValueByColumnAndRow(20, $row, $detailData[$j]['surat_jalan']);
                $sheet->setCellValueByColumnAndRow(21, $row, $detailData[$j]['load_qty'] );
                $sheet->setCellValueByColumnAndRow(22, $row, $detailData[$j]['start_loading']);
                $sheet->setCellValueByColumnAndRow(23, $row, $detailData[$j]['finish_loading']);
                $sheet->setCellValueByColumnAndRow(24, $row, $detailData[$j]['user_loading']);
                $sheet->setCellValueByColumnAndRow(25, $row, $detailData[$j]['remark']);

                $row++;
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Material Out Report.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

        }
        catch(\Exception $exception)
        {
            echo $exception->errorMessage();
            $this->load->view('components/error_memory_limit');

        }
	}

    public function export_excel(){
        // dd('ayam');
        ini_set('display_errors', false);
        ini_set('memory_limit','6143M'); // boost the memory limit if it's low ;)
        ini_set('max_execution_time', 4000);
        error_reporting(-1);
        
        // register_shutdown_function(function(){
        //     $error = error_get_last();
        //     if(null !== $error)
        //     {
        //         header('location:error_limit');
        //     }
        // });
        
        // try
        // {
            
            $where = '';
            $params = array(
                'group_by'		=> filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
                'source_id'		=> filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
                'source_type'	=> filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
                'source_name'	=> filter_var(trim($this->input->post('source_name')), FILTER_SANITIZE_STRING),
                'from'			=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
                'to'			=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
                'sn'            => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
                'item'          => $this->input->post('item'),
                'warehouse'     => $this->input->post('warehouse')
            );

        if(!empty($params['to']) && !empty($params['from'])){
            $where .= "and picking_start between '".$params['from']." 00:00:00' and '".$params['to']." 23:59:00'";
        }
            
        $data = $this->db->query("select * from v_item_out where 1=1 ".$where)->result_array();

		$this->load->library('PHPExcel');
        // $data = $this->report_model->getMaterialOut($params);

        $warehouse = "ALL";
        $this->load_model('warehouse_model');
        if(!empty($params['warehouse'])){
            $warehouse = $this->warehouse_model->getById($params['warehouse'])->row_array();
        }

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Material Out Report - ".$warehouse)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);
        $sheet->getColumnDimension('P')->setAutoSize(true);
        $sheet->getColumnDimension('Q')->setAutoSize(true);
        $sheet->getColumnDimension('R')->setAutoSize(true);
        $sheet->getColumnDimension('S')->setAutoSize(true);
        $sheet->getColumnDimension('T')->setAutoSize(true);
        $sheet->getColumnDimension('U')->setAutoSize(true);
        $sheet->getColumnDimension('V')->setAutoSize(true);
        $sheet->getColumnDimension('W')->setAutoSize(true);
        $sheet->getColumnDimension('X')->setAutoSize(true);
        $sheet->getColumnDimension('Y')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'Group By')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, ucfirst(strtolower($params['group_by'])))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 2, 'Source Type')->getStyle('A2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 2, ucfirst($params['source_type']))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 3, 'Source Name')->getStyle('A3')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 3, ucfirst($params['source_name']))->getStyle('B3')->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(3, 1, 'From')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, $params['from'])->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 2, 'To')->getStyle('D2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 2, $params['from'])->getStyle('E2')->getFont()->setBold(true);

        // if(strtolower($params['group_by']) == 'date')
        //     $gr='Periode';
        // else
        //     $gr='Source';

		$sheet->setCellValueByColumnAndRow(0, 5, 'Outbound Doc. #')->getStyle('A5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 5, 'Outbound Date')->getStyle('B5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 5, 'LPN')->getStyle('C5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 5, 'Item Name')->getStyle('D5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 5, 'Picking Doc. #')->getStyle('E5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 5, 'Picking Start')->getStyle('F5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 5, 'Picking Qty')->getStyle('G5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 5, 'User Picking')->getStyle('H5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(8, 5, 'Packing Doc. #')->getStyle('I5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(9, 5, 'Packing Time')->getStyle('J5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(10, 5, 'Packing Qty')->getStyle('K5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(11, 5, 'User Packing')->getStyle('L5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(12, 5, 'DN Doc. #')->getStyle('M5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(13, 5, 'User DN')->getStyle('N5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(14, 5, 'Manifest Doc. #')->getStyle('O5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(15, 5, 'Manifest Start')->getStyle('P5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(16, 5, 'Manifest Finish')->getStyle('Q5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(17, 5, 'User Loading')->getStyle('R5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(18, 5, 'Status Picking')->getStyle('S5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(19, 5, 'LPN Original')->getStyle('T5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(20, 5, 'Item Name Original')->getStyle('U5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(21, 5, 'Dealer Name')->getStyle('V5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(22, 5, 'Weight Packing')->getStyle('W5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(23, 5, 'Volume Packing')->getStyle('X5')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(24, 5, 'Priority')->getStyle('Y5')->getFont()->setBold(true);

        $row = 6;

        // $data = $data['data'];

        // foreach($data as $k => $v){

        //     $dt = key($data[$k]);
        
        //     $row++;
        
        //     $detailData = $v[$dt];
        //     $iLen = count($detailData);
        
        
        // }
        
        for($j = 0; $j < count($data); $j++){
            // $sheet->setCellValueByColumnAndRow(0, $row, $dt)->getStyle('A'.$row)->getFont()->setBold(true);
            $sheet->setCellValueByColumnAndRow(0, $row, $data[$j]['outbound_code']);
            $sheet->setCellValueByColumnAndRow(1, $row, $data[$j]['outbound_date']);
            $sheet->setCellValueByColumnAndRow(2, $row, $data[$j]['lpn']);
            $sheet->setCellValueByColumnAndRow(3, $row, $data[$j]['item_name']);
            $sheet->setCellValueByColumnAndRow(4, $row, $data[$j]['picking_code']);
            $sheet->setCellValueByColumnAndRow(5, $row, $data[$j]['picking_start']);
            $sheet->setCellValueByColumnAndRow(6, $row, $data[$j]['picking_qty']);
            $sheet->setCellValueByColumnAndRow(7, $row, $data[$j]['user_picking']);
            $sheet->setCellValueByColumnAndRow(8, $row, $data[$j]['packing_code']);
            $sheet->setCellValueByColumnAndRow(9, $row, $data[$j]['packing_time']);
            $sheet->setCellValueByColumnAndRow(10, $row, $data[$j]['packing_qty']);
            $sheet->setCellValueByColumnAndRow(11, $row, $data[$j]['user_packing']);
            $sheet->setCellValueByColumnAndRow(12, $row, $data[$j]['dn_code']);
            $sheet->setCellValueByColumnAndRow(13, $row, $data[$j]['user_dn']);
            $sheet->setCellValueByColumnAndRow(14, $row, $data[$j]['manifest_code']);
            $sheet->setCellValueByColumnAndRow(15, $row, $data[$j]['manifest_start']);
            $sheet->setCellValueByColumnAndRow(16, $row, $data[$j]['manifest_finish']);
            $sheet->setCellValueByColumnAndRow(17, $row, $data[$j]['user_loading']);
            $sheet->setCellValueByColumnAndRow(18, $row, $data[$j]['status_picking']);
            $sheet->setCellValueByColumnAndRow(19, $row, $data[$j]['lpn_original']);
            $sheet->setCellValueByColumnAndRow(20, $row, $data[$j]['item_name_original']);
            $sheet->setCellValueByColumnAndRow(21, $row, $data[$j]['name_dealer'] );
            $sheet->setCellValueByColumnAndRow(22, $row, $data[$j]['weight']);
            $sheet->setCellValueByColumnAndRow(23, $row, $data[$j]['volume']);
            $sheet->setCellValueByColumnAndRow(24, $row, $data[$j]['m_priority_name']);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Material Out Report.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

        // }
        // catch(\Exception $exception)
        // {
        //     echo $exception->errorMessage();
        //     $this->load->view('components/error_memory_limit');

        // }
	}

	//REQUEST
	public function getMaterialOut(){

		$result = array();

		$params = array(
			'group_by'			=> filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
			'destination_id'	=> $this->input->post('destination_id'),
			'destination_type'	=> filter_var(trim($this->input->post('destination_type')), FILTER_SANITIZE_STRING),
			'destination_name'	=> filter_var(trim($this->input->post('destination_name')), FILTER_SANITIZE_STRING),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'                => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'              => $this->input->post('item'),
            'warehouse'         => $this->input->post('warehouse'),
            'length'            => $this->input->post('length'),
            'start'             => $this->input->post('start')
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->report_model->getMaterialOut($params);

		echo json_encode($result);
	}

    public function export_excel_detail(){

        ini_set('display_errors', false);

        error_reporting(-1);

        register_shutdown_function(function(){
                $error = error_get_last();
                if(null !== $error)
                {
                    header('location:error_limit');
                }
            });

        try
        {

        $params = array(
            'group_by'      => filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
            'source_id'     => filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
            'source_type'   => filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
            'source_name'   => filter_var(trim($this->input->post('source_name')), FILTER_SANITIZE_STRING),
            'from'          => filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
            'to'            => filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'            => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'          => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
        );

        $this->load->library('PHPExcel');
        $data = $this->report_model->getMaterialOutDetail($params);

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="Materil Out Report.csv"');
        $fp = fopen('php://output', 'wb');

        $head = ['No','Outbound Doc.','Picking Doc #','Serial Number','Outer Label','Item','Qty','User'];
        fputcsv($fp, $head);

        $i=1;
        foreach ( $data as $d ) {

            $d['no'] = $i;
            fputcsv($fp, $d);
            $i++;
        }

        fclose($fp);

        }
        catch(\Exception $exception)
        {

            $this->load->view('components/error_memory_limit');

        }

    }


    public function export_excel_detail_bak(){

        ini_set('display_errors', false);

        error_reporting(-1);

        register_shutdown_function(function(){
                $error = error_get_last();
                if(null !== $error)
                {
                    header('location:error_limit');
                }
            });

        try
        {

        $params = array(
            'group_by'      => filter_var(trim($this->input->post('group_by')), FILTER_SANITIZE_STRING),
            'source_id'     => filter_var(trim($this->input->post('source_id')), FILTER_SANITIZE_NUMBER_INT),
            'source_type'   => filter_var(trim($this->input->post('source_type')), FILTER_SANITIZE_STRING),
            'source_name'   => filter_var(trim($this->input->post('source_name')), FILTER_SANITIZE_STRING),
            'from'          => filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
            'to'            => filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
            'sn'            => filter_var(trim($this->input->post('sn')), FILTER_SANITIZE_STRING),
            'item'          => $this->input->post('item'),
            'warehouse'     => $this->input->post('warehouse')
        );

        $this->load->library('PHPExcel');
        $data = $this->report_model->getMaterialOutDetail($params);

        $this->load_model('warehouse_model');
        $warehouse = "ALL";
        if($params['warehouse']){
            $warehouse = $this->warehouse_model->getById($params['warehouse'])->row_array();
        }

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Material Out Report - ".$warehouse)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'Group By')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, ucfirst(strtolower($params['group_by'])))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 2, 'Source Type')->getStyle('A2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 2, ucfirst($params['source_type']))->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(0, 3, 'Source Name')->getStyle('A3')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 3, ucfirst($params['source_name']))->getStyle('B3')->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(3, 1, 'From')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, $params['from'])->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 2, 'To')->getStyle('D2')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 2, $params['from'])->getStyle('E2')->getFont()->setBold(true);

        if(strtolower($params['group_by']) == 'date')
            $gr='Periode';
        else
            $gr='Source';

        $sheet->setCellValueByColumnAndRow(0, 5, 'No')->getStyle('A5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 5, 'Group By '.$gr)->getStyle('B5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 5, 'Outbound Doc. #')->getStyle('C5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 5, 'Picking Doc. #')->getStyle('D5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 5, 'Serial Number')->getStyle('E5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 5, 'Outer Label')->getStyle('F5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(6, 5, 'Item')->getStyle('G5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(7, 5, 'Qty')->getStyle('H5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(8, 5, 'User')->getStyle('I5')->getFont()->setBold(true);

        $row = 6;


        $i=1;
        foreach($data as $d){

            $sheet->getStyle('E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);;

            $sheet->setCellValueByColumnAndRow(0, $row, $i);
            $sheet->setCellValueByColumnAndRow(1, $row, '-');
            $sheet->setCellValueByColumnAndRow(2, $row, $d['document_code']);
            $sheet->setCellValueByColumnAndRow(3, $row, $d['document_picking_code']);
            $sheet->setCellValueByColumnAndRow(4, $row, $d['serial_number']);
            $sheet->setCellValueByColumnAndRow(5, $row, $d['outer_label']);
            $sheet->setCellValueByColumnAndRow(6, $row, $d['item_name']);
            $sheet->setCellValueByColumnAndRow(7, $row, $d['picked_qty'] );
            $sheet->setCellValueByColumnAndRow(8, $row, $d['picked_user'] );

            $row++;

            $i++;

        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Material Out Report - '.$warehouse.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
        }
        catch(\Exception $exception)
        {

            $this->load->view('components/error_memory_limit');

        }
    }

    public function error_limit(){
        $this->load->view('components/error_memory_limit');
    }

}

<?php

/**
 * Description of referensi_supplier
 *
 * @author Hady Pratama
 */
class sources extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {

        $data['title'] = '<i class="icon-plus-sign-alt"></i> Source Master';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;

        /* JS Page*/
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = "assets/pages/scripts/sources/sources.js";
        $data['jsPage'][] = "assets/custom/js/share.js";


        /* Get Source / Destination Category Data */
        $data['categories'] = $this->options('source_categories','source_category_id','source_category_name');

        /* Get Source / Destination Category Data */
        $data['sourceType'] = $this->options('source_categories','source_category_id','source_category_name',true);

        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

		/* Otoritas Tombol tambah */
    	$data['button_group'] = array();
    	if ($this->access_right->otoritas('add')) {
    		$data['button_group'] = array(
    			anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
    		);
    	}

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Source Master');
        /* END OF INSERT LOG */

        $this->load->view('template', $data);

    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $data = $this->sources_model->get_data(array('source_id' => $id))->row_array();

            $process_result = $data;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){

        $columns = array(
            0 => '',
            1 => 'source_code',
            2 => 'source_name',
            3 => 'source_address',
            4 => 'source_phone',
            5 => 'source_contact_person',
            6 => 'source_email',
            7 => 'source_city',
            8 => 'source_category_name',
            9 => 'status'
        );
        
        $param = '';

        if(!empty($this->input->post('source_type_filter'))){
            $this->db->where('a.source_category_id',$this->input->post('source_type_filter'));
        }

        if(!empty($this->input->post('source_city_filter'))){
            $this->db->like('lower(source_city)',strtolower($this->input->post('source_city_filter')));
        }

        if(!empty($this->input->post('source_code_filter'))){
            $this->db->like('lower(source_code)',strtolower($this->input->post('source_code_filter')));
        }

        if(!empty($this->input->post('source_name_filter'))){
            $this->db->like('lower(source_name)',strtolower($this->input->post('source_name_filter')));
        }

        if(!empty($this->input->post('source_contact_person_filter'))){
            $this->db->like('lower(source_contact_person)',strtolower($this->input->post('source_contact_person_filter')));
        }
		
		if(!empty($this->input->post('status'))){
            $this->db->where('UPPER(a.status)',strtoupper($this->input->post('status')));
        }

        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $supplier = $this->sources_model->data($param)->get();
        $iTotalRecords = $this->sources_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($supplier->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->source_id,'delete' => $value->source_id));
			
			$statusText = '';
			if(strtoupper($value->status) == 'ACTIVE'){
				$statusText = "<span class='label label-sm label-default' style='background:green;'>".strtoupper($value->status)."</span>";
			} else {
				$statusText = "<span class='label label-sm label-danger'>".strtoupper($value->status)."</span>";
			}

            $records["data"][] = array(
 
                $_REQUEST['start']+$i,
                $value->source_code,
 				$value->source_name,
                $value->source_address,
                $value->source_phone,
                $value->source_contact_person,
                $value->source_email,
                $value->source_city,
                $value->source_category_name,
                $statusText,
                $action
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function add($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load_model('sources_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->sources_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;



            $this->load->view($this->class_name . '/form2', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
		$this->access_right->otoritas('edit', true);
        $this->add($id);
    }

	public function export(){
		$this->load->library('PHPExcel');

		$params = array();

		$result = $this->sources_model->get_data($params)->result_array();
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Master-Supplier")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Source Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Source Name')->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 1, 'Source Address')->getStyle('C1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 1, 'Source Phone')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, 'Source Contact Person')->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 1, 'Source Email')->getStyle('F1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['source_code']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['source_name']);
            $sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['source_address']);
            $sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['source_phone']);
            $sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['source_contact_person']);
            $sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['source_email']);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Source-Master'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

    public function proses() {
        $proses_result['success'] = 1;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('source_code', 'Source Code', 'required|trim');
            $this->form_validation->set_rules('source_name', 'Source Name', 'required|trim');
            $this->form_validation->set_rules('source_address', 'Source Address', 'required|trim');

            if ($this->form_validation->run()) {
                $id = $this->input->post('id');

                $data = array(
                    'source_code' => $this->input->post('source_code'),
                    'source_name' => $this->input->post('source_name'),
                    'source_address' => $this->input->post('source_address'),
                    'source_phone' => $this->input->post('source_phone'),
                    'source_contact_person' => $this->input->post('source_contact_person'),
                    'source_email' => $this->input->post('source_email'),
                    'source_category_id' => $this->input->post('source_category_id'),
                    'source_city' => $this->input->post('source_city')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $create = $this->sources_model->create($data);
                    if ($create) {
                        $id = $this->db->insert_id();
                        $proses_result['add'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Source Master');
						/* END OF INSERT LOG */

                    }
                } else {
                    if ($this->sources_model->update($data, $id)) {
                        $proses_result['edit'] = 1;
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Source Master');
						/* END OF INSERT LOG */
                    }
                }


            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();

        if ($this->access_right->otoritas('delete', true)) {

            $id = $this->input->post("id");
            $result = $this->sources_model->delete($id);

        } else {

            $result['status'] = 'ERR';
			$result['no_delete'] = 1;

        }

        echo json_encode($result);
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Source Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        'Source Code'           =>  'source_code',
                        'Source Name'           =>  'source_name',
                        'Source City'           =>  'source_city',
                        'Source Address'        =>  'source_address',
                        'Source Phone'          =>  'source_phone',
                        'Source Email'          =>  'source_email',
                        'Source Contact Person' =>  'source_contact_person',
                        'Source Type'           =>  'source_type'
                    ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->sources_model->importFromExcel($data);

                        if($process['status']){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage($process['message']);

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }
 
        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }    
}

/* End of file referensi_supplier.php */
/* Location: ./application/controllers/referensi_supplier.php */

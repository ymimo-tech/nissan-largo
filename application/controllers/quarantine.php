<?php

/**
 * Description of item_stok
 *
 * @author SANGGRA HANDI
 */
class quarantine extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        /* Otoritas */
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model','referensi_satuan_model','referensi_kategori_model'));
    }

    function __rev1(){
        $this->quarantine_model->rev1();
        echo 'berhasil';
    }

/*    public function testt(){

        $data = $this->db->get('item_receiving_details')->result_array();

        foreach ($data as $d) {

            $len = strlen("36314A613130533030303030303037");

            if( strlen($d['unique_code']) == $len){

                $serialNumber = hex2bin($d['unique_code']);
                $this->db
                    ->set('rfid_tag',$serialNumber)
                    ->where('unique_code',$d['unique_code'])
                    ->update('item_receiving_details');

            }else if( strlen($d['unique_code']) > $len ){
                // $serialNumber = substr($d['unique_code'], 0, $len);
                // $serialNumber = hex2bin($d['unique_code']);

                // $this->db
                //     ->set('rfid_tag',$serialNumber)
                //     ->where('unique_code',$d['unique_code'])
                //     ->update('item_receiving_details');

            }else{
                $serialNumber = bin2hex($d['unique_code']);

                $this->db
                    ->set('rfid_tag',$d['unique_code'])
                    ->set('unique_code', $serialNumber)
                    ->where('unique_code',$d['unique_code'])
                    ->update('item_receiving_details');
            }
        }
        exit;

    }
*/
    public function index() {
        $data['title'] = 'Quarantine List';
        $data['subTitle'] = '<i class="fa fa-th-list"></i> Quarantine LIST';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;

        $this->include_required_file($data);
		$data['jsPage'][] = "assets/custom/js/share.js";
        $data['jsPage'][] = "assets/pages/scripts/quarantine/quarantine_view.js";
        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
        $data['today'] = date('d/m/Y');

        /* Otoritas Tombol tambah */

            $data['button_group'] = array();


        //=========tombol cetak excel==========
        $base_url_images = base_url() . 'images';
        $base_url = base_url() . $this->class_name;
        if ($this->access_right->otoritas('print')) {
            $data['print_group'] = array(
                anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $base_url . '/excel')),
                anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel-format2', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel format 2', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $base_url . '/excel_format2'))
            );


        }
        //-------------------------------------
            $data['status'] = $this->quarantine_model->getStatus();
			$data['category'] = $this->quarantine_model->getCategory();
			$data['location'] = $this->quarantine_model->getLocation();
			$data['location_type'] = $this->quarantine_model->getLocationType();
			$data['item'] = $this->quarantine_model->getItem();
            $data['uom'] = $this->quarantine_model->getUom();


			//$this->load_model('dashboard_model');
			//$data['stock'] = $this->dashboard_model->getActiveStock();
			$data['low'] = $this->quarantine_model->getLowQty();

            //$data['options_satuan'] = $this->referensi_satuan_model->options("--Semua--");
            //$data['options_kategori']   = $this->referensi_kategori_model->options("--Semua--");
            //$data['tahun_aktif'] = hgenerator::list_year(); //----Tahun Aktif------------
            //$data['options_kelas']     = hgenerator::opsi_kelas_plafond('','--Semua--');
            //$data['options_satuan']    = hgenerator::opsi_satuan_nominal('','--Semua--');
        /* INSERT LOG */
        $this->access_right->activity_logs('view','Master Item Stock');
        /* END OF INSERT LOG */

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $this->load->view('template', $data);
    }

    public function getLowQty(){

        $params = array(
            'warehouse'   => $this->input->post('warehouse')
        );

        $results['total'] = $this->quarantine_model->getLowQty($params);

        echo json_encode($results);
    }

    public function option(){

        $query = $this->input->post('query');

        // if(!empty($query)){
        //     $this->db->like('lower(a.name)',strtolower($query));
        // }

        $locations = $this->db
                        ->select('id, code, name')
                        ->from('items')
                        // ->where('lower(name) like', strtolower($query))
                        ->like('lower(name)',strtolower($query))
                        ->get()
                        ->result_array();
        $data= array();

        foreach ($locations as $loc) {
            $data['results'][] = array(
                'id' => $loc['id'],
                'name' => $loc['name'],
                'code'  => $loc['code']
            );
        }

        echo json_encode($data);
    }

    public function code(){

        $query = $this->input->post('query');

        // if(!empty($query)){
        //     $this->db->like('lower(a.name)',strtolower($query));
        // }

        $locations = $this->db
                        ->select('id, code, name')
                        ->from('items')
                        // ->where('lower(name) like', strtolower($query))
                        ->like('lower(code)',strtolower($query))
                        ->get()
                        ->result_array();
        $data= array();

        foreach ($locations as $loc) {
            $data['results'][] = array(
                'id' => $loc['id'],
                'name' => $loc['code'],
                'code'  => $loc['code']
            );
        }

        echo json_encode($data);
    }



	public function getListQty(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
            // 'warehouse' => $this->input->post('warehouse')
		);

        $result['total_qty'] = $this->quarantine_model->quantity_qc_hold($params['id_barang']);
        // dd($result);


		echo json_encode($result);
	}

	public function getListQtyDataTable(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->quarantine_model->getListQtyDataTable($params);

		echo json_encode($result);
	}

	public function export(){
		$this->load->library('PHPExcel');

		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
			'id_satuan'		=> $this->input->post('id_satuan'),
			'id_qc'			=> $this->input->post('id_qc'),
			'id_kategori'	=> $this->input->post('id_kategori'),
			'loc_id'		=> $this->input->post('loc_id'),
			'loc_type'		=> $this->input->post('loc_type'),
            'warehouse'     => $this->input->post('warehouse'),
			'params'		=> $this->input->post('params')
        );


		foreach($params as $k => $v){
            $params[$k] = str_replace('null','',$v);
		}

		$result = $this->quarantine_model->getListRaw($params);
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Active-Inventory")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Item Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Item Name')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Category')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'UoM')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Location')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Location Type')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Qty')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'Minimum Replenishment Stock')->getStyle('H1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['kd_barang']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['nama_barang']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['nama_kategori']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['nama_satuan']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['loc_name']);
			$sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['loc_type_all']);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['qty']);
			$sheet->setCellValueByColumnAndRow(7, $row, $result[$i]['minimum_stock']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Active-Inventory'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

    public function export_format2(){
        $this->load->library('PHPExcel');

        $params = array(
            'id_barang'     => $this->input->post('id_barang'),
            'id_satuan'     => $this->input->post('id_satuan'),
            'id_qc'         => $this->input->post('id_qc'),
            'id_kategori'   => $this->input->post('id_kategori'),
            'loc_id'        => $this->input->post('loc_id'),
            'loc_type'      => $this->input->post('loc_type'),
            'warehouse'     => $this->input->post('warehouse'),
            'params'        => $this->input->post('params')
        );

        foreach($params as $k => $v){
            $params[$k] = str_replace('null','',$v);
        }

        $result = $this->quarantine_model->getListRawFormat2($params);
        $iLen = count($result);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Active-Inventory")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'Item Code')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, 'Item Name')->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 1, 'Category')->getStyle('C1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 1, 'UoM')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, 'Location')->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 1, 'Location Type')->getStyle('F1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(6, 1, 'Qty')->getStyle('G1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(7, 1, 'Minimum Replenishment Stock')->getStyle('H1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['kd_barang']);
            $sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['nama_barang']);
            $sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['nama_kategori']);
            $sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['nama_satuan']);
            $sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['loc_name']);
            $sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['loc_type_all']);
            $sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['qty']);
            $sheet->setCellValueByColumnAndRow(7, $row, $result[$i]['minimum_stock']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Active-Inventory'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    function duration(){
        $data = array();
        $data = [
            ['id' => 1,
            'name' => 'Kurang dari 30 Hari'],
            ['id' => 2,
            'name' => 'Kurang dari 60 Hari'],
            ['id' => 3,
            'name' => 'Kurang dari 120 Hari'],
        ];

        echo json_encode($data);

    }

	public function getList(){
        $result = array();

		ini_set('memory_limit','2048M');
		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
			'code_barang'		=> $this->input->post('code_barang'),
			// 'id_qc'			=> '2',
			'auto_release'	=> $this->input->post('auto_release'),
			'quarantine'		=> $this->input->post('quarantine'),
			'loc_type'		=> $this->input->post('loc_type'),
            'warehouse'     => $this->input->post('warehouse'),
			'params'		=> $this->input->post('params')
        );

        $result = $this->quarantine_model->getList($params);

		echo json_encode($result);
	}

    public function get_list(){
		$param = array();

		$idBarang = $this->input->post('id_barang');
		if(!empty($idBarang))
			$param['a.id_barang'] = filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_STRING);

		$idSatuan = $this->input->post('id_satuan');
		if(!empty($idSatuan))
			$param['a.id_satuan'] =  filter_var(trim($this->input->post('id_satuan')), FILTER_SANITIZE_STRING);

        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $barang = $this->quarantine_model->data($param)->get();
        $iTotalRecords = $this->quarantine_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($barang->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_barang,'delete' => $value->id_barang));

            $records["data"][] = array(
                //'<input type="checkbox" name="id[]" value="'.$value->id_barang.'">',
                $_REQUEST['start']+$i,
                "<a href='".base_url($this->class_name)."/detail/".$value->id_barang."'>".$value->kd_barang."</a>",
                $value->nama_barang,
				$value->nama_kategori,
                $value->kd_satuan,
                $value->qty,
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

	public function get_list_detail(){
		$result = array();

		$params = array(
            'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
            'status_quarantine' => $this->input->post('status_quarantine'),
            'warehouse' => $this->input->post('warehouse'),
            'from_in'          => filter_var(trim($this->input->post('from_in')), FILTER_SANITIZE_STRING),
            'to_in'            => filter_var(trim($this->input->post('to_in')), FILTER_SANITIZE_STRING),
            // 'from_out'          => filter_var(trim($this->input->post('from_out')), FILTER_SANITIZE_STRING),
            // 'to_out'            => filter_var(trim($this->input->post('to_out')), FILTER_SANITIZE_STRING),
            'params'	=> $this->input->post('params'),
            'status_item'      => $this->input->post('status_item')
        );

        $result = $this->quarantine_model->getDetailList($params);

		echo json_encode($result);
    }

    public function QuarantineDetailUpdate(){
        $data = $this->db
                    ->select("DISTINCT(ird.item_id) as itemd_id")
                    ->from('item_receiving_details ird')
                    ->where('qc_id', 2)
                    ->get()
                    ->result_array();

        foreach ($data as $key => $value) {
            $params = array('id_barang'	=> $value['itemd_id'] );
            $update = $this->quarantine_model->UpdateQuarantineDetailList($params, 'update');
        }
    }

    public function Api_quarantine($parameter=[], $item_id ){
        $results = array();
        $data_item = $this->quarantine_model->get_data_item($item_id);

        $data = count($parameter["unique_code"]);

        for($i = 0; $i < $data; $i++){
            $results['data'][] = array(
                "serial_number" => $parameter["unique_code"][$i],
                "qty" => $parameter["data_qty"][$i],
                "status_item" => $parameter["data_statusItem"][$i],
                "max_quarantine" => $parameter["data_maxQuarantine"][$i],
                "in_date" => $parameter["data_inDate"][$i],
            );
        }

        $data_merge = array_merge($data_item,$results);

        $result['status']	= 200;
		$result['message']	= 'Success';
		$result['response']	= true;
        $result['results']	= $data_merge;

        //Send data//

        return json_encode($result);
    }

    public function manual_release(){
        $result = [];

        $unique_code = explode(',',$this->input->get('unique_code'));
        $data_qty = explode(',',$this->input->get('data_qty'));
        $data_statusItem = explode(',',$this->input->get('data_statusItem'));
        $data_maxQuarantine = explode(',',$this->input->get('data_maxQuarantine'));
        $data_inDate = explode(',',$this->input->get('data_inDate'));


        $data = [];

        $id_item = $this->input->get('item_id');

        $parameter = array(
            "unique_code" => $unique_code,
            "data_qty" => $data_qty,
            "data_statusItem" => $data_statusItem,
            "data_maxQuarantine" => $data_maxQuarantine,
            "data_inDate" => $data_inDate
        );

        $this->Api_quarantine($parameter, $id_item);
        foreach($unique_code as $unique){
            $id_barang = $this->quarantine_model->get_id_barang($unique);
            $checker = $this->quarantine_model->check_sn($unique, '1');
            if($checker == FALSE){
                $result['status'] = FALSE;
                $result['serial_number'] = $unique;
                break;
            }else{
                $this->quarantine_model->release($id_barang, $unique);
                $result['status'] = TRUE;
            }
        }
        echo json_encode($result);
    }

    public function action_release(){
        $unique_code = $this->input->post('unique_code');
        $result = $this->quarantine_model->release('',$unique_code);

        echo json_encode($result);
    }

    public function action_reject(){

        $params = array(
            'unique_code' => $this->input->post('unique_code'),
            'reason_reject' => $this->input->post('remark')
        );
        $result = $this->quarantine_model->reject('',$params);
        echo json_encode($result);
    }

    public function action_hold(){
        $unique_code = $this->input->post('unique_code');
        $result = $this->quarantine_model->hold('',$unique_code);

        echo json_encode($result);
    }

    public function manual_reject(){
        $result = [];

        $unique_code = explode(',',$this->input->get('unique_code'));
        $data_qty = explode(',',$this->input->get('data_qty'));
        $data_statusItem = explode(',',$this->input->get('data_statusItem'));
        $data_maxQuarantine = explode(',',$this->input->get('data_maxQuarantine'));
        $data_inDate = explode(',',$this->input->get('data_inDate'));
        $reason_reject = $this->input->get('reason_reject');


        $data = [];

        $id_item = $this->input->get('item_id');

        $parameter = array(
            "unique_code" => $unique_code,
            "data_qty" => $data_qty,
            "data_statusItem" => $data_statusItem,
            "data_maxQuarantine" => $data_maxQuarantine,
            "data_inDate" => $data_inDate,
            'reason_reject' => $reason_reject
        );

        $this->Api_quarantine($parameter, $id_item);

        foreach($unique_code as $unique){
            $id_barang = $this->quarantine_model->get_id_barang($unique);
            $checker = $this->quarantine_model->check_sn($unique, '3');
            if($checker == FALSE){
                $result['status'] = FALSE;
                $result['serial_number'] = $unique;
                break;
            }else{
                $this->quarantine_model->reject($id_barang, $unique, $reason_reject);
                $result['status'] = TRUE;
            }
        }
        echo json_encode($result);
    }

    public function manual_hold(){
        $result = [];

        $unique_code = explode(',',$this->input->get('unique_code'));
        $data_qty = explode(',',$this->input->get('data_qty'));
        $data_statusItem = explode(',',$this->input->get('data_statusItem'));
        $data_maxQuarantine = explode(',',$this->input->get('data_maxQuarantine'));
        $data_inDate = explode(',',$this->input->get('data_inDate'));


        $data = [];

        $id_item = $this->input->get('item_id');

        $parameter = array(
            "unique_code" => $unique_code,
            "data_qty" => $data_qty,
            "data_statusItem" => $data_statusItem,
            "data_maxQuarantine" => $data_maxQuarantine,
            "data_inDate" => $data_inDate
        );

        $this->Api_quarantine($parameter, $id_item);

        foreach($unique_code as $unique){
            $id_barang = $this->quarantine_model->get_id_barang($unique);
            $checker = $this->quarantine_model->check_sn($unique, '2');
            if($checker == FALSE){
                $result['status'] = FALSE;
                $result['serial_number'] = $unique;
                break;
            }else{
                $this->quarantine_model->hold($id_barang, $unique);
                $result['status'] = TRUE;
            }
        }
        echo json_encode($result);
    }

    public function get_list_detail_export(){
        $result = array();

        $params = array(
            'id_barang' => filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
            'warehouse' => $this->input->post('warehouse'),
            'params'    => $this->input->post('params')
        );

        $result = $this->quarantine_model->getDetailListExport($params);
        $iLen = count($result);

        $this->load->library('PHPExcel');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("".$params['id_barang']."- Quarantine-Inventory")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        // $sheet->getColumnDimension('L')->setAutoSize(true);


        $sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
        // $sheet->setCellValueByColumnAndRow(1, 1, 'SKU')->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, 'Serial Number')->getStyle('C1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 1, 'Qty')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 1, 'Lot Number')->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, 'Doc Reference')->getStyle('F1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 1, 'In Date')->getStyle('G1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(6, 1, 'Location')->getStyle('H1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(7, 1, 'Quarantine Duration')->getStyle('I1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(8, 1, 'Date Release')->getStyle('J1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(9, 1, 'Auto Release')->getStyle('K1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(10, 1, 'Status')->getStyle('L1')->getFont()->setBold(true);


        $row = 2;
        for($i = 0; $i < $iLen; $i++){

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i][0]);
            $sheet->setCellValueByColumnAndRow(1, $row, $result[$i][1]);
            $sheet->setCellValueByColumnAndRow(2, $row, $result[$i][2]);
            $sheet->setCellValueByColumnAndRow(3, $row, $result[$i][3]);
            $sheet->setCellValueByColumnAndRow(4, $row, $result[$i][4]);
            $sheet->setCellValueByColumnAndRow(5, $row, $result[$i][5]);
            $sheet->setCellValueByColumnAndRow(6, $row, $result[$i][6]);
            $sheet->setCellValueByColumnAndRow(7, $row, $result[$i][7]);
            $sheet->setCellValueByColumnAndRow(8, $row, $result[$i][8]);
            $sheet->setCellValueByColumnAndRow(9, $row, $result[$i][9]);
            $sheet->setCellValueByColumnAndRow(10, $row, $result[$i][10]);
            // $sheet->setCellValueByColumnAndRow(11, $row, $result[$i][11]);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$params['id_barang'].'- Quarantine-Inventory.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

        // echo json_encode($result);
    }

    public function __get_list_detail1(){
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $id_barang = $this->input->post('id_barang');
        $receiving = $this->quarantine_model->get_receiving_by_id_barang($id_barang)->get();
        $iTotalRecords = $this->quarantine_model->get_receiving_by_id_barang($id_barang)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($receiving->result() as $value){
            //echo $value->kd_parent;
            $kd_parent = 1212;//empty($value->kd_parent) ? $value->kd_parent : '-' ;
            $records["data"][] = array(
				'<input type="checkbox" name="sn[]" value="'.$value->kd_unik.'">',
                $_REQUEST['start'] + $i,
                $value->kd_unik,
                $kd_parent,
                $value->kd_receiving,
                DateTime::createFromFormat('Y-m-d', $value->tgl_exp)->format('d/m/Y'),
                DateTime::createFromFormat('Y-m-d H:i:s', $value->tgl_in)->format('d/m/Y'),
                $value->loc_name,
				$value->loc_type,
				$value->kd_qc
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    //DETAIL
    public function detail($id){
        if ($this->access_right->otoritas('view')) {
            $data['title'] = 'Quarantine Inventory';
            $data['subTitle'] = '<i class="fa fa-th-list"></i> DETAIL QUARANTINE INVENTORY';
            $data['page'] = $this->class_name . '/detail';

            $this->include_required_file($data);
            // $data['jsPage'][] = "assets/pages/scripts/report/lr-material-in.js";
            $data['jsPage'][] = "assets/pages/scripts/quarantine/quarantine_detail.js";
            $data['jsPage'][] = "assets/pages/scripts/quarantine/stock-card.js";
            $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
            $data['id'] = $id;
            $data['today'] = date('d/m/Y');
            $data['barang'] = $this->quarantine_model->get_by_id($id)->row();
            $data['quarantine'] = $this->db
                                                ->select('quarantineduration, autorelease')
                                                ->from('items')
                                                ->where('id', $id)
                                                ->get()
                                                ->row_array();
            $data['stock_card_total'] = $this->quarantine_model->getStockCardTotal($id);

            /* INSERT LOG */
            $this->access_right->activity_logs('view','Item stock detail');
            /* END OF INSERT LOG */

            $this->load->view('template', $data);
        }
    }

    private function include_required_file(&$data){
        $data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['csss'][] = "assets/global/plugins/select2/css/select2.min.css";
        $data['csss'][] = "assets/global/plugins/select2/css/select2-bootstrap.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
        $data['jsPage'][] = 'assets/global/plugins/select2/js/select2.min.js';
        $data['jsPage'][] = 'assets/custom/js/share.js';

        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        // $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        // $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        // $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        // $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        // $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        // $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        // $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
        // $data['jsPage'][] = 'assets/custom/js/share.js';
    }

	public function getStockCard(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
            'warehouse' => $this->input->post('warehouse'),
			'start'		=> filter_var(trim($this->input->post('start')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->quarantine_model->getStockCard($params);

		echo json_encode($result);
	}

    public function save_pdf($page = 0) {
        $this->load->library("custom_table_plain");

        $table = new stdClass();
        $header[0] = array(
                            "Item Code", 1, 1,
                            "Item Name", 1, 1,
                            "UoM",1,1,
                            "Category", 1, 1,
                            "QTY",1,1,
                        );

        $table->header = $header;
        $table->id     = 't_item_stok';
        $table->align = array('jabatan' => 'left', 'jenis_perawatan' => 'center',  'kelas' => 'center',  'plafond' => 'right',  'satuan' => 'center','jumlah'=>'right', 'aksi' => 'center','jumlah_po'=>'right');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "quarantine_model->data_table_plain";
        $table->limit = 10000;
        $table->page = 1;
        $data = $this->custom_table_plain->generate($table);
        //$this->load->view($this->class_name . '/save_pdf', $data);
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        $filename = "Item Stok";
        $pdfFilePath = FCPATH."/downloads/reports/$filename.pdf";
        $data['title'] = 'Item Stok'; // pass data to the view
        //

        /*if (file_exists($pdfFilePath) == FALSE)
        {*/
            ini_set('memory_limit','2048M'); // boost the memory limit if it's low <img class="emoji" draggable="false" alt="😉" src="https://s.w.org/images/core/emoji/72x72/1f609.png">
            //$html = $this->load->view($this->class_name . '/save_pdf',$data,TRUE);
            $html = $this->load->view($this->class_name . '/save_pdf', $data, true);


            $this->load->library('pdf');
            $pdf = $this->pdf->load();
            $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img class="emoji" draggable="false" alt="😉" src="https://s.w.org/images/core/emoji/72x72/1f609.png">
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'F'); // save to file because we can
        //}

        redirect("/downloads/reports/$filename.pdf");
    }

	public function printZpl(){
		$result = array();
		$this->load->library('mimoclientprint');


        $this->db
            ->select('unique_code, rfid_tag')
            ->from('item_receiving_details')
            ->where_in('rfid_tag',$this->input->post('sn'))
            ->group_by('unique_code,rfid_tag');

        $data = $this->db->get()->result_array();
        $len = count($data);
        $cmd = '';

        foreach ($data as $d) {

            $tpl = file_get_contents(APPPATH . 'views/label/label.tpl');

            $tpl = str_replace('{serial_number}', $d['unique_code'], $tpl);
            $tpl = str_replace('{barcode}', $d['unique_code'], $tpl);
            $tpl = str_replace('{month}', substr($d['unique_code'], 3,4), $tpl);
            $tpl = str_replace('{year}', substr($d['unique_code'], 0,2), $tpl);

            $cmd .= $tpl;

        }

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);

		echo json_encode($result);
	}

    public function printZplLP(){
        $result = array();
        $this->load->library('mimoclientprint');
        $ol = $this->input->post('sn');

        $cmd="";
        if($ol){

            foreach ($ol as $v) {

                $tpl = file_get_contents(APPPATH . 'views/label/outerlabel.tpl');

                $tpl = str_replace('{barcode}', $v, $tpl);
                $tpl = str_replace('{outerlabel}', $v, $tpl);
                $tpl = str_replace('{month}', substr($v, 5,6), $tpl);
                $tpl = str_replace('{year}', substr($v, 3,4), $tpl);

            }

        }

        $params = array(
            'printerCommands'   => $cmd
        );

        $result = $this->mimoclientprint->printIt($params);

        echo json_encode($result);
    }

    // public function printZplLP(){

    //     $result = array();
    //     $this->load->library('mimoclientprint');

    //     $ol = $this->input->post('sn');

    //     $cmd = '';

    //     for ($i=0; $i < count($ol); $i++) {
    //         $tpl = file_get_contents(APPPATH . 'views/label/label_asn_zulu.tpl');

    //         $tpl = str_replace('{LP_Barcode}', $ol[$i], $tpl);
    //         $tpl = str_replace('{LP_Value}', $ol[$i], $tpl);

    //         $this->db
    //             ->select('*')
    //             ->from('item_receiving_details ird')
    //             ->join('items itm','itm.id=ird.item_id')
    //             ->where('parent_code',$ol[$i])
    //             ->order_by('ird.unique_code');

    //         $data = $this->db->get()->result_array();
    //         $len = count($data);

    //         if(!$data){
    //             continue;
    //         }

    //         $list1 = round(($len / 3),0);
    //         $list2 = round((($len-$list1)/2),0);
    //         $list3 = ($len-$list1)-$list2;

    //         $sList1=326;
    //         $sList2=326;
    //         $sList3=326;

    //         $tempList1="";
    //         $tempList2="";
    //         $tempList3="";

    //         $items=array();

    //         foreach ($data as $d) {
    //             if(array_key_exists($d['code'], $items)){

    //                 $items[$d['code']]['code'] = $d['code'];
    //                 $items[$d['code']]['qty'] = $items[$d['code']]['qty']+1;

    //             }else{

    //                 $items[$d['code']]['code'] = $d['code'];
    //                 $items[$d['code']]['qty'] = 1;

    //             }
    //         }

    //         $i=1;
    //         foreach ($items as $item) {
    //             $tpl = str_replace('{IQ'.$i.'}', $item['qty'], $tpl);
    //             $tpl = str_replace('{LP_I'.$i.'}', $item['code'], $tpl);
    //             $i++;
    //         }

    //         for ($i=1; $i < 7; $i++) {
    //             $tpl = str_replace('{IQ'.$i.'}', '', $tpl);
    //             $tpl = str_replace('{LP_I'.$i.'}', '', $tpl);
    //         }

    //         for ($i=0; $i < ($list1-1); $i++) {
    //             $tempList1 .= "^FT36,".$sList1."^A0N,28,28^FH\^FD".$data[$i]['rfid_tag']."^FS";
    //             $sList1=$sList1+34;
    //         }

    //         for ($i=$list1; $i < (($list1+$list2)-1); $i++) {
    //             $tempList2 .= "^FT287,".$sList2."^A0N,28,28^FH\^FD".$data[$i]['rfid_tag']."^FS";
    //             $sList2=$sList2+34;
    //         }

    //         for ($i=($list1+$list2); $i < (($list1+$list2+$list3)-1); $i++) {
    //             $tempList3 .= "^FT542,".$sList3."^A0N,28,28^FH\^FD".$data[$i]['rfid_tag']."^FS";
    //             $sList3=$sList3+34;
    //         }

    //         // foreach ($data as $d) {
    //         //     if($col == 1){
    //         //         $sList1=$sList1+34;
    //         //         $list1 .= "^FT36,".$sList1."^A0N,28,28^FH\^FD".$d['rfid_tag']."^FS";
    //         //         $col++;
    //         //     }else if($col == 2){
    //         //         $sList2=$sList2+34;
    //         //         $list2 .= "^FT287,".$sList3."^A0N,28,28^FH\^FD".$d['rfid_tag']."^FS";
    //         //         $col++;
    //         //     }else{
    //         //         $sList3=$sList3+34;
    //         //         $list3 .= "^FT542,".$sList3."^A0N,28,28^FH\^FD".$d['rfid_tag']."^FS";
    //         //         $col=1;
    //         //     }
    //         // }

    //         $tpl = str_replace('^FT36,326^A0N,28,28^FH\^FD{LP_List1}^FS', $tempList1, $tpl);
    //         $tpl = str_replace('^FT287,326^A0N,28,28^FH\^FD{LP_List2}^FS', $tempList2, $tpl);
    //         $tpl = str_replace('^FT542,327^A0N,28,28^FH\^FD{LP_List3}^FS', $tempList3, $tpl);


    //         // echo $tpl;
    //         // exit;

    //         $cmd .= $tpl;
    //     }

    //     $params = array(
    //         'printerCommands'   => $cmd
    //     );

    //     $result = $this->mimoclientprint->printIt($params);

    //     echo json_encode($result);

    // }

    public function print_single_label($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $title = 'Barcode Label Printing';
            $data['title'] = '<i class="icon-edit"></i> ' . $title;
            $data['form_action'] = $this->class_name . '/proses';
            $data['qty_print'] = $this->quarantine_model->get_detail_by_id($id)->kd_unik;
            $data['id'] = $id;
            $this->load->view($this->class_name . '/form_barcode',$data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function initPrinting(){
        $this->load->helper('printing_label_helper');

        $ci =& get_instance();
        $cmd = '';
        $nc = $ci->input->post('nc');
        $id_rcv = $ci->input->post('id_rcv');
        $kd_unik = $ci->input->post('qty_print');

         $cmd .="
            ^XA";
        $cmd .= "
            ^MMA
            ^PW264
            ^LL0144
            ^LS0
            ^BY2,3,73^FT41,92^BCN,,Y,N
            ^FD>;".$kd_unik."^FS
            ^PQ1,0,1,Y
            ~JSB
            ^XZ";


                    //echo $i,'-'.$serial_number.'<br>';
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>

            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>
                <br />
            </div>

            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>



            </div>

        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>

            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc).
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D

            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.
            </div>
        </fieldset>
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }

    public function get_detail_item_stok($id_barang) {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Detail Item Stok';
        $data['page'] = $this->class_name . '/get_detail_item_stok';
        $data['data_source'] = base_url($this->class_name . '/load_detail_item_stok');

        //=========tombol cetak excel==========
        $base_url_images = base_url() . 'images';
        $base_url = base_url() . $this->class_name;
        if ($this->access_right->otoritas('print')) {
            $data['print_group'] = array(
                anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel-'.$id_barang, 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel', 'onclick' => 'do_print(this.id, \'#ffilter_detail_'.$id_barang.'\')', 'data-source' => $base_url . '/excel_detail/'.$id_barang))
            );
        }
        //-------------------------------------
        $this->load_model('referensi_satuan_model');
        $data['options_satuan'] = $this->referensi_satuan_model->options();

         $data['id_barang'] = $id_barang;
        /* INSERT LOG */
        $this->access_right->activity_logs('view','detail item_stok');
        /* END OF INSERT LOG */
        $this->load->view('item_stok/detail_item_stok2', $data);
    }

    public function load_detail_item_stok($page = 0) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "Serial Number", 1, 1,
                            "Location", 1, 1,
                            "Exp Date",1,1,
                            "Status",1,1,
                            'Action',1,1,
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "Serial Number", 1, 1,
                            "Location", 1, 1,
                            "Exp Date",1,1,
                            "Status",1,1,
                            'Action',1,1,
                            );
        }

        $table->header = $header;
        $table->id     = 't_item_stok_detail';
        $table->align = array('kd_barang' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center', 'aksi' => 'center','nama_barang'=>'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "quarantine_model->data_table_detail";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table_detail->generate($table);

        echo json_encode($data);
    }


    public function excel($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "KODE BARANG", 1, 1,
                            "NAMA BARANG", 1, 1,
                            "SATUAN",1,1,
                            "KATEGORI", 1, 1,
                            "JUMLAH",1,1,
                            'PO',1,1
                        );


        $table->header = $header;
        $table->id     = 't_barang';
        $table->align = array('jabatan' => 'left', 'jenis_perawatan' => 'center',  'kelas' => 'center',  'plafond' => 'right',  'satuan' => 'center','jumlah'=>'right','jumlah_po'=>'right', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "quarantine_model->data_table_excel";
        $table->limit = $this->limit;
        $table->page = 1;
        $table->border = 1;
        $data = $this->custom_table->generate($table);

        //===========filter di excel===================
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang = $this->input->post('nama_barang');
        $id_satuan = $this->input->post('id_satuan');
        $id_kategori = $this->input->post('id_kategori');
        $tanggal_awal = $this->input->post('tanggal_awal_bbk');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbk');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        if($id_satuan==''){
            $satuan = 'Semua';
        }else{
            $this->load_model('referensi_satuan_model');
            $data_satuan  = $this->referensi_satuan_model->get_by_id($id_satuan)->row();
            $satuan       = $data_satuan->nama_satuan;
        }
        if($id_kategori==''){
            $kategori = 'Semua';
        }else{
            $this->load_model('referensi_kategori_model');
            $data_kategori  = $this->referensi_kategori_model->get_by_id($id_kategori)->row();
            $kategori       = $data_kategori->nama_kategori;
        }
        if($tanggal_awal==''||$tanggal_akhir==''){
            $tanggal_awal = 'Semua';
            $tanggal_akhir = 'Semua';
        }

        $filter = array(
            'Kode Barang'       => $kd_barang,
            'Nama Barang'       => $nama_barang,
            'Satuan'            =>$satuan,
            'Kategori'          => $kategori,
            'Tahun Aktif'       => $tahun_aktif,
        );
        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Data Barang';
        $data['filename'] = 'Data Barang('.$date.')';
        $this->load->view('cetak_excel',$data);
    }

    public function excel_detail_bbm($id) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "TANGGAL BBM",1,1,
                            "KODE BBM", 1, 1,
                            "KODE PURCHASE ORDER", 1, 1,
                            "SUPPLIER",1,1,
                            "JUMLAH",1,1
                        );

        $table->header = $header;
        $table->kolom  = 6;
        $table->id     = 't_barang_detail';
        $table->align = array('tanggal_barang' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "quarantine_model->data_table_detail_bbm_excel";
        $table->page = 1;
        $table->border = 1;
        $data = $this->custom_table_detail->generate($table);
        return $data;

    }

    public function excel_detail_bbk($id) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "TANGGAL BBK",1,1,
                            "KODE BBK", 1, 1,
                            "PROYEK", 1, 1,
                            "JUMLAH",1,1
                        );

        $table->header = $header;
        $table->kolom = 5;
        $table->id     = 't_barang_detail';
        $table->align = array('tanggal_barang' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "quarantine_model->data_table_detail_bbk_excel";
        $table->page = 1;
        $table->border = 1;
        $data = $this->custom_table_detail->generate($table);

        return $data;
    }

    public function excel_detail($id,$tahun_aktif){
        $data['table_bbk'] = $this->excel_detail_bbk($id);
        $data['table_bbm'] = $this->excel_detail_bbm($id);
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }

        $jumlah_awal = $this->quarantine_model->jumlah_awal($id,$tahun_aktif);
        $jumlah_son = $this->quarantine_model->jumlah_son($id,$tahun_aktif);
        $tahun_aktif = $tahun_aktif;

        //============data detail==================
        $data_temp = $this->quarantine_model->get_by_id($id,$tahun_aktif);
        if($data_temp->num_rows()>0){

            $barang = $data_temp->row();
            $jumlah_total = $barang->jumlah_awal + $barang->jumlah_masuk - $barang->jumlah_keluar + $barang->surplus_son - $barang->minus_son;
            //$jumlah_total = $barang->jumlah_awal + $barang->jumlah_masuk - $barang->jumlah_keluar;
            $filter = array(
                'Kode Barang'  =>$barang->kd_barang,
                'Nama Barang'  =>$barang->nama_barang,
                'Satuan'       =>$barang->nama_satuan,
                'Kategori'     =>$barang->nama_kategori,
                'Jumlah Total' =>$jumlah_total,
                'Jumlah Awal'  =>$jumlah_awal,
                'Jumlah Stok Opname' => $jumlah_son,
                'Tahun Aktif'  =>$tahun_aktif,
                );
            $data['filter'] = $filter;
        }
        //------------------------------------------------

        $date = date('Ymd His');
        $data['judul_kecil'] = 'Detail Barang';
        $data['filename'] = 'Detail Barang ('.$date.')';
        $this->load->view('cetak_excel_detail_barang',$data);

    }

    public function excel_detail_po_2($id,$tahun_aktif ='') {

        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "TANGGAL BBK",1,1,
                            "KODE BBK", 1, 1,
                            "PROYEK", 1, 1,
                            "JUMLAH",1,1
                        );

        $table->header = $header;
        $table->kolom = 5;
        $table->id     = 't_barang_detail';
        $table->align = array('tanggal_barang' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "quarantine_model->data_table_detail_po_excel";
        $table->page = 1;
        $table->border = 1;
        $data = $this->custom_table_detail->generate($table);

        return $data;
    }

    public function excel_detail_po($id,$tahun_aktif){
        $data['table_po'] = $this->excel_detail_po_2($id,$tahun_aktif);
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }

        $tahun_aktif = $tahun_aktif;

        //============data detail==================
        $data_temp = $this->quarantine_model->get_by_id($id,$tahun_aktif);
        if($data_temp->num_rows()>0){

            $barang = $data_temp->row();
            $jumlah_total = $barang->jumlah_awal + $barang->jumlah_masuk - $barang->jumlah_keluar + $barang->surplus_son - $barang->minus_son;
            //$jumlah_total = $barang->jumlah_awal + $barang->jumlah_masuk - $barang->jumlah_keluar;
            $filter = array(
                'Kode Barang'  =>$barang->kd_barang,
                'Nama Barang'  =>$barang->nama_barang,
                'Satuan'       =>$barang->nama_satuan,
                'Kategori'     =>$barang->nama_kategori,
                'Tahun Aktif'  =>$tahun_aktif,
                );
            $data['filter'] = $filter;
        }
        //------------------------------------------------

        $date = date('Ymd His');
        $data['judul_kecil'] = 'Detail Barang';
        $data['filename'] = 'Detail Barang ('.$date.')';
        $this->load->view('cetak_excel_detail_barang',$data);

    }

    public function stockcard_to_excel(){

        $allQty = $this->input->post('all_qty');
        $params = array(
            'id_barang' => filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_STRING)
        );

        $this->load->library('PHPExcel');
        $data = $this->quarantine_model->getStockCard($params);
        $itemName = $data['item_name'] ;
        $itemCode = $data['item_code'] ;

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle('Stock Card '.$itemName)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'SKU')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, $itemCode.' - '.$itemName)->getStyle('B1')->getFont()->setBold(true);


        $sheet->setCellValueByColumnAndRow(0, 5, 'Date')->getStyle('A5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 5, 'In')->getStyle('B5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 5, 'Out')->getStyle('C5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 5, 'Balance')->getStyle('D5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 5, 'By')->getStyle('E5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 5, 'Ref Doc')->getStyle('F5')->getFont()->setBold(true);

        $row = 6;

        $data = $data['data'];

        $total = $allQty;

        foreach($data as $k){

            $in = ($k['type'] == 'IN') ? $k['qty'] : 0;
            $out = ($k['type'] == 'OUT') ? $k['qty'] : 0;

            $sheet->setCellValueByColumnAndRow(0, $row, $k['date']);
            $sheet->setCellValueByColumnAndRow(1, $row, $in);
            $sheet->setCellValueByColumnAndRow(2, $row, $out);
            $sheet->setCellValueByColumnAndRow(3, $row, $total);
            $sheet->setCellValueByColumnAndRow(4, $row, $k['user_name']);
            $sheet->setCellValueByColumnAndRow(5, $row, $k['doc']);

            $total = ($total - $k['qty']);

            $row++;

        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Stock Card '.$itemCode.' - '. date('Ymd') .'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    public function get_location_list(){

        $params = array(
            "keyword"  => $this->input->post('term'),
            "page"      => $this->input->post('page')
        );


        $data  = $this->quarantine_model->get_location_list($params);

        echo json_encode($data);

    }
}

/* End of file referensi_hari.php */
/* Location: ./application/controllers/referensi_hari.php */

<?php

/**
 * Description of menu_group
 *
 * @author Warman Suganda
 */
class menu_group extends CI_Controller {

    private $class_name;
    private $limit = 10;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        /*$this->access_right->check();
        $this->access_right->otoritas('view', true);*/

        // Global Model
        $this->load->model($this->class_name . '_model');
    }

    public function index() {
        $data['title'] = '<i class="icon-tasks"></i> Grup Menu';
        $data['page'] = $this->class_name . '/index';
        $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-barang.js";

        $data['button_group'] = array();
        if ($this->access_right->otoritas('view')) {
            $data['button_group'][] = anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')));
        }

        $this->load->view('template', $data);
    }


     private function include_other_view_params(&$data) {
        $data['categories'] = $this->menu_group_model->create_options("kategori", "id_kategori", "kd_kategori");
        $data['satuan'] = $this->menu_group_model->create_options("satuan", "id_satuan", "nama_satuan");
        $data['shipment_types'] = $this->menu_group_model->create_options_code("m_shipment_type", "shipment_type_code", "shipment_type");
        $data['fifo_periods'] = $this->menu_group_model->create_options_code("m_shipment_periode", "periode_code", "periode");
       // $data['shipment_types'] = array('FEFOY' => "FEFOY");
        //$data['fifo_periods'] = array('MONTHLY' => "MONTHLY");
        $data['owners'] = $this->menu_group_model->create_options("owner", "id_owner", "owner_name");
    }

    public function get_list(){
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $barang = $this->menu_group_model->data($param)->get();
        $iTotalRecords = $this->menu_group_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($barang->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_barang,'delete' => $value->id_barang));
            
            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="'.$value->id_barang.'">',
                $_REQUEST['start']+$i,
                $value->kd_barang,
                $value->nama_barang,
                $value->kd_kategori,
                $value->nama_satuan,
                $value->tipe_barang,
                $value->shipment_type,
                $value->fifo_period,
                $value->owner_name,
                $action
            );
            $i++;
        }
        
        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $barang = $this->menu_group_model->get_data(array('id_barang' => $id))->row_array();
            $process_result = $barang;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }


    public function add($id = '',$st_delete='') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';
            if($st_delete!=''){
                $data['form_action'] = $this->class_name . '/proses_delete/'.$id;
                $data['st_delete'] = 1;
            }
            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->menu_group_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;

            $this->load->view($this->class_name . '/form', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
        $this->add($id);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('nama', 'Nama Grup', 'required');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'nama_grup_menu' => $this->input->post('nama'),
                    'icon' => $this->input->post('icon')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->menu_group_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#content_table');
                    }
                } else {
                    if ($this->menu_group_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#content_table');
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function delete($id) {
        $this->access_right->otoritas('delete', true);
        $this->add($id,'delete');
    }

    public function proses_delete($id) {
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->menu_group_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#content_table');
            }
            echo json_encode($message);
        }
    }

}

/* End of file menu_group.php */
/* Location: ./application/controllers/menu_group.php */
<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class cycle_count extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

	public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Cycle Count List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/cycle_count/lr-cycle-count-view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

		$data['today'] = date('d/m/Y');
		$data['status'] = $this->cycle_count_model->getCcStatus();

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Stok Opname - Stok Opname');
        /* END OF INSERT LOG */

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	//ADD
	public function add(){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> New Cycle Count Job';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/cycle_count/lr-cycle-count-addedit.js";

			$data['id'] = '';
			$data['today'] = date('d/m/Y');
			$data['not_usage'] = 'true';
			$data['cc_code'] = $this->cycle_count_model->get_cc_code();
            $data['no_person'] = '';
            $data['users'] = $this->cycle_count_model->get_user();

			$type = array(
				'0'	=> array(
					'type_value' 	=> 'LOC',
					'type_name'		=> 'By Location'
				)
			);

			$data['type'] = $type;
			$data['is_used'] = 'false';

	        /* Get Data Select*/
	        $this->include_other_viewdata($data);

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);
		}
	}

	//EDIT
	public function edit($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Cycle Count Job';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/cycle_count/lr-cycle-count-addedit.js";

			$data['id'] = $id;
			$data['today'] = date('d/m/Y');
			$data['not_usage'] = 'true';
			$data['cc_code'] = '';
            $data['no_person'] = '';
            $data['users'] = $this->cycle_count_model->get_user();

			$type = array(
				'0'	=> array(
					'type_value' 	=> 'SN',
					'type_name'		=> 'By Item'
				),
				'1'	=> array(
					'type_value' 	=> 'LOC',
					'type_name'		=> 'By Location'
				)
			);

			$data['type'] = $type;

			$isUsed = $this->cycle_count_model->checkIsUsed($id);
			if($isUsed['is_used'])
				$data['is_used'] = 'true';
			else
				$data['is_used'] = 'false';

	        $this->include_other_viewdata($data);

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);
		}
	}

	//DETAIL
	public function detail($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Stok Opname Document';
			$data['page'] = $this->class_name . '/detail';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/cycle_count/lr-cycle-count-detail.js";

			$data['id'] = $id;
			$data['data'] = $this->cycle_count_model->getEdit(array('cc_id' => $id));
			$isUsed = $this->cycle_count_model->checkIsUsed($id);

			if($isUsed['is_used'])
				$data['is_used'] = 'true';
			else
				$data['is_used'] = 'false';

			//$data['not_usage'] = $this->outbound_model->cek_not_usage_do($id);

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	public function check_result($id){

		if ($this->access_right->otoritas('view')) {

			// $sts = $this->cycle_count_model->checkStatus($id);
			// if($sts != 0)
				// redirect(base_url() . $this->class_name);

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Stok Opname Check Result';
			$data['page'] = $this->class_name . '/check_result';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/cycle_count/lr-cycle-count-check.js";

			$data['id'] = $id;
			$data['data'] = $this->cycle_count_model->getEdit(array('cc_id' => $id));
			$data['not_usage'] = 'true';

			$isFinish = $this->cycle_count_model->checkIsFinish($id);

			$data['is_finish'] = $isFinish;
			$data['is_close'] = $this->cycle_count_model->checkIsClose($id);

			//$data['not_usage'] = $this->outbound_model->cek_not_usage_do($id);

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	private function include_other_viewdata(&$data){

        /* Get Data Select*/
        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

	}

	//REQUEST
	public function finishCc(){
		$result = array();

		$params = array(
			'cc_id'	=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->cycle_count_model->finishCc($params);

		echo json_encode($result);
	}

	public function adjustment(){
		$result = array();

		$params = array(
			'data'		=> $this->input->post('data'),
			'remark'	=> filter_var(trim($this->input->post('remark')), FILTER_SANITIZE_STRING)
		);

		$result = $this->cycle_count_model->adjustment($params);

		echo json_encode($result);
	}

	public function getDetailCheck(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'cc_type'	=> filter_var(trim($this->input->post('cc_type')), FILTER_SANITIZE_STRING)
		);

		$result = $this->cycle_count_model->getDetailCheck($params);

		echo json_encode($result);
	}

	public function getDetailItem(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'cc_type'	=> filter_var(trim($this->input->post('cc_type')), FILTER_SANITIZE_STRING)
		);

		$result = $this->cycle_count_model->getDetailItem($params);

		echo json_encode($result);
	}

	public function getDetailDataTable(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->cycle_count_model->getDetailDataTable($params);

		echo json_encode($result);
	}

	public function closeCc(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'remark'	=> filter_var(trim($this->input->post('remark')), FILTER_SANITIZE_STRING)
		);

		$result = $this->cycle_count_model->closeCc($params);

		echo json_encode($result);
	}

	public function print_cc_doc(){
		$data = array();

		$params = array(
			'cc_id'	=> $this->input->post('cc_id')
		);

		$cc = $this->cycle_count_model->getEdit($params);

		$data['page'] = $this->class_name . '/cc_report';
        $data['cc'] = $cc;

		$params['cc_type'] = $cc['cc_type'];

		$data['items']['data'] = $this->cycle_count_model->getItems($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "cc_report.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

    public function print_cc_doc2(){
        date_default_timezone_set('Asia/Jakarta');
		$data = array();

		$params = array(
			'cc_id'	=> $this->input->post('cc_id')
		);

		$cc = $this->cycle_count_model->getEdit($params);

		$data['page'] = $this->class_name . '/cc_report2';
        $data['cc'] = $cc;

        $data['scans'] = $this->cycle_count_model->getScans($params);

		$params['cc_type'] = $cc['cc_type'];

		$data['items']['data'] = $this->cycle_count_model->getItems($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "cc_report2.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

	public function delete(){
		$result = array();

		$params = array(
			'cc_id'	=> filter_var(trim($this->input->post('id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->cycle_count_model->delete($params);
		$result['success'] = '1';

		echo json_encode($result);
	}

	public function getEdit(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->cycle_count_model->getEdit($params);
		$result['cc_users'] = $this->cycle_count_model->get_cc_users($params);

		$rows = "";

		if($result['cc_type'] == 'SN'){
			//$this->load_model('outbound_model');

			//$data['items'] = $this->outbound_model->create_options("barang", "id_barang", "nama_barang", "-- Choose item --");

			$data['items'] = $this->cycle_count_model->getChooseItem($params);
			$detail = $this->cycle_count_model->getDetail($params);

			foreach ($detail as $item) {
				$data['item'] = $item;
				$rows .= $this->load->view("cycle_count/add_item_row", $data, true);
			}

		}else{

            $data['areas'] = $this->cycle_count_model->getLocationArea($params);
			$detail = $this->cycle_count_model->getDetail($params);

			foreach ($detail as $location) {
                $params['loc_area_id']=$location['area_id'];
            	$data['loc'] = $this->cycle_count_model->getLocation2($params);
				$data['location'] = $location;
				$rows .= $this->load->view("cycle_count/add_loc_row", $data, true);
			}

		}

		$result['html'] = $rows;

		echo json_encode($result);
	}

	public function getCcCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->cycle_count_model->getCcCode($params);

		echo json_encode($result);
	}

	public function proses() {
       // $this->output->enable_profiler(TRUE);
        $proses_result = array();

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('cc_code', 'Stok Opname code', 'required|trim');
            $this->form_validation->set_rules('tanggal_cc', 'Stok Opname date', 'required|trim');

            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $tanggal = $this->input->post("tanggal_cc");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal)))) . ' ' . date('H:i:s');

                $data = array(
                    'cc_code'	 			=> $this->input->post("cc_code"),
                    'no_person'	 			=> $this->input->post("no_person"),
                    'cc_time'		 		=> $date,
                    'cc_type'			 	=> $this->input->post("type"),
					'user_id'				=> $this->session->userdata('user_id'),
					'warehouse_id'			=> $this->input->post('warehouse'),
					'preorder'				=> $this->input->post('preorder')
             	);

                $detail = array();
                $idBarang = $this->input->post("id_barang");
				$idLocation = $this->input->post("loc_id");
				$idArea = $this->input->post("area_id");
				$idUser = $this->input->post('users');

				if($data['cc_type'] == 'SN'){

					if(!empty($idBarang)){
						if($idBarang){
							foreach ($idBarang as $key => $value) {
								$detail[] = array(
									'item_id' => $value
								);
							}
						}
					}

				}else{

					if(!in_array("0", $idArea)){

						if(!empty($idLocation)){
							if($idLocation){
								foreach ($idLocation as $key => $value) {
									if($value == 0){
										$getLocInArea = $this->cycle_count_model->getLocInArea($idArea[$key]);
										foreach ($getLocInArea->result() as $row) {
											$dtl = array(
												'loc_id' => $row->loc_id,
			                                    'area_id' => $idArea[$key]
											);
											$detail[] = $dtl;
										}
									}else{
		                                $dtl = array(
											'loc_id' => $value,
		                                    'area_id' => $idArea[$key]
										);
										$detail[] = $dtl;
									}
								}
							}
						}

					}else{

						$getLocInWarehouse = $this->cycle_count_model->getLocInWarehouse($data['warehouse_id']);
						foreach ($getLocInWarehouse->result() as $row) {
							$dtl = array(
								'loc_id' => $row->loc_id,
                                'area_id' => $row->loc_area_id
							);
							$detail[] = $dtl;
						}

					}

				}

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if (empty($id)) {

					$data['status'] = 0;

                    if ($this->cycle_count_model->create($data, $detail, $idUser,$data['preorder'])) {
                        $proses_result['add'] = 1;
						$this->cycle_count_model->add_cc_code();

						/* INSERT LOG */
						$this->access_right->activity_logs('add','Stok Opname');
						/* END OF INSERT LOG */
                    }

                } else {

                    if ($this->cycle_count_model->update($data, $detail, $id, $idUser,$data['preorder'])) {
                        $proses_result['edit'] = 1;

						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Stok Opname');
						/* END OF INSERT LOG */
                    }

                }

                $proses_result['success'] = 1;

            } else {

                $proses_result['message'] = validation_errors();

            }

        } else {

            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }

        }

        echo json_encode($proses_result);
    }

	public function getNewCycleCountCode(){
		$result = array();

		$row = $this->cycle_count_model->get_cc_code();

		if($row){

			$result['status'] = 'OK';
			$result['cycle_count_code'] = $row;

		}else{

			$result['status'] = 'OK';
			$result['cycle_count_code'] = $row;

		}

		echo json_encode($result);
	}

	public function get_add_loc_row() {
        //$data['loc'] = $this->cycle_count_model->getLocation();
        $params = array(
        	'warehouse_id' => $this->input->post('warehouse_id')
        );

        $data['areas'] = $this->cycle_count_model->getLocationArea($params);
        echo $this->load->view("cycle_count/add_loc_row", $data, true);
    }

    public function get_loc() {
    	$params['loc_area_id'] = $this->input->post('loc_area_id');
    	$params['warehouse_id'] = $this->input->post('warehouse_id');
        $data['loc'] = $this->cycle_count_model->getLocation2($params);
        echo $this->load->view("cycle_count/loc_option", $data, true);
    }

	public function get_add_item_row() {
		//$this->load_model('inbound_model');
        //$data['items'] = $this->inbound_model->create_options("barang", "id_barang", "kd_barang, nama_barang", "-- Choose item --");

		$data['items'] = $this->cycle_count_model->getChooseItem();
        echo $this->load->view("cycle_count/add_item_row", $data, true);
    }

	public function getList(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'status'	=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_NUMBER_INT),
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'warehouse'		=> $this->input->post('warehouse')
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->cycle_count_model->getList($params);

		echo json_encode($result);
	}

	public function getCheckResult(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'cc_type'	=> filter_var(trim($this->input->post('cc_type')), FILTER_SANITIZE_STRING)
		);

		$result = $this->cycle_count_model->getCheckResultItem($params);

		echo json_encode($result);
	}

	public function getCheckResultDetails(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'cc_type'	=> filter_var(trim($this->input->post('cc_type')), FILTER_SANITIZE_STRING),
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_STRING)
		);

		$result = $this->cycle_count_model->getCheckResultItemDetails($params);

		echo json_encode($result);
	}

	public function print_pdf(){

		$params = array(
			'cc_id'	=> $this->input->post('cc_id'),
			'cc_type' => $this->input->post('cc_type')
		);

		$data['page'] = $this->class_name . '/cc_check_result_report';

		$data['cc'] = $this->cycle_count_model->getEdit($params);
		$data['cc_result'] = $this->cycle_count_model->print_pdf($params);
		$data['actor'] = $this->cycle_count_model->getCcActor($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "cc_check_result_report.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

	}

	public function export_excel(){

		$data = array();

		$params = array(
			'cc_id'	=> $this->input->post('cc_id'),
			'cc_type' => $this->input->post('cc_type')
		);

        $this->load->library('PHPExcel');

		$data = $this->cycle_count_model->getResultSummary($params);

        $objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Cycle Count Report")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->createSheet(0);

		$sheet->setTitle("Summary");

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Item Code')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Item Name')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'System Qty')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Scanned Qty')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Reject')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Discrepancies')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'Status')->getStyle('H1')->getFont()->setBold(true);

        $row = 2;
        $len = count($data);

        if($len > 0){

            for($i = 0; $i < $len; $i++){
                $sheet->setCellValueByColumnAndRow(0, $row, $i+1);
                $sheet->setCellValueByColumnAndRow(1, $row, $data[$i]['item_code']);
                $sheet->setCellValueByColumnAndRow(2, $row, $data[$i]['item_name']);
                $sheet->setCellValueByColumnAndRow(3, $row, $data[$i]['system_qty']);
                $sheet->setCellValueByColumnAndRow(4, $row, $data[$i]['scanned_qty']);
                $sheet->setCellValueByColumnAndRow(5, $row, '');
                $sheet->setCellValueByColumnAndRow(6, $row, ($data[$i]['system_qty'] - $data[$i]['scanned_qty']));
                $sheet->setCellValueByColumnAndRow(7, $row, $data[$i]['status']);
                $row++;
            }

        }

        // Sheet 2
		$sheet2 = $objPHPExcel->createSheet(1);
		$sheet2->setTitle("Details");

		$sheet2->getColumnDimension('A')->setAutoSize(true);
		$sheet2->getColumnDimension('B')->setAutoSize(true);
		$sheet2->getColumnDimension('C')->setAutoSize(true);
		$sheet2->getColumnDimension('D')->setAutoSize(true);
		$sheet2->getColumnDimension('E')->setAutoSize(true);
		$sheet2->getColumnDimension('F')->setAutoSize(true);
		$sheet2->getColumnDimension('G')->setAutoSize(true);
		$sheet2->getColumnDimension('H')->setAutoSize(true);

		$sheet2->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(1, 1, 'Item Code')->getStyle('B1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(2, 1, 'Item Name')->getStyle('C1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(3, 1, 'Serial Number')->getStyle('D1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(4, 1, 'License Plate')->getStyle('E1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(5, 1, 'Location')->getStyle('F1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(6, 1, 'System Qty')->getStyle('G1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(7, 1, 'Scanned Qty')->getStyle('H1')->getFont()->setBold(true);


		$data2 = $this->cycle_count_model->getResultDetails($params);
        $row = 2;
        $len = count($data2);

        if($len > 0){

            for($i = 0; $i < $len; $i++){
                $sheet2->setCellValueByColumnAndRow(0, $row, $i+1);
                $sheet2->setCellValueByColumnAndRow(1, $row, $data2[$i]['item_code']);
                $sheet2->setCellValueByColumnAndRow(2, $row, $data2[$i]['item_name']);
                $sheet2->setCellValueByColumnAndRow(3, $row, $data2[$i]['kd_unik']." ");
                $sheet2->setCellValueByColumnAndRow(4, $row, $data2[$i]['parent_code']);
                $sheet2->setCellValueByColumnAndRow(5, $row, $data2[$i]['loc_name']);
                $sheet2->setCellValueByColumnAndRow(6, $row, $data2[$i]['system_qty']);
                $sheet2->setCellValueByColumnAndRow(7, $row, $data2[$i]['scanned_qty']);
                $row++;
            }

        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Cycle Count Report'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

	}


	public function export(){


		$sql = "
			SELECT
				CAST('0' as INTEGER) as no, itm.code as item_code, itm.name as item_name, ccs.unique_code as serial_number, ccs.parent_code as outer_label, loc.name as location_name,
				COALESCE(cctq.qty,0) as system_qty ,COALESCE(ccs.qty,0) as scanned_qty, COALESCE(wh.name,whs.name) as warehouse_name, cc.code as cycle_count_code, ccs.time as time_scan, cc.finish
			FROM
				cycle_counts cc
			LEFT JOIN cycle_count_scanned ccs
				ON cc.cc_id=ccs.cycle_count_id
			-- LEFT JOIN item_receiving_details ird
			-- 	ON ird.unique_code=ccs.unique_code and ird.
			LEFT JOIN cycle_count_temp_qc cctq
				ON cctq.unique_code=ccs.unique_code and cctq.cycle_count_id=ccs.cycle_count_id
			LEFT JOIN locations loc
				ON loc.id=ccs.location_id
			LEFT JOIN location_areas loc_area
				ON loc_area.id=loc.location_area_id
			LEFT JOIN warehouses wh
				ON wh.id=loc_area.warehouse_id
			LEFT JOIN warehouses whs
				ON whs.id=cc.warehouse_id
			LEFT JOIN items itm
				ON itm.id=ccs.item_id
		";



		$data= $this->db->query($sql)->result_array();

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="Cycle Count Report.csv"');
        $fp = fopen('php://output', 'wb');

        $header = ['No','Item Code','Item Name','Serial Number','Outer Label','Location Name','System Qty','Scanned Qty','Warehouse Name','Cycle Count Document','Time'];
        fputcsv($fp, $header);

        $i=1;
        foreach ( $data as $d ) {
        	$d['no'] = $i;
            fputcsv($fp, $d);
            $i++;
        }

        fclose($fp);


	}

}

<?php

/**
 * Description of referensi_customer
 *
 * @author SANGGRA HANDI
 */
class referensi_customer extends CI_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load->model(array($this->class_name . '_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Master Customer';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = "assets/pages/scripts/lr-ref-customer.js";

		/* Otoritas Tombol tambah */
	 
		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
			);
		}
		 
		/* INSERT LOG */
		$this->access_right->activity_logs('view','Referensi Customer');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);


    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $customer = $this->referensi_customer_model->get_data(array('id_customer' => $id))->row_array();
            $process_result = $customer;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $customer = $this->referensi_customer_model->data($param)->get();
        $iTotalRecords = $this->referensi_customer_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array(); 

        $i=1;
        foreach($customer->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_customer,'delete' => $value->id_customer));
            
            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="'.$value->id_customer.'">',
                $_REQUEST['start']+$i,
                $value->nama_customer,
                $value->alamat_customer,
                $value->no_telp,
                $action
            );
            $i++;
        }
        
        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function add($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('referensi_customer_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->referensi_customer_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;

            

            $this->load->view($this->class_name . '/form2', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
		$this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function proses() {
        $proses_result['success'] = 1;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('nama_customer', 'Customer Name', 'required|trim');
            $this->form_validation->set_rules('alamat_customer', 'Customer Address', 'required|trim');
            $this->form_validation->set_rules('no_telp', 'Customer Contact', 'required|trim');

            if ($this->form_validation->run()) {
                $id = $this->input->post('id');

                $data = array(
                    'nama_customer' => $this->input->post('nama_customer'),
                    'alamat_customer' => $this->input->post('alamat_customer'),
                    'no_telp' => $this->input->post('no_telp')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->referensi_customer_model->create($data)) {
                        $proses_result['add'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Referensi Customer');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->referensi_customer_model->update($data, $id)) {
                        $proses_result['edit'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Referensi Customer');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $processResult = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            $this->referensi_customer_model->delete($id);
            $processResult['success'] = 1;
        } else {
            $processResult['no_delete'] = 1;
        }
        echo json_encode($processResult);
    }
}

/* End of file referensi_customer.php */
/* Location: ./application/controllers/referensi_customer.php */

<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class packing extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

	public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Packing List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

		$data['jsPage'][] = "assets/pages/scripts/packing/lr-packing-view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Packing');
        /* END OF INSERT LOG */

        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	public function getList(){
		$result = array();

        $params = array(
            'warehouse' => $this->input->post('warehouse')
        );

		$result = $this->packing_model->getList($params);

		echo json_encode($result);
	}

    public function getOutboundList(){
		$result = array();

        $param=array('pl_id'=>$this->input->post('pl_id'));

		$result = $this->packing_model->getOutboundList($param);

		echo json_encode($result);
	}

    public function getRecList(){
		$result = array();

        $param=array('pl_id'=>$this->input->post('pl_id'),'id_outbound'=>$this->input->post('id_outbound'));

		$result = $this->packing_model->getRecList($param);

		echo json_encode($result);
	}

    public function get_picking_id(){
        $pl_name = $this->input->post('pl_name');
        $picking = $this->packing_model->get_picking_id($pl_name);
        if($picking->num_rows()>0){
            $pl_id=$picking->row()->pl_id;
        }else{
            $pl_id='no_data';
        }
        echo json_encode(array('pl_id'=>$pl_id));
    }

    public function check_kd_unik(){
        $kd_unik = $this->input->post('kd_unik');
        $id_outbound = $this->input->post('id_outbound');
        $pc_code = $this->input->post('pc_code');
        $inc = $this->input->post('inc');
        $id_picking = $this->packing_model->get_picking($id_outbound);
        $exist = $this->packing_model->get_avail_qty($kd_unik,$id_picking);
        if($exist){
            // $cek_barang = $this->packing_model->get_avail_qty($kd_unik,$id_picking);
            
            // if($cek_barang!=0){

                $checkQty = $this->packing_model->checkQty($id_outbound,$id_picking,$kd_unik);
                if($checkQty){

                    $id_receiving_barang = $this->packing_model->get_receiving_barang($kd_unik);

                    $this->db->trans_start();

                    $this->packing_model->update_picking_recomendation($kd_unik,$id_outbound,$id_picking);
                    $this->packing_model->insert_outbound_receiving($id_outbound,$id_picking,$pc_code,$inc,$kd_unik);

                    // $this->packing_model->update_receiving_barang($kd_unik);
                    // $this->packing_model->update_location_outbound($kd_unik);
                    // $this->packing_model->updatePicking($id_picking);

                    $this->db->trans_complete();

                    $status='exist';

                }else{
                    $status='scanned';
                }
            // }else{
            //     $status='scanned';
            // }
        }else{
            $status='no_data';
        }
        echo json_encode(array('status'=>$status));
    }

    public function get_pc_code(){
        $id_outbound=$this->input->post('id_outbound');
        $pn = $this->packing_model->get_packing_number($id_outbound);
        if($pn->num_rows()>0){
            $pc_code=$pn->row()->packing_number;
            //$pc_string_tmp = $pn->row()->pc_string;
            $inc=$pn->row()->inc;
            $pc_string_tmp = '';
            /*for($i=1;$i<=$inc;$i++){
                if($inc<10){
                    $pad=0;
                }else{
                    $pad='';
                }
                if($i==$inc){
                    $pc_string_tmp = '- <b>'.$pc_code.$pad.$i.'</b><br/> '.$pc_string_tmp;
                }else{
                    $pc_string_tmp = '&nbsp;&nbsp;- '.$pc_code.$pad.$i.'<br/> '.$pc_string_tmp;
                }
            }*/
            //$pc_string = str_replace('x',$inc,$pc_string_tmp);
			
			$pc_string_tmp = $pc_code . str_pad($inc, 4, '0', STR_PAD_LEFT);
        }else{
            $this->packing_model->add_pc_code();
            $pc_code = $this->packing_model->get_pc_code();
            $pc_string_tmp= '<b>'.$pc_code.'0001</b>';
            $inc=1;
        }
        echo json_encode(array('pc_code'=>$pc_code,'inc'=>$inc,'pc_string'=>$pc_string_tmp));
    }

    public function print_surat_jalan($id_outbound){
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('enc');
		$data = array();

		$params = array(
			'id_outbound'	=> $this->enc->decode($id_outbound)
		);

		$data['data'] = $this->packing_model->getOutboundPack($params);

		$data['page'] = $this->class_name . '/surat_jalan';

        $html = $this->load->view($data['page'], $data, true);
        $path = "surat_jalan.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

    public function surat_jalan(){
        $this->load->view('packing/surat_jalan');
    }

    public function print_perintah_keluar_barang($id_outbound){
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('enc');
        $data = array();

        $params = array(
            'id_outbound'   => $this->enc->decode($id_outbound)
        );

        $data['data'] = $this->packing_model->getOutboundPack($params);
        $data['page'] = $this->class_name . '/perintah_keluar_barang';
        $data['qty']     = $this->packing_model->get_qty_print($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "perintah_keluar_barang.pdf";

        ini_set('memory_limit','32M');

        $this->load->library('pdf');
        $pdf = $this->pdf->load();

        $pdf->AddPage('','','','','',5,5,5,5,10,10);
        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->Output($path, 'I'); // save to file because we can
    }

    public function perintah_keluar_barang(){
        $this->load->view('packing/perintah_keluar_barang');
    }

    public function test(){
        //$this->packing_model->update_staging_outbound_by_id_outbound_min();
    }

    public function printZpl(){
        date_default_timezone_set('Asia/Jakarta');
        $id_outbound = $this->input->post('id_outbound');
        $this->load->library('enc');
        $data = array();

        $params = array(
            'id_outbound'   => $this->enc->decode($id_outbound)
        );
        $id_outbound = $this->enc->decode($id_outbound);
        $qty_print = $this->packing_model->get_qty_print($params);
		$result = array();
		$this->load->library('mimoclientprint');
        $this->load_model('receiving_model');

		$qty = $qty_print;
        $label = $this->packing_model->get_data_label($params);

        $delivery_date = date('d/m/Y',strtotime($label->delivery_date));

		$cmd = '';

		for($i = 1; $i <= $qty; $i++) {
            if($i<10){
                $pad=0;
            }else{
                $pad='';
            }
            if($qty<10){
                $pad2=0;
            }else{
                $pad2='';
            }

            $sn = $label->packing_number.$pad.$i.$pad2.$qty;
            $pack = 'Colly '.$i.'/'.$qty;
			//$snInfo = substr($sn, 0, 6) . " " . substr($sn, 6);

			$tpl = file_get_contents(APPPATH . 'views/label/packing_label.tpl');

			// $tpl = str_replace('{outbound_code}', $label->kd_outbound, $tpl);
            $tpl = str_replace('{{barcode}}', $sn, $tpl);
			$tpl = str_replace('{{text}}', $sn, $tpl);
            $tpl = str_replace('{{address}}', $label->DestinationAddress, $tpl);
            $tpl = str_replace('{{pack}}', $pack, $tpl);
            $tpl = str_replace('{{date}}', $delivery_date, $tpl);
            $tpl = str_replace('{{remark}}', $label->note, $tpl);
            $tpl = str_replace('{{ekspedisi}}', $label->ekspedisi, $tpl);
            $tpl = str_replace('{{destination}}', $label->DestinationName, $tpl);
            $tpl = str_replace('{{picking_code}}', $label->picking_code, $tpl);

			$cmd .= $tpl;
		}

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);
        // $this->packing_model->update_staging_outbound_by_id_outbound($id_outbound);
		echo json_encode($result);
	}

    public function check_pc_code(){

        $params = array(
            "pc_code"   => $this->input->post('pc_code'),
            "inc"       => $this->input->post('inc'),
            'id_outbound' => $this->input->post('id_outbound')
        );

        $data = $this->packing_model->check_pc_code($params);

        $result = array();

        if($data['total'] > 0){
            $result['status'] = 'OK';
        }else{
            $result['status'] = 'ERR';
            $result['message'] = 'Please scan item before create new packing';
        }

        echo json_encode($result);
    }
}


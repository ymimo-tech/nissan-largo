<?php

class packing extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load_model('api_packing_model');
        $this->load->library("randomidgenerator");
    }

	public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Packing List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

		$data['jsPage'][] = "assets/pages/scripts/packing/lr-packing-view.js?2";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Packing');
        /* END OF INSERT LOG */

        $this->load->view('template', $data);
    }

	public function edit($pc_code) {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Packing List';
        $data['page'] = $this->class_name . '/index';

		$data['pc_code'] = $pc_code;

		$this->include_required_file($data);

		$data['jsPage'][] = "assets/pages/scripts/packing/lr-packing-view.js?2";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Packing');
        /* END OF INSERT LOG */

        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	public function getPackingCode(){
		$add = $this->input->post('add');
		if($add == 1) {
			$this->packing_model->add_pc_code();
		}

		$pc_code = $this->packing_model->get_pc_code();
		$pc_string_tmp = $pc_code.str_pad(1, 3, '0', STR_PAD_LEFT);
		$inc=1;

        echo json_encode(array('pc_code'=>$pc_code,'inc'=>$inc,'pc_string'=>$pc_string_tmp));
    }

	public function getPackingDetail(){
		$pc_code = $this->input->post('pc_code');

		$get = $this->packing_model->get_pc_detail($pc_code);

        echo json_encode(array('pc_code'=>$pc_code,'weight'=>$get['weight'],'volume'=>$get['volume'],'destination_code'=>$get['destination_code'],'destination_name'=>$get['destination_name'],'destination_address'=>$get['destination_address']));
    }

	public function getRecList(){
		$result = array();

        $param=array(
					'packing_number'=>$this->input->post('packing_number'),
					'delivery_note'=>$this->input->post('delivery_note'),
				);

		$result = $this->packing_model->getRecList($param);

		echo json_encode($result);
	}

	public function getItemQtyByPicking(){
		$result = array();

        $param=array(
					'item_code'		=> $this->input->post('item_code'),
					'picking_list'	=> $this->input->post('picking_list'),
				);

		$result = $this->packing_model->getItemQtyByPicking($param);

		echo json_encode($result);
	}

	public function getItemQtyByDN(){
		$result = array();

        $param=array(
					'item_code'		=> $this->input->post('item_code'),
					'delivery_note'	=> $this->input->post('delivery_note'),
				);

		$result = $this->packing_model->getItemQtyByDN($param);

		echo json_encode($result);
	}

	public function isPCLoaded(){
		$result = array();

        $param=array(
					'packing_number'	=> $this->input->post('packing_number')
				);

		$result = $this->packing_model->isPCLoaded($param);

		echo json_encode($result);
	}

	public function isCarton(){
		$result = array();

        $param=array(
					'carton'	=> $this->input->post('carton')
				);

		$result = $this->packing_model->isCarton($param);

		echo json_encode($result);
	}

	public function savePacking(){
		$result = array();

		$param = array(
					'packing_number'	=> $this->input->post('packing_number'),
					'carton'			=> $this->input->post('carton'),
					// 'picking_list'		=> $this->input->post('picking_list'),
					'delivery_note'		=> $this->input->post('delivery_note'),
					'item_code'			=> $this->input->post('item_code'),
					'destination_code'	=> $this->input->post('destination_code'),
					'quantity'			=> $this->input->post('quantity'),
					'weight'			=> $this->input->post('weight'),
					'volume'			=> $this->input->post('volume')
				);

		if($param['quantity'] > 0) {
			$result = $this->packing_model->savePacking($param);
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Quantity can't 0.",
			);
		}

		echo json_encode($result);
	}
}

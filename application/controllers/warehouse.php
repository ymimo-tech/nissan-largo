<?php

/**
 * Description of warehouse
 *
 * @author HADY PRATAMA
 */
class warehouse extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        /* Otoritas */
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Warehouse Info';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/custom/js/share.js';
        $data['jsPage'][] = "assets/pages/scripts/lr-warehouse.js";

        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

        /* Otoritas Tombol tambah */

        $data['button_group'] = array();
        if ($this->access_right->otoritas('add')) {
            $data['button_group'] = array(
                anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add')))
            );
        }

        /* VIEW LOG */
        $this->access_right->activity_logs('view','Referensi Warehouse');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

    public function export(){
        $this->load->library('PHPExcel');

        $params = array();

        $result = $this->warehouse_model->get_data($params)->result_array();
        $iLen = count($result);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Master-Warehouse")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'Warehouse Name')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, 'Warehouse Description')->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 1, 'Warehouse Description')->getStyle('C1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['name']);
            $sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['description']);
            $sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['address']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Master-Warehouse'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $warehouse = $this->warehouse_model->get_data(array('id' => $id))->row_array();
            $process_result = $warehouse;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){

        $columns = array(
            1 => 'name',
            2 => 'description'
        );

        $param = array();

        $param['a.name IS NOT NULL'] = null;

        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $warehouse = $this->warehouse_model->get_data($param);
        $iTotalRecords = $this->warehouse_model->get_data($param)->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($warehouse->result() as $value){
            //---------awal button action---------
            $action = '';

            $action = '<div class="btn-group">
                        <button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                            &nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= '<a class="data-table-edit" data-id="'.$value->id.'"><i class="fa fa-edit"></i> Edit</a>';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= '<a class="data-table-delete" data-id="'.$value->id.'"><i class="fa fa-trash"></i> Delete</a>';
                $action .= '</li>';
            }

            $action .= '</ul>';

            $records["data"][] = array(
                $_REQUEST['start']+$i,
                $value->name,
                $value->description,
                $action
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function proses() {
        $proses_result['success'] = 1;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('warehouse_name', 'Nama Warehouse', 'required|trim');

            if ($this->form_validation->run()) {
                $id = $this->input->post('id');

                $data = array(
                    'name'  => $this->input->post('warehouse_name'),
                    'description'   => $this->input->post('warehouse_desc'),
                    'address'   => $this->input->post('warehouse_addr'),
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->warehouse_model->create($data)) {
                        $proses_result['add'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Referensi Warehouse');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->warehouse_model->update($data, $id)) {
                        $proses_result['edit'] = 1;
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Referensi Warehouse');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function get(){

        $this->db->select('id,name');
        $results['results'] = $this->warehouse_model->get_data()->result_array();
        echo json_encode($results);

    }

    public function delete() {
        $processResult = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            $processResult = $this->warehouse_model->delete($id);
        } else {
            $processResult['status'] = 'ERR';
            $processResult['message'] = 'You dont have authority to delete data';
        }
        echo json_encode($processResult);
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Warehouse Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        'Warehouse Name'     => 'warehouse_name',
                        'Warehouse Description'  => 'warehouse_desc',
                        'Warehouse Address'         => 'warehouse_addr',
                        'Group'                => 'warehouse_group'
                    ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->warehouse_model->importFromExcel($data);

                        if($process){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage('Please check again your data !');

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }

        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }

    public function getWarehouseByDocument(){

        $results = array();

        $params = array(
            'document_id' => $this->input->post('document_id'),
            'warehouse_id' => $this->input->post('warehouse_id'),
            'warehouse_id_2' => $this->input->post('warehouse_id_2')
        );

        $data = $this->warehouse_model->get_by_document($params);

        if($data->num_rows()){
            $results['data'] = $data->result_array();
            $results['status'] = 'OK';
        }else{
            $results['status'] = 'ERR';
            $results['message'] = 'Warehouse Not Available';
        }

        echo json_encode($results);

    }

}

/* End of file warehouse.php */
/* Location type: ./application/controllers/warehouse.php */

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require "api/Api.php";
class api_integration_temp extends Api {


	/* Static Auth */

	private $username 		= "largodms";
	private $authorization 	= '5m!9-fr99$e$$m#g';

	/* End - Static Auth */

	private $payload = array(
		"params"	=> NULL,
		"response"	=> FALSE,
		"message"	=> "",
		"d"			=> array()
	);

	/* ------------------------------------------------------------------------------------- */

	function __construct() {

		parent::__construct();

		//$this->auth->static_auth($this->username,$this->authorization);

		$this->load_model('api_integration_temp_model');
		$this->load_model('api_setting_model');

	}

	public function test(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		var_dump($auth);
	}

	public function syncProduct(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/syncProduct_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'syncProduct';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncProduct';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['id_item_erp'] = !empty($data[$i]['Id']) ? $data[$i]['Id'] : 0;
					$param['name'] = $data[$i]['Item Name'];
					$param['code'] = $data[$i]['Item Code'];
					$param['oem']	= $data[$i]['OEM'];
					$param['packing_description'] = $data[$i]['Packing Description'];
					$param['has_batch'] = strtolower($data[$i]['Have Batch']) == 'no' ? 0 : 1;
					$param['category_id'] = $data[$i]['Item Category'];
					$param['has_qty']	= strtolower($data[$i]['Multi Qty']) == 'yes' ? 1 : 0;
					$param['def_qty']	= $data[$i]['Default Qty'];
					$param['weight'] = !empty($data[$i]['Weight']) ? $data[$i]['Weight'] : 0;
					$param['width'] = !empty($data[$i]['Width']) ? $data[$i]['Width'] : 0;
					$param['height'] = !empty($data[$i]['Height']) ? $data[$i]['Height'] : 0;
					$param['length'] = !empty($data[$i]['Length']) ? $data[$i]['Length'] : 0;
					$param['unit_id'] = $data[$i]['Unit Of Measurement'];
					$param['has_expdate'] = strtolower($data[$i]['Have Expired Date']) == 'no' ? 0 : 1;
					$param['shipment_id'] = strtolower($data[$i]['Picking Strategy']) == 'fifo' ? 1 : 2;
					$param['fifo_period'] = $data[$i]['Picking Period'];
					$param['minimum_stock'] = !empty($data[$i]['Minimum Replenishment Stock']) ? $data[$i]['Minimum Replenishment Stock'] : 0 ;
					$param['min_reorder'] = !empty($data[$i]['Minimum Reorder Stock']) ? $data[$i]['Minimum Reorder Stock'] : 0;
					$param['warehouse'] = $data[$i]['Warehouse'];
					$param['quarantine'] = strtolower($data[$i]['Quarantine']) == 'yes' ? 1 : 0;
					$param['quarantineduration'] = !empty($data[$i]['Quarantine Duration']) ? $data[$i]['Quarantine Duration'] : 0;
					$param['autorelease'] = strtolower($data[$i]['Autorelease']) == 'yes' ? 1 : 0;
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					// $param['uom_id'] = $data[$i]['uom_id'][0];
					// $param['uom'] = $data[$i]['uom_id'][1];
					// $param['convert_qty_1'] = '';
					// $param['convert_qty_unit_id_1'] = '';

					// if($data[$i]['uom_ids']) {
					// 	//if($data[$i]['uom_ids']['uom_type'] == 'bigger') {
					// 		$param['convert_qty_1'] = $data[$i]['uom_ids'][0]['factor_product'];
					// 		$param['convert_qty_unit_id_1'] = $data[$i]['uom_ids'][0]['name'];
					// 	//}
					// }

					$insert = $this->api_integration_temp_model->insert_product($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncProductSubstitute(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'syncProductSubstitute';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncProductSubstitute';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['item_code'] = $data[$i]['item_code'];
					$param['subs_item_code'] = $data[$i]['subs_item_code'];
					$param['status'] = !empty($data[$i]['status']) ? $data[$i]['status'] : 'ACTIVE';

					$insert = $this->api_integration_temp_model->insert_product_substitute($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncVendor(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/syncVendor_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'syncVendor';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncVendor';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['id_source_erp'] = !empty($data[$i]['Source Id']) ? $data[$i]['Source Id'] : 0;
					$param['source_code'] = $data[$i]['Source Code'];
					$param['source_name'] = $data[$i]['Source Name'];
					$param['source_address'] = $data[$i]['Source Address'];
					$param['source_city'] = $data[$i]['Source City'];
					$param['source_phone'] =  $data[$i]['Source Phone'];
					$param['source_email'] = $data[$i]['Source Email'];
					$param['source_contact_person'] = $data[$i]['Source Contact Person'];
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					$insert = $this->api_integration_temp_model->insert_vendor($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncDocument(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/syncVendor_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'syncVendor';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncUser';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['type'] = $data[$i]['document'];
					$param['name'] = $data[$i]['name'];
					// $param['nama'] = $data[$i]['Full Name'];
					// $param['user_password'] = $data[$i]['Password'];
					// $param['grup_id'] = $data[$i]['Role'];
					// $param['warehouse'] = $data[$i]['Warehouse'];

					$insert = $this->api_integration_temp_model->insert_doc($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}


	public function syncUser(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/syncVendor_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'syncVendor';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncUser';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);
		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					// $param['id_users_erp'] = $data[$i]['User Id'];
					$param['nik'] = $data[$i]['User Id'];
					$param['user_name'] = $data[$i]['User Name'];
					$param['nama'] = $data[$i]['Full Name'];
					$param['user_password'] = $data[$i]['Password'];
					$param['grup_id'] = $data[$i]['Role'];
					$param['warehouse'] = $data[$i]['Warehouse'];
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					$insert = $this->api_integration_temp_model->insert_user($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncCustomer(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$file = '/var/www/html/largo/indofresh/debug/syncCustomer_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'syncCustomer';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncCustomer';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();
		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['id_destination_erp'] = !empty($data[$i]['Destination Id']) ? $data[$i]['Destination Id'] : 0;
					$param['destination_code'] = $data[$i]['Destination Code'];
					$param['destination_name'] = $data[$i]['Destination Name'];
					$param['destination_address'] = $data[$i]['Destination Address'];
					$param['destination_city'] = $data[$i]['Destination City'];
					$param['destination_phone'] =  $data[$i]['Destination Phone'];
					$param['destination_email'] = $data[$i]['Destination Email'];
					$param['destination_contact_person'] = $data[$i]['Destination Contact Person'];
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					$insert = $this->api_integration_temp_model->insert_customer($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncWarehouse(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/syncWarehouse_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'syncWarehouse';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncWarehouse';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params['result'][0];

				for($i = 0; $i < count($data); $i++) {
					$param['id'] = $data[$i]['id'];
					$param['name'] = $data[$i]['name'];

					$insert = $this->api_integration_temp_model->insert_warehouse($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function syncLocation(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/syncLocation_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'syncLocation';
		$log_param['api_url'] = base_url() . get_class($this) . '/syncLocation';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				for($i = 0; $i < count($data); $i++) {
					$param['id_location_erp'] = !empty($data[$i]['Bin Id']) ? $data[$i]['Bin Id'] : 0;
					$param['name'] = $data[$i]['Bin Name'];
					$param['description'] = $data[$i]['Bin Description'];
					$param['location_type_id'] = $data[$i]['Bin Type'];
					$param['location_category_id'] = $data[$i]['Bin Category'];
					$param['location_area_id'] = $data[$i]['Bin Area'];
					$param['rack'] = $data[$i]['Bin Rack'];
					$param['row'] = $data[$i]['Bin Row'];
					$param['level'] = $data[$i]['Bin Level'];
					$param['bin'] = $data[$i]['Bin Bin'];
					$param['color_code_bin'] = $data[$i]['Bin Color Code Bin'];
					$param['color_code_level'] = $data[$i]['Bin Color Code Level'];
					$param['status'] = !empty($data[$i]['Status']) ? $data[$i]['Status'] : 'ACTIVE';

					$insert = $this->api_integration_temp_model->insert_location($param);
				}

				if($insert) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Sync Success."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Failed to Update Data."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function postInboundDoc(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/postInboundDoc_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'postInboundDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/postInboundDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				// $param['id_inbound'] = $data['id'];
				$param['id_inbound_erp'] = $data['id'];
				$param['code'] = $data['number'];
				$param['date'] = $data['date'];
				$param['supplier_id'] = $data['source_code'];
				$param['document_id'] = $data['inbound_type'];
				$param['status_id'] = 3;
				$param['user_id'] = $this->session->userdata('user_id');
				$param['remark'] = $data['remark'];
				$param['warehouse_id'] = $data['warehouse'];

				// $param['no_pallet_original'] = $data['no_container'];

				for($i = 0; $i < count($data['items']); $i++) {
					$item['item_name'] = $data['items'][$i]['item_code'];
					$check_item_exist = $this->api_integration_temp_model->check_product_by_name($item);
					if($check_item_exist->num_rows() == 0) {
						$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Item product code (".$data['items'][$i]['item_code'].") is not exist."
						);
						break;
					}
				}

				if($check_item_exist->num_rows() > 0) {
					$insert_inbound = $this->api_integration_temp_model->insert_inbound($param);
					$inbound_id = $this->db->select('id_inbound')->from('inbound')->where('code', $data['number'])->get()->row_array()['id_inbound'];

					for($i = 0; $i < count($data['items']); $i++) {
						$item_id = $this->db->select('id')->from('items')->where('code', $data['items'][$i]['item_code'])->get()->row_array()['id'];
						$param['item_id'] = $item_id;
						$param['qty'] = $data['items'][$i]['qty'];
						$param['inbound_id'] = $inbound_id;

						$insert_inbound_item = $this->api_integration_temp_model->insert_inbound_item($param);
					}

					if($insert_inbound) {
						$this->payload = array(
									"params"	=> "",
									"response"	=> TRUE,
									"message"	=> "Insert Success."
								);
					} else {
						$this->payload = array(
								"params"	=> "",
								"response"	=> FALSE,
								"message"	=> "Update PO Success."
							);
					}
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function postOutboundDoc($params = array()) {
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/postOutboundDoc_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'postOutboundDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/postOutboundDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				$param['id_outbound_erp'] = $data['id'];
				$param['code'] = $data['number'];
				$param['date'] = $data['date'];
				$param['shipping_group'] = $data['shipping_group'];
				$param['customer_id'] = 0;
				$param['document_id'] = $data['outbound_type'];
				$param['user_id'] = $data['user'];
				$param['note'] = $data['note'];
				$param['destination_id'] = $data['destination_code'];
				$param['id_warehouse'] = $data['warehouse'];
				$param['ekspedisi'] = $data['no_container'];

				for($i = 0; $i < count($data['items']); $i++) {
					$item['item_name'] = $data['items'][$i]['item_code'];
					$check_item_exist = $this->api_integration_temp_model->check_product_by_name($item);
					if($check_item_exist->num_rows() == 0) {
						$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Item product (".$data['items'][$i]['item_code'].") is not exist."
						);
						break;
					}
				}

				if($check_item_exist->num_rows() > 0) {
					$insert_outbound = $this->api_integration_temp_model->insert_outbound($param);
					$outbound_id = $this->db->select('id')->from('outbound')->where('code', $data['number'])->get()->row_array()['id'];

					for($i = 0; $i < count($data['items']); $i++) {
						$item_id = $this->db->select('id')->from('items')->where('code', $data['items'][$i]['item_code'])->get()->row_array()['id'];
						$uom_id = $this->db->select('id')->from('units')->where('code', $data['items'][$i]['uom_id'])->get()->row_array()['id']; 

						$param['item_id'] = $item_id;
						$param['qty'] = $data['items'][$i]['qty'];
						$param['unit_id'] = $uom_id;
						// $param['grade'] = $data['items'][$i]['grade'];
						$param['id_outbound'] = $outbound_id;

						$insert_outbound_item = $this->api_integration_temp_model->insert_outbound_item($param);
					}

					if($insert_outbound) {
						$this->payload = array(
									"params"	=> "",
									"response"	=> TRUE,
									"message"	=> "Sync Success."
								);
					} else {
						$this->payload = array(
								"params"	=> "",
								"response"	=> FALSE,
								"message"	=> "SO already exist."
							);
					}
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function cancelInboundDoc(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/cancelInboundDoc_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'cancelInboundDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/cancelInboundDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				$param['id_staging'] = $data['id'];
				$param['code'] = $data['number'];

				$cancel_inbound = $this->api_integration_temp_model->cancel_inbound($param);

				if($cancel_inbound) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Successfuly cancel inbound document (".$data['number'].")."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Cancel PO Failed."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}

	public function cancelOutboundDoc(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$file = '/var/www/html/largo/indofresh/debug/cancelOutboundDoc_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". file_get_contents('php://input') ."\n\n", FILE_APPEND);
		$log_param['name'] = 'cancelOutboundDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/cancelOutboundDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;

				$param['id_staging'] = $data['id'];
				$param['code'] = $data['number'];

				$cancel_inbound = $this->api_integration_temp_model->cancel_outbound($param);

				if($cancel_inbound) {
					$this->payload = array(
								"params"	=> "",
								"response"	=> TRUE,
								"message"	=> "Successfuly cancel outbound document (".$data['number'].")."
							);
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Cancel PO Failed."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}
	
	public function postASNDoc(){
		$auth = $this->api_setting_model->user_authentication();
		$this->method('POST');

		$params = json_decode(file_get_contents('php://input'),TRUE);
		$param = array();

		$log_param['name'] = 'postASNDoc';
		$log_param['api_url'] = base_url() . get_class($this) . '/postASNDoc';
		$log_param['parameter'] = file_get_contents('php://input');
		$insert_log = $this->api_integration_temp_model->insert_log($log_param);

		if($auth){
			if($params) {
				$data = $params;
				
				// $param['supplier_id'] = $data['COMPANY_CODE']; // SOURCE/VENDOR/SUPPLIER
				$param['id_erp'] = (!empty($data['id'])) ? $data['id'] : 0; // ID ERP
				$param['code'] = $data['BIN_DOC_NO']; // RECEIVING DOC CODE
				$param['date'] = $data['BIN_DOC_DATE']; // RECEIVING DOC DATE
				// $param['inbound_doc'] = $data['PO_DOC_NO']; // INBOUND DOC CODE
				$param['status_id'] = 3;
				$param['user_id'] = (!empty($data['user'])) ? $data['user'] : $this->input->get_request_header('User', true); // USER
				$param['delivery_doc'] = $data['SUPPLIER_CASE_NO']; // CASE NO/DELIVERY DOC/NO CONTAINER
				$param['warehouse_id'] = $data['WHS_CODE']; // WAREHOUSE CODE

				for($i = 0; $i < count($data['ITEMS']); $i++) {
					$item['item_name'] = $data['ITEMS'][$i]['ITEM_CODE']; // ITEM CODE
					$check_item_exist = $this->api_integration_temp_model->check_product_by_name($item);
					if($check_item_exist->num_rows() == 0) {
						$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Item product code (".$item['item_name'].") is not exist."
						);
						break;
					}
				}

				$inbound_detail = $this->db->get_where('inbound',['code'=>$data['PO_DOC_NO']]);
				if($inbound_detail->num_rows() > 0) {
					if($check_item_exist->num_rows() > 0) {
						$insert_receiving = $this->api_integration_temp_model->insert_receiving($param);
						$id_receiving = $this->db->get_where('receivings',['code'=>$param['code']])->row_array()['id'];
						$id_inbound = $inbound_detail->row_array()['id_inbound'];

						for($i = 0; $i < count($data['ITEMS']); $i++) {
							$item_id = $this->db->get_where('items',['code'=>$data['ITEMS'][$i]['ITEM_CODE']])->row_array()['id'];
							$param['item_id'] = $item_id;
							$param['qty'] = $data['ITEMS'][$i]['QTY_DO'];
							$param['id_receiving'] = $id_receiving;
							$param['id_inbound'] = $id_inbound;

							$insert_asn_item = $this->api_integration_temp_model->insert_asn_item($param);
						}

						if($insert_receiving) {
							$this->payload = array(
										"params"	=> "",
										"response"	=> TRUE,
										"message"	=> "Insert ASN Success."
									);
						} else {
							$this->payload = array(
									"params"	=> "",
									"response"	=> FALSE,
									"message"	=> "Update ASN Success."
								);
						}
					}
				} else {
					$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "Inbound Document (".$param['inbound_doc'].") is not exist. Please insert inbound document first."
						);
				}
			}
		} else {
			$this->payload = array(
							"params"	=> "",
							"response"	=> FALSE,
							"message"	=> "User not authorized."
						);
		}

		$log_param['id'] = $insert_log;
		$log_param['response'] = json_encode($this->payload);
		$this->api_integration_temp_model->update_log($log_param);
		echo json_encode($this->payload);
	}
}

<?php 

class Node3_master extends CI_Controller { 
	
	public $interval;
	
	public function __construct() { 
		parent::__construct(); 
		
		$this->interval = 30; //IN SECOND
		$this->load->model('node3_model');
	}
	
	public function index(){
		if(!$this->input->is_cli_request()){
			echo "This script can only be accessed via the command line" . PHP_EOL;
			return;
		}
		
		while(1){
		
			echo "\n\n::::::::::Start : " . date('d M Y H:i:s') . "::::::::::\n\n";
			
			//WAREHOUSE
			echo "Warehouse Sync is in progress... \n";
			$result = $this->node3_model->syncMasterWarehouse();
			$this->response($result);
			
			//CATEGORY
			echo "Category Sync is in progress... \n";
			$result = $this->node3_model->syncMasterCategory();
			$this->response($result);
			
			//CATEGORY2
			echo "Category2 Sync is in progress... \n";
			$result = $this->node3_model->syncMasterCategory2();
			$this->response($result);
			
			//UOM
			echo "UOM Sync is in progress... \n";
			$result = $this->node3_model->syncMasterUom();
			$this->response($result);
			
			//ITEM
			echo "Item Sync is in progress... \n";
			$result = $this->node3_model->syncMasterItem();
			$this->response($result);
			
			//BIN LOCATION
			echo "Bin Location Sync is in progress... \n";
			$result = $this->node3_model->syncMasterBin();
			$this->response($result);
			
			//SOURCE
			echo "Source Sync is in progress... \n";
			$result = $this->node3_model->syncMasterSource();
			$this->response($result);
			
			//DESTINATION
			echo "Destination Sync is in progress... \n";
			$result = $this->node3_model->syncMasterDestination();
			$this->response($result);
			
			echo "::::::::::End   : " . date('d M Y H:i:s') . "::::::::::\n\n";
			
			sleep($this->interval);
		
		}
	}
	
	public function response($result = array()){
		if(isset($result['message'])){
			
			echo "Message : " . $result['message'] . "\n\n";
		
		}else{
			
			print_r($result);
			
		}
	}
}
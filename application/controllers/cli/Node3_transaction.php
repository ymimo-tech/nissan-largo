<?php 

class Node3_transaction extends CI_Controller { 
	
	public $interval;
	
	public function __construct() { 
		parent::__construct(); 
		
		$this->interval = 30; //IN SECOND
		$this->load->model('node3_model');
	}
	
	public function index(){
		if(!$this->input->is_cli_request()){
			echo "This script can only be accessed via the command line" . PHP_EOL;
			return;
		}
		
		while(1){
		
			echo "\n\n::::::::::Start : " . date('d M Y H:i:s') . "::::::::::\n\n";
			
			//INBOUND
			echo "Inbound Sync is in progress... \n";
			$result = $this->node3_model->syncInbound();
			$this->response($result);
			
			//OUTBOUND
			echo "Outbound Sync is in progress... \n";
			$result = $this->node3_model->syncOutbound();
			$this->response($result);
			
			echo "::::::::::End   : " . date('d M Y H:i:s') . "::::::::::\n\n";
			
			sleep($this->interval);
		
		}
	}
	
	public function response($result = array()){
		if(isset($result['message'])){
			
			echo "Message : " . $result['message'] . "\n\n";
		
		}else{
			
			print_r($result);
			
		}
	}
}
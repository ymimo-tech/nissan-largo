<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class receiving extends MY_Controller {

    private $class_name;
    private $limit = 50;
    private $offset = 0;

	/* ===== PID PRINTER COMMAND WCPP =====
	0 => Use Default Printer
	1 => Display a Printer dialog
	2 => Use an installed Printer
	3 => Use an IP/Etherner Printer
	4 => Use a LPT port
	5 => Use a RS232 (COM) port
	============= END PID ===============*/

	CONST PID = 1;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		//$this->access_right->check();
		//$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
		$this->load_model('inbound_model');
    }


    public function index() {

		//$this->load->library('wcpp'); //LOAD WEBCLIENTPRINT

        $data['title'] = '<i class="icon-plus-sign-alt"></i> Receiving List';
        $data['page'] = $this->class_name . '/index';

		$this->load->helper('diff_helper');

		$this->include_required_file($data);

		//$data['jsPage'][] = $this->wcpp->generateScript();

		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
        $data['jsPage'][] = "assets/pages/scripts/receiving/lr-receiving-view.js";

		/* Otoritas Tombol tambah */
		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add'))).'',
				//anchor(null, '<i class="fa fa-arrow-right"></i> Receiving', array('id' => 'button-receiv', 'class' => 'btn green btn-sm', 'data-source' => base_url($this->class_name . '/process/')))
			);
		}

        //=========tombol cetak excel==========
        //$base_url_images = base_url() . 'images';
        //$base_url = base_url() . $this->class_name;

        if ($this->access_right->otoritas('print')) {

            $data['print_group'] = array(
				anchor(null, '<i class="fa fa-print"></i> PRINT OUTER LABEL', array('id' => 'button-print-qty-barcode-lp', 'class' => 'btn default btn-sm','data-value'=>'license-plate')),
				anchor(null, '<i class="fa fa-print"></i> PRINT LABEL', array('id' => 'button-print-qty-barcode-sn', 'class' => 'btn default btn-sm', 'data-value'=>'serial-number', 'style'=>'margin-left:5px'))
                //anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $base_url . '/excel'))
            );
		}

		if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import','components/import/form-import-inbound-stock'];

            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
            // $data['jsPage'][] = "assets/pages/scripts/import/lr-import-inbound-stock.js";
        }
        //-------------------------------------
        //option
        //$this->load_model('referensi_supplier_model');
        //$this->load_model('referensi_purchase_order_model');

		$this->include_other_view_data_filter($data);
		$data['today'] = date('d/m/Y');
		$data['widget'] = $this->receiving_model->getWidget();
		$data['status'] = $this->receiving_model->getStatus();
		$data['items'] = $this->db->get('items')->result_array();

		/* Begin Andri */
		// var_dump(secsToTime($data['widget']['average']));
		$data['widget']['average'] = secsToTime($data['widget']['average']);
		/* End Andri */

		//PRINT ZPL
		$data['pid'] = self::PID;

        //$data['options_bbm'] = $this->receiving_model->options();
        //$data['options_supplier'] = $this->referensi_supplier_model->options();
        //$data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
        //$data['tahun_aktif'] = hgenerator::list_year(); //----Tahun Aktif------------

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Bukti Barang Masuk');
        /* END OF INSERT LOG */

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
	}

	public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Receiving Master Import Template.xlsx');

            echo json_encode($data);
        }
	}

	public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

	    	$this->load->library('import');

	        $path = 'uploads/excel/'. $this->class_name .'/';
	        $upload = $this->import->upload($path);

	        if($upload['status']){

	            $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

	            if(!$arr){

	                $result = $this->import->errorMessage('Wrong Template');

	            }else{

			        $field = [
						'RCV Doc'	=> 'rcv_doc',
			            'Inbound Doc'=> 'inbound_doc',
			            'RCV Date' => 'rcv_date',
			            'Transpotter' => 'vehicle_plate',
			            'Driver Name' => 'dock',
			            'Delivery Doc' => 'delivery_doc',
			            'Cross Doc' => 'is_cross_doc',
			        ];
					$data = $this->import->toFieldTable($field, $arr);

	                if(!$data){

	                    $result = $this->import->errorMessage('Wrong Template');

	                }else{

	                	$process = $this->receiving_model->importFromExcel($data);

	                	if($process){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

	                	}else{

			                $result = $this->import->errorMessage($upload['data']);

	                	}

	                }

	            }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }

        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

   }

	//ADD
	public function add($id_inbound=''){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> New Receiving';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/receiving/lr-receiving-addedit.js";


			$this->include_other_view_data($data);

			$this->load_model('referensi_supplier_model');

			$data['id'] = '';
			$data['id_inbound'] = $id_inbound;
      // $data['rcv_code'] = 'RCV -'.date('ymd');
			$data['rcv_code'] = 'RCV-'.date('ymd').str_pad($this->receiving_model->get_rcv_code(), 4, 0, STR_PAD_LEFT);
			$data['inbound'] = $this->receiving_model->getInboundList();
			$data['today'] = date('d/m/Y');

			/* INSERT LOG */
			$this->access_right->activity_logs('add','Receiving');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//EDIT
	public function edit($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Receiving';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/receiving/lr-receiving-addedit.js";

			$this->include_other_view_data($data);

			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = $id;
			$data['id_inbound'] = '';
			$data['rcv_code'] = '';
			$data['inbound'] = $this->receiving_model->getInboundList();
			$data['today'] = date('d/m/Y');

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//DETAIL
	public function detail($id){
		ini_set('max_execution_time', 300);
		ini_set('memory_limit','128M');

		if ($this->access_right->otoritas('view')) {

			//$this->load->library('wcpp'); //LOAD WEBCLIENTPRINT

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Receiving Document';
			$data['page'] = $this->class_name . '/detail';

			$this->include_required_file($data);

			//$data['jsPage'][] = $this->wcpp->generateScript();
			$data['jsPage'][] = "assets/pages/scripts/receiving/lr-receiving-detail.js";

			$this->include_other_view_data($data);

			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = $id;
			$data['data'] = $this->getDetail($id);

			$data['pid'] = self::PID;

			/* INSERT LOG */
			$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//PROCESS RECEIVING
	public function process(){

		if ($this->access_right->otoritas('view')) {

			//$this->load->library('wcpp'); //LOAD WEBCLIENTPRINT

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Receiving';
			$data['page'] = $this->class_name . '/process';

			$this->include_required_file($data);

			//$data['jsPage'][] = $this->wcpp->generateScript();
			$data['jsPage'][] = "assets/pages/scripts/receiving/lr-receiving-process.js";

			$this->include_other_view_data($data);

			$params = array(
				'id_receiving'	=> $this->input->post('id_receiving')
			);

			$data['rcv_id'] = array();
			$data['pid'] = self::PID;

			if(!empty($params['id_receiving']))
				$data['rcv_id'] = $this->input->post('id_receiving');

			/* INSERT LOG */
			$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";

        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	private function include_other_view_data_filter(&$data) {
		$this->load_model('referensi_supplier_model');
        $data['suppliers'] = $this->referensi_supplier_model->options();

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues
    }

	private function include_other_view_data(&$data) {
		$this->load_model('referensi_supplier_model');
        $data['suppliers'] = $this->referensi_supplier_model->options();

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues
    }

	public function finishTally(){
		$result = array();

		$params = array(
			'id_receiving'	=> $this->input->post('id_receiving'),
			'remark'		=> filter_var($this->input->post('remark'), FILTER_SANITIZE_STRING)
		);

		$this->db->trans_start();

		$result = $this->receiving_model->finishTally($params);

		// Send to Staging
		if($result['status'] == 'OK'){
			$this->receiving_model->sendToStaging($params['id_receiving']);
		}

		$this->db->trans_complete();

		echo json_encode($result);
	}

	public function getInboundDetail(){
		$result = array();

		$params = array(
			'id_inbound' => filter_var($this->input->post('id_inbound'), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->receiving_model->getInboundDetail($params);

		echo json_encode($result);
	}

	public function print_tally(){
		$data = array();

		$params = array(
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		if(!empty($params['id_receiving'])){

			$rcv = $this->receiving_model->printTallyInformation(array('id_receiving' => $params['id_receiving']))->row_array();
	        $rcv['tanggal_receiving'] = DateTime::createFromFormat('Y-m-d H:i:s', $rcv['tanggal_receiving'])->format('d/m/Y');

			$data['page'] = $this->class_name . '/tally_report';
	        $data['receiving'] = $rcv;
			$data['items'] = $this->receiving_model->getDetailItemReceiving($params);

	        $html = $this->load->view($data['page'], $data, true);
	        $path = "tally-report.pdf";

	        ini_set('memory_limit','128M'); // boost the memory limit if it's low ;)

			$this->load->library('pdf');
			$pdf = $this->pdf->load();

			$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

			//START TALLY
			//$this->receiving_model->startTally($params);

			$pdf->WriteHTML($html); // write the HTML into the PDF
			$pdf->Output($path, 'I'); // save to file because we can
		}

    }

    public function print_grn(){

		$data = array();

		$params = array(
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		if(!empty($params['id_receiving'])){

			$data['page'] = $this->class_name . '/grn_report';
			$data['inbound'] = $this->receiving_model->inboundReceiving($params['id_receiving']);
			$data['receivings'] = $this->receiving_model->receivedItem($params['id_receiving']);
			$data['receivingItem'] = $this->receiving_model->receivedQCItem($params['id_receiving']);

	        $html = $this->load->view($data['page'], $data, true);
	        $path = "Good Receipt Note.pdf";

	        ini_set('memory_limit','128M'); // boost the memory limit if it's low ;)

			$this->load->library('pdf');
			$pdf = $this->pdf->load();

			$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

			$pdf->WriteHTML($html); // write the HTML into the PDF
			$pdf->Output($path, 'I'); // save to file because we can

		}

    }

	public function saveReceiving(){
		$result = array();
		$id = $this->input->post('id_receiving');
		$data = $this->input->post('barang');
		$len = count($data);

		for($i = 0; $i < $len; $i++){

			$idBarang = $data[$i]['id_barang'];
			$qty = $data[$i]['qty'];

			$this->receiving_model->update_rcv_qty($id,$idBarang,$qty);
		}

		$result['status'] = 'OK';
		$result['message'] = 'Update receiving qty success';

		echo json_encode($result);
	}

	public function getRcvCode(){
		$result = array();

		$params = array(
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		$result = $this->receiving_model->getRcvCode($params);

		echo json_encode($result);
	}

	public function getEdit() {
        $process_result = array();

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $id = $this->input->post("id");

            $rcv = $this->receiving_model->get_edit(array('id_receiving' => $id))->row_array();
            $rcv['id_inbound'] = $this->receiving_model->get_inbound_document($id);
            $rcv['tanggal_receiving'] = DateTime::createFromFormat('Y-m-d H:i:s', $rcv['tanggal_receiving'])->format('d/m/Y');

            $process_result = $rcv;

        } else {

            $process_result['no_data'] = 1;
        }

        echo json_encode($process_result);
    }

	public function getDetailItemInboundReceiving(){
		ini_set('max_execution_time', 300);
		ini_set('memory_limit','128M');
		$result = array();

		$params = array(
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		$result = $this->receiving_model->getDetailItemInboundReceiving($params);

		echo json_encode($result);
	}

	public function getReceivedList(){
		$result = array();
		$this->load_model('search_model');

		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		$result = $this->search_model->getReceivedList($params);

		echo json_encode($result);
	}

	public function getDetailItemReceiving(){
		$result = array();

		$params = array(
			'id_inbound'	=> $this->input->post('id_inbound'),
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		$result = $this->receiving_model->getDetailItemReceiving($params);

		echo json_encode($result);
	}

	public function getDetailItemDataTable(){
		$result = array();

		$params = array(
			'id_inbound'	=> $this->input->post('id_inbound'),
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		$result = $this->receiving_model->getDetailItemDataTable($params);

		echo json_encode($result);
	}

	public function getDetailItem(){
		$result = array();

		$params = array(
			'id_inbound'	=> $this->input->post('id_inbound'),
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		$result = $this->receiving_model->getDetailItem($params);

		echo json_encode($result);
	}

	public function getDetail($id = false){
		$result = array();

		$params = array(
			'id_receiving'	=> $id
		);

		$result = $this->receiving_model->getDetail($params);
		//result['tanggal_receiving'] = date('d/m/Y', strtotime($result['tanggal_receiving']));
		//$result['tanggal_receiving'] = DateTime::createFromFormat('Y-m-d', $result['tanggal_receiving'])->format('d/m/Y');

		return $result;
	}

	public function getReceivingCode(){
		$result = array();

		$params = array(
			'query' 	=> filter_var(trim($this->input->post('query')), FILTER_SANITIZE_STRING)
		);

		$result = $this->receiving_model->getReceivingCode($params);

		echo json_encode($result);
	}

	public function getInboundCode(){
		$result = array();

		$params = array(
			'query' 	=> filter_var(trim($this->input->post('query')), FILTER_SANITIZE_STRING),
			'params' 	=> $this->input->post('params')
		);

		$result = $this->receiving_model->getInboundCode($params);

		echo json_encode($result);
	}

	public function getList(){

		$result = array();

		$params = array(
			'id_receiving'	 	=> filter_var(trim($this->input->post('id_receiving')), FILTER_SANITIZE_STRING),
			'id_po' 			=> filter_var(trim($this->input->post('id_po')), FILTER_SANITIZE_STRING),
			'id_supplier' 		=> filter_var(trim($this->input->post('id_supplier')), FILTER_SANITIZE_NUMBER_INT),
			'from' 				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to' 				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'params'			=> filter_var(trim($this->input->post('params')), FILTER_SANITIZE_STRING),
			'className'			=> $this->class_name,
			'status'			=> $this->input->post('status'),
			'warehouse'			=> $this->input->post('warehouse'),
			'has_preorder'		=> $this->input->post('has_preorder')
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->receiving_model->getList($params);

		echo json_encode($result);
	}

	public function getNewRcvCode(){
		$result = array();
		$row = $this->receiving_model->get_rcv_code();

		if($row){

			$result['status'] = 'OK';
			$result['rcv_code'] = $row;

		}else{

			$result['status'] = 'OK';
			$result['rcv_code'] = $row;

		}

		echo json_encode($result);
	}

	//SAVE
	public function proses() {
		$proses_result = array();

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
			$proses_result['success'] = 1;

            $this->form_validation->set_rules('kd_receiving', 'Receiving Code', 'required|trim');
            $this->form_validation->set_rules('rcv_date', 'Receiving Date', 'required|trim');
            $this->form_validation->set_rules('doc', 'Delivery DOkument', 'required|trim');

            if ($this->form_validation->run()) {
                //$message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

				//DateTime::createFromFormat('d/m/Y', $this->input->post('rcv_date'))->format('Y-m-d') . ' ' . date('H:i:s');

				$date = date('Y-m-d', strtotime(str_replace('/','-',trim($this->input->post('rcv_date')))))	. ' ' .	date('H:i:s');

                $data = array(
                    'code' 		=> filter_var(trim($this->input->post('kd_receiving')), FILTER_SANITIZE_STRING),
                    //'tanggal_receiving' => $date,
                    // 'po_id' 			=> $this->input->post('inbound'),
                    'vehicle_plate'		=> filter_var(trim($this->input->post('plate')), FILTER_SANITIZE_STRING),
                    'dock' 				=> filter_var(trim($this->input->post('driver')), FILTER_SANITIZE_STRING),
                    'delivery_doc' 		=> filter_var(trim($this->input->post('doc')), FILTER_SANITIZE_STRING),
					'user_id'			=> $this->session->userdata('user_id'),
					'is_cross_doc'		=> $this->input->post('crossdoc')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

				$brg = $this->input->post('barang');

				// $this->test1($idInbound,$brg);
				// exit;

				/*
					Module : Validasi Inbound Quantity
					Creator : Hady Pratama
				*/

                $inboundIds = $this->input->post('inbound');

                $checkInboundSource = $this->receiving_model->checkInboundSource($inboundIds);
                if($checkInboundSource){
	            	$validQty = $this->receiving_model->validQty($inboundIds,$brg,$id);
	            	if(!$validQty['response']){

	                	$proses_result = $validQty;

	                }else{

	                /* End - Validasi Inbound Quantity */

						$this->db->trans_start();

		                if (empty($id)) {

								$data['date'] = $date;
								$create = $this->receiving_model->create($data);

			                    if ($create > 0) {

									$id = $create;
			                        $this->receiving_model->add_rcv_code();
									$proses_result['add'] = 1;

									// $brg = $this->input->post('barang');

									if(!empty($brg)){
										$data = $this->input->post('barang');
										$len = count($data);

										for($i = 0; $i < $len; $i++){

											$idBarang = $data[$i]['id_barang'];
											$qty = $data[$i]['qty'];
											$idInbound = $data[$i]['inbound_id'];

											$this->receiving_model->update_rcv_qty($id,$idBarang,$qty,$idInbound);
										}
									}

									$this->receiving_model->updateInboundStatus($this->input->post('inbound'));

			                        //$message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
									/* INSERT LOG */
									$this->access_right->activity_logs('add','Tambah BBM');
									/* END OF INSERT LOG */
								}
		                } else {
		                    if ($this->receiving_model->update($data, $id)) {
								$proses_result['edit'] = 1;


								//RECEIVING BARANG

								// $brg = $this->input->post('barang');

								if(!empty($brg)){
									$data = $this->input->post('barang');
									$len = count($data);

									for($i = 0; $i < $len; $i++){

										$idBarang = $data[$i]['id_barang'];
										$qty = $data[$i]['qty'];
										$idInbound = $data[$i]['inbound_id'];

										$this->receiving_model->update_rcv_qty($id,$idBarang,$qty,$idInbound);
									}
								}

		                        //$message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');

								/* INSERT LOG */
								$this->access_right->activity_logs('edit','Edit BBM');
								/* END OF INSERT LOG */
		                    }
		                }

		                /*
		                	Module 		: Add or Update Receiving Table relation with Inbound Table
		                	Creator 	: Hady Pratama
						*/
		                $this->receiving_model->inbound_receiving_relation($inboundIds,$id);

		                $this->db->trans_complete();

		                /* End - Add or Update Receiving Table relation with Inbound Table */

		            }
		        }else{

		        	$proses_result['success'] = 0;
	                $proses_result['message'] = "Inbound document harus berasal dari sumber yang sama.";

		        }

            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }

		echo json_encode($proses_result);
    }

	public function delete() {
        $result = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");

            $result = $this->receiving_model->delete($id);
            $result['success'] = 1;
        } else {
            $result['no_delete'] = 1;
        }
        echo json_encode($result);
    }

	public function printZpl(){
		$result = array();
		$this->load->library('mimoclientprint');

		$type = $this->input->post('type_print');
		$qty = $this->input->post('qty_print');
		$item = $this->input->post('item');
		$cmd = '';

		for($i = 1; $i <= $qty; $i++) {

			if($type == 'serial-number'){

				$sn = "IL".$this->receiving_model->get_next_barcode($item);

				/* SN convert to Hex*/
				// $sn = bin2hex($sn);

				$tpl = file_get_contents(APPPATH . 'views/label/label.tpl');

				$tpl = str_replace('{{text}}', $sn, $tpl);
				$tpl = str_replace('{{barcode}}', $sn, $tpl);
				$tpl = str_replace('{{date}}', date('d/m/Y'), $tpl);

				$cmd .= $tpl;

			}else{

				$wh = $this->input->post('warehouse');
				$ol = "OL".$this->receiving_model->get_next_barcode_lp($wh);

				$tpl = file_get_contents(APPPATH . 'views/label/outerlabel.tpl');

				$tpl = str_replace('{{barcode}}', $ol, $tpl);
				$tpl = str_replace('{{outerlabel}}', $ol, $tpl);
				$tpl = str_replace('{{date}}', date('d/m/Y'), $tpl);

				$cmd .= $tpl;

			}

		}

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);

		echo json_encode($result);
	}

	public function printZpl1(){
		$result = array();
		//$this->load->library('wcpp');

		$qty = $this->input->post('qty_print');
		$cmd = '';

		for($i = 1; $i <= $qty; $i++) {
			$sn = $this->receiving_model->get_next_barcode();
			/*
			$cmd .= "^XA";

			if($i < $qty)
				$cmd .= "^XB";

			$cmd .= "
				^MMA
				^PW264
				^LL0144
				^LS0
				^BY2,3,73^FT41,92^BCN,,Y,N
				^FD>;".$sn."^FS
				^PQ1,0,1,Y
				~JSB
				^XZ";
			*/
			$snInfo = substr($sn, 0, 6) . " " . substr($sn, 6);
			$cmd .= '^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR4,4~SD15^JUS^LRN^CI0^XZ
					^XA
					^MMT
					^PW320
					^LL0240
					^LS0
					^FO0,0^GFA,03456,03456,00036,:Z64:eJztlU2SozAMhWVXsmGFq5pLcAovOEB8I9cs5xQsXT7l6McCt9XQq9mhSiylIV8/XmQZ4IknrmPKnHylutZaJO+Y1lrlnleUtPGCgVkSfYrfOVk4SPCSlArg5FaXcAkJI2KN8QFaU8+ZOg6nArTW/E1PbHqipq2pGzj4Xgs+T/a14IsquviWxwsLLvR+zxBI35fTv5ycv7vUHoVQqp6ead17PZRfVG3AIja+IFcbZ1UOVGB360QIv3f+JPcBFgIfqsBp6jl/SqtXy2l6tp/1QK+ndpxi9DR/3Nz0cIWJP0itHH/DUT2k4l4PtuHAAeGU0x/0Qf1J4gmEOPhDv9I1R/7ty+gBqwd7ZeTkgyP9I13D3TKLJ2D8IU7W391wmp5IYu714EMop1hO8yeSH9zInyt/kEMvrOsPnFPPubF4x1s9mTmyTwvv+F6PmoFWBAoqHBeDP/hQvnGy5TQ98Lse4pRrPXxnYitIQmr+JONP5a9QPRXt51EPT8Ff+rlyS7e50e+Ls3/cF78XdkQ8WYw/hVl9/9j9JbPvVg9P5GsO++NkFnfzB9Pgj3DyrZ7XlZ5uHk48Q5Vj5+FhQxA9bT6LTdJdwtlHji+MmDo9sqieSAqNHv6Ocmi4T4VPRjkv2BrulRDFHzy4osM2wiMsJOFUaT6CKcecX5v27qHHnl/H7QentTXj+J53WIL0ynuW+UO9s8xyZR44vjSOOd+feOKJ/xP/AEHaE+w=:3407
					^BY3,3,107^FT9,188^BCN,,N,N
					^FD>;'.$sn.'^FS
					^FT42,225^A0N,39,38^FH\^FD'.$snInfo.'^FS
					^PQ1,0,1,Y^XZ';
		}

		/*
		$params = array(
			'sid'				=> $this->input->post('sid'),
			'pid'				=> $this->input->post('pid'),
			'printerCommands'	=> $cmd
		);
		*/
		//$this->wcpp->printIt($params);

		$result['zpl'] = '<textarea id="cmd">'.$cmd.'</textarea>';
		echo json_encode($result);
	}

	public function getQtyPrint() {
		$result = array();

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
			$params = array(
				'id_receiving'	=> filter_var(trim($this->input->post('id_receiving')), FILTER_SANITIZE_NUMBER_INT)
			);
            $qty = $this->receiving_model->get_qty_print($params['id_receiving']);

			$result['status'] = 'OK';
			$result['message'] = '';
			$result['qty'] = $qty;

        } else {
            //$this->access_right->redirect();

			$result['status'] = 'ERR';
			$result['message'] = 'Failed to get qty print';
			$result['qty'] = 0;
        }

		echo json_encode($result);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "Receiving Date",1,1,
                            "Receiving Code", 1, 1,
                            "PO Code", 1, 1,
                            "Supplier",1,1,
                            "Vehicle Plate",1,1,
                            'Remark',1,1,
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "Receiving Date",1,1,
                            "Receiving Code", 1, 1,
                            "PO Code", 1, 1,
                            "Supplier",1,1,
                            "Vehicle Plate",1,1,
                            'Remark',1,1,
                            "Action",1,1
                            );
        }

        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_receiving' => 'center', 'kd_receiving' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover table-condensed";
        $table->model = "receiving_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate($table);

        echo json_encode($data);
    }

    public function get_list(){
        $this->session->set_userdata('current_filter',$this->input->post('periode'));
        // if(isset($_REQUEST['customActionName']) && isset($_REQUEST['customActionType'])){
            // $ids=$_REQUEST['ids'];
            // if($_REQUEST['customActionName']=='put'){
                // foreach($ids as $id){
                    // $id_pallet=$id;
                    // $this->_put_pallet($id_pallet);
                // }
            // }
        // }

        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $receiving = $this->receiving_model->data($param)->get();
        $iTotalRecords = $this->receiving_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $i=1;
        $nama_cabang='';
        foreach($receiving->result() as $receiving){
            //---------awal button action---------
            $action='';
            if (($this->access_right->otoritas('view') OR $this->access_right->otoritas('edit') OR $this->access_right->otoritas('delete'))) {

            }

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="'.$receiving->id_receiving.'">',
                $_REQUEST['start']+$i,
                $receiving->kd_receiving,
                $receiving->kd_po,
                $receiving->nama_supplier,
                $receiving->vehicle_plate,
            );
            $i++;
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customGroupAction"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customGroupActionMessage"] = "Put process has been completed!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        $records["current_filter"] = $this->session->userdata('current_filter');

        echo json_encode($records);
	}

	public function export_excel(){
		$data = array();

		$params = array(
			'id_receiving'	=> $this->input->post('id_receiving')
		);

        $this->load->library('PHPExcel');
		$data = $this->receiving_model->getDetailItemInboundReceivingExcel($params);

        $objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Tally Report")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->createSheet(0);

		$sheet->setTitle("Summary");

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Item Code')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Item Name')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Ordered')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Doc. Qty')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Received')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Putaway')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'Discrepancies')->getStyle('H1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(8, 1, 'UoM')->getStyle('I1')->getFont()->setBold(true);

        $row = 2;
        $len = count($data);

        if($len > 0){

            for($i = 0; $i < $len; $i++){
                $sheet->setCellValueByColumnAndRow(0, $row, $data[$i][0]);
                $sheet->setCellValueByColumnAndRow(1, $row, $data[$i][1]);
                $sheet->setCellValueByColumnAndRow(2, $row, $data[$i][2]);
                $sheet->setCellValueByColumnAndRow(3, $row, $data[$i][3]);
                $sheet->setCellValueByColumnAndRow(4, $row, $data[$i][4]);
                $sheet->setCellValueByColumnAndRow(5, $row, $data[$i][5]);
                $sheet->setCellValueByColumnAndRow(6, $row, $data[$i][6]);
                $sheet->setCellValueByColumnAndRow(7, $row, $data[$i][7]);
                $sheet->setCellValueByColumnAndRow(8, $row, $data[$i][8]);
                $row++;
            }

        }

        // Sheet 2
		$sheet2 = $objPHPExcel->createSheet(1);
		$sheet2->setTitle("Details");

		$sheet2->getColumnDimension('A')->setAutoSize(true);
		$sheet2->getColumnDimension('B')->setAutoSize(true);
		$sheet2->getColumnDimension('C')->setAutoSize(true);
		$sheet2->getColumnDimension('D')->setAutoSize(true);
		$sheet2->getColumnDimension('E')->setAutoSize(true);
		$sheet2->getColumnDimension('F')->setAutoSize(true);
		$sheet2->getColumnDimension('G')->setAutoSize(true);
		$sheet2->getColumnDimension('H')->setAutoSize(true);
		$sheet2->getColumnDimension('I')->setAutoSize(true);

		$sheet2->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(1, 1, 'Item Code')->getStyle('B1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(2, 1, 'Serial Number')->getStyle('C1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(3, 1, 'License Plate')->getStyle('D1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(4, 1, 'Batch')->getStyle('E1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(5, 1, 'Expired Date')->getStyle('F1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(6, 1, 'Date In')->getStyle('G1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(7, 1, 'Location')->getStyle('H1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(8, 1, 'Status')->getStyle('I1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(9, 1, 'Received By')->getStyle('J1')->getFont()->setBold(true);


		$data2 = $this->receiving_model->getDetailItemInboundReceiving2($params);
        $row = 2;
        $len = count($data2);

        if($len > 0){

            for($i = 0; $i < $len; $i++){
                $sheet2->setCellValueByColumnAndRow(0, $row, $data2[$i][0]);
                $sheet2->setCellValueByColumnAndRow(1, $row, $data2[$i][1]);
                $sheet2->setCellValueByColumnAndRow(2, $row, $data2[$i][2]);
                $sheet2->setCellValueByColumnAndRow(3, $row, $data2[$i][3]);
                $sheet2->setCellValueByColumnAndRow(4, $row, $data2[$i][4]);
                $sheet2->setCellValueByColumnAndRow(5, $row, $data2[$i][5]);
                $sheet2->setCellValueByColumnAndRow(6, $row, $data2[$i][6]);
                $sheet2->setCellValueByColumnAndRow(7, $row, $data2[$i][7]);
                $sheet2->setCellValueByColumnAndRow(8, $row, $data2[$i][8]);
                $sheet2->setCellValueByColumnAndRow(9, $row, $data2[$i][9]);
                $row++;
            }

        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Tally Report'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

	}

	public function export_excel2(){
		$result = array();
		$this->load_model('search_model');

		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
			'id_receiving'	=> $this->input->post('id_receiving')
		);

		$this->load->library('PHPExcel');
		$data = $this->search_model->getReceivedListExcel($params);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Location List")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Serial Number')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Expired Date')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Date In')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Location')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Status')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Received By')->getStyle('G1')->getFont()->setBold(true);

        $row = 2;
        $len = count($data);

        if($len > 0){

            for($i = 0; $i < $len; $i++){
                $sheet->setCellValueByColumnAndRow(0, $row, $data[$i][0]);
                $sheet->setCellValueByColumnAndRow(1, $row, $data[$i][1]);
                $sheet->setCellValueByColumnAndRow(2, $row, $data[$i][2]);
                $sheet->setCellValueByColumnAndRow(3, $row, $data[$i][3]);
                $sheet->setCellValueByColumnAndRow(4, $row, $data[$i][4]);
                $sheet->setCellValueByColumnAndRow(5, $row, $data[$i][5]);
                $sheet->setCellValueByColumnAndRow(6, $row, $data[$i][6]);
                $row++;
            }

        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Location List'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

	public function backToTally($receivingId=""){

		$data['status']	 = false;
		$data['message'] = "Failed back to tally";

		if(!empty($receivingId)){

			$this->receiving_model->backToTally($receivingId);

			$data['status']	 = true;
			$data['message'] = "Success back to tally";

		}

		echo json_encode($data);

	}

	public function resetReceiving($receivingId=""){

		$data['status']	 = false;
		$data['message'] = "Failed reset receiving";

		if(!empty($receivingId)){

			$data = $this->receiving_model->resetReceiving($receivingId);

		}

		echo json_encode($data);

	}

	public function setComplete($receivingId=""){

		$data['status']	 = false;
		$data['message'] = "Failed set complete.";

		if(!empty($receivingId)){

			$this->receiving_model->setToComplete($receivingId);

			$data['status']	 = true;
			$data['message'] = "Success set complete.";

		}

		echo json_encode($data);

	}

	public function cancel_receiving(){

		$receiving_id = $this->input->post('receiving_id');

		$process = $this->receiving_model->cancel_receiving($receiving_id);

		$data = array();

		if($process){

			$data['status']	 = true;
			$data['message'] = "Cancel Receiving Successful.";

		}else{

			$data['status']	 = false;
			$data['message'] = "Cancel Receiving Failed.";

		}

		echo json_encode($data);

	}

	public function cancel_receiving_item(){

		$receiving_id = $this->input->post('receiving_id');
		$unique_code = $this->input->post('serial_number');

		$process = $this->receiving_model->cancel_receiving_item($receiving_id,$unique_code);

		$data = array();

		if($process){

			$data['status']	 = true;
			$data['message'] = "Cancel Receiving Successful.";

		}else{

			$data['status']	 = false;
			$data['message'] = "Cancel Receiving Failed.";

		}

		echo json_encode($data);

	}

	public function getInboundList(){

		$result = array();

		$params = array(
			'id_inbound' => $this->input->post('id_inbound'),
			'id_barang' => $this->input->post('id_barang')
		);

		$getInboundList= $this->receiving_model->getInboundList($params);

		$result = $getInboundList;


		echo json_encode($result);
	}

}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class picking_list extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Picking List';
        $data['page'] = $this->class_name . '/index';

		$this->load->helper('diff_helper');

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/picking_list/lr-picking-list-view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			/*$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);*/
		}

        //$this->include_other_view_data_filter($data);
		$data['today'] = date('d/m/Y');
		$data['widget'] = $this->picking_list_model->getWidget();
		$data['status']	= $this->picking_list_model->getStatus();

		/* Begin Andri */
		// var_dump(secsToTime($data['widget']['average']));
		$data['widget']['average'] = secsToTime($data['widget']['average']);
		/* End Andri */

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Outbound');
        /* END OF INSERT LOG */

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	//ADD
	public function add($id_outbound=''){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> New Picking List';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/picking_list/lr-picking-list-addedit.js";

			$data['id'] = '';
			$data['id_outbound'] = $id_outbound;
			$data['data']['pl_start'] = '-';
			$data['pl_name'] = $this->picking_list_model->get_pl_code();
			$data['today'] = date('d/m/Y');
			$data['doc_number'] = $this->picking_list_model->getOutboundCodeList();
			$data['picking_areas'] = $this->picking_list_model->getPickingAreaList();

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);
		}
	}

	//EDIT
	public function edit($id){
        // $this->output->enable_profiler(TRUE);

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Picking List';
			$data['page'] = $this->class_name . '/edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/picking_list/lr-picking-list-addedit.js";

			$data['id'] = $id;
			$data['id_outbound'] = '';
			$data['pl_name'] = '';
			$data['today'] = '';

			/*
			$notUse = $this->picking_list_model->cek_not_usage_do($id);
			$nUse = 'false';
			if($notUse)
				$nUse = 'true';
			*/
            $getDetail=$this->getDetail($id);
			$data['data'] = $getDetail->row_array();
            $otbs=array();
            $pickas=array();
            foreach($getDetail->result_array() as $otb){
                $otbs[]=$otb['id_outbound'];
                $pickas[]=$otb['id_outbound'];
            }
			$data['otbs'] = $otbs;
			$data['pickas'] = $pickas;
			
			$data['not_usage'] = '';
			$data['doc_number'] = $this->picking_list_model->getOutboundCodeList();

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//DETAIL
	public function detail($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Document Info';
			$data['page'] = $this->class_name . '/detail';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/picking_list/lr-picking-list-detail.js";

			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = $id;
            $getDetail = $this->getDetail($id);
			$data['data'] = $getDetail->row_array();
            $data['otbs'] = $getDetail->result_array();

			// $data['not_usage'] = $this->picking_list_model->cek_not_usage_do($id);
			$data['not_usage'] = '';

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

    private function include_other_view_data_filter(&$data) {
    	$this->load_model('outbound_document_model');
        $data['outbound_docs'] = $this->outbound_document_model->options();
		$data['doc_status'] = $this->picking_list_model->statusOptions();
    }

    public function get_add_item_row() {
        $data['items'] = $this->picking_list_model->create_options("barang", "id_barang", "nama_barang", "-- All --");
        $data['random_string'] = $this->randomidgenerator->generateRandomString(4);
        echo $this->load->view("outbound/add_item_row", $data, true);
    }

	public function getDetail($id = false){
		$result = array();

		$params = array(
			'pl_id'	=> $id
		);

		$result = $this->picking_list_model->getDetail2($params);

		return $result;
	}

	//REQUEST
	public function getBatchRow(){
		$result = array();

		$params = array(
			'id_outbound'		=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT),
			'id_barang'			=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
			'kd_batch'			=> filter_var(trim($this->input->post('kd_batch')), FILTER_SANITIZE_STRING)
		);

		$result = $this->picking_list_model->getBatchRow($params);

		echo json_encode($result);
	}

	public function getScannedList(){
		$result = array();

		$params = array(
			'pl_id'				=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT),
			'id_barang'			=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->picking_list_model->getScannedList($params);

		echo json_encode($result);
	}

	public function getDetailItemList(){
		$result = array();

		$params = array(
			'pl_id'				=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT),
			'id_outbound'		=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT),
		);

		if($params['pl_id'] != '') {
			$result = $this->picking_list_model->getDetailItemList($params);
		}

		echo json_encode($result);
	}

	public function getSubstitute(){
		$result = array();
		$params = array(
			'item_id' => $this->input->post('item_id'),
			'pq_id' => $this->input->post('pq_id')
		);
		
		$result = $this->picking_list_model->getSubstitute($params);

		echo json_encode($result);
	}

	public function checkSubstitute(){
		$result = array();
		$data = array();
		$params = array(
			'pq_id' => $this->input->post('pq_id'),
			'item_substitute' => $this->input->post('item_subs'),
			'user_id' => $this->session->userdata('user_id'),
			'qty' => $this->input->post('qty')
		);
		
		$result = $this->picking_list_model->checkSubstitute($params);
		
		if($result){
			$data['message'] = 'Item has been update';
			$data['status'] = 'OK';
		}else{
			$data['message'] = 'Please Try Again!';
			$data['status'] = 'OK';
		}

		return json_encode($data);
	}

	public function getSubItem(){
		$result = array();

		$params = array(
			'pl_id'				=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT),
			'id_barang'			=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->picking_list_model->getSubItem($params);

		echo json_encode($result);
	}

	public function closePicking(){
		$result = array();

		$params = array(
			'pl_id'		=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->picking_list_model->closePicking($params);

		echo json_encode($result);
	}

	public function print_picking_list(){
		$data = array();

		$params = array(
			'pl_id'	=> $this->input->post('pl_id')
		);

		$this->picking_list_model->printed($params);

		$pick = $this->picking_list_model->getEdit($params);

		$data['page'] = $this->class_name . '/picking_list_report';
        $data['pick'] = $pick;
		$data['items'] = $this->picking_list_model->getItems($params);
		// dd($data['items']);
        $html = $this->load->view($data['page'], $data, true);
        $path = "picking_list_report.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load('picking_list');

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		//START TALLY
		//$this->receiving_model->startTally($params);
		$pdf->addPage('P');
		$pdf->WriteHTML($html); // write the HTML into the PDF
		// $pdf->writeHTML($tray);
		$pdf->Output($path, 'I'); // save to file because we can

    }

	public function print_picking_list_bulk(){
		$data = array();

		$pl_id = json_decode($this->input->post('pl_id'));
		
		$this->load->library('pdf');
		$pdf = $this->pdf->load('picking_list');
		for($i=0;$i<count($pl_id);$i++){
			$params = array();

			$params = array(
				'pl_id'	=> $pl_id[$i]
			);

			$this->picking_list_model->printed($params);

			$pick = $this->picking_list_model->getEdit($params);

			$data['page'] = $this->class_name . '/picking_list_report';
			$data['pick'] = $pick;
			$data['items'] = $this->picking_list_model->getItems($params);
			// dd($data['items']);
			$html = $this->load->view($data['page'], $data, true);
			$path = "picking_list_report.pdf";

			ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)
			
			//START TALLY
			//$this->receiving_model->startTally($params);
			$pdf->addPage('P');
			$pdf->WriteHTML($html); // write the HTML into the PDF
			// $pdf->writeHTML($tray);
		}
		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)
		$pdf->Output($path, 'I'); // save to file because we can

    }

	public function print_tray_list(){
		$data = array();

		$params = array(
			'pl_id'	=> $this->input->post('pl_id')
		);
		
		$pick = $this->picking_list_model->getEdit($params);

		$data['page'] = $this->class_name . '/picking_list_report';
        $data['pick'] = $pick;
		$data['items'] = $this->picking_list_model->getItems($params);
		$data['tray'] = $this->picking_list_model->getInTransit($params);

		$tray = $this->load->view($data['page'].'_tray', $data, true);
        $path = "tray_list.pdf";
        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load('picking_list');
		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)
		$pdf->writeHTML($tray);
		$pdf->Output($path, 'I'); // save to file because we can
	}

	public function export_excel(){
		$data = array();

		$params = array(
			'picking_id'	=> $this->input->post('picking_id')
		);

        $this->load->library('PHPExcel');

        $objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Picking Report")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->createSheet(0);

		$sheet->setTitle("Summary");

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Item Code')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Item Name')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Doc. Qty')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Pick. Qty')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Discrepancies')->getStyle('H1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'UoM')->getStyle('I1')->getFont()->setBold(true);


		$summary = $this->picking_list_model->export_excel($params);

		$row=2;
		$i=1;

		foreach ($summary as $s) {

            $sheet->setCellValueByColumnAndRow(0, $row, $i);
            $sheet->setCellValueByColumnAndRow(1, $row, $s['item_code']);
            $sheet->setCellValueByColumnAndRow(2, $row, $s['item_name']);
            $sheet->setCellValueByColumnAndRow(3, $row, $s['doc_qty']);
            $sheet->setCellValueByColumnAndRow(4, $row, $s['pick_qty']);
            $sheet->setCellValueByColumnAndRow(5, $row, ($s['doc_qty'] - $s['pick_qty']));
            $sheet->setCellValueByColumnAndRow(6, $row, $s['unit_code']);

            $row++;
            $i++;
		}

        // Sheet 2
		$sheet2 = $objPHPExcel->createSheet(1);
		$sheet2->setTitle("Details");

		$sheet2->getColumnDimension('A')->setAutoSize(true);
		$sheet2->getColumnDimension('B')->setAutoSize(true);
		$sheet2->getColumnDimension('C')->setAutoSize(true);
		$sheet2->getColumnDimension('D')->setAutoSize(true);
		$sheet2->getColumnDimension('E')->setAutoSize(true);
		$sheet2->getColumnDimension('F')->setAutoSize(true);
		$sheet2->getColumnDimension('G')->setAutoSize(true);

		$sheet2->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(1, 1, 'Item Code')->getStyle('B1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(2, 1, 'Serial Number')->getStyle('C1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(3, 1, 'License Plate')->getStyle('D1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(4, 1, 'Pick Locator')->getStyle('E1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(5, 1, 'Batch')->getStyle('F1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(6, 1, 'Expired Date')->getStyle('G1')->getFont()->setBold(true);
		$sheet2->setCellValueByColumnAndRow(7, 1, 'Picked By')->getStyle('H1')->getFont()->setBold(true);


		$details = $this->picking_list_model->export_excel_detail($params);

        $row = 2;
        $i=1;


        foreach ($details as $d) {

            $sheet->getStyle('C'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $sheet2->setCellValueByColumnAndRow(0, $row, $i);
            $sheet2->setCellValueByColumnAndRow(1, $row, $d['item_code']);
            $sheet2->setCellValueByColumnAndRow(2, $row, $d['unique_code']);
            $sheet2->setCellValueByColumnAndRow(3, $row, $d['parent_code']);
            $sheet2->setCellValueByColumnAndRow(4, $row, $d['pick_locator']);
            $sheet2->setCellValueByColumnAndRow(5, $row, '-');
            $sheet2->setCellValueByColumnAndRow(6, $row, $d['expired_date']);
            $sheet2->setCellValueByColumnAndRow(7, $row, $d['user_picked']);


            $row++;
            $i++;

        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Picking Report'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

	}

	public function getEdit(){
		$result = array();

		$params = array(
			'pl_id'	=> filter_var(trim($this->input->post('id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->picking_list_model->getEdit($params);

		echo json_encode($result);
	}

	public function checkExistDoNumber(){
		$result = array();

		$params = array(
			'id_outbound'	=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT),
			'kd_outbound'	=> filter_var(trim($this->input->post('kd_outbound')), FILTER_SANITIZE_STRING)
		);

		$result = $this->picking_list_model->checkExistDoNumber($params);

		echo json_encode($result);
	}

	public function getDetailItemPickedDataTable(){
		$result = array();

		$params = array(
			'pl_id'			=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->picking_list_model->getDetailItemPickedDataTable($params);

		echo json_encode($result);
	}

	public function getDetailItemPicked($id = false){
		$result = array();
		$this->load_model('outbound_model');

		$params = array(
			'id_outbound'	=> filter_var($this->input->post('id_outbound'), FILTER_SANITIZE_NUMBER_INT),
			'pl_id'			=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->picking_list_model->getDetailItemPicked($params);

		echo json_encode($result);
	}

	public function getDetailItem($id = false){
		$result = array();
		$this->load_model('outbound_model');

		if($this->input->post('id_outbound') != null) {
			// $id_area = $this->input->post('id_area') != null ? filter_var(trim(implode('\', \'',$this->input->post('id_area'))), FILTER_SANITIZE_STRING) : '';
			
			$params = array(
				'id_outbound'	=> $this->input->post('id_outbound'),
				'id_area'		=> $this->input->post('id_area'),
				'pl_id'			=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT)
			);

			// dd($params);

			$result = $this->picking_list_model->getDetailItem($params);
		}

		echo json_encode($result);
	}

	public function getPickingList(){
		$result = array();

		$params = array(
			'id_outbound'		=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT),
			'params'			=> filter_var(trim($this->input->post('params')), FILTER_SANITIZE_STRING)
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->picking_list_model->getPickingList($params);

		echo json_encode($result);
	}

	public function getShippingList(){
		$result = array();

		$params = array(
			'id_outbound'		=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT),
			'params'			=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_STRING)
		);

		$result = $this->picking_list_model->getShippingList($params);

		echo json_encode($result);
	}

	public function getNewPickingListCode(){
		$result = array();
		$row = $this->picking_list_model->get_pl_code();

		if($row){

			$result['status'] = 'OK';
			$result['outbound_code'] = $row;

		}else{

			$result['status'] = 'OK';
			$result['outbound_code'] = $row;

		}

		echo json_encode($result);
	}

	public function getPlCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->picking_list_model->getPlCode($params);

		echo json_encode($result);
	}

	public function getDnList(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->picking_list_model->getDnList($params);

		echo json_encode($result);
	}

	public function getCustomerBySearch(){
		$result = array();

		$params = array(
			'query'	=> filter_var(trim($this->input->post('query')), FILTER_SANITIZE_STRING)
		);

		$result = $this->picking_list_model->getCustomerBySearch($params);

		echo json_encode($result);
	}

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            //$outbound = $this->picking_list_model->get_data(array('id_outbound' => $id))->row_array();
			$outbound = $this->picking_list_model->get_data($id)->row_array();
            $outbound['tanggal_outbound'] = date('d/m/Y', strtotime(str_replace('/','-', $outbound['tanggal_outbound'])));

            $arr_outbound_barang = $this->picking_list_model->get_outbound_barang($outbound['id_outbound'])->result_array();
            $rows = "";
            $data['items'] = $this->picking_list_model->create_options("barang", "id_barang", "nama_barang", "-- All --");
            foreach ($arr_outbound_barang as $outbound_barang) {
                $data['random_string'] = $this->randomidgenerator->generateRandomString(4);
                $data['outbound_barang'] = $outbound_barang;
                $rows .= $this->load->view("outbound/add_item_row", $data, true);
            }
            $outbound['outbound_barang_html'] = $rows;

            $process_result = $outbound;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){
        $this->session->set_userdata('current_filter',$this->input->post('periode'));
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $outbounds = $this->picking_list_model->data($param)->get();
        $iTotalRecords = $this->picking_list_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($outbounds->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_outbound,'delete' => $value->id_outbound));

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="1">',
                $_REQUEST['start']+$i,
                $value->kd_outbound,
                DateTime::createFromFormat('Y-m-d', $value->tanggal_outbound)->format('d/m/Y'),
                $value->outbound_document_name,
                $value->customer_name,
                $action
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        $records["current_filter"] = $this->session->userdata('current_filter');

        echo json_encode($records);
    }


    public function proses_old() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

			$proses_result['success'] = 1;

			$this->form_validation->set_rules('pl_name', 'Picking list number', 'required|trim');
			$this->form_validation->set_rules('tanggal_picking', 'Picking date', 'required|trim');
			$this->form_validation->set_rules('id_outbound', 'Outbound Doc.', 'required');

			if ($this->form_validation->run()) {
				$id = $this->input->post('id');
				$tanggal = $this->input->post("tanggal_picking");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal)))) . ' ' . date('H:i:s');

				//'id_outbound'		 	=> $this->input->post("id_outbound"),
				$data = array(
					'pl_name'	 			=> $this->input->post("pl_name"),
					'pl_date'		 		=> $date,
					'id_status_picking'		=> 1,
					'user_id'				=> $this->session->userdata('user_id')
				);

				$idOutbound = $this->input->post("id_outbound");

        $priority = $this->db->select('m_priority_id')->from('outbound')->where('id', $idOutbound)->get()->row_array()['m_priority_id'];

        $data['priority'] = $priority;

				if (empty($id)) {

					$picking_exist = $this->picking_list_model->check_picking($idOutbound[0]);
					if(!$picking_exist){

						$data['pl_status'] = '0';

            $create = $this->picking_list_model->create($data);


						// if ($this->picking_list_model->create($data)) {
            if ($create > 0) {

							// $id = $this->db->insert_id();
              $id = $create;


							$this->picking_list_model->add_pl_code();

							$params = array(
								'pl_id'			=> $id,
								'id_outbound'	=> $idOutbound
							);

							// $this->picking_list_model->insertRelationTable($params);
              $this->picking_list_model->insertRelationTable2($params);
							$proses_result['add'] = 1;

							$brg = $this->input->post('barang');
							if(!empty($brg)){
								$data = $this->input->post('barang');
								$len = count($data);

								for($i = 0; $i < $len; $i++){

									$idBarang = $data[$i]['id_barang'];
									$qty = $data[$i]['qty'];
									$batch = $data[$i]['batch'];

									$this->picking_list_model->update_pl_qty($id,$idBarang,$qty,$batch);
								}
							}

							// $this->picking_list_model->setRecomendation($params);
							$this->picking_list_model->updateOutboundStatus($idOutbound[0]);

							//$message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
							/* INSERT LOG */
							$this->access_right->activity_logs('add','Picking list');
							/* END OF INSERT LOG */
						}
					}else{
						$proses_result['picking_exist'] = 1;
						$proses_result['success'] = 0;
					}
				} else {
					$data['pl_status'] = '6';

					if ($this->picking_list_model->update($data, $id)) {

						$params = array(
							'pl_id'			=> $id,
							'id_outbound'	=> $idOutbound
						);

						$this->picking_list_model->insertRelationTable2($params);

						$proses_result['edit'] = 1;

						$brg = $this->input->post('barang');
						if(!empty($brg)){
							$data = $this->input->post('barang');
							$len = count($data);

							for($i = 0; $i < $len; $i++){

								$idBarang = $data[$i]['id_barang'];
								$qty = $data[$i]['qty'];
								$batch = $data[$i]['batch'];

								$this->picking_list_model->update_pl_qty($id,$idBarang,$qty,$batch);
							}
						}

						$this->picking_list_model->setRecomendation($params);

						//$message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Picking list');
						/* END OF INSERT LOG */
					}
				}

				/*
				$data2 = array();
				$arr_id_barang = $this->input->post("id_barang");
				$arr_item_quantity = $this->input->post("qty");

				if(!empty($arr_id_barang)){
					if($arr_id_barang){
						foreach ($arr_id_barang as $key => $value) {
							$data2[] = array(
									'jumlah_barang' => $arr_item_quantity[$key],
									'id_barang' => $value
								);
						}
					}
				}

				/*
				* If : $id == '', lakukan proses create data
				* Else : lakukan proses update
				//

				if (empty($id)) {
					if ($this->picking_list_model->create($data, $data2)) {
						$proses_result['add'] = 1;
						$this->picking_list_model->add_do_code();

						/* INSERT LOG //
						$this->access_right->activity_logs('add','Picking List');
						/* END OF INSERT LOG //
					}
				} else {
					if ($this->picking_list_model->update($data, $data2, $id)) {
						$proses_result['edit'] = 1;

						/* INSERT LOG //
						$this->access_right->activity_logs('edit','Picking List');
						/* END OF INSERT LOG //
					}
				}
				$proses_result['success'] = 1;
				*/
			} else {
				$proses_result['success'] = 0;
				$proses_result['message'] = validation_errors();
			}
        } else {
            if(!$this->access_right->otoritas('add')) {
				$proses_result['success'] = 0;
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
				$proses_result['success'] = 0;
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }
	
	public function proses() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

			$proses_result['success'] = 1;

			$this->form_validation->set_rules('pl_name', 'Picking list number', 'required|trim');
			$this->form_validation->set_rules('tanggal_picking', 'Picking date', 'required|trim');
			$this->form_validation->set_rules('id_outbound', 'Outbound Doc.', 'required');

			if ($this->form_validation->run()) {
				$id = $this->input->post('id');
				$tanggal = $this->input->post("tanggal_picking");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal)))) . ' ' . date('H:i:s');

				//'id_outbound'		 	=> $this->input->post("id_outbound"),
				$data = array(
					'pl_name'	 			=> $this->input->post("pl_name"),
					'pl_date'		 		=> $date,
					'id_status_picking'		=> 1,
					'user_id'				=> $this->session->userdata('user_id')
				);

				$idOutbound = $this->input->post("id_outbound");

        $priority = $this->db->select('m_priority_id')->from('outbound')->where('id', $idOutbound)->get()->row_array()['m_priority_id'];

        $data['priority'] = $priority;

				if (empty($id)) {

					$picking_exist = $this->picking_list_model->check_picking($idOutbound[0]);
					if(!$picking_exist){

						$data['pl_status'] = '0';

            $create = $this->picking_list_model->create($data);


						// if ($this->picking_list_model->create($data)) {
            if ($create > 0) {

							// $id = $this->db->insert_id();
              $id = $create;


							$this->picking_list_model->add_pl_code();

							$params = array(
								'pl_id'			=> $id,
								'id_outbound'	=> $idOutbound
							);

							// $this->picking_list_model->insertRelationTable($params);
              $this->picking_list_model->insertRelationTable2($params);
							$proses_result['add'] = 1;

							$brg = $this->input->post('barang');
							if(!empty($brg)){
								$data = $this->input->post('barang');
								$len = count($data);

								for($i = 0; $i < $len; $i++){

									$idBarang = $data[$i]['id_barang'];
									$qty = $data[$i]['qty'];
									$batch = $data[$i]['batch'];

									$this->picking_list_model->update_pl_qty($id,$idBarang,$qty,$batch);
								}
							}

							// $this->picking_list_model->setRecomendation($params);
							$this->picking_list_model->updateOutboundStatus($idOutbound[0]);

							//$message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
							/* INSERT LOG */
							$this->access_right->activity_logs('add','Picking list');
							/* END OF INSERT LOG */
						}
					}else{
						$proses_result['picking_exist'] = 1;
						$proses_result['success'] = 0;
					}
				} else {
					$data['pl_status'] = '0';
					
					if ($this->picking_list_model->update($data, $id)) {

						$params = array(
							'pl_id'			=> $id,
							'id_outbound'	=> $idOutbound
						);

						//$this->picking_list_model->insertRelationTable2($params);

						$proses_result['edit'] = 1;

						$brg = $this->input->post('barang');
						if(!empty($brg)){
							$data = $this->input->post('barang');
							$len = count($data);
							//dd($data);
							for($i = 0; $i < $len; $i++){

								$idBarang = $data[$i]['id_barang'];
								$qty = $data[$i]['qty'];
								$batch = $data[$i]['batch'];
								
								if($qty > 0){
									$this->picking_list_model->update_pl_qty($id,$idBarang,$qty,$batch);
								}else{
									$this->picking_list_model->delete_pl_qty($id,$idBarang,$qty,$batch);
								}
								
							}
						}

						//$this->picking_list_model->setRecomendation($params);

						//$message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Picking list');
						/* END OF INSERT LOG */
					}
				}

				/*
				$data2 = array();
				$arr_id_barang = $this->input->post("id_barang");
				$arr_item_quantity = $this->input->post("qty");

				if(!empty($arr_id_barang)){
					if($arr_id_barang){
						foreach ($arr_id_barang as $key => $value) {
							$data2[] = array(
									'jumlah_barang' => $arr_item_quantity[$key],
									'id_barang' => $value
								);
						}
					}
				}

				/*
				* If : $id == '', lakukan proses create data
				* Else : lakukan proses update
				//

				if (empty($id)) {
					if ($this->picking_list_model->create($data, $data2)) {
						$proses_result['add'] = 1;
						$this->picking_list_model->add_do_code();

						/* INSERT LOG //
						$this->access_right->activity_logs('add','Picking List');
						/* END OF INSERT LOG //
					}
				} else {
					if ($this->picking_list_model->update($data, $data2, $id)) {
						$proses_result['edit'] = 1;

						/* INSERT LOG //
						$this->access_right->activity_logs('edit','Picking List');
						/* END OF INSERT LOG //
					}
				}
				$proses_result['success'] = 1;
				*/
			} else {
				$proses_result['success'] = 0;
				$proses_result['message'] = validation_errors();
			}
        } else {
            if(!$this->access_right->otoritas('add')) {
				$proses_result['success'] = 0;
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
				$proses_result['success'] = 0;
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function proses_backup() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

			$proses_result['success'] = 1;

			$this->form_validation->set_rules('pl_name', 'Picking list number', 'required|trim');
			$this->form_validation->set_rules('tanggal_picking', 'Picking date', 'required|trim');
			// $this->form_validation->set_rules('id_outbound', 'Outbound Doc.', 'required|trim');
      $this->form_validation->set_rules('id_outbound', 'Outbound Doc.', 'required');


			if ($this->form_validation->run()) {
				$id = $this->input->post('id');
				$tanggal = $this->input->post("tanggal_picking");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal)))) . ' ' . date('H:i:s');

				//'id_outbound'		 	=> $this->input->post("id_outbound"),
				$data = array(
					'pl_name'	 			=> $this->input->post("pl_name"),
					'pl_date'		 		=> $date,
					'id_status_picking'		=> 1,
					'user_id'				=> $this->session->userdata('user_id')
				);

				$idOutbound = $this->input->post("id_outbound");

				if (empty($id)) {

					$picking_exist = $this->picking_list_model->check_picking($idOutbound);
					if(!$picking_exist){

						$data['pl_status'] = '0';

						if ($this->picking_list_model->create($data)) {

							$id = $this->db->insert_id();

							$this->picking_list_model->add_pl_code();

							$params = array(
								'pl_id'			=> $id,
								'id_outbound'	=> $idOutbound
							);

							$this->picking_list_model->insertRelationTable($params);

							$proses_result['add'] = 1;

							$brg = $this->input->post('barang');
							if(!empty($brg)){
								$data = $this->input->post('barang');
								$len = count($data);

								for($i = 0; $i < $len; $i++){

									$idBarang = $data[$i]['id_barang'];
									$qty = $data[$i]['qty'];
									$batch = $data[$i]['batch'];

									$this->picking_list_model->update_pl_qty($id,$idBarang,$qty,$batch);
								}
							}

							$this->picking_list_model->setRecomendation($params);
							$this->picking_list_model->updateOutboundStatus($idOutbound);

							//$message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
							/* INSERT LOG */
							$this->access_right->activity_logs('add','Picking list');
							/* END OF INSERT LOG */
						}
					}else{
						$proses_result['picking_exist'] = 1;
						$proses_result['success'] = 0;
					}
				} else {

					if ($this->picking_list_model->update($data, $id)) {

						$params = array(
							'pl_id'			=> $id,
							'id_outbound'	=> $idOutbound
						);

						$this->picking_list_model->insertRelationTable($params);

						$proses_result['edit'] = 1;

						$brg = $this->input->post('barang');
						if(!empty($brg)){
							$data = $this->input->post('barang');
							$len = count($data);

							for($i = 0; $i < $len; $i++){

								$idBarang = $data[$i]['id_barang'];
								$qty = $data[$i]['qty'];
								$batch = $data[$i]['batch'];

								$this->picking_list_model->update_pl_qty($id,$idBarang,$qty,$batch);
							}
						}

						$this->picking_list_model->setRecomendation($params);

						//$message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Picking list');
						/* END OF INSERT LOG */
					}
				}

				/*
				$data2 = array();
				$arr_id_barang = $this->input->post("id_barang");
				$arr_item_quantity = $this->input->post("qty");

				if(!empty($arr_id_barang)){
					if($arr_id_barang){
						foreach ($arr_id_barang as $key => $value) {
							$data2[] = array(
									'jumlah_barang' => $arr_item_quantity[$key],
									'id_barang' => $value
								);
						}
					}
				}

				/*
				* If : $id == '', lakukan proses create data
				* Else : lakukan proses update
				//

				if (empty($id)) {
					if ($this->picking_list_model->create($data, $data2)) {
						$proses_result['add'] = 1;
						$this->picking_list_model->add_do_code();

						/* INSERT LOG //
						$this->access_right->activity_logs('add','Picking List');
						/* END OF INSERT LOG //
					}
				} else {
					if ($this->picking_list_model->update($data, $data2, $id)) {
						$proses_result['edit'] = 1;

						/* INSERT LOG //
						$this->access_right->activity_logs('edit','Picking List');
						/* END OF INSERT LOG //
					}
				}
				$proses_result['success'] = 1;
				*/
			} else {
				$proses_result['success'] = 0;
				$proses_result['message'] = validation_errors();
			}
        } else {
            if(!$this->access_right->otoritas('add')) {
				$proses_result['success'] = 0;
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
				$proses_result['success'] = 0;
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function save_new_customer() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $proses_result['success'] = 1;
            $this->form_validation->set_rules('customer_name', 'outbound Code', 'required|trim');
            $this->form_validation->set_rules('phone', 'outbound Code', 'required|trim');

            if ($this->form_validation->run()) {
                $data = array(
                    'customer_name' => $this->input->post("customer_name"),
                    'phone' => $this->input->post("phone")
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($this->picking_list_model->create_customer($data)) {
                    $proses_result['add'] = 1;
                    /* INSERT LOG */
                    $this->access_right->activity_logs('add','Outbound - Creating new customer');
                    /* END OF INSERT LOG */
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            $result = $this->picking_list_model->delete($id);
            $result['success'] = 1;
        } else {
            $result['no_delete'] = 1;
        }
        echo json_encode($result);
    }

    public function delete_by_id($id) {
        $result = array();
        if ($this->access_right->otoritas('delete', true)) {
            //$id = $this->input->post("id");
            $result = $this->picking_list_model->force_delete($id);
            $result['success'] = 1;
        } else {
            $result['no_delete'] = 1;
        }
        echo $result;
    }

    public function load_customers() {
        $customer_options = $this->picking_list_model->create_options("m_customer", "id_customer", "customer_name", "-- Choose customer --");
        $html = "";
        foreach ($customer_options as $key => $option) {
            $html .= "<option value='$key'>$option</option>";
        }
        echo $html;
    }

    public function cancel(){

		$result = array();

		$params = array(
			'picking_id' => $this->input->post('picking_id'),
		);

		$cancel = $this->picking_list_model->cancel($params);

		if($cancel){

			$result['status'] = "success";
			$result['message'] = "Success to update data";

		}else{

			$result['status'] = "failed";
			$result['message'] = "Failed to update data";

		}

		echo json_encode($result);

    }

    public function resetPicking(){

		$result = array();

		$params = array(
			'picking_id' => $this->input->post('picking_id')
		);

		$cancel = $this->picking_list_model->resetPicking($params);

		if($cancel){

			$result['status'] = "success";
			$result['message'] = "Success to cancel item picked";

		}else{

			$result['status'] = "failed";
			$result['message'] = "Failed to cancel item picked";

		}

		echo json_encode($result);

    }



    public function cancel_item_picked(){

		$result = array();

		$params = array(
			'picking_id'		=> filter_var(trim($this->input->post('picking_id')), FILTER_SANITIZE_NUMBER_INT),
			'serial_number'		=> $this->input->post('serial_number'),
			'quantity'			=> $this->input->post('quantity')
		);

		$cancel = $this->picking_list_model->cancel_item_picked($params);

		if($cancel){

			$result['status'] = true;
			$result['message'] = "Success to cancel item picked";

		}else{

			$result['status'] = false;
			$result['message'] = "Failed to cancel item picked";

		}

		echo json_encode($result);

    }

public function setComplete(){

    	$result = array();

    	$params = array(
    		'picking_id'	=> filter_var(trim($this->input->post('picking_id')), FILTER_SANITIZE_NUMBER_INT)
    	);

    	$setComplete = $this->picking_list_model->setComplete($params['picking_id']);

    	if($setComplete){
    		$result['status'] = true;
    		$result['message'] = 'Success to complete picking document.';
    	}else{
    		$result['status'] = false;
    		$result['message'] = "Failed to complete picking document.";
    	}

    	echo json_encode($result);

    }

}

/* End of file referensi_barang.php */
/* Location: ./application/controllers/referensi_barang.php */

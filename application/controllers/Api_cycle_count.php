<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_cycle_count extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_cycle_count_model');
		$this->load_model('api_receiving_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	//andri
	public function tempPost($json = '', $cc_document = '') {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else {
			$json			= $this->input->post('json');
			$cc_document	= $this->input->post('cc_document');
			$result = $this->api_cycle_count_model->temp_post_cc($json, $cc_document);
			
			$data['param']		= $cc_document;
			$data['status']		= 200;
			$data['message']	= 'Done';
			$data['response']	= true;
			$data['time']		= date("Y-m-d H:i:s");
			$data['results']	= $result;
		}
	}

	public function load()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= 'CYCLE COUNT';
				$data['status']		= 200;
				$data['message']	= 'Load data for available cycle count list.';
				$data['response']	= true;
				$data['results']	= $this->api_cycle_count_model->load()->result_array();
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function load_type($type = '', $cycle_code = '', $user = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $type == '' || $cycle_code == '' || $user == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= $type;
				$data['status']		= 200;
				$data['message']	= 'Load data type for positive adjustment.';
				$data['response']	= true;
				$data['results']	= $this->api_cycle_count_model->load_type($type)->result_array();
				$cc_to_rcv 			= $this->api_cycle_count_model->get_cc_to_rcv($cycle_code)->result_array();
				if($cc_to_rcv){
					// nothing to do.
				}else{
					// $this->api_cycle_count_model->set_cc_to_rcv($cycle_code,$user);
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get($cycle_code = '', $cycle_type = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $cycle_code == '' || $cycle_type == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$cycle_code = urldecode($cycle_code);
				$check = $this->api_cycle_count_model->check($cycle_code)->row_array();
				if($check){
					if($check['status'] == 1){
						$data['param']		= $cycle_code;
						$data['status']		= 401;
						$data['message']	= $cycle_code . ' has been cycled before.';
						$data['response']	= false;
					} else{
						$data['param']		= $cycle_code;
						$data['status']		= 200;
						$data['message']	= $cycle_code . ' is available.';
						$data['response']	= true;
						$data['results']	= $this->api_cycle_count_model->get($cycle_code, $cycle_type)->result_array();
					}
				} else{
					$data['param']		= $cycle_code;
					$data['status']		= 401;
					$data['message']	= $cycle_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	
	public function get_positive_type($cycle_code = '', $type = '', $type1 = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $cycle_code == '' || $type == '' || $type1 == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$check = $this->api_cycle_count_model->check_positive_type($cycle_code, $type, $type1)->row_array();
				if($check){
					$data['param']		= $cycle_code;
					$data['status']		= 200;
					$data['message']	= $cycle_code . ' is available.';
					$data['response']	= true;
				} else{
					$data['param']		= $cycle_code;
					$data['status']		= 401;
					$data['message']	= $cycle_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	/*
	public function get_serial($cycle_code = '', $type = '', $item_code = '', $serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $cycle_code == '' || $item_code == '' || $type == '' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_cycle_count_model->get_serial($cycle_code,$item_code,$type,$serial_number)->result_array();
				if($result){
					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= $serial_number . ' is valid.';
					$data['response']	= true;
					$data['time']		= date("Y-m-d H:i:s");
					$data['results']	= $result;
					$this->api_cycle_count_model->start($cycle_code);
				} else{
					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
					$data['time']		= date("Y-m-d H:i:s");
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	*/
	
	public function get_serial()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$cycle_code		= $this->input->post('cycle_code');
				$type			= $this->input->post('type');
				$item_code		= $this->input->post('item_code');
				$serial_number	= $this->input->post('serial_number');
				$result = $this->api_cycle_count_model->get_serial($cycle_code,$item_code,$type,$serial_number)->result_array();
				if($result){
					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= $serial_number . ' is valid.';
					$data['response']	= true;
					$data['time']		= date("Y-m-d H:i:s");
					$data['results']	= $result;
					$this->api_cycle_count_model->start($cycle_code);
				} else{

					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
					$data['time']		= date("Y-m-d H:i:s");
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	
	public function get_serial_loc()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$cycle_code		= $this->input->post('cycle_code');
				$type			= $this->input->post('type');
				$item_code		= $this->input->post('item_code');
				$serial_number	= $this->input->post('serial_number');
				$result = $this->api_cycle_count_model->get_serial_loc($serial_number)->result_array();
				if($result){
					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= $serial_number . ' is valid.';
					$data['response']	= true;
					$data['time']		= date("Y-m-d H:i:s");
					$data['results']	= $result;
					$this->api_cycle_count_model->start($cycle_code);
				} else{
					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
					$data['time']		= date("Y-m-d H:i:s");
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	
	public function get_serial_item($serial_number){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_cycle_count_model->get_serial_item($serial_number)->result_array();
				if($result != NULL){

					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= $serial_number . ' is valid.';
					$data['response']	= true;
					$data['results']	= $result;

				} else{

					$checkLp = $this->api_cycle_count_model->get_serial_lp($serial_number)->result_array();
					if($checkLp){

						$data['param']		= $serial_number;
						$data['status']		= 500;
						$data['message']	= $serial_number . ' is license plate.';
						$data['response']	= false;
						$data['results']	= $result;

					}else{
	
						$data['param']		= $serial_number;
						$data['status']		= 401;
						$data['message']	= $serial_number . ' is not valid.';
						$data['response']	= false;
						$data['results']	= $result;
	
					}

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	
	public function start_cc($cycle_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $cycle_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data['param']		= $cycle_code;
				$data['status']		= 200;
				$data['message']	= $cycle_code . ' is valid.';
				$data['response']	= true;
				$this->api_cycle_count_model->start($cycle_code);
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_finish($cycle_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $cycle_code == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$check = $this->api_cycle_count_model->check($cycle_code)->row_array();
				if($check){
					$data['param']		= $cycle_code;
					$data['status']		= 200;
					$data['message']	= $cycle_code . ' is already cycled.';
					$data['response']	= true;
					$this->api_cycle_count_model->finish($cycle_code);
				} else{
					$data['param']		= $cycle_code;
					$data['status']		= 401;
					$data['message']	= $cycle_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_positive_serial()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$cycle_code		= $this->input->post('cycle_code');
				$type			= $this->input->post('type');
				$item_code		= $this->input->post('item_code');
				$serial_number	= $this->input->post('serial_number');
				$result = $this->api_cycle_count_model->get_positive_serial($cycle_code,$item_code,$type,$serial_number)->result_array();
				if($result){
					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= $serial_number . ' is valid.';
					$data['response']	= true;
					$data['time']		= date("Y-m-d H:i:s");
					$data['results']	= $result;
					// $this->api_cycle_count_model->start($cycle_code);
				} else{

					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
					$data['time']		= date("Y-m-d H:i:s");

				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	public function get_positive_serial_bak($serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$result = $this->api_cycle_count_model->get_positive_serial($serial_number)->result_array();
				if($result){
					$data['param']		= $serial_number;
					$data['status']		= 200;
					$data['message']	= $serial_number . ' is valid.';
					$data['response']	= true;
					$data['results']	= $result; //ini gw tambahin, bener atau engga
				} else{
					$data['param']		= $serial_number;
					$data['status']		= 401;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	
	public function get_item()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['type']			= $this->input->post('type');
				$result = $this->api_cycle_count_model->get_item($params['type'])->result_array();
				if($result){
					$data['param']		= $params['type'];
					$data['status']		= 200;
					$data['message']	= $params['type'] . ' is valid.';
					$data['response']	= true;
					$data['results']	= $result;
				} else{
					$data['param']		= $params['type'];
					$data['status']		= 401;
					$data['message']	= $params['type'] . ' is not valid.';
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['cc_code']		= $this->input->post('cc_code');
				$params['type']			= $this->input->post('type');
				$params['type1']		= $this->input->post('type1');
				$params['kd_unik']		= $this->input->post('serial_number');
				$params['cc_qty']		= $this->input->post('qty');
				$params['cc_time']		= $this->input->post('time');
				$params['user']			= $this->input->post('uname');
				$params['kd_qc']		= $this->input->post('kd_qc');
				$params['tgl_exp']		= $this->input->post('tgl_exp');
				$params['tgl_in']		= $this->input->post('tgl_in');
				$params['putaway_time']	= $this->input->post('putaway_time');
				$params['outer_label']	= $this->input->post('outer_label');

				if($params['cc_code'] == '' || $params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{

					$this->db->trans_start();

					$cek_exist = $this->api_cycle_count_model->is_exist_to_post($params)->row_array();

					if ($cek_exist) {

						$cc_type = $this->api_cycle_count_model->get_type($params['cc_code'])->row_array();
						$this->api_cycle_count_model->delPost($params,$cc_type);

						if($cc_type['cc_type'] == 'LOC'){
							$this->api_cycle_count_model->post_loc($params);
						} else{ // if cc_type is SN
							$this->api_cycle_count_model->post_sn($params);
						}

					} else{

						$cc_type = $this->api_cycle_count_model->get_type($params['cc_code'])->row_array();
						if($cc_type['cc_type'] == 'LOC'){
							$this->api_cycle_count_model->post_loc($params);
						} else{ // if cc_type is SN
							$this->api_cycle_count_model->post_sn($params);
						}
						// $this->api_cycle_count_model->post($params);
					}

					$this->db->trans_complete();

					$this->api_cycle_count_model->start($params['cc_code']);

					$data['param']		= $params['kd_unik'];
					$data['status']		= 200;
					$data['message']	= 'Data has been updated.';
					$data['response']	= true;

				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function post_positive_serial()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$params['kd_receiving']		= $this->input->post('receiving_code');
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['kd_batch']			= $this->input->post('batch_code');
				$params['tgl_exp']			= $this->input->post('tgl_exp');
				$params['qty']				= $this->input->post('qty');
				$params['kd_qc']			= $this->input->post('qc_code');
				$params['tgl_in']			= $this->input->post('tgl_in');
				$params['user_name']		= $this->input->post('uname');

				if($params['kd_receiving'] == '' || $params['kd_barang'] == '' || $params['kd_unik'] == ''){
					$data['param']		= $params['kd_unik'];
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;
				} else{
					//$this->api_receiving_model->insert($params);
					$data['param']		= $params['kd_unik'];
					$data['status']		= 200;
					$data['message']	= 'Data has been inserted.';
					$data['response']	= true;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function get_item_picked($cc_code='',$cc_type='',$code=''){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$params = array(
					"cc_code"	=> $cc_code,
					"cc_type"	=> $cc_type,
					"code"		=> $code
				);

				$data['param']		= $code;
				$data['status']		= 200;
				$data['message']	= 'Get data Successfully.';
				$data['response']	= true;
				$data['results']	= $this->api_cycle_count_model->get_item_picked($params);

			} else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}

		echo json_encode($data);	
	}

	public function post_bulk(){

		$post = json_decode($this->input->post('data'), true);

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST' || empty($post)){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				if(!array_key_exists('cc_code', $post) || !array_key_exists('location', $post) || !array_key_exists('serial_number', $post)){
					$data['param']		= '';
					$data['status']		= 401;
					$data['message']	= 'Failed to update data.';
					$data['response']	= false;

				} else{

					if(empty($post['serial_number'])){

						$data['param']		= $post['cc_code'];
						$data['status']		= 401;
						$data['message']	= 'Serial number is empty.';
						$data['response']	= false;

					} else{
						$params['cc_code']			= $post['cc_code'];
						$params['location']			= $post['location'];
						$params['serial_number']	= $post['serial_number'];
						$params['outer_label']		= (array_key_exists('outer_label', $post)) ? $post['outer_label'] : '';

						$cycle = $this->db->get_where('cycle_counts',['code'=>$params['cc_code']])->row_array();
						if($cycle){

							// $this->db
							// 	->select('unique_code')
							// 	->from('cycle_count_scanned')
							// 	->where('cycle_count_id',$cycle['cc_id'])
							// 	->where_in('unique_code',$params['serial_number']);

							// $scanSN = $this->db->get()->result_array();
							// $scannedSN = array();
							// foreach ($scanSN as $sn) {
							// 	$scannedSN[] = $sn['unique_code'];
							// }

							$serialNumber = $params['serial_number'];
							// $serialNumber = array_diff($params['serial_number'],$scannedSN);

							// if(!empty($serialNumber)){

								$now = date('Y-m-d h:i:s');
								$user = $this->db->get_where('users',['user_name'=>$this->input->get_request_header('User', true)])->row_array();
								$location = $this->db->get_where('locations',['name'=>$post['location']])->row_array();

								$getData = array();
								$dataInsert = array();
								// $serialNumber = implode("','", $params['serial_number']);

					            $this->db->select(["ird.unique_code","itm.code AS type1","COALESCE(cctq.qc_id,ird.qc_id) as qc_id", "tgl_exp", "tgl_in", "putaway_time","ird.parent_code","ird.item_id"]);
					            $this->db->from("item_receiving_details ird");
					            $this->db->join('cycle_count_temp_qc cctq','cctq.unique_code=ird.unique_code and cctq.cycle_count_id='.$cycle['cc_id'],'left');
					            $this->db->join("qc", "ird.qc_id = qc.id", "left");
					            $this->db->join("items itm", "ird.item_id = itm.id", "left");
					            $this->db->where_in("ird.unique_code", $serialNumber);
					            $this->db->where('ird.last_qty >',0);
					            $this->db->where("ird.item_id IS NOT NULL",NULL);
					            $this->db->where("ird.location_id IS NOT NULL",NULL);
					            $this->db->where_not_in('ird.location_id',[104]);

					            $getData = $this->db->get()->result_array();

								$this->db->trans_start();

					            $availableSn = array();

					            if($getData){

						            foreach ($getData as $d) {

								        $dataInsert[] = array(  
								            'cycle_count_id'    => $cycle["cc_id"], 
								            'item_id'           => $d["item_id"],
								            'location_id'       => $location["id"], 
								            'unique_code'       => $d['unique_code'],
								            'qty'               => $post['qty'],
								            'time'              => $now,
								            'user_id'           => $user["id"],
											'qc_id' 			=> $d['qc_id'],
											'exp_date' 			=> $d['tgl_exp'],
											'in_date' 			=> $d['tgl_in']
								        );

										$availableSn[] = $d['unique_code'];

									}
								}

								$arrayDif = array_diff($serialNumber,$availableSn);
								foreach ($arrayDif as $snDif) {

									$sn_visual = hex2bin($snDif);
									$itemCode = substr($sn_visual, 0,7);

									$item = $this->db->get_where('items',['code'=>$itemCode])->row_array();
									if($item){

								        $dataInsert[] = array(  
								            'cycle_count_id'    => $cycle["cc_id"], 
								            'item_id'           => $item["id"],
								            'location_id'       => $location["id"], 
								            'unique_code'       => $snDif,
								            'qty'               => $post['qty'],
								            'time'              => $now,
								            'user_id'           => $user["id"],
											'qc_id' 			=> 1,
											'exp_date' 			=> $now,
											'in_date' 			=> $now,
								            'parent_code'       => $params['outer_label']
								        );

							       }

								}

						        $this->db->where('cycle_count_id',$cycle['cc_id']);
						        $this->db->where_in('unique_code',$serialNumber);
						        $delete = $this->db->delete('cycle_count_scanned');
						        $this->db->insert_batch("cycle_count_scanned",$dataInsert);

						        $this->db->set("st_cc", 1);
						        $this->db->where_in("unique_code", $serialNumber);
						        $this->db->update("item_receiving_details");

								$this->db->trans_complete();

								$this->api_cycle_count_model->start($params['cc_code']);

							// }

							$data['param']		= $params['cc_code'];
							$data['status']		= 200;
							$data['message']	= 'Data has been updated.';
							$data['response']	= true;

						}else{

							$data['param']		= $params['cc_code'];
							$data['status']		= 401;
							$data['message']	= 'Failed to update data.';
							$data['response']	= false;

						}

					}
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		} 
		echo json_encode($data);

	}

    public function getOuterLabelItem()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){

                $params['parent_code']     = $this->input->post('outer_label');
                $params['cc_code']   	  = $this->input->post('cc_code');

                if($params['parent_code'] == '' || $params['cc_code'] == ''){
                    $data['param']      = $params['parent_code'];
                    $data['status']     = 400;
                    $data['message']    = 'Outer Label not available';
                    $data['response']   = false;
                } else{

					$datas = $this->api_cycle_count_model->getOuterLabelItem($params);
					if($datas){
						$data['param']		= $params['parent_code'];
						$data['status']		= 200;
						$data['response']	= true;
						$data['message']	= $datas;
					} else{
						$data['param']		= $params['parent_code'];
						$data['status']		= 400;
						$data['response']	= false;
						$data['message']	= 'Outer Label not available';
					}

                }           
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

}

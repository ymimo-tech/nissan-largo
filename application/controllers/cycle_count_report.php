<?php

class cycle_count_report extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Cycle Count Result';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/report/lr-cc-report.js";
		$data['today'] = date('d/m/Y');
		//$data['item'] = $this->inv_transfer_model->getItem();
		//$data['location'] = $this->inv_transfer_model->getLocation();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Cycle Count Report');
        /* END OF INSERT LOG */


        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	//REQUEST
	public function getCcDoc(){
		$result = array();

		$params = array(
			'query'	=> filter_var(trim($this->input->post('query')), FILTER_SANITIZE_STRING)
		);

		$result = $this->cycle_count_report_model->getCcDoc($params);

		echo json_encode($result);
	}

	public function export(){
		$data = array();

		$ccCode = '-- All --';

		$ccId = $this->input->post('cc_id');

		if(!empty($ccId) && $ccId != 'null'){
			$ccCode = $this->cycle_count_report_model->getCcCodeById(filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT));
		}

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'cc_code'	=> $ccCode,
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'status'	=> $this->input->post('status'),
            'warehouse'     => $this->input->post('warehouse')
		);

		$data['page'] = $this->class_name . '/cycle_count_result';
		$data['params'] = $params;
		$data['data'] = $this->cycle_count_report_model->getListRaw($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "cycle_count_report.pdf";

        ini_set('memory_limit', $this->config->item('allocated_memory')); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            10, // margin bottom
            10, // margin header
            10
		); // margin footer

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

	public function getList(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'status'	=> $this->input->post('status'),
            'warehouse'     => $this->input->post('warehouse')
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->cycle_count_report_model->getList($params);

		echo json_encode($result);
	}
}

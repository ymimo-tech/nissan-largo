<?php

class replenishment extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Replenishment';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/report/lr-replenishment.js";
		$data['today'] = date('d/m/Y');

		$this->load_model('item_stok_model');
		$data['item'] = $this->item_stok_model->getItem();
		//$data['item'] = $this->inv_transfer_model->getItem();
		//$data['location'] = $this->inv_transfer_model->getLocation();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Replenishment');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	//REQUEST
	public function export(){
		$data = array();

		$params = array(
			'id_barang'	=> $this->input->post('id_barang')
		);

		$data['page'] = $this->class_name . '/replenishment';
		$data['params'] = $params;
		$data['data'] = $this->replenishment_model->getListRaw($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "replenishment.pdf";

        ini_set('memory_limit', $this->config->item('allocated_memory')); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            10, // margin bottom
            10, // margin header
            10
		); // margin footer

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

    public function export_excel(){
		$data = array();

		$params = array(
			'id_barang'	=> $this->input->post('id_barang')
		);

        $this->load->library('PHPExcel');
		$data = $this->replenishment_model->getListRaw($params);

        $objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Replenishment")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'SKU')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Nama Barang')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Qty Minimal Replenish')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'QTY Lokasi Picking')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Qty Lokasi Putaway')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Qty Total')->getStyle('G1')->getFont()->setBold(true);

        $row = 2;
        $len = count($data);

        if($len > 0){

            for($i = 0; $i < $len; $i++){
                $total = $data[$i]['qty_picking'] + $data[$i]['qty_putaway'];
                $sheet->setCellValueByColumnAndRow(0, $row, ($i + 1));
                $sheet->setCellValueByColumnAndRow(1, $row, $data[$i]['kd_barang']);
                $sheet->setCellValueByColumnAndRow(2, $row, $data[$i]['nama_barang']);
                $sheet->setCellValueByColumnAndRow(3, $row, $data[$i]['minimum_stock']);
                $sheet->setCellValueByColumnAndRow(4, $row, $data[$i]['qty_picking']);
                $sheet->setCellValueByColumnAndRow(5, $row, $data[$i]['qty_putaway']);
                $sheet->setCellValueByColumnAndRow(6, $row, $total); //total?????
                $row++;
            }


        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Replenishment'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

    }

	public function getList(){
		$result = array();

		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
            'warehouse'     => $this->input->post('warehouse')
		);

		$result = $this->replenishment_model->getList($params);

		echo json_encode($result);
	}
}

<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class order_kitting extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

    		/* Otoritas */
    		$this->access_right->check();
    		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Repack Document List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/order_kitting/lr-order_kitting-view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

        $this->include_other_view_data_filter($data);
		$data['today'] = date('d/m/Y');

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Order Kitting');
        /* END OF INSERT LOG */
        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	//ADD
	public function add(){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> New Repack Job';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/order_kitting/lr-order_kitting-addedit.js";

			$this->include_other_view_data($data);

			$data['id'] = '';
			$data['today'] = date('d/m/Y');
			$data['not_usage'] = true;
			$data['doc_number'] = '';

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//EDIT
	public function edit($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Repack Job';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/order_kitting/lr-order_kitting-addedit.js";

			$this->include_other_view_data($data);

			$data['id'] = $id;
			$data['today'] = '';

			// $notUse = $this->order_kitting_model->cek_not_usage_do($id);
			// $nUse = 'false';
			// if($notUse)
			// 	$nUse = 'true';
            //
			// $data['not_usage'] = $nUse;
			$data['doc_number'] = '';

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//DETAIL
	public function detail($id=''){

		if(empty($id)){
			redirect('order_kitting/index');
		}

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Order Kitting Document';
			$data['page'] = $this->class_name . '/detail';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/order_kitting/lr-order_kitting-detail.js";

			$this->include_other_view_data($data);

			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = $id;
			$data['data'] = $this->getDetail($id);
			//$data['status_picking'] = $this->order_kitting_model->checkStatusPicking($id);
			//$data['not_usage'] = $this->order_kitting_model->cek_not_usage_do($id);
			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

    private function include_other_view_data_filter(&$data) {
        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues
    }

	private function include_other_view_data(&$data) {

        /* Get Data Select*/
        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues
		$data['types'] = $this->order_kitting_model->typeOptions();

    }

    public function get_add_item_row() {
        $data['items'] = $this->order_kitting_model->create_option_kitting('-- Choose Item --');
        echo $this->load->view("order_kitting/add_item_row", $data, true);
    }

	public function getDetail($id = false){
		$result = array();

		$params = array(
			'id_order_kitting'	=> $id
		);

		$result = $this->order_kitting_model->getDetail($params);

		return $result;
	}

	//REQUEST
	public function getBatch(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->order_kitting_model->getBatch($params);

		echo json_encode($result);
	}

	public function closeOrderKitting(){
		$result = array();

		$params = array(
			'id_order_kitting'	=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->order_kitting_model->closeOrderKitting($params);

		echo json_encode($result);
	}

	public function finishTallyOrderKitting(){
		$result = array();

		$params = array(
			'id_order_kitting'	=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->order_kitting_model->finishTallyOrderKitting($params);

		echo json_encode($result);
	}

	public function checkExistDoNumber(){
		$result = array();

		$params = array(
			'id_order_kitting'	=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT),
			'kd_order_kitting'	=> filter_var(trim($this->input->post('kd_order_kitting')), FILTER_SANITIZE_STRING)
		);

		$result = $this->order_kitting_model->checkExistDoNumber($params);

		echo json_encode($result);
	}


	public function getDetailItem($id = false){
		$result = array();

		$params = array(
			'id_order_kitting'	=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->order_kitting_model->getDetailItem($params);

		echo json_encode($result);
	}

	public function getDetailItemPicked($id = false){
		$result = array();

		$params = array(
			'id_order_kitting'	=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->order_kitting_model->getDetailItemPicked($params);

		echo json_encode($result);
	}

	public function getDetailItemPickedDetails($id = false){
		$result = array();

		$params = array(
			'id_order_kitting'	=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->order_kitting_model->getDetailItemPickedDetails($params);

		echo json_encode($result);
	}

	public function getTallyDetails($id = false){
		$result = array();

		$params = array(
			'id_order_kitting'	=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->order_kitting_model->getTallyDetails($params);

		echo json_encode($result);
	}

	public function getPickingList(){
		$result = array();

		$params = array(
			'id_order_kitting'		=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT),
			'params'			=> filter_var(trim($this->input->post('params')), FILTER_SANITIZE_STRING),
			'pl_id'				=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT),
			'id_customer'		=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_NUMBER_INT),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'status'			=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_NUMBER_INT)
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->order_kitting_model->getPickingList($params);

		echo json_encode($result);
	}

	public function getShippingList(){
		$result = array();

		$params = array(
			'id_order_kitting'		=> filter_var(trim($this->input->post('id_order_kitting')), FILTER_SANITIZE_NUMBER_INT),
			'params'			=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_STRING),
			'id_shipping'		=> filter_var(trim($this->input->post('id_shipping')), FILTER_SANITIZE_NUMBER_INT),
			'pl_id'				=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'status'			=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_NUMBER_INT)
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->order_kitting_model->getShippingList($params);

		echo json_encode($result);
	}

	public function getList(){
		$result = array();

		$params = array(
			'do_number'		=> filter_var(trim($this->input->post('do_number')), FILTER_SANITIZE_NUMBER_INT),
			'id_customer'	=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_NUMBER_INT),
			'from'			=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'			=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'doc_type'		=> filter_var(trim($this->input->post('doc_type')), FILTER_SANITIZE_STRING),
			'doc_status'	=> filter_var(trim($this->input->post('doc_status')), FILTER_SANITIZE_STRING),
			'warehouse'		=> $this->input->post('warehouse'),
			'className'		=> $this->class_name
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->order_kitting_model->getList($params);

		echo json_encode($result);
	}

	public function getNewOrderKittingCode(){
		$result = array();
		$row = $this->order_kitting_model->get_do_code();

		if($row){

			$result['status'] = 'OK';
			$result['order_kitting_code'] = $row;

		}else{

			$result['status'] = 'OK';
			$result['order_kitting_code'] = $row;

		}

		echo json_encode($result);
	}

	public function getOrderKittingCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->order_kitting_model->getOrderKittingCode($params);

		echo json_encode($result);
	}

	public function getCustomerBySearch(){
		$result = array();

		$params = array(
			'query'	=> filter_var(trim($this->input->post('query')), FILTER_SANITIZE_STRING)
		);

		$result = $this->order_kitting_model->getCustomerBySearch($params);

		echo json_encode($result);
	}

    public function get_data() {
        $process_result;
		$this->load_model('inbound_model');

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            //$order_kitting = $this->order_kitting_model->get_data(array('id_order_kitting' => $id))->row_array();
			$order_kitting = $this->order_kitting_model->get_data($id)->row_array();
            $order_kitting['tanggal_order_kitting'] = date('d/m/Y', strtotime(str_replace('/','-', $order_kitting['tanggal_order_kitting'])));

            $arr_order_kitting_barang = $this->order_kitting_model->get_order_kitting_barang($order_kitting['id_order_kitting'])->result_array();
            $rows = "";

            $data['items'] = $this->order_kitting_model->create_option_kitting("kd_barang, nama_barang", "-- Choose item --");

            foreach ($arr_order_kitting_barang as $order_kitting_barang) {
                //$data['random_string'] = $this->randomidgenerator->generateRandomString(4);
                $data['order_kitting_barang'] = $order_kitting_barang;

				$params = array(
					'id_kitting'	=> $order_kitting_barang['id_kitting']
				);

				// $r = $this->order_kitting_model->getBatch($params);
				// $data['batchs'] = $r['data'];

                $rows .= $this->load->view("order_kitting/add_item_row", $data, true);
            }
            $order_kitting['order_kitting_barang_html'] = $rows;

            $process_result = $order_kitting;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){
        $this->session->set_userdata('current_filter',$this->input->post('periode'));
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $order_kittings = $this->order_kitting_model->data($param)->get();
        $iTotalRecords = $this->order_kitting_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($order_kittings->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_order_kitting,'delete' => $value->id_order_kitting));

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="1">',
                $_REQUEST['start']+$i,
                $value->kd_order_kitting,
                DateTime::createFromFormat('Y-m-d', $value->tanggal_order_kitting)->format('d/m/Y'),
                ucwords($value->status),
                $action
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        $records["current_filter"] = $this->session->userdata('current_filter');

        echo json_encode($records);
    }

    public function proses() {
        $proses_result = array();

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_order_kitting', 'order_kitting Code', 'required|trim');
            $this->form_validation->set_rules('tanggal_order_kitting', 'order_kitting date', 'required|trim');


            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $tanggal = $this->input->post("tanggal_order_kitting");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal))));

                $data = array(
                    'kd_order_kitting' 			=> $this->input->post("kd_order_kitting"),
                    'tanggal_order_kitting' 	=> $date,
					'order_kitting_type_id'		=> $this->input->post('type'),
					'user_id'					=> $this->session->userdata('user_id'),
					'warehouse_id'				=> $this->input->post('warehouse')
             	);

                $data2 = array();
                $arr_id_kitting = $this->input->post("id_kitting");
                $arr_item_quantity = $this->input->post("item_quantity");
				$len = count($arr_id_kitting);

				if(!empty($arr_id_kitting)){
					if($arr_id_kitting){

						for($i = 0; $i < $len; $i++){
							$data2[] = array(
								'jumlah_kitting' => $arr_item_quantity[$i],
								'id_kitting' 	=> $arr_id_kitting[$i]
							);
						}

						// foreach ($arr_id_barang as $key => $value) {

							// if(isset($batch[$value])){

								// $data2[] = array(
									// 'jumlah_barang' => $arr_item_quantity[$key],
									// 'id_barang' 	=> $value,
									// 'batch'			=> $batch[$value]
								// );

							// }else{

								// $data2[] = array(
									// 'jumlah_barang' => $arr_item_quantity[$key],
									// 'id_barang' 	=> $value,
									// 'batch'			=> null
								// );

							// }
						// }
					}
				}

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                $validQty = $this->order_kitting_model->validation_items_qty($data,$data2);
                if($validQty['status']){
	                if (empty($id)) {

	                    if ($this->order_kitting_model->create($data, $data2)) {
	                        $proses_result['add'] = 1;
							$this->order_kitting_model->add_ok_code();

							/* INSERT LOG */
							$this->access_right->activity_logs('add','order_kitting');
							/* END OF INSERT LOG */
	                    }

						$proses_result['success'] = 1;

	                } else {

						$del = $this->input->post("delete");

						if($del)
							$data['delete'] = $del;

						$result = $this->order_kitting_model->update($data, $data2, $id);

						if(isset($result['status'])){
							if($result['status'] == 'ERR'){

								$proses_result['status'] = 'ERR';
								$proses_result['message'] = $result['message'];

							}else{

								if ($this->order_kitting_model->update($data, $data2, $id)) {
									$proses_result['edit'] = 1;

									/* INSERT LOG */
									$this->access_right->activity_logs('edit','order_kitting');
									/* END OF INSERT LOG */
								}

								$proses_result['success'] = 1;
							}
						}else{

							if($del){

								$proses_result['success'] = 1;

							}else{

								if ($this->order_kitting_model->update($data, $data2, $id)) {
									$proses_result['edit'] = 1;

									/* INSERT LOG */
									$this->access_right->activity_logs('edit','order_kitting');
									/* END OF INSERT LOG */
								}

								$proses_result['success'] = 1;

							}

						}
	                }
	            }else{

					$proses_result['status'] = 'ERR';
					$proses_result['message'] = $validQty['message'];

	            }
                //$proses_result['success'] = 1;
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function save_new_customer() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $proses_result['success'] = 1;
            $this->form_validation->set_rules('customer_name', 'order_kitting Code', 'required|trim');
            $this->form_validation->set_rules('phone', 'order_kitting Code', 'required|trim');

            if ($this->form_validation->run()) {
                $data = array(
                    'customer_name' => $this->input->post("customer_name"),
                    'phone' => $this->input->post("phone")
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($this->order_kitting_model->create_customer($data)) {
                    $proses_result['add'] = 1;
                    /* INSERT LOG */
                    $this->access_right->activity_logs('add','Order Kitting - Creating new customer');
                    /* END OF INSERT LOG */
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            $result = $this->order_kitting_model->delete($id);
            $result['success'] = 1;
        } else {
            $result['no_delete'] = 1;
        }
        echo json_encode($result);
    }

    public function load_customers() {
        $customer_options = $this->order_kitting_model->create_options("m_customer", "id_customer", "customer_name", "-- Choose customer --");
        $html = "";
        foreach ($customer_options as $key => $option) {
            $html .= "<option value='$key'>$option</option>";
        }
        echo $html;
    }

    public function print_tally(){
		$data = array();

		$params = array(
			'id_order_kitting'	=> $this->input->post('id_receiving')
		);

		$rcv = $this->order_kitting_model->get_data($params['id_order_kitting'])->row_array();
        $rcv['tanggal_receiving'] = DateTime::createFromFormat('Y-m-d', $rcv['tanggal_order_kitting'])->format('d/m/Y');

		$data['page'] = $this->class_name . '/tally_report';
        $data['receiving'] = $rcv;
        $data['temp'] = 112;
        $data['temp2'] = $this->order_kitting_model->getProduct($params);
		$data['items'] = $this->order_kitting_model->getDetailItemKitting($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "Order-repack ".$rcv['kd_order_kitting'] ." ".$rcv['tanggal_receiving'].".pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		//START TALLY
		//$this->receiving_model->startTally($params);

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }
}

/* End of file referensi_barang.php */
/* Location: ./application/controllers/referensi_barang.php */

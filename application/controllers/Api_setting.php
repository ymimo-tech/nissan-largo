<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_setting extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load->library("randomidgenerator");
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function post_server()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$params['server'] 	= $this->input->post('server_address');

			$data['param']		= $params['server'];
			$data['status']		= 200;
			$data['message']	= 'Device already connected to server.';
			$data['response']	= true;
		}
		echo json_encode($data);
	}

	public function login()
	{

		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$this->expired();
			$this->checkOnlineUser();

			$params['uname'] 	= $this->input->post('username');
			$params['upass'] 	= md5($this->input->post('password'));
			$params['app_version'] 	= $this->input->post('app_version');

			// $checkVersion = $this->db->get_where('update_handheld',['version'=>$params['app_version']])->row_array();
			$checkVersion = true;

			if($checkVersion){

				$retrieve			= $this->api_setting_model->user_login($params)->row_array();

				if($retrieve){

					// if($retrieve['handheld_session_code'] == NULL){

						if($params['uname'] == "dev"){
							$session = "dev";
						}else{
							$session 			= $this->randomidgenerator->createSessionToken();
						}

						$this->api_setting_model->user_session($retrieve['user_id'], $session);

						$data['param']		= $params['uname'];
						$data['status']		= 200;
						$data['message']	= 'Login successfully.';
						$data['response']	= true;
						$data['results']	= $this->api_setting_model->user_login_detail($params['uname'], $session)->result_array();

	/*				}else{

						$data['param']		= $params['uname'];
						$data['status']		= 400;
						$data['message']	= 'User sedang online.';
						$data['response']	= false;

					}
	*/
				} else{

					$data['param']		= $params['uname'];
					$data['status']		= 401;
					$data['message']	= 'Login failed.';
					$data['response']	= false;

				}
			}else{

				$data['param']		= $params['app_version'];
				$data['status']		= 400;
				$data['message']	= 'Silahkan update ke versi terbaru.';
				$data['response']	= false;

			}

		}

		echo json_encode($data);

	}

	public function logout(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;

		} else{

			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$params['uname'] = $this->input->get_request_header('User', true);
				$params['handheld_session_code'] = $this->input->get_request_header('Authorization', true);

				$logout = $this->api_setting_model->user_logout($params);

				if($logout){

					$data['param']		= $params['uname'];
					$data['status']		= 200;
					$data['message']	= 'Logout successfully.';
					$data['response']	= true;

				} else{

					$data['param']		= $params['uname'];
					$data['status']		= 400;
					$data['message']	= 'Logout failed.';
					$data['response']	= false;

				}

			} else{

				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;

			}
		}
		echo json_encode($data);
	}

	public function get_time(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$data['param']		= 'SERVER TIME';
			$data['status']		= 200;
			$data['message']	= 'Getting server time.';
			$data['response']	= true;
			$data['time']		= date("Y-m-d H:i:s");
		}
		echo json_encode($data);
	}

	public function get_update($customer = '', $version = ''){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $customer == '' || $version == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){
				$data = $this->api_setting_model->get_update($customer)->row_array();
				if($data['version'] == $version){
					$data['param']		= 'largo-' . $data['version'] . '.CAB';
					$data['status']		= 401;
					$data['message']	= 'There are no software update available.';
					$data['response']	= false;
				} else{
					$data['param']		= 'largo-' . $data['version'] . '.CAB';
					$data['status']		= 200;
					$data['message']	= 'A software update is available.';
					$data['response']	= true;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function expired(){

		if($this->license->is_expired()){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Masa aktif anda telah habis';
			$data['response']	= false;

			echo json_encode($data);
			exit;

		}
	}

	private function checkOnlineUser(){

		$onlineUser = $this->api_setting_model->onlineUser();

		$limitUser = $this->license->data()->limit_user;

		if($onlineUser >= $limitUser){

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= $onlineUser;
			$data['response']	= false;

			echo json_encode($data);
			exit;
		}

	}

	private function checkAppVer() {
		$appVersion = $this->api_setting_model->checkAppVer();

		$data['param'] = null;
		$data['status'] = 200;
		$data['message'] = $appVersion;
		$data['response'] = true;

		echo json_encode($data);
		exit;
	}

	private function updateAppVer() {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST') {
			$data['param'] = null;
			$data['status'] = 400;
			$data['message'] = 'Bad request.';
			$data['response'] = false;
		} else {
			$auth = $this->api_setting_model->user_authentication();
			if($auth) {
				$update = $this->api_setting_model->updateAppVer($this->input->post('version'))->row_array();

				if($update) {
					$data['param'] = null;
					$data['status'] = 200;
					$data['message'] = 'Update success.';
					$data['response'] = true;
				}
			} else {
				$data['param'] = null;
				$data['status'] = 401;
				$data['message'] = 'Unauthorized user.';
				$data['response'] = false;
			}
		}
	}

	public function getOuterLabelFromPL($pickingId){

		$this->db
			->select(['itm.code as item_code','parent_code',"COALESCE(count(*),0) as qty"])
			->from('item_receiving_details ird')
			->join('items itm','itm.id=ird.item_id','left')
			->where('picking_id',$pickingId)
			->group_by('parent_code, itm.code');

		$data = $this->db->get()->result_array();

		foreach ($data as $d) {
			echo $d['item_code'].", ".$d['parent_code'].", ".$d['qty']."<br/>";
		}

	}

	public function getOuterLabelFromReceiving($receivingId){

		$sql = "select parent_code from item_receiving_details where item_receiving_id IN (select id from item_receiving where receiving_id=$receivingId) and putaway_time IS NULL group by parent_code";

		$data = $this->db->query($sql)->result_array();

		foreach ($data as $d) {
			echo $d['parent_code']."<br/>";
		}

	}
}

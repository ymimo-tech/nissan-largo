<?php

/**
 * Description of item_stok
 *
 * @author SANGGRA HANDI
 */
class item_stok_by_location extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        /* Otoritas */
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model','referensi_satuan_model','referensi_kategori_model'));
    }

    public function index() {
        $data['title'] = 'Active Inventory By Location';
        $data['subTitle'] = '<i class="fa fa-th-list"></i> Locations List';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;

        $this->include_required_file($data);
		$data['jsPage'][] = "assets/custom/js/share.js";
        $data['jsPage'][] = "assets/pages/scripts/item_stok_by_location/view.js";

        /* Otoritas Tombol tambah */

            $data['button_group'] = array();


        //=========tombol cetak excel==========
        $base_url_images = base_url() . 'images';
        $base_url = base_url() . $this->class_name;
        if ($this->access_right->otoritas('print')) {
            $data['print_group'] = array(
                anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $base_url . '/excel')),
                anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel-format2', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel format 2', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $base_url . '/excel_format2'))
            );


        }
        //-------------------------------------
        $data['status'] = $this->item_stok_by_location_model->getStatus();
  			$data['category'] = $this->item_stok_by_location_model->getArea();
            //   dd($data['category']);
  			$data['location'] = $this->item_stok_by_location_model->getLocation();
  			$data['location_type'] = $this->item_stok_by_location_model->getLocationType();
  			$data['item'] = $this->item_stok_by_location_model->getItem();

        /* INSERT LOG */
        $this->access_right->activity_logs('view','Master Item Stock');
        /* END OF INSERT LOG */

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $this->load->view('template', $data);
    }

	public function getListQtyDataTable(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->item_stok_by_location_model->getListQtyDataTable($params);

		echo json_encode($result);
	}

	public function export(){
		$this->load->library('PHPExcel');

		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
			'id_satuan'		=> $this->input->post('id_satuan'),
			'id_qc'			=> $this->input->post('id_qc'),
			'id_kategori'	=> $this->input->post('id_kategori'),
			'loc_id'		=> $this->input->post('loc_id'),
			'loc_type'		=> $this->input->post('loc_type'),
      'warehouse'     => $this->input->post('warehouse'),
			'params'		=> $this->input->post('params')
		);

		foreach($params as $k => $v){
			$params[$k] = str_replace('null','',$v);
		}

		$result = $this->item_stok_by_location_model->getListRaw($params);
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Active-Inventory")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Item Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Item Name')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Category')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'UoM')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Location')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Location Type')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Qty')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'Minimum Replenishment Stock')->getStyle('H1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['kd_barang']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['nama_barang']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['nama_kategori']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['nama_satuan']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['loc_name']);
			$sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['loc_type_all']);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['qty']);
			$sheet->setCellValueByColumnAndRow(7, $row, $result[$i]['minimum_stock']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Active-Inventory'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

    public function export_format2(){
        $this->load->library('PHPExcel');

        $params = array(
            'id_barang'     => $this->input->post('id_barang'),
            'id_satuan'     => $this->input->post('id_satuan'),
            'id_qc'         => $this->input->post('id_qc'),
            'id_kategori'   => $this->input->post('id_kategori'),
            'loc_id'        => $this->input->post('loc_id'),
            'loc_type'      => $this->input->post('loc_type'),
            'warehouse'     => $this->input->post('warehouse'),
            'params'        => $this->input->post('params')
        );

        foreach($params as $k => $v){
            $params[$k] = str_replace('null','',$v);
        }

        $result = $this->item_stok_by_location_model->getListRawFormat2($params);
        $iLen = count($result);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Active-Inventory")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'Item Code')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, 'Item Name')->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 1, 'Category')->getStyle('C1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 1, 'UoM')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, 'Location')->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 1, 'Location Type')->getStyle('F1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(6, 1, 'Qty')->getStyle('G1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(7, 1, 'Minimum Replenishment Stock')->getStyle('H1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['kd_barang']);
            $sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['nama_barang']);
            $sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['nama_kategori']);
            $sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['nama_satuan']);
            $sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['loc_name']);
            $sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['loc_type_all']);
            $sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['qty']);
            $sheet->setCellValueByColumnAndRow(7, $row, $result[$i]['minimum_stock']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Active-Inventory'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

	public function getList(){
		$result = array();

		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
			'id_kategori'	=> $this->input->post('id_kategori'),
			'loc_id'		=> $this->input->post('loc_id'),
			'loc_type'		=> $this->input->post('loc_type'),
      'warehouse'     => $this->input->post('warehouse'),
			'params'		=> $this->input->post('params')
		);

        $result = $this->item_stok_by_location_model->getList($params);
        // dd($result);

		echo json_encode($result);
	}

	public function get_list_detail(){
		$result = array();

		$params = array(
			'location_id'	=> filter_var(trim($this->input->post('location_id')), FILTER_SANITIZE_NUMBER_INT),
      'warehouse' => $this->input->post('warehouse'),
			'item_id'	=> $this->input->post('item_id')
        );

		$result = $this->item_stok_by_location_model->getDetailList($params);

		echo json_encode($result);
	}

    public function get_list_detail_item(){
        $result = array();

        $params = array(
            'location_id'   => filter_var(trim($this->input->post('location_id')), FILTER_SANITIZE_NUMBER_INT),
            'warehouse' => $this->input->post('warehouse'),
            'params'    => $this->input->post('params')
        );



        $result = $this->item_stok_by_location_model->getDetailListItem($params);

        echo json_encode($result);
    }

    public function option(){

        $query = $this->input->post('query');

        $locations = $this->db
                        ->select('id, code, name')
                        ->from('items')
                        // ->where('lower(name) like', strtolower($query))
                        ->like('lower(code)',strtolower($query))
                        ->get()
                        ->result_array();
        $data= array();

        foreach ($locations as $loc) {
            $data['results'][] = array(
                'id' => $loc['id'],
                'name' => $loc['code'],
                'code'  => $loc['code']
            );
        }

        echo json_encode($data);
    }

    public function get_list_detail_item_export(){
        $result = array();

        $params = array(
            'location_id' => filter_var(trim($this->input->post('location_id')), FILTER_SANITIZE_NUMBER_INT),
            'warehouse' => $this->input->post('warehouse'),
            'params'    => $this->input->post('params')
        );

        $result = $this->item_stok_by_location_model->getDetailListItemExport($params);
        $iLen = count($result);

        $this->load->library('PHPExcel');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Active-Inventory-By-Location")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, 'SKU')->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 1, 'Item Name')->getStyle('C1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 1, 'Qty')->getStyle('D1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i][0]);
            $sheet->setCellValueByColumnAndRow(1, $row, $result[$i][1]);
            $sheet->setCellValueByColumnAndRow(2, $row, $result[$i][2]);
            $sheet->setCellValueByColumnAndRow(3, $row, $result[$i][3]);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Active-Inventory By Location'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

        echo json_encode($result);
    }

    public function get_list_detail_export(){
      dd('here');
        $result = array();

        $params = array(
            'location_id' => filter_var(trim($this->input->post('location_id')), FILTER_SANITIZE_NUMBER_INT),
            'warehouse' => $this->input->post('warehouse'),
            'params'    => $this->input->post('params')
        );

        $result = $this->item_stok_by_location_model->getDetailListExport($params);
        $iLen = count($result);

        $this->load->library('PHPExcel');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Active-Inventory-By-Location")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'No')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, 'SKU')->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 1, 'Serial Number')->getStyle('C1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 1, 'License Plate')->getStyle('D1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 1, 'Qty')->getStyle('E1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 1, 'Doc Reference')->getStyle('F1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(6, 1, 'Exp Date')->getStyle('G1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(7, 1, 'In Date')->getStyle('H1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(8, 1, 'Item Status')->getStyle('I1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i][0]);
            $sheet->setCellValueByColumnAndRow(1, $row, $result[$i][1]);
            $sheet->setCellValueByColumnAndRow(2, $row, $result[$i][2]);
            $sheet->setCellValueByColumnAndRow(3, $row, $result[$i][3]);
            $sheet->setCellValueByColumnAndRow(4, $row, $result[$i][4]);
            $sheet->setCellValueByColumnAndRow(5, $row, $result[$i][5]);
            $sheet->setCellValueByColumnAndRow(6, $row, $result[$i][6]);
            $sheet->setCellValueByColumnAndRow(7, $row, $result[$i][7]);
            $sheet->setCellValueByColumnAndRow(8, $row, $result[$i][8]);
            $sheet->setCellValueByColumnAndRow(9, $row, $result[$i][9]);

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Active-Inventory By Location'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

        echo json_encode($result);
    }

    //DETAIL
    public function detail($id){
        if ($this->access_right->otoritas('view')) {
            $data['title'] = 'LOCATION DETAIL';
            $data['subTitle'] = '<i class="fa fa-th-list"></i> Location Info';
            $data['page'] = $this->class_name . '/detail';

            $this->include_required_file($data);

            $data['jsPage'][] = "assets/pages/scripts/item_stok_by_location/detail.js";

            $data['id'] = $id;

            $this->load_model('referensi_lokasi_model');
            $data['location'] = $this->referensi_lokasi_model->get_by_id($id)->row();

            /* INSERT LOG */
            $this->access_right->activity_logs('view','Item stock detail');
            /* END OF INSERT LOG */

            $this->load->view('template', $data);
        }
    }

    private function include_required_file(&$data){
        $data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['csss'][] = "assets/global/plugins/select2/css/select2.min.css";
        $data['csss'][] = "assets/global/plugins/select2/css/select2-bootstrap.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
        $data['jsPage'][] = 'assets/global/plugins/select2/js/select2.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
    }

	public function getStockCard(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT),
            'warehouse' => $this->input->post('warehouse'),
			'start'		=> filter_var(trim($this->input->post('start')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->item_stok_by_location_model->getStockCard($params);

		echo json_encode($result);
	}

	public function printZpl(){
		$result = array();
		$this->load->library('mimoclientprint');

		$data = $this->input->post('sn');
		$len = count($data);
		$cmd = '';

		for($i = 0; $i < $len; $i++) {
			$sn = $data[$i];
			$snInfo = substr($sn, 0, 6) . " " . substr($sn, 6);

			$tpl = file_get_contents(APPPATH . 'views/label/label.tpl');

			$tpl = str_replace('{{ sn }}', $sn, $tpl);
			$tpl = str_replace('{{ sn_info }}', $snInfo, $tpl);

			$cmd .= $tpl;

		}

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);

		echo json_encode($result);
	}

    public function print_single_label($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $title = 'Barcode Label Printing';
            $data['title'] = '<i class="icon-edit"></i> ' . $title;
            $data['form_action'] = $this->class_name . '/proses';
            $data['qty_print'] = $this->item_stok_by_location_model->get_detail_by_id($id)->kd_unik;
            $data['id'] = $id;
            $this->load->view($this->class_name . '/form_barcode',$data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function initPrinting(){
        $this->load->helper('printing_label_helper');

        $ci =& get_instance();
        $cmd = '';
        $nc = $ci->input->post('nc');
        $id_rcv = $ci->input->post('id_rcv');
        $kd_unik = $ci->input->post('qty_print');

        $cmd .="
            ^XA";
        $cmd .= "
            ^MMA
            ^PW264
            ^LL0144
            ^LS0
            ^BY2,3,73^FT41,92^BCN,,Y,N
            ^FD>;".$kd_unik."^FS
            ^PQ1,0,1,Y
            ~JSB
            ^XZ";


                    //echo $i,'-'.$serial_number.'<br>';
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>

            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>
                <br />
            </div>

            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>



            </div>

        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>

            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc).
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D

            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.
            </div>
        </fieldset>
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }

    public function get_detail_item_stok($id_barang) {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Detail Item Stok';
        $data['page'] = $this->class_name . '/get_detail_item_stok';
        $data['data_source'] = base_url($this->class_name . '/load_detail_item_stok');

        //=========tombol cetak excel==========
        $base_url_images = base_url() . 'images';
        $base_url = base_url() . $this->class_name;
        if ($this->access_right->otoritas('print')) {
            $data['print_group'] = array(
                anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel-'.$id_barang, 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel', 'onclick' => 'do_print(this.id, \'#ffilter_detail_'.$id_barang.'\')', 'data-source' => $base_url . '/excel_detail/'.$id_barang))
            );
        }
        //-------------------------------------
        $this->load_model('referensi_satuan_model');
        $data['options_satuan'] = $this->referensi_satuan_model->options();

         $data['id_barang'] = $id_barang;
        /* INSERT LOG */
        $this->access_right->activity_logs('view','detail item_stok');
        /* END OF INSERT LOG */
        $this->load->view('item_stok/detail_item_stok2', $data);
    }

    public function stockcard_to_excel(){

        $allQty = $this->input->post('all_qty');
        $params = array(
            'id_barang' => filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_STRING)
        );

        $this->load->library('PHPExcel');
        $data = $this->item_stok_by_location_model->getStockCard($params);
        $itemName = $data['item_name'] ;
        $itemCode = $data['item_code'] ;

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle('Stock Card '.$itemName)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->setCellValueByColumnAndRow(0, 1, 'SKU')->getStyle('A1')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 1, $itemCode.' - '.$itemName)->getStyle('B1')->getFont()->setBold(true);


        $sheet->setCellValueByColumnAndRow(0, 5, 'Date')->getStyle('A5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(1, 5, 'In')->getStyle('B5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(2, 5, 'Out')->getStyle('C5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(3, 5, 'Balance')->getStyle('D5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(4, 5, 'By')->getStyle('E5')->getFont()->setBold(true);
        $sheet->setCellValueByColumnAndRow(5, 5, 'Ref Doc')->getStyle('F5')->getFont()->setBold(true);

        $row = 6;

        $data = $data['data'];

        $total = $allQty;

        foreach($data as $k){

            $in = ($k['type'] == 'IN') ? $k['qty'] : 0;
            $out = ($k['type'] == 'OUT') ? $k['qty'] : 0;

            $sheet->setCellValueByColumnAndRow(0, $row, $k['date']);
            $sheet->setCellValueByColumnAndRow(1, $row, $in);
            $sheet->setCellValueByColumnAndRow(2, $row, $out);
            $sheet->setCellValueByColumnAndRow(3, $row, $total);
            $sheet->setCellValueByColumnAndRow(4, $row, $k['user_name']);
            $sheet->setCellValueByColumnAndRow(5, $row, $k['doc']);

            $total = ($total - $k['qty']);

            $row++;

        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();

        // Sending headers to force the user to download the file

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Stock Card '.$itemCode.' - '. date('Ymd') .'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

    public function get_location_list(){

        $params = array(
            "keyword"  => $this->input->post('term'),
            "page"      => $this->input->post('page')
        );


        $data  = $this->item_stok_by_location_model->get_location_list($params);

        echo json_encode($data);

    }

}

/* End of file referensi_hari.php */
/* Location: ./application/controllers/referensi_hari.php */

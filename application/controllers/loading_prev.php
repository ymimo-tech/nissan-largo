<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class loading extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");

		$this->load_model('api_integration_model');
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Loading List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/loading/lr-loading-view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

        //$this->include_other_view_data_filter($data);
		$data['today'] = date('d/m/Y');
		$data['status'] = $this->loading_model->statusOptions();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Outbound');
        /* END OF INSERT LOG */

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        $this->load->view('template', $data);
    }

	//ADD
	public function add(){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> New Loading';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/loading/lr-loading-addedit.js";

			$data['id'] = '';
			// $data['shipping_code'] = $this->loading_model->getLoadingCodeIncrement();
			$data['shipping_code'] = 'SJ-CL-' . strtotime(date("Y-m-d h:i:sa")) . rand(10,99);
			$data['today'] = date('d/m/Y');
			$data['doc_number'] = $this->loading_model->getPickingDocument(array());
			$data['not_usage'] = 'true';

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);
		}
	}

	//EDIT
	public function edit($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Loading';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/loading/lr-loading-addedit.js";

			$data['id'] = $id;
			$data['shipping_code'] = '';
			$data['today'] = '';

			$params = array(
				'shipping_id'	=> $id
			);

			$data['doc_number'] = $this->loading_model->getPickingDocument($params);

			/*
			$notUse = $this->picking_list_model->cek_not_usage_do($id);
			$nUse = 'false';
			if($notUse)
				$nUse = 'true';
			*/

			$data['not_usage'] = 'true';
			//$data['doc_number'] = $this->picking_list_model->getOutboundCodeList();

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//DETAIL
	public function detail($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Document Info';
			$data['page'] = $this->class_name . '/detail';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/loading/lr-loading-detail.js";


			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = $id;
			$data['data'] = $this->getDetail($id)->row_array();
			$data['datas'] = $this->getDetail($id)->result_array();
			$data['not_usage'] = 'true';

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

    private function include_other_view_data_filter(&$data) {
        $data['outbound_docs'] = $this->picking_list_model->create_options("m_outbound_document", "id_outbound_document", "outbound_document_name", "-- All --");
		$data['doc_status'] = $this->picking_list_model->create_options("m_status_outbound", "id_status_outbound", "nama_status", "-- All --");
    }

	public function getDetail($id = false){
		$result = array();

		$params = array(
			'shipping_id'	=> $id
		);

		$result = $this->loading_model->getDetail($params);

		return $result;
	}

	//REQUEST
	public function printPlate(){
		$result = array();
		$this->load->library('mimoclientprint');
		$this->load->helper('print_plate_helper');

		$plate = trim($this->input->post('plate'));
		$cmd = '';
		$cmd .= getPrintTemplate($plate);

		$params = array(
			'printerCommands'	=> $cmd
		);

		$result = $this->mimoclientprint->printIt($params);

		echo json_encode($result);
	}

	public function printPlate1(){

		$result = array();
		$this->load->library('mimoclientprint');
		//$this->load->helper('print_helper');

		$plate = trim($this->input->post('plate'));
		$barcode = $plate;

		if(strpos($plate, ' ') > 0){
			//BE >51234>6 ASD
			$barcode = '';

			$exp = explode(' ', $plate);
			$len = count($exp);

			$j = 5;
			for($i = 0; $i < $len; $i++){

				if($i < ($len - 1)){

					if($i % 2 == 0){
						$exp[$i] = $exp[$i] . ' >' . $j;
					}else{
						$exp[$i] = $exp[$i] . '>' . $j . ' ';
					}

				}

				$barcode .= $exp[$i];

				$j++;
			}
		}

		$tpl = file_get_contents(APPPATH . 'views/label/label.plate.tpl');

		$tpl = str_replace('{{ barcode }}', $barcode, $tpl);
		$tpl = str_replace('{{ text }}', $plate, $tpl);

		$params = array(
			'printerCommands'	=> $tpl
		);

		$result = $this->mimoclientprint->printIt($params);

		echo json_encode($result);
	}

	public function closeLoading(){
		$result = array();

		$params = array(
			'shipping_id'		=> filter_var(trim($this->input->post('shipping_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->loading_model->closeLoading($params);

		echo json_encode($result);
	}

	public function getScannedList(){
		$result = array();

		$params = array(
			'shipping_id'	=> filter_var($this->input->post('shipping_id'), FILTER_SANITIZE_NUMBER_INT),
			'id_barang'		=> filter_var($this->input->post('id_barang'), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->loading_model->getScannedList($params);

		echo json_encode($result);
	}

	public function getDetailItem(){
		$result = array();

		$params = array(
			'shipping_id'	=> filter_var($this->input->post('shipping_id'), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->loading_model->getDetailItem($params);

		echo json_encode($result);
	}

	public function getDetailPicking(){
		$result = array();

		$params = array(
			'shipping_id'	=> filter_var($this->input->post('shipping_id'), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->loading_model->getDetailPicking($params);

		echo json_encode($result);
	}

	public function getDetailPacking(){
		$result = array();
		

		$params = array(
			'shipping_id'	=> filter_var($this->input->post('shipping_id'), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->loading_model->getDetailPacking($params);

		echo json_encode($result);
	}

	public function getItemPacking(){
		$result = array();
		
		$params = array(
					'shipping_id' => $this->input->post('shipping_id'),
					'packing_id' => $this->input->post('packing_id'),
		);

		$result = $this->loading_model->getDetailPacking($params, 'item_packing');

		echo json_encode($result);
	}

	public function checkDoNumber(){
		$result = array();

		$params = array(
			'do_number'		=> filter_var($this->input->post('do_number'), FILTER_SANITIZE_STRING),
			'shipping_id'	=> filter_var($this->input->post('shipping_id'), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->loading_model->checkDoNumber($params);

		echo json_encode($result);
	}

	public function print_delivery_order(){
		$data = array();

		$params = array(
			'do_number'		=> $this->input->post('do_number'),
			'shipping_id'	=> $this->input->post('shipping_id')
		);

		$this->loading_model->saveDo($params);

		$loading = $this->loading_model->getEdit($params);

		$data['page'] = $this->class_name . '/delivery_order';
        $data['loading'] = $loading;
		$data['items'] = $this->loading_model->getItems($params);
		$data['do_number'] = $params['do_number'];

        $html = $this->load->view($data['page'], $data, true);
        $path = "delivery_order.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

    public function print_surat_jalan(){
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('enc');
		$data = array();

		$params = array(
			'shipping_id'	=> $this->input->post('shipping_id')
		);

		$data['data'] = $this->loading_model->getLoadingPack($params);

		$data['page'] =  $this->class_name .'/surat_jalan';

        $html = $this->load->view($data['page'], $data, true);
        $path = "surat_jalan.pdf";

        ini_set('memory_limit','128M');

        $this->load->library('pdf');
        $pdf = $this->pdf->load();

        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->Output($path, 'I'); // save to file because we can

    }

    public function print_packing_document(){
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('enc');
		$data = array();

		$params = array(
			'shipping_id'	=> $this->input->post('shipping_id')
		);

		$data['data'] = $this->loading_model->getLoadingPackNew($params);
		$data['truck'] = $this->loading_model->getLoadingTruckNew($params);

		$data['page'] =  $this->class_name .'/packing_document';

        $html = $this->load->view($data['page'], $data, true);
        $path = "surat_jalan.pdf";

        ini_set('memory_limit','32M');

        $this->load->library('pdf');
        $pdf = $this->pdf->load();

        $pdf->AddPage('L');
        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->Output($path, 'I'); // save to file because we can

    }

	public function print_manifest(){
		$data = array();

		$params = array(
			'shipping_id'	=> $this->input->post('shipping_id')
		);

		$loading = $this->loading_model->getEdit($params);

		$data['page'] = $this->class_name . '/manifest_report';
        $data['loading'] = $loading;
		$data['items'] = $this->loading_model->getItems($params);
		$data['packing_lists'] = $this->loading_model->manifestPacking($params, 'manifest');


        $html = $this->load->view($data['page'], $data, true);
        $path = "manifest_report.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

	public function getDoNUmber(){
		$result = array();

		$params = array(
			'shipping_id'	=> filter_var(trim($this->input->post('shipping_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->loading_model->getDoNumber($params);

        echo json_encode($result);
	}

	public function getPickingNumber(){
		$result = array();
        $params = array();

		$result['data'] = $this->loading_model->getPickingDocument($params);
		$result['status'] = 'OK';

        echo json_encode($result);
	}

	public function delete(){
		$result = array();
        if ($this->access_right->otoritas('delete', true)) {
            $params = array(
				'shipping_id'	=> filter_var($this->input->post('id'), FILTER_SANITIZE_NUMBER_INT)
			);

			$result = $this->loading_model->delete($params);
            $result['success'] = 1;
        } else {
            $result['no_delete'] = 1;
        }

        echo json_encode($result);
	}

	public function getEdit(){
		$result = array();

		$params = array(
			'shipping_id'	=> filter_var(trim($this->input->post('id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->loading_model->getEdit($params);

		/*
		$outboundDocument = $this->loading_model->getOutboundDocument($params);
		$rows = "";

		foreach ($outboundDocument as $outbound){
			$data['doc'] = array();
			$data['doc'][] = $outbound;
			$data['edit'] = true;
			$rows .= $this->load->view("loading/add_item_row", $data, true);
		}

		$result['outbound_document_html'] = $rows;
		*/

		echo json_encode($result);
	}

	public function get_add_item_row() {
        //$data['doc'] = $this->loading_model->getOutboundNotInShipping();

		$params = array(
			'id_outbound' =>  filter_var($this->input->post('id_outbound'), FILTER_SANITIZE_NUMBER_INT)
		);

		$data['doc'] = $this->loading_model->getPickingDocument($params);
        $data['random_string'] = $this->randomidgenerator->generateRandomString(4);
        echo $this->load->view("loading/add_item_row", $data, true);
    }

	public function getLoadingCodeIncrement(){
		$result = array();

		$params = array();

		$row = $this->loading_model->getLoadingCodeIncrement($params);

		$result['status'] = 'OK';
		$result['loading_code'] = $row;

		echo json_encode($result);
	}

	public function getLoadingCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->loading_model->getLoadingCode($params);

		echo json_encode($result);
	}

	 public function proses() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('loading_code', 'Loading Code', 'required|trim');
            $this->form_validation->set_rules('tanggal_loading', 'loading date', 'required|trim');
            $this->form_validation->set_rules('driver', 'driver', 'required|trim');
            // $this->form_validation->set_rules('plate', 'plate', 'required|trim');
            // $this->form_validation->set_rules('barcode', 'barcode', 'required|trim');


            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $tanggal = $this->input->post("tanggal_loading");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal))));

                $data = array(
                    'shipping_code' 		=> $this->input->post("loading_code"),
                    'shipping_date' 		=> $date,
                    'driver_name'			=> $this->input->post("driver"),
                    'license_plate'		 	=> $this->input->post("plate"),
                    'barcode'				=> $this->input->post('barcode'),
					'user_id'				=> $this->session->userdata('user_id')
             	);

				$idOutbound = $this->input->post('outbound');

                //$data2 = array();
				$data2 = '';
                $picking = $this->input->post("picking");

				if(!empty($picking)){
					$data2 = $picking;
				}

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                $this->db->trans_start();

                if (empty($id)) {
                    if ($this->loading_model->create($data, $idOutbound, $data2)) {
                        $proses_result['add'] = 1;
						$this->loading_model->add_shipping_code();

						/* INSERT LOG */
						$this->access_right->activity_logs('add','loading');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->loading_model->update($data, $idOutbound,  $data2, $id)) {
                        $proses_result['edit'] = 1;

						/* INSERT LOG */
						$this->access_right->activity_logs('edit','loading');
						/* END OF INSERT LOG */
                    }
                }

                $this->db->trans_complete();

				$this->api_integration_model->repost_gi($picking);

                $proses_result['success'] = 1;
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

	public function printOuterLabel(){

		$result = array();
		$this->load->library('mimoclientprint');

		$shippingId= $this->input->post('shipping_id');

		$cmd ="";

		$outerLabel = $this->loading_model->getOuterLabelbyShipping($this->input->post('shipping_id'));

		if($outerLabel){

			foreach ($outerLabel as $ol) {

		        $tpl = file_get_contents(APPPATH . 'views/label/label_asn_zulu.tpl');

				if($ol['parent_code'] == ""){
					$ol['parent_code'] = "NO OL";
				}

		        $tpl = str_replace('{LP_Barcode}', $ol['parent_code'], $tpl);
		        $tpl = str_replace('{LP_Value}', $ol['parent_code'], $tpl);


		        $sn = $this->loading_model->getSNByOuterLabel($shippingId,$ol['parent_code']);
		        $len = count($sn);

	            $items=array();

	            foreach ($sn as $d) {
	                if(array_key_exists($d['code'], $items)){

	                    $items[$d['code']]['code'] = $d['code'];
	                    $items[$d['code']]['qty'] = $items[$d['code']]['qty']+1;

	                }else{

	                    $items[$d['code']]['code'] = $d['code'];
	                    $items[$d['code']]['qty'] = 1;

	                }
	            }

	            $i=1;
	            foreach ($items as $item) {
	                $tpl = str_replace('{IQ'.$i.'}', $item['qty'], $tpl);
	                $tpl = str_replace('{LP_I'.$i.'}', $item['code'], $tpl);
	                $i++;
	            }

	            for ($i=1; $i < 7; $i++) {
	                $tpl = str_replace('{IQ'.$i.'}', '', $tpl);
	                $tpl = str_replace('{LP_I'.$i.'}', '', $tpl);
	            }

		        $list1 = round(($len / 3),0);
		        $list2 = round((($len-$list1)/2),0);
		        $list3 = ($len-$list1)-$list2;

		        $sList1=326;
		        $sList2=326;
		        $sList3=326;

		        $tempList1="";
		        $tempList2="";
		        $tempList3="";

		        for ($i=0; $i < ($list1-1); $i++) {
		            $tempList1 .= "^FT36,".$sList1."^A0N,28,28^FH\^FD".$sn[$i]['serial_number']."^FS";
		            $sList1=$sList1+34;
		        }

		        for ($i=$list1; $i < (($list1+$list2)-1); $i++) {
		            $tempList2 .= "^FT287,".$sList2."^A0N,28,28^FH\^FD".$sn[$i]['serial_number']."^FS";
		            $sList2=$sList2+34;
		        }

		        for ($i=($list1+$list2); $i < (($list1+$list2+$list3)-1); $i++) {
		            $tempList3 .= "^FT542,".$sList3."^A0N,28,28^FH\^FD".$sn[$i]['serial_number']."^FS";
		            $sList3=$sList3+34;
		        }

		        $tpl = str_replace('^FT36,326^A0N,28,28^FH\^FD{LP_List1}^FS', $tempList1, $tpl);
		        $tpl = str_replace('^FT287,326^A0N,28,28^FH\^FD{LP_List2}^FS', $tempList2, $tpl);
		        $tpl = str_replace('^FT542,327^A0N,28,28^FH\^FD{LP_List3}^FS', $tempList3, $tpl);

		        $cmd .= $tpl;

			}
	    }

        $params = array(
            'printerCommands'   => $cmd
        );

        $result = $this->mimoclientprint->printIt($params);

        echo json_encode($result);

	}

    public function cancel(){

		$result = array();

		$params = array(
			'shipping_id' => $this->input->post('shipping_id'),
		);

		$cancel = $this->loading_model->cancel($params);

		if($cancel){

			$result['status'] = "success";
			$result['message'] = "Cancel loading success.";

		}else{

			$result['status'] = "failed";
			$result['message'] = "Cancel loading failed.";

		}

		echo json_encode($result);

    }

    public function cancel_item(){

		$result = array();

		$params = array(
			'shipping_id'		=> filter_var(trim($this->input->post('shipping_id')), FILTER_SANITIZE_NUMBER_INT),
			'serial_number'		=> $this->input->post('serial_number')
		);

		$cancel = $this->loading_model->cancel_item($params);

		if($cancel){

			$result['status'] = "success";
			$result['message'] = "Cancel loading success.";

		}else{

			$result['status'] = "failed";
			$result['message'] = "Cancel loading failed.";

		}


		echo json_encode($result);

    }
}

<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class stock_opname extends CI_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load->model(array($this->class_name . '_model'));
        
        $this->load->library("randomidgenerator");
    }

	public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Cycle Count List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/stock_opname/lr-cycle-count-view.js";

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

		$data['today'] = date('d/m/Y');
		$data['status'] = $this->stock_opname_model->getCcStatus();

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Cycle Count - Cycle Count');
        /* END OF INSERT LOG */

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	//ADD
	public function add(){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> New Cycle Count';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/stock_opname/lr-cycle-count-addedit.js";

			$data['id'] = '';
			$data['today'] = date('d/m/Y');
			$data['not_usage'] = 'true';
			$data['cc_code'] = $this->stock_opname_model->get_cc_code();
            $data['no_person'] = '';

			$type = array(
				'0'	=> array(
					'type_value' 	=> 'SN',
					'type_name'		=> 'By Item'
				),
				'1'	=> array(
					'type_value' 	=> 'LOC',
					'type_name'		=> 'By Location'
				)
			);

			$data['type'] = $type;
			$data['is_used'] = 'false';

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);
		}
	}

	//EDIT
	public function edit($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Cycle Count';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/stock_opname/lr-cycle-count-addedit.js";

			$data['id'] = $id;
			$data['today'] = date('d/m/Y');
			$data['not_usage'] = 'true';
			$data['cc_code'] = '';
            $data['no_person'] = '';

			$type = array(
				'0'	=> array(
					'type_value' 	=> 'SN',
					'type_name'		=> 'By Item'
				),
				'1'	=> array(
					'type_value' 	=> 'LOC',
					'type_name'		=> 'By Location'
				)
			);

			$data['type'] = $type;

			$isUsed = $this->stock_opname_model->checkIsUsed($id);
			if($isUsed['is_used'])
				$data['is_used'] = 'true';
			else
				$data['is_used'] = 'false';

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);
		}
	}

	//DETAIL
	public function detail($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Cycle Count Document';
			$data['page'] = $this->class_name . '/detail';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/stock_opname/lr-cycle-count-detail.js";

			$data['id'] = $id;
			$data['data'] = $this->stock_opname_model->getEdit(array('cc_id' => $id));
			$isUsed = $this->stock_opname_model->checkIsUsed($id);

			if($isUsed['is_used'])
				$data['is_used'] = 'true';
			else
				$data['is_used'] = 'false';

			//$data['not_usage'] = $this->outbound_model->cek_not_usage_do($id);

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	public function check_result($id){

		if ($this->access_right->otoritas('view')) {

			// $sts = $this->stock_opname_model->checkStatus($id);
			// if($sts != 0)
				// redirect(base_url() . $this->class_name);

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Cycle Count Check Result';
			$data['page'] = $this->class_name . '/check_result';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/stock_opname/lr-cycle-count-check.js";

			$data['id'] = $id;
			$data['data'] = $this->stock_opname_model->getEdit(array('cc_id' => $id));
			$data['not_usage'] = 'true';

			$isFinish = $this->stock_opname_model->checkIsFinish($id);
			$data['is_finish'] = $isFinish;

			//$data['not_usage'] = $this->outbound_model->cek_not_usage_do($id);

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	//REQUEST
	public function finishCc(){
		$result = array();

		$params = array(
			'cc_id'	=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->stock_opname_model->finishCc($params);

		echo json_encode($result);
	}

	public function adjustment(){
		$result = array();

		$params = array(
			'data'		=> $this->input->post('data'),
			'remark'	=> filter_var(trim($this->input->post('remark')), FILTER_SANITIZE_STRING)
		);

		$result = $this->stock_opname_model->adjustment($params);

		echo json_encode($result);
	}

	public function getDetailCheck(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'cc_type'	=> filter_var(trim($this->input->post('cc_type')), FILTER_SANITIZE_STRING)
		);

		$result = $this->stock_opname_model->getDetailCheck($params);

		echo json_encode($result);
	}

	public function getDetailItem(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'cc_type'	=> filter_var(trim($this->input->post('cc_type')), FILTER_SANITIZE_STRING)
		);

		$result = $this->stock_opname_model->getDetailItem($params);

		echo json_encode($result);
	}

	public function getDetailDataTable(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->stock_opname_model->getDetailDataTable($params);

		echo json_encode($result);
	}

	public function closeCc(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'remark'	=> filter_var(trim($this->input->post('remark')), FILTER_SANITIZE_STRING)
		);

		$result = $this->stock_opname_model->closeCc($params);

		echo json_encode($result);
	}

	public function print_cc_doc(){
		$data = array();

		$params = array(
			'cc_id'	=> $this->input->post('cc_id')
		);

		$cc = $this->stock_opname_model->getEdit($params);

		$data['page'] = $this->class_name . '/cc_report';
        $data['cc'] = $cc;

		$params['cc_type'] = $cc['cc_type'];

		$data['items']['data'] = $this->stock_opname_model->getItems($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "cc_report.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

    public function print_cc_doc2(){
        date_default_timezone_set('Asia/Jakarta');
		$data = array();

		$params = array(
			'cc_id'	=> $this->input->post('cc_id')
		);

		$cc = $this->stock_opname_model->getEdit($params);

		$data['page'] = $this->class_name . '/cc_report2';
        $data['cc'] = $cc;

        $data['scans'] = $this->stock_opname_model->getScans($params);

		$params['cc_type'] = $cc['cc_type'];

		$data['items']['data'] = $this->stock_opname_model->getItems($params);

        $html = $this->load->view($data['page'], $data, true);
        $path = "cc_report2.pdf";

        ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can

    }

	public function delete(){
		$result = array();

		$params = array(
			'cc_id'	=> filter_var(trim($this->input->post('id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->stock_opname_model->delete($params);
		$result['success'] = '1';

		echo json_encode($result);
	}

	public function getEdit(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('id')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->stock_opname_model->getEdit($params);

		$rows = "";

		if($result['cc_type'] == 'SN'){
			//$this->load->model('outbound_model');

			//$data['items'] = $this->outbound_model->create_options("barang", "id_barang", "nama_barang", "-- Choose item --");

			$data['items'] = $this->stock_opname_model->getChooseItem($params);
			$detail = $this->stock_opname_model->getDetail($params);

			foreach ($detail as $item) {
				$data['item'] = $item;
				$rows .= $this->load->view("stock_opname/add_item_row", $data, true);
			}

		}else{

            $data['areas'] = $this->stock_opname_model->getLocationArea($params);
			$detail = $this->stock_opname_model->getDetail($params);

			foreach ($detail as $location) {
                $params['loc_area_id']=$location['area_id'];
            	$data['loc'] = $this->stock_opname_model->getLocation2($params);
				$data['location'] = $location;
				$rows .= $this->load->view("stock_opname/add_loc_row", $data, true);
			}

		}

		$result['html'] = $rows;

		echo json_encode($result);
	}

	public function getCcCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->stock_opname_model->getCcCode($params);

		echo json_encode($result);
	}

	public function proses() {
       // $this->output->enable_profiler(TRUE);
        $proses_result = array();

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('cc_code', 'Cycle count code', 'required|trim');
            $this->form_validation->set_rules('no_person', 'No. Person', 'required|trim');
            $this->form_validation->set_rules('tanggal_cc', 'Cycle count date', 'required|trim');

            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $tanggal = $this->input->post("tanggal_cc");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal)))) . ' ' . date('H:i:s');

                $data = array(
                    'cc_code'	 			=> $this->input->post("cc_code"),
                    'no_person'	 			=> $this->input->post("no_person"),
                    'cc_time'		 		=> $date,
                    'cc_type'			 	=> $this->input->post("type"),
					'user_id'				=> $this->session->userdata('user_id')
             	);

                $detail = array();
                $idBarang = $this->input->post("id_barang");
				$idLocation = $this->input->post("loc_id");
				$idArea = $this->input->post("area_id");

				if($data['cc_type'] == 'SN'){

					if(!empty($idBarang)){
						if($idBarang){
							foreach ($idBarang as $key => $value) {
								$detail[] = array(
									'item_id' => $value
								);
							}
						}
					}

				}else{

					if(!empty($idLocation)){
						if($idLocation){
							foreach ($idLocation as $key => $value) {
                                $dtl = array(
									'loc_id' => $value,
                                    'area_id' => $idArea[$key]
								);
								$detail[] = $dtl;
							}
						}
					}

				}

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if (empty($id)) {

					$data['status'] = 0;

                    if ($this->stock_opname_model->create($data, $detail)) {
                        $proses_result['add'] = 1;
						$this->stock_opname_model->add_cc_code();

						/* INSERT LOG */
						$this->access_right->activity_logs('add','Cycle Count');
						/* END OF INSERT LOG */
                    }

                } else {

                    if ($this->stock_opname_model->update($data, $detail, $id)) {
                        $proses_result['edit'] = 1;

						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Cycle Count');
						/* END OF INSERT LOG */
                    }

                }

                $proses_result['success'] = 1;

            } else {

                $proses_result['message'] = validation_errors();

            }

        } else {

            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }

        }

        echo json_encode($proses_result);
    }

	public function getNewCycleCountCode(){
		$result = array();

		$row = $this->stock_opname_model->get_cc_code();

		if($row){

			$result['status'] = 'OK';
			$result['cycle_count_code'] = $row;

		}else{

			$result['status'] = 'OK';
			$result['cycle_count_code'] = $row;

		}

		echo json_encode($result);
	}

	public function get_add_loc_row() {
        //$data['loc'] = $this->stock_opname_model->getLocation();
        $data['areas'] = $this->stock_opname_model->getLocationArea();
        echo $this->load->view("stock_opname/add_loc_row", $data, true);
    }

    public function get_loc() {
        $data['loc'] = $this->stock_opname_model->getLocation2($this->input->post());
        echo $this->load->view("stock_opname/loc_option", $data, true);
    }

	public function get_add_item_row() {
		//$this->load->model('inbound_model');
        //$data['items'] = $this->inbound_model->create_options("barang", "id_barang", "kd_barang, nama_barang", "-- Choose item --");

		$data['items'] = $this->stock_opname_model->getChooseItem();
        echo $this->load->view("stock_opname/add_item_row", $data, true);
    }

	public function getList(){
		$result = array();

		$params = array(
			'cc_id'		=> filter_var(trim($this->input->post('cc_id')), FILTER_SANITIZE_NUMBER_INT),
			'status'	=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_NUMBER_INT),
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING)
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->stock_opname_model->getList($params);

		echo json_encode($result);
	}
}

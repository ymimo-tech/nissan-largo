<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
class outbound extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Outbound Document List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

        $data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/outbound/lr-outbound-view.js";

        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

		/* Otoritas Tombol tambah */

		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
		}

        $this->include_other_view_data_filter($data);
		$data['today'] = date('d/m/Y');

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Outbound');
        /* END OF INSERT LOG */
        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }
    
    public function export_so(){
        $this->load->library('PHPExcel');

		$params = array(
			'outbound'		=> filter_var(trim($this->input->post('outbound')), FILTER_SANITIZE_STRING),
			'destination'		=> filter_var(trim($this->input->post('destination')), FILTER_SANITIZE_STRING),
			'doc_type'		=> filter_var(trim($this->input->post('doc_type')), FILTER_SANITIZE_STRING),
			'to'		=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'from'		=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'status'		=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_STRING)
		);

		$result = $this->outbound_model->export_so($params);
        // dd($result);
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Active-Inventory")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'SO')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Customer')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Date')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Order Type')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Item SKU')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Item Name')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Qty')->getStyle('G1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

            $sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['code']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['destination_name']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['date']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['m_priority_name']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['sku']);
			$sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['name']);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['qty']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Outbound-export'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

	//ADD
	public function add(){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> New Outbound';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/outbound/lr-outbound-addedit.js";


			$this->include_other_view_data($data);

			$data['id'] = '';
			$data['today'] = date('d/m/Y');
			$data['not_usage'] = true;
			$data['doc_number'] = '';

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//EDIT
	public function edit($id=""){

		if ($this->access_right->otoritas('view')) {

			if($id){

				$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Outbound';
				$data['page'] = $this->class_name . '/add_edit';

				$this->include_required_file($data);

				$data['jsPage'][] = "assets/pages/scripts/outbound/lr-outbound-addedit.js";

				$this->include_other_view_data($data);

				$data['id'] = $id;
				$data['today'] = '';

				$notUse = $this->outbound_model->cek_not_usage_do($id);
				$nUse = 'false';
				if($notUse)
					$nUse = 'true';

				$data['not_usage'] = $nUse;
				$data['doc_number'] = '';

				$data['destination'] = $this->outbound_model->getDestinationByOutbound($id);

				/* INSERT LOG */
				//$this->access_right->activity_logs('view','Bukti Barang Masuk');
				/* END OF INSERT LOG */

				$this->load->view('template', $data);
			}else{
				show_404();
			}

		}else{
			show_404();
		}

	}

	//DETAIL
	public function detail($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Document Detail';
			$data['page'] = $this->class_name . '/detail';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/outbound/lr-outbound-detail.js";

			$this->include_other_view_data($data);

			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = $id;
			$data['data'] = $this->getDetail($id);
			$data['status_picking'] = $this->outbound_model->checkStatusPicking($id);
			$data['not_usage'] = $this->outbound_model->cek_not_usage_do($id);

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}
	
	public function getPackingList($id = false){
		$result = array();
		
		$params = array(
			'id_shipping'		=> filter_var(trim($this->input->post('id_shipping')), FILTER_SANITIZE_STRING),
			'id_outbound'		=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_STRING),
			'outbound'			=> filter_var(trim($this->input->post('outbound')), FILTER_SANITIZE_STRING),
			'pccode'			=> $this->input->post('pccode'),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'status'			=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_NUMBER_INT),
			'warehouse'			=> $this->input->post('warehouse'),
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->outbound_model->getPackingList($params);

		echo json_encode($result);
	}

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
		$data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

    private function include_other_view_data_filter(&$data) {
    	$this->load_model('outbound_document_model');
        $data['outbound_docs'] = $this->outbound_document_model->options();
		$data['doc_status'] = $this->outbound_model->statusOptions();
		// $this->load_model('m_gudang_model');

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues
    }

	private function include_other_view_data(&$data) {
    	$this->load_model('outbound_document_model');
        $data['outbound_docs'] = $this->outbound_document_model->options();
        // $data['destination'] = $this->outbound_model->create_options("m_gudang", "idGudang", "namaGudang", "--");

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

		$this->load_model('cost_center_model');
		$data['cost_center'] = $this->cost_center_model->get_data()->result_array();

		$priority = $this->db->get('m_priority')->result_array();

		$data['priority'] = array();

		if ($priority) {
			foreach ($priority as $prio) {
				$data['priority'][$prio['m_priority_id']] = $prio['m_priority_name'];
			}
		}

    }

    public function get_add_item_row($seq) {
    	$this->load_model('referensi_barang_model');
        $data['items'] = $this->referensi_barang_model->options();
        $data['random_string'] = $this->randomidgenerator->generateRandomString(4);
		$data['seq'] = $seq;
        echo $this->load->view("outbound/add_item_row", $data, true);
    }

	public function getDetail($id = false){
		$result = array();

		$params = array(
			'id_outbound'	=> $id
		);

		$result = $this->outbound_model->getDetail($params);

		return $result;
	}

	//REQUEST
	public function getBatch(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->outbound_model->getBatch($params);

		echo json_encode($result);
	}

	public function closeOutbound(){
		$result = array();

		$params = array(
			'id_outbound'	=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->outbound_model->closeOutbound($params);

		echo json_encode($result);
	}

	public function checkExistDoNumber(){
		$result = array();

		$params = array(
			'id_outbound'	=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT),
			'kd_outbound'	=> filter_var(trim($this->input->post('kd_outbound')), FILTER_SANITIZE_STRING)
		);

		$result = $this->outbound_model->checkExistDoNumber($params);

		echo json_encode($result);
	}


	public function getDetailItem($id = false){
		$result = array();

		$params = array(
			'id_outbound'	=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->outbound_model->getDetailItem($params);

		echo json_encode($result);
	}

	public function getPickingList(){
		$result = array();

		$params = array(
			'id_outbound'		=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT),
			'params'			=> filter_var(trim($this->input->post('params')), FILTER_SANITIZE_STRING),
			'pl_id'				=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT),
            'area'              => $this->input->post('area'),
            'id_gate'           => filter_var(trim($this->input->post('id_gate')), FILTER_SANITIZE_NUMBER_INT),
			'id_customer'		=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_NUMBER_INT),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'warehouse'			=> $this->input->post('warehouse'),
			'status'			=> $this->input->post('status')
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->outbound_model->getPickingList($params);

		echo json_encode($result);
	}

	public function getShippingList(){
		$result = array();

		$params = array(
			'id_outbound'		=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT),
			'params'			=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_STRING),
			'id_shipping'		=> filter_var(trim($this->input->post('id_shipping')), FILTER_SANITIZE_NUMBER_INT),
			'pl_id'				=> filter_var(trim($this->input->post('pl_id')), FILTER_SANITIZE_NUMBER_INT),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'status'			=> filter_var(trim($this->input->post('status')), FILTER_SANITIZE_NUMBER_INT),
			'warehouse'			=> $this->input->post('warehouse'),
			'gi_status'			=> $this->input->post('gi_status')
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->outbound_model->getShippingList($params);

		echo json_encode($result);
	}

	public function getList(){
		$result = array();

		$params = array(
			'do_number'		=> filter_var(trim($this->input->post('do_number')), FILTER_SANITIZE_NUMBER_INT),
			'id_customer'	=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_NUMBER_INT),
			'from'			=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'			=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'doc_type'		=> filter_var(trim($this->input->post('doc_type')), FILTER_SANITIZE_STRING),
			'doc_status'	=> $this->input->post('doc_status'),
			'warehouse'		=> $this->input->post('warehouse'),
			'priority'		=> $this->input->post('priority'),
			'className'		=> $this->class_name
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

		$result = $this->outbound_model->getList($params);

		echo json_encode($result);
	}

    public function create_multi_picking($idst=''){
        $this->load_model('picking_list_model');
        if($this->input->post('ids')){
            $idst = $this->input->post('ids');
        }else{
            $idst = $idst['id'];
        }
        $ids = explode(',',$idst);
        $date = date('Y-m-d H:i:s');

        //'id_outbound'		 	=> $this->input->post("id_outbound"),
        $data = array(
            'pl_name'	 			=> $this->picking_list_model->get_pl_code(),
            'pl_date'		 		=> $date,
            'id_status_picking'		=> 1,
            'user_id'				=> $this->session->userdata('user_id'),
            'pl_status'             => '0',
            'priority'				=> $this->input->post('priority')
        );
        $picking_exist = $this->picking_list_model->check_picking($ids);
		if(!$picking_exist){

            $this->db->trans_start();
			$create = $this->picking_list_model->create($data);

            if ($create > 0) {

                // $id = $this->db->insert_id();
				$id = $create;

                $this->picking_list_model->add_pl_code();

                foreach($ids as $idOutbound){
                    $params = array(
                        'pl_id'			=> $id,
                        'id_outbound'	=> $idOutbound
                    );

                    $this->picking_list_model->insertRelationTable2($params);

                    //$this->picking_list_model->setRecomendation($params);
                    $this->picking_list_model->updateOutboundStatus($idOutbound);
                }

                $outbound_barang = $this->outbound_model->getOutboundItem($ids);
                $data=array();
                $i=0;

                $kittingItems = array();

                foreach($outbound_barang->result() as $barang){

                	$kittingItems = $this->outbound_model->getKittingItems($barang->id_barang);
                	if($kittingItems){

						foreach($kittingItems as $kitItem){
		                    if(isset($data[$kitItem->item_id]['id_barang'])){
		                        $data[$kitItem->item_id]['qty']+= ($kitItem->qty*$barang->jumlah_barang);
		                    }else{
		                        $data[$kitItem->item_id]['id_barang']=$kitItem->item_id;
		                        $data[$kitItem->item_id]['qty']=($kitItem->qty*$barang->jumlah_barang);
		                    }
						}

                	}else{

                        if(isset($data[$barang->id_barang.'-'.$barang->unit_id]['id_barang'])){
                            $data[$barang->id_barang.'-'.$barang->unit_id]['qty']+=$barang->jumlah_barang;
                        }else{
                            $data[$barang->id_barang.'-'.$barang->unit_id]['id_barang']=$barang->id_barang;
                            $data[$barang->id_barang.'-'.$barang->unit_id]['qty']=$barang->jumlah_barang;
                            $data[$barang->id_barang.'-'.$barang->unit_id]['unit_id']=$barang->unit_id;
                        }
                	}

                }

                if(!empty($data)){

                     $this->picking_list_model->update_pl_qty2($id,$data);

                    // foreach($data as $dd){
                    //     $idBarang = $dd['id_barang'];
                    //     $qty = $dd['qty'];
                    //     $batch = '';
                    //		$this->picking_list_model->update_pl_qty($id,$idBarang,$qty,$batch);
                    // }
                }

                //$message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('add','Picking list');
                /* END OF INSERT LOG */

                $this->db->trans_complete();

        if($this->input->post('ids')){
            echo json_encode(array('success'=>1));
        }

            }else{
                $this->db->trans_complete();
                echo json_encode(array('success'=>0));
            }
        }else{
            echo json_encode(array('success'=>2));
        }
    }

	public function getNewOutboundCode(){
		$result = array();
		$row = $this->outbound_model->get_do_code();

		if($row){

			$result['status'] = 'OK';
			$result['outbound_code'] = $row;

		}else{

			$result['status'] = 'OK';
			$result['outbound_code'] = $row;

		}

		echo json_encode($result);
	}

	public function getOutboundCode(){
		$result = array();

		$params = array(
			'query'	=> filter_var($this->input->post('query'), FILTER_SANITIZE_STRING)
		);

		$result = $this->outbound_model->getOutboundCode($params);

		echo json_encode($result);
	}

	public function getCustomerBySearch(){
		$result = array();

		$params = array(
			'query'	=> filter_var(trim($this->input->post('query')), FILTER_SANITIZE_STRING)
		);

		$result = $this->outbound_model->getCustomerBySearch($params);

		echo json_encode($result);
	}

    public function getGateBySearch(){
		$result = array();

		$params = array(
			'query'	=> filter_var(trim($this->input->post('query')), FILTER_SANITIZE_STRING)
		);

		$result = $this->outbound_model->getGateBySearch($params);

		echo json_encode($result);
	}

	public function getDestination(){

		$result = array();

		$params = array(
			'document_id' => $this->input->post('document_id'),
			'destination_id' => $this->input->post('destination_id')
		);

		$result = $this->outbound_model->getDestination($params);

		echo json_encode($result);

	}

	public function getDestinationDetail(){

		$result = array();

		$params = array(
			'document_id' => $this->input->post('document_id'),
			'destination_id' => $this->input->post('destination_id')
		);

		$result = $this->outbound_model->getDestination($params);

		echo json_encode($result);

	}

    public function get_data() {
        $process_result;
		$this->load_model('inbound_model');

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            //$outbound = $this->outbound_model->get_data(array('id_outbound' => $id))->row_array();
			$outbound = $this->outbound_model->get_data($id)->row_array();

            $arr_outbound_barang = $this->outbound_model->get_outbound_barang($outbound['id_outbound'])->result_array();
            $rows = "";

            $this->load_model('referensi_barang_model');
            $data['items'] = $this->referensi_barang_model->options();

			$seq = 0;
            foreach ($arr_outbound_barang as $outbound_barang) {
                $data['random_string'] = $this->randomidgenerator->generateRandomString(4);
                $data['outbound_barang'] = $outbound_barang;

				$params = array(
					'id_barang'	=> $outbound_barang['id_barang']
				);

				$r = $this->outbound_model->getBatch($params);
				$data['batchs'] = $r['data'];

				$data['seq'] = $seq;
        // dd($outbound);
				$seq = $seq + 1;

                $rows .= $this->load->view("outbound/add_item_row", $data, true);
            }
            $outbound['outbound_barang_html'] = $rows;

            $process_result = $outbound;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function get_list(){
        $this->session->set_userdata('current_filter',$this->input->post('periode'));
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $outbounds = $this->outbound_model->data($param)->get();
        $iTotalRecords = $this->outbound_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($outbounds->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_outbound,'delete' => $value->id_outbound));

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="1">',
                $_REQUEST['start']+$i,
                $value->kd_outbound,
                DateTime::createFromFormat('Y-m-d', $value->tanggal_outbound)->format('d/m/Y'),
                $value->outbound_document_name,
                $value->customer_name,
                $action
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        $records["current_filter"] = $this->session->userdata('current_filter');

        echo json_encode($records);
    }

    public function proses() {

        $proses_result = array();

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_outbound', 'outbound Code', 'required|trim');
            $this->form_validation->set_rules('tanggal_outbound', 'outbound date', 'required|trim');
            $this->form_validation->set_rules('id_outbound_document', 'outbound document type', 'required|trim');
            $this->form_validation->set_rules('tanggal_outbound', 'outbound date', 'required|trim');
            $this->form_validation->set_rules('name', 'name', 'required|trim');
            // $this->form_validation->set_rules('address', 'address', 'required|trim');
            // $this->form_validation->set_rules('Phone', 'Phone', 'required|trim');
            $this->form_validation->set_rules('ShippingNote', 'Shipping Note', 'required|trim');
            // $this->form_validation->set_rules('DestinationAddressL1', 'DestinationAddressL1', 'required|trim');
            // $this->form_validation->set_rules('DestinationAddressL2', 'DestinationAddressL2', 'required|trim');
            // $this->form_validation->set_rules('DestinationAddressL3', 'DestinationAddressL3', 'required|trim');
            // $this->form_validation->set_rules('cost_center', 'cost_center', 'required|trim');
            //$this->form_validation->set_rules('destination', 'destination', 'required|trim');


            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $tanggal = $this->input->post("tanggal_outbound");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal))));
				$delivery_date = date('Y-m-d', strtotime(str_replace('/','-', trim($this->input->post('delivery_date')))));

                $data = array(
                    'code' 			=> $this->input->post("kd_outbound"),
                    'date' 		=> $date,
                    'document_id' 	=> $this->input->post("id_outbound_document"),
                    //'id_customer' 			=> $this->input->post("destination"),
					'user_id'				=> $this->session->userdata('user_id'),
					'destination_name'		=> $this->input->post('name'),
					'address_1'	=> $this->input->post('DestinationAddressL1'),
					'address_2'	=> $this->input->post('DestinationAddressL2'),
					'address_3'	=> $this->input->post('DestinationAddressL3'),
					'phone'                 => $this->input->post('Phone'),
					'note'		    => $this->input->post('ShippingNote'),
					'destination_id'		=> $this->input->post('destination'),
					'warehouse_id'				=> $this->input->post('warehouse'),
					'shipping_group'		=> $this->input->post('shipping_group'),
					'delivery_date'			=> $delivery_date,
					'address'				=> $this->input->post('address'),
					'm_priority_id'				=> $this->input->post('priority')
                 );
                 // dd($data);

                $data['cost_center_id']		= (!empty($this->input->post('cost_center'))) ? $this->input->post('cost_center') : NULL;

                $data2 = array();
                $arr_id_barang = $this->input->post("id_barang");
                $arr_item_quantity = $this->input->post("item_quantity");
				$batch = $this->input->post("batchcode");
                $unit = $this->input->post('unit');
				$len = count($arr_id_barang);

				if(!empty($arr_id_barang)){
					if($arr_id_barang){

						for($i = 0; $i < $len; $i++){
							if(isset($batch[$arr_id_barang[$i]])){

								$data2[] = array(
									'jumlah_barang' => $arr_item_quantity[$i],
									'id_barang' 	=> $arr_id_barang[$i],
                                    'batch'         => $batch[$arr_id_barang[$i]],
                                    'unit_id'       => $unit[$i]
                                );

							}else{

								$data2[] = array(
									'jumlah_barang' => $arr_item_quantity[$i],
									'id_barang' 	=> $arr_id_barang[$i],
									'batch'			=> null,
                                    'unit_id'       => $unit[$i]
								);

							}
                        }


						// foreach ($arr_id_barang as $key => $value) {

							// if(isset($batch[$value])){

								// $data2[] = array(
									// 'jumlah_barang' => $arr_item_quantity[$key],
									// 'id_barang' 	=> $value,
									// 'batch'			=> $batch[$value]
								// );

							// }else{

								// $data2[] = array(
									// 'jumlah_barang' => $arr_item_quantity[$key],
									// 'id_barang' 	=> $value,
									// 'batch'			=> null
								// );

							// }
						// }
					}
				}

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if (empty($id)) {

                    $data['status_id'] = 1;

                    if ($this->outbound_model->create($data, $data2)) {
                        $this->outbound_model->addOutDocument($data);


                        $proses_result['add'] = 1;
						$this->outbound_model->add_do_code();

						/* INSERT LOG */
						$this->access_right->activity_logs('add','outbound');
                        /* END OF INSERT LOG */

                        if($data['m_priority_id'] == 2){

                            $id_outbound = $this->db
                                            ->select('id')
                                            ->from('outbound')
                                            ->where('code', $data['code'])
                                            ->get()
                                            ->row_array();

                            $this->create_multi_picking($id_outbound);
                        }

                        //check warehouse transfer, check by id (hardcode)
//                        if($data['id_outbound_document']==7){

                            // date_default_timezone_set('Asia/Jakarta');
                            // $date = date('Y-m-d H:i:s');
                            // $this->load_model('api_staging_model');

                            // $this->load_model('outbound_document_model');
                            // $this->load_model('destination_model');

                            // $outboundType = $this->outbound_document_model->get_by_id($data['id_outbound_document'])->row_array();
                            // $destination = $this->destination_model->getById($data['id_destination'])->row_array();

                            // //outbound
                            // $outbound_data = array('data'=>array(
                            //     'DocDate'=>$date,
                            //     'BaseCreatedDate'=>$date,
                            //     'OutbondNo'=>$data['kd_outbound'],
                            //     'Cancelled'=>'N',
                            //     'OutbondCat'=>'', // ?
                            //     'OutbondType'=> $outboundType['outbound_document_name'],
                            //     'WhsFrom'=>'', // ?
                            //     'WhsTo'=>'', // ?
                            //     'Destination'=>$destination['namaGudang'],
                            //     'DestinationName'=>$data['DestinationName'],
                            //     'DestinationAddressL1'=>$data['DestinationAddressL1'],
                            //     'DestinationAddressL2'=>$data['DestinationAddressL2'],
                            //     'DestinationAddressL3'=>$data['DestinationAddressL3'],
                            //     'PhoneNo'=>$data['Phone'],
                            //     'ShippingNote'=>$data['ShippingNote'],
                            //     'SyncStatus'=>'N',
                            //     'SyncDate'=>'',
                            //     'OutbondStatus'=>'', // ?
                            //     'OutbondDate'=>$data['tanggal_outbound']
                            // ));
                            // $otb = $this->api_staging_model->_curl_process('insert','f_t_outbound_h',$outbound_data);
                            // if($otb->status=='001'){
                            //     //outbound l1
                            //     foreach($data2 as $key=>$l1){
                            //         $SKU = $this->outbound_model->get_barang_SKU($l1['id_barang']);
                            //         $outboundl1_data = array('data'=>array(
                            //             'DocEntry'=>$otb->id,
                            //             'LineNum'=>$key+1,
                            //             'SKU'=>$SKU,
                            //             'DocQty'=>$l1['jumlah_barang']
                            //         ));
                            //         $data2[$key]['SKU']=$SKU;
                            //         $lires=$this->api_staging_model->_curl_process('insert','f_t_outbound_l1',$outboundl1_data);
                            //     }
                            // }

//                        }
                    }

					$proses_result['success'] = 1;

                } else {


					$del = $this->input->post("delete");

					if($del)
						$data['delete'] = $del;

					$result = $this->outbound_model->update($data, $data2, $id);

					if(isset($result['status'])){
						if($result['status'] == 'ERR'){

							$proses_result['status'] = 'ERR';
							$proses_result['message'] = $result['message'];

						}else{

							if ($this->outbound_model->update($data, $data2, $id)) {
								$proses_result['edit'] = 1;

								/* INSERT LOG */
								$this->access_right->activity_logs('edit','outbound');
								/* END OF INSERT LOG */
							}

							$proses_result['success'] = 1;
						}
					}else{

						if($del){

							$proses_result['success'] = 1;

						}else{

							if ($this->outbound_model->update($data, $data2, $id)) {
								$proses_result['edit'] = 1;

								/* INSERT LOG */
								$this->access_right->activity_logs('edit','outbound');
								/* END OF INSERT LOG */
							}

							$proses_result['success'] = 1;

						}

					}


                }

/*                if($data['id_outbound_document']==3 || $data['id_outbound_document'] == 14){


                    $this->load_model('inbound_model');

                    $dataInbound = array(
                        'kd_inbound'            =>  $data['kd_outbound'],
                        'tanggal_inbound'       =>  $data['tanggal_outbound'],
                        'id_supplier'           =>  $data['warehouse'],
                        'id_inbound_document'   =>  $data['id_outbound_document'],
                        'user_id'               =>  $data['user_id'],
                        'remark'                =>  '',
                        'warehouse' 			=>  $data['id_destination']
                    );

                    $inbound = $this->inbound_model->check_inbound($data['kd_outbound'])->row();
                    if(count($inbound) > 0){

                        $this->inbound_model->update($dataInbound,$data2,$inbound->id_inbound);

                    }else{

                        $this->inbound_model->create($dataInbound,$data2);
                    }

                }
*/
                //$proses_result['success'] = 1;
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function save_new_customer() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $proses_result['success'] = 1;
            $this->form_validation->set_rules('customer_name', 'outbound Code', 'required|trim');
            $this->form_validation->set_rules('phone', 'outbound Code', 'required|trim');

            if ($this->form_validation->run()) {
                $data = array(
                    'customer_name' => $this->input->post("customer_name"),
                    'phone' => $this->input->post("phone")
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($this->outbound_model->create_customer($data)) {
                    $proses_result['add'] = 1;
                    /* INSERT LOG */
                    $this->access_right->activity_logs('add','Outbound - Creating new customer');
                    /* END OF INSERT LOG */
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            $result = $this->outbound_model->delete($id);
            $result['success'] = 1;
        } else {
            $result['no_delete'] = 1;
        }
        echo json_encode($result);
    }

    public function load_customers() {
        $customer_options = $this->outbound_model->create_options("m_customer", "id_customer", "customer_name", "-- Choose customer --");
        $html = "";
        foreach ($customer_options as $key => $option) {
            $html .= "<option value='$key'>$option</option>";
        }
        echo $html;
    }

    function __sync_outbound(){
        $this->db->trans_start();
        $new_outbounds =  $this->outbound_model->get_new_outbound();
        $new=array();
        foreach($new_outbounds->result() as $outbound){
            // $outbound->DocEntry
            // $outbound->DocDate
            // $outbound->BaseCreatedDate
            // $outbound->OutbondNo
            // $outbound->Cancelled
            // $outbound->OutbondType
            // $outbound->WhsFrom
            // $outbound->WhsTo
            // $outbound->DestinationName
            // $outbound->DestinationAddressL1
            // $outbound->DestinationAddressL2
            // $outbound->DestinationAddressL3
            // $outbound->SyncStatus
            // $outbound->SyncDate
            // $outbound->OutbondStatus
            // $outbound->OutbondDate

            $doc =  $this->outbound_model->get_outbound_document($outbound->OutbondType);
            $id_doc = $doc->num_rows()?$doc->row()->id_outbound_document:0;
            $data_outbound=array(
                'kd_outbound'=>$outbound->OutbondNo,
                'tanggal_outbound'=>$outbound->DocDate,
                'id_outbound_document'=>$id_doc,
                'id_status_outbound'=>1,
                'id_staging'=>$outbound->DocEntry,
                'DestinationName'=>$outbound->DestinationName,
                'DestinationAddressL1'=>$outbound->DestinationAddressL1,
                'DestinationAddressL2'=>$outbound->DestinationAddressL2,
                'DestinationAddressL3'=>$outbound->DestinationAddressL3
            );
            $new_outbound_id = $this->outbound_model->create_outbound($data_outbound);
            $outbound_items = $this->outbound_model->get_outbound_item($outbound->DocEntry);
            foreach($outbound_items->result() as $item){
                // $item->DocEntry
                // $item->LineNum
                // $item->SKU
                // $item->DocQty
                $barang =  $this->outbound_model->get_barang($item->SKU);
                $id_barang = $barang->num_rows()?$barang->row()->id_barang:0;
                $data_item = array(
                    'id_outbound'=>$new_outbound_id,
                    'id_barang'=>$id_barang,
                    'jumlah_barang'=>$item->DocQty,
                    'id_staging'=>$item->LineNum
                );
                $new_outbound_barang_id = $this->outbound_model->create_outbound_barang($data_item);
            }
            $SyncDate=date('Y-m-d H:i:s');
            $this->outbound_model->set_outbound_status($outbound->DocEntry,$SyncDate);
            $new[]=array('nomor'=>$outbound->OutbondNo,'id'=>$new_outbound_id);
        }
        $this->db->trans_complete();
        return $new;
    }

    function longpolling(){
	    date_default_timezone_set("Asia/Jakarta");
	    header('Cache-Control: no-cache');
	    header("Content-Type: text/event-stream\n\n");
	    while (1) {
            //check new outbound
            $new = $this->sync_outbound();
            //sleep(10);
            //$new = array(array('nomor'=>'PO170411-001','id'=>48),array('nomor'=>'PO170425-001','id'=>49));
	        $output = array('newc'=>$new);
	        echo "\nevent: sync\n";
	        echo 'data: '.json_encode($output);
	        echo "\n\n";
	        ob_flush();
	        flush();
            session_write_close();
	        sleep(30);
	    }
	}

    function manual_sync_outbound(){
        date_default_timezone_set("Asia/Jakarta");
        $new = $this->sync_outbound();
        $json = array('newc'=>$new);
        echo json_encode($json);
    }

    public function getOutDocument(){

    	$id = $this->input->post('id');
      $id_outbound = $this->input->post('id_outbound');
    	// $data= $this->outbound_model->getOutDocument($id);
      $data= $this->outbound_model->getOutDocument($id, $id_outbound);

    	$output = array();
    	if($data){

    		$output['param']	= "";
    		$output['status']	= true;
    		$output['message']	= "Success Get Outbound Document Number";
    		$output['result']	= $data;

    	}else{

    		$output['param'] 	= "";
    		$output['status'] 	= false;
    		$output['message'] 	= "Can't Get Outbound Document Number";

    	}

    	echo json_encode($output);
    }

    public function approval(){

        if ($this->access_right->otoritas('approve')) {

        	$params['status'] = $this->input->post('status');
        	$params['id_outbound'] 	= $this->input->post('id_outbound');

        	if(!empty($params['status']) && !empty($params['id_outbound'])){

	        	$data = $this->outbound_model->approval($params);

		    	if($data){

		    		$output['param']	= $params['status'];
		    		$output['status']	= 200;
		    		$output['response'] = true;
		    		$output['message']	= $params['status']. " this document success.";

		    	}else{

		    		$output['param'] 	= $params['status'];
		    		$output['status'] 	= 400;
		    		$output['response']	= false;
		    		$output['message']	= "Please Try Again !";

		    	}

				$this->access_right->activity_logs('approval','outbound');

		    }else{

	    		$output['param'] 	= "";
	    		$output['status'] 	= 400;
	    		$output['response']	= false;
	    		$output['message'] 	= "Try Again Please !";

		    }

	    	echo json_encode($output);

        }
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Outbound Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

	    	$this->load->library('import');

	        $path = 'uploads/excel/'. $this->class_name .'/';
	        $upload = $this->import->upload($path);

	        if($upload['status']){

	            $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

	            if(!$arr){

	                $result = $this->import->errorMessage('Wrong Template');

	            }else{

			        $field = [
			            'No Document'=> 'document_name',
			            'Date' => 'date',
			            'Type' => 'document_id',
			            'Destination' => 'destination_name',
			            'Address' => 'address_1',
			            'Phone'	=>	'phone',
			            'Shipping Note' => 'note',
			            'Material ID' => 'item_id',
			            'Qty' => 'qty',
  			            'Warehouse' => 'warehouse'
			        ];

	                $data = $this->import->toFieldTable($field, $arr);

	                if(!$data){

	                    $result = $this->import->errorMessage('Wrong Template');

	                }else{

	                	$process = $this->outbound_model->importFromExcel($data);

	                	if($process){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

	                	}else{

			                $result = $this->import->errorMessage($upload['data']);

	                	}

	                }

	            }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }

        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

   }

   public function getOutboundDocument(){

        $results = array();

        $params = array(
            'warehouse' => $this->input->post('warehouse')
        );

        $this->load_model('outbound_document_model');

        $data = $this->outbound_document_model->getDocumentByWarehouse($params);

        if($data->num_rows() > 0){

            $results['data'] = $data->result_array();
            $results['response'] = true;

        }else{

            $results['response'] = false;
            $results['message'] = 'Please create document type first!';

        }

        echo json_encode($results);

    }

    public function getUomByItem(){

        $result = array();

        $params= array(
            "id_barang" => $this->input->post('id_barang')
        );

        $this->db
            ->select('unt.id as unit_id, unt.code as unit_code')
            ->from('items itm')
            ->join('units unt','unt.id=itm.unit_id or unt.id=itm.convert_qty_unit_id_1 or unt.id=itm.convert_qty_unit_id_2')
            ->where('itm.id',$params['id_barang']);

        $result = $this->db->get()->result_array();

        echo json_encode($result);

    }

}

/* End of file referensi_barang.php */
/* Location: ./application/controllers/referensi_barang.php */

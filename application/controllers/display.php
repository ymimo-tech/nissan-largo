<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Display extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('display_model');
		//$this->output->enable_profiler(TRUE);
	}
	public function index(){
		$data['inbounds'] = $this->display_model->get_display_inbound();
		$data['outbounds'] = $this->display_model->get_display_outbound();
		$data['total'] = $this->display_model->get_display_total_outbound();
		$data['cycles'] = $this->display_model->get_display_cycle();
		$data['widget'] = $this->display_model->getWidget();
		// $data['widget']['total_item']=0;
		// $data['widget']['total_qty']=0;
		// $data['widget']['total_tally']=0;
		// $data['widget']['total_qty_tally']=0;
		// $data['widget']['total_putaway']=0;
		// $data['widget']['total_qty_putaway']=0;
		// $data['widget']['average']=0;
        $this->load->view('display/display_view',$data);
	}

	public function longpolling(){
		date_default_timezone_set("Asia/Jakarta");
	    header('Cache-Control: no-cache');
	    header("Content-Type: text/event-stream\n\n");
	    while (1) {
			session_start();
            $inbound_ids = $this->session->userdata('inbound_ids');
			$outbound_ids = $this->session->userdata('outbound_ids');
			$cycle_ids = $this->session->userdata('cycle_ids');

			$new_inbound_ids = array();
			$new_outbound_ids = array();
			$new_cycle_ids = array();

			$inbound=array();
			$outbound=array();
			$cycle=array();

			$ib_data=array();
			$ob_data=array();
			$cy_data=array();

			$inbounds = $this->display_model->get_display_inbound();
			foreach($inbounds->result() as $inbound){
				$new_inbound_ids[]=$inbound->id;
				if(!in_array($inbound->id,$inbound_ids)){
					$ib_data[]=$inbound;
				}
			}
			$this->session->set_userdata('inbound_ids',$new_inbound_ids);

			$outbounds = $this->display_model->get_display_outbound();
			foreach($outbounds->result() as $outbound){
				$new_outbound_ids[]=$outbound->id;
				if(!in_array($outbound->id,$outbound_ids)){
					$ob_data[]=$outbound;
				}
			}
			$this->session->set_userdata('outbound_ids',$new_outbound_ids);

			$cycles = $this->display_model->get_display_cycle();
			foreach($cycles->result() as $cycle){
				$new_cycle_ids[]=$cycle->id;
				if(!in_array($cycle->id,$cycle_ids)){
					$cy_data[]=$cycle;
				}
			}
			$this->session->set_userdata('cycle_ids',$new_cycle_ids);

			$inbound = $this->load->view('display/display_inbound_view',array('data'=>$ib_data),TRUE);
			$outbound = $this->load->view('display/display_outbound_view',array('data'=>$ob_data),TRUE);
			$cycle = $this->load->view('display/display_cycle_view',array('data'=>$cy_data),TRUE);

			$exp_inbound = array_diff($inbound_ids,$new_inbound_ids);
			$exp_outbound = array_diff($outbound_ids,$new_outbound_ids);
			$exp_cycle = array_diff($cycle_ids,$new_cycle_ids);

			$totals = $this->display_model->get_display_total_outbound();
			$total['total_out']=$totals->total_out;
			$total['process']=$totals->process;
			$total['picking']=$totals->picking;
			$total['packing']=$totals->packing;
			$total['loading']=$totals->loading;
            $new = array('inbound'=>$inbound,'outbound'=>$outbound,'cycle'=>$cycle,'total'=>$total,'exp_inbound'=>$exp_inbound,'exp_outbound'=>$exp_outbound,'exp_cycle'=>$exp_cycle);
	        $output = array('newc'=>$new);
	        echo "\nevent: sync\n";
	        echo 'data: '.json_encode($output);
	        echo "\n\n";
			ob_flush();
			flush();
			session_write_close();
			sleep(1);
	    }
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

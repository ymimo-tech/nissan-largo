<?php

/**
 * Description of referensi_barang
 *
 * @author SANGGRA HANDI
 */
ini_set('memory_limit', '2048M');

class inbound extends MY_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Inbound Document List';
        $data['page'] = $this->class_name . '/index';

		$this->include_required_file($data);

		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/inbound/lr-inbound-view.js";

		if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import','components/import/form-import-inbound-stock'];

            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
            $data['jsPage'][] = "assets/pages/scripts/import/lr-import-inbound-stock.js";
        }

        // if($this->access_right->otoritas('import')){
            // $data['page'] = [$data['page'],'components/form-import'];
            // $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        // }

		/* Otoritas Tombol tambah */
		$data['button_group'] = array();
		if ($this->access_right->otoritas('add')) {
			/* NEW DOCUMENT FROM ERP - COMMENT FOR HIDE IT
			$data['button_group'] = array(
				anchor(base_url() . $this->class_name . '/add', '<i class="fa fa-file-o"></i> &nbsp;NEW', array('id' => 'button-add', 'class' => 'btn blue btn-sm min-width120', 'data-source' => base_url($this->class_name . '/add')))
			);
			*/
		}

        $this->include_other_view_data_filter($data);

		$this->load_model('referensi_supplier_model');
        //$data['options_bbm'] = $this->receiving_model->options();
        //$data['options_supplier'] = $this->referensi_supplier_model->options2();
        //$data['tahun_aktif'] = hgenerator::list_year(); //----Tahun Aktif------------
		$data['today'] = date('d/m/Y');

		/* INSERT LOG */
		$this->access_right->activity_logs('view','Bukti Barang Masuk');
        /* END OF INSERT LOG */

        $data['current_filter']=($this->session->userdata('current_filter')?$this->session->userdata('current_filter'):'this_month');

        $this->load->view('template', $data);
    }

	//ADD
	public function add(){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> New Inbound Doc';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/inbound/lr-inbound-addedit.js";

			$this->include_other_view_data($data);

			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = '';
			$data['today'] = date('d/m/Y');
			$data['not_usage'] = true;

			/*SETTING BLOCK*/
			// $this->load_model('setting_model');
			// $this->load_model('outbound_model');

			// $data['setting'] = $this->setting_model->getData();

			// $data['outbound_peminjaman'] = array();
			// if($data['setting']['peminjaman'] == 'A')
			// 	$data['outbound_peminjaman'] = $this->outbound_model->getOutboundPeminjaman();
			/*ENBLOCK*/

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Referensi Barang');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//EDIT
	public function edit($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Edit Inbound Doc';
			$data['page'] = $this->class_name . '/add_edit';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/inbound/lr-inbound-addedit.js";

			$this->include_other_view_data($data);

			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = $id;
			$data['today'] = '';

			$notUse = $this->inbound_model->cek_not_usage_po($id);
			$nUse = 'false';
			if($notUse)
				$nUse = 'true';

			$data['not_usage'] = $nUse;

			/*SETTING BLOCK*/
			// $this->load_model('setting_model');
			// $this->load_model('outbound_model');

			// $data['setting'] = $this->setting_model->getData();

			// $data['outbound_peminjaman'] = array();
			// if($data['setting']['peminjaman'] == 'A')
			// 	$data['outbound_peminjaman'] = $this->outbound_model->getOutboundPeminjaman();
			/*ENBLOCK*/

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	//DETAIL
	public function detail($id){

		if ($this->access_right->otoritas('view')) {

			$data['title'] = '<i class="icon-plus-sign-alt"></i> Document Info';
			$data['page'] = $this->class_name . '/detail';

			$this->include_required_file($data);

			$data['jsPage'][] = "assets/pages/scripts/inbound/lr-inbound-detail.js";

			$this->include_other_view_data($data);

			//$this->load_model('referensi_supplier_model');
			//$data['options_bbm'] = $this->receiving_model->options();
			$data['id'] = $id;
			$data['data'] = $this->getDetail($id);
			$data['not_usage'] = $this->inbound_model->cek_not_usage_po($id);

			/* INSERT LOG */
			//$this->access_right->activity_logs('view','Bukti Barang Masuk');
			/* END OF INSERT LOG */

			$this->load->view('template', $data);

		}
	}

	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}

	private function include_other_view_data_filter(&$data) {

        $data['suppliers'] = $this->options('sources','source_id','source_name', true);

        $data['inbound_docs'] = $this->options('documents','document_id','name',true,["type like '%INBOUND%'"=>NULL]);

		$data['inbound_status'] = $this->options('status','status_id','name',true,["type like '%INBOUND%'"=>NULL]);

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues
    }

    private function include_other_view_data(&$data) {
    	$this->load_model('referensi_supplier_model');
        $data['suppliers'] = $this->referensi_supplier_model->options();

    	$this->load_model('inbound_document_model');
        $data['inbound_docs'] = $this->inbound_document_model->options();

		$data['inbound_status'] = $this->inbound_model->statusOptions();

        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

    }

    public function get_add_item_row($seq) {
		$this->load_model('referensi_barang_model');
        $data['items'] = $this->referensi_barang_model->options();
        $data['random_string'] = $this->randomidgenerator->generateRandomString(4);
		$data['seq'] = $seq;

    	$this->load_model('cost_center_model');
        $data['cost_center'] = $this->cost_center_model->options('-- Pilih Cost Center --');

        echo $this->load->view("inbound/add_item_row", $data, true);
    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $inbound = $this->inbound_model->get_data(array('a.id_inbound' => $id))->row_array();
            $inbound['tanggal_inbound'] = DateTime::createFromFormat('Y-m-d', $inbound['tanggal_inbound'])->format('d/m/Y');

            $arr_inbound_barang = $this->inbound_model->get_inbound_barang($inbound['id_inbound'])->result_array();
            $rows = "";

            $this->load_model('referensi_barang_model');
            $data['items'] = $this->referensi_barang_model->options();

	    	$this->load_model('cost_center_model');
	        $data['cost_center'] = $this->cost_center_model->options('-- Pilih Cost Center --');

			$seq = 0;
            foreach ($arr_inbound_barang as $inbound_barang) {
                $data['random_string'] = $this->randomidgenerator->generateRandomString(4);
                $data['inbound_barang'] = $inbound_barang;
				$data['seq'] = $seq;
				$seq = $seq + 1;
                $rows .= $this->load->view("inbound/add_item_row", $data, true);
            }
            $inbound['inbound_barang_html'] = $rows;

            $process_result = $inbound;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

	/*---- START ADJ -----*/

	public function getDetailOutbound(){
		$result = array();

		$params = array(
			'id_outbound'	=> filter_var(trim($this->input->post('id_outbound')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->inbound_model->getDetailOutbound($params);

		echo json_encode($result);
	}

	public function getUomByItem(){
		$result = array();

		$params = array(
			'id_barang'	=> filter_var(trim($this->input->post('id_barang')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->inbound_model->getUomByItem($params);

		echo json_encode($result);
	}

	public function getSource(){

		$result = array();

		$params = array(
			'document_id' => $this->input->post('document_id')
		);

		$result = $this->inbound_model->getSource($params);

		echo json_encode($result);

	}

	public function getCustomer(){
		$result = array();

		$params = array();

		$result = $this->inbound_model->getCustomer($params);

		echo json_encode($result);
	}

	public function getSupplier(){
		$result = array();

		$params = array();

		$result = $this->inbound_model->getSupplier($params);

		echo json_encode($result);
	}

	public function getDetail($id = false){
		$result = array();

		$params = array(
			'id_inbound'	=> $id
		);

		$result = $this->inbound_model->getDetail($params);

		return $result;
	}

	public function getDetailItem($id = false){
		$result = array();

		$params = array(
			'id_inbound'	=> filter_var(trim($this->input->post('id_inbound')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->inbound_model->getDetailItem($params);

		echo json_encode($result);
	}

    public function export(){
		$this->load->library('PHPExcel');

		$params = array(
            'id_inbound' => filter_var(trim($this->input->post('id_inbound')), FILTER_SANITIZE_NUMBER_INT)
        );

		$result = $this->inbound_model->get_data_excel($params);
		$iLen = count($result);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Inbound - Ordered Items")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);

		$sheet->setCellValueByColumnAndRow(0, 1, 'Item Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Item Name')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Ordered')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'Received')->getStyle('D1')->getFont()->setBold(true);

        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['kd_barang']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['nama_barang']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['jumlah_barang']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['total_received']);

                //$col++;
            //}

            $row++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		ob_end_clean();

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Inbound - Ordered Items'.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
	}

	public function get_list(){
		$result = array();

		$params = array(
			'po' 			=> filter_var(trim($this->input->post('po')), FILTER_SANITIZE_STRING),
			'id_supplier' 	=> filter_var(trim($this->input->post('id_supplier')), FILTER_SANITIZE_NUMBER_INT),
			'from' 			=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to' 			=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING),
			'doc_type' 		=> filter_var(trim($this->input->post('doc_type')), FILTER_SANITIZE_STRING),
			'doc_status' 	=> $this->input->post('doc_status'),
			'warehouse'		=> $this->input->post('warehouse'),
			'className'		=> $this->class_name
		);

        $this->session->set_userdata('current_filter',$this->input->post('periode'));

        //--------update status inbound-----------//
        // $this->inbound_model->updateInboundStatus();
        //==============end update================//
		$result = $this->inbound_model->getList($params);

		echo json_encode($result);
	}

	public function closePo(){
		$result = array();

		$params = array(
			'id_inbound'	=> filter_var(trim($this->input->post('id_inbound')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->inbound_model->closePo($params);

		echo json_encode($result);
	}

	public function checkInboundDocNumberExist(){
		$result = array();

		$params = array(
			'po'			=> filter_var(trim($this->input->post('po')), FILTER_SANITIZE_STRING),
			'id_inbound'	=> filter_var(trim($this->input->post('id_inbound')), FILTER_SANITIZE_NUMBER_INT)
		);

		$result = $this->inbound_model->checkInboundDocNumberExist($params);

		echo json_encode($result);
	}

	/*---- END ADJ -----*/

    public function get_list1(){
        $param = '';
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $inbounds = $this->inbound_model->data($param)->get();
        $iTotalRecords = $this->inbound_model->data($param)->get()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;
        foreach($inbounds->result() as $value){
            //---------awal button action---------
            $action='';
            $action = $this->buttons->actions(array('edit'=>$value->id_inbound,'delete' => $value->id_inbound));

            $records["data"][] = array(
                '<input type="checkbox" name="id[]" value="1">',
                $_REQUEST['start']+$i,
                $value->kd_inbound,
                DateTime::createFromFormat('Y-m-d', $value->tanggal_inbound)->format('d/m/Y'),
                $value->inbound_document_name,
                $value->kd_supplier,
                $value->nama_status,
                $action
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function proses() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_inbound', 'Inbound Code', 'required|trim');
            $this->form_validation->set_rules('tanggal_inbound', 'Inbound Date', 'required|trim');
            $this->form_validation->set_rules('source', 'Source ID', 'required|trim');
            $this->form_validation->set_rules('id_inbound_document', 'Doc. Type ID', 'required|trim');


            if ($this->form_validation->run()) {
                $id = $this->input->post('id');
                $tanggal = $this->input->post("tanggal_inbound");

				$date = date('Y-m-d', strtotime(str_replace('/','-', trim($tanggal))));

                $data = array(
                    'kd_inbound' 			=> $this->input->post("kd_inbound"),
                    'tanggal_inbound'		=> $date,
                    'id_supplier' 			=> $this->input->post("source"),
                    'id_inbound_document' 	=> $this->input->post("id_inbound_document"),
					'user_id'				=> $this->session->userdata('user_id'),
					'outbound_peminjaman'	=> $this->input->post('outbound_peminjaman'),
					'remark'				=> $this->input->post('remark'),
					'warehouse'				=> $this->input->post('warehouse')
             	);

				//if(!empty($id))
					//$data['id_status_inbound'] = $this->input->post("id_inbound_status");

                $data2 = array();
                $arr_id_barang = $this->input->post("id_barang");
                $arr_item_quantity = $this->input->post("item_quantity");
                $arr_item_po = $this->input->post("po_item");
				$arr_cost_center = $this->input->post('cost_center');
				$len = count($arr_id_barang);

				if(!empty($arr_id_barang)){

					for($i = 0; $i < $len; $i++){
						$data2[] = array(
							'jumlah_barang'	=> $arr_item_quantity[$i],
							'id_barang'		=> $arr_id_barang[$i],
							'po_item'		=> $arr_item_po[$i],
							'cost_center'	=> $arr_cost_center[$i]
						);
					}

					// foreach ($arr_id_barang as $key => $value) {
						// $data2[] = array(
							// 'jumlah_barang' => $arr_item_quantity[$key],
							// 'id_barang' => $value
						// );
					// }
				}

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {

					$data['id_status_inbound'] = 1;

					$create = $this->inbound_model->create($data, $data2);

                    if ($create) {

						$this->db->trans_start();

							$this->inbound_model->addInDocument($data);

							$this->load_model('inbound_document_model');
							$this->load_model('referensi_barang_model');
							$this->load_model('api_staging_model');

							$inboundType = $this->inbound_document_model->get_by_id($data['id_inbound_document'])->row_array();

/*							$source = NULL;
							if($data['id_inbound_document'] == 2 || $data['id_inbound_document'] == 7 ){
								$this->load_model('customer_model');
								$source = $this->customer_model->get_by_id($data['id_supplier'])->row_array();
								$source = $source['customer_code'];
							}else{
								$this->load_model('referensi_supplier_model');
								$source = $this->referensi_supplier_model->get_by_id($data['id_supplier'])->row_array();
								$source = $source['kd_supplier'];
							}

							Title : send to staging

	                        $inbound_data = array('data'=>array(
	                            'DocDate'=>$date,
	                            'BaseCreatedDate'=>$date,
	                            'InbNo'=> $data['kd_inbound'],
	                            'Cancelled'=>'N',
	                            'InbType'=>$inboundType['inbound_document_name'],
	                            'Supplier'=>$source,
	                            'WhsFrom'=>'',
	                            'WhsTo'=>'KRG',
	                            'SyncStatus'=>'N',
	                            'SyncDate'=>'',
	                            'InbStatus'=>'',
	                            'ReceivingDate'=>$date
	                        ));

	                        $inb = $this->api_staging_model->_curl_process('insert','f_t_inbound_h',$inbound_data);

	                        if($inb->status=='001'){
	                            foreach($data2 as $key=>$l1){
	                            	$SKU = $this->referensi_barang_model->get_by_id($l1['id_barang'])->row_array();
	                                $inboundl1_data = array('data'=>array(
	                                    'DocEntry'=>$inb->id,
	                                    'LineNum'=>$key+1,
	                                    'SKU'=>$SKU['sku'],
	                                    'DocQty'=>$l1['jumlah_barang']
	                                ));
	                                $this->api_staging_model->_curl_process('insert','f_t_inbound_l1',$inboundl1_data);
	                            }
	                        }
                       */

	                    $this->db->trans_complete();

			            $proses_result['success'] = 1;
                        $proses_result['add'] = 1;

						/* INSERT LOG */
						$this->access_right->activity_logs('add','Inbound');

						/* END OF INSERT LOG */

                    }

                } else {
                    if ($this->inbound_model->update($data, $data2, $id)) {
						$data['id_status_inbound'] = $this->input->post('id_status_inbound');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Inbound');
						/* END OF INSERT LOG */
			            $proses_result['success'] = 1;
                        $proses_result['edit'] = 1;
                    }
                }
            } else {
                $proses_result['message'] = validation_errors();
            }
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete() {
        $result = array();
        if ($this->access_right->otoritas('delete', true)) {
            $id = $this->input->post("id");
            $result = $this->inbound_model->delete($id);
            $result['success'] = 1;
        } else {
            $result['no_delete'] = 1;
        }
        echo json_encode($result);
    }

    function sync_inbound(){
        $this->db->trans_start();
        $new_inbounds =  $this->inbound_model->get_new_inbound();
        $new=array();
        foreach($new_inbounds->result() as $inbound){
            // $inbound->DocEntry
            // $inbound->DocDate
            // $inbound->InbNo
            // $inbound->BaseCreatedDate
            // $inbound->Cancelled
            // $inbound->InbType
            // $inbound->Supplier
            // $inbound->WhsFrom
            // $inbound->WhsTo
            // $inbound->SyncStatus
            // $inbound->SyncDate
            // $inbound->InbStatus
            // $inbound->ReceivingDate
            $supplier =  $this->inbound_model->get_supplier($inbound->Supplier);
            $id_supplier = $supplier->num_rows()?$supplier->row()->id_supplier:0;
            $doc =  $this->inbound_model->get_inbound_document($inbound->InbType);
            $id_doc = $doc->num_rows()?$doc->row()->id_inbound_document:0;
            $data_inbound=array(
                'kd_inbound'=>$inbound->InbNo,
                'tanggal_inbound'=>$inbound->DocDate,
                'id_supplier'=>$id_supplier,
                'id_inbound_document'=>$id_doc,
                'id_status_inbound'=>1,
                'id_staging'=>$inbound->DocEntry
            );
            $exist = $this->inbound_model->check_inbound($inbound->InbNo);
            if($exist->num_rows()>0){
            	$new_inbound_id = $exist->row()->id_inbound;
                $this->inbound_model->update_inbound($data_inbound,$new_inbound_id);
            }else{
            	$new_inbound_id = $this->inbound_model->create_inbound($data_inbound);
            }

            $inbound_items = $this->inbound_model->get_inbound_item($inbound->DocEntry);
            foreach($inbound_items->result() as $item){
                // $item->DocEntry
                // $item->LineNum
                // $item->SKU
                // $item->DocQty
                $barang =  $this->inbound_model->get_barang($item->SKU);
                $id_barang = $barang->num_rows()?$barang->row()->id_barang:0;
                $data_item = array(
                    'id_inbound'=>$new_inbound_id,
                    'id_barang'=>$id_barang,
                    'jumlah_barang'=>$item->DocQty,
                    'id_staging'=>$item->LineNum,
                    'FinaCode' => $item->FinaCode
                );
                $exist_item = $this->inbound_model->check_inbound_barang(array('id_staging'=>$item->LineNum,'id_inbound'=>$new_inbound_id));
                if($exist_item->num_rows()>0){
                    $new_inbound_barang_id = $exist_item->row()->id_inbound_barang;
                    $this->inbound_model->update_inbound_barang($data_item,$new_inbound_barang_id);
                }else{
                    $new_inbound_barang_id = $this->inbound_model->create_inbound_barang($data_item);
                }
            }
            $SyncDate=date('Y-m-d H:i:s');
            $this->inbound_model->set_inbound_status($inbound->DocEntry,$SyncDate);
            $new[]=array('nomor'=>$inbound->InbNo,'id'=>$new_inbound_id);
        }
        $this->db->trans_complete();
        return $new;
    }

    function longpolling(){
	    date_default_timezone_set("Asia/Jakarta");
	    header('Cache-Control: no-cache');
	    header("Content-Type: text/event-stream\n\n");
	    while (1) {
            //check new inbound
            $new = $this->sync_inbound();
            //sleep(10);
            //$new = array(array('nomor'=>'PO170411-001','id'=>48),array('nomor'=>'PO170425-001','id'=>49));
	        $output = array('newc'=>$new);
	        echo "\nevent: sync\n";
	        echo 'data: '.json_encode($output);
	        echo "\n\n";
	        ob_flush();
	        flush();
            session_write_close();
	        sleep(30);
	    }
	}

    function manual_sync_inbound(){
        date_default_timezone_set("Asia/Jakarta");
        $new = $this->sync_inbound();
        $json = array('newc'=>$new);
        echo json_encode($json);
    }

    public function getInDocument(){

    	$id = $this->input->post('id');
    	if(!empty($id)){
	    	$data= $this->inbound_model->getInDocument($id);

	    	$output = array();
	    	if($data){

	    		$output['param']	= "";
	    		$output['status']	= true;
	    		$output['message']	= "Success get Inbound Document Number";
	    		$output['result']	= $data;

	    	}else{

	    		$output['param'] 	= "";
	    		$output['status'] 	= false;
	    		$output['message'] 	= "Can't get Inbound Document Number";

	    	}

	    	echo json_encode($output);
	    }
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Inbound Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

	    	$this->load->library('import');

	        $path = 'uploads/excel/'. $this->class_name .'/';
	        $upload = $this->import->upload($path);

	        if($upload['status']){

	            $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

	            if(!$arr){

	                $result = $this->import->errorMessage('Wrong Template');

	            }else{

			        $field = [
			            'No Document'=> 'document_name',
			            'Date' => 'date',
			            'Type' => 'document_id',
			            'Source' => 'supplier_id',
			            'Remark' => 'remark',
			            'Material ID' => 'item_id',
			            'Qty' => 'qty',
			            'Warehouse' => 'warehouse',
			        ];

	                $data = $this->import->toFieldTable($field, $arr);

	                if(!$data){

	                    $result = $this->import->errorMessage('Wrong Template');

	                }else{

	                	$process = $this->inbound_model->importFromExcel($data);

	                	if($process){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

	                	}else{

			                $result = $this->import->errorMessage($upload['data']);

	                	}

	                }

	            }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }

        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

   }

   public function GetPOItem(){

   	$poNumber = $this->input->post('po_number');
   	if(empty($poNumber)){
   		show_404();
   	}else{

   		$this->load_model('api_staging_model');

        $apiPOHEADER = "po_maintain/POHeaders(':$poNumber')";
        $POHeaders 	= $this->api_staging_model->_curl_process_get($apiPOHEADER);

        if(isset($POHeaders->d)){

        	$poData = $POHeaders->d;

        	if(empty($poData->ReleaseIsNotCompleted)){

	        	$data['status'] = true;

	        	// Check Supplier
	        	$getSupplier = $this->db->get_where('suppliers',['code'=>$poData->Supplier])->row_array();
	        	if($getSupplier){

			        $apiPOITEM = $apiPOHEADER."/Items";
			        $POItems	= $this->api_staging_model->_curl_process_get($apiPOITEM);

			        if(isset($POItems->d)){


			        	$data['results']['PONumber'] = $poData->PurchaseOrder;
			        	$data['results']['Supplier'] = $getSupplier['id'];
			        	$data['results']['PODate'] 	 = date("d/m/Y", substr($poData->PurchaseOrderDate, 6, 10));
			        	$data['results']['Items'] = "";

			        	$itemData = $POItems->d;

			        	// Check Item Master
			        	foreach ($itemData->results as $item) {
			        		if($item->IsDeleted){
			        			continue;
			        		}

			        		$this->db
			        			->select('itm.*, unt.name as nama_satuan')
			        			->from('items itm')
			        			->join('units unt','unt.id=itm.unit_id','left')
			        			->where('sku',$item->Material);

			        		$getItem = $this->db->get()->row_array();

			        		if(!$getItem){
			        			$data['status'] = false;
			        			$data['message'][] = "This Material '<b>".$item->Material."</b>' not exist!";
			        		}else{

			        			$dataItem = array();

						    	$this->load_model('cost_center_model');
						        $dataItem['cost_center'] = $this->cost_center_model->options('-- Pilih Cost Center --');
						        $dataItem['enable_cost_center'] = 0;

					            $this->load_model('referensi_barang_model');
					            $dataItem['items'] = $this->referensi_barang_model->options();
				                $dataItem['inbound_barang'] = array(
				                	'id_barang'		=> $getItem['id'],
				                	'jumlah_barang' => $item->PurgDocItemQuantity,
				                	'nama_satuan'	=> $getItem['nama_satuan'],
				                	'po_item'		=> $item->PurchaseOrderItem
				                );

				                $data['results']['Items'] .= $this->load->view("inbound/add_item_row", $dataItem, true);

			        		}


			        	}

			        }else{

			        	$data['status'] = false;
			        	$data['message'] = $POItems->error->message->value;

			        }
			    }else{

	        		$data['status'] = false;
	        		$data['message'] = "This Supplier '<b>".$poData->Supplier."</b>' not exist !";

			    }

			}else{

 		      	$data['status'] = false;
        		$data['message'] = "PO SAP is Not Released Yet";

			}
        }else{

        	$data['status'] = false;

        	if($POHeaders->error->message->value === "Resource not found for segment 'POHeader'."){
	        	$data['message'] = "PO Number is not created";
        	}else{
	        	$data['message'] = $POHeaders->error->message->value;
    	    }

        }

        echo json_encode($data);
   	}

   }

    public function getInboundDocument(){

        $results = array();

        $params = array(
            'warehouse' => $this->input->post('warehouse')
        );

        $this->load_model('inbound_document_model');
        $data = $this->inbound_document_model->getDocumentByWarehouse($params);

        if($data->num_rows() > 0){

            $results['data'] = $data->result_array();
            $results['response'] = true;

        }else{

            $results['response'] = false;
            $results['message'] = 'Please create document type first!';

        }

        echo json_encode($results);

    }

    public function updateInboundDocumentNumber(){

    	$id = $this->input->post('id');
    	if(!empty($id)){
	    	$data= $this->inbound_model->updateInboundDocumentNumber($id);

	    	$output = array();
	    	if($data){

	    		$output['param']	= "";
	    		$output['status']	= true;
	    		$output['message']	= "Success get Inbound Document Number";
	    		$output['result']	= $data;

	    	}else{

	    		$output['param'] 	= "";
	    		$output['status'] 	= false;
	    		$output['message'] 	= "Can't get Inbound Document Number";

	    	}

	    	echo json_encode($output);
	    }
    }

	public function download_template_inbound_stock(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Inbound Stock Import Template.xlsx');

            echo json_encode($data);
        }
    }

	public function importFromExcelInboundStock(){

        if ($this->access_right->otoritas('import')) {

	    	$this->load->library('import');

	        $path = 'uploads/excel/'. $this->class_name .'/';
	        $upload = $this->import->upload($path);

	        if($upload['status']){

	            $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

	            if(!$arr){

	                $result = $this->import->errorMessage('Wrong Template');

	            }else{

			        $field = [
			            'SN'			=> 'serial_number',
			            'Bin Loc' 		=> 'location',
			            'No SKU'		=> 'item_code',
			            'Quantity'		=> 'quantity'
			        ];

	                $data = $this->import->toFieldTable($field, $arr);

	                if(!$data){

	                    $result = $this->import->errorMessage('Wrong Template');

	                }else{

	                	$process = $this->inbound_model->importFromExcelInboundStock($data,$this->input->post('warehouse'));

	                	if($process['status']){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

	                	}else{

			                $result = $this->import->errorMessage($process['message']);

	                	}

	                }

	            }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }

        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

	}
	
	public function dataInsert($warehouseId=2879){

    	$this->db->trans_start();

    	/* Create Inbound */
    	$dataInbound = array(
    		"code"	=> "Manual Inbound 1",
    		"date"	=> date('Y-m-d'),
    		"warehouse_id"	=> $warehouseId,
    		"document_id"	=> 44,
    		"user_id"		=> $this->session->userdata('user_id'),
    		"status_id"		=> 4,
    		"supplier_id"	=> 7
    	);

    	$this->db->insert('inbound',$dataInbound);
    	$inbId = $this->db->insert_id();

    	$sql = "INSERT INTO inbound_item(inbound_id,item_id,qty) SELECT CAST($inbId as INTEGER) as inbound_id, id as item_id, CAST('100' as INTEGER) FROM items";
    	$this->db->query($sql);

    	/* Create Inbound */
    	$dataReceiving = array(
    		"code"	=> "Manual Receiving",
    		"date"	=> date('Y-m-d'),
    		"st_receiving" => 1,
    		"start_tally"	=> date('Y-m-d h:i:s'),
    		"finish_tally"	=> date('Y-m-d h:i:s'),
    		"start_putaway"	=> date('Y-m-d h:i:s'),
    		"finish_putaway"	=> date('Y-m-d h:i:s'),
    		"warehouse_id"	=> $warehouseId,
    		"user_id"		=> $this->session->userdata('user_id'),
    		"po_id"			=> $inbId
    	);

    	$this->db->insert('receivings',$dataReceiving);
    	$recId = $this->db->insert_id();

    	$sql = "INSERT INTO item_receiving(receiving_id,item_id,qty) SELECT CAST($recId as INTEGER) as receiving_id, id as item_id, CAST('100' as INTEGER) FROM items";
    	$this->db->query($sql);

    	$items = $this->db->select('*')->from('items')->where('def_loc_id IS NOT NULL')->get()->result_array();

    	foreach ($items as $item) {

    		$itemReceiving = $this->db->get_where('item_receiving',['receiving_id'=>$recId,'item_id'=>$item['id']])->row_array();

    		$sql = "SELECT def_loc_id as location_id FROM items WHERE id = ".$item['id']." LIMIT 1";
    		$location	= $this->db->query($sql)->row_array();

    		$n = 1;

    		$dataIrd = array();
    		for ($i=0; $i < $n; $i++) { 

    			$sn_visual = "IL".date('Ymd').strtoupper($item['code'])."01"; 
    			// $snHex = strtoupper(bin2hex($sn_visual));
    			$snHex = $sn_visual;

    			$dataIrd[] = array(
    				"item_receiving_id" => $itemReceiving['id'],
    				"unique_code"		=> $snHex,
    				"tgl_in"			=> date('Y-m-d h:i:s'),
    				"putaway_time"		=> date('Y-m-d h:i:s'),
    				"location_id"		=> $location['location_id'],
    				"item_id"			=> $item['id'],
    				"rfid_tag"			=> $sn_visual,
    				"st_receive"		=> 1,
    				"first_qty"			=> 100,
    				"last_qty"			=> 100,
    				"qc_id"				=> 1
    			);

    		}

    		$this->db->insert_batch('item_receiving_details',$dataIrd);

    	}

    	$this->db->trans_complete();

    }

}

/* End of file referensi_barang.php */
/* Location: ./application/controllers/referensi_barang.php */

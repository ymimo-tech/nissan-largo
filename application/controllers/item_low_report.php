<?php

class item_low_report extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load_model(array($this->class_name . '_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Item Low';
        $data['page'] = $this->class_name . '/index';
		
		$this->include_required_file($data);
		
		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/report/lr-itemlow-report.js";
		$data['today'] = date('d/m/Y');
		
		$this->load_model('item_stok_model');
		$data['item'] = $this->item_stok_model->getItem();
		//$data['item'] = $this->inv_transfer_model->getItem();
		//$data['location'] = $this->inv_transfer_model->getLocation();
		 
		/* INSERT LOG */
		$this->access_right->activity_logs('view','Item Low Report');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }
	
	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}
	
	//REQUEST	
	public function export(){
		$data = array();
		
		$params = array(
			'id_barang'	=> $this->input->post('id_barang'),
			'warehouse'	=> $this->input->post('warehouse')
		);
		
		$data['page'] = $this->class_name . '/item_low';
		$data['params'] = $params;
		$data['data'] = $this->item_low_report_model->getListRaw($params);	
		
        $html = $this->load->view($data['page'], $data, true);
        $path = "item_low_report.pdf";
 
        ini_set('memory_limit', $this->config->item('allocated_memory')); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            10, // margin bottom
            10, // margin header
            10
		); // margin footer

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can  
		
    }
	
	public function getList(){
		$result = array();
		
		$params = array(
			'id_barang'		=> $this->input->post('id_barang'),
			'warehouse'		=> $this->input->post('warehouse')
		);
		
		$result = $this->item_low_report_model->getList($params);
		
		echo json_encode($result);
	}
}
<?php

class peminjaman_list extends CI_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load->model(array('peminjaman_model'));
        $this->load->library("randomidgenerator");
    }

    public function index() {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Peminjaman List';
        $data['page'] = 'peminjaman/peminjaman_list';
		
		$this->include_required_file($data);
		
		$data['jsPage'][] = "assets/global/plugins/mimo-periode-filter/mimo-periode-filter.js";
		$data['jsPage'][] = "assets/pages/scripts/peminjaman/lr-peminjaman-list.js";
		$data['today'] = date('d/m/Y');
		
		$data['customer'] = $this->peminjaman_model->getCustomer();
		
		//$data['item'] = $this->inv_transfer_model->getItem();
		//$data['location'] = $this->inv_transfer_model->getLocation();
		 
		/* INSERT LOG */
		$this->access_right->activity_logs('view','Peminjaman List');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);
    }
	
	private function include_required_file(&$data){
		$data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/pages/css/lr-inbound.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['csss'][] = "assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css";
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
	}
	
	//REQUEST	
	public function export(){
		$data = array();
		
		$params = array();
		
		$data['page'] = 'peminjaman/peminjaman_export';
		$data['params'] = $params;
		$data['data'] = $this->peminjaman_model->getReportListRaw($params);	
		
        $html = $this->load->view($data['page'], $data, true);
        $path = "peminjaman_list.pdf";
 
        ini_set('memory_limit', $this->config->item('allocated_memory')); // boost the memory limit if it's low ;)

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            10, // margin top
            10, // margin bottom
            10, // margin header
            10
		); // margin footer

		$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822).'. Printed by Largo'); // Add a footer for good measure ;)

		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($path, 'I'); // save to file because we can  
		
    }
	
	public function getList(){
		$result = array();
		
		$params = array(
			'id_customer'		=> filter_var(trim($this->input->post('id_customer')), FILTER_SANITIZE_NUMBER_INT),
			'from'				=> filter_var(trim($this->input->post('from')), FILTER_SANITIZE_STRING),
			'to'				=> filter_var(trim($this->input->post('to')), FILTER_SANITIZE_STRING)
		);
		
		$result = $this->peminjaman_model->getList($params);
		
		echo json_encode($result);
	}
}
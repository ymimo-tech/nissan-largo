<?php 

class customer extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		$this->access_right->check();
		$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load_model(array($this->class_name . '_model'));
    }

    public function index() {
		
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Master Customer';
        $data['page'] = $this->class_name . '/index';
        $data['page_class'] = $this->class_name;
        // $data['data_source'] = base_url($this->class_name . '/load');
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
		$data['jsPage'][] = 'assets/custom/js/share.js';
        $data['jsPage'][] = "assets/pages/scripts/customer/lr-customer.js";

        /* Get Data Select*/
        $this->load_model('warehouse_model');
        $data['warehouses'] = $this->warehouse_model->options(); // Get Data Warehoues

        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }
		 
		/* INSERT LOG */
		$this->access_right->activity_logs('view','Referensi Customer');
        /* END OF INSERT LOG */
        $this->load->view('template', $data);

    }
	
	//REQUEST
	public function export(){
		$this->load->library('PHPExcel');
		
		$params = array();
		
		$result = $this->customer_model->getCustomer($params);
		$iLen = count($result);
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Master-Customer")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        
		$sheet = $objPHPExcel->getActiveSheet();
		
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		
		$sheet->setCellValueByColumnAndRow(0, 1, 'Customer Code')->getStyle('A1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(1, 1, 'Customer Name')->getStyle('B1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(2, 1, 'Address')->getStyle('C1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(3, 1, 'City')->getStyle('D1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(4, 1, 'Postal Code')->getStyle('E1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(5, 1, 'Country')->getStyle('F1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(6, 1, 'Phone')->getStyle('G1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(7, 1, 'Fax')->getStyle('H1')->getFont()->setBold(true);
		$sheet->setCellValueByColumnAndRow(8, 1, 'Contact Person')->getStyle('I1')->getFont()->setBold(true);
 
        $row = 2;
        for($i = 0; $i < $iLen; $i++){
            //$col = 0;
            //foreach ($fields as $field)
            //{

			$sheet->setCellValueByColumnAndRow(0, $row, $result[$i]['customer_code']);
			$sheet->setCellValueByColumnAndRow(1, $row, $result[$i]['customer_name']);
			$sheet->setCellValueByColumnAndRow(2, $row, $result[$i]['address']);
			$sheet->setCellValueByColumnAndRow(3, $row, $result[$i]['city']);
			$sheet->setCellValueByColumnAndRow(4, $row, $result[$i]['postal_code']);
			$sheet->setCellValueByColumnAndRow(5, $row, $result[$i]['country']);
			$sheet->setCellValueByColumnAndRow(6, $row, $result[$i]['phone']);
			$sheet->setCellValueByColumnAndRow(7, $row, $result[$i]['fax']);
			$sheet->setCellValueByColumnAndRow(8, $row, $result[$i]['contact_person']);
			
                //$col++;
            //}
 
            $row++;
        }
 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		
		ob_end_clean();
		
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Master-Customer'.'.xlsx"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');
	}
	
	public function delete(){		
		$result = array();
		
        if ($this->access_right->otoritas('delete', true)) {
			
            $id = $this->input->post("customer_id");
            $result = $this->customer_model->delete($id);
			
        } else {
			
            $result['status'] = 'ERR';
			$result['no_delete'] = 1;
			
        }
		
        echo json_encode($result);
	}
	
	public function getEdit(){
		$result = array();
		
		$params = array(
			'customer_id'	=> filter_var(trim($this->input->post('customer_id')), FILTER_SANITIZE_NUMBER_INT)
		);

		
		$result = $this->customer_model->getEdit($params);

        $warehouses = $this->customer_model->getCustomerWarehouse($params['customer_id']);
        $result['warehouses'] = array();
        foreach ($warehouses as $wh) {
            $result['warehouses'][] = $wh['warehouse_id'];
        }
		
		echo json_encode($result);
	}
	
	public function save(){
		$result = array();
		
		if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
		
			$params = array(
				'customer_id'		=> filter_var(trim($this->input->post('customer_id')), FILTER_SANITIZE_NUMBER_INT),
				'customer_code'		=> filter_var(trim($this->input->post('customer_code')), FILTER_SANITIZE_STRING),
				'customer_name'		=> filter_var(trim($this->input->post('customer_name')), FILTER_SANITIZE_STRING),
				'customer_address'	=> filter_var(trim($this->input->post('customer_address')), FILTER_SANITIZE_STRING),
				'customer_city'		=> filter_var(trim($this->input->post('customer_city')), FILTER_SANITIZE_STRING),
				'customer_postcode'	=> filter_var(trim($this->input->post('customer_postcode')), FILTER_SANITIZE_STRING),
				'customer_country'	=> filter_var(trim($this->input->post('customer_country')), FILTER_SANITIZE_STRING),
				'customer_phone'	=> filter_var(trim($this->input->post('customer_phone')), FILTER_SANITIZE_STRING),
				'customer_fax'		=> filter_var(trim($this->input->post('customer_fax')), FILTER_SANITIZE_STRING),
				'customer_cp'		=> filter_var(trim($this->input->post('contact_person')), FILTER_SANITIZE_STRING),
				'warehouse'			=> $this->input->post('warehouse')
			);
			
			$result = $this->customer_model->save($params);
			
		}else{
			
            if(!$this->access_right->otoritas('add')) {
                $result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $result['no_edit'] = 1;
            }
			
		}
		
		echo json_encode($result);
	}
	
	public function checkCustomerCode(){
		$result = array();
		
		$params = array(
			'customer_id'	=> filter_var(trim($this->input->post('customer_id')), FILTER_SANITIZE_NUMBER_INT),
			'customer_code'	=> filter_var(trim($this->input->post('customer_code')), FILTER_SANITIZE_STRING)
		);
		
		$result = $this->customer_model->checkCustomerCode($params);
		
		echo json_encode($result);
	}
	
	public function getList(){
		$result = array();
		
		$params = array();
		$result = $this->customer_model->getList($params);
		
		echo json_encode($result);
	}

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Customer Master Import Template.xlsx');

            echo json_encode($data);
        }
    }

    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        'Customer Name'     		=>	'customer_name',
                        'Customer Code'     		=>	'customer_code',
                        'Customer Address'  		=>	'customer_addr',
                        'Customer City'				=>	'customer_city',
                        'Customer Postal Code'		=>	'customer_postal_code',
                        'Customer Country'			=>	'customer_country',
                        'Customer Phone'			=>	'customer_phone',
                        'Customer Fax'				=>	'customer_fax',
                        'Customer Contact Person'	=> 'customer_contact_person',
                        'Warehouse'					=> 'warehouse'
                    ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->customer_model->importFromExcel($data);

                        if($process['status']){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage($process['message']);

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }
 
        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }
}
<?php

/**
 * Description of roles
 *
 * @author Warman Suganda
 */
class roles extends MY_Controller {

    private $class_name;
    private $limit = 10;

    public function __construct() {
        parent::__construct();

        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);

        // Global Model
        $this->load_model($this->class_name . '_model');
    }

    function index() {
        $data['title'] = "Roles";
        $data['subTitle'] = '<i class="icon-check"></i> Role Management';
        $data['page'] = $this->class_name . '/index';

        $this->include_required_file($data);
        $data['jsPage'][] = 'assets/pages/scripts/lr-roles.js';

        /* Import */
        if($this->access_right->otoritas('import')){
            $data['page'] = [$data['page'],'components/form-import'];
            $data['jsPage'][] = "assets/pages/scripts/lr-import.js";
        }

        $this->access_right->activity_logs('view','Manajemen Role');
        $this->load->view('template', $data);
    }

    public function get_data() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post("id");
            $role = $this->roles_model->get_by_id($id)->row();

            $process_result = $role;
        } else {
            $process_result['no_data'] = 1;
        }
        echo json_encode($process_result);
    }

    public function save_role() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post('id');

            $data = array(
                'grup_nama' => $this->input->post('nama_role'),
                'grup_deskripsi' => $this->input->post('deskripsi'),
                'otoritas_data' => $this->input->post('otoritas_data'),
                'otoritas_menu_view' => $this->input->post('otoritas_menu_view'),
                'otoritas_menu_add' => $this->input->post('otoritas_menu_add'),
                'otoritas_menu_edit' => $this->input->post('otoritas_menu_edit'),
                'otoritas_menu_delete' => $this->input->post('otoritas_menu_delete'),
                'otoritas_menu_approve' => $this->input->post('otoritas_menu_approve'),
                'otoritas_menu_export' => $this->input->post('otoritas_menu_export'),
                'otoritas_menu_print' => $this->input->post('otoritas_menu_print')
            );

            /*
             * If : $id == '', lakukan proses create data
             * Else : lakukan proses update
             */

            if ($id == '') {
                if ($this->roles_model->create_role($data)) {
                    $insert_id = $this->session->userdata('insert_id');
                    $this->access_right->activity_logs('add','Manajemen Role');
                }
            } else {
                if ($this->roles_model->update_role($data, $id)) {
                    $insert_id = $id;
                    $this->access_right->activity_logs('edit','Manajemen Role');
                }
            }

            $process_result['success'] = 1;
        } else {
            $process_result['success'] = 0;

            if(!$this->access_right->otoritas('add')) {
                $process_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $process_result['no_edit'] = 1;
            }
        }

        echo json_encode($process_result);
    }

    public function save_role_handheld() {
        $process_result;
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post('id');

            $data = array(
                'grup_nama' => $this->input->post('nama_role'),
                'grup_deskripsi' => $this->input->post('deskripsi'),
                'otoritas_data' => $this->input->post('otoritas_data'),
                'otoritas_menu_view' => $this->input->post('otoritas_menu_view'),
                'otoritas_menu_add' => $this->input->post('otoritas_menu_add'),
                'otoritas_menu_edit' => $this->input->post('otoritas_menu_edit'),
                'otoritas_menu_delete' => $this->input->post('otoritas_menu_delete'),
                'otoritas_menu_approve' => $this->input->post('otoritas_menu_approve'),
                'otoritas_menu_export' => $this->input->post('otoritas_menu_export'),
                'otoritas_menu_print' => $this->input->post('otoritas_menu_print')
            );

            /*
             * If : $id == '', lakukan proses create data
             * Else : lakukan proses update
             */

            if ($id == '') {
                if ($this->roles_model->create_role_handheld($data)) {
                    $insert_id = $this->session->userdata('insert_id');
                    $this->access_right->activity_logs('add','Manajemen Role');
                }
            } else {
                if ($this->roles_model->update_role_handheld($data, $id)) {
                    $insert_id = $id;
                    $this->access_right->activity_logs('edit','Manajemen Role');
                }
            }

            $process_result['success'] = 1;
        } else {
            $process_result['success'] = 0;

            if(!$this->access_right->otoritas('add')) {
                $process_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $process_result['no_edit'] = 1;
            }
        }

        echo json_encode($process_result);
    }

    public function load_role() {
        $process_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load_model('menu_group_model');
            $this->load_model('menu_model');

            $data['menu_group'] = $this->menu_group_model->get_data();
            $data['menu'] = $this->menu_model->parsing_data();

            $id = $this->input->post("id");
            $process_result['grup'] = $this->roles_model->get_by_id($id)->row();
            $data['roles'] = array(
                'is_view' => array(), 'is_add' => array(), 'is_edit' => array(), 'is_delete' => array(), 'is_approve' => array(), 'is_import' => array(), 'is_print' => array()
            );
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->roles_model->get_by_id($id);
                $roles = $this->roles_model->parsing_roles(array('grup_id' => $id));
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                    $data['roles'] = $roles;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;
            $roleTableContent = $this->load->view($this->class_name . '/role_form', $data, true);
            
            $process_result['success'] = 1;
            $process_result['content'] = $roleTableContent;
        } else {
            $process_result['success'] = 0;
            if(!$this->access_right->otoritas('add')) {
                $process_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $process_result['no_edit'] = 1;
            }
        }

        echo json_encode($process_result);
    }

    public function load_role_handheld() {
        $process_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load_model('menu_group_model');
            $this->load_model('menu_model');

            $data['menu_group'] = $this->menu_group_model->get_data();
            $data['menu'] = $this->menu_model->parsing_data();

            $id = $this->input->post("id");
            $process_result['grup'] = $this->roles_model->get_by_id_handheld($id)->row();
            $data['roles'] = array(
                'is_view' => array(), 'is_add' => array(), 'is_edit' => array(), 'is_delete' => array(), 'is_approve' => array(), 'is_import' => array(), 'is_print' => array()
            );
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->roles_model->get_by_id($id);
                $roles = $this->roles_model->parsing_roles_handheld(array('grup_id' => $id));
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                    $data['roles'] = $roles;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;
            $roleTableContent = $this->load->view($this->class_name . '/role_form_handheld', $data, true);
            
            $process_result['success'] = 1;
            $process_result['content'] = $roleTableContent;
        } else {
            $process_result['success'] = 0;
            if(!$this->access_right->otoritas('add')) {
                $process_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $process_result['no_edit'] = 1;
            }
        }

        echo json_encode($process_result);
    }

    public function get_list(){

        $columns = array(
            0 => '',
            1 => 'grup_nama',
            2 => 'grup_deskripsi'
        );

        $param = '';
        $this->db->order_by($columns[$_REQUEST['order'][0]['column']],$_REQUEST['order'][0]['dir']);
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $users = $this->roles_model->get_data();
        $iTotalRecords = $this->roles_model->get_data()->num_rows();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $i=1;

        foreach($users->result() as $value){
            $records["data"][] = array(
                $_REQUEST['start']+$i,
                $value->grup_nama,
                $value->grup_deskripsi,
                $this->buttons->actions(
                    array(
                        'edit'=>$value->grup_id,
                        'edit-role'=>$value->grup_id,
                        // 'edit-role-handheld'=>$value->grup_id,
                        'delete' =>$value->grup_id
                    )
                )
            );
            $i++;
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    private function include_required_file(&$data){
        $data['page_class'] = $this->class_name;
        $data['csss'][] = "assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css";
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js';
        $data['jsPage'][] = 'assets/global/plugins/jquery-validation/js/additional-methods.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootstrap-toastr/toastr.min.js';
        $data['jsPage'][] = 'assets/global/plugins/bootbox/bootbox.min.js';
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array("Nama Role", 1, 1, "Deskripsi", 1, 1, "Aksi", 1, 1);
        $table->header = $header;
        $table->align = array('nik' => 'center', 'tanggal' => 'center', 'menu_urutan' => 'center', 'action' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->numbering = true;

        $table->model = $this->class_name . '_model->data_table';
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate($table);

        echo json_encode($data);
    }

    public function add($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load_model('menu_group_model');
            $this->load_model('menu_model');

            $data['menu_group'] = $this->menu_group_model->get_data();
            $data['menu'] = $this->menu_model->parsing_data();

            $data['id'] = $id;
            $data['roles'] = array(
                'is_view' => array(), 'is_add' => array(), 'is_edit' => array(), 'is_delete' => array(), 'is_approve' => array(), 'is_import' => array(), 'is_print' => array()
            );
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->roles_model->get_by_id($id);
                $roles = $this->roles_model->parsing_roles(array('grup_id' => $id));
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                    $data['roles'] = $roles;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;

            $this->load->view($this->class_name . '/form', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
        $this->add($id);
    }

    public function proses() {
        $proses_result = array();
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post('id');

            $data = array(
                'grup_nama' => $this->input->post("grup_name"),
                'grup_deskripsi' => $this->input->post("description"),
                'otoritas_data' => 0
            );

            /*
             * If : $id == '', lakukan proses create data
             * Else : lakukan proses update
             */

            if ($id == '') {
                if ($this->roles_model->create($data)) {
                    $proses_result['add'] = 1;
                    /* INSERT LOG */
                    $this->access_right->activity_logs('add','roles');
                    /* END OF INSERT LOG */
                }
            } else {
                if ($this->roles_model->update($data, $id)) {
                    $proses_result['edit'] = 1;
                    /* INSERT LOG */
                    $this->access_right->activity_logs('edit','roles');
                    /* END OF INSERT LOG */
                }
            }
            $proses_result['success'] = 1;
        } else {
            if(!$this->access_right->otoritas('add')) {
                $proses_result['no_add'] = 1;
            }

            if(!$this->access_right->otoritas('edit')) {
                $proses_result['no_edit'] = 1;
            }
        }
        echo json_encode($proses_result);
    }

    public function delete(){

        $results = array();

        $id=$this->input->post('id');

        if ($this->access_right->otoritas('delete', true)) {

            if ($this->roles_model->delete($id)) {

                $results['success'] = 1;
                $results['message'] = 'Proses hapus data berhasil.';
                $this->access_right->activity_logs('delete','Manajemen Role');

            }else{

                $results['message'] = 'Proses hapus data gagal.';

            }

        }else{
            $results['no_delete'] = 1;
        }

        echo json_encode($results);
    }

    public function download_template(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $data = $this->import->downloadTemplate('Role Master Import Template.xlsx');

            echo json_encode($data);
        }
    }


    public function importFromExcel(){

        if ($this->access_right->otoritas('import')) {

            $this->load->library('import');

            $path = 'uploads/excel/'. $this->class_name .'/';
            $upload = $this->import->upload($path);

            if($upload['status']){

                $arr = $this->import->excelToArray($path.$upload['data']['file_name']);

                if(!$arr){

                    $result = $this->import->errorMessage('Wrong Template');

                }else{

                    $field = [
                        'Role Name'         => 'role_name',
                        'Role Description'  => 'role_description',
                        'View'              => 'view',
                        'Add'               => 'add',
                        'Edit'              => 'edit',
                        'Delete'            => 'delete',
                        'Approve'           => 'approve',
                        'Import'            => 'import',
                        'Export'            => 'export'
                     ];

                    $data = $this->import->toFieldTable($field, $arr);

                    if(!$data){

                        $result = $this->import->errorMessage('Wrong Template');

                    }else{

                        $process = $this->roles_model->importFromExcel($data);

                        if($process){

                            $result = ['status'=> 1, 'message'=>'Import Successfully'];

                        }else{

                            $result = $this->import->errorMessage('Please check again your data !');

                        }

                    }

                }

            }else{
                $result = $this->import->errorMessage($upload['data']);
            }

        } else {

            $result['status'] = 0;
            $result['message'] = 'You dont have authority to import data.';

        }
 
        $this->output
            ->set_content_type('application/json')
                ->set_output(json_encode($result));

    }


}

/* End of file roles.php */
/* Location: ./application/controllers/roles.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_repack_tally extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load_model('api_setting_model');
		$this->load_model('api_repack_tally_model');
	}

	public function index()
	{
		$data['param']		= null;
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;

		echo json_encode($data);
	}

	public function loadDocument(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$results = $this->api_repack_tally_model->loadDocument();

				if(count($results) > 0){

					$data['param']				= '-';
					$data['status']				= 200;
					$data['message']			= '-';
					$data['response']			= true;
					$data['resultsDocument']	= $results;
					$data['resultsDetail']		= $this->api_repack_tally_model->loadDetail($results);

				}else{

					$data['param']		= '-';
					$data['status']		= 400;
					$data['message']	= 'There are document to be procesed.';
					$data['response']	= false;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		echo json_encode($data);

	}

	public function get_item($kd_barang=''){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $kd_barang == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$result = $this->api_repack_tally_model->get_item($kd_barang)->result_array();
				if(count($result) > 0){

					$data['param']				= '-';
					$data['status']				= 200;
					$data['message']			= '-';
					$data['response']			= true;
					$data['results']			= $result;

				}else{

					$data['param']		= null;
					$data['status']		= 400;
					$data['message']	= 'Invalid Item Code.';
					$data['response']	= false;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		echo json_encode($data);

	}

	public function post(){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$params['id_order_kitting']	= $this->input->post('id_order_kitting');
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_batch']			= $this->input->post('batch_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['weight']			= $this->input->post('weight');
				$params['kd_qc']			= $this->input->post('qc_code');
				$params['tgl_in']			= $this->input->post('tgl_in');
				$params['tgl_exp']			= $this->input->post('tgl_exp');
				$params['uname']			= $this->input->post('uname');
				$params['kd_parent']		= $this->input->post('parent_code');
				$params['qty']				= $this->input->post('qty');

				if($params['qty'] > 0){

					$checkQty = $this->api_repack_tally_model->validQty($params);
					if($checkQty){
						$this->api_repack_tally_model->postSN($params);
						
						$data['param']		= $params['kd_unik'];
						$data['status']		= 200;
						$data['response']	= true;
						$data['message']	= 'Data has been inserted.';

					}else{

						$data['param']		= '-';
						$data['status']		= 400;
						$data['message']	= "this material item has been picked.";
						$data['response']	= false;

					}

				}else{

					$data['param']		= '-';
					$data['status']		= 400;
					$data['message']	= "Qty must greater than 0";
					$data['response']	= false;

				}

			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}

		echo json_encode($data);

	}


	public function validSN()
	{
		$serial_number = $this->input->post('serial_number');
		$method = $_SERVER['REQUEST_METHOD'];

		if($method != 'POST' || $serial_number == ''){
			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$auth = $this->api_setting_model->user_authentication();
			if($auth){

				$params['kd_unik'] = $serial_number;
				$validSN = $this->api_repack_tally_model->validSN($params);
				if($validSN){
					$data['param']		= $serial_number;
					$data['status']		= 'REGISTERED';
					$data['message']	= $serial_number . ' has been registered before.';
					$data['response']	= false;
				} else{
					$data['param']		= $serial_number;
					$data['status']		= 'AVAILABLE';
					$data['message']	= $serial_number . ' is not registered.';
					$data['response']	= true;
				}
			} else{
				$data['param']		= null;
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}
	
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require "api/Api.php";
class Api_picking extends Api {

	function __construct() {

		parent::__construct();
		$this->auth->user_authentication();
		$this->load_model('api_picking_model');

	}

	public function get_document()
	{

		$this->method("GET");

		$data['param']		= 'PICKING';
		$data['status']		= 200;
		$data['response']	= true;
		$data['message']	= 'Get Document for Picking.';
		$data['results']	= $this->api_picking_model->get_document();

		$this->toJson($data);

	}

	public function load()
	{

		$this->method("GET");

		$payload['param']		= 'PICKING';
		$payload['status']		= 200;
		$payload['message']	= 'Load data for available picking list.';
		$payload['response']	= true;
		$payload['results']	= $this->api_picking_model->load()->result_array();

		$this->toJson($payload);

	}

	public function get($pickingCode = '')
	{
		$this->method("GET");

		$get = $this->api_picking_model->get($pickingCode)->result_array();
		if($get){
			$payload['param']		= $pickingCode;
			$payload['status']		= 200;
			$payload['message']		= $pickingCode . ' is available.';
			$payload['response']	= true;
			$payload['results']		= $get;
			$payload['locations']	= $get;
		}else{
			$this->errorMessage($pickingCode." is not available.",$pickingCode);
		}

		$this->toJson($payload);
	}

	public function get_picked($pickingCode = '')
	{

		$this->method("GET");

		if(!empty($pickingCode)){
			$picked = $this->api_picking_model->get($pickingCode)->row_array();
			if($picked){

				if($picked['pl_status'] != 1){

					$payload['param']		= $pickingCode;
					$payload['status']		= 200;
					$payload['message']		= $pickingCode . ' is available.';
					$payload['response']	= true;
					$payload['results']		= $this->api_picking_model->get_picked($pickingCode)->result_array();

				}else{
					$this->errorMessage($pickingCode." has been picked before.",$pickingCode);
				}
			}else{
				$this->errorMessage($pickingCode." is not valid.");
			}
		}else{
			$this->errorMessage('Picking Code is empty !.');
		}

		$this->toJson($payload);
	}

	public function get_batch($itemCode = '', $batchCode = '')
	{
		$this->method("GET");

		if(!empty($itemCode) && !empty($batchCode)){
			$result = $this->api_picking_model->get_batch($itemCode, $batchCode)->row_array();
			if($result){

				$payload['param']		= $batchCode;
				$payload['status']		= 200;
				$payload['message']		= $batchCode . ' is valid.';
				$payload['response']	= true;

			} else{
				$this->errorMessage($batchCode.' is not valid.');
			}
		}else{
			if(empty($itemCode)){
				$this->errorMessage('Item code is empty.');
			}else{
				$this->errorMessage('Batch code is empty.');
			}
		}

		$this->toJson($payload);
	}

	public function get_serial($pickingCode = '', $serialNumber = '')
	{

		$this->method('GET');

		if(!empty($pickingCode) && !empty($serialNumber)){

			$result = $this->api_picking_model->get_serial($serialNumber)->reslut_array();
			if($result){

				$data['param']		= $serialNumber;
				$data['status']		= 200;
				$data['message']	= $serialNumber . ' is valid.';
				$data['response']	= true;
				$data['time']		= date("Y-m-d H:i:s");
				$data['locations']	= $result;

				$this->api_picking_model->start($pickingCode);

			}else{

				$this->errorMessage($serialNumber.' is not valid.',$serialNumber);

			}

		}else{

			if(empty($pickingCode)){

				$this->errorMessage('Picking code is empty.');

			}else{

				$this->errorMessage('Serial number is empty.');

			}

		}

		$this->toJson($payload);

	}

	public function post_serial()
	{

		$this->method('POST');

		$params = array(
			"picking_code"	=> $this->input->post('picking_code'), // mandatory
			"serial_number"		=> strtoupper($this->input->post('serial_number')), // mandatory
			"qty"			=> $this->input->post('qty'), // mandatory
			"time"			=> date('Y-m-d h:i:s'),
			"tray_location" => $this->input->post('tray_location') //optional
		);

		if(!empty($params['picking_code']) && !empty($params['serial_number']) && !empty($params['qty'])){

			/* Check Quantity Pick Document*/
			$pickedQtyValidation = $this->api_picking_model->get_count($params);
			if ($pickedQtyValidation) {

				/* Check & Get Pick Recomendation */
	            $checkRecomendation = $this->api_picking_model->get_picking_recomendation_by_sn($params['kd_unik']);
				if($checkRecomendation['status']){

					/* Get Parameter type */
					$getType = $this->api_picking_model->getType($params['kd_unik']);

					$process= array();
					switch ($getType) {
						case 'serial_number':
							$process = $this->api_picking_model->post_serial($params);
							break;

						case 'outer_label':
							$process = $this->api_picking_model->post_serial_LP($params);
							break;

						case 'batch':
							$process = $this->api_picking_model->post_serial_by_batch($params);
							break;
						
						default:
							# code...
							break;
					}

					if($process['status']){

						$data['param']		= $params['kd_unik'];
						$data['status']		= 200;
						$data['message']	= 'Data has been updated.';
						$data['response']	= true;

					}else{

						$data['param']		= $params['kd_unik'];
						$data['status']		= 400;
						$data['message']	= $process['message'];
						$data['response']	= false;

					}

					$this->api_picking_model->start($params['pl_name']);

					$this->load_model('api_putaway_model');
					$isFinish = $this->api_putaway_model->isFinish($params);
					if($isFinish){
						$this->api_putaway_model->finish($isFinish['kd_receiving']);
					}

				}else{

					$data['param']		= $params['kd_unik'];
					$data['status']		= 400;
					$data['message']	= 'Silahkan ambil serial number ';

					foreach ($checkRecomendation['data'] as $cr) {
						$data['message'] .= $cr['unique_code'].' - '.$cr['location_name'].'<br/>';
					}

					$data['response']	= false;

				}

            } else{
				$data['param']		= $params['kd_unik'];
				$data['status']		= 401;
				$data['message']	= 'Maximum quantity has been reached. Please return last scanned item.';
				$data['response']	= false;
			}

		}else{

			if(empty($params['picking_code'])){
				$emptyParam = "Picking Code";
			}elseif(empty($params['serial_number'])){
				$emptyParam = "Serial Number";
			}else{
				$emptyParam = "Quantity";
			}

			$this->errorMessage($emptyParam." is empty.");
		}

		$this->toJson($payload);
	}

	public function post_to_outbound()
	{

		$this->method('POST');

		$params = array(
			"picking_code"		=> $this->input->post('picking_name'),
			"serial_number"		=> $this->input->post('serial_number'),
			"qty"				=> $this->input->post('qty'),
			"time"				=> date('Y-m-d h:i:s'),
			"new_location"		=> $this->input->post('outbound_loc')
		);

		if(!empty($params['picking_code']) && !empty($params['serial_number']) && !empty($params['qty']) && !empty($params['new_location'])){

			$process = $this->api_picking_model->post_to_outbound($params);
			if($process){

				$payload['param']		= $params['serial_number'];
				$payload['status']		= 200;
				$payload['message']		= 'Data has been updated.';
				$payload['response']	= true;

			}else{

				$this->errorMessage('Failed to update.');

			}
		}else{

			if(empty($params['picking_code'])){
				$emptyParam = "Picking code";
			}else if(empty($params['serial_number'])){
				$emptyParam = "Serial number";
			}else if(empty($params['qty'])){
				$emptyParam = "Quantity";
			}else{
				$emptyParam = "New location";
			}

			$this->errorMessage($emptyParam." is empty.");

		}

		$this->toJson($payload);
	}

	public function get_location($location = '')
	{
		$this->method('GET');

		if(!empty($location)){

			$result = $this->api_picking_model->get_location($location)->row_array();
			if($result){

				$data['param']			= $location;
				$data['status']			= 200;
				$data['message']		= $location . ' is available.';
				$data['response']		= true;

			} else{
				$this->errorMessage($location." is not available.");
			}
		}else{
			$this->errorMessage($location." is empty.");
		}

		$this->toJson($payload);
	}
	
	public function post_array()
	{

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'POST'){
			
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
			
		} else{
			
			$auth = $this->api_setting_model->user_authentication();
			
			if($auth){
				
				$params['picking_code']		= $this->input->post('picking_name');
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['old_loc']			= $this->input->post('old_loc');
				$params['qty']				= $this->input->post('qty');
				$params['uname_pick']		= $this->input->post('user_pick');
				$params['pick_time']		= $this->input->post('pick_time');
				$params['new_loc']			= $this->input->post('outbound_loc');
				$params['uname_put']		= $this->input->post('user_put');
				$params['put_time']			= $this->input->post('put_time');
				
				$this->api_picking_model->update_location($params['picking_code'][0],$params);
				
				$equalizer = $this->api_picking_model->get_picked($params['picking_code'][0]);
				$sum_qty = 0;
				$sum_single = 0;
				$sum_multi = 0;

				foreach ($equalizer->result() as $row) {
					$sum_qty += $row->qty;
					$sum_single += $row->single_picked;
					$sum_multi += $row->multiple_picked;
				}

				if($sum_qty == $sum_single || $sum_qty == $sum_multi){
					$this->api_picking_model->finish($params['picking_code'][0]);
				}

				$data['param']		= $params['picking_code'][0];
				$data['status']		= 200;
				$data['message']	= 'Data has been updated. For picking code ' . $params['picking_code'][0];
				$data['response']	= true;
				
				
			} else{
				
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				
			}
		}

		echo json_encode($data);

	}

	public function get_sn_by_pl($userName='',$pickingCode='')
	{

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'GET'){
			
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
			
		} else{
			
			$auth = $this->api_setting_model->user_authentication();
			
			if($auth){

				if($userName == "" || $pickingCode == ""){

					$data['param']		= $pickingCode;
					$data['status']		= 400;
					$data['message']	= '';
					$data['response']	= false;

				} else{

					$data['param']		= $pickingCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getSnByPl($userName,$pickingCode)->result_array();

				}				
				
			} else{
				
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				
			}
		}

		echo json_encode($data);

	}

	public function get_picking_recomendation($pickingCode=''){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'GET'){
			
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
			
		} else{
			
			$auth = $this->api_setting_model->user_authentication();
			
			if($auth){

				if($pickingCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Picking code is empty';
					$data['response']	= false;

				} else{

					$data['param']		= $pickingCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getPickingRecomendation($pickingCode);

				}				
				
			} else{
				
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				
			}
		}

		echo json_encode($data);

	}

	public function get_location_by_item($itemCode=''){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'GET'){
			
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
			
		} else{
			
			$auth = $this->api_setting_model->user_authentication();
			
			if($auth){

				if($itemCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Item code is empty';
					$data['response']	= false;

				} else{

					$data['param']		= $itemCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getLocationByItem($itemCode);

				}				
				
			} else{
				
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				
			}
		}

		echo json_encode($data);		

	}

	public function get_sn_by_location($locationCode='',$itemCode=''){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'GET'){
			
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
			
		} else{
			
			$auth = $this->api_setting_model->user_authentication();
			
			if($auth){

				if($locationCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Location code is empty';
					$data['response']	= false;

				} else{

					$data['param']		= $locationCode;
					$data['status']		= 200;
					$data['response']	= true;
					$data['results']	= $this->api_picking_model->getSerialNumberByLocation($locationCode,$itemCode);

				}				
				
			} else{
				
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				
			}
		}

		echo json_encode($data);

	}

	public function get_tray_location($locationCode=""){

		$data = array();
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != 'GET'){
			
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
			
		} else{
			
			$auth = $this->api_setting_model->user_authentication();
			
			if($auth){

				if($locationCode == ''){

					$data['param']		= NULL;
					$data['status']		= 400;
					$data['message']	= 'Location code is empty.';
					$data['response']	= false;

				} else{

					$result = $this->api_picking_model->getTrayLocation($locationCode);

					if($result){
	
						$data['param']		= $locationCode;
						$data['status']		= 200;
						$data['response']	= true;

					}else{

						$data['param']		= $locationCode;
						$data['status']		= 400;
						$data['message']	= "Location doesn't exists.";

					}



				}				
				
			} else{
				
				$data['param']		= '-';
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
				
			}
		}

		echo json_encode($data);

	}

}

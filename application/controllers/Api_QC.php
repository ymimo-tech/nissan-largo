<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_qc extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load_model('api_setting_model');
        $this->load_model('api_qc_model');
    }

    public function index()
    {
        $data['param']      = null;
        $data['status']     = 400;
        $data['message']    = 'Bad request.';
        $data['response']   = false;

        echo json_encode($data);
    }

    public function get($serial_number = '')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $serial_number == ''){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $serialized = $this->api_qc_model->get($serial_number)->row_array();
                if($serialized){
                    if($serialized['loc_id'] != null){
                        $data['param']          = $serial_number;
                        $data['status']         = 200;
                        $data['message']        = $serial_number . ' is available.';
                        $data['response']       = true;
                        $data['results'] 		= $this->api_qc_model->retrieve($serial_number)->result_array();
                        $this->api_qc_model->intransit($serial_number);
                    } else{
                        $data['param']      = $serial_number;
                        $data['status']     = 401;
                        $data['message']    = $serial_number . ' is not available.';
                        $data['response']   = false;
                    }
                } else{
                    $data['param']      = $serial_number;
                    $data['status']     = 401;
                    $data['message']    = $serial_number . ' is not available.';
                    $data['response']   = false;
                }
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function get_out($serial_number = '')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $serial_number == ''){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $serialized = $this->api_qc_model->get_out($serial_number)->row_array();
                if($serialized){
                    if($serialized['loc_id'] != null){
                        $data['param']          = $serial_number;
                        $data['status']         = 200;
                        $data['message']        = $serial_number . ' is available.';
                        $data['response']       = true;
                        $data['results']        = $this->api_qc_model->retrieve($serial_number)->result_array();
                        $this->api_qc_model->intransit($serial_number);
                    } else{
                        $data['param']      = $serial_number;
                        $data['status']     = 401;
                        $data['message']    = $serial_number . ' is not available.';
                        $data['response']   = false;
                    }
                } else{
                    $data['param']      = $serial_number;
                    $data['status']     = 401;
                    $data['message']    = $serial_number . ' is not available.';
                    $data['response']   = false;
                }
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function get_location($location = '')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $location == ''){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $result = $this->api_qc_model->get_location($location)->row_array();
                if($result){
                    $data['param']          = $location;
                    $data['status']         = 200;
                    $data['message']        = $location . ' is available.';
                    $data['response']       = true;
                } else{
                    $data['param']      = $location;
                    $data['status']     = 401;
                    $data['message']    = $location . ' is not available.';
                    $data['response']   = false;
                }
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function get_location_out($location = '')
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'GET' || $location == ''){
            $data['param']      = null;
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $result = $this->api_qc_model->get_location_out($location)->row_array();
                if($result){
                    $data['param']          = $location;
                    $data['status']         = 200;
                    $data['message']        = $location . ' is available.';
                    $data['response']       = true;
                    $data['loc_type']       = $result['loc_type'];
                } else{
                    $data['param']      = $location;
                    $data['status']     = 401;
                    $data['message']    = $location . ' is not available.';
                    $data['response']   = false;
                }
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function post()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $params['kd_unik']          = $this->input->post('serial_number');
                $params['loc_name_old']     = $this->input->post('loc_name_old');
                $params['user_pick']        = $this->input->post('user_pick');
                $params['pick_time']        = $this->input->post('pick_time');
                $params['loc_name_new']     = $this->input->post('loc_name_new');
                $params['user_put']         = $this->input->post('user_put');
                $params['put_time']         = $this->input->post('put_time');
                $params['kd_qc'] 	        = $this->input->post('qc');
                $params['remark']           = $this->input->post('remark');

                if($params['kd_unik'] == ''){
                    $data['param']      = $params['kd_unik'];
                    $data['status']     = 401;
                    $data['message']    = 'Failed to update data.';
                    $data['response']   = false;
                } else{

                    $post = $this->api_qc_model->post($params);
                    if($post){

                        $data['param']      = $params['kd_unik'];
                        $data['status']     = 200;
                        $data['message']    = $params['kd_unik'] . ' has been moved to ' . $params['loc_name_new'] . '.';
                        $data['response']   = true;

                        // if(!is_bool($post)){
                            // $this->api_qc_model->sendToStaging($post);
                        // }

                    }else{

                        $data['param']      = $params['kd_unik'];
                        $data['status']     = 401;
                        $data['message']    = 'Failed to update data.';
                        $data['response']   = false;

                    }
                }           
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

    public function cancel()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method != 'POST'){
            $data['status']     = 400;
            $data['message']    = 'Bad request.';
            $data['response']   = false;
        } else{
            $auth = $this->api_setting_model->user_authentication();
            if($auth){
                $params['kd_unik']          = $this->input->post('serial_number');
                $params['loc_name']         = $this->input->post('loc_name_old');
                $params['user']             = $this->input->post('user_pick');
                $params['pick_time']        = $this->input->post('pick_time');
                $params['kd_qc']            = $this->input->post('old_qc');

                if($params['kd_unik'] == ''){
                    $data['param']      = $params['kd_unik'];
                    $data['status']     = 401;
                    $data['message']    = 'Failed to update data.';
                    $data['response']   = false;
                } else{
                    $this->api_qc_model->cancel($params);
                    $data['param']      = $params['kd_unik'];
                    $data['status']     = 200;
                    $data['message']    = $params['kd_unik'] . ' has been canceled.';
                    $data['response']   = true;
                }           
            } else{
                $data['param']      = null;
                $data['status']     = 401;
                $data['message']    = 'Unauthorized user.';
                $data['response']   = false;
            }
        }
        echo json_encode($data);
    }

}

<?php 
	
class node3_model extends CI_model{
	
	public $warehouseTable;
	public $categoryTable;
	public $category2Table ;
	public $uomTable;
	public $itemTable;
	public $locationTable;
	public $sourceTable;
	public $destinationTable;
	
	public $inboundTable;
	public $outboundTable;
	
	public $limit;
	
	public function __construct(){
		parent::__construct();
		
		$this->warehouseTable = 'master_warehouse';
		$this->categoryTable = 'master_item_category';
		$this->category2Table = 'master_item_category_2';
		$this->uomTable = 'master_uom';
		$this->itemTable = 'master_item';
		$this->locationTable = 'master_bin_location';
		$this->sourceTable = 'master_source';
		$this->destinationTable = 'master_destination';
		
		$this->inboundTable = 'inbound_document';
		$this->outboundTable = 'outbound_document';
		
		$this->limit = 10;
		
		$this->load->library('pgpdo');
	}
	
	public function saveString($data){
		$save = filter_var(trim($data), FILTER_SANITIZE_STRING);
		
		return $save;
	}
	
	public function stockAdjustment($post = array()){
		$result = array();
		
		$post = array(
			'cc_doc'			=> filter_var(trim($post['cc_doc']), FILTER_SANITIZE_STRING),
			'cc_date'			=> filter_var(trim($post['cc_date']), FILTER_SANITIZE_STRING),
			'cc_type'			=> filter_var(trim($post['cc_type']), FILTER_SANITIZE_STRING),
			'item_code'			=> filter_var(trim($post['item_code']), FILTER_SANITIZE_STRING),
			'qty'				=> filter_var(trim($post['qty']), FILTER_SANITIZE_NUMBER_INT)
		);
		
		$sql = 'INSERT INTO outbound_document_confirmation(
					cc_doc_num,
					cc_date,
					cc_type,
					item_code,
					quantity,
					sync_status
				)VALUES(
					cc_doc_num,
					cc_date,
					cc_type,
					item_code,
					quantity,
					sync_status
				)';
				
		$params = array(
			':cc_doc_num'				=> $post['cc_doc'],
			':cc_date'					=> date('Y-m-d', strtotime($post['cc_date'])),
			':cc_type'					=> $post['cc_type'],
			':item_code'				=> $post['item_code'],
			':quantity'					=> $post['qty'],
			':sync_status'				=> 'N'
		);
		
		$q = $this->pgpdo->exec($sql, $params);
		
		if($q){
			
			$result['status'] = 'OK';
			$result['message'] = 'Insert stock adjustment success';
			
			file_put_contents('log/stock.adjustment.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $post['cc_doc'] . ' insert success' . PHP_EOL, FILE_APPEND);
			
		}else{
			
			$result['status'] = 'ERR';
			$result['message'] = 'Insert stock adjustment failed';
			
			file_put_contents('log/stock.adjustment.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $post['cc_doc'] . ' insert failed' . PHP_EOL, FILE_APPEND);
			
		}
		
		return $result;
	}
	
	public function outboundConfirmation($post = array()){
		$result = array();
		
		$post = array(
			'outbound_doc'		=> filter_var(trim($post['outbound_doc']), FILTER_SANITIZE_STRING),
			'loading_doc'		=> filter_var(trim($post['loading_doc']), FILTER_SANITIZE_STRING),
			'loading_date'		=> filter_var(trim($post['loading_date']), FILTER_SANITIZE_STRING),
			'outbound_doc_type'	=> filter_var(trim($post['outbound_doc_type']), FILTER_SANITIZE_STRING),
			'item_code'			=> filter_var(trim($post['item_code']), FILTER_SANITIZE_STRING),
			'qty'				=> filter_var(trim($post['qty']), FILTER_SANITIZE_NUMBER_INT),
			'loc_code'			=> filter_var(trim($post['loc_code']), FILTER_SANITIZE_STRING)
		);
		
		$sql = 'INSERT INTO outbound_document_confirmation(
					outbound_document_num,
					loading_doc_num,
					loading_date,
					outbound_doc_type,
					item_code,
					quantity,
					location_code,
					sync_status
				)VALUES(
					:outbound_document_num,
					:loading_doc_num,
					:loading_date,
					:outbound_doc_type,
					:item_code,
					:quantity,
					:location_code,
					:sync_status
				)';
				
		$params = array(
			':outbound_document_num'	=> $post['outbound_doc'],
			':loading_doc_num'			=> $post['loading_doc'],
			':loading_date'				=> date('Y-m-d', strtotime($post['loading_date'])),
			':outbound_doc_type'		=> $post['outbound_doc_type'],
			':item_code'				=> $post['item_code'],
			':quantity'					=> $post['qty'],
			':location_code'			=> $post['loc_code'],
			':sync_status'				=> 'N'
		);
		
		$q = $this->pgpdo->exec($sql, $params);
		
		if($q){
			
			$result['status'] = 'OK';
			$result['message'] = 'Insert outbound confirmation success';
			
			file_put_contents('log/outbound.confirmation.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $post['outbound_doc'] . ' insert success' . PHP_EOL, FILE_APPEND);
			
		}else{
			
			$result['status'] = 'ERR';
			$result['message'] = 'Insert outbound confirmation failed';
		
			file_put_contents('log/outbound.confirmation.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $post['outbound_doc'] . ' insert failed' . PHP_EOL, FILE_APPEND);
		
		}
		
		return $result;
	}
	
	public function inboundConfirmation($post = array()){
		$result = array();
		
		$post = array(
			'inbound_doc'		=> filter_var(trim($post['inbound_doc']), FILTER_SANITIZE_STRING),
			'outbound_doc'		=> filter_var(trim($post['outbound_doc']), FILTER_SANITIZE_STRING),
			'receiving_doc'		=> filter_var(trim($post['receiving_doc']), FILTER_SANITIZE_STRING),
			'receiving_date'	=> filter_var(trim($post['receiving_date']), FILTER_SANITIZE_STRING),
			'inbound_doc_type'	=> filter_var(trim($post['inbound_doc_type']), FILTER_SANITIZE_STRING),
			'item_code'			=> filter_var(trim($post['item_code']), FILTER_SANITIZE_STRING),
			'qty'				=> filter_var(trim($post['qty']), FILTER_SANITIZE_NUMBER_INT),
			'loc_code'			=> filter_var(trim($post['loc_code']), FILTER_SANITIZE_STRING)
		);
		
		$sql = 'INSERT INTO inbound_document_confirmation(
					inbound_document_num,
					outbound_document_num,
					gr_doc_num,
					gr_date,
					inbound_doc_type,
					item_code,
					quantity,
					location_code,
					sync_status
				)VALUES(
					:inbound_document_num,
					:outbound_document_num,
					:gr_doc_num,
					:gr_date,
					:inbound_doc_type,
					:item_code,
					:quantity,
					:location_code,
					:sync_status
				)';
				
		$params = array(
			':inbound_document_num'		=> $post['inbound_doc'],
			':outbound_document_num'	=> $post['outbound_doc'],
			':gr_doc_num'				=> $post['receiving_doc'],
			':gr_date'					=> date('Y-m-d', strtotime($post['receiving_date'])),
			':inbound_doc_type'			=> $post['inbound_doc_type'],
			':item_code'				=> $post['item_code'],
			':quantity'					=> $post['qty'],
			':location_code'			=> $post['loc_code'],
			':sync_status'				=> 'N'
		);
		
		$q = $this->pgpdo->exec($sql, $params);
		
		if($q){
			
			$result['status'] = 'OK';
			$result['message'] = 'Insert inbound confirmation success';
			
			file_put_contents('log/inbound.confirmation.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $post['inbound_doc'] . ' insert success' . PHP_EOL, FILE_APPEND);
			
		}else{
			
			$result['status'] = 'ERR';
			$result['message'] = 'Insert inbound confirmation failed';
			
			file_put_contents('log/inbound.confirmation.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $post['inbound_doc'] . ' insert failed' . PHP_EOL, FILE_APPEND);
			
		}
		
		return $result;
	}
	
	public function updateSyncFlag($field, $code, $table){
		$result = array();
		
		$sql = 'UPDATE 
					'.$table.' 
				SET 
					sync_status=:syn_status,
					sync_date=NOW() 
				WHERE 
					'.$field.'=:code';
					
		$params = array(
			':sync_status'		=> 'Y',
			':code'				=> $code
		);
		
		$q = $this->pgpdo->exec($sql, $params);
	}
	
	//---------------TRANSACTION BLOCK---------------------//
	
	public function getDocumentIdInbound($code = false){
		$result = 0;
		
		$sql = 'SELECT 
					id_inbound_document 
				FROM 
					m_inbound_document 
				WHERE 
					LOWER(inbound_document_name)=\''.strtolower($code).'\'';
					
		$r = $this->db->query($sql)->row_array();
		$result = $r['id_inbound_document'];		
		
		return $result;
	}
	
	public function getCustomerId($code = false){
		$result = 0;
		
		$sql = 'SELECT 
					id_customer 
				FROM 
					m_customer
				WHERE 
					LOWER(customer_code)=\''.strtolower($code).'\'';
					
		$r = $this->db->query($sql)->row_array();
		$result = $r['id_customer'];		
		
		return $result;
	}
	
	public function getSupplierId($code = false){
		$result = 0;
		
		$sql = 'SELECT 
					id_supplier 
				FROM 
					supplier 
				WHERE 
					LOWER(kd_supplier)=\''.strtolower($code).'\'';
					
		$r = $this->db->query($sql)->row_array();
		$result = $r['id_supplier'];		
		
		return $result;
	}
	
	public function getInboundId($code = false){
		$result = 0;
		
		$sql = 'SELECT 
					id_inbound 
				FROM 
					inbound 
				WHERE 
					LOWER(kd_inbound)=\''.strtolower($code).'\'';
					
		$r = $this->db->query($sql)->row_array();
		$result = $r['id_inbound'];		
		
		return $result;
	}
	
	public function getItemId($code = false){
		$result = 0;
		
		$sql = 'SELECT 
					id_barang 
				FROM 
					barang 
				WHERE 
					LOWER(kd_barang)=\''.strtolower($code).'\'';
					
		$r = $this->db->query($sql)->row_array();
		$result = $r['id_barang'];		
		
		return $result;
	}
	
	public function syncInbound(){
		$result = array();

		$sql = 'SELECT 
					* 
				FROM 
				(
					SELECT 
						inbound_document_num,
						inbound_date,
						inbound_doc_type,
						source_code,
						line_num,
						item_code,
						quantity,
						RANK()
						OVER(PARTITION BY inbound_document_num) AS rank
					FROM 
						'.$this->inboundTable.' 
					WHERE 
						sync_status=:sync_status 
					LIMIT '.$this->limit.' 
				) AS tbl 
				WHERE 
					rank = 1';
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						inbound 
					WHERE 
						kd_inbound=\''.$this->saveString($r[$i]['inbound_doc_num']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			$idDocument = $this->getDocumentIdInbound($r[$i]['inbound_doc_type']);
			if($idDocument == 2){
				$idSupplier = $this->getCustomerId($r[$i]['source_code']);
			}else{
				$idSupplier = $this->getSupplierId($r[$i]['source_code']);
			}
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							inbound 
						SET 
							tanggal_inbound=\''.$this->saveString(date('Y-m-d H:i:s', strtotime($r[$i]['inbound_date']))).'\',
							id_inbound_document=\''.$idDocument.'\',
							id_supplier=\''.$idSupplier.'\'
						WHERE 
							kd_inbound=\''.$this->saveString($r[$i]['inbound_doc_num']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$sql = 'SELECT 
								inbound_document_num,
								inbound_date,
								inbound_doc_type,
								source_code,
								line_num,
								item_code,
								quantity
							FROM 
								'.$this->inboundTable.' 
							WHERE 
								inbound_document_num=:doc_num 
							ORDER BY 
								line_num ASC';
								
					$params = array(
						':doc_num'	=> $r[$i]['inbound_doc_num']
					);
					
					$item = $this->pgpdo->fetchAll($sql, $params);
					$iLen = count($item);
					
					$idInbound = $this->getInboundId($this->saveString($r[$i]['inbound_doc_num']));
					
					$sql = 'DELETE FROM inbound_barang WHERE id_inbound=\''.$idInbound.'\'';
					$this->db->query($sql);
					
					//WATCH HERE
					for($j = 0; $j < $iLen; $j++){
						
						$idItem = $this->getItemId($this->saveString($item[$j]['item_code']));
						
						$sql = 'INSERT INTO inbound_barang(
									id_inbound,
									id_barang,
									jumlah_barang
								)VALUES(
									\''.$idInbound.'\',
									\''.$idItem.'\',
									\''.$item[$j]['quantity'].'\'
								)';
					}
					
					$this->updateSyncFlag('inbound_doc_num', $r[$i]['inbound_doc_num'], $this->inboundTable);
					$j++;
					
					file_put_contents('log/inbound.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['inbound_doc_num'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/inbound.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['inbound_doc_num'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO inbound(
							kd_inbound,
							tanggal_inbound,
							id_inbound_document,
							id_supplier
						)VALUES(
							\''.$this->saveString($r[$i]['inbound_doc_num']).'\',
							\''.$this->saveString(date('Y-m-d', strtotime($r[$i]['inbound_date']))).'\',
							\''.idDocument.'\',
							\''.$idSupplier.'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$sql = 'SELECT 
								inbound_document_num,
								inbound_date,
								inbound_doc_type,
								source_code,
								line_num,
								item_code,
								quantity
							FROM 
								'.$this->inboundTable.' 
							WHERE 
								inbound_document_num=:doc_num 
							ORDER BY 
								line_num ASC';
								
					$params = array(
						':doc_num'	=> $r[$i]['inbound_doc_num']
					);
					
					$item = $this->pgpdo->fetchAll($sql, $params);
					$iLen = count($item);
					
					$idInbound = $this->getInboundId($this->saveString($r[$i]['inbound_doc_num']));
					
					$sql = 'DELETE FROM inbound_barang WHERE id_inbound=\''.$idInbound.'\'';
					$this->db->query($sql);
					
					//WATCH HERE
					for($j = 0; $j < $iLen; $j++){
						
						$idItem = $this->getItemId($this->saveString($item[$j]['item_code']));
						
						$sql = 'INSERT INTO inbound_barang(
									id_inbound,
									id_barang,
									jumlah_barang
								)VALUES(
									\''.$idInbound.'\',
									\''.$idItem.'\',
									\''.$item[$j]['quantity'].'\'
								)';
					}
					
					$this->updateSyncFlag('inbound_doc_num', $r[$i]['inbound_doc_num'], $this->inboundTable);
					$j++;
					
					file_put_contents('log/inbound.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['inbound_doc_num'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/inbound.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['inbound_doc_num'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	public function syncOutbound(){
		$result = array();

		$sql = 'SELECT 
					* 
				FROM 
				(
					SELECT 
						outbound_document_num,
						shipment_doc_num,
						outbound_date,
						outbound_doc_type,
						destination_code,
						line_num,
						item_code,
						quantity,
						RANK()
						OVER(PARTITION BY outbound_doc_type) AS rank
					FROM 
						'.$this->outboundTable.' 
					WHERE 
						sync_status=:sync_status 
					LIMIT '.$this->limit.' 
				) AS tbl 
				WHERE 
					rank = 1';
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						outbound 
					WHERE 
						kd_outbound=\''.$this->saveString($r[$i]['outbound_doc_num']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			$idDocument = $this->getDocumentIdOutbound($r[$i]['outbound_doc_type']);
			if($idDocument == 2){
				$idSupplier = $this->getSupplierId($r[$i]['destination_code']);
			}else{
				$idSupplier = $this->getCustomerId($r[$i]['destination_code']);
			}
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							outbound 
						SET 
							tanggal_outbound=\''.$this->saveString(date('Y-m-d H:i:s', strtotime($r[$i]['outbound_date']))).'\',
							id_outbound_document=\''.$idDocument.'\',
							id_customer=\''.$idSupplier.'\'
						WHERE 
							kd_outbound=\''.$this->saveString($r[$i]['outbound_doc_num']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$sql = 'SELECT 
								outbound_document_num,
								shipment_doc_num,
								outbound_date,
								outbound_doc_type,
								destination_code,
								line_num,
								item_code,
								quantity
							FROM 
								'.$this->outboundTable.' 
							WHERE 
								outbound_document_num=:doc_num 
							ORDER BY 
								line_num ASC';
								
					$params = array(
						':doc_num'	=> $r[$i]['outbound_document_num']
					);
					
					$item = $this->pgpdo->fetchAll($sql, $params);
					$iLen = count($item);
					
					$idOutbound = $this->getOutboundId($this->saveString($r[$i]['outbound_document_num']));
					
					$sql = 'UPDATE 
								shipping_doc 
							SET 
								shipment_doc=\''.$r[$i]['shipment_doc_num'].'\'
							WHERE 
								outbound_id=\''.$idOutbound.'\'';
								
					$this->db->query($sql);
					
					$sql = 'DELETE FROM outbound_barang WHERE id_outbound=\''.$idOutbound.'\'';
					$this->db->query($sql);
					
					//WATCH HERE
					for($j = 0; $j < $iLen; $j++){
						
						$idItem = $this->getItemId($this->saveString($item[$j]['item_code']));
						
						$sql = 'INSERT INTO outbound_barang(
									id_inbound,
									id_barang,
									jumlah_barang
								)VALUES(
									\''.$idOutbound.'\',
									\''.$idItem.'\',
									\''.$item[$j]['quantity'].'\'
								)';
					}
					
					$this->updateSyncFlag('outbound_doc_num', $r[$i]['outbound_doc_num'], $this->outboundTable);
					$j++;
					
					file_put_contents('log/outbound.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['outbound_doc_num'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/outbound.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['outbound_doc_num'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO outbound(
							kd_outbound,
							tanggal_outbound,
							id_outbound_document,
							id_customer
						)VALUES(
							\''.$this->saveString($r[$i]['outbound_doc_num']).'\',
							\''.$this->saveString(date('Y-m-d', strtotime($r[$i]['outbound_date']))).'\',
							\''.idDocument.'\',
							\''.$idSupplier.'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$sql = 'SELECT 
								outbound_document_num,
								shipment_doc_num,
								outbound_date,
								outbound_doc_type,
								destination_code,
								line_num,
								item_code,
								quantity
							FROM 
								'.$this->outboundTable.' 
							WHERE 
								outbound_document_num=:doc_num 
							ORDER BY 
								line_num ASC';
								
					$params = array(
						':doc_num'	=> $r[$i]['outbound_doc_num']
					);
					
					$item = $this->pgpdo->fetchAll($sql, $params);
					$iLen = count($item);
					
					$idOutbound = $this->getOutboundId($this->saveString($r[$i]['outbound_doc_num']));
					
					$sql = 'UPDATE 
								shipping_doc 
							SET 
								shipment_doc=\''.$r[$i]['shipment_doc_num'].'\'
							WHERE 
								outbound_id=\''.$idOutbound.'\'';
								
					$this->db->query($sql);
					
					$sql = 'DELETE FROM outbound_barang WHERE id_outbound=\''.$idOutbound.'\'';
					$this->db->query($sql);
					
					//WATCH HERE
					for($j = 0; $j < $iLen; $j++){
						
						$idItem = $this->getItemId($this->saveString($item[$j]['item_code']));
						
						$sql = 'INSERT INTO outbound_barang(
									id_outbound,
									id_barang,
									jumlah_barang
								)VALUES(
									\''.$idOutbound.'\',
									\''.$idItem.'\',
									\''.$item[$j]['quantity'].'\'
								)';
					}
					
					$this->updateSyncFlag('outbound_doc_num', $r[$i]['outbound_doc_num'], $this->outboundTable);
					$j++;
					
					file_put_contents('log/outbound.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['outbound_doc_num'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/outbound.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['outbound_doc_num'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	
	//---------------MASTER BLOCK---------------------//
	
	public function syncMasterWarehouse(){
		
		//WAREHOUSE
		$result = array();
		
		$sql = 'SELECT 
					warehouse_code,
					warehouse_name 
				FROM 
					'.$this->warehouseTable.' 
				WHERE 
					sync_status=:sync_status 
				LIMIT '.$this->limit;
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						m_wh_type 
					WHERE 
						code_wh=\''.$this->saveString($r[$i]['warehouse_code']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							m_wh_type 
						SET 
							nama_wh=\''.$this->saveString($r[$i]['warehouse_name']).'\',
							created_date=\''.$rectime.'\' 
						WHERE 
							code_wh=\''.$this->saveString($r[$i]['warehouse_code']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('warehouse_code', $r[$i]['warehouse_code'], $this->warehouseTable);
					$j++;
					
					file_put_contents('log/warehouse.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['warehouse_code'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/warehouse.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['warehouse_code'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO m_wh_type(
							code_wh,
							nama_wh,
							status,
							created_date
						)VALUES(
							\''.$this->saveString($r[$i]['warehouse_code']).'\',
							\''.$this->saveString($r[$i]['warehouse_name']).'\',
							\'A\',
							\''.$rectime.'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('warehouse_code', $r[$i]['warehouse_code'], $this->warehouseTable);
					$j++;
					
					file_put_contents('log/warehouse.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['warehouse_code'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/warehouse.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['warehouse_code'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	public function syncMasterCategory(){
		
		//CATEGORY
		$result = array();
		
		$sql = 'SELECT 
					item_category_code,
					item_category_name 
				FROM 
					'.$this->categoryTable.' 
				WHERE 
					sync_status=:sync_status
				LIMIT '.$this->limit;
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						kategori 
					WHERE 
						kd_kategori=\''.$this->saveString($r[$i]['item_category_code']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							kategori 
						SET 
							nama_kategori=\''.$this->saveString($r[$i]['item_category_name']).'\'
						WHERE 
							kd_kategori=\''.$this->saveString($r[$i]['item_category_code']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('item_category_code', $r[$i]['item_category_code'], $this->categoryTable);
					$j++;
					
					file_put_contents('log/category.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_category_code'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/category.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_category_code'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO kategori(
							kd_kategori,
							nama_kategori
						)VALUES(
							\''.$this->saveString($r[$i]['item_category_code']).'\',
							\''.$this->saveString($r[$i]['item_category_name']).'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('item_category_code', $r[$i]['item_category_code'], $this->categoryTable);
					$j++;
					
					file_put_contents('log/category.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_category_code'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/category.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_category_code'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	public function syncMasterCategory2(){
		
		//CATEGORY2
		$result = array();
		
		$sql = 'SELECT 
					item_category_2_code,
					item_category_2_name 
				FROM 
					'.$this->category2Table.' 
				WHERE 
					sync_status=:sync_status
				LIMIT '.$this->limit;
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						kategori_2 
					WHERE 
						kd_kategori_2=\''.$this->saveString($r[$i]['item_category_2_code']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							kategori_2 
						SET 
							nama_kategori_2=\''.$this->saveString($r[$i]['item_category_2_name']).'\'
						WHERE 
							kd_kategori_2=\''.$this->saveString($r[$i]['item_category_2_code']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('item_category_2_code', $r[$i]['item_category_2_code'], $this->category2Table);
					$j++;
					
					file_put_contents('log/category2.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_category_2_code'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/category2.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_category_2_code'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO kategori(
							kd_kategori_2,
							nama_kategori_2
						)VALUES(
							\''.$this->saveString($r[$i]['item_category_2_code']).'\',
							\''.$this->saveString($r[$i]['item_category_2_name']).'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('item_category_2_code', $r[$i]['item_category_2_code'], $this->category2Table);
					$j++;
					
					file_put_contents('log/category2.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_category_2_code'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/category2.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_category_2_code'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	public function syncMasterUom(){
		
		//UOM
		$result = array();
		
		$sql = 'SELECT 
					uom_code,
					uom_name 
				FROM 
					'.$this->uomTable.' 
				WHERE 
					sync_status=:sync_status
				LIMIT '.$this->limit;
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						satuan 
					WHERE 
						kd_satuan=\''.$this->saveString($r[$i]['uom_code']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							satuan 
						SET 
							nama_satuan=\''.$this->saveString($r[$i]['uom_name']).'\'
						WHERE 
							kd_satuan=\''.$this->saveString($r[$i]['uom_code']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('uom_code', $r[$i]['uom_code'], $this->uomTable);
					$j++;
					
					file_put_contents('log/uom.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['uom_code'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/uom.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['uom_code'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO satuan(
							kd_satuan,
							nama_satuan
						)VALUES(
							\''.$this->saveString($r[$i]['uom_code']).'\',
							\''.$this->saveString($r[$i]['uom_name']).'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('uom_code', $r[$i]['uom_code'], $this->uomTable);
					$j++;
					
					file_put_contents('log/uom.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['uom_code'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/uom.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['uom_code'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	public function getCategoryId($code){
		$result = 0;
		
		$sql = 'SELECT 
					id_kategori 
				FROM 
					kategori 
				WHERE 
					kd_kategori=\''.$code.'\'';
					
		$r = $this->db->query($sql)->row_array();
		$result = $r['id_kategori'];
		
		if(empty($result))
			file_put_contents('log/category.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $code . ' id_kategori not found in largo database' . PHP_EOL, FILE_APPEND);
		
		return $result;
	}
	
	public function getCategory2Id($code){
		$result = 0;
		
		$sql = 'SELECT 
					id_kategori_2 
				FROM 
					kategori_2 
				WHERE 
					kd_kategori_2=\''.$code.'\'';
					
		$r = $this->db->query($sql)->row_array();
		$result = $r['id_kategori_2'];
		
		if(empty($result))
			file_put_contents('log/category2.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $code . ' id_kategori_2 not found in largo database' . PHP_EOL, FILE_APPEND);
		
		return $result;
	}
	
	public function getUomId($code){
		$result = 0;
		
		$sql = 'SELECT 
					id_satuan
				FROM 
					satuan 
				WHERE 
					kd_satuan=\''.$code.'\'';
					
		$r = $this->db->query($sql)->row_array();
		$result = $r['id_satuan'];
		
		if(empty($result))
			file_put_contents('log/uom.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $code . ' id_satuan not found in largo database' . PHP_EOL, FILE_APPEND);
		
		return $result;
	}
	
	//ITEM
	public function syncMasterItem(){
		
		//ITEM
		$result = array();
		
		$sql = 'SELECT 
					item_code,
					item_name,
					item_category_code,
					item_category_2_code,
					has_batch,
					has_expdate,
					uom_code, 
					picking_strategy,
					picking_period
				FROM 
					'.$this->itemTable.' 
				WHERE 
					sync_status=:sync_status 
				LIMIT '.$this->limit;
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						barang 
					WHERE 
						kd_barang=\''.$this->saveString($r[$i]['item_code']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			$idCategory = $this->getCategoryId($this->saveString($r[$i]['item_category_code']));
			$idCategory2 = $this->getCategory2Id($this->saveString($r[$i]['item_category_2_code']));
			$idUom = $this->geUomId($this->saveString($r[$i]['uom_code']));
			
			$hasBatch = 0;
			if($r[$i]['has_batch'] == 'Y')
				$hasBatch = 1;
			
			$hasExpdate = 0;
			if($r[$i]['has_expdate'] == 'Y')
				$hasBatch = 1;
			
			$pStrategy = str_replace('+','_', $this->saveString($r[$i]['picking_strategy']));
			$pPeriod = str_replace('+','_', $this->saveString($r[$i]['picking_period']));
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							barang
						SET 
							nama_barang=\''.$this->saveString($r[$i]['item_name']).'\',
							id_kategori=\''.$idCategory.'\',
							id_kategori_2=\''.$idCategory2.'\',
							has_batch=\''.$hasBatch.'\',
							has_expdate=\''.$hasExpdate.'\',
							id_satuan=\''.$idUom.'\',
							picking_strategy=\''.$pStrategy.'\',
							picking_period=\''.$pPeriod.'\'
						WHERE 
							kd_barang=\''.$this->saveString($r[$i]['item_code']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('item_code', $r[$i]['item_code'], $this->itemTable);
					$j++;
					
					file_put_contents('log/item.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_code'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/item.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_code'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO satuan(
							kd_barang,
							nama_barang,
							id_kategori,
							id_kategori_2,
							has_batch,
							has_expdate,
							has_sernum,
							id_satuan,
							picking_strategy,
							picking_period
						)VALUES(
							\''.$this->saveString($r[$i]['item_code']).'\',
							\''.$this->saveString($r[$i]['item_name']).'\',
							\''.$idCategory.'\',
							\''.$idCategory2.'\',
							\''.$hasBatch.'\',
							\''.$hasExpdate.'\',
							\'N\',
							\''.$idUom.'\',
							\''.$pStrategy.'\',
							\''.$pPeriod.'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('item_code', $r[$i]['item_code'], $this->itemTable);
					$j++;
					
					file_put_contents('log/uom.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_code'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/uom.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['item_code'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	//BIN
	public function syncMasterBin(){
		
		//BIN
		$result = array();
		
		$sql = 'SELECT 
					location_code,
					location_name,
					location_type
				FROM 
					'.$this->locationTable.' 
				WHERE 
					sync_status=:sync_status
				LIMIT '.$this->limit;
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						m_loc 
					WHERE 
						loc_name=\''.$this->saveString($r[$i]['location_code']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							m_loc
						SET 
							loc_desc=\''.$this->saveString($r[$i]['locatin_name']).'\',
							loc_type=\''.$this->saveString($r[$i]['locatin_type']).'\'
						WHERE 
							loc_name=\''.$this->saveString($r[$i]['location_code']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('location_code', $r[$i]['location_code'], $this->locationTable);
					$j++;
					
					file_put_contents('log/bin.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['location_code'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/bin.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['location_code'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO m_loc(
							loc_name,
							loc_desc,
							loc_type
						)VALUES(
							\''.$this->saveString($r[$i]['location_code']).'\',
							\''.$this->saveString($r[$i]['location_name']).'\',
							\''.$this->saveString($r[$i]['location_type']).'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('location_code', $r[$i]['location_code'], $this->locationTable);
					$j++;
					
					file_put_contents('log/bin.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['location_code'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/bin.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['location_code'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	//SOURCE
	public function syncMasterSource(){
		
		//SOURCE
		$result = array();
		
		$sql = 'SELECT 
					source_code,
					source_name,
					source_address,
					source_phone,
					source_contact_person,
					source_email
				FROM 
					'.$this->sourceTable.' 
				WHERE 
					sync_status=:sync_status
				LIMIT '.$this->limit;
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						m_loc 
					WHERE 
						loc_name=\''.$this->saveString($r[$i]['location_code']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							supplier
						SET 
							nama_supplier=\''.$this->saveString($r[$i]['source_name']).'\',
							alamat_supplier=\''.$this->saveString($r[$i]['source_address']).'\',
							telepon_supplier=\''.$this->saveString($r[$i]['source_phone']).'\',
							cp_supplier=\''.$this->saveString($r[$i]['source_contact_person']).'\',
							email_supplier=\''.$this->saveString($r[$i]['source_email']).'\'
						WHERE 
							kd_supplier=\''.$this->saveString($r[$i]['source_code']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('source_code', $r[$i]['source_code'], $this->sourceTable);
					$j++;
					
					file_put_contents('log/source.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['source_code'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/source.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['source_code'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO supplier(
							kd_supplier,
							nama_supplier,
							alamat_supplier,
							telepon_supplier,
							cp_supplier,
							email_supplier
						)VALUES(
							\''.$this->saveString($r[$i]['source_code']).'\',
							\''.$this->saveString($r[$i]['source_name']).'\',
							\''.$this->saveString($r[$i]['source_address']).'\',
							\''.$this->saveString($r[$i]['source_phone']).'\',
							\''.$this->saveString($r[$i]['source_contact_person']).'\',
							\''.$this->saveString($r[$i]['source_email']).'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('source_code', $r[$i]['source_code'], $this->sourceTable);
					$j++;
					
					file_put_contents('log/source.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['source_code'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/source.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['source_code'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
	
	//DESTINATION
	public function syncMasterDestination(){
		
		//DESTINATION
		$result = array();
		
		$sql = 'SELECT 
					destination_code,
					destination_name,
					destination_address,
					destination_city,
					destination_post_code,
					destination_country,
					destination_phone,
					destination_fax,
					destination_contact_person
				FROM 
					'.$this->destinationTable.' 
				WHERE 
					sync_status=:sync_status
				LIMIT '.$this->limit;
					
		$params = array(
			':sync_status'	=> 'N'
		);
		
		$r = $this->pgpdo->fetchAll($sql, $params);
		$len = count($r);
		$j = 0;
		
		for($i = 0; $i < $len; $i++){
			
			$sql = 'SELECT 
						COUNT(*) 
					FROM 
						m_customer 
					WHERE 
						customer_code=\''.$this->saveString($r[$i]['destination_code']).'\'';
			
			$row = $this->db->query($sql)->row_array();
			$rectime = date('Y-m-d H:i:s');
			
			if($row['total'] > 0){
				
				$sql = 'UPDATE 
							m_customer
						SET 
							customer_name=\''.$this->saveString($r[$i]['destination_name']).'\',
							address=\''.$this->saveString($r[$i]['destination_address']).'\',
							city=\''.$this->saveString($r[$i]['destination_city']).'\',
							postal_code=\''.$this->saveString($r[$i]['destination_post_code']).'\',
							country=\''.$this->saveString($r[$i]['destination_country']).'\',
							phone=\''.$this->saveString($r[$i]['destination_phone']).'\',
							fax=\''.$this->saveString($r[$i]['destination_fax']).'\',
							contact_person=\''.$this->saveString($r[$i]['destination_contact_person']).'\'
						WHERE 
							customer_code=\''.$this->saveString($r[$i]['destination_code']).'\'';
				
				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('destination_code', $r[$i]['destination_code'], $this->destinationTable);
					$j++;
					
					file_put_contents('log/destination.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['destination_code'] . ' sync success (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/destination.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['destination_code'] . ' sync failed  (UPDATE)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}else{
				
				$sql = 'INSERT INTO m_customer(
							customer_code,
							customer_name,
							address,
							city,
							postal_code,
							country.
							phone,
							fax,
							contact_person
						)VALUES(
							\''.$this->saveString($r[$i]['destination_code']).'\',
							\''.$this->saveString($r[$i]['destination_name']).'\',
							\''.$this->saveString($r[$i]['destination_address']).'\',
							\''.$this->saveString($r[$i]['destination_city']).'\',
							\''.$this->saveString($r[$i]['destination_post_code']).'\',
							\''.$this->saveString($r[$i]['destination_country']).'\',
							\''.$this->saveString($r[$i]['destination_phone']).'\',
							\''.$this->saveString($r[$i]['destination_fax']).'\',
							\''.$this->saveString($r[$i]['destination_contact_person']).'\'
						)';

				$q = $this->db->query($sql);
				
				if($q){
					
					$this->updateSyncFlag('destination_code', $r[$i]['destination_code'], $this->destinationTable);
					$j++;
					
					file_put_contents('log/destination.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['destination_code'] . ' sync success (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}else{
					
					file_put_contents('log/destination.'.date('dMYHis').'.log', '['.date('d M Y H:i:s').'] ' . $r[$i]['destination_code'] . ' sync failed  (INSERT)' . PHP_EOL, FILE_APPEND);
					
				}
				
			}
			
		}
		
		$result['message'] = sprintf('Sync %s of %s done.', $j, $len);
		
		return $result;
	}
}
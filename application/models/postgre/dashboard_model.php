<?php
class dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

	public function getReplenishment($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'itm.name',
			1 => 'stock',
			2 => 'min'
		);

		$where = '';

		$this->db
			->select('item_id, sum(last_qty) as qty')
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id')
			->group_by('item_id');

		$tbl = $this->db->get_compiled_select();

		$this->db->start_cache();

		$this->db
			->from('items itm')
			->join("($tbl) as tbl",'tbl.item_id=itm.id')
			->join('units unt','unt.id=itm.unit_id','left')
			->where('COALESCE(tbl.qty, 0) <= minimum_stock');

		$this->db->stop_cache();

		$row = $this->db->get();
		$totalData = $row->num_rows();

		$this->db
			->select([
				"itm.code as nama_barang",
				"CONCAT(itm.code, ' - ', itm.name) AS nama_barang",
				"COALESCE(tbl.qty, 0) AS stock",
				"minimum_stock AS min",
				"unt.name as nama_satuan"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['stock'];
			$nested[] = $row[$i]['min'];
			$nested[] = $row[$i]['nama_satuan'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getReplenishment1($post = array()){
		$result = array();

		$sql = 'SELECT
					kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					tbl.qty AS stock, minimum_stock AS min, nama_satuan
				FROM
					barang brg
				JOIN
				(
					SELECT
						id_barang, COUNT(*) AS qty
					FROM
						receiving_barang r
					JOIN m_loc loc
						ON loc.loc_id=r.loc_id
					GROUP BY
						id_barang
				) AS tbl
					ON tbl.id_barang=brg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					tbl.qty <= minimum_stock';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

	public function getInboundStatus($post = array()){
		$result = array();
		$today = date('Y-m-d');

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
					(
						SELECT
							SUM(ri.qty)
						FROM
							receivings r
						JOIN inbound inb
							ON inb.id_inbound=r.po_id
						JOIN warehouses wh
							ON wh.id=inb.warehouse_id
						JOIN users_warehouses uw
							ON uw.warehouse_id=wh.id
						JOIN item_receiving ri
							ON ri.receiving_id=r.id
						WHERE
							uw.user_id = ".$this->session->userdata('user_id')."
						AND
							DATE(r.date) = CURRENT_DATE
						AND
							start_tally IS NOT NULL
						AND
							finish_tally IS NULL
						$where
					) AS tally_in_progress,
					(
						SELECT
							SUM(ri.qty)
						FROM
							receivings r
						JOIN inbound inb
							ON inb.id_inbound=r.po_id
						JOIN warehouses wh
							ON wh.id=inb.warehouse_id
						JOIN users_warehouses uw
							ON uw.warehouse_id=wh.id
						JOIN item_receiving ri
							ON ri.receiving_id=r.id
						WHERE
							uw.user_id = ".$this->session->userdata('user_id')."
						AND
							DATE(r.date) = CURRENT_DATE
						AND
							start_tally IS NULL
						$where
					) AS ready_for_tally,
					(
						SELECT
							SUM(ri.qty)
						FROM
							receivings r
						JOIN inbound inb
							ON inb.id_inbound=r.po_id
						JOIN warehouses wh
							ON wh.id=inb.warehouse_id
						JOIN users_warehouses uw
							ON uw.warehouse_id=wh.id
						JOIN item_receiving ri
							ON ri.receiving_id=r.id
						WHERE
							uw.user_id = ".$this->session->userdata('user_id')."
						AND
							DATE(r.date) = CURRENT_DATE
						AND
							start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
						$where
					) AS ready_to_bin,
					(
						SELECT
							SUM(ri.qty)
						FROM
							receivings r
						JOIN inbound inb
							ON inb.id_inbound=r.po_id
						JOIN warehouses wh
							ON wh.id=inb.warehouse_id
						JOIN users_warehouses uw
							ON uw.warehouse_id=wh.id
						JOIN item_receiving ri
							ON ri.receiving_id=r.id
						WHERE
							uw.user_id = ".$this->session->userdata('user_id')."
						AND
							DATE(r.date) = CURRENT_DATE
						AND
							start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
						$where
					) AS completed";

		$row = $this->db->query($sql)->row_array();

		$data = array();

		foreach($row as $k => $v){
			$data[] = array(
				'label'		=> str_replace('_',' ', ucfirst($k)),
				'qty'		=> $row[$k]
			);
		}

		$result['status'] = 'OK';
		$result['data'] = $data;

		return $result;
	}

	public function getOutboundStatus($post = array()){
		$result = array();
		$today = date('Y-m-d');

		$sql = "SELECT
					(
						SELECT
							SUM(pq.qty)
						FROM
							pickings p
						JOIN picking_qty pq
							ON pq.picking_id=p.pl_id
						WHERE
							DATE(p.date) = '$today'
						AND
							start_time IS NOT NULL
						AND
							end_time IS NULL
					) AS picking_in_progress,
					(
						SELECT
							SUM(pq.qty)
						FROM
							pickings p
						JOIN picking_qty pq
							ON pq.picking_id=p.pl_id
						WHERE
							DATE(p.date) = '$today'
						AND
							start_time IS NULL
					) AS ready_for_pick,
					(
						SELECT
							SUM(pq.qty)
						FROM
							pickings p
						JOIN picking_qty pq
							ON pq.picking_id=p.pl_id
						JOIN shipping_picking sp
							ON sp.pl_id=p.pl_id
						JOIN shippings s
							ON s.shipping_id=sp.shipping_id
						WHERE
							DATE(p.date) = '$today'
						AND
							start_time IS NOT NULL AND end_time IS NOT NULL AND shipping_start IS NULL
					) AS ready_to_load,
					(
						SELECT
							SUM(pq.qty)
						FROM
							pickings p
						JOIN picking_qty pq
							ON pq.picking_id=p.pl_id
						JOIN shipping_picking sp
							ON sp.pl_id=p.pl_id
						JOIN shippings s
							ON s.shipping_id=sp.shipping_id
						WHERE
							DATE(p.date) = '$today'
						AND
							shipping_start IS NOT NULL AND shipping_finish IS NOT NULL
					) AS completed";

		$row = $this->db->query($sql)->row_array();

		$data = array();

		foreach($row as $k => $v){
			$data[] = array(
				'label'		=> str_replace('_',' ', ucfirst($k)),
				'qty'		=> $row[$k]
			);
		}

		$result['status'] = 'OK';
		$result['data'] = $data;

		return $result;
	}

	public function getTopReceived($post = array()){
		$result = array();

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
					itm.code as kd_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang,
					sum(first_qty)AS qty, unt.name as nama_satuan
				FROM
					item_receiving_details ird
				JOIN
					item_receiving ir
					ON ir.id=ird.item_receiving_id
				JOIN receivings r
					ON r.id=ir.receiving_id
				JOIN inbound inb
					ON inb.id_inbound=r.po_id
				JOIN warehouses wh
					ON wh.id=inb.warehouse_id
				JOIN users_warehouses uw
					ON uw.warehouse_id=wh.id
				JOIN items itm
					ON itm.id=ir.item_id
				LEFT JOIN units unt
					ON unt.id=itm.unit_id
				WHERE
					uw.user_id = ".$this->session->userdata('user_id')."
				AND
					r.finish_putaway IS NOT NULL
				AND
					DATE(ird.tgl_in) BETWEEN '".$post['start']."' AND '".$post['end']."'
				$where
				GROUP BY
					ir.item_id, itm.code, itm.name, unt.name
				ORDER BY
					qty DESC
				LIMIT 5";

		$row = $this->db->query($sql)->result_array();

		$result['data'] = $row;
		$result['status'] = 'OK';

		return $result;
	}

	public function getTopOrdered($post = array()){
		$result = array();

		$sql = "SELECT
					itm.code as kd_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang,
					(
						SELECT
							COUNT(*)
						FROM
							item_receiving_details ird2
						JOIN shippings s
							ON s.shipping_id=ird2.shipping_id
						LEFT JOIN
							item_receiving ir2
							ON ir2.id=ird2.item_receiving_id
						WHERE
							s.shipping_finish IS NOT NULL
						AND
							ir2.item_id=ir.item_id
						AND
							s.date BETWEEN '".$post['start']."' AND '".$post['end']."'
						GROUP BY
							ir.item_id
					) AS qty,
					unt.name as nama_satuan
				FROM
					item_receiving_details ird
				JOIN item_receiving ir
					ON ir.id=ird.item_receiving_id
				JOIN shippings shipp
					ON shipp.shipping_id=ird.shipping_id
				JOIN items itm
					ON itm.id=ir.item_id
				LEFT JOIN units unt
					ON unt.id=itm.unit_id
				WHERE
					shipp.shipping_finish IS NOT NULL
				AND
					shipp.date BETWEEN '".$post['start']."' AND '".$post['end']."'
				GROUP BY
					ir.item_id, itm.code, itm.name, unt.name
				ORDER BY
					qty DESC
				LIMIT 5";

		$row = $this->db->query($sql)->result_array();

		$result['data'] = $row;
		$result['status'] = 'OK';

		return $result;
	}

	public function getStatusStock($post){
		$result = array();

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
				(
					SELECT
						sum(last_qty)
					FROM
						qc qc
					JOIN item_receiving_details ird
						ON ird.qc_id=qc.id
					JOIN locations loc
						ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id
					WHERE
						uw.user_id = ".$this->session->userdata('user_id')."
					AND
						ird.qc_id=1
					AND
						loc_type <> ".$this->config->item('sentout_location')."
					$where
				) AS good,
				(
					SELECT
						COUNT(last_qty)
					FROM
						qc qc
					JOIN item_receiving_details ird
						ON ird.qc_id=qc.id
					JOIN locations loc
						ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id
					WHERE
						uw.user_id = ".$this->session->userdata('user_id')."
					AND
						ird.qc_id=2
					$where
				) AS qc_hold,
				(
					SELECT
						COUNT(last_qty)
					FROM
						qc qc
					JOIN item_receiving_details ird
						ON ird.qc_id=qc.id
					JOIN locations loc
						ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id
					WHERE
						uw.user_id = ".$this->session->userdata('user_id')."
					AND
						ird.qc_id=3
					$where
				) AS ng,
				(
					SELECT
						COUNT(last_qty)
					FROM
						qc qc
					JOIN item_receiving_details ird
						ON ird.qc_id=qc.id
					JOIN locations loc
						ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id
					WHERE
						uw.user_id = ".$this->session->userdata('user_id')."
					AND
						ird.qc_id = 4
					$where
				) AS cc_hold";

		$row = $this->db->query($sql)->row_array();

		$data = array();
		$total = 0;

		foreach($row as $k => $v){
			$data[] = array(
				'label'		=> str_replace('_',' ', strtoupper($k)),
				'qty'		=> $row[$k]
			);

			$total += $row[$k];
		}

		$result['status'] = 'OK';
		$result['data'] = $data;
		$result['total'] = $total;

		return $result;
	}

	public function getActiveStock($post){
		$result = array();

		$user_id = $this->session->userdata('user_id');

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
				COALESCE((
					SELECT
						sum(first_qty)
					FROM
						item_receiving_details ird
					JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					JOIN receivings r
						ON r.id=ir.receiving_id
					JOIN hp_receiving_inbound hri
						ON hri.receiving_id=r.id
					JOIN inbound inb
						ON inb.id_inbound=hri.inbound_id
					JOIN warehouses wh
						ON wh.id=inb.warehouse_id
					JOIN users_warehouses uw
						ON uw.warehouse_id=wh.id
					WHERE
						uw.user_id = $user_id
					AND
						location_id IN (100,107)
					$where
				),0) AS rec_qty,
				COALESCE((
					SELECT
						sum(last_qty)AS onhand_qty
					FROM
						item_receiving_details ird
					JOIN locations loc ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id
					JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					LEFT JOIN items itm
						ON itm.id=ir.item_id
					WHERE
						uw.user_id = $user_id
					AND
						location_id NOT IN (100,104)
					AND
						(CASE WHEN itm.has_qty=1 THEN ird.last_qty >=1 ELSE ird.picking_id IS NULL END)
					$where
				),0	) AS on_hand_qty,
				COALESCE( (
					SELECT
						sum(qty)
					FROM
						picking_recomendation pr
					JOIN (SELECT pl_id, min(id_outbound) as id_outbound FROM picking_list_outbound GROUP BY pl_id) as plo
						ON plo.pl_id=pr.picking_id
					JOIN outbound outb
						ON outb.id=plo.id_outbound
					JOIN warehouses wh
						ON wh.id=outb.warehouse_id
					JOIN users_warehouses uw
						ON uw.warehouse_id=wh.id
					-- LEFT JOIN shipping_picking sp
					-- 	ON sp.pl_id=pr.picking_id
					-- LEFT JOIN shippings s
					-- 	ON s.shipping_id=sp.shipping_id
					WHERE
						uw.user_id = $user_id
					AND
						pr.location_id <> 104
					$where
				),0) AS allocated_qty,
				COALESCE((
					SELECT
						COALESCE(SUM(ird.last_qty),0) as qty
					FROM
						item_receiving_details ird
					JOIN locations loc ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id
					JOIN qc qc
						ON qc.id=ird.qc_id
					WHERE
						uw.user_id = $user_id
					AND
						ird.qc_id IN (2,4)
					$where
				),0) AS hold_qty,
				COALESCE((
					SELECT
						COALESCE(SUM(ird.last_qty),0) as damage_qty
					FROM
						item_receiving_details ird
					JOIN locations loc ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id
					JOIN qc qc
						ON qc.id=ird.qc_id
					WHERE
						uw.user_id = $user_id
					AND
						ird.qc_id IN (3)
					$where
					),0) AS damage_qty";

		$row = $this->db->query($sql)->row_array();
		$row['on_hand_qty'] = $row['on_hand_qty'] + $row['allocated_qty'];
		$row['available_qty'] = $row['on_hand_qty'] - ($row['allocated_qty'] + $row['hold_qty'] + $row['damage_qty']);
		$total = $row['on_hand_qty'] + $row['rec_qty'];

		$data = array();

		foreach($row as $k => $v){
			$data[] = array(
				'label'		=> str_replace('_',' ', ucfirst($k)),
				'qty'		=> $row[$k]
			);
		}

		$result['status'] = 'OK';
		$result['data'] = $data;
		$result['total'] = $total;

		return $result;
	}

	public function getLoadingChart($post = array()){
		$result = array();

		$sql = "SELECT
					TO_CHAR(shipp.date, 'MM/YYYY') AS date, COUNT(*) AS qty
				FROM
					shippings shipp
				JOIN item_receiving_details ird
					ON ird.shipping_id=shipp.shipping_id
				JOIN item_receiving ir
					ON ir.id=ird.item_receiving_id
				JOIN items itm
					ON itm.id=ir.item_id
				JOIN locations loc
				    ON loc.id=ird.location_id
				WHERE
					DATE(shipp.date) BETWEEN '".$post['start']."' AND '".$post['end']."'
				AND
				    loc.loc_type='SENTOUT'
				GROUP BY
					date
				ORDER BY
					shipp.date ASC";

		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		for($i = 0; $i < $len; $i++){
			$result[] = array($row[$i]['date'], $row[$i]['qty']);
		}

		return $result;
	}

	public function getInboundChart($post = array()){
		$result = array();

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
					TO_CHAR(r.date, 'MM/YYYY') AS date, COUNT(*) AS qty
				FROM
					receivings r
				JOIN inbound inb
					ON inb.id_inbound=r.po_id
				JOIN warehouses wh
					ON wh.id=inb.warehouse_id
				JOIN users_warehouses uw
					ON uw.warehouse_id=wh.id
				JOIN item_receiving ir
					ON ir.receiving_id=r.id
				JOIN item_receiving_details ird
					ON ird.item_receiving_id=ir.id
				JOIN items itm
					ON itm.id=ir.item_id
				WHERE
					uw.user_id = ".$this->session->userdata('user_id')."
				AND
					DATE(r.date) BETWEEN '".$post['start']."' AND '".$post['end']."'
				$where
				GROUP BY
					r.date
				ORDER BY
					r.date ASC";

		$row = $this->db->query($sql)->result_array();

		$len = count($row);
		for($i = 0; $i < $len; $i++){
			$result[] = array($row[$i]['date'], $row[$i]['qty']);
		}

		return $result;
	}

	public function getPicking($post = array()){
		$result = array();

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
					COUNT(*) AS total_picking,
					(
						SELECT
							COUNT(*)
						FROM
							pickings p
						JOIN picking_list_outbound plo
							ON plo.pl_id=p.pl_id
						JOIN outbound outb
							ON outb.id=plo.id_outbound
						JOIN warehouses wh
							ON wh.id=outb.warehouse_id
						JOIN users_warehouses uw
							ON uw.warehouse_id=wh.id
						JOIN picking_qty pq
							ON pq.picking_id=p.pl_id
						JOIN items itm
							ON itm.id=pq.item_id
						WHERE
							uw.user_id = ".$this->session->userdata('user_id')."
						AND
							DATE(p.date) BETWEEN '".$post['start']."' AND '".$post['end']."'
						$where
					) AS total_item
				FROM
					pickings p
				JOIN picking_list_outbound plo
					ON plo.pl_id=p.pl_id
				JOIN outbound outb
					ON outb.id=plo.id_outbound
				JOIN warehouses wh
					ON wh.id=outb.warehouse_id
				JOIN users_warehouses uw
					ON uw.warehouse_id=wh.id
				WHERE
					uw.user_id = ".$this->session->userdata('user_id')."
				AND
					DATE(p.date) BETWEEN '".$post['start']."' AND '".$post['end']."'
				$where
				";

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

	public function getOutbound($post = array()){
		$result = array();

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
					COUNT(*) AS total_outbound,
					(
						SELECT
							COUNT(*)
						FROM
						(
							SELECT
								COUNT(*)
							FROM
								outbound outb
							JOIN warehouses wh
								ON wh.id=outb.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id
							JOIN outbound_item outbitm
								ON outbitm.id_outbound=outb.id
							JOIN items itm
								ON itm.id=outbitm.item_id
							WHERE
								uw.user_id = ".$this->session->userdata('user_id')."
							AND
								status_id <> 4
							AND
								outb.date BETWEEN '".$post['start']."' AND '".$post['end']."'
							$where
							GROUP BY
								outbitm.item_id
						) AS s_table
					) AS total_item
				FROM
					outbound outb
				JOIN warehouses wh
					ON wh.id=outb.warehouse_id
				JOIN users_warehouses uw
					ON uw.warehouse_id=wh.id
				WHERE
					uw.user_id = ".$this->session->userdata('user_id')."
				AND
					status_id <> 4
				AND
					date BETWEEN '".$post['start']."' AND '".$post['end']."'
				$where
				";

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

	public function getInbound($post = array()){
		$result = array();

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
					COUNT(*) AS total_inbound,
					(
						SELECT
							COUNT(*)
						FROM
						(
							SELECT
								COUNT(*)
							FROM
								inbound inb
							JOIN warehouses wh
								ON wh.id=inb.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id
							JOIN inbound_item inbitm
								ON inbitm.inbound_id=inb.id_inbound
							JOIN items itm
								ON itm.id=inbitm.item_id
							WHERE
								uw.user_id = ".$this->session->userdata('user_id')."
							AND
								status_id <> 4
							AND
								inb.date BETWEEN '".$post['start']."' AND '".$post['end']."'
							$where
							GROUP BY
								inbitm.item_id
						) AS s_table
					) AS total_item
				FROM
					inbound inb
				JOIN warehouses wh
					ON wh.id=inb.warehouse_id
				JOIN users_warehouses uw
					ON uw.warehouse_id=wh.id
				WHERE
					uw.user_id = ".$this->session->userdata('user_id')."
				AND
					status_id <> 4
				AND
					date BETWEEN '".$post['start']."' AND '".$post['end']."'
				$where
				";

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

	public function getReceiving($post = array()){
		$result = array();

		$where = "";

		if(!empty($post['warehouse'])){
			$where = "AND wh.id=".$post['warehouse'];
		}

		$sql = "SELECT
    					COUNT(*) AS total_receiving,
    					(
    						SELECT
    							COUNT(*)
    						FROM
    							receivings r
    						JOIN inbound inb
    							ON inb.id_inbound=r.po_id
    						JOIN warehouses wh
    							ON wh.id=inb.warehouse_id
    						JOIN users_warehouses uw
    							ON uw.warehouse_id=wh.id
    						JOIN item_receiving ir
    							ON ir.receiving_id=r.id
    						JOIN items itm
    							ON itm.id=ir.item_id
    						WHERE
    							uw.user_id = ".$this->session->userdata('user_id')."
    						AND
    							DATE(r.date) BETWEEN '".$post['start']."' AND '".$post['end']."'
    						$where
    					) AS total_item
    				FROM
    					receivings r
    				JOIN inbound inb
    					ON inb.id_inbound=r.po_id
    				JOIN warehouses wh
    					ON wh.id=inb.warehouse_id
    				JOIN users_warehouses uw
    					ON uw.warehouse_id=wh.id
    				WHERE
    					uw.user_id = ".$this->session->userdata('user_id')."
    				AND
    					DATE(r.date) BETWEEN '".$post['start']."' AND '".$post['end']."'
    				$where
				";

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

    public function cek_null_loc(){
        // $update = array('loc_id'=>100);
        // $where = array(
        //     'id_barang IS NOT NULL'=> NULL,
        //     'loc_id IS NULL' => NULL);

        // $this->db->where($where);
        // $this->db->update('receiving_barang',$update);
    }

    public function get_open_po(){
        $this->db->select('count(status_po_id) as jum');
        $this->db->from('purchase_order');
        $this->db->where('status_po_id',1);
        return $this->db->get()->row()->jum;
    }

    public function get_open_do(){
        $this->db->select('count(id_do) as jum');
        $this->db->from('do');
        $this->db->where('shipping_id IS NULL',NULL);
        return $this->db->get()->row()->jum;
    }

    public function get_total_stok(){
        $condition['pl_id IS NULL'] = NULL;
        $condition['st_cc'] = 0;
        $this->db->select('count(id_barang) as jum');
        $this->db->from('receiving_barang');
        $this->db->where($condition);
        return $this->db->get()->row()->jum;
    }

    public function get_graph_inbound(){
        $this->db->select('bulan,qty');
        $this->db->from('v_graph_inbound');
        return $this->db->get();
    }

    public function get_total_inbound(){
        $bulan = date('m');
        $tahun = date('Y');

        $this->db->select('count(id_barang) as qty_inbound');
        $this->db->from('receiving_barang');
        $this->db->where('month(tgl_in)',$bulan);
        $this->db->where('year(tgl_in)',$tahun);
        return $this->db->get()->row()->qty_inbound;

    }

    public function get_total_outbond(){
        $bulan = date('m');
        $tahun = date('Y');

        $this->db->select('count(rb.id_barang) as qty_outbound');
        $this->db->from('receiving_barang rb');
        $this->db->join('shipping s','s.shipping_id = rb.shipping_id','left');
        $this->db->where('month(shipping_date)',$bulan);
        $this->db->where('year(shipping_date)',$tahun);
        return $this->db->get()->row()->qty_outbound;
    }

    public function get_graph_outbound(){
        $this->db->select('bulan,qty');
        $this->db->from('v_graph_outbound');
        return $this->db->get();
	}

	// Model New Dasboard

	function getActiveSKU($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(['COALESCE(count(DISTINCT(itm.id)),0) as total'])
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('items itm','ird.item_id=itm.id')
			->join('qc','qc.id=ird.qc_id')
			->where('loc.loc_type IS NULL', NULL)
			->where('qc.id',1)
			->where('usr.id',$this->session->userdata('user_id'));

		$res = $this->db->get()->row_array();

		return $res['total'];

	}

	function getHoldSKU($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(['COALESCE(sum(last_qty),0) as total'])
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('items itm','ird.item_id=itm.id')
			->join('qc','qc.id=ird.qc_id')
			->where_not_in('loc.id',[104,101,100])
			->where_in('qc.id',[4])
			->where('usr.id',$this->session->userdata('user_id'));

		$res = $this->db->get()->row_array();

		return $res['total'];

	}

	function getQcSKU($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(['COALESCE(sum(last_qty),0) as total'])
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('items itm','ird.item_id=itm.id')
			->join('qc','qc.id=ird.qc_id')
			->where_not_in('loc.id',[104,101,100])
			->where_in('qc.id',[2])
			->where('usr.id',$this->session->userdata('user_id'));

		$res = $this->db->get()->row_array();

		return $res['total'];

	}

	function getBinLocations($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select("count(*) as total")
			->from('locations loc')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('loc.loc_type IS NULL',NULL);

		$sql = $this->db->get_compiled_select();

		$this->db
			->select(["COALESCE(($sql),0) as total"]);


		$res = $this->db->get()->row_array();

		return $res['total'];
	}

	function getFilledBinLocations($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select('loc.id as location_id')
			->from('locations loc')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('item_receiving_details ird','ird.location_id=loc.id')
			->join('users usr','usr.id=uw.user_id')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('loc.loc_type IS NULL',NULL)
			->group_by('loc.id');

		$sql = $this->db->get_compiled_select();

		$this->db
			->select(["COALESCE(COUNT(*),0) as total"])
			->from("($sql) as tbl");

		$res = $this->db->get()->row_array();

		return $res['total'];
	}

	function getInboundTopList($params){

		$this->db
			->select(["itm.code as sku", "COALESCE(count(*),0) as count","COALESCE(sum(inbitm.qty),0) as qty"])
			->from('inbound inb')
			->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound')
			->join('items itm','itm.id=inbitm.item_id')
			->where('status_id <>',100)
			->group_by('itm.code')
			->limit(10);

		$data = $this->db->get()->result_array();

		return $data;

	}

	function getInboundChartTime($params){

		$this->db
			->select(["TO_CHAR(start_tally,'HH24') as hours","COALESCE(COUNT(*),0) as qty"])
			->from('receivings')
			->where('start_tally IS NOT NULL',NULL)
      ->where('start_tally > CURRENT_DATE')
			->group_by("hours")
			->order_by('hours');

		$data = $this->db->get()->result_array();

		$datas = array();
		foreach ($data as $d) {
			$datas[$d['hours']] = $d['qty'];
		}

		$res = array();

		for ($i=0; $i <= 24 ; $i++) {

			$res[$i]['hours'] = ($i < 10) ? "0".$i : $i;

			if(array_key_exists($res[$i]['hours'], $datas)){
				$res[$i]['qty'] = $datas[$res[$i]['hours']];
			}else{
				$res[$i]['qty'] = 0;
			}

		}

		return $res;

	}

	function getInboundTimeAverage($params){

/*		$sql = "
			SELECT
				r.id,
				COALESCE(ird.first_qty) as qty,
				r.start_tally,
				r.finish_tally,
				r.start_putaway,
				r.finish_putaway
			FROM receivings r
			JOIN item_receiving ir
				ON r.id=ir.receiving_id
			LEFT JOIN item_receiving_details ird
				ON ird.item_receiving_id=ir.id
			GROUP BY
				r.id
		";
*/

		$this->db->start_cache();

		$this->db
			->select([
				"r.id",
				"COALESCE(COUNT(ird.first_qty),0) as qty",
				"r.start_tally",
				"r.finish_tally",
				"r.start_putaway",
				"r.finish_putaway"
			])
			->from('receivings r')
			->join('item_receiving ir','ir.receiving_id=r.id')
			->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left')
			->group_by('r.id, r.start_tally, r.finish_tally, r.start_putaway, r.finish_putaway');

		$this->db->stop_cache();


		$data = $this->db->get()->result_array();
		$dataLen = count($data);

		$results = array(
			"tally_average"		=> 0,
			"putaway_average"	=> 0,
			"total_average"		=> 0
		);

		if($data){

			$tallySecond = 0;
			$tallyCount = 0;

			$putawaySecond = 0;
			$putawayCount = 0;
			foreach ($data as $d) {

				if(!empty($d['start_tally']) && !empty($d['finish_tally'])){

					$dteStart = new DateTime($d['start_tally']);
					$dteEnd   = new DateTime($d['finish_tally']);
					$dtDiff = $dteStart->diff($dteEnd)->format("%Y-%M-%D %H:%I:%S");

					$tallySecond += $this->dateTimeToSecond($dtDiff);

					$tallyCount++;

				}

				if(!empty($d['start_putaway']) && !empty($d['finish_putaway'])){

					$dteStart = new DateTime($d['start_putaway']);
					$dteEnd   = new DateTime($d['finish_putaway']);
					$dtDiff = $dteStart->diff($dteEnd)->format("%Y-%M-%D %H:%I:%S");

					$putawaySecond += $this->dateTimeToSecond($dtDiff);

					$putawayCount++;

				}

			}

			$tallyAverage = ($tallySecond / $tallyCount) / 60;
			$putawayAverage = ($putawaySecond / $putawayCount) / 60;
			$totalAverage = ((($tallySecond / $tallyCount) + ($putawaySecond / $putawayCount)) / 2) / 60;

			$results['tally_average'] = number_format((float)$tallyAverage, 2, '.', '');
			$results['putaway_average'] = number_format((float)$putawayAverage, 2, '.', '');
			$results['total_average'] = number_format((float)$totalAverage, 2, '.', '');

		}


		return $results;


	}

	function dateTimeToSecond($dateTime=""){

		$tSecond = 0;

		if(!empty($dateTime)){

			$exp = explode(" ", $dateTime);

			$date = explode("-", $exp[0]);
			$time = explode(":", $exp[1]);


			if($date[0] != "00"){
				$tSecond += $date[0]*31536000;
			}

			if($date[1] != "00"){
				$tSecond += $date[1]*2628000;
			}

			if($date[2] != "00"){
				$tSecond += $date[2]*86400;
			}

			if($time[0] != "00"){
				$tSecond += $time[0]*3600;
			}

			if($time[1] != "00"){
				$tSecond += $time[1]*60;
			}

			if($time[2] != "00"){
				$tSecond += $time[2];
			}


		}

		return $tSecond;

	}

	function getNewInboundStatus($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(inb.id_inbound)),0) as total_inbound","COALESCE(SUM(inbitm.qty),0) as total_sku_qty","COALESCE(COUNT(inbitm.item_id),0) as total_sku"])
			->from('inbound inb')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound','left')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('inb.date','NOW()')
			->group_by('inb.id_inbound');

		$data = $this->db->get()->row_array();

		return $data;

	}

	function getOpenInboundStatus($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(inb.id_inbound)),0) as total_inbound","COALESCE(SUM(inbitm.qty),0) as total_sku_qty","COALESCE(COUNT(inbitm.item_id),0) as total_sku"])
			->from('inbound inb')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound','left')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('inb.status_id',3)
			->where('inb.date','NOW()')
			->group_by('inb.id_inbound');

		$data = $this->db->get()->row_array();

		return $data;

	}

	function getOnProgressInboundStatus($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(inb.id_inbound)),0) as total_inbound","COALESCE(SUM(inbitm.qty),0) as total_sku_qty","COALESCE(COUNT(inbitm.item_id),0) as total_sku"])
			->from('inbound inb')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound','left')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('inb.status_id',5)
			->where('inb.date','NOW()')
			->group_by('inb.id_inbound');

		$data = $this->db->get()->row_array();

		return $data;

	}

	function getCompleteInboundStatus($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(inb.id_inbound)),0) as total_inbound","COALESCE(SUM(inbitm.qty),0) as total_sku_qty","COALESCE(COUNT(inbitm.item_id),0) as total_sku"])
			->from('inbound inb')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound','left')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('inb.status_id',4)
			->where('inb.date','NOW()')
			->group_by('inb.id_inbound');

		$data = $this->db->get()->row_array();

		return $data;

	}


	function getOutboundTopList($params){

		$this->db
			->select(["itm.code as sku", "COALESCE(count(*),0) as count","COALESCE(sum(outbitm.qty),0) as qty"])
			->from('outbound outb')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id')
			->join('items itm','itm.id=outbitm.item_id')
			->where('status_id <>',100)
			->group_by('itm.code')
			->limit(10);

		$data = $this->db->get()->result_array();

		return $data;

	}

	function getOutboundChartTime($params){

		$this->db
			->select(["TO_CHAR(start_time,'HH24') as hours","COALESCE(COUNT(*),0) as qty"])
			->from('pickings')
			->where('start_time IS NOT NULL',NULL)
      ->where('start_time > CURRENT_DATE')
			->group_by("hours")
			->order_by('hours');

		$data = $this->db->get()->result_array();

		$datas = array();
		foreach ($data as $d) {
			$datas[$d['hours']] = $d['qty'];
		}

		$res = array();

		for ($i=0; $i <= 24 ; $i++) {

			$res[$i]['hours'] = ($i < 10) ? "0".$i : $i;

			if(array_key_exists($res[$i]['hours'], $datas)){
				$res[$i]['qty'] = $datas[$res[$i]['hours']];
			}else{
				$res[$i]['qty'] = 0;
			}

		}

		return $res;

	}

	function getOutboundTimeAverage($params){

/*		$sql = "
			SELECT
			(
				SELECT
					COALESCE((DATE_PART('minute', p.start_time::timestamp - p.end_time::timestamp) / COALESCE(SUM(pq.qty),0)),0)
				FROM pickings p
				JOIN picking_qty pq ON pq.picking_id=p.pl_id
				GROUP BY p.pl_id
			) as picking_average,
			(
				COALESCE(NULL,0)
			) as packing_average,
			(
				SELECT
					COALESCE((DATE_PART('minute', shipp.shipping_start::timestamp - shipp.shipping_finish::timestamp) / COALESCE(SUM(pq.qty),0)),0)
				FROM shippings shipp
				JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id
				JOIN picking_qty pq ON pq.picking_id=sp.pl_id
				GROUP BY shipp.shipping_id
			) as loading_average
		";

		$data = $this->db->query($sql)->result_array();
		$dataLen = count($data);

		$pickAverage = 0;
		foreach ($data as $d) {
			$pickAverage = $pickAverage + $d['picking_average'];
		}

		$packAverage = 0;
		foreach ($data as $d) {
			$packAverage = $packAverage + $d['packing_average'];
		}

		$loadAverage = 0;
		foreach ($data as $d) {
			$loadAverage = $loadAverage + $d['loading_average'];
		}

		$results = array(
			"picking_average"	=> $pickAverage,
			"packing_average"	=> $packAverage,
			"loading_average"	=> $loadAverage
		);*/

		$results = array(
			"picking_average"	=> 0,
			"packing_average"	=> 0,
			"loading_average"	=> 0
		);

		return $results;

	}

	function getNewOutboundStatus($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(outb.id)),0) as total_outbound","COALESCE(SUM(outbitm.qty),0) as total_sku_qty","COALESCE(COUNT(outbitm.item_id),0) as total_sku"])
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id','left')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('outb.date','NOW()')
			->group_by('outb.id');

		$data = $this->db->get()->row_array();

		return $data;

	}

	function getOpenOutboundStatus($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(outb.id)),0) as total_outbound","COALESCE(SUM(outbitm.qty),0) as total_sku_qty","COALESCE(COUNT(outbitm.item_id),0) as total_sku"])
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id','left')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('outb.status_id',3)
			->where('outb.date','NOW()')
			->group_by('outb.id');

		$data = $this->db->get()->row_array();

		return $data;

	}

	function getOnProgressOutboundStatus($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(outb.id)),0) as total_outbound","COALESCE(SUM(outbitm.qty),0) as total_sku_qty","COALESCE(COUNT(outbitm.item_id),0) as total_sku"])
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id','left')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('outb.status_id',5)
			->where('outb.date','NOW()')
			->group_by('outb.id');

		$data = $this->db->get()->row_array();

		return $data;

	}

	function getCompleteOutboundStatus($params){

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(outb.id)),0) as total_outbound","COALESCE(SUM(outbitm.qty),0) as total_sku_qty","COALESCE(COUNT(outbitm.item_id),0) as total_sku"])
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id','left')
			->where('usr.id',$this->session->userdata('user_id'))
			->where('outb.status_id',4)
			->where('outb.date','NOW()')
			->group_by('outb.id');

		$data = $this->db->get()->row_array();

		return $data;

	}

	function getOutboundStat($params){

		$this->db->start_cache();

		if(!empty($params['warehouse'])){
			$this->db->where_in('wh.id',$params['warehouse']);
		}

		$this->db
			->select(["COALESCE(count(*),0) as total"])
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id')
			->where('usr.id',$this->session->userdata('user_id'));

		$this->db->stop_cache();

		$this->db
			->where('outb.date','NOW()');

		$newOutbound = $this->db->get_compiled_select();

		$this->db
			->where('outb.status_id',3);

		$openOutbound = $this->db->get_compiled_select();

		$this->db
			->where('outb.status_id',5);

		$inProgressOutbound = $this->db->get_compiled_select();

		$this->db
			->where('outb.status_id',4);

		$closeOutbound = $this->db->get_compiled_select();

		$sql = "SELECT
				($newOutbound) as new_outbound,
				($openOutbound) as open_outbound,
				($inProgressOutbound) as inprogress_outbound,
				($closeOutbound) as close_outbound";

		$data = $this->db->query($sql)->row_array();

		return $data;
	}

	function getLastCycleCount(){
	}

	function getCycleCountSKU(){
	}

	function getCycleCountAccuracy(){
	}

	function getCycleCountDiscrepancies(){
	}

	function getBinStorageByType($param=""){

		$this->db->start_cache();

		if(!empty($param)){
			$this->db->where("(loc_type.name='$param' or loc_type.description='$param')",NULL);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(loc.id)),0) as total"])
			->from('locations loc')
			->join('location_types loc_type','loc_type.id=loc.location_type_id');

		$this->db->stop_cache();

		$totalBin = $this->db->get()->row()->total;

		$this->db
			->join('item_receiving_details ird','ird.location_id=loc.id');

		$filledBin = $this->db->get()->row()->total;

		if($totalBin > 0 && $filledBin > 0){
			$percentage = ($filledBin/$totalBin)*100;
			$percentage = number_format($percentage, 2, '.', ' ');
		}else{
			$percentage = 0;

		}

		return $percentage;


	}

	function getBinStorageByCategory($param){

		$this->db->start_cache();

		if(!empty($param)){
			$this->db->where("(cat.name='$param' or cat.code='$param')",NULL);
		}

		$this->db
			->select(["COALESCE(COUNT(DISTINCT(loc.id)),0) as total"])
			->from('locations loc')
			->join('categories cat','cat.id=loc.location_category_id');

		$this->db->stop_cache();

		$totalBin = $this->db->get()->row()->total;

		$this->db
			->join('item_receiving_details ird','ird.location_id=loc.id');

		$filledBin = $this->db->get()->row()->total;

		if($totalBin > 0 && $filledBin > 0){
			$percentage = ($filledBin/$totalBin)*100;
			$percentage = number_format($percentage, 2, '.', ' ');
		}else{
			$percentage = 0;

		}

		return $percentage;

	}

	function getReplenishmentSKU($params){

		$where ="";

		if(!empty($params['warehouse'])){
			$where .= "AND wh.id=".$params['warehouse'];
		}

		$sql = "
			SELECT
				COALESCE(COUNT(DISTINCT(itm.id)),0) as total
			FROM items itm
			JOIN items_warehouses itmwh ON itmwh.item_id=itm.id
			JOIN warehouses wh ON wh.id=itmwh.warehouse_id
			LEFT JOIN (
				SELECT
					item_id, COALESCE(SUM(last_qty),0) as qty
				FROM
					item_receiving_details ird
				JOIN locations loc
					ON loc.id=ird.location_id
				JOIN location_areas loc_area
					ON loc_area.id=loc.location_area_id
				JOIN warehouses wh
					ON wh.id=loc_area.warehouse_id
				WHERE
					last_qty > 0
				$where
				GROUP BY item_id
			) as ird
				ON ird.item_id=itm.id
			WHERE
				ird.qty < itm.min_reorder
			$where
		";

		$data = $this->db->query($sql)->row();

		return $data->total;
	}

	function getExpiredSKU($params){

		$where = "";

		if(!empty($params['warehouse'])){
			$where .= "AND wh.id=".$params['warehouse'];
		}

		$sql = "
			SELECT
				COALESCE(COUNT(DISTINCT(itm.id)),0) as total
			FROM items itm
			JOIN items_warehouses itmwh ON itmwh.item_id=itm.id
			JOIN warehouses wh ON wh.id=itmwh.warehouse_id
			JOIN (
				SELECT
					item_id
				FROM
					item_receiving_details ird
				JOIN locations loc
					ON loc.id=ird.location_id
				JOIN location_areas loc_area
					ON loc_area.id=loc.location_area_id
				JOIN warehouses wh
					ON wh.id=loc_area.warehouse_id
				WHERE
					last_qty > 0
				AND
					tgl_exp < CURRENT_DATE
				$where
				GROUP BY item_id
			) as ird
				ON ird.item_id=itm.id
			WHERE
				1=1
			$where
		";

		$data = $this->db->query($sql)->row();

		return $data->total;

	}

	function getNonMovingSKU($params){

		$where = '';
		$where1 = '';

		if(!empty($params['warehouse'])){
			$where 	.= "AND itm.wh=".$params['warehouse'];
			$where1 .= "AND outb.warehouse_id=".$params['warehouse'];
		}

		$sql = "
			SELECT
				count(*) as total
			FROM
				(
					SELECT
						itm.id as item_id, COALESCE(SUM(tbl.qty),0) as qty
					FROM items itm
					JOIN items_warehouses itmwh ON itmwh.item_id=itm.id
					LEFT JOIN
						(
							SELECT
								pr.item_id, COALESCE(SUM(pr.qty),0) as qty
							FROM picking_recomendation pr
							JOIN pickings p ON p.pl_id=pr.picking_id
							LEFT JOIN picking_list_outbound plo ON plo.pl_id=p.pl_id
							LEFT JOIN outbound outb ON outb.id=plo.id_outbound
							WHERE
								1=1
							$where1
							GROUP BY pr.item_id
						) as tbl
						ON tbl.item_id=itm.id
					WHERE
						1=1
						$where
					GROUP BY itm.id
					HAVING COALESCE(SUM(tbl.qty),0) < 1
				) as tbl
		";

		$data = $this->db->query($sql)->row_array();

		return $data['total'];

	}

	function getShelfLifeSKU($params){
		return 0;
	}

  function getQCItem($params){

      $sql = "
          SELECT
              (
                  SELECT
                      COALESCE(SUM(last_qty),0)
                  FROM item_receiving_details
              ) as total_item,
              (
                  SELECT
                      COALESCE(SUM(last_qty),0)
                  FROM item_receiving_details
                  WHERE qc_id=1
              ) as good_item,
              (
                  SELECT
                      COALESCE(SUM(last_qty),0)
                  FROM item_receiving_details
                  WHERE qc_id=3
              ) as damage_item,
              (
                  SELECT
                      COALESCE(SUM(last_qty),0)
                  FROM item_receiving_details
                  WHERE qc_id IN (2,4)
              ) as qc_item
      ";

      $data = $this->db->query($sql)->row_array();

      return $data;
    }

    public function getReceivingInProgress($post = array()){
  		$result = array();

  		$where = "";

  		if(!empty($post['warehouse'])){
  			$where = " AND rcv.warehouse_id=".$post['warehouse'];
  		}

  		$sql = "SELECT DISTINCT
                  inb.code AS inb_doc,
                  rcv.code AS rcv_doc,
                  src.source_name AS source
              FROM receivings AS rcv
              JOIN hp_receiving_inbound AS rcvinb
                   ON rcvinb.receiving_id = rcv.id
              JOIN inbound AS inb
                   ON inb.id_inbound = rcvinb.inbound_id
              JOIN sources AS src
                   ON src.source_id = inb.supplier_id
              WHERE rcv.st_receiving = 0 and rcv.date > current_date"
              .$where.
              "
              ORDER BY rcv_doc ASC
              LIMIT (10)";

  		$row = $this->db->query($sql)->result_array();

  		return $row;
  	}

    public function getPickingInProgress($post = array()){
  		$result = array();

  		$where = "";

  		if(!empty($post['warehouse'])){
  			$where = " AND warehouse_id=".$post['warehouse'];
  		}

  		$sql = "SELECT DISTINCT
                  outb.code AS outb_doc,
                  pl.name AS pl_doc,
                  dest.destination_name AS destination
              FROM pickings AS pl
              JOIN picking_list_outbound AS plout
                   ON plout.pl_id = pl.pl_id
              JOIN outbound AS outb
                   ON outb.id = plout.id_outbound
              JOIN destinations AS dest
                   ON dest.destination_id = outb.destination_id
              WHERE pl.pl_status = 0 "
              .$where.
              "
			  and pl.date > current_date
              ORDER BY pl_doc ASC
              LIMIT (10)
              ";

  		$row = $this->db->query($sql)->result_array();

  		return $row;
  	}
}

?>

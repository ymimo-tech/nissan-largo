<?php
class vehicles_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'vehicles';

    public function data($condition = array()) {

        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;

    }

    public function get_by_id($id) {
        $this->db->where('a.vehicle_id',$id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id'])){
            $condition['vehicle_id'] = $condition['id'];
            unset($condition['id']);
        }
        $this->data($condition);

        return $this->db->get();

    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('vehicle_id' => $id));
    }

    public function delete($id) {

		$result = array();

        $t1 = $this->db->get_where('inbound',['supplier_id'=>$id])->row_array();

		if($t1){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('vehicle_id' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}

        return $result;

    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        for ($i=0; $i < $dataLen ; $i++) {

            $insert= array(
                'vehicle_name'          => $data[$i]['vehicle_name'],
                'vehicle_plate'          => $data[$i]['vehicle_plate']
            );

            $this->db
                ->from($this->table)
                ->where('LOWER(vehicle_name)', strtolower($data[$i]['vehicle_name']))
                ->or_where('lower(vehicle_plate)', strtolower($data[$i]['vehicle_plate']));

            $check = $this->db->get()->row_array();

            if($check){

                $id = $check['vehicle_id'];
                $this->db->where('vehicle_id',$id);
                $this->db->update($this->table,$insert);

            }else{

                $this->db->insert($this->table,$insert);
                $id = $this->db->insert_id();

            }

        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );
    }

}

?>

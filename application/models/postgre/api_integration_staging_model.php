<?php

class api_integration_staging_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }
	
	function getBinTransferDocList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('INVENTORY_TRANSFER_REQ');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListByBinTransferDocCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('INVENTORY_TRANSFER_REQ')
				->where('BASEENTRY', $params['id_staging']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getInboundList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('PURCHASE_ORDER');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getGRDocList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('GOODS_RECEIPT_REQUEST');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getSalesReturnDocList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('RETURN_REQUEST');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getGoodsReturnDocList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('GOODS_RETURN_REQUEST');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getAPCMDocList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('AP_CREDITMEMO_REQUEST');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getARCMDocList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('AR_CREDITMEMO_REQUEST');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getOutboundList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('PICK_LIST');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getGIDocList() {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('GOODS_ISSUE_REQUEST');
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListByInboundCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('PURCHASE_ORDER')
				->where('DOCENTRY', $params['id_staging']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListByGRDocCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('GOODS_RECEIPT_REQUEST')
				->where('BASEENTRY', $params['id_staging'])
				->where('GRTYPE', $params['document_code']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListBySalesReturnDocCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('RETURN_REQUEST')
				->where('BASEENTRY', $params['id_staging']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListByGoodsReturnDocCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('GOODS_RETURN_REQUEST')
				->where('BASEENTRY', $params['id_staging']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListByAPCMDocCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('AP_CREDITMEMO_REQUEST')
				->where('BASEENTRY', $params['id_staging']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListByARCMDocCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('AR_CREDITMEMO_REQUEST')
				->where('BASEENTRY', $params['id_staging']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListByOutboundCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('PICK_LIST')
				->where('DOCENTRY', $params['id_staging']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function getItemListByGIDocCode($params = array()) {
		$result = false;
		
		$staging = $this->load->database('staging', TRUE);
		$staging->select('*')
				->from('GOODS_ISSUE_REQUEST')
				->where('BASEENTRY', $params['id_staging'])
				->where('GITYPE', $params['document_code']);
		
		$data = $staging->get();
		
		if($data->num_rows() > 0) {
			$result = $data->result_array();
		}
		
		return $result;
	}
	
	function insert_bin_transfer($params = array()) {
		$result = false;
		
		if($params) {
			$id_staging = $params['id_staging'];
			$code = $params['code'];
			$warehouses = $this->db->get_where('warehouses',['name' => strval($params['id_warehouse'])])->row_array();
			$id_warehouse = $warehouses['id'];
			$status_id = $params['status_id'];
			$user = $params['user'];
			$userData = $this->db->get_where('users',['user_name' => strval($user)])->row_array();
			
			$check_bin_transfer = $this->db->get_where('bin_transfer',['bin_transfer_code' => strval($code), 'id_staging' => strval($id_staging)]);
			if($check_bin_transfer->num_rows() == 0) {
				$this->db->trans_start();
				$data_insert 	= array(
									'id_staging'			=> $id_staging,
									'bin_transfer_code'		=> $code,
									'status_id'				=> $status_id,
									'user_id'				=> $userData['id'],
									'created_at'			=> date('Y-m-d H:i:s'),
									'warehouse_id'			=> $id_warehouse
								);
				
				$this->db->insert('bin_transfer', $data_insert);
				$this->db->trans_complete();
				
				$this->db->limit(1);
				$this->db->order_by('bin_transfer_id','desc');
				$bin_transfer_id = $this->db->get('bin_transfer')->row_array()['bin_transfer_id'];
				return $bin_transfer_id;
			} else {
				$this->db->set('status_id', $status_id);
				$this->db->where('id_staging', $check_bin_transfer['bin_transfer_id']);
				$this->db->update('bin_transfer');
				
				return $check_bin_transfer['bin_transfer_id'];
			}
		}
		
		return $result;
	}
	
	function insert_bin_transfer_item($params = array()) {
		$result = false;
		
		if($params) {
			$bin_transfer_id = $params['bin_transfer_id'];
			$qty = $params['qty'];
			$itemData = $this->db->get_where('items',['code' => strval($params['item_id'])])->row_array();
			if(count($itemData) > 0) {
				$item_id = $itemData['id'];
				
				$check_bin_transfer_item = $this->db->get_where('bin_transfer_item',['item_id' => intval($item_id), 'bin_transfer_id' => $bin_transfer_id]);
				
				if($check_bin_transfer_item->num_rows() == 0) {
					$data_insert	= 	array(
											'bin_transfer_id'	=> $bin_transfer_id,
											'quantity'			=> $qty,
											'item_id'			=> $item_id,
											'unit_id'			=> $itemData['unit_id']
										);					
										
					$this->db->insert('bin_transfer_item', $data_insert);
				} else {
					$this->db->set('item_id', $item_id);
					$this->db->set('quantity', $qty);
					$this->db->where('bin_transfer_id', $bin_transfer_id);
					$this->db->update('bin_transfer_item');
				}
				$result = true;
			}
		}
		
		return $result;
	}

	function insert_inbound($params = array()) {
		$result = false;
		
		if($params) {			
			$id_staging = $params['id_staging'];
			$code = $params['code'];
			$date = $params['date'];
			if($params['document_code'] == 'SALES RETURN' || $params['document_code'] == 'AR CREDIT MEMO') {
				$suppliers = $this->db->get_where('destinations',['destination_code' => strval($params['id_supplier'])])->row_array();
				$id_supplier = (count($suppliers) > 0) ? $suppliers['destination_id'] : 17;
			} else {
				$suppliers = $this->db->get_where('sources',['source_code' => strval($params['id_supplier'])])->row_array();
				$id_supplier = (count($suppliers) > 0) ? $suppliers['source_id'] : 17;
			}
			$document_code = $params['document_code'];
			$user = $params['user'];
			$remark = $params['remark'];
			$warehouses = $this->db->get_where('warehouses',['name' => strval($params['id_warehouse'])])->row_array();
			$id_warehouse = $warehouses['id'];
			$status_id = $params['status_id'];
			$po_number = $params['po_number'];
			$st_staging = $params['st_staging'];
			
			$check_inbound = $this->check_inbound($params);
			if($check_inbound->num_rows() == 0) {
				$params['doc_type'] = 'INBOUND';
				$getDocDetail = $this->get_doc_detail($params);
				$document_id = $getDocDetail->result_array()[0]['document_id'];
				
				$getUserDetail = $this->get_user_detail($params);
				$user_id = $getUserDetail->result_array()[0]['id'];
				
				$data_insert	= 	array(
										'id_staging'			=> $id_staging,
										'code'					=> $code,
										'date'					=> $date,
										'supplier_id'			=> $id_supplier,
										'document_id'			=> $document_id,
										'user_id'				=> $user_id,
										'remark'				=> $remark,
										'status_id'				=> $status_id,
										'warehouse_id'			=> $id_warehouse,
										'po_number'				=> $po_number,
										'st_staging'			=> $st_staging,
									);					
									
				$this->db->insert('inbound', $data_insert);
				$id_inbound = $this->db->insert_id();
				
				$inbound_warehouses_insert = array(
										'inbound_id'			=> $id_inbound,
										'warehouse_id'			=> $id_warehouse,
									);
				$this->db->insert('inbound_warehouses', $inbound_warehouses_insert);
									
				$result = $id_inbound;
			} else {
				$this->db->set('supplier_id', $id_supplier);
				$this->db->set('status_id', $status_id);
				$this->db->set('po_number', $po_number);
				$this->db->set('st_staging', $st_staging);
				$this->db->where('id_inbound', $check_inbound->row_array()['id_inbound']);
				$this->db->where('status_id', 3);
				$this->db->update('inbound');
				return $check_inbound->row_array()['id_inbound'];
			}
		}		
		
		return $result;
	}
	
	function check_inbound($params = array()){
		
		$result = false;
		
		if($params) {
			$id_staging = $params['id_staging'];
			$code = $params['code'];
			
			$this->db->select('*');
			$this->db->from('inbound');
			$this->db->where('id_staging', $id_staging);
			$this->db->where('code', $code);
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	function get_doc_detail($params = array()){
		
		$result = false;
		
		if($params) {
			$code = $params['document_code'];
			$doc_type = $params['doc_type'];
			
			$this->db->select('*');
			$this->db->from('documents');
			$this->db->where('code', $code);
			$this->db->or_where('name', $code);
			$this->db->where('type', $doc_type);
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	function get_user_detail($params = array()){
		
		$result = false;
		
		if($params) {
			$user = $params['user'];
			
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_name', $user);
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	function insert_inbound_item($params = array()) {
		$result = false;
		
		if($params) {			
			$id_staging = $params['id_staging'];
			$qty = $params['qty'];
			$item_id = $this->db->get_where('items',['code' => strval($params['item_id'])])->row_array();
			if(count($item_id) > 0) {
				$item_id = $item_id['id'];
				
				$inbound_detail = $this->get_inbound_detail_by_staging_id($params);
				$id_inbound = $inbound_detail->result_array()[0]['id_inbound'];
				
				$params['id_inbound'] = $id_inbound;
				$check_inbound_item = $this->check_inbound_item($params);
				
				if($check_inbound_item->num_rows() == 0) {
					$data_insert	= 	array(
											'inbound_id'	=> $id_inbound,
											'qty'			=> $qty,
											'item_id'		=> $item_id,
											'id_staging'	=> $id_staging,
										);					
										
					$this->db->insert('inbound_item', $data_insert);
				} else {
					$this->db->set('item_id', $item_id);
					$this->db->set('qty', $qty);
					$this->db->where('id', $check_inbound_item->result_array()[0]['id']);
					$this->db->update('inbound_item');
				}
				$result = true;
			}
		}		
		
		return $result;
	}
	
	function get_inbound_detail_by_staging_id($params = array()) {
		
		$result = false;
		
		if($params) {
			$id_staging = $params['id_staging'];
			$code = $params['code'];
			
			$this->db->select('*');
			$this->db->from('inbound');
			$this->db->where('id_staging', $id_staging);
			$this->db->where('code', $code);
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	function check_inbound_item($params = array()){
		
		$result = false;
		
		if($params) {
			$id_inbound = $params['id_inbound'];
			$item_id = $this->db->get_where('items',['code' => strval($params['item_id'])])->row_array();
			
			$this->db->select('*');
			$this->db->from('inbound_item');
			$this->db->where('inbound_id', $id_inbound);
			$this->db->where('item_id', $item_id['id']);
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	function insert_outbound($params = array()) {
		$result = false;
		
		if($params) {		
			$id_staging = $params['id_staging'];
			$code = $params['code'];
			$date = $params['date'];
			if($params['document_code'] == 'GOODS RETURN' || $params['document_code'] == 'AP CREDIT MEMO') {
				$destinations = $this->db->get_where('sources',['source_code' => strval($params['customer_id'])])->row_array();
				$customer_id = (count($destinations) > 0) ? $destinations['source_id'] : 9027;
			} else {
				$destinations = $this->db->get_where('destinations',['destination_code' => strval($params['customer_id'])])->row_array();
				$customer_id = (count($destinations) > 0) ? $destinations['destination_id'] : 9027;
			}
			$document_code = $params['document_code'];
			$shipping_group = $params['shipping_group'];
			$user = $params['user'];
			$note = $params['note'];
			$ekspedisi = $params['ekspedisi'];
			$warehouses = $this->db->get_where('warehouses',['name' => strval($params['id_warehouse'])])->row_array();
			$id_warehouse = $warehouses['id'];
			$status_id = $params['status_id'];
			
			$check_outbound = $this->check_outbound($params);
			if($check_outbound->num_rows() == 0) {
				$params['doc_type'] = 'OUTBOUND';
				$getDocDetail = $this->get_doc_detail($params);
				$document_id = $getDocDetail->result_array()[0]['document_id'];
				
				$getUserDetail = $this->get_user_detail($params);
				$user_id = $getUserDetail->result_array()[0]['id'];
				
				if($document_code == 'MFG') {
					$getWarehouseDetail = $this->db->get_where('warehouses',['id' => intval($id_warehouse)])->row_array();
					$destination_id = $getWarehouseDetail['id'];
					$destination_name = $getWarehouseDetail['name'];
					$destination_address = $getWarehouseDetail['address'];
					$destination_city = $getWarehouseDetail['name'];
				} else {
					$params['customer_id'] = $customer_id;
					$getDestinationDetail = $this->get_destination_detail($params);
					if($params['document_code'] == 'GOODS RETURN') {
						$destination_id = $getDestinationDetail->result_array()[0]['source_id'];
						$destination_name = $getDestinationDetail->result_array()[0]['source_name'];
						$destination_address = $getDestinationDetail->result_array()[0]['source_address'];
						$destination_city = $getDestinationDetail->result_array()[0]['source_city'];
					} else {
						$destination_id = $getDestinationDetail->result_array()[0]['destination_id'];
						$destination_name = $getDestinationDetail->result_array()[0]['destination_name'];
						$destination_address = $getDestinationDetail->result_array()[0]['destination_address'];
						$destination_city = $getDestinationDetail->result_array()[0]['destination_city'];
					}
				}
				
				$data_insert	= 	array(
										'id_staging'				=> $id_staging,
										'code'						=> $code,
										'date'						=> $date,
										'delivery_date'				=> $date,
										'destination_id'			=> $destination_id,
										'destination_name'			=> $destination_name,
										'address'					=> $destination_address,
										'shipping_group'			=> $shipping_group,
										'document_id'				=> $document_id,
										'user_id'					=> $user_id,
										'note'						=> $note,
										'ekspedisi'					=> $ekspedisi,
										'status_id'					=> $status_id,
										'warehouse_id'				=> $id_warehouse,
										'm_priority_id'				=> $params['m_priority_id'],
										'type'						=> 'FIFO',
									);			
				$this->db->insert('outbound', $data_insert);
									
				$result = true;
			} else {
				$this->db->set('status_id', $status_id);
				$this->db->where('id', $check_outbound->row_array()['id']);
				$this->db->where('status_id', 3);
				$this->db->update('outbound');
				return false;
			}
		}		
		
		return $result;
	}
	
	function insert_outbound_item($params = array()) {
		$result = false;
		
		if($params) {			
			$id_staging = $params['id_staging'];
			$outbound_code = $params['code'];
			$qty = $params['qty'];
			$item_id = $this->db->get_where('items',['code' => strval($params['item_id'])])->row_array();
			$unit_id = 4;
			
			if(count($item_id) > 0) {
				$item_id = $item_id['id'];
				$outbound_detail = $this->get_outbound_detail_by_staging_id($params);
				$id_outbound = $outbound_detail->result_array()[0]['id'];
				
				// $check_outbound_item = $this->check_outbound_item($params, $id_outbound);
				
				// if($check_outbound_item->num_rows() == 0) {
					$data_insert	= 	array(
											'id_outbound'			=> intval($id_outbound),
											'id_staging'			=> intval($id_staging),
											'qty'					=> intval($qty),
											'item_id'				=> intval($item_id),
											'unit_id'				=> intval($unit_id),
										);					
										
					$this->db->insert('outbound_item', $data_insert);
				// } else {
					// $this->db->set('item_id', intval($item_id));
					// $this->db->set('unit_id', intval($unit_id));
					// $this->db->set('qty', intval($qty));
					// $this->db->where('id', $check_outbound_item->result_array()[0]['id']);
					// $this->db->update('outbound_item');
				// }
				$result = true;
			}
		}		
		
		return $result;
	}
	
	function check_outbound($params = array()){
		
		$result = false;
		
		if($params) {
			$id_staging = $params['id_staging'];
			$code = $params['code'];
			
			$this->db->select('*');
			$this->db->from('outbound');
			$this->db->where('id_staging', $id_staging);
			$this->db->where('code', $code);
			$this->db->where('status_id != 17', null);
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	function check_outbound_item($params = array(), $id_outbound = ''){
		
		$result = false;
		
		if($params) {
			$id_staging = $params['id_staging'];
			$item_id = $this->db->get_where('items',['code' => strval($params['item_id'])])->row_array();
			
			$this->db->select('*');
			$this->db->from('outbound_item');
			$this->db->where('id_staging', $id_staging);
			// $this->db->where('code', $params['code']);
			$this->db->where('item_id', $item_id['id']);
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	function get_outbound_detail_by_staging_id($params = array()) {
		
		$result = false;
		
		if($params) {
			$id_staging = $params['id_staging'];
			$code = $params['code'];
			
			$this->db->select('*');
			$this->db->from('outbound');
			$this->db->where('id_staging', $id_staging);
			$this->db->where('code', $code);
			$this->db->where('status_id != 17', null);
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	function get_destination_detail($params = array()) {
		
		$result = false;
		
		if($params) {
			if($params['document_code'] == 'GOODS RETURN') {
				$destination_id = $params['customer_id'];
				
				$this->db->select('*');
				$this->db->from('sources');
				$this->db->where('source_id', $destination_id);
			} else {
				$destination_id = $params['customer_id'];
				
				$this->db->select('*');
				$this->db->from('destinations');
				$this->db->where('destination_id', $destination_id);
			}
			
			$result = $this->db->get();
		}
		
		return $result;
		
	}
	
	/* --BEGIN RECEIPT-- */
	function post_receipt($kd_receiving = '', $unique_code = ''){
		return true;
		$result = false;
		
		if($unique_code != '') {
			$this->db->select(["doc.code AS doc_code", "COALESCE(source.source_code,dest.destination_code) AS CARDCODE", "inb.code AS DOCNUM", "inb.id_staging AS BASEENTRY", "inb.po_number AS BASETYPE", "inb.st_staging AS BASELINE", "inb.date AS DOCDATE", "rcv.code AS receiving", "TO_CHAR(ird.putaway_time::DATE, 'yyyy-mm-dd') AS date", "u.user_name AS operator", "rcv.vehicle_plate AS vehicle", "inb.status_id AS status"]);
			$this->db->from('inbound inb');
			$this->db->join('documents doc', 'inb.document_id = doc.document_id');
			$this->db->join('item_receiving ir', 'inb.id_inbound = ir.inbound_id');
			$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
			$this->db->join('item_receiving_details ird', 'ird.item_receiving_id = ir.id');
			$this->db->join('users u', 'ird.user_id_putaway = u.id', 'LEFT');
			$this->db->join('sources source', 'inb.supplier_id = source.source_id', 'LEFT');
			$this->db->join('destinations dest', 'inb.supplier_id = dest.destination_id', 'LEFT');
			$this->db->where('rcv.code', $kd_receiving);
			$this->db->where('ird.unique_code', $unique_code);
			
			$result = $this->db->get();
			
			$this->db->select("COUNT(ird.unique_code) as total_line");
			$this->db->from('inbound inb');
			$this->db->join('documents doc', 'inb.document_id = doc.document_id');
			$this->db->join('item_receiving ir', 'inb.id_inbound = ir.inbound_id');
			$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
			$this->db->join('item_receiving_details ird', 'ird.item_receiving_id = ir.id');
			$this->db->join('users u', 'ird.user_id_putaway = u.id', 'LEFT');
			$this->db->join('sources source', 'inb.supplier_id = source.source_id', 'LEFT');
			$this->db->join('destinations dest', 'inb.supplier_id = dest.destination_id', 'LEFT');
			$this->db->where('rcv.code', $kd_receiving);
			$result2 = $this->db->get();
			
			if($result->num_rows() > 0) {
				$data = $result->result_array();
				$data2 = $result2->result_array();

				$this->db->select(["(SELECT sum(first_qty) from item_receiving_details where item_id = itm.id and unique_code = '".$unique_code."') AS QTY", "itm.id AS product_id", "itm.code AS ITEMCODE", "COALESCE(ird.first_qty, 0) AS binqty", "itm.unit_id AS uom", "ird.tgl_exp AS date_expired", "ird.unique_code AS pallet", "ird.location_id AS location_id", "loc.absentry AS BIN", "wloc.name AS WAREHOUSE"]);
				$this->db->from('item_receiving_details ird');
				$this->db->join('item_receiving ir', 'ird.item_receiving_id = ir.id');
				$this->db->join('items itm', 'ir.item_id = itm.id');
				$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
				$this->db->join('locations loc', 'loc.id = ird.location_id');
				$this->db->join('location_areas locarea', 'locarea.id = loc.location_area_id', 'LEFT');
				$this->db->join('warehouses wloc', 'wloc.id = locarea.warehouse_id', 'LEFT');
				$this->db->where('rcv.code', $kd_receiving);
				$this->db->where('ird.unique_code', $unique_code);
				$this->db->group_by('ird.first_qty, itm.id, itm.code, itm.unit_id, loc.name, wloc.name, ird.tgl_exp, ird.unique_code, ird.location_id, loc.absentry');
				
				$productsGet = $this->db->get();
				if($data[0]['doc_code'] == 'INTRANSIT') {
					for($i = 0; $i < $result->num_rows(); $i++) {
						
						if($productsGet->num_rows() > 0) {
							$productsData = $productsGet->result_array();
							
							echo "<pre>";
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM INVENTORY_TRANSFER_REQ WHERE RECEIPTNUM = '".$data[$i]['docnum']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								
								$this->db->select("SUM(ird.first_qty) as total_qty");
								$this->db->from('item_receiving_details ird');
								$this->db->join('item_receiving ir', 'ird.item_receiving_id = ir.id');
								$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
								$this->db->join('items itm', 'itm.id=ir.item_id');
								$this->db->join('inbound inb', 'inb.id_inbound = ir.inbound_id');
								$this->db->where('rcv.code', $kd_receiving);
								$this->db->where('itm.code', $productsData[$j]['itemcode']);
								$this->db->where('inb.code', $data[$i]['docnum']);
								// $this->db->where('ird.location_id > 110',null);
								// $this->db->group_by('ird.first_qty');
								// $this->db->order_by('ird.first_qty','desc');
								
								$productsData[$j]['total_qty'] = $this->db->get()->row_array()['total_qty'];
								
								$this->db->select(["inb.code", "ird.item_id", "count(ird.item_id) as total_line"]);
								$this->db->from('inbound inb');
								$this->db->join('documents doc', 'inb.document_id = doc.document_id');
								$this->db->join('item_receiving ir', 'inb.id_inbound = ir.inbound_id');
								$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
								$this->db->join('item_receiving_details ird', 'ird.item_receiving_id = ir.id');
								$this->db->where('rcv.code', $kd_receiving);
								$this->db->group_by('inb.code, ird.item_id');
								$result2 = $this->db->get();
								
								$stagingCheckQuery = "SELECT * FROM INVENTORY_TRANSFER WHERE BASEENTRY = '".$stagingData['BASEENTRY']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingCheckData = $staging->query($stagingCheckQuery)->row_array();
								if(count($stagingCheckData) == 0) {
									// $deleteStagingQuery = "DELETE FROM INVENTORY_TRANSFER WHERE BASEENTRY = '".$stagingData['BASEENTRY']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
									// $deleteStagingCheck = $staging->query($deleteStagingQuery);
									$data_insert_1	= 	array(
										'DOCDATE'			=> $stagingData['DOCDATE'],
										'DOCDUEDATE'		=> $stagingData['DOCDUEDATE'],
										'TAXDATE'			=> date('Y-m-d'),
										'BASEENTRY'			=> $stagingData['BASEENTRY'],
										'BASELINE'			=> $stagingData['BASELINE'],
										'BASETYPE'			=> $stagingData['BASETYPE'],
										'ITEMCODE'			=> $productsData[$j]['itemcode'],
										'QTY'				=> $productsData[$j]['total_qty'],
										'FROM_WAREHOUSE'	=> $stagingData['FROM_WAREHOUSE'],
										'TO_WAREHOUSE'		=> $stagingData['TO_WAREHOUSE'],
										'INTERNALNO'		=> $kd_receiving,
										'TOTAL_LINE'		=> $result2->num_rows(),
									);
									$staging->insert('INVENTORY_TRANSFER', $data_insert_1);
								}
								
								$stagingCheckQuery2 = "SELECT * FROM ITBINFROM WHERE INTERNALNO = '".$kd_receiving."' AND BASEENTRY = ".$stagingData['BASEENTRY']." AND BASELINE = " . $stagingData['BASELINE'];
								$stagingCheckData2 = $staging->query($stagingCheckQuery2)->row_array();
								if(count($stagingCheckData2) == 0) {
									$data_insert_2	= 	array(
										'BASEENTRY'			=> $stagingData['BASEENTRY'],
										'BASELINE'			=> $stagingData['BASELINE'],
										'BINENTRY'			=> NULL,
										'BINQTY'			=> $productsData[$j]['total_qty'],
										'INTERNALNO'		=> $kd_receiving,
									);
									$staging->insert('ITBINFROM', $data_insert_2);
								}
								
								if($productsData[$j]['qty'] > 0) {
									$data_insert_3	= 	array(
										'BASEENTRY'			=> $stagingData['BASEENTRY'],
										'BASELINE'			=> $stagingData['BASELINE'],
										'BINENTRY'			=> $productsData[$j]['bin'],
										'BINQTY'			=> $productsData[$j]['qty'],
										'INTERNALNO'		=> $kd_receiving,
									);
									
									$staging->insert('ITBINTO', $data_insert_3);
								}
							}
						}
					}
				} else if($data[0]['doc_code'] == 'PO') {
					for($i = 0; $i < $result->num_rows(); $i++) {
						
						if($productsGet->num_rows() > 0) {
							$productsData = $productsGet->result_array();
							
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM PURCHASE_ORDER WHERE BASEENTRY = ".$data[$i]['baseentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								
								
								// $stagingCheckQuery = "SELECT * FROM PURCHASE_ORDER WHERE BASEENTRY = '".$data[$i]['baseentry']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								// $stagingCheckData = $staging->query($stagingCheckQuery)->row_array();
								// if(count($stagingCheckData) > 0) {
									$this->db->select("SUM(ird.first_qty) as total_qty");
									$this->db->from('inbound inb');
									$this->db->join('documents doc', 'inb.document_id = doc.document_id');
									$this->db->join('item_receiving ir', 'inb.id_inbound = ir.inbound_id');
									$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
									$this->db->join('item_receiving_details ird', 'ird.item_receiving_id = ir.id');
									$this->db->join('users u', 'ird.user_id_putaway = u.id', 'LEFT');
									$this->db->join('sources source', 'inb.supplier_id = source.source_id', 'LEFT');
									$this->db->join('destinations dest', 'inb.supplier_id = dest.destination_id', 'LEFT');
									$this->db->join('items itm', 'itm.id=ir.item_id', 'LEFT');
									$this->db->where('rcv.code', $kd_receiving);
									$this->db->where('itm.code', $productsData[$j]['itemcode']);
									$this->db->where('ird.location_id > 110',null);
									$this->db->group_by('ird.first_qty');
									
									$productsData[$j]['total_qty'] = $this->db->get()->row_array()['total_qty'];
									
									// $deleteStagingQuery = "DELETE FROM GRPO WHERE BASEENTRY = '".$stagingData['BASEENTRY']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
									// $deleteStagingCheck = $staging->query($deleteStagingQuery);
								// }
								
								
								$data_insert	= 	array(
									'DOCENTRY'		=> $data[$i]['baseentry'],
									'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $data[$i]['cardcode'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $data[$i]['baseentry'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'BINQTY'		=> $productsData[$j]['binqty'],
									'QTY'			=> $productsData[$j]['total_qty'],
									'WAREHOUSE'		=> $productsData[$j]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['bin'],
									'INTERNALNO'	=> $data[$i]['receiving'],
									'TOTAL_LINE'	=> $data2[$i]['total_line'],
								);
								
								$staging = $this->load->database('staging', TRUE);
								$staging->insert('GRPO', $data_insert);
							}
						}
					}
				} else if($data[0]['doc_code'] == 'SR') {
					for($i = 0; $i < $result->num_rows(); $i++) {
						
						if($productsGet->num_rows() > 0) {
							$productsData = $productsGet->result_array();
							
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM RETURN_REQUEST WHERE BASEENTRY = ".$data[$i]['baseentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									'DOCENTRY'		=> $data[$i]['baseentry'],
									//'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $data[$i]['cardcode'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									// 'RETURNDATE'	=> date('Y-m-d'),
									'BASEENTRY'		=> $data[$i]['baseentry'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'RETURNREASON'	=> $stagingData['RETURNREASON'],
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									// 'BINQTY'		=> $productsData[$j]['binqty'],
									'QTY'			=> $productsData[$j]['binqty'],
									'WAREHOUSE'		=> $productsData[$j]['warehouse'],
									// 'BIN'			=> $productsData[$j]['bin'],
									'INTERNALNO'	=> $data[$i]['receiving'],
									'TOTAL_LINE'	=> $data2[$i]['total_line'],
								);
								
								$staging = $this->load->database('staging', TRUE);
								$staging->insert('AR_RETURN', $data_insert);
							}
						}
					}
				} else if($data[0]['doc_code'] == 'ARCM') {
					for($i = 0; $i < $result->num_rows(); $i++) {
						
						if($productsGet->num_rows() > 0) {
							$productsData = $productsGet->result_array();
							
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM AR_CREDITMEMO_REQUEST WHERE BASEENTRY = ".$data[$i]['baseentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									//'DOCENTRY'		=> $data[$i]['baseentry'],
									'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $stagingData['CARDCODE'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $data[$i]['baseentry'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'], 
									'RETURNREASON'	=> $stagingData['RETURNREASON'],
									'INVOICE'		=> $stagingData['INVOICE'], 
									'UNITPRICE'		=> $stagingData['UNITPRICE'], 
									'DISCHEADER'	=> $stagingData['DISCHEADER'], 
									'DISCDETAIL'	=> $stagingData['DISCDETAIL'], 
									'TAXCODE'		=> $stagingData['TAXCODE'], 
									'ACCOUNT'		=> $stagingData['ACCOUNT'], 
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'BINQTY'		=> $productsData[$j]['binqty'],
									'QTY'			=> $productsData[$j]['binqty'],
									'WAREHOUSE'		=> $productsData[$j]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['bin'],
									'INTERNALNO'	=> $data[$i]['receiving'],
									'TOTAL_LINE'	=> $data2[$i]['total_line'],
								);
								
								$staging = $this->load->database('staging', TRUE);
								$staging->insert('AR_CREDITMEMO', $data_insert);
							}
						}
					}
				} else {
					for($i = 0; $i < $result->num_rows(); $i++) {
						
						if($productsGet->num_rows() > 0) {
							$productsData = $productsGet->result_array();
							
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM GOODS_RECEIPT_REQUEST WHERE BASEENTRY = ".$data[$i]['baseentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									// 'DOCENTRY'		=> $data[$i]['baseentry'],
									'DOCNUM'		=> $data[$i]['docnum'],
									// 'CARDCODE'		=> $data[$i]['cardcode'],
									'DOCDATE'		=> $data[$i]['docdate'],
									// 'DOCDUEDATE'	=> $data[$i]['docdate'],
									// 'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $data[$i]['baseentry'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'GRTYPE'		=> $stagingData['GRTYPE'],
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'BINQTY'		=> $productsData[$j]['binqty'],
									'QTY'			=> $productsData[$j]['binqty'],
									'WAREHOUSE'		=> $productsData[$j]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['bin'],
									'INTERNALNO'	=> $data[$i]['receiving'],
									'TOTAL_LINE'	=> $data2[$i]['total_line'],
								);
								
								$staging = $this->load->database('staging', TRUE);
								$staging->insert('GOODS_RECEIPT', $data_insert);
							}
						}
					}
				}
			}
		}
		
		return $result;
		
	}
	/* --END RECEIPT-- */
	
	/* --BEGIN LOADING-- */
	function post_loading($ship_code = '', $unique_code = ''){
		return true;
		$result = false;
		
		if($ship_code != '' && $unique_code != '') {
			$unique_code = str_replace('%2A','*',$unique_code);
			$this->db->select(["outb.code AS docnum", "outb.id_staging AS docentry", "outb.date AS docdate", "pick.name AS pl_name", "u.user_name AS operator", "outb.status_id AS status", "doc.code AS doc_code", "outb.warehouse_id AS destination_id", "COALESCE(COALESCE(dest.destination_code, wdest.name),s.source_code) AS cardcode", "w.name AS warehouse"]);
			$this->db->from('outbound outb');
			$this->db->join('picking_list_outbound plo', 'outb.id = plo.id_outbound');
			$this->db->join('pickings pick', 'plo.pl_id = pick.pl_id');
			$this->db->join('picking_recomendation pr', 'pick.pl_id = pr.picking_id');
			$this->db->join('users u', 'pr.user_id = u.id');
			$this->db->join('documents doc', 'outb.document_id = doc.document_id');
			$this->db->join('destinations dest', 'outb.destination_id=dest.destination_id', 'LEFT');
			$this->db->join('sources s', 'outb.destination_id=s.source_id', 'LEFT');
			$this->db->join('warehouses wdest', 'outb.destination_id=wdest.id', 'LEFT');
			$this->db->join('warehouses w', 'outb.warehouse_id=w.id');
			$this->db->where('pr.unique_code', $unique_code);
			$this->db->where('pick.pl_id', $ship_code);
			$this->db->order_by('pr.id', 'DESC');
			$this->db->limit(1);
			
			$result = $this->db->get();
			
			if($result->num_rows() > 0) {
				$jsonData = array();
				$data = $result->result_array();
				
				$this->db->select(["ird.item_id AS product_id", "outb.code AS docnum", "outb.id_staging AS docentry", "itm.code AS itemcode", "COALESCE(pr.qty, 0) AS qty", "pr.unit_id AS uom", "ird.tgl_exp AS date_expired", "pr.unique_code AS pallet", "l.absentry AS absentry"]);
				$this->db->select(["(SELECT count(unique_code) FROM picking_recomendation WHERE picking_id = pr.picking_id AND qty > 0) AS total_line"]);
				$this->db->select(["(SELECT sum(qty) FROM picking_recomendation WHERE picking_id = pr.picking_id AND qty > 0 AND item_id = pr.item_id) AS total_qty"]);
				$this->db->from('picking_recomendation pr');
				$this->db->join('item_receiving_details ird', 'pr.item_receiving_detail_id = ird.id');
				$this->db->join('items itm', 'ird.item_id = itm.id');
				$this->db->join('pickings pick', 'pr.picking_id = pick.pl_id');
				$this->db->join('picking_qty pq', 'pick.pl_id = pq.picking_id');
				$this->db->join('locations l', 'pr.old_location_id = l.id');
				$this->db->join('outbound outb', 'pr.outbound_id = outb.id');
				// $this->db->join('outbound_item outbitm','outbitm.item_id=pr.item_id and pr.outbound_id=outbitm.id_outbound');
				$this->db->where('pr.unique_code', $unique_code);
				$this->db->where('pick.pl_id', $ship_code);
				$this->db->group_by('ird.item_id, outb.code, outb.id_staging, itm.code, pr.qty, pr.item_id, pr.unit_id, ird.tgl_exp, pr.unique_code, pr.picking_id, l.absentry');
				
				$productsGet = $this->db->get();
				
				for($i = 0; $i < $result->num_rows(); $i++) {
					if($data[0]['doc_code'] == 'SO') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();
							
							// echo '<pre>';
							// var_dump($productsData);
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM PICK_LIST WHERE DOCENTRY = '".$productsData[$j]['docentry']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								
								// var_dump($query);exit;
								
								$stagingCheckQuery = "SELECT * FROM DELIVERY_ORDER WHERE DOCENTRY = '".$data[$i]['docentry']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingCheckData = $staging->query($stagingCheckQuery)->row_array();
								if(count($stagingCheckData) > 0) {
									
								} else {
									$data_insert	= 	array(
										'DOCENTRY'		=> $data[$i]['docentry'],
										'CARDCODE'		=> $data[$i]['cardcode'],
										'DOCDATE'		=> $data[$i]['docdate'],
										'DOCDUEDATE'	=> $data[$i]['docdate'],
										'TAXDATE'		=> date('Y-m-d'),
										'BASEENTRY'		=> $stagingData['BASEENTRY'],
										'BASELINE'		=> $stagingData['BASELINE'],
										'BASETYPE'		=> $stagingData['BASETYPE'],
										'ITEMCODE'		=> $productsData[$j]['itemcode'],
										'BINQTY'		=> $productsData[$j]['qty'],
										'QTY'			=> $productsData[$j]['total_qty'],
										'TOTAL_LINE'	=> $productsData[$j]['total_line'],
										'WAREHOUSE'		=> $data[$i]['warehouse'],
										'BINENTRY'		=> $productsData[$j]['absentry'],
										'INTERNALNO'	=> $data[$i]['pl_name'],
									);
									
									$staging = $this->load->database('staging', TRUE);
									$staging->insert('DELIVERY_ORDER', $data_insert);
								}
							}
						}
					} else if($data[0]['doc_code'] == 'APCM') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();
							
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM AP_CREDITMEMO_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $stagingData['CARDCODE'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $stagingData['BASEENTRY'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'INVOICE'		=> $stagingData['INVOICE'], 
									'UNITPRICE'		=> $stagingData['UNITPRICE'], 
									'DISCHEADER'	=> $stagingData['DISCHEADER'], 
									'DISCDETAIL'	=> $stagingData['DISCDETAIL'], 
									'TAXCODE'		=> $stagingData['TAXCODE'], 
									'ACCOUNT'		=> $stagingData['ACCOUNT'], 
									'RETURNREASON'	=> $stagingData['RETURNREASON'], 
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'QTY'			=> $productsData[$j]['total_qty'],
									'BINQTY'		=> $productsData[$j]['qty'],
									'TOTAL_LINE'	=> $productsData[$j]['total_line'],
									'WAREHOUSE'		=> $data[$i]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['absentry'],
									'INTERNALNO'	=> $data[$i]['pl_name'],
								);
								
								$staging = $this->load->database('staging', TRUE);
								$staging->insert('AP_CREDITMEMO', $data_insert);
							}
						}
					} else if($data[0]['doc_code'] == 'GRTN') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();
							
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM GOODS_RETURN_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									// 'DOCENTRY'		=> $data[$i]['docentry'],
									'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $data[$i]['cardcode'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $stagingData['BASEENTRY'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'RETURNREASON'	=> $stagingData['RETURNREASON'],
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'QTY'			=> $productsData[$j]['total_qty'],
									'BINQTY'		=> $productsData[$j]['qty'],
									'TOTAL_LINE'	=> $productsData[$j]['total_line'],
									'WAREHOUSE'		=> $data[$i]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['absentry'],
									'INTERNALNO'	=> $data[$i]['pl_name'],
									// 'RETURNDATE'	=> date('Y-m-d')
								);
								
								$staging = $this->load->database('staging', TRUE);
								$staging->insert('GOODS_RETURN', $data_insert);
							}
						}
					} else {
						$productsArray = array();
						$productsData = $productsGet->result_array();
						
						for($j = 0; $j < $productsGet->num_rows(); $j++) {
							$staging = $this->load->database('staging', TRUE);
							$query = "SELECT * FROM GOODS_ISSUE_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
							$stagingData = $staging->query($query)->row_array();
							$data_insert	= 	array(
								'CARDCODE'		=> $data[$i]['cardcode'],
								'DOCDATE'		=> $data[$i]['docdate'],
								'DOCDUEDATE'	=> $data[$i]['docdate'],
								'TAXDATE'		=> date('Y-m-d'),
								'BASEENTRY'		=> $stagingData['BASEENTRY'],
								'BASELINE'		=> $stagingData['BASELINE'],
								'BASETYPE'		=> $stagingData['BASETYPE'],
								'ITEMCODE'		=> $productsData[$j]['itemcode'],
								// 'BINQTY'		=> $productsData[$j]['qty'],
								'QTY'			=> $productsData[$j]['qty'],
								'TOTAL_LINE'	=> $productsData[$j]['total_line'],
								'WAREHOUSE'		=> $data[$i]['warehouse'],
								// 'BINENTRY'		=> $productsData[$j]['absentry'],
								'INTERNALNO'	=> $data[$i]['pl_name'],
							);
							
							$staging = $this->load->database('staging', TRUE);
							$staging->insert('GOODS_ISSUE', $data_insert);
						}
					}
				}
			}
		}
		
		return $result;
		
	}
	/* --END LOADING-- */
	
	/* --BEGIN BIN TRANSFER-- */
	function post_bin_transfer($bin_transfer_id = '', $unique_code = '') {
		return true;
		$result = false;
		
		$this->db->select(["bt.bin_transfer_id", "bt.bin_transfer_code as docnum", "w.name as warehouse", "bts.quantity as qty", "bt.id_staging as baseentry", "i.code as itemcode", "lfrom.absentry as loc_from", "lto.absentry as loc_to"])
					->from("bin_transfer bt")
					->join("bin_transfer_scanned bts","bt.bin_transfer_id = bts.bin_transfer_id")
					->join("item_receiving_details ird","bts.item_receiving_detail_id = ird.id")
					->join("warehouses w","bt.warehouse_id = w.id")
					->join("locations lfrom","bts.from_location_id = lfrom.id")
					->join("locations lto","bts.to_location_id = lto.id")
					->join("items i","bts.item_id = i.id")
					->where("bt.bin_transfer_code",$bin_transfer_id)
					->where("ird.unique_code",$unique_code);
					
		$datas = $this->db->get();
		
		if($datas->num_rows > 0) {
			$data = $datas->row_array();
			
			$staging = $this->load->database('staging', TRUE);
			$query = "SELECT * FROM INVENTORY_TRANSFER_REQ WHERE BASEENTRY = ".$data['baseentry']." AND ITEMCODE = '".$data['itemcode']."'";
			$stagingData = $staging->query($query)->row_array();
			$data_insert_1	= 	array(
				'DOCDATE'			=> $stagingData['DOCDATE'],
				'DOCDUEDATE'		=> $stagingData['DOCDUEDATE'],
				'TAXDATE'			=> date('Y-m-d'),
				'BASEENTRY'			=> $stagingData['BASEENTRY'],
				'BASELINE'			=> $stagingData['BASELINE'],
				'BASETYPE'			=> $stagingData['BASETYPE'],
				'ITEMCODE'			=> $data['itemcode'],
				'QTY'				=> $data['qty'],
				'FROM_WAREHOUSE'	=> $data['warehouse'],
				'TO_WAREHOUSE'		=> $data['warehouse'],
			);
			
			$data_insert_2	= 	array(
				'BASEENTRY'			=> $stagingData['BASEENTRY'],
				'BASELINE'			=> $stagingData['BASELINE'],
				'BINENTRY'			=> $data['loc_from'],
				'BINQTY'			=> $data['qty']
			);
			
			$data_insert_3	= 	array(
				'BASEENTRY'			=> $stagingData['BASEENTRY'],
				'BASELINE'			=> $stagingData['BASELINE'],
				'BINENTRY'			=> $data['loc_to'],
				'BINQTY'			=> $data['qty']
			);
		} else {
			$this->db->select(["bt.bin_transfer_id", "bt.bin_transfer_code as docnum", "w.name as warehouse", "bts.quantity as qty", "bt.id_staging as baseentry", "i.code as itemcode", "lfrom.absentry as loc_from", "lto.absentry as loc_to"])
					->from("bin_transfer bt")
					->join("bin_transfer_scanned bts","bt.bin_transfer_id = bts.bin_transfer_id")
					->join("item_receiving_details ird","bts.item_receiving_detail_id = ird.id")
					->join("warehouses w","bt.warehouse_id = w.id")
					->join("locations lfrom","bts.from_location_id = lfrom.id")
					->join("locations lto","bts.to_location_id = lto.id")
					->join("items i","bts.item_id = i.id")
					->where("bt.bin_transfer_code",$bin_transfer_id)
					->where("ird.unique_code",$unique_code);
					
			$datas = $this->db->get();
			
			$data_insert_1	= 	array(
				'DOCDATE'			=> date('Y-m-d'),
				'DOCDUEDATE'		=> date('Y-m-d'),
				'TAXDATE'			=> date('Y-m-d'),
				'BASEENTRY'			=> NULL,
				'BASELINE'			=> NULL,
				'BASETYPE'			=> NULL,
				'ITEMCODE'			=> $data['itemcode'],
				'QTY'				=> $data['qty'],
				'FROM_WAREHOUSE'	=> $data['warehouse'],
				'TO_WAREHOUSE'		=> $data['warehouse'],
			);
			
			$data_insert_2	= 	array(
				'BASEENTRY'			=> NULL,
				'BASELINE'			=> NULL,
				'BINENTRY'			=> $data['loc_from'],
				'BINQTY'			=> $data['qty']
			);
			
			$data_insert_3	= 	array(
				'BASEENTRY'			=> NULL,
				'BASELINE'			=> NULL,
				'BINENTRY'			=> $data['loc_to'],
				'BINQTY'			=> $data['qty']
			);
		}
		
		$staging->insert('INVENTORY_TRANSFER', $data_insert_1);
		$staging->insert('ITBINFROM', $data_insert_2);
		$staging->insert('ITBINTO', $data_insert_3);
		
		return $result;
	}
	/* --END BIN TRANSFER-- */
	
	public function repost_gr($rcv_code) {
		$this->db->select('unique_code')
				->from('item_receiving_details ird')
				->join('item_receiving ir','ird.item_receiving_id=ir.id')
				->join('receivings r','ir.receiving_id=r.id')
				->where('r.code',$rcv_code);
		
		$res = $this->db->get();
		$data = $res->result_array();
		
		// echo '<pre>';
		// var_dump($data);
		
		for($i=0;$i<$res->num_rows();$i++) {
			
			$this->post_receipt($rcv_code, $data[$i]['unique_code']);
			// echo $data[$i]['unique_code']. ',';
			
		}
		
		// var_dump($data);
	}
	
	public function reinsert_picking() {
		$delete = 'DELETE FROM picking_qty';
		$this->db->query($delete);
		
		$reinsert = 'insert into picking_qty(picking_id,item_id,qty,unit_id)
					select pl_id, oi.item_id, oi.qty, oi.unit_id
					from picking_list_outbound plo
					join outbound_item oi on plo.id_outbound=oi.id_outbound';
		$this->db->query($reinsert);
	}
	
	public function repost_gi($picking) {
		$this->db->select('unique_code')
				->from('picking_recomendation pr')
				// ->where('pr.picking_id',$picking)
				->where_in('pr.picking_id',$picking)
				->where('qty > 0',null);
		
		$res = $this->db->get();
		$data = $res->result_array();
		
		for($i=0;$i<$res->num_rows();$i++) {
			
			$this->post_loading($picking, $data[$i]['unique_code']);
			
		}
		
		// echo "<pre>";
		// var_dump($data);
	}
	
	public function repost_loading($picking) {
		$this->db->select('unique_code')
				->from('picking_recomendation pr')
				->where('pr.picking_id',$picking)
				->where('qty > 0',null);
		
		$res = $this->db->get();
		$data = $res->result_array();
		
		for($i=0;$i<$res->num_rows();$i++) {
			
			$this->repost_loading_r($picking, $data[$i]['unique_code']);
			
		}
		
		echo "<pre>";
		var_dump($data);
	}
	
	public function repost_loading_r($ship_code = '', $unique_code = ''){
		
		$result = false;
		
		if($ship_code != '' && $unique_code != '') {
			$unique_code = str_replace('%2A','*',$unique_code);
			$this->db->select(["outb.code AS docnum", "outb.id_staging AS docentry", "outb.date AS docdate", "pick.name AS pl_name", "u.user_name AS operator", "outb.status_id AS status", "doc.code AS doc_code", "outb.warehouse_id AS destination_id", "COALESCE(COALESCE(dest.destination_code, wdest.name),s.source_code) AS cardcode", "w.name AS warehouse"]);
			$this->db->from('outbound outb');
			$this->db->join('picking_list_outbound plo', 'outb.id = plo.id_outbound');
			$this->db->join('pickings pick', 'plo.pl_id = pick.pl_id');
			$this->db->join('picking_recomendation pr', 'pick.pl_id = pr.picking_id');
			$this->db->join('users u', 'pr.user_id = u.id');
			$this->db->join('documents doc', 'outb.document_id = doc.document_id');
			$this->db->join('destinations dest', 'outb.destination_id=dest.destination_id', 'LEFT');
			$this->db->join('sources s', 'outb.destination_id=s.source_id', 'LEFT');
			$this->db->join('warehouses wdest', 'outb.destination_id=wdest.id', 'LEFT');
			$this->db->join('warehouses w', 'outb.warehouse_id=w.id');
			$this->db->where('pr.unique_code', $unique_code);
			$this->db->where('pick.pl_id', $ship_code);
			$this->db->order_by('pr.id', 'DESC');
			$this->db->limit(1);
			
			$result = $this->db->get();
			
			if($result->num_rows() > 0) {
				$jsonData = array();
				$data = $result->result_array();
				
				$this->db->select(["ird.item_id AS product_id", "outb.code AS docnum", "outb.id_staging AS docentry", "itm.code AS itemcode", "COALESCE(pr.qty, 0) AS qty", "pr.unit_id AS uom", "ird.tgl_exp AS date_expired", "pr.unique_code AS pallet", "l.absentry AS absentry"]);
				$this->db->select(["(SELECT count(unique_code) FROM picking_recomendation WHERE picking_id = pr.picking_id AND qty > 0) AS total_line"]);
				$this->db->select(["(SELECT sum(qty) FROM picking_recomendation WHERE picking_id = pr.picking_id AND qty > 0 AND item_id = pr.item_id) AS total_qty"]);
				$this->db->from('picking_recomendation pr');
				$this->db->join('item_receiving_details ird', 'pr.item_receiving_detail_id = ird.id');
				$this->db->join('items itm', 'ird.item_id = itm.id');
				$this->db->join('pickings pick', 'pr.picking_id = pick.pl_id');
				$this->db->join('picking_qty pq', 'pick.pl_id = pq.picking_id');
				$this->db->join('locations l', 'pr.old_location_id = l.id');
				$this->db->join('outbound outb', 'pr.outbound_id = outb.id');
				// $this->db->join('outbound_item outbitm','outbitm.item_id=pr.item_id and pr.outbound_id=outbitm.id_outbound');
				$this->db->where('pr.unique_code', $unique_code);
				$this->db->where('pick.pl_id', $ship_code);
				$this->db->group_by('ird.item_id, outb.code, outb.id_staging, itm.code, pr.qty, pr.item_id, pr.unit_id, ird.tgl_exp, pr.unique_code, pr.picking_id, l.absentry');
				
				$productsGet = $this->db->get();
				
				for($i = 0; $i < $result->num_rows(); $i++) {
					if($data[0]['doc_code'] == 'SO') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();
							
							// echo '<pre>';
							// var_dump($productsData);
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM PICK_LIST WHERE DOCENTRY = '".$productsData[$j]['docentry']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								
								// var_dump($query);exit;
								
								// $stagingCheckQuery = "SELECT * FROM DELIVERY_ORDER WHERE DOCENTRY = '".$data[$i]['docentry']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								// $stagingCheckData = $staging->query($stagingCheckQuery)->row_array();
								// if(count($stagingCheckData) > 0) {
									
								// } else {
									$data_insert	= 	array(
										'DOCENTRY'		=> $data[$i]['docentry'],
										'CARDCODE'		=> $data[$i]['cardcode'],
										'DOCDATE'		=> $data[$i]['docdate'],
										'DOCDUEDATE'	=> $data[$i]['docdate'],
										'TAXDATE'		=> date('Y-m-d'),
										'BASEENTRY'		=> $stagingData['BASEENTRY'],
										'BASELINE'		=> $stagingData['BASELINE'],
										'BASETYPE'		=> $stagingData['BASETYPE'],
										'ITEMCODE'		=> $productsData[$j]['itemcode'],
										'BINQTY'		=> $productsData[$j]['qty'],
										'QTY'			=> $productsData[$j]['total_qty'],
										'TOTAL_LINE'	=> $productsData[$j]['total_line'],
										'WAREHOUSE'		=> $data[$i]['warehouse'],
										'BINENTRY'		=> $productsData[$j]['absentry'],
										'INTERNALNO'	=> $data[$i]['pl_name'],
									);
									
									$staging = $this->load->database('staging', TRUE);
									$staging->insert('DELIVERY_ORDER', $data_insert);
								// }
							}
						}
					} else if($data[0]['doc_code'] == 'APCM') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();
							
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM AP_CREDITMEMO_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $stagingData['CARDCODE'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $stagingData['BASEENTRY'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'INVOICE'		=> $stagingData['INVOICE'], 
									'UNITPRICE'		=> $stagingData['UNITPRICE'], 
									'DISCHEADER'	=> $stagingData['DISCHEADER'], 
									'DISCDETAIL'	=> $stagingData['DISCDETAIL'], 
									'TAXCODE'		=> $stagingData['TAXCODE'], 
									'ACCOUNT'		=> $stagingData['ACCOUNT'], 
									'RETURNREASON'	=> $stagingData['RETURNREASON'], 
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'QTY'			=> $productsData[$j]['total_qty'],
									'BINQTY'		=> $productsData[$j]['qty'],
									'TOTAL_LINE'	=> $productsData[$j]['total_line'],
									'WAREHOUSE'		=> $data[$i]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['absentry'],
									'INTERNALNO'	=> $data[$i]['pl_name'],
								);
								
								$staging = $this->load->database('staging', TRUE);
								$staging->insert('AP_CREDITMEMO', $data_insert);
							}
						}
					} else if($data[0]['doc_code'] == 'GRTN') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();
							
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM GOODS_RETURN_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									// 'DOCENTRY'		=> $data[$i]['docentry'],
									'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $data[$i]['cardcode'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $stagingData['BASEENTRY'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'RETURNREASON'	=> $stagingData['RETURNREASON'],
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'QTY'			=> $productsData[$j]['total_qty'],
									'BINQTY'		=> $productsData[$j]['qty'],
									'TOTAL_LINE'	=> $productsData[$j]['total_line'],
									'WAREHOUSE'		=> $data[$i]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['absentry'],
									'INTERNALNO'	=> $data[$i]['pl_name'],
									// 'RETURNDATE'	=> date('Y-m-d')
								);
								
								$staging = $this->load->database('staging', TRUE);
								$staging->insert('GOODS_RETURN', $data_insert);
							}
						}
					} else {
						$productsArray = array();
						$productsData = $productsGet->result_array();
						
						for($j = 0; $j < $productsGet->num_rows(); $j++) {
							$staging = $this->load->database('staging', TRUE);
							$query = "SELECT * FROM GOODS_ISSUE_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
							$stagingData = $staging->query($query)->row_array();
							$data_insert	= 	array(
								'CARDCODE'		=> $data[$i]['cardcode'],
								'DOCDATE'		=> $data[$i]['docdate'],
								'DOCDUEDATE'	=> $data[$i]['docdate'],
								'TAXDATE'		=> date('Y-m-d'),
								'BASEENTRY'		=> $stagingData['BASEENTRY'],
								'BASELINE'		=> $stagingData['BASELINE'],
								'BASETYPE'		=> $stagingData['BASETYPE'],
								'ITEMCODE'		=> $productsData[$j]['itemcode'],
								// 'BINQTY'		=> $productsData[$j]['qty'],
								'QTY'			=> $productsData[$j]['qty'],
								'TOTAL_LINE'	=> $productsData[$j]['total_line'],
								'WAREHOUSE'		=> $data[$i]['warehouse'],
								// 'BINENTRY'		=> $productsData[$j]['absentry'],
								'INTERNALNO'	=> $data[$i]['pl_name'],
							);
							
							$staging = $this->load->database('staging', TRUE);
							$staging->insert('GOODS_ISSUE', $data_insert);
						}
					}
				}
			}
		}
		
		return $result;
		
	}

	public function getItemStock($post=[]){

		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'nama_kategori',
			4 => 'nama_satuan',
			5 => 'loc_name',
			6 => 'loc_type',
			7 => 'qty',
			8 => 'minimum_stock'
		);

		$params = '';
		$getQty = false;
		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if($params == 'receiving'){

			$this->db->where_in('l.id',[100,107]);
			// $this->db->where('l.loc_type','INBOUND');
		}

		if($params == 'onhand'){

			$this->db->where_not_in('l.id',[100,107]);

			// $locType = "(l.loc_type IS NULL OR l.loc_type='QC' OR l.loc_type='NG')";
			// $this->db->where($locType, NULL);
			// $this->db->select(["COALESCE( (sum(ird.last_qty) + COALESCE(sum(pr.qty),0) ),0) as qty"]);

			// $getQty = true;

		}

		if($params == 'allocated'){

			$this->db->select(["COALESCE(sum(pr.qty),0) as qty"]);
				$this->db->where('pr.st_shipping IS NULL or pr.st_shipping = 0',NULL);

			$getQty = true;

		}

		if($params == 'suspend'){
			$this->db->where('ird.qc_id', 2);
		}

		if($params == 'damage'){
			$this->db->where('ird.qc_id', 3);
		}

		if($params == 'available'){
			$this->db->where('ird.qc_id', 1);
			$this->db->where('l.loc_type', NULL);
			$this->db->where_not_in('l.id',[100,107]);
		}

		if(!empty($post['id_qc'])){
			$this->db->where_in('qc_id', $post['id_qc']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		if(!$getQty){
			$this->db->select(["COALESCE( (sum(ird.last_qty) + COALESCE(sum(pr.qty),0) ),0) as qty"]);
		}

		if(!empty($post['id_qc'])){
			$this->db->where_in('ird.qc_id',$post['id_qc']);
		}

		if(!empty($post['loc_id'])){
			$this->db->where_in('ird.location_id',$post['loc_id']);
		}

		if(!empty($post['loc_type'])){
			$this->db->where_in('l.loc_type',$post['loc_type']);
		}

		$this->db
			->select([
				"ird.item_id","SUM(COALESCE(p.pl_id, 0)) as pl_id",
				"string_agg(DISTINCT CAST(qc_id as VARCHAR), ',') as id_qc_all",
				// "string_agg(DISTINCT CAST(ird.location_id as VARCHAR), ', ') as loc_id_all",
				// "string_agg(DISTINCT l.loc_type, ', ') as loc_type_all",
				// "string_agg(DISTINCT l.name, ',') as loc_name"
			])
			->from('item_receiving_details ird')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('receivings r','r.id=ir.receiving_id','left')
			->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id','left')
			->join('inbound inb','inb.id_inbound=hpi.inbound_id','left')
			->join("(
					SELECT
						pr.st_shipping, pr.unique_code, pr.location_id, o.warehouse_id, COALESCE(SUM(pr.qty),0) as qty
					FROM
						picking_recomendation pr
					JOIN pickings p
						ON p.pl_id=pr.picking_id
					JOIN (SELECT pl_id, min(id_outbound) as id_outbound FROM picking_list_outbound GROUP BY pl_id) as plo
						ON plo.pl_id=p.pl_id
					JOIN outbound o
						ON o.id=plo.id_outbound
					WHERE
						pr.location_id <> 104
					GROUP BY pr.unique_code, pr.location_id, o.warehouse_id, pr.st_shipping
				) as pr
				",'pr.unique_code=ird.unique_code','left')
			->join('locations l','l.id=ird.location_id')
			->join('location_areas loc_area','loc_area.id=l.location_area_id','left')
			->join('warehouses wh','wh.id=loc_area.warehouse_id and l.id NOT IN (100,101,102,103,105,106,107) or l.id IN (100,101,102,103,105,106,107) and wh.id=inb.warehouse_id')
			// ->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('items itm','itm.id=ird.item_id','left')
			->where_not_in('ird.location_id', [104])
			->group_by(['ird.item_id','itm.has_qty']);

		$rb = $this->db->get_compiled_select();

		$this->db->start_cache();

		if(!empty($post['id_barang']))
			$this->db->where_in('itm.id', $post['id_barang']);

		if(!empty($post['id_satuan']))
			$this->db->where_in('itm.unit_id', $post['id_satuan']);

		if(!empty($post['id_qc'])){
			$qcLen = count($post['id_qc']);

			$qc ='(';

			for ($i=0; $i < $qcLen; $i++) {
				if($i == 0)
					$qc .= " rb.id_qc_all like '%".$post['id_qc'][$i]."%'";
				else
					$qc .= " OR rb.id_qc_all like '%".$post['id_qc'][$i]."%'";
			}

			$qc .= ')';

			$this->db->where($qc, NULL);
		}

		if(!empty($post['id_kategori']))
			$this->db->where_in('itm.category_id', $post['id_kategori']);

		if(!empty($post['loc_id'])){
			$locId = $post['loc_id'];
			$len = count($locId);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_id_all', $locId[$i]);
			}
		}

		if(!empty($post['loc_type'])){
			$locType = $post['loc_type'];
			$len = count($locType);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_type_all', $locType[$i]);
			}
		}

		if($params != ''){
			if($params == 'low'){
				$this->db->where("COALESCE(rb.qty,0) <= minimum_stock", NULL);
			}else{
				$this->db->where("rb.qty >", 0);
			}
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}


		$this->db
			->select(["itm.id as id_barang", "itm.name as nama_barang", "itm.code as kd_barang", "cat.name as nama_kategori", "unt.name as nama_satuan", "COALESCE(rb.qty, 0) as qty"])
			->from('items itm')
			// ->join('items_warehouses iw','iw.item_id=itm.id')
			// ->join('warehouses wh','wh.id=iw.warehouse_id')
			// ->join('users_warehouses uw','uw.warehouse_id=wh.id')
			// ->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('categories cat','cat.id=itm.category_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->join("($rb) as rb",'rb.item_id=itm.id','left')
			->group_by('itm.id, itm.name, itm.code, itm.minimum_stock, cat.name, unt.name, rb.qty');

		$this->db->stop_cache();

		$row = $this->db->get()->result_array();


		return $row;

	}
	
}

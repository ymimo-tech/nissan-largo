<?php
class config_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'config';
    private $table4 = 'config_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';

    private function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->order_by('a.kd_config ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail($condition = array()) {
        $this->db->select('*,b.id as id_config_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_config = b.id_config');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_config'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_config= $this->input->post("kd_config");
        $id_proyek= $this->input->post("kd_proyek");
        $tanggal_awal = $this->input->post('tanggal_awal_config');
        $tanggal_akhir = $this->input->post('tanggal_akhir_config');

        if(!empty($kd_config)){
            $condition["a.kd_config like '%$kd_config%' or a.nama_config like '%$kd_config%'"]=null;
            //$condition["a.nama_config like '%$kd_config%'"]=null;
        }
        if(!empty($id_proyek)){
            $condition["a.id_proyek"]=$id_proyek;
        }

        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.tanggal_config >= '$tanggal_awal'"] = null ;
            $condition["a.tanggal_config <= '$tanggal_akhir'"] = null;
        }

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_config;
            $action = '';
            $action .= anchor(null, '<i class="icon-cog"></i>', array('id' => 'drildown_key_t_config_' . $id, 'class' => 'btn', 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_config', 'data-original-title' => 'Detil Proses',  'data-source' => base_url('config/get_detail_config/' . $id))) . ' ';

            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('config/edit/' . $id))) . ' ';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('config/delete/' . $id)));
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_config,
                    'tanggal_config' => hgenerator::switch_tanggal($value->tanggal_config),
                    'kd_config' => $value->kd_config,
                    'nama_config' => $value->nama_config,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_config,
                    'tanggal_config' => hgenerator::switch_tanggal($value->tanggal_config),
                    'kd_config' => $value->kd_config,
                    'nama_config' => $value->nama_config,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function data_table_detail() {
        // Filtering
        $condition = array();
        $kd_config= $this->input->post("kd_config");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_config = $this->input->post('id_config');
        if(!empty($kd_config)){
            $condition["a.kd_config like '%$kd_config%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_config)){
            $condition["a.id_config"]=$id_config;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));

        
        // Total Record
        $total = $this->data_detail($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail($condition)->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_config_barang;
            $action = '';

            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('config/edit_detail/' .$id_config.'/'. $id))) . ' ';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('config/delete_detail/' .$id_config.'/'. $id)));
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    } 

    public function data_table_excel() {
        // Filtering
        $condition = array();
        $kd_config= $this->input->post("kd_config");
        $id_proyek= $this->input->post("kd_proyek");
        $tanggal_awal = $this->input->post('tanggal_awal_config');
        $tanggal_akhir = $this->input->post('tanggal_akhir_config');

        if(!empty($kd_config)){
           $condition["a.kd_config like '%$kd_config%'"]=null;
        }
        if(!empty($id_proyek)){
            $condition["a.id_proyek"]=$id_proyek;
        }

        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.tanggal_config >= '$tanggal_awal'"] = null ;
            $condition["a.tanggal_config <= '$tanggal_akhir'"] = null;
        }

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_config;
            $action = '';
                $rows[] = array(
                    'tanggal_config' => hgenerator::switch_tanggal($value->tanggal_config),
                    'kd_config' => $value->kd_config,
                    'nama_config' => $value->nama_config,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }  

    public function data_table_detail_excel() {
        // Filtering
        $condition = array();
        $kd_config= $this->input->post("kd_config");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_config = $this->input->post('id_config');
        if(!empty($kd_config)){
            $condition["a.kd_config like '%$kd_config%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_config)){
            $condition["a.id_config"]=$id_config;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));

        
        // Total Record
        $total = $this->data_detail($condition)->count_all_results();

        // List Data
        $data_detail = $this->data_detail($condition)->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_config_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_config' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table4, array('id_config' => $id));
        return $this->db->delete($this->table, array('id_config' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Pilih Kode Config--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_config] = $row->kd_config.' / '.$row->nama_config ;
        }
        return $options;
    }

    
}

?>
<?php
class loading_model extends CI_Model {

    public function __construct() {
        parent::__construct();
	}


	public function manifestPacking($params){
		$requestData = $_REQUEST;

		$data = [];

		$results = $this->db
					->select(["oird.id_outbound_receiving_barang","oird.id_item_receiving_details","oird.id_outbound","oird.packing_number",
					"oird.inc","itm.code", "itm.name","ird.unique_code", "SUM(pr.qty) as qty", "unt.name as uom_name","
					(
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(pr.qty)* CAST(itm.weight as float8)),0) as weight FROM shippings shipp  
							JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id 
							JOIN pickings pl ON pl.pl_id=sp.pl_id
							JOIN picking_qty pq ON pq.picking_id=pl.pl_id
							JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=pq.item_id
							LEFT JOIN items itm ON itm.id=pr.item_id
							WHERE itm.id = 1 group by itm.weight
						) as tbl
					) as weight",
					"(
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(pr.qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM shippings shipp 
							JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id
							JOIN pickings pl ON pl.pl_id=sp.pl_id
							JOIN picking_qty pq ON pq.picking_id=pl.pl_id
							JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=pq.item_id
							LEFT JOIN items itm ON itm.id=pr.item_id
							WHERE itm.id =1 group by itm.height, itm.width, itm.length
						) as tbl
					) as volume"])
					->from('outbound_item_receiving_details oird')
					->join('item_receiving_details ird', 'ird.id=oird.id_item_receiving_details', 'LEFT')
					->join('picking_recomendation pr', 'pr.item_receiving_detail_id=ird.id', 'LEFT')
					->join('items itm', 'itm.id=ird.item_id', 'LEFT')
					->join('units unt','unt.id=itm.unit_id','left')
					->join('outbound outb', 'outb.id = oird.id_outbound', 'LEFT')
					->join('picking_list_outbound plo', 'plo.id_outbound=outb.id')
					->join('pickings p', 'p.pl_id=plo.pl_id')
					->join('shipping_picking sp','sp.pl_id=p.pl_id')
					->join('shippings shipp', 'shipp.shipping_id=sp.shipping_id')
					->where('shipp.shipping_id', $params["shipping_id"])
					->group_by('oird.id_outbound_receiving_barang, oird.id_item_receiving_details, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, pr.qty, unt.name')
					->get()
					->result_array();


					foreach($results as $key => $result){
						$packing_number = $result['packing_number'] . sprintf('%02d', $result['inc']);
						$data[$packing_number][$key] = array( 'name' => $result['name'], 'unique_code' => $result['unique_code'], 'item_code' => $result['code'], 'qty' => $result['qty'], 'name_uom' => $result['uom_name'], 'weight' => $result['weight'], 'volume' => $result['volume']);
					}

					return $data;
	}
	
	public function getDetailPacking($params, $manifest=''){

		$requestData = $_REQUEST;

		$data = [];

		$results = $this->db
					->select('oird.id_outbound_receiving_barang, oird.id_item_receiving_details
								, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, SUM(pr.qty) as qty, unt.name as uom_name')
					->from('outbound_item_receiving_details oird')
					->join('item_receiving_details ird', 'ird.id=oird.id_item_receiving_details', 'LEFT')
					->join('picking_recomendation pr', 'pr.item_receiving_detail_id=ird.id', 'LEFT')
					->join('items itm', 'itm.id=ird.item_id', 'LEFT')
					->join('units unt','unt.id=itm.unit_id','left')
					->join('outbound outb', 'outb.id = oird.id_outbound', 'LEFT')
					->join('picking_list_outbound plo', 'plo.id_outbound=outb.id')
					->join('pickings p', 'p.pl_id=plo.pl_id')
					->join('shipping_picking sp','sp.pl_id=p.pl_id')
					->join('shippings shipp', 'shipp.shipping_id=sp.shipping_id')
					->where('shipp.shipping_id', $params["shipping_id"])
					->group_by('oird.id_outbound_receiving_barang, oird.id_item_receiving_details, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, pr.qty, unt.name')
					->get()
					->result_array();

					foreach($results as $key => $result){
						$packing_number = $result['packing_number'] . sprintf('%02d', $result['inc']);
						$data[$packing_number][$key] = array( 'name' => $result['name'], 'unique_code' => $result['unique_code'], 'item_code' => $result['code'], 'qty' => $result['qty'], 'name_uom' => $result['uom_name']);
					}

					if($manifest == 'item_packing'){
							$i = 0;
						foreach($data[$params["packing_id"]] as $key=>$item_packing_data){
							$nested = array();
							$nested[] = $i + 1 ;
							$nested[] = $item_packing_data['item_code'];
							$nested[] = $item_packing_data['name'];	
							$nested[] = $item_packing_data['unique_code'];
							$nested[] = $item_packing_data['qty'].' '.$item_packing_data['name_uom'];
							$nested[] = '';
							$nested[] = ''; 					
							$result_packing[] = $nested;
							$i++;
					}
						$results_new = array(
							"draw"            => intval( $requestData['draw'] ),
							"recordsTotal"    => intval( count($result_packing) ),
							"recordsFiltered" => intval( count($result_packing) ),
							"data"            => $result_packing
						);
					
							return $results_new;
					}

					if($manifest == 'manifest'){
						return $data;
					}


					$i = 0;
					foreach($data as $key=>$packing_data){
						$nested = array();
						$nested[] = $i + 1 ;
						$nested[] = $key;
						$nested[] = '';
						$nested[] = '-';	
						$nested[] = $params["shipping_id"]; 					
						$result_packing[] = $nested;
						$i++;
					}

					$results_new = array(
						"draw"            => intval( $requestData['draw'] ),
						"recordsTotal"    => intval( count($result_packing) ),
						"recordsFiltered" => intval( count($result_packing) ),
						"data"            => $result_packing
					);
					
					return $results_new;

	}
	
	public function getDoNumber($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					do_number 
				FROM 
					shipping_delivery_order 
				WHERE 
					shipping_id=\''.$post['shipping_id'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		if($row){
			$result = $row;
		}else{
			$result['do_number'] = '';
		}
		
		return $result;
	}
	
	public function statusOptions($default = '--Pilih Status--', $key = '') {

    	$this->db
			 ->select(["s.status_id as status_id","s.name as name"])
			 ->from("shippings ship")
			 ->join("status s","ship.status_id=s.status_id")
			 ->group_by("s.status_id");
        $data = $this->db->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->status_id] = $row->name ;
        }
        return $options;
    }
	
	public function getScannedList($post = array()){
		$result = array();
					
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'kd_unik',
			2 => 'tgl_exp_sort',
			3 => 'tgl_in_sort',
			4 => 'user_name'
		);
		
		$this->db->start_cache();

		$this->db
			->select(["DISTINCT(ird.unique_code) as kd_unik"])
			->from('shippings shipp')
			->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
			->join('picking_recomendation pr','pr.picking_id=sp.pl_id')
			->join('item_receiving_details ird', 'ird.id=pr.item_receiving_detail_id')
			->join('items itm','itm.id=ird.item_id')
			->join('units unt','unt.id=itm.unit_id','left')
			->join('users usr','usr.id=ird.user_id_loading','left')
			->where('shipp.shipping_id', $post['shipping_id'])
			->where('pr.item_id',$post['id_barang'])
			->where('pr.location_id=104',NULL)
			->group_by('ird.unique_code');

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();		

		$this->db
			->select([
				"tgl_exp as tgl_exp_sort",
				"tgl_in as tgl_in_sort",
				"COALESCE(TO_CHAR(tgl_exp, 'DD/MM/YYYY'), '-') as tgl_exp",
				"COALESCE(TO_CHAR(tgl_in, 'DD/MM/YYYY'), '') as tgl_in", "COALESCE(usr.user_name, '-') as user_name","SUM(pr.qty) as qty",
				"shipp.status_id"
			])
			->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir'])
			->limit($requestData['length'],  $requestData['start'])
			->group_by('shipp.shipping_id, ird.tgl_exp, ird.tgl_in, usr.user_name, shipp.status_id');

		$this->db->select('count(*) as total');

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = " - ";
            if ($this->access_right->otoritas('delete')) {

            	$disabledClass="";
            	if($row[$i]['status_id'] == 4){
    	        	$disabledClass = "disabled-link";
	            }

	            $action = '<button class="btn blue btn-xs '.$disabledClass.'" type="button" id="data-table-cancel" data-id="'.$row[$i]['kd_unik'].'">
        				Cancel
                </button>';

			}

			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['qty'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['user_name'];
			$nested[] = $action;
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}

    public function getLoadingPack($param=array()){

    	$data = array();

        $this->db
        	->select('shipp.shipping_id, shipp.code as shipping_code, outb.code as outbound_code, outb.destination_name, outb.address, outb.address_1, outb.address_2, outb.address_3, itm.id as item_id, itm.name as item_name, itm.code as item_code, shipp.driver as driver_name, shipp.license_plate as plate_number, wh.name as warehouse_name')
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_list_outbound plo','plo.pl_id=pl.pl_id')
        	->join('outbound outb','outb.id=plo.id_outbound')
        	->join("warehouses wh",'wh.id=outb.warehouse_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.picking_id=pr.picking_id and ird.unique_code=pr.unique_code and ird.item_id=pr.item_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $param['shipping_id'])
        	->group_by('shipp.shipping_id, shipp.code, outb.code, outb.destination_name, outb.address_1, outb.address_2, outb.address_3, itm.name, itm.id, itm.code, pl.pl_id, ird.parent_code, outb.address, wh.name');

        $summary = $this->db->get()->result_array();

        if($summary){

        	$data = array(
        		"shipping_id" 		=> $summary[0]['shipping_id'],
        		"shipping_code"		=> $summary[0]['shipping_code'],
        		"outbound_code"		=> $summary[0]['outbound_code'],
        		"destination_name"	=> $summary[0]['destination_name'],
        		"address_1"			=> $summary[0]['address_1'],
        		"address_2"			=> $summary[0]['address_2'],
        		"address_3"			=> $summary[0]['address_3'],
        		"driver_name"		=> $summary[0]['driver_name'],
        		"address"			=> $summary[0]['address'],
        		"plate_number"		=> $summary[0]['plate_number'],
        		'warehouse_name'	=> $summary[0]['warehouse_name']
        	);

	        $i=0;
	        foreach ($summary as $sum) {

	        	$parentI = $i;

	        	$data['items'][$i] = array(
	        		"item_name"		=> $sum['item_name'],
	        		"item_code"		=> $sum['item_code'],
	        		"parent_code"	=> "-",
	        		"qty_per_colly"	=> "-",
	        		"total_colly"	=> 0,
	        		"qty_pcs"		=> 0
				);
				

	        	$i++;

		        $this->db
					->select(["COALESCE(SUM(pr.qty),0) as qty","ird.parent_code","itm.name as item_name","itm.code as item_code","ird.parent_code",
						"(
							SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
								SELECT COALESCE((SUM(pr.qty)* CAST(itm.weight as float8)),0) as weight FROM shippings shipp  
								JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id 
								JOIN pickings pl ON pl.pl_id=sp.pl_id
								JOIN picking_qty pq ON pq.picking_id=pl.pl_id
								JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=pq.item_id
								LEFT JOIN items itm ON itm.id=pr.item_id
								WHERE itm.id =".$sum['item_id']." group by itm.weight
							) as tbl
						) as weight",
						"(
							SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
								SELECT COALESCE((SUM(pr.qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM shippings shipp 
								JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id
								JOIN pickings pl ON pl.pl_id=sp.pl_id
								JOIN picking_qty pq ON pq.picking_id=pl.pl_id
								JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=pq.item_id
								LEFT JOIN items itm ON itm.id=pr.item_id
								WHERE itm.id =".$sum['item_id']." group by itm.height, itm.width, itm.length
							) as tbl
						) as volume",
							])
		        	->from('shippings shipp')
		        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
		        	->join('pickings pl','pl.pl_id=sp.pl_id')
		        	->join('picking_qty pq','pq.picking_id=pl.pl_id')
		        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id and pr.item_id=pq.item_id')
		        	->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id')
		        	->join('items itm','itm.id=pr.item_id','left')
		        	->where('shipp.shipping_id', $param['shipping_id'])
		        	->where('itm.id',$sum['item_id'])
		        	->group_by('itm.name, itm.code, pl.pl_id, ird.parent_code');

				$res = $this->db->get()->result_array();

		        if($res){

			        foreach ($res as $det) {

			        	$data['items'][$parentI]['total_colly'] += 1;
						$data['items'][$parentI]['qty_pcs'] += $det['qty'];
						$data['items'][$parentI]['weight'] = $det['weight'].' '.WEIGHT_UOM;
						$data['items'][$parentI]['volume'] = $det['volume'].' '.VOLUME_UOM;

						
			        	// $data['items'][$i] = array(

			        	// 	"item_name"		=> $det['item_name'],
			        	// 	"item_code"		=> $det['item_code'],
			        	// 	"parent_code"	=> $det['parent_code'],
			        	// 	"qty_per_colly"	=> $det['qty'],
			        	// 	"total_colly"	=> "-",
			        	// 	"qty_pcs"		=> "-"
			        	// );

			        	$i++;
			        	
			        }

			    }
			}

		}

        return $data;
    }

    public function getLoadingTruck($param=array()){

        $this->db
        	->select(['shipp.license_plate',"string_agg(DISTINCT parent_code, ', ') as outerlabel","COALESCE(SUM(pr.qty),0) as qty"])
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $param['shipping_id'])
        	->group_by('shipp.license_plate');

        $result = $this->db->get();

        return $result->result_array();

    }

    public function getLoadingPackNew($param=array()){

        $this->db
        	->select('shipp.shipping_id, shipp.code as shipping_code, outb.code as outbound_code, outb.destination_name, outb.address_1, outb.address_2, outb.address_3, itm.name as item_name, itm.code as item_code')
        	->select(["COALESCE(SUM(pr.qty),0) as qty","COALESCE((SELECT string_agg(DISTINCT parent_code, ', ')  FROM item_receiving_details WHERE picking_id=pl.pl_id group by picking_id), '-') as outerlabel"])
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	// ->join('shipping_scanned ss','ss.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_list_outbound plo','plo.pl_id=pl.pl_id')
        	->join('outbound outb','outb.id=plo.id_outbound')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $param['shipping_id'])
        	->group_by('shipp.shipping_id, shipp.code, outb.code, outb.destination_name, outb.address_1, outb.address_2, outb.address_3, itm.name, itm.code, pl.pl_id');

        $result = $this->db->get();

        return $result->result_array();
    }

    public function getLoadingTruckNew($param=array()){

        $this->db
        	->select(['shipp.license_plate',"string_agg(DISTINCT parent_code, ', ') as outerlabel","COALESCE(SUM(pr.qty),0) as qty"])
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $param['shipping_id'])
        	->group_by('shipp.license_plate');

        $result = $this->db->get();

        return $result->result_array();

    }	

	public function closeLoading($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					st_shipping 
				FROM 
					shipping 
				WHERE
					shipping_id=\''.$post['shipping_id'].'\''; 
		
		$row = $this->db->query($sql)->row_array();
		if($row['st_shipping'] == 1){
			
			$result['status'] = 'ERR';
			$result['message'] = 'Loading already finish.';
			
		}else{
			$rectime = date('Y-m-d H:i:s');
		
			$sql = 'UPDATE 
						shipping 
					SET 
						st_shipping=\'1\'
						shipping_finish=\''.$rectime.'\'
					WHERE 
						shipping_id=\''.$post['shipping_id'].'\'';
						
			$q = $this->db->query($sql);
			
			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Finish loading success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Finish loading failed';
			}
		
		}
		
		return $result;
	}
	
	public function checkDoNumber($post = array()){
		$result = array();
		
		$sql = 'SELECT COUNT(*) AS total 
				FROM shipping_delivery_order 
				WHERE 
					do_number=\''.$post['do_number'].'\' 
				AND 
					shipping_id!=\''.$post['shipping_id'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		
		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Delivery Order number already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}
		
		return $result;
	}
	
	public function saveDo($post = array()){
		$result = array();
		
		$sql = 'SELECT COUNT(*) AS total 
				FROM shipping_delivery_order 
				WHERE shipping_id=\''.$post['shipping_id'].'\'';
				
		$row = $this->db->query($sql)->row_array();
		
		if($row['total'] > 0){
			$rectime = date('Y-m-d H:i:s');
			$sql = 'UPDATE 
						shipping_delivery_order 
					SET 
						do_number=\''.$post['do_number'].'\',
						update_by=\''.$this->session->userdata('user_id').'\',
						modified_date=\''.$rectime.'\' 
					WHERE 
						shipping_id=\''.$post['shipping_id'].'\'';
						
			$this->db->query($sql);
			
		}else{
			
			$rectime = date('Y-m-d H:i:s');
			$sql = 'INSERT INTO shipping_delivery_order(
						do_number,
						shipping_id,
						created_by,
						created_date,
						update_by,
						modified_date
					)VALUES(
						\''.$post['do_number'].'\',
						\''.$post['shipping_id'].'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\'
					)';
					
			$this->db->query($sql);
		}
		
		return $result;
	}
	
	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 =>  'pl_id_qty',
			1 => 'shipping_id',
			2 => 'id_barang',
			3 => '',
			4 => 'kd_barang',
			5 => 'nama_barang',
			6 => 'picked'
		);
		
		$where = '';

		$this->db->start_cache();

		$this->db
			->select(["DISTINCT(pq.item_id)"])
			->from('shippings s')
			->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
			->join('pickings p','p.pl_id=sp.pl_id')
			->join('picking_qty pq','pq.picking_id=p.pl_id')
			->join('items itm','itm.id=pq.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->where('s.shipping_id', $post['shipping_id'])
			->group_by('pq.item_id');

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->select([
				"pq.id as pl_id_qty","s.shipping_id","itm.id as id_barang","SUM(pq.qty) as picked","itm.code as kd_barang","CONCAT(itm.name, ' - ', itm.code) as nama_barang",
				"COALESCE(unt.name, '') as nama_satuan"
			])
			->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir'])
			->limit($requestData['length'],  $requestData['start'])
			->group_by('pq.id, s.shipping_id, itm.id, unt.name, itm.code, itm.name');

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $row[$i]['pl_id_qty'];
			$nested[] = $row[$i]['shipping_id'];
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['picked'] . ' ' . $row[$i]['nama_satuan'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function getDetailPicking($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => 'pl_id',
			1 => '',
			2 => 'pl_name',
			3 => 'pl_date_sort',
			4 => 'remark',
			5 => 'pl_start_sort',
			6 => 'pl_finish_sort',
			7 => 'nama_status'
		);
		
		$where = '';

		$this->db->start_cache();

		if(!empty($post['shipping_id'])){
			$this->db->where('shipp.shipping_id', $post['shipping_id']);
		}

		$this->db
			->from('shippings shipp')
			->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
			->join('pickings pl','pl.pl_id=sp.pl_id')
			->join('users usr','usr.id=shipp.user_id','left')
			->join('status st','st.status_id=pl.status_id','left')
			->where('shipp.shipping_id',$post['shipping_id'])
			->group_by('pl.pl_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select([
				"pl.pl_id","pl.name as pl_name",
				"pl.date as pl_date_sort", "start_time as pl_start_sort", "end_time as pl_finish_sort",
				"TO_CHAR(pl.date, 'DD/MM/YYYY') as pl_date", 
				"COALESCE(remark, ' - ') as remark",
				"COALESCE(TO_CHAR(start_time, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY</span>'), '-') as pl_start",
				"COALESCE(TO_CHAR(end_time, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY</span>'), '-') as pl_finish",
				"COALESCE(TO_CHAR(shipping_start, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY</span>'), '-') as shipping_start",
				"COALESCE(TO_CHAR(shipping_finish, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY</span>'), '-') as shipping_finish",
				"pl.status_id",
				"st.name as nama_status"
			])
			->group_by('shipp.shipping_start, shipping_finish, st.name, pl.name, pl.date, pl.remark, pl.start_time, pl.end_time, pl.status_id, st.name')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);
		
		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'picking_list/detail/'.$row[$i]['pl_id'].'">'.$row[$i]['pl_name'].'</a>';
			$nested[] = $row[$i]['pl_date'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['pl_start'];
			$nested[] = $row[$i]['pl_finish'];
			$nested[] = $row[$i]['nama_status'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function getDetail($post = array()){
		$result = array();

		$this->db
			->select(["shipp.shipping_id","shipp.code as shipping_code","outb.code as kd_outbound","outb.document_id as id_outbound_document","TO_CHAR(shipp.date, 'DD/MM/YYYY') as shipping_date",
				"d.destination_code as kd_supplier","d.destination_name as nama_supplier",
				"d.destination_address as alamat_supplier","d.destination_phone as telepon_supplier","d.destination_contact_person as cp_supplier",
				"CONCAT(d.destination_code, ' - ', d.destination_name) as customer_name",
				"COALESCE(TO_CHAR(shipping_start, 'DD/MM/YYYY HH24:MI:SS'), '-') as shipping_start","COALESCE(TO_CHAR(shipping_finish, 'DD/MM/YYYY HH24:MI:SS'), '-') as shipping_finish","user_name","st.name as nama_status","driver as driver_name","license_plate"])
			->from('shippings shipp')
			->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
			->join('pickings pl','pl.pl_id=sp.pl_id')
			->join('picking_list_outbound plo','plo.pl_id=pl.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('destinations d','d.destination_id=outb.destination_id','left')
			->join('users usr','usr.id=shipp.user_id','left')
			->join('status st','st.status_id=shipp.status_id','left')
			->where('shipp.shipping_id',$post['shipping_id'])
			->group_by('shipp.shipping_id, outb.code, outb.document_id, 
				
				d.destination_name, d.destination_code, d.destination_address,d.destination_phone, d.destination_contact_person,
				usr.user_name, st.name, shipp.code, shipp.date, shipp.shipping_start, shipp.shipping_finish, shipp.driver, shipp.license_plate');

		$result = $this->db->get();
		return $result;
	}
	
	public function getOutboundCompletePicking(){
		$result = array();
		
		$sql = 'SELECT 
					outb.id_outbound, kd_outbound 
				FROM 
					outbound outb 
				JOIN picking_list_outbound plo 
					ON plo.id_outbound=outb.id_outbound 
				WHERE 
					outb.id_status_outbound != \'2\'
				ORDER BY 
					id_outbound DESC';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getPickingDocument($post = array()){
		$result = array();
		
		$where = '';
		if(isset($post['shipping_id'])){
			$this->db->where('p.status_id', 9);
		}else{
			// $this->db->where('p.pl_id NOT IN ((SELECT pl_id FROM shipping_picking))', NULL);
			$this->db->where('p.status_id', 8);
		}
		
		if(isset($post['id_outbound'])){
			$this->db->where('outb.id', $post['id_outbound']);
		}
		

		$this->db
			->select(["p.pl_id", "p.name as pl_name","TO_CHAR(p.date, 'DD/MM/YYYY') as pl_date","COALESCE(CONCAT(spl.code, ' - ', spl.name), cust.name) as customer_name"])
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id','left')
			->join('suppliers spl','spl.id=outb.customer_id AND outb.document_id = 2','left outer')
			->join('customers cust','cust.id=outb.customer_id AND outb.document_id <> 2','left outer')
			->group_by('p.pl_id, spl.code, spl.name, cust.name,p.name, p.date');
					
		$result = $this->db->get()->result_array();
		
		return $result;
	}
	
	public function getItems($post = array()){
		$result = array();

		$this->db
			->select(["COALESCE(SUM(pr.qty),0) as qty",'itm.code as kd_barang',"CONCAT(itm.name, ' - ', itm.code) as nama_barang","COALESCE(unt.name, '') as nama_satuan"])
			->from('shippings s')
			->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
			->join('pickings p','p.pl_id=sp.pl_id')
			->join('picking_qty pq','pq.picking_id=p.pl_id')
			->join('picking_recomendation pr','pr.picking_id=p.pl_id and pr.item_id=pq.item_id')
			->join('items itm','itm.id=pq.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->where('s.shipping_id',$post['shipping_id'])
			->group_by('itm.code, itm.name, unt.name')
			;

		$row = $this->db->get()->result_array();
		$result['data'] = $row;
		
		return $result;
	}
	
	public function delete($post = array()){
		$result = array();

		$this->db->select('count(*) as total')
			->from('shippings')
			->where('shipping_id', $post['shipping_id'])
			->where('shipping_start IS NOT NULL', NULL);

		$r = $this->db->get()->row_array();
		if($r['total'] > 0){
			
			$result['status'] = 'ERR';
			$result['message'] = 'Delete data failed, because loading process already started';
			
		}else{

			$this->db->trans_start();

			$this->db
				->set('status_id',8)
				->where('pl_id IN (SELECT pl_id FROM shipping_picking WHERE shipping_id='.$post['shipping_id'].')', NULL)
				->update('pickings');

			$this->db->delete('shipping_picking',['shipping_id'=>$post['shipping_id']]);
			// $this->db->delete('shippings',['shipping_id'=>$post['shipping_id']]);
			
			$this->db
				->set('status_id',17)
				->where('shipping_id',$post['shipping_id'])
				->update('shippings');

			$this->db->trans_complete();

			if($this->db->trans_status()){
				$result['status'] = 'OK';
				$result['message'] = 'Delete success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Delete failed';
			}
		}
		
		return $result;
	}
	
	public function getOutboundDocument($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					pl.pl_id, pl_name, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS  pl_date, 
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					IFNULL(alamat_supplier, address) AS address 
				FROM 
					shipping shipp 
				JOIN shipping_picking shipppick 
					ON shipppick.shipping_id=shipppick.shipping_id 
				JOIN picking_list pl
					ON pl.pl_id=shipppick.pl_id 
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id 
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound
				LEFT JOIN m_status_outbound mso 
					ON mso.id_status_outbound=outb.id_status_outbound 
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\' 
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\'';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getEdit($post = array()){
		$result = array();
		
		$this->db
			->select([
				"TO_CHAR(s.date, 'DD/MM/YYYY') as shipping_date",
				"d.destination_name as customer_name",
				"d.destination_address as address",
				"s.driver as driver_name","license_plate","sp.pl_id", "s.shipping_id", "s.code as shipping_code", "plo.id_outbound", "outb.code as kd_outbound","s.barcode"
			])
			->from('shippings s')
			->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
			->join('pickings p','p.pl_id=sp.pl_id')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('destinations d','d.destination_id=outb.destination_id','left')
			->where('s.shipping_id', $post['shipping_id']);
					
		$result = $this->db->get()->row_array();
		
		return $result;
	}
	
	public function updateOutbound1($idOutbound, $data = array(), $id){
		
		$sql = 'DELETE FROM shipping_picking WHERE shipping_id=\''.$id.'\'';
		$this->db->query($sql);
		
		$len = count($data);
		for($i = 0; $i < $len; $i++){
			
			$sql = 'INSERT INTO shipping_picking(
						shipping_id,
						id_outbound,
						pl_id
					)VALUES(
						\''.$id.'\',
						\''.$idOutbound.'\',
						\''.$data[$i].'\'
					)';
						
			$this->db->query($sql);
			
			//update_shipping_id_in_receiving_barang ?
		}
	}
	
	public function updateShippingPicking($idOutbound, $idPicking, $id){
		
		$this->db->delete('shipping_picking', ['shipping_id'=>$id]);

		$this->db
			->set('shipping_id', $id)
			->set('pl_id', $idPicking)
			->insert('shipping_picking');

	}
	
	public function updateShippingIdInReceivingBarang($shippingId){
		$result = array();
		
		$this->db
			->select('item_id as id_barang, p.pl_id')
			->from('shippings s')
			->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
			->join('pickings p','p.pl_id=sp.pl_id')
			->join('picking_qty pq','pq.picking_id=p.pl_id')
			->where('s.shipping_id', $shippingId);
					
		$row = $this->db->get()->result_array();

		$len = count($row);
		
		for($i = 0; $i < $len; $i++){
								
			$this->db
				->set('shipping_id', $shippingId)
				->where('item_id', $row[$i]['id_barang'])
				->where('picking_id', $row[$i]['pl_id'])
				->update('item_receiving_details');

		}
		
		return $result;
	}
	
	public function updatePicking($id){
		$sql = 'UPDATE pickings SET status_id = 9 WHERE pl_id = \''.$id.'\'';
		$this->db->query($sql);
	}	
	
	public function setShipmentDoc($pickingId, $shippingId){
		
		$sql = 'SELECT 
					shipment_doc  
				FROM 
					picking_list_outbound plo 
				JOIN shipping_doc sd 
					ON sd.outbound_id=plo.id_outbound
				WHERE 
					pl_id=\''.$pickingId.'\'';

					
		$r = $this->db->get()->row_array();
		if($r){
			
			$rectime = date('Y-m-d H:i:s');
			
			$sql = 'INSERT INTO shipping_delivery_order(
						shipping_id, 
						do_number,
						created_by,
						create_at,
						update_by,
						updated_at
					)VALUES(
						\''.$shippingId.'\',
						\''.$r['shipment_doc'].'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\'
					)';
			
			$this->db->query($sql);
			
		}
	}


	private function data_insert($data){

		$data = array(
			"code" 			=> $data['shipping_code'],
			"date" => $data['shipping_date'],
			"driver"	=> $data['driver_name'],
			"license_plate" => $data['license_plate'],
			'barcode'	=> $data['barcode'],
			"user_id"		=> $data['user_id']
		);

		return $data;
	}	

	public function create($data, $idOutbound, $picking) {	
		
		
		$data= $this->data_insert($data);
		$data['status_id'] = 3;
		$this->db->trans_start();
        $this->db->insert('shippings', $data);
		$this->db->trans_complete();
		
		$this->db->order_by('shipping_id','desc');
		$this->db->limit(1);
		$shippingId = $this->db->get('shippings')->row_array()['shipping_id'];
		// $shippingId = $this->db->insert_id();
		
		$this->updatePicking($picking);
		$this->updateShippingPicking($idOutbound, $picking, $shippingId);
		$this->updateShippingIdInReceivingBarang($shippingId);
		
		//INTEGRATION
		// $this->setShipmentDoc($picking, $shippingId);
		
		return true;
    }

    public function update($data, $idOutbound, $picking, $id) {
		$data= $this->data_insert($data);
        $this->db->update('shippings', $data, array('shipping_id' => $id));
		
		$this->updateShippingPicking($idOutbound, $picking, $id);
		
		//INTEGRATION
		// $this->setShipmentDoc($picking, $id);
    }
	
	public function add_shipping_code(){
        $this->db->set('inc_shipping',"inc_shipping+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }
	
	public function getOutboundNotInShipping(){
		$result = array();
		
		$sql = 'SELECT 
					outb.id_outbound, kd_outbound, DATE_FORMAT(tanggal_outbound, \'%d\/%m\/%Y\') AS  tanggal_outbound, 
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name					
				FROM 
					outbound outb 
				JOIN picking_list pick 
					ON pick.id_outbound=pick.id_outbound 
				LEFT JOIN outbound_barang outbrg 
					ON outbrg.id_outbound=outb.id_outbound 
				LEFT JOIN receiving_barang rcvbrg 
					ON rcvbrg.id_barang=outbrg.id_barang
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\' 
				WHERE 
					rcvbrg.shipping_id IS NULL
				GROUP BY 
					outb.id_outbound';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getLoadingCodeIncrement(){
		
		$result = '';
		
		$sql = 'SELECT 
					CONCAT(pre_code_shipping, \'\', inc_shipping) AS loading_code 
				FROM 	
					m_increment 
				WHERE 
					id_inc = \'1\'';
					
		$row = $this->db->query($sql)->row_array();
		$result = $row['loading_code'];
		
		return $result;

	}
	
	public function getLoadingCode($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					shipping_id, code as shipping_code
				FROM 
					shippings 
				WHERE 
					code LIKE \'%'.$post['query'].'%\'
					AND status_id != 17';
					
		$row = $this->db->query($sql)->result_array();
		
		$result['status'] = 'OK';
		$result['result'] = $row;
		
		return $result;
	}

	public function getOuterLabelbyShipping($shippingId){

        $this->db
        	->select("ird.parent_code")
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $shippingId)
        	->group_by('ird.parent_code');

        $result = $this->db->get();

        return $result->result_array();

	}

	public function getSNByOuterLabel($shippingId,$outerLabel){

        $this->db
        	->select("ird.rfid_tag as serial_number, itm.code")
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $shippingId)
        	->where('ird.parent_code', $outerLabel)
        	->group_by('ird.rfid_tag, itm.code');

        $result = $this->db->get();

        return $result->result_array();

	}

    public function cancel($params=array()){

    	if(!empty($params)){

	    	$this->db
	    		->select('s.*, sp.*, sp.pl_id as picking_id')
	    		->from('shippings s')
	    		->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
	    		->where('s.shipping_id',$params['shipping_id']);

	    	$data = $this->db->get()->row_array();

	    	if($data){

		    	$this->db->trans_start();
	
		    	$this->db
		    		->set('location_id',"(CASE WHEN location_id <> 104 THEN location_id ELSE 101 END)",FALSE)
		    		->where('id IN (SELECT item_receiving_detail_id FROM picking_recomendation where picking_id='.$data['picking_id'].')',NULL)
		    		->update('item_receiving_details');

		       	$this->db
		       		->set('location_id',101)
		       		->where('picking_id',$data['picking_id'])
		       		->update('picking_recomendation');

		        $this->db->set("status_id", 8);
		        $this->db->where("pl_id", $data['picking_id']);
		        $this->db->update("pickings");

		        $this->db
		        	->set("status_id", 17)
		        	->set("st_shipping", 1)
		        	->set('shipping_finish',date('Y-m-d h:i:s'))
		        	->where("shipping_id", $data['shipping_id'])
		        	->update("shippings");

		    	$this->db->trans_complete();

		    	return true;
		    }

    	}

    	return false;

    }

    function cancel_item($params=array()){

    	if(!empty($params)){

	    	$this->db->trans_start();

	    	$this->db
	    		->select(["pr.picking_id","item_receiving_detail_id","unique_code","old_location_id","COALESCE(sum(qty),0) as qty"])
	    		->from('shipping_picking sp')
	    		->join('picking_recomendation pr','pr.picking_id=sp.pl_id')
	    		->where('sp.shipping_id',$params['shipping_id'])
	    		->where('pr.unique_code',$params['serial_number'])
	    		->group_by('picking_id, unique_code, old_location_id, item_receiving_detail_id')
	    		;

	    	$data = $this->db->get()->row_array();

	    	if($data){

		    	$this->db
		    		->set('shipping_id',NULL)
		    		->set('user_id_shipping',NULL)
		    		->set('location_id',"(CASE WHEN location_id <> 104 THEN location_id ELSE 101 END)",FALSE)
		    		->where('id',$data['item_receiving_detail_id'])
		    		->update('item_receiving_details');

		    	$this->db
		    		->set('location_id',101)
		    		->where('picking_id',$data['picking_id'])
		    		->where('unique_code',$params['serial_number'])
		    		->update('picking_recomendation');

		    	$this->db
		    		->set('st_shipping',0)
		    		->set('shipping_finish',NULL)
		    		->set('status_id',5)
		    		->where('shipping_id',$params['shipping_id'])
		    		->update('shippings');

		    	$this->db->trans_complete();

		    	return true;

		    }
    
    	}

    	return false;

    }

}

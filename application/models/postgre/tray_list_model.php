<?php
class tray_list_model extends CI_Model {

	CONST MONTHLY = 'MONTHLY';
	CONST YEARLY = 'YEARLY';

	CONST FIFO = 'FIFO';
	CONST FEFO = 'FEFO';
	CONST FIFOFEFO = 'FIFOFEFO';
	CONST FEFOFIFO = 'FEFOFIFO';

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'pickings';
    private $table2 = 'pl_do';
    private $table4 = 'do_barang';
    private $table5 = 'items';
    private $table6 = 'units';
    private $table7 = 'categories';
    private $do = 'do';

    public function getTrayLocation($post = array()){
        $result = array();

		$this->db
			->select('id as pl_id, name as pl_name')
			->from('locations')
            ->where('location_type_id',6)
            ->where('status','ACTIVE')
			->like('name', $post['query']);

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
    }

	public function export_dn($params=array()){


		$where = '';

		if($params['destination']!== 'null' && $params['destination']!== ''){
			$where .= ' AND d.destination_id='.$params['destination'];
		}

		if($params['pl_id']!== 'null'){
			$where .= ' AND p.pl_id='.$params['pl_id'];
		}

		if($params['to'] !== 'null' && $params['from'] !== 'null'){
			$where .= ' AND (dn.created_at between\''.$params['from'].'\' and \''.$params['to'].'\')';
		}

		$sql = "select dn.name as dn_name, dn.created_at, p.name, sum(pr.qty) as qty, d.destination_name, mp.m_priority_name, o.code from delivery_pl dp
		left join delivery_notes dn on dn.id = dp.dn_id 
		left join picking_recomendation pr on pr.id = dp.pl_id
		left join pickings p on p.pl_id = pr.picking_id 
		left join outbound o on o.id = pr.outbound_id 
		left join m_priority mp on o.m_priority_id = mp.m_priority_id 
		left join destinations d on d.destination_id = o.destination_id where 1=1".$where."
		group by dn.name,dn.created_at, p.name, mp.m_priority_name, o.code, d.destination_name";

		$result = $this->db->query($sql)->result_array();
		return $result;
	}

    public function create($post=[]){
        $this->db
		->select(['pr.id as pr_id','pr.unique_code as u_code', 'p.pl_id as pl_id','p.name as picking', 'i.name as item_name', 'pr.qty as qty','l.name as location_name'])
		->from('picking_recomendation pr')
		->join('items i','i.id = pr.item_id','left')
		->join('locations l','l.id = pr.location_id','left')
		->join('location_types lt','lt.id = l.location_type_id','left')
		->join('pickings p','p.pl_id = pr.picking_id','left');

        if($post['location_id']!=='null'){
			$this->db->where('l.id',$post['location_id']);
		}

        $rows = $this->db->get()->result_array();
		// dd($rows);
		$temp = array();
		for($i = 0; $i < count($rows); $i++){
			if(!in_array($rows[$i]['picking'], $temp)){
				array_push($temp, $rows[$i]['picking']);
			}
		}

		

		$this->db->trans_start();
		for($i = 0; $i < count($temp) ; $i++){
			// INsert table delivery_note
			$params = array(
				'name' => $this->get_dn_code(),
				'user_id' => $this->session->userdata('user_id'),
				'created_at' => date("Y/m/d"),
				'location_id' => $post['location_id']
			);

			$insert = $this->db->insert('delivery_notes',$params);
			//add DN COde
			$this->add_dn_code();
			for($j = 0; $j < count($rows); $j++){
				if($rows[$j]['picking']==$temp[$i]){
					$id = $this->db->select(['id'])->from('delivery_notes')->where('name',$params['name'])->get()->result_array();

					//Insert batch table delivery_pl
					$batch = array();
					$batch[] = array(
						'pl_id' => $rows[$j]['pr_id'],
						'dn_id' => $id[0]['id']
					);
					$insert_batch = $this->db->insert_batch('delivery_pl',$batch);
				}
			}
		}
		
		
		//INACTIVE LOCATION
		$this->db->set('status','INACTIVE');
		$this->db->where('id',$post['location_id']);
		$up = $this->db->update('locations');

        $complete = $this->db->trans_complete();

		if($complete)
		{
			$data = [
				'status' => '200'
			];
			return $data;
		}else{
			$data = [
				'status' => '400'
			];
			return $data;
		}
    }

    public function getDNList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'date',
			1 => 'dn_id',
			2 => 'name',
			3 => 'date',
			4 => 'location',
			5 => 'user'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['pl_id'])){
			$this->db->where('p.pl_id',$post['pl_id']);
		}

		if(!empty($post['id_outbound'])){
			$this->db->where('outb.id', $post['id_outbound']);
		}

		if(!empty($post['id_customer'])){
			$this->db->where('outb.destination_id', $post['id_customer']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('dn.created_at >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). ' 00:00:00');
			$this->db->where('dn.created_at <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). ' 23:59:59');
		}

		if($post['status'] !== ''){
			if($post['status']==0){
				$this->db->where('dn.status is null',null);
			}else{
				$this->db->where('dn.status','COMPLETE');
			}
		}
		
		if(!empty($post['dn'])){
			$this->db->where('dn.id', $post['dn']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id', $post['warehouse']);
		}

		// $this->db
		// 	->select('DISTINCT(p.pl_id) as pl_id')
		// 	->from('pickings p')
		// 	->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
		// 	->join('outbound outb','outb.id=plo.id_outbound')
		// 	->join('warehouses wh','wh.id=outb.warehouse_id')
		// 	->join('users_warehouses uw','uw.warehouse_id=wh.id')
		// 	->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
		// 	->join('status st','st.status_id=p.status_id','left')
		// 	->join('m_priority prior','prior.m_priority_id=outb.m_priority_id','left')
		// 	->group_by('p.pl_id');

        $this->db
            ->select(['dn.name as name', 'dn.created_at as date', 'loc.name as location', 'u.nama as user','dn.status as status'])
            ->from('delivery_notes dn')
            ->join('locations loc', 'loc.id=dn.location_id','left')
			->join('delivery_pl dp','dp.dn_id = dn.id','left')
			->join('picking_recomendation pr','dp.pl_id = pr.id','left')
			->join('pickings p','p.pl_id = pr.picking_id','left')
            ->join('users u','u.id = dn.user_id','left');

		$this->db->stop_cache();

		$row = $this->db->get();
		$totalData = $row->num_rows();

		// $this->db
		// 	->select([
		// 		"COALESCE(COUNT(outb.id),0) as total",
		// 		'p.name as pl_name',
		// 		'COALESCE(p.printed,0) as printed',
		// 		"string_agg(DISTINCT CAST(outb.id as VARCHAR), ',') as id_outbound",
		// 		"string_agg(DISTINCT outb.code, ',') as kd_outbound",
		// 		"p.date as sort_date","TO_CHAR(p.date, 'DD/MM/YYYY') as pl_date",
		// 		"string_agg(DISTINCT outb.destination_name, ',') as destination_name",
		// 		"COALESCE(remark, '-') as remark",
		// 		"p.start_time as pl_start_sort","p.end_time as pl_finish_sort",
		// 		"COALESCE(TO_CHAR(p.start_time, 'HH24:MI<br><span class=\"small-date \">DD/MM/YYYY</span>'), '-') AS pl_start",
		// 		"COALESCE(TO_CHAR(p.end_time, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY<span>'), '-') AS pl_finish",
		// 		"p.status_id as id_status_picking",
		// 		"
		// 		(CASE
		// 			WHEN p.status_id = 6 THEN 1
		// 			WHEN p.status_id = 7 THEN 2
		// 			WHEN p.status_id = 8 THEN 3
		// 			WHEN p.status_id = 9 THEN 4
		// 			WHEN p.status_id = 10 THEN 5
		// 			ELSE 6
		// 		END) AS sequence
		// 		",
		// 		"st.name as nama_status",
		// 		//"(CASE WHEN p.priority=1 THEN 'Normal' ELSE 'High' END) as priority"
		// 		"prior.m_priority_name as priority"

		// 	])
		// 	->group_by('st.name, p.name, p.printed,
		// 		p.date, p.remark, p.start_time, p.end_time, p.status_id, prior.m_priority_name')
		// 	->order_by('sequence','asc')
		// 	->order_by('priority','asc')
		// 	->order_by('p.date','desc')
        $this->db
            ->select(['dn.name as dn_name', 'dn.created_at as date', 'p.name as picking_name', 'loc.name as location', 'u.nama as user','dn.id as dn_id',"COALESCE(dn.status,'-') as status"])
            ->from('delivery_notes dn')
            ->join('locations loc', 'loc.id=dn.location_id','left')
			->join('delivery_pl dp','dp.dn_id = dn.id','left')
			->join('picking_recomendation pr','dp.pl_id = pr.id','left')
			->join('pickings p','p.pl_id = pr.picking_id','left')
            ->join('users u','u.id = dn.user_id','left')
			->group_by('dn.name, dn.created_at, p.name, loc.name, u.nama, dn.id, dn.status')
			->order_by('date desc, dn_name desc')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';
			// if($row[$i]['pl_start'] != '-'){
			// 	$class = 'disabled-link';
			// }

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			// if ($this->access_right->otoritas('print')) {
			// 	// $action .= '<li>';
			// 	// $action .= '<a class="data-table-print '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-print"></i> Print Picking List</a>';
			// 	// $action .= '</li>';

			// 	// if($class == 'disabled-link'){
			// 	// 	$class = '';
			// 	// }else{
			// 	// 	$class = 'disabled-link';
			// 	// }

            // }
            $action .= '<li>';
            $action .= '<a class="data-table-tray '.$class.'" data-id="'.$row[$i]['dn_id'].'"><i class="fa fa-print"></i> Print Tray List</a>';
            $action .= '</li>';
			
			if($row[$i]['status'] !== 'COMPLETE'){
				$action .= '<li>';
				$action .= '<a class="data-table-complete '.$class.'" data-id="'.$row[$i]['dn_id'].'"><i class="fa fa-trash"></i> delete</a>';
				$action .= '</li>';
			}

			$class = '';
			/*if($row[$i]['pl_start'] != '-'){
				$class = 'disabled-link';
			}*/

			// $printed = ($row[$i]['printed'] == 1) ? ' (printed)' : '';

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['dn_id'];
			$nested[] = '<input type="checkbox" name="id[]" value="'.$row[$i]['dn_id'].'">';
			$nested[] = $_REQUEST['start'] + ($i + 1);
			// $nested[] = '<a href="'.base_url().'picking_list/detail/'.$row[$i]['pl_id'].'">'.$row[$i]['pl_name'].$printed.'</a>';
			// $nested[] = $kd_outbound;
			$nested[] = $row[$i]['dn_name'];
			$nested[] = $row[$i]['picking_name'];
			// $nested[] = $destination;
			$nested[] = $row[$i]['location'];
			$nested[] = $row[$i]['date'];
			$nested[] = $row[$i]['user'];
			$nested[] = $row[$i]['status'];
            $nested[] = $action;

			// if($post['params'] != 'no_action')

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    public function printhead($post = []){
        $this->db
            ->select(['dn.name as name', 'dn.created_at as date', 'loc.name as location', 'u.nama as user'])
            ->from('delivery_notes dn')
            ->join('locations loc', 'loc.id=dn.location_id','left')
            ->join('users u','u.id = dn.user_id','left')
            ->where('dn.id',$post['dn_id']);
        
        $row = $this->db->get()->result_array();
        return $row;
    }

    public function printitem($post= []){
        $this->db
            ->select(['i.code as item_code','o.note','i.name as item_name','pr.qty as qty','p.name as pl_name',"concat(d.destination_name,' <b>(',d.destination_contact_person,')</b>') as destination_name",'d.destination_address','mp.m_priority_name'])
            ->from('delivery_pl dp')
            ->join('picking_recomendation pr','pr.id = dp.pl_id','left')
			->join('outbound o','o.id = pr.outbound_id','left')
			->join('m_priority mp','mp.m_priority_id = o.m_priority_id','left')
			->join('destinations d','d.destination_id = o.destination_id','left')
            ->join('items i','i.id = pr.item_id','left')
            ->join('pickings p','p.pl_id = pr.picking_id','left')
            ->where('dp.dn_id',$post['dn_id']);
        
        $row = $this->db->get()->result_array();
        return $row;

    }
	
	public function complete($post=[]){
		$this->db->trans_start();
		$this->db->set('status',"'COMPLETE'",FALSE);
        $this->db->where('id',$post['dn_id']);
        $this->db->update('delivery_notes');
		
		$id = $this->db->select(['location_id'])->from('delivery_notes')->where('id',$post['dn_id'])->get()->result_array();
		
		// $this->db->set('status',"'ACTIVE'",FALSE);
        // $this->db->where('id',$id[0]['location_id']);
        
		// $up = $this->db->update('locations');
		$up = $this->db->trans_complete();
		

		if($up){
			$data = [
			'status' => '200'
		];
			return $data;
		}else{
			$data = [
			'status' => '400'
		];
			return $data;
		}
	}
	
	public function delete($params=[]){
		$this->db->trans_start();
		$dp = "delete from delivery_pl where dn_id =".$params['dn_id'];
		$dn = "delete from delivery_notes where id =".$params['dn_id'];
		$this->db->query($dp);
		$this->db->query($dn);
		$up = $this->db->trans_complete();
		
		if($up){
			$data = [
			'status' => '200'
		];
			return $data;
		}else{
			$data = [
			'status' => '400'
		];
			return $data;
		}
	}

	public function complete_tray_name($post=[]){
		$this->db->trans_start();
		$this->db->set('status',"'COMPLETE'",FALSE);
        $this->db->where('name',$post['dn_name']);
        $this->db->update('delivery_notes');
		
		$id = $this->db->select(['location_id'])->from('delivery_notes')->where('name',$post['dn_name'])->get()->result_array();
		// dd($id);
		// $this->db->set('status',"'ACTIVE'",FALSE);
        // $this->db->where('id',$id[0]['location_id']);
		// $up = $this->db->update('locations');
		$up = $this->db->trans_complete();
		

		if($up){
			$data = [
			'status' => '200'
		];
			return $data;
		}else{
			$data = [
			'status' => '400'
		];
			return $data;
		}
	}

	public function free_tray($post=[]){
		$this->db->trans_start();

		$isLocation = $this->db->select('*')
								->from('locations')
								->where('name',$post['tray_name'])
								->get()
								->result_array();
		
		if(count($isLocation)>0){
			$isEmpty = $this->db->select('*')
			->from('picking_recomendation pr')
			->join('locations loc','loc.id = pr.location_id','left')
			->where('loc.name',$post['tray_name'])
			->get()
			->result_array();

			if(count($isEmpty)==0){
				$this->db->set('status',"'ACTIVE'",FALSE);
				$this->db->where('name',$post['tray_name']);
				$this->db->where('location_type_id',6);
				$up = $this->db->update('locations');
			}else{
				$up = false;
			}

			$this->db->trans_complete();

			if($up){
				$data = [
				'status' => '200'
			];
				return $data;
			}else{
				$data = [
				'status' => '400'
			];
				return $data;
			}
		}else{
			$data = [
				'status' => '400'
			];
				return $data;
		}

	}

    public function get_dn_code(){
        $this->db->select('pre_code_dn,inc_dn');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_dn.$result->inc_dn;
    }

    public function add_dn_code(){
        $this->db->set('inc_dn',"inc_dn+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

	public function get_document(){
		$result = $this->db->select(['name as dn_name'])
				->from('delivery_notes dn')
				->where('dn.status is null',null)
				->get()
				->result_array();
		return $result;				
	}

	public function get_document_item($post = []){
		if($post['name']!=''){
			$this->db->where('dn.name',$post['name']);
		}
		return $this->db->select(['itm.name as item_name', 'itm.code as item_code','pr.unique_code as code','pr.qty as qty', 'p.name as pl_name', 'usr.id as user_id', 'usr.user_name as user_name'])
				->from('delivery_pl dp')
				->join('delivery_notes as dn','dn.id = dp.dn_id','left')
				->join('picking_recomendation as pr','pr.id = dp.pl_id','left')
				->join('pickings as p','p.pl_id = pr.picking_id','left')
				->join('items itm','itm.id = pr.item_id','left')
				->join('users usr','usr.id = pr.user_id','left')
				->get()
				->result_array();
	}
}
<?php
class inbound_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'inbound';
    private $table2 = "suppliers";
    private $table3 = "status";
    private $table4 = "documents";
    private $table5 = "inbound_item";
	private $table6 = "users";
	private $table7 = "inbound_outbound";

    public function get_new_inbound(){
        $result = $this->fina->get_where('f_t_inbound_h',array('SyncStatus'=>'N'));
        return $result;
    }

    public function get_supplier($kode_supplier=''){
        $result = $this->db->get_where('supplier',array('kd_supplier'=>$kode_supplier));
        return $result;
    }

    public function get_inbound_document($inbound_document_name=''){
        $result = $this->db->get_where('m_inbound_document',array('inbound_document_name'=>$inbound_document_name));
        return $result;
    }

    public function create_inbound($data=array()){
        $result = $this->db->insert('inbound',$data);
        return $this->db->insert_id();
    }

    public function update_inbound($data=array(),$id=0){
        $result = $this->db->update('inbound',$data,array('id_inbound'=>$id));
        return $result;
    }

    public function get_inbound_item($DocEntry=0){
        $result = $this->fina->get_where('f_t_inbound_l1',array('DocEntry'=>$DocEntry));
        return $result;
    }

    public function get_barang($kd_barang=''){
        $result = $this->db->get_where('barang',array('kd_barang'=>$kd_barang));
        return $result;
    }

    public function create_inbound_barang($data=array()){
        $result = $this->db->insert('inbound_barang',$data);
        return $this->db->insert_id();
    }

    public function update_inbound_barang($data=array(),$id_inbound_barang=0){
        $result = $this->db->update('inbound_barang',$data,array('id_inbound_barang'=>$id_inbound_barang));
        return $result;
    }

    public function check_inbound($kd_inbound=''){
        $result = $this->db->get_where('inbound',array('code'=>$kd_inbound));
        return $result;
    }

    public function check_inbound_barang($where=array()){
        $result = $this->db->get_where('inbound_barang',$where);
        return $result;
    }

    public function set_inbound_status($DocEntry=0,$SyncDate=''){
        $result = $this->fina->update('f_t_inbound_h',array('SyncStatus'=>'Y','SyncDate'=>$SyncDate,'InbStatus'=>'INPROCESS'),array('DocEntry'=>$DocEntry));
        return $result;
    }

    public function data($condition = array()) {
		$this->db->select(
			'a.id_inbound, a.code as kd_inbound, a.date as tanggal_inbound, a.supplier_id as id_supplier, b.code as kd_supplier, b.name as nama_supplier,
			a.document_id as id_inbound_document, d.name as inbound_document_name, e.user_name, e.nama, f.id_outbound,
			a.status_id as id_status_inbound, c.code as kd_status, c.name as nama_status, a.warehouse_id, a.remark'
		);

        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', "a.supplier_id=b.id", "left");
        $this->db->join($this->table3 . ' c', "a.status_id=c.status_id", "left");
        $this->db->join($this->table4 . ' d', "a.document_id=d.document_id", "left");
		$this->db->join($this->table6 . ' e', "a.user_id=e.id", "left");
		$this->db->join($this->table7 . ' f', "a.id_inbound=f.id_inbound", "left");
        $this->db->where_condition($condition);

        return $this->db;
    }

	/*---- START ADJ -----*/

	public function getDetailOutbound($post = array()){
		$result = array();

		$sql = 'SELECT
					id_customer
				FROM
					outbound
				WHERE
					id_outbound=\''.$post['id_outbound'].'\'';

		$r = $this->db->query($sql)->row_array();
		if($r){
			$sql = 'SELECT
						id_barang, jumlah_barang
					FROM
						outbound_barang
					WHERE
						id_outbound=\''.$post['id_outbound'].'\'
					ORDER BY
						id ASC';

			$brg = $this->db->query($sql)->result_array();

			$result['status'] = 'OK';
			$result['id_customer'] = $r['id_customer'];
			$result['brg'] = $brg;
		}else{
			$result['status'] = 'OK';
			$result['id_customer'] = 0;
			$result['brg'] = array();
		}

		return $result;
	}

	public function getUomByItem($post = array()){
		$result = array();

		$this->db
			->select(["COALESCE(unt.name, '') as nama_satuan"])
			->from('units unt')
			->join('items itm','itm.unit_id=unt.id')
			->where('itm.id',$post['id_barang']);

		$result = $this->db->get()->row_array();

		return $result;
	}

	public function checkInboundDocNumberExist($post = array()){

		$result = array();

		$this->db
			->select('count(*) as total')
			->from('inbound')
			->where('code',$post['po']);

		if(!empty($post['id_inbound'])){
			$this->db->where('id_inbound <>',$post['id_inbound']);
		}

		$row = $this->db->get()->row_array();

		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Doc. number already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}

		return $result;
	}

	public function closePo($post = array()){
		$result = array();

		$this->db
			->set('status_id', 4)
			->where('id_inbound', $post['id_inbound']);

		$q = $this->db->update('inbound');

		$this->db
			->set('st_receiving',1)
			->where('po_id',$post['id_inbound'])
			->update('receivings');

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Close Inbound Document Success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Close Inbound Document Failed';
		}

		return $result;
	}

	public function getSupplier($post = array()){
		$result = array();

		$sql = "SELECT id as id_supplier, name as nama_supplier, code as kd_supplier FROM suppliers ORDER BY (CASE WHEN split_part(code, '-',1)~E'^\\\d+$' THEN CAST (split_part(code, '-',1) AS BIGINT) ELSE 0 END) ASC";
		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

	public function getCustomer($post = array()){
		$result = array();

		$this->db->select('id as id_customer, name as customer_name, code as customer_code');
		$this->db->from('customers');
		$this->db->order_by('id','ASC');
		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

	public function getSource($post = array()){
		$result = array();

		$this->db
			->from('documents d')
			->join('source_categories sc','sc.source_category_id=d.source_category_id')
			->where('document_id',$post['document_id']);

		$data = $this->db->get()->row_array();

		$row=array();

		if($data){

			switch ($data['source_category_id']) {
				case 3:
					// Get supplier
					$this->db
						->select(['source_id as id',"CONCAT(source_code, ' - ', source_name) as text"])
						->where('UPPER(status)','ACTIVE')
						->from('sources');
					
					break;

				case 4:
					// Get customer
					$this->db
						->select(['destination_id as id',"CONCAT(destination_code, ' - ', destination_name, ' ( ', destination_city ,' )') as text"])
						->where('UPPER(status)','ACTIVE')
						->from('destinations');
	
					if(isset($post['destination_id'])) {
						$this->db->where('destination_id',$post['destination_id']);
					}
					
					break;

				default:
					break;
			}
		}

/*		$this->db
			->select(["source_id as id","CONCAT(source_code,' - ', source_name) as text"])
			->from('sources s')
			->join('documents doc','doc.source_category_id=s.source_category_id')
			->where('doc.document_id',$post['document_id']);
		$row = $this->db->get()->result_array();
*/
		
		$row = $this->db->get();
		$rows = array();
		$j = 0;
		if(isset($post['destination_id'])) {
			$rows[0]['id'] = '';
			$rows[0]['text'] = '-- Please Select --';
			$j = 1;
		}
		
		for($i=0;$i<$row->num_rows();$i++) {
			$rows[$j]['id'] 	= $row->result_array()[$i]['id'];
			$rows[$j]['text'] 	= $row->result_array()[$i]['text'];
			$j++;
		}
		
		$result['status'] = 'OK';
		$result['data'] = $rows;

		return $result;
	}

	public function getSource_($post = array()){
		$result = array();
/*
		$this->db
			->from('documents d')
			->join('m_source_type mst','mst.m_source_type_id=d.m_source_type_id')
			->where('document_id',$post['document_id']);

		$data = $this->db->get()->row_array();

		$row=array();

		if($data){

			switch ($data['m_source_type_id']) {
				case 1:
					// Get supplier
					$this->db
						->select(['id',"CONCAT(code, ' - ', name) as text"])
						->from('suppliers');

					$row = $this->db->get()->result_array();
					break;

				case 2:
					// Get customer
					$this->db
						->select(['id',"CONCAT(code, ' - ', name) as text"])
						->from('customers');

					$row = $this->db->get()->result_array();
					break;

				case 3:
					// Get customer
					$this->db
						->select('id,name as text')
						->from('warehouses');

					$row = $this->db->get()->result_array();
					break;

				case 4:
					// Get customer
					$this->db
						->select('destination_id as id,destination_code as text')
						->from('warehouses');

					$row = $this->db->get()->result_array();
					break;

				default:
					break;
			}
		}
*/
		$this->db
			->select(["source_id as id","CONCAT(source_code,' - ', source_name) as text"])
			->from('sources s')
			->join('documents doc','doc.source_category_id=s.source_category_id')
			->where('doc.document_id',$post['document_id']);
		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

	public function updateStatusReceiving($post = array()){
		$result = array();

		$iLen = $post['id_inbound'];
		for($i = 0; $i < $iLen; $i++){

		}

		$result['status'] = 'OK';
		$result['message'] = 'Update status to receiving success';

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$this->db
			->select([
				"inb.id_inbound","inb.code as kd_inbound", "TO_CHAR(inb.date, 'DD/MM/YYYY') AS tanggal_inbound",
				"inb.document_id as id_inbound_document","doc.name as inbound_document_name",
/*				"spl.code as kd_supplier",
				"spl.name as nama_supplier",
				"spl.address as alamat_supplier",
				"spl.phone as telepon_supplier",
				"spl.contact_person as cp_supplier",
				"cust.name as customer_name",
				"cust.address",
				"cust.phone",
*/
				"s.source_code as kd_supplier",
				"s.source_name as nama_supplier",
				"s.source_address as alamat_supplier",
				"s.source_phone as telepon_supplier",
				"s.source_contact_person as cp_supplier",
				"st.name as nama_status", "user_name",
				"inb.status_id as id_status_inbound"
			])
			->from('inbound inb')
/*			->join('suppliers spl','spl.id=inb.supplier_id AND inb.document_id NOT IN (2,3)','LEFT OUTER')
			->join('customers cust','cust.id=inb.supplier_id AND inb.document_id IN (2)','LEFT OUTER')
			->join('warehouses whs','whs.id=inb.supplier_id AND inb.document_id IN (3)','LEFT OUTER')
*/
			->join('sources s','s.source_id=inb.supplier_id','left')
			->join('status st','st.status_id=inb.status_id','LEFT')
			->join('documents doc','doc.document_id=inb.document_id','LEFT')
			->join('users usr','usr.id=inb.user_id')
			->where('inb.id_inbound',$post['id_inbound']);

		$result = $this->db->get()->row_array();

		return $result;
	}

    public function get_data_excel($post=array()){
        $where='';
        if(!empty($post['id_inbound'])){
        	$this->db->where('inb.id_inbound', $post['id_inbound']);
        }
        $this->db
        	->select([
        		"inbitm.id as id_inbound_barang","itm.code as kd_barang",
        		"(CASE itm.preorder WHEN 1 THEN CONCAT(itm.name, ' - PREORDER') ELSE itm.name END) as nama_barang",
        		"COALESCE(CONCAT(inbitm.qty, ' ', unt.name), CAST(inbitm.qty as CHAR)) as jumlah_barang",
        		"CONCAT((
					SELECT
						COALESCE(SUM(first_qty),0)
					FROM
						receivings r
					LEFT JOIN item_receiving ir
						ON ir.receiving_id=r.id
					LEFT JOIN item_receiving_details ird
						ON ird.item_receiving_id=ir.id
					WHERE
						r.po_id=inb.id_inbound
					AND
						ir.item_id=itm.id
        		), ' ', unt.name) as total_received"
        	])
        	->from('inbound inb')
        	->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound')
        	->join('items itm','itm.id=inbitm.item_id')
        	->join('units unt','unt.id=itm.unit_id','left')
        	->join('categories cat','cat.id=itm.category_id','left');

        $row = $this->db->get()->result_array();
        return $row;
    }

	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_inbound_barang',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
			4 => 'jumlah_barang',
			5 => 'total_received'
		);

		$where = '';
		$search = filter_var(trim($_POST['search']['value']), FILTER_SANITIZE_STRING);

		$this->db->start_cache();

		if(!empty($search)){
			$this->db->group_start();
			$this->db->like('LOWER(itm.name)',$search);
			$this->db->like('LOWER(itm.code)',$search);
			$this->db->like('LOWER(inbitm.qty)',$search);
			$this->db->group_end();
		}

		if(!empty($post['id_inbound'])){
			$this->db->where('inb.id_inbound',$post['id_inbound']);
		}

		$this->db
			->from('inbound inb')
			->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound')
			->join('items itm','itm.id=inbitm.item_id')
			->join('units unt','unt.id=itm.unit_id','left')
			->join('categories cat','cat.id=itm.category_id','left');

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->select([
				"inbitm.id as id_inbound_barang","itm.code as kd_barang","(CASE itm.preorder WHEN 1 THEN CONCAT(itm.name, ' - PREORDER') ELSE itm.name END) as nama_barang","COALESCE(CONCAT(inbitm.qty, ' ', unt.name), CAST(inbitm.qty as CHAR)) as jumlah_barang",
				"CONCAT((
					SELECT
						COALESCE(SUM(first_qty),0)
					FROM
						receivings r 
						JOIN item_receiving ir ON ir.receiving_id=r.id
						JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
						JOIN hp_receiving_inbound hpi ON hpi.receiving_id=r.id
					WHERE
						hpi.inbound_id=inb.id_inbound
					AND
						ir.item_id=itm.id
				), ' ', unt.name) as total_received",
				"itm.preorder"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id_inbound_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			// $nested[] = ($row[$i]['preorder'] == 1) ? "YES" : "NO";
			$nested[] = $row[$i]['jumlah_barang'];
			$nested[] = $row[$i]['total_received'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function cek_not_usage_po($id_po){
        $this->db->from('receivings');
        $this->db->where('po_id',$id_po);
        $hasil = $this->db->get();
        if($hasil->num_rows()>0){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    public function updateInboundStatus(){
    	//--------step 1 update per masing - masing barang di inbound barang-------------
    	$sql = "SELECT
    				ib.id_inbound_barang,
					ib.id_barang,
					IFNULL(ib.jumlah_barang,0) as jumlah_barang,
					IFNULL(tq.qty_rcv,0) as qty_rcv,
					i.kd_inbound
				FROM
					inbound i
				LEFT JOIN inbound_barang ib on ib.id_inbound = i.id_inbound
				LEFT JOIN(
				SELECT
					r.id_po,
					id_barang,
					sum(first_qty)AS qty_rcv
				FROM
					receiving_barang rb
				LEFT JOIN receiving r ON r.id_receiving = rb.id_receiving
				GROUP BY
					id_barang,
					r.id_po
				)tq ON tq.id_barang = ib.id_barang
				AND ib.id_inbound = tq.id_po
				WHERE (ib.st_inbound_barang IS NULL OR ib.st_inbound_barang != 2)";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			if($row->jumlah_barang == $row->qty_rcv){
				$data_update = array(
					'st_inbound_barang' => 2);
				$this->db->where('id_inbound_barang',$row->id_inbound_barang);
				$this->db->update('inbound_barang',$data_update);
			}
			# code...
		}

		//---------step 2, update per inboundberdasarkan jumlah barang yg sudah close----------
		$sql = "SELECT
					ib.id_inbound,
					count(id_barang)AS qty_barang,
					sum(st_inbound_barang)AS qty_finish
				FROM
					inbound_barang ib
				LEFT JOIN inbound i ON i.id_inbound = ib.id_inbound
				WHERE
					(
						id_status_inbound != 2
						OR id_status_inbound IS NULL
					)
				GROUP BY
					ib.id_inbound";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			$jum_barang = $row->qty_barang * 2;
			/*if( $row->qty_finish > 0){
				$data_update = array(
					'id_status_inbound' => 3);
				$this->db->where('id_inbound',$row->id_inbound);
				$this->db->update('inbound',$data_update);
			}*/
			if($jum_barang == $row->qty_finish){
				$data_update = array(
					'id_status_inbound' => 2);
				$this->db->where('id_inbound',$row->id_inbound);
				$this->db->update('inbound',$data_update);
			}
			# code...
		}
    }

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_inbound',
			1 => '',
			2 => 'kd_inbound',
			3 => 'sort_date',
			4 => 'inbound_document_name',
			5 => 'kd_supplier',
			6 => 'total_item',
			7 => '',
			8 => 'nama_status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['po'])){
			$this->db->where('inb.id_inbound',$post['po']);
		}

		if(!empty($post['id_supplier'])){
			$this->db->where('inb.supplier_id',$post['id_supplier']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('inb.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'));
			$this->db->where('inb.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'));
		}

		if(!empty($post['doc_type'])){
			$this->db->where('inb.document_id',$post['doc_type']);
		}

		if(!empty($post['doc_status'])){
			$this->db->where_in('inb.status_id', $post['doc_status']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('inb.warehouse_id', $post['warehouse']);
		}

		$this->db
			->select('DISTINCT(inb.id_inbound) as id_inbound')
			->from('inbound inb')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
/*			->join('suppliers spl','spl.id=inb.supplier_id AND inb.document_id NOT IN (2,3)','LEFT OUTER')
			->join('customers cust','cust.id=inb.supplier_id AND inb.document_id IN (2)','LEFT OUTER')
			->join('warehouses whs','whs.id=inb.supplier_id AND inb.document_id IN (3)','LEFT OUTER')
*/
			->join('sources s','s.source_id=inb.supplier_id','LEFT')
			->join('destinations d','d.destination_id=inb.supplier_id','LEFT')
			->join('status st','st.status_id=inb.status_id','LEFT')
			->join('documents doc','doc.document_id=inb.document_id','LEFT')
			// STATUS ID 100 = DELETE
			->group_by('inb.id_inbound')
			->where('inb.status_id <> 100');

		$this->db->stop_cache();
		$row = $this->db->get();
		$totalData = $row->num_rows();

		$tbl = "SELECT
					inbitm.inbound_id, COUNT(*) AS total_item
				FROM
					inbound_item inbitm
				JOIN items itm
					ON itm.id=inbitm.item_id
				GROUP BY
					inbitm.inbound_id";

		$total_received = "	SELECT
								COUNT(*)
							FROM
								item_receiving_details ird
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN receivings rcv
								ON rcv.id=ir.receiving_id
							JOIN hp_receiving_inbound hri
								ON hri.receiving_id=rcv.id
							JOIN inbound inb
								ON inb.id_inbound=hri.inbound_id
							JOIN locations loc
								ON loc.id=ird.location_id
							WHERE
								inb.id_inbound=inb.id_inbound
							AND
								loc.id NOT IN (100,104)
							AND
								inb.status_id <> 4";

		$this->db
			->select([
				"inb.code as kd_inbound",
				"inb.date as sort_date",
				"TO_CHAR(inb.date, 'DD/MM/YYYY') as tanggal_inbound",
				"doc.name as inbound_document_name",
				// "COALESCE(COALESCE(spl.name, cust.name), whs.name) as kd_supplier",
				"COALESCE(s.source_name, d.destination_name) as kd_supplier",
				"inb.status_id as id_status_inbound",
				"st.name as nama_status",
				"total_item",
				"inb.remark",
				"SUM(ird.first_qty) as total_received"
			])
			->join("($tbl) as tbl",'tbl.inbound_id=inb.id_inbound','left')
			->join('hp_receiving_inbound hri','hri.inbound_id=inb.id_inbound','left')
			->join('receivings r','r.id=hri.receiving_id','left')
			->join('item_receiving ir','ir.receiving_id=r.id','left')
			->join('item_receiving_details ird','ird.item_receiving_id=ir.id and ird.location_id NOT IN (100)','left')
			// ->group_by('inb.code, inb.date, doc.name, spl.name, cust.name, whs.name, inb.status_id, st.name, tbl.total_item, inb.remark')
			->group_by('inb.code, inb.date, doc.name, s.source_name, d.destination_name, inb.status_id, st.name, tbl.total_item, inb.remark')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			$disabled = '';
			if($row[$i]['total_received'] > 0){
				if($row[$i]['id_status_inbound'] == 4){
					$disabled = 'disabled-link';
				}else{
					$disabled = '';
				}
			}else{
				$disabled = 'disabled-link';
			}

			$disabled1 = '';
			if($row[$i]['id_status_inbound'] == 4)
				$disabled1 = 'disabled-link';

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$disabled.'" data-id="'.$row[$i]['id_inbound'].'"><i class="fa fa-check"></i> Close Inbound</a>';
			$action .= '</li>';

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$disabled1.'" data-id="'.$row[$i]['id_inbound'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$disabled1.'" data-id="'.$row[$i]['id_inbound'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['id_inbound'];
			//$nested[] = '<input type="checkbox" name="id[]" value="'.$row[$i]['id_inbound'].'">';
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().$post['className'].'/detail/'.$row[$i]['id_inbound'].'">'.$row[$i]['kd_inbound'].'</a>';
			$nested[] = $row[$i]['tanggal_inbound'];
			$nested[] = $row[$i]['inbound_document_name'];
			$nested[] = $row[$i]['kd_supplier'];
			$nested[] = $row[$i]['total_item'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['nama_status'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data,
            "current_filter"  => $this->session->userdata('current_filter')
		);

		return $result;
	}
	/*---- END ADJ -----*/

    public function get_by_id($id) {
        $condition['a.id_inbound'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function get_inbound_barang($id_inbound) {

		$this->db
			->select('*, inbitm.item_id as id_barang, inbitm.qty as jumlah_barang, unt.name as nama_satuan, inbitm.cost_center_id as cost_center')
			->from('inbound_item inbitm')
			->join('items itm','itm.id=inbitm.item_id')
			->join('units unt','unt.id=itm.unit_id','left')
			->where('inbitm.inbound_id',$id_inbound)
			->order_by('inbitm.id','ASC');

		return $this->db->get();

    }

    public function data_insert($data){

        $resData = array(
        	'code'					=> $data['kd_inbound'],
        	'date'					=> $data['tanggal_inbound'],
        	'supplier_id'			=> $data['id_supplier'],
        	'document_id'			=> $data['id_inbound_document'],
        	'user_id'				=> $data['user_id'],
			'remark'				=> $data['remark'],
			'warehouse_id'			=> $data['warehouse']
     	);

    	if(isset($data['po_number'])){
     		$resData['po_number'] = $data['po_number'];
     	}

        return $resData;

    }

    public function data2_insert($data2){

    	$array = array();

    	$i=0;
		foreach ($data2 as $val) {
			$array[$i] = array(
				'qty'		=> $val['jumlah_barang'],
				'item_id'	=> $val['id_barang']
			);

			if(isset($val['po_item'])){
				$array[$i]['po_item'] = $val['po_item'];
			}

			if(isset($val['cost_center'])){
				$array[$i]['cost_center_id'] = $val['cost_center'];
			}

			$i++;
		}

		return $array;
    }

    public function create($data, $data2) {
        $this->db->trans_start();

		$data = $this->data_insert($data);
		$data['status_id'] = 3;

        $this->db->insert($this->table, $data);
		$this->db->trans_complete();
		
		$this->db->limit(1);
		$this->db->order_by('id_inbound','desc');
        $id_inbound = $this->db->get($this->table)->row_array()['id_inbound'];
        // $id_inbound = $this->db->insert_id();

        $data2 = $this->data2_insert($data2);
		if(!empty($data2)){
			foreach ($data2 as &$inbound_barang) {
				$inbound_barang['inbound_id'] = $id_inbound;
			}
			$this->db->insert_batch($this->table5, $data2);
		}

        return $id_inbound;
    }

    public function update($data, $data2, $id) {
        $this->db->trans_start();

		$data = $this->data_insert($data);
        $this->db->update($this->table, $data, array('id_inbound' => $id));

        //remove all inbound_barang which the id_inbound is $id
        $this->db->where("inbound_id", $id);
        $this->db->delete($this->table5);

        //add all new inbound_barang
        $data2 = $this->data2_insert($data2);
		if(!empty($data2)){
			foreach ($data2 as &$inbound_barang) {
				$inbound_barang['inbound_id'] = $id;
			}
			$this->db->insert_batch($this->table5, $data2);
		}
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function delete($id) {
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					receivings
				WHERE
					po_id=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		if($row['total'] > 0 ){

			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';

		}else{

			$this->db
				->set('status_id', 100)
				->where('id_inbound', $id)
				->update($this->table);

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';

		}

        return $result;
    }

    public function options($default = '--Pilih Kode Barang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->nama_barang ;
		}
		
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load_model('setting_model');

        foreach ($data as $dt) {

			if($table_name == 'm_inbound_document'){

				$r = $this->setting_model->getData();

				if(strtolower($dt['inbound_document_name']) == 'peminjaman' && $r['peminjaman'] == 'N')
					continue;
			}

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }

    public function getInDocument($id){
    	
    	$doc = $this->db->get_where('documents',array('document_id'=>$id))->row();

    	$docName = "";

    	if($doc){

    		$docName .= $doc->code;
    		$docName .= $doc->separator;

			switch ($doc->year_type) {
			    case 1:
			    	$docName .= date("y");
			        break;
			    case 2:
			    	$docName .= date("Y");
			        break;
			    default:
			    	$docName .= ''; 
			}

			if($doc->month_type){
				$docName .= date("m");
			}

			$seqlen = $doc->seq_length - strlen($doc->seq_current);

			for($i=0;$i<$seqlen;$i++){
				$docName .= '0';
			}

			$docName .= $doc->seq_current;

    	}

    	if(empty($docName)){
    		return false;
    	}

    	return $docName;
    }

    public function addInDocument($data=array()){

    	$sql = "UPDATE documents SET seq_current = seq_current + 1 WHERE document_id = ".$data['id_inbound_document'];
    	$this->db->query($sql);

    }

    public function statusOptions($default = '--Pilih Status--', $key = '') {

    	$this->db->like('type','INBOUND');
        $data = $this->db->get('status');
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->status_id] = $row->name ;
        }
        return $options;
    }

    public function importFromExcel($data){

    	$inbounds = array();
    	$inbound_item = array();
    	$items = array();
    	$documents = array();
    	$suppliers = array();

    	$len = count($data);

    	$this->db->trans_start();

    	for ($i=0; $i < $len ; $i++) {

    		if(!array_key_exists($data[$i]['document_name'], $inbounds)){

        		if(!array_key_exists($data[$i]['document_id'], $documents)){
            		$documents[$data[$i]['document_id']] = $this->db->get_where('documents',['code'=>$data[$i]['document_id'],'type'=>'INBOUND'])->row()->document_id;
        		}

        		if(!array_key_exists($data[$i]['supplier_id'], $suppliers)){
            		$suppliers[$data[$i]['supplier_id']] = $this->db->get_where('suppliers',['code'=>$data[$i]['supplier_id']])->row()->id;
        		}

        		$data_inbound = [
        			'code'					=> $data[$i]['document_name'],
        			'date'					=> $data[$i]['date'],
        			'document_id' 			=> $documents[$data[$i]['document_id']],
        			'supplier_id'			=> $suppliers[$data[$i]['supplier_id']],
        			'remark'				=> $data[$i]['remark'],
        			'status_id'				=> 3,
        			'user_id'				=> $this->session->userdata('user_id')
        		];

        		$inbounds[$data[$i]['document_name']] = $this->inbound_model->create_inbound($data_inbound);

    		}

    		if(!array_key_exists($data[$i]['item_id'], $items)){
        		$items[$data[$i]['item_id']] = $this->db->get_where('items',['code'=>$data[$i]['item_id']])->row()->id;
    		}

    		$inbound_item[] = [
    			'inbound_id'=> $inbounds[$data[$i]['document_name']],
    			'item_id'	=> $items[$data[$i]['item_id']],
    			'qty'		=> $data[$i]['qty']
    		];

    	}

    	$this->db->insert_batch('inbound_item', $inbound_item);

    	$this->db->trans_complete();

    	return $this->db->trans_status();

    }

    function inboundSync(){

    	$this->load_model('api_staging_model');

    	$api = "po_maintain/POHeaders";

    	$this->api_staging_model->_curl_process_get($api);

    }

    public function updateInboundDocumentNumber($id){
    

    	$this->db->query("UPDATE documents SET seq_current=seq_current+1 WHERE document_id=".$id);

    	$doc = $this->db->get_where('documents',array('document_id'=>$id))->row();

    	$docName = "";

    	if($doc){

    		$docName .= $doc->code;
    		$docName .= $doc->separator;

			switch ($doc->year_type) {
			    case 1:
			    	$docName .= date("y");
			        break;
			    case 2:
			    	$docName .= date("Y");
			        break;
			    default:
			    	$docName .= ''; 
			}

			if($doc->month_type){
				$docName .= date("m");
			}

			$seqlen = $doc->seq_length - strlen($doc->seq_current);

			for($i=0;$i<$seqlen;$i++){
				$docName .= '0';
			}

			$docName .= $doc->seq_current;

    	}

    	if(empty($docName)){
    		return false;
    	}

    	return $docName;
    	
    }
	
	public function importFromExcelInboundStock($data,$warehouse){

    	$len = count($data);

    	$userId = $this->session->userdata('user_id');
    	$documentId=44;

    	$date = date('Y-m-d h:i:s');

    	$inboundItem = array();
    	$receivingItem = array();
    	$receivingItemDetail = array();

    	$this->db->trans_start();

    	$this->db
    		->set('code',$this->getInDocument($documentId))
    		->set('warehouse_id',$warehouse)
    		->set('user_id',$userId)
    		->set('status_id',4)
    		->set('date',date('Y-m-d'))
    		->set('document_id',$documentId)
    		->insert('inbound');

    	// $inboundId = $this->db->insert_id();
		$this->db->trans_complete();
		$this->db->limit(1);
		$this->db->order_by('id_inbound','desc');
		$inboundId = $this->db->get('inbound')->row_array()['id_inbound'];

    	$this->load_model('receiving_model');

		$this->db->trans_start();
		
    	$this->db
    		->set('code',$this->receiving_model->get_rcv_code())
    		->set('vehicle_plate','-')
    		->set('user_id',$userId)
    		->set('dock','-')
    		->set('st_receiving',1)
    		->set('remark','-')
    		->set('start_tally',$date)
    		->set('finish_tally',$date)
    		->set('start_putaway',$date)
    		->set('finish_putaway',$date)
    		->set('date',date('Y-m-d'))
    		->set('delivery_doc','-')
    		->insert('receivings');

    	// $receivingId = $this->db->insert_id();
		$this->db->trans_complete();
		$this->db->limit(1);
		$this->db->order_by('id','desc');
		$receivingId = $this->db->get('receivings')->row_array()['id'];

		$this->db->trans_start();
		
    	$this->db
    		->set('receiving_id',$receivingId)
    		->set('inbound_id',$inboundId)
    		->insert('hp_receiving_inbound');

    	$sql = "UPDATE documents SET seq_current = seq_current + 1 WHERE document_id =$documentId";
    	$this->db->query($sql);

        $this->db->set('inc_rcv',"inc_rcv+1",FALSE);
        $this->db->update('m_increment');


    	$items = array();
    	$locations = array();
    	$serialNumbers = array();

    	for ($i=0; $i < $len; $i++) { 

    		if(!array_key_exists($data[$i]['location'],$locations)){
				if($data[$i]['location'] != null) {
					$locations[$data[$i]['location']] = $this->db->get_where('locations',['name'=>$data[$i]['location']])->row_array();
					if(!$locations[$data[$i]['location']]){
						return array('status'=>false,'message'=>'Location dengan kode \''.$data[$i]['location'].'\' tidak tersedia.');
					}
				}
    		}

			$data[$i]['item_code'] = (string)$data[$i]['item_code'];

    		if($data[$i]['item_code'] == "" || $data[$i]['serial_number'] == ""){
    			continue;
    		}

    		if(!array_key_exists($data[$i]['item_code'],$items)){

    			$items[$data[$i]['item_code']]['item_id'] = $this->db->get_where('items',['sku'=>strval($data[$i]['item_code'])])->row();
    			if(!$items[$data[$i]['item_code']]['item_id']){
	    			return array('status'=>false,'message'=>'Item dengan kode \''.$data[$i]['item_code'].'\' tidak tersedia.');
    			}

    			$items[$data[$i]['item_code']]['item_id'] = $items[$data[$i]['item_code']]['item_id']->id;


	    		$receivingItem[$data[$i]['item_code']] = array(
	    			"receiving_id"	=> $receivingId,
	    			"item_id"		=> $items[$data[$i]['item_code']]['item_id'],
	    			'inbound_id'	=> $inboundId,
	    			"qty"			=> $data[$i]['quantity']
	    		);
				
	    		$this->db->insert('item_receiving',$receivingItem[$data[$i]['item_code']]);
				$this->db->trans_complete();

				$this->db->limit(1);
				$this->db->order_by('id','desc');
				$receivingItem[$data[$i]['item_code']]['id'] = $this->db->get('item_receiving')->row_array()['id'];
    			// $receivingItem[$data[$i]['item_code']]['id'] = $this->db->insert_id();
				
				$this->db->trans_start();

	    		$inboundItem[$data[$i]['item_code']] = array(
	    			"inbound_id"	=> $inboundId,
	    			"item_id" 		=> $items[$data[$i]['item_code']]['item_id'],
	    			"qty"			=> $data[$i]['quantity']
	    		);

	    	}else{

	    		$receivingItem[$data[$i]['item_code']]['qty'] += $data[$i]['quantity'];

	    		$inboundItem[$data[$i]['item_code']]['qty'] += $data[$i]['quantity'];

	    	}


	    	$receivingItemDetail[] = array(
	    		"item_receiving_id"	=> $receivingItem[$data[$i]['item_code']]['id'],
	    		"first_qty"			=> $data[$i]['quantity'],
	    		"last_qty"			=> $data[$i]['quantity'],
	    		"unique_code"		=> str_replace(" ","",$data[$i]['serial_number']),
	    		"tgl_in"			=> date('Y-m-d h:i:s'),
	    		"putaway_time"		=> date('Y-m-d h:i:s'),
	    		"location_id"		=> $locations[$data[$i]['location']]['id'],
	    		"st_receive"		=> 1,
	    		"user_id_receiving" => $userId,
	    		"user_id_putaway"	=> $userId,
	    		"qc_id"				=> 1,
	    		"item_id"			=> $items[$data[$i]['item_code']]['item_id'],
	    		"rfid_tag"			=> $data[$i]['serial_number']
	    	);

	    	//if(!in_array($data[$i]['serial_number'], $serialNumbers)){
		    	$serialNumbers[] = $data[$i]['serial_number'];
		    /*}else{
    			return array('status'=>false,'message'=>'Serial number \''.$data[$i]['serial_number'].'\' terdapat lebih dari satu.');
		    }*/

    	}

    	$this->db->insert_batch('inbound_item', $inboundItem);
    	$this->db->insert_batch('item_receiving_details',$receivingItemDetail);
    	$this->db->update_batch('item_receiving', $receivingItem, 'id');

    	$this->db->trans_complete();

    	return array('status'=>$this->db->trans_status());

    }

}

?>


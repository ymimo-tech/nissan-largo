<?php
class expired_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
	}
	
	public function getItem(){
		$result = array();
		
		$sql = 'SELECT 
					id as id_barang, CONCAT(code, \' - \', name) AS nama_barang 
				FROM 
					items
				WHERE has_expdate=1';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getListRaw($post = array()){
		$result = array();
		
		$where = '';

		if(!empty($post['id_barang']) && $post['id_barang'] != 'null'){
			$this->db->where_in('ird.item_id',$post['id_barang']);
		}
		
		if(!empty($post['loc_id']) && $post['loc_id'] != 'null'){
			$this->db->where_in('ird.location_id',$post['loc_id']);
		}

		if(!empty($post['params']) && $post['params'] != 'undefined' && $post['params'] != '' || $post['params'] == 0){
			if($post['params'] != 'undefined'){

				switch ($post['params']) {
					case 30:
						$this->db->having('(tgl_exp - current_date) > 0 and (tgl_exp - current_date) <= 30');
						break;

					case 60:
						$this->db->having('(tgl_exp - current_date) > 30 and (tgl_exp - current_date) <= 60');
						break;

					case 120:
						$this->db->having('(tgl_exp - current_date) > 60 and (tgl_exp - current_date) <= 120');
						break;
					
					default:
						$this->db->having('(tgl_exp - current_date) <= 0');
						break;
				}

			}
		}
		$this->db
			->select(["itm.code as sku","itm.name as nama_barang","COALESCE(SUM(last_qty),0) as stock","unt.code as nama_satuan","DATE(tgl_exp) as tgl_exp","loc.name as loc_name"])
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id')
			->join('items itm','itm.id=ird.item_id')
			->join('units unt','unt.id=itm.unit_id')
			->where('putaway_time IS NOT NULL',NULL)
			->where('last_qty >',0)
			->where('tgl_exp IS NOT NULL',NULL)
			->group_by('itm.code, itm.name, unt.code, tgl_exp, loc.name');
			
		$result = $this->db->get()->result_array();

		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'item_code',
			2 => 'item_name',
			3 => 'qty',
			4 => 'unit',
			5 => 'tgl_exp'
		);
		
		$where = '';
		$wh='';
		
		$this->db->start_cache();

		if(!empty($post['id_barang'])){
			$this->db->where_in('ird.item_id',$post['id_barang']);
		}
		
		if(!empty($post['loc_id']) && $post['loc_id'] != 'null'){
			$this->db->where_in('ird.location_id',$post['loc_id']);
		}

		if(!empty($post['params']) || ($post['params'] == 0 && $post['params'] != '')){
			switch ($post['params']) {
				case 30:
					$this->db->having('(tgl_exp - current_date) > 0 and (tgl_exp - current_date) <= 30');
					break;

				case 60:
					$this->db->having('(tgl_exp - current_date) > 30 and (tgl_exp - current_date) <= 60');
					break;

				case 120:
					$this->db->having('(tgl_exp - current_date) > 60 and (tgl_exp - current_date) <= 120');
					break;
				
				default:
					$this->db->having('(tgl_exp - current_date) <= 0');
					break;
			}
		}

		$this->db
			->select(["itm.code as item_code","itm.name as item_name","COALESCE(SUM(last_qty),0) as qty","unt.code as unit","DATE(tgl_exp) as tgl_exp"])
			->from('item_receiving_details ird')
			->join('items itm','itm.id=ird.item_id')
			->join('units unt','unt.id=itm.unit_id')
			->where('putaway_time IS NOT NULL',NULL)
			->where('last_qty >',0)
			->where('tgl_exp IS NOT NULL',NULL)
			->group_by('itm.code, itm.name, unt.code, tgl_exp');

		$this->db->stop_cache();
				
		$totalData = $this->db->get()->num_rows();

		$this->db
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);
		
		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			$exp_date = DateTime::createFromFormat('Y-m-d', $row[$i]['tgl_exp']);
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['item_code'];
			$nested[] = $row[$i]['item_name'];
			$nested[] = $row[$i]['qty'];
			$nested[] = $row[$i]['unit'];
			$nested[] = ( $row[$i]['tgl_exp'] == '0000-00-00') ? 'Not Set Yet' : $exp_date->format('d M Y');
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}

	public function getExpiredList($post=array()){

		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'unique_code',
			2 => 'last_qty',
			3 => 'tgl_exp'
		);
		
		$where = '';
		
		if(!empty($post['id_barang'])){
			$this->db->where_in('item_id',$post['id_barang']);
		}
		$this->db->start_cache();
		
		if(!empty($post['tgl_exp'])){
			if($post['tgl_exp'] != 'Not Set Yet'){
				$exp_date = new DateTime($post['tgl_exp']);
				$this->db->where('tgl_exp',$exp_date->format('Y-m-d'));
			}else{
				$this->db->where('tgl_exp','0000-00-00');
			}
		}

		$this->db
			->from('item_receiving_details')
			->where('putaway_time IS NOT NULL',NULL)
			->where('last_qty >',0)
			->where('tgl_exp IS NOT NULL',NULL);

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);
		
		$data = array();

		for($i=0;$i<$totalFiltered;$i++){
			$exp_date = DateTime::createFromFormat('Y-m-d', $row[$i]['tgl_exp']);
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['unique_code'];
			$nested[] = $row[$i]['last_qty'];
			$nested[] = ( $row[$i]['tgl_exp'] == '0000-00-00') ? 'Not Set Yet' : $exp_date->format('d M Y');
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;

	}

	public function getWidget(){

		$sql = "
				SELECT
					COALESCE(SUM(qty),0)
				FROM
				(
					SELECT 
						SUM(last_qty) AS qty, (DATE(tgl_exp) - current_date) as expired
					FROM 
						item_receiving_details ird
					WHERE
						putaway_time IS NOT NULL
					AND last_qty > 0
		            AND tgl_exp IS NOT NULL
		            GROUP BY tgl_exp
		            HAVING %s
	            ) as tbl
	        ";

		$oneMonth = sprintf($sql,"(DATE(tgl_exp) - current_date) >= 1 AND (DATE(tgl_exp) - current_date) <= 30");
		$twoMonth = sprintf($sql, "(DATE(tgl_exp) - current_date) > 30 AND (DATE(tgl_exp) - current_date) <= 60");
		$threeMonth = sprintf($sql, "(DATE(tgl_exp) - current_date) > 60 AND (DATE(tgl_exp) - current_date) <= 90");
		$expired = sprintf($sql, "(DATE(tgl_exp) - current_date) < 1 ");

		$sqls = "
			SELECT
				($oneMonth) as one_month,
				($twoMonth) as two_month,
				($threeMonth) as three_month,
				($expired) as expired
		";

		$rows = $this->db->query($sqls)->row_array();

		$result['data'] = $rows;
		return $result;

	}
}
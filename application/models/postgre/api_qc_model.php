<?php

class Api_qc_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    function get($serial_number){
        $this->db->select("location_id as loc_id");
        $this->db->from("item_receiving_details");
        $this->db->where("unique_code", $serial_number);
        $this->db->where("item_id IS NOT NULL");
        $this->db->where("last_qty >",0);
        $this->db->where_not_in("qc_id", 2);
        $this->db->where_not_in("location_id", [100,101,104]);
        return $this->db->get();
    }

    function get_out($serial_number){
        $this->db->select("location_id as loc_id");
        $this->db->from("item_receiving_details");
        $this->db->where("unique_code", $serial_number);
        $this->db->where("item_id IS NOT NULL");
        $this->db->where_in("qc_id", 2);
        $this->db->where_not_in("location_id", [100,101,104]);
        return $this->db->get();
    }

    function retrieve($serial_number){
        $this->db->select("loc.name as loc_name, qc.code as kd_qc");
        $this->db->from("item_receiving_details ird");
        $this->db->join("locations loc", "loc.id=ird.location_id", "left");
        $this->db->join("qc", "qc.id=ird.qc_id", "left");
        $this->db->where("unique_code", $serial_number);
        $this->db->where("last_qty >",0);
        $this->db->where_not_in("location_id", [100,101,104]);
        return $this->db->get();
    }

    function intransit($serial_number){
        $this->db->set("location_id", 103);
        $this->db->where("unique_code", $serial_number);
        $this->db->where("last_qty >",0);
        $this->db->where_not_in("location_id", [100,101,104]);
        $this->db->update("item_receiving_details");
    }

    function get_location($location){
        $this->db->select("name as loc_name");
        $this->db->from("locations");
        $this->db->where("name", $location);
        $this->db->where("(loc_type='QC' or location_type_id=4)", NULL);
        return $this->db->get();
    }

    function get_location_out($location){
        $available = array('1','2','3','5');
        $this->db->select("name as loc_name, loc_type");
        $this->db->from("locations");
        $this->db->where("name", $location);
        $this->db->where_in("location_type_id", $available);
        return $this->db->get();
    }

    function cancel($data){
        $id_loc         = $this->get_id("id as loc_id", "name", $data["loc_name"], "locations")->row_array();
        $id_user        = $this->get_id("id as user_id", "user_name", $data["user"], "users")->row_array();
        $id_qc          = $this->get_id("id as id_qc", "code", $data["kd_qc"], "qc")->row_array();
        
        $this->db->set("location_id", $id_loc["loc_id"]);
        $this->db->set("qc_id", $id_qc["id_qc"]);
        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->update("item_receiving_details");

        $data_insert = array(   
                            'unique_code'       => $data["kd_unik"], 
                            'loc_id_old'    => $id_loc["loc_id"],
                            'user_id_pick'  => $id_user["user_id"],
                            'pick_time'     => $data["pick_time"],
                            'loc_id_new'    => $id_loc["loc_id"],
                            'user_id_put'   => $id_user["user_id"],
                            'put_time'      => date("Y-m-d H:i:s"),
                            'process_name'  => 'QC CANCEL'
                        );

        $this->db->insert("transfers", $data_insert);
    }

    function post($data){

        $id_loc_old     = $this->get_id("id as loc_id", "name", $data["loc_name_old"], "locations")->row_array();
        $id_user_old    = $this->get_id("id as user_id", "user_name", $data["user_pick"], "users")->row_array();
        $id_loc_new     = $this->get_id("id as loc_id", "name", $data["loc_name_new"], "locations")->row_array();
        $id_user_new    = $this->get_id("id as user_id", "user_name", $data["user_put"], "users")->row_array();

        if(empty($id_loc_old) || empty($id_user_old) || empty($id_user_new) || empty($id_loc_new)){

            return false;

        }else{

            if(empty($data['kd_qc']) || $data['kd_qc'] == ''){
                $data['kd_qc'] = 'GOOD';
            }

            if($data['kd_qc'] == "QC HOLD"){
                $this->db->where_in('qc_id',[1,3]);
            }else{
                $this->db->where_in('qc_id',[2]);
	        }

            $this->db
                ->select('item_id')
                ->from('item_receiving_details')
                ->where('unique_code',$data['kd_unik'])
                ->where('last_qty >',0)
                ->limit(1);

            $item = $this->db->get()->row_array();

            if($item){

                $id_qc          = $this->get_id("id as id_qc", "code", $data["kd_qc"], "qc")->row_array();

	            if($data['kd_qc'] == "QC HOLD"){
	                $this->db->where_in('qc_id',[1,3]);
	            }else{
	                $this->db->where_in('qc_id',[2]);
	            }

                $this->db->set("location_id", $id_loc_new["loc_id"]);
                $this->db->set("qc_id", $id_qc["id_qc"]);
                $this->db->set('reason_reject',$data['remark']);
                $this->db->where("unique_code", $data["kd_unik"]);
                $this->db->where_in("location_id", [103]);
                $this->db->where('last_qty >',0);
                $this->db->update("item_receiving_details");

                $data_insert = array(   'unique_code'       => $data["kd_unik"], 
                                        'item_id'       => $item['item_id'],
                                        'loc_id_old'    => $id_loc_old["loc_id"],
                                        'user_id_pick'  => $id_user_old["user_id"],
                                        'pick_time'     => $data["pick_time"],
                                        'loc_id_new'    => $id_loc_new["loc_id"],
                                        'user_id_put'   => $id_user_new["user_id"],
                                        'put_time'      => $data["put_time"],
                                        'process_name'  => 'QC',
                                        'description'   => $data['remark']
                                        );

                $this->db->insert("transfers", $data_insert);

                // return $this->db->insert_id();

                return true;
            }
        }

        return false;

    }

    function sendToStaging(){
        return true;
    }

}
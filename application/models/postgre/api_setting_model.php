<?php

class Api_setting_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function user_validation($uname, $sessionCode) {
    	// return $this->db->get_where("users", array("user_name" => $uname, "handheld_session_code" => $sessionCode));
      return $this->db->get_where("users", array("user_name = '$uname' or nik =" => $uname, "handheld_session_code" => $sessionCode));

    }

    function user_authentication(){
        $uname 		= $this->input->get_request_header('User', true);
        $session    = $this->input->get_request_header('Authorization', true);

        if(!empty($uname) || !empty($session)){

            $validation = $this->user_validation($uname, $session)->row_array();
            if($validation){
                return true;
            } else{
                return false;
            }

        }else{
            return false;
        }
    }

    function user_login($data){
        $this->db->select('id as user_id, *');
        $uname = $data["uname"];
    	// return $this->db->get_where("users", array("user_name" => $data["uname"], "user_password" => $data["upass"]));
      return $this->db->get_where("users", array("user_name = '$uname' or nik =" => $data["uname"], "user_password" => $data["upass"]));

    }

    function user_session($uid, $session_code) {
    	$data["handheld_session_code"] = $session_code;
    	$this->db->where("id", $uid);
    	$this->db->update("users", $data);
    }

    function user_login_detail($uname, $session){
    	$this->db->select("id as user_id");
		$this->db->select("user_name");
    	$this->db->select("handheld_session_code");
    	$this->db->from("users");
    	// $this->db->where("user_name", $uname);
      $this->db->where("user_name ='$uname' or nik ='$uname'");
    	$this->db->where("handheld_session_code", $session);
    	return $this->db->get();
    }

	function get_update($customer){
		$this->db->select("*");
		$this->db->from("update_handheld");
		$this->db->where("target", $customer);
		$this->db->order_by("id", "DESC");
		$this->db->limit(1);
		return $this->db->get();
	}

    function onlineUser(){
        $this->db->select('count(*) as total');
        $this->db->where('handheld_session_code IS NOT NULL', null, false);
        $res = $this->db->get('users')->row();

        return $res->total;
    }

    function user_warehouse(){
        $uname         = $this->input->get_request_header('User', true);
        $session    = $this->input->get_request_header('Authorization', true);

        $this->db->select('wu.warehouse_id');
        $this->db->from('users usr');
        $this->db->join('users_warehouses wu','wu.user_id=usr.id');
        // $this->db->where('usr.user_name',$uname);
        $this->db->where("usr.user_name ='$uname' or nik='$uname'");
        $this->db->where('usr.handheld_session_code',$session);
        $result = $this->db->get();

        $warehouse = [0];

        if($result->num_rows() > 0){
            $warehouse=[];
            foreach ($result->result() as $wh) {
                $warehouse[] = $wh->warehouse_id;
            }
        }

        return $warehouse;
    }

    function checkAppVer(){
            $this->db->select("*");
            $this->db->from("update_handheld");
            $this->db->where("target", 'android');
            $this->db->order_by("id", "DESC");
            $this->db->limit(1);
            return $this->db->get();
    }

    function updateAppVer($version){

        $update = $this->db
                ->set('version',$version)
                ->where('target','android')
                ->update('update_handheld');

        if($update){
            return true;
        }

        return false;

    }


}

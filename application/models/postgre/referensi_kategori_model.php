<?php
class referensi_kategori_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'categories';

    public function data($condition = array()) {
        $this->db->select('a.id as id_kategori, a.code as kd_kategori, a.name as nama_kategori');
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $this->db->where('a.id',$id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id_kategori'])){
            $condition['id'] = $condition['id_kategori'];
            unset($condition['id_kategori']);
        }
 
        $this->data($condition);
        return $this->db->get();
    }  

    public function getData($data=[]){
        $data = [
            'code' => $data['kd_kategori'],
            'name' => $data['nama_kategori']
        ];

        return $data;
    }

    public function create($data) {
        $data = $this->getData($data);
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        $data = $this->getData($data);
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        //return $this->db->delete($this->table, array('id_kategori' => $id));
		
		$result = array();
		
        $this->db->select('count(*) as t')
            ->from('items')
            ->where('category_id', $id);

        $row = $this->db->get()->row_array();
		if($row['t'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('id' => $id));
			
			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}
			
        return $result;
		
    }
    
    public function options($default = '--Pilih Kode Kategori--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_kategori] = $row->nama_kategori ;
        }
        return $options;
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $inserts = array();
        $dataLen = count($data);

        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->select('count(*) as total')
                ->from($this->table)
                ->where('LOWER(code)', strtolower($data[$i]['category_code']))
                ->or_where('lower(name)', strtolower($data[$i]['category_name']));

            $check = $this->db->get()->row()->total;

            if($check > 0){
                continue;
            }else{

                $inserts[] = array(
                    'code'          => $data[$i]['category_code'],
                    'name'          => $data[$i]['category_name']
                );

            }

        }

        if(!$inserts){
            $this->db->trans_complete();
            return false;
        }

        $this->db->insert_batch($this->table,$inserts);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }
    
}

?>
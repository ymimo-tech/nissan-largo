<?php
class destination_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'destination';
    private $table2 = 'z_destination';

    public function data($condition = array()) {
        $this->db->select('destination_id as "idGudang",destination_name as "namaGudang", destination_code as "kodeGudang", destination_desc as description');
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function getById($id){
        $condition['destination_id'] = $id;
        $this->data($condition);

        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['idGudang'])){
            $condition['destination_id'] = $condition['idGudang'];
            unset($condition['idGudang']);
        }

        $this->data($condition);

        return $this->db->get();
    }

    private function data_insert($data){

        $post = array(
            'destination_name'          => $data['namaGudang'],
            'destination_code'          => $data['kodeGudang']
        );

        return $post;

    }

    public function get_doc_gudang($id){

        $this->db
            ->select('document_id')
            ->from($this->table2)
            ->where('destination_id',$id);

        $data = $this->db->get()->result_array();

        $results = array();
        foreach ($data as $d) {
            $results[]  = $d['document_id'];
        }

        return $results;
    }

    public function create($data,$data2) { 
        $this->db->trans_start();

        $data = $this->data_insert($data);

        $this->db->insert($this->table, $data);

        $id = $this->db->insert_id();

        if(!empty($data2)){
            $data_insert = array();
            foreach($data2 as $idDoc){
                $data_insert[] = array(
                    'document_id' => $idDoc,
                    'destination_id' => $id
                );
            }
        }

        $this->db->insert_batch($this->table2, $data_insert);

        $this->db->trans_complete();
    }

    public function update($id, $data, $data2) {

        $this->db->trans_start();

        $data = $this->data_insert($data);

        $this->db->update($this->table, $data, array('destination_id' => $id));

        $this->db->delete($this->table2,array('destination_id'=>$id));

        if(!empty($data2)){
            $data_insert = array();
            foreach($data2 as $idDoc){
                $data_insert[] = array(
                    'document_id' => $idDoc,
                    'destination_id' => $id
                );
            }
            $this->db->insert_batch($this->table2,$data_insert);
        }

        $this->db->trans_complete();
        return $this->db->trans_status();

    }

    public function delete($id) {
        $result = array();
        
        $sql = 'SELECT 
                    COUNT(*) AS t 
                FROM 
                    outbound 
                WHERE 
                    destination_id=\''.$id.'\'';
                    
        $row = $this->db->query($sql)->row_array();

        if($row['t'] > 0){

            $result['status'] = 'ERR';
            $result['message'] = 'Cannot delete data because this data have relation to another table';

        }else{

            $this->db->delete($this->table,array('destination_id'=>$id));
            $this->db->delete($this->table2,array('destination_id'=>$id));
            
            $result['status'] = 'OK';
            $result['message'] = 'Delete data success';
        }
            
        return $result;
    }

    public function getDestination($post=array()){

        $results = array();

        if(!empty($post['id'])){

            $this->db
                ->select('a.destination_id as "idGudang", destination_name as "namaGudang"')
                ->from($this->table2.' a')
                ->join($this->table.' b',' b.destination_id=a.destination_id')
                ->where('a.document_id', $post['id']);

            $data = $this->db->get()->result_array();

            if($data){
                $results['data'] = $data;
                $results['status'] = 'OK';
            }else{
                $results['status'] = 'ERR';
                $results['message'] = 'Destination Not Available';
            }

        }else{
            $results['status'] = 'OK';
            $results['data'] = array();
        }

        return $results;
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->select('count(*) as total')
                ->from('destination')
                ->where('LOWER(destination_code)', strtolower($data[$i]['dest_initial']))
                ->or_where('LOWER(destination_name)', strtolower($data[$i]['dest_name']));

            $check = $this->db->get()->row()->total;

            if($check > 0){
                continue;
            }else{

                $inserts[] = array(
                    'destination_code'          => $data[$i]['dest_initial'],
                    'destination_name'          => $data[$i]['dest_name']
                );

            }

        }

        if(!$inserts){
            $this->db->trans_complete();
            return false;
        }

        $this->db->insert_batch($this->table,$inserts);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

}

?>
<?php
class warehouse_transfer_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getList($post = array()){

		$result = array();

		$columns = array(
			1 => 'outbound_document',
			2 => 'warehouse_from',
			3 => 'warehouse_destination',
			4 => 'serial_number',
			5 => 'item_name',
			6 => 'status'
		);

		$requestData = $_REQUEST;

		$where = '';

		if(!empty($post['sn'])){
			$where .= " AND pr.unique_code = '".$post['sn']."'";
		}

		if(!empty($post['item'])){
			$items = implode(",", $post['item']);
			$where .= " AND pr.item_id IN ($items)";
		}

		if(!empty($post['from']) && !empty($post['to'])){

			$where .= " AND outb.date >= '".DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d')."'";
			$where .= " AND outb.date <= '".DateTime::createFromFormat('d/m/Y', $post['to'])->modify('+1 day')->format('Y-m-d')."'";

		}

		if(!empty($post['warehouse'])){
			$where .= " AND outb.warehouse_id = '".$post['warehouse_id']."'";
		}

		$sql = "
			SELECT
				outb.code as outbound_document,
				whf.name as warehouse_from,
				whd.name as warehouse_destination,
				pr.unique_code as serial_number,
				CONCAT(itm.code, ' - ', itm.name) as item_name,
				COALESCE(inb.unique_code,'FALSE') as status
			FROM
				outbound outb
			JOIN
				picking_list_outbound plo ON plo.id_outbound=outb.id
			JOIN
				picking_recomendation pr ON pr.picking_id=plo.pl_id
			JOIN
				warehouses whf ON whf.id=outb.warehouse_id
			JOIN
				warehouses whd ON whd.id=outb.destination_id
			LEFT JOIN
				(
					SELECT 
						inb.code, ird.unique_code
					FROM
						inbound inb
					JOIN
						receivings r ON r.po_id=inb.id_inbound
					JOIN
						item_receiving ir ON ir.receiving_id=r.id
					JOIN
						item_receiving_details ird ON ird.item_receiving_id=ir.id
					WHERE
						inb.document_id=3
				) as inb ON inb.code=outb.code and pr.unique_code=inb.unique_code
			LEFT JOIN
				items itm ON itm.id=pr.item_id
			WHERE
				outb.document_id=3
			$where
				";

		$totalData = $this->db->query($sql)->num_rows();

		$sql .= " ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$sql .= " LIMIT ".$requestData['length']." OFFSET ".$requestData['start'];


		$row = $this->db->query($sql)->result_array();

		$totalFiltered = count($row);

		$data = array();
		$action = '';

		for($i=0;$i<$totalFiltered;$i++){

			$status = "Done";
			if($row[$i]['status'] == 'FALSE'){
				$status = "In Transit";
			}

			$nested = array();
			$nested[] = $i+1;
			$nested[] = $row[$i]['outbound_document'];
			$nested[] = $row[$i]['warehouse_from'];
			$nested[] = $row[$i]['warehouse_destination'];
			$nested[] = $row[$i]['serial_number'];
			$nested[] = $row[$i]['item_name'];
			$nested[] = $status;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function export_excel($post = array()){

		$result = array();

		$where = '';

		if(!empty($post['sn'])){
			$where .= " AND pr.unique_code = '".$post['sn']."'";
		}

		if(!empty($post['item'])){
			$items = implode(",", $post['item']);
			$where .= " AND pr.item_id IN ($items)";
		}

		if(!empty($post['from']) && !empty($post['to'])){

			$where .= " AND outb.date >= '".DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d')."'";
			$where .= " AND outb.date <= '".DateTime::createFromFormat('d/m/Y', $post['to'])->modify('+1 day')->format('Y-m-d')."'";

		}

		if(!empty($post['warehouse'])){
			$where .= " AND outb.warehouse_id = '".$post['warehouse_id']."'";
		}

		$sql = "
			SELECT
				outb.code as outbound_document,
				whf.name as warehouse_from,
				whd.name as warehouse_destination,
				pr.unique_code as serial_number,
				CONCAT(itm.code, ' - ', itm.name) as item_name,
				(CASE WHEN inb.unique_code IS NULL THEN 'In Transit' ELSE 'Done' END) as status
			FROM
				outbound outb
			JOIN
				picking_list_outbound plo ON plo.id_outbound=outb.id
			JOIN
				picking_recomendation pr ON pr.picking_id=plo.pl_id
			JOIN
				warehouses whf ON whf.id=outb.warehouse_id
			JOIN
				warehouses whd ON whd.id=outb.destination_id
			LEFT JOIN
				(
					SELECT 
						inb.code, ird.unique_code
					FROM
						inbound inb
					JOIN
						receivings r ON r.po_id=inb.id_inbound
					JOIN
						item_receiving ir ON ir.receiving_id=r.id
					JOIN
						item_receiving_details ird ON ird.item_receiving_id=ir.id
					WHERE
						inb.document_id=3
				) as inb ON inb.code=outb.code and pr.unique_code=inb.unique_code
			LEFT JOIN
				items itm ON itm.id=pr.item_id
			WHERE
				outb.document_id=3
			$where
				";

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	
}
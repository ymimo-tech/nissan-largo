<?php
class item_stok_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'items';
    private $table2 = 'categories';
    private $table3 = 'units';
    private $table4 = 'receivings';
    private $table5 = 'item_receiving_details';
    private $table6 = 'suppliers';
    private $loc     = 'locations';

    function __rev1(){
        $this->db->from('temp_barang');
        $sql = $this->db->get();
        foreach ($sql->result() as $row){
            $data = array('jumlah_awal'=>$row->stok);
            $where = array('id_barang'=>$row->id);
            $this->db->update('barang',$data,$where);
        }
    }

	public function getStockCardTotal($id){
		$result = array();

		$sql = "SELECT COUNT(*) AS total
				FROM
					(
						(
							SELECT
								TO_CHAR(finish_putaway, 'DD/MM/YYYY') AS date,
								finish_putaway AS date_sort,
								(
									SELECT
										COUNT(*)
									FROM
										item_receiving_details irds
									JOIN item_receiving irs
										ON irs.id=irds.item_receiving_id
									JOIN items itms
										ON itms.id=irs.item_id
									WHERE
										irs.receiving_id=rcv.id
									AND
										irs.item_id=itm.id
								) AS qty, 'IN' AS type, user_name, rcv.code AS doc,
								rcv.id AS id_doc
							FROM
								receivings rcv
							JOIN item_receiving ir
								ON ir.receiving_id=rcv.id
							JOIN item_receiving_details ird
								ON ird.item_receiving_id=ir.id
							JOIN items itm
								ON itm.id=ir.item_id
							LEFT JOIN users usr
								ON usr.id=rcv.user_id
							WHERE
								ir.item_id=$id
						)
						UNION
						(
							SELECT
								TO_CHAR(shipping_finish, 'DD/MM/YYYY') AS date,
								shipping_finish AS date_sort,
								(
									SELECT
										COUNT(*)
									FROM
										item_receiving_details irds
									JOIN item_receiving irs
										ON irs.id=irds.item_receiving_id
									JOIN items itms
										ON itms.id=irs.item_id
									WHERE
										irds.shipping_id=shipp.shipping_id
									AND
										irs.item_id=itm.id
								) AS qty, 'OUT' AS type, user_name, shipp.code AS doc,
								shipp.shipping_id AS id_doc
							FROM
								shippings shipp
							JOIN item_receiving_details ird
								ON ird.shipping_id=shipp.shipping_id
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN items itm
								ON itm.id=ir.item_id
							LEFT JOIN users usr
								ON usr.id=shipp.user_id
							WHERE
								ir.item_id=$id
						)
					) AS tbl";

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

	public function getStockCard($post = array()){
		$result = array();

		$offset = 10;
		$limit = ' 0, ' . $offset;

		$where = "";

		if(isset($post['start'])){
			if(!empty($post['start']))
				$limit = ' ' . $post['start'].', '.$offset;
		}

		if(!empty($post['warehouse'])){

			if(is_array($post['warehouse'])){
				$wh = implode(',', $post['warehouse']);
			}else{
				$wh = $post['warehouse'];
			}

			$where .= "AND wh.id IN ($wh)";

		}

		$idBarang = $post['id_barang'];

		$userId = $this->session->userdata('user_id');

		$sql = "SELECT *
				FROM
					(
						(
							SELECT
								COALESCE(TO_CHAR(rcv.date, 'DD/MM/YYYY HH24:MI:SS'), '-') AS date,
								ird.putaway_time AS date_sort,
								COALESCE(SUM(first_qty),0) AS qty,
								'IN' AS type, COALESCE(user_name, '-') AS user_name,
								rcv.code as doc,
								rcv.id  AS id_doc,
								'' AS balance,
								'RCV' as type_doc,
								COALESCE(NULL,0) as system_qty,
								COALESCE(NULL,0) as scanned_qty
							FROM
								item_receiving_details ird
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN items itm
								ON itm.id=ir.item_id
							JOIN receivings rcv
								ON rcv.id=ir.receiving_id
							JOIN hp_receiving_inbound hri
								ON hri.receiving_id=rcv.id
							JOIN inbound inb
								ON inb.id_inbound=hri.inbound_id
							JOIN warehouses wh
								ON wh.id=inb.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							LEFT JOIN users usr
								ON usr.id=rcv.user_id
							WHERE
								ir.item_id=$idBarang
							$where
							group by
								rcv.id, rcv.date, usr.user_name, rcv.code, usr.user_name, ird.putaway_time
						)
						UNION
						(
							SELECT
								TO_CHAR(COALESCE(pl.date), 'DD/MM/YYYY HH24:MI:SS') AS date,
								pl.start_time AS date_sort,
								COALESCE(SUM(pr.qty),0) AS qty,
								'OUT' AS type, COALESCE(user_name, '-') AS user_name, pl.name as doc,
								pl.pl_id  AS id_doc, '' AS balance, 'PICK' as type_doc,
								COALESCE(NULL,0) as system_qty, COALESCE(NULL,0) as scanned_qty
							FROM
								pickings pl
							JOIN (SELECT pl_id, min(id_outbound) as id_outbound FROM picking_list_outbound GROUP BY pl_id) as plo
								ON plo.pl_id=pl.pl_id
							JOIN outbound outb
								ON outb.id=plo.id_outbound
							JOIN warehouses wh
								ON wh.id=outb.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							JOIN picking_recomendation pr
								ON pr.picking_id=pl.pl_id
							JOIN item_receiving_details ird
								ON ird.unique_code=pr.unique_code
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN items itm
								ON itm.id=ir.item_id
							LEFT JOIN users usr
								ON usr.id=pl.user_id
							WHERE
								ir.item_id=$idBarang
							$where
							group by
								pl.pl_id, usr.user_name, pl.end_time, pl.start_time, pl.name, pl.date
						)
						UNION
						(
							SELECT
								TO_CHAR(COALESCE(ro.date), 'DD/MM/YYYY HH24:MI:SS') AS date,
								ro.date AS date_sort,
								COALESCE(SUM(rop.qty),0) AS qty,
								'OUT' AS type, COALESCE(user_name, '-') AS user_name, ro.code as doc,
								ro.id  AS id_doc, '' AS balance, 'REPACK' as type_doc,
								COALESCE(NULL,0) as system_qty, COALESCE(NULL,0) as scanned_qty
							FROM
								repack_orders ro
							JOIN warehouses wh
								ON wh.id=ro.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							JOIN repack_order_picking rop
								ON rop.repack_order_id=ro.id
							JOIN item_receiving_details ird
								ON ird.unique_code=rop.unique_code
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN items itm
								ON itm.id=ir.item_id
							LEFT JOIN users usr
								ON usr.id=ro.user_id
							WHERE
								ir.item_id=$idBarang
							$where
							group by
								ro.id, usr.user_name, ro.date, rop.qty, ro.code
						)UNION(
							SELECT
								TO_CHAR(COALESCE(cc.time), 'DD/MM/YYYY HH24:MI:SS') AS date,
								cc.time AS date_sort,
								COALESCE(SUM(ccr.qty),0) AS qty,
								'IN' AS type, COALESCE(user_name, '-') AS user_name, cc.code as doc,
								cc.cc_id  AS id_doc, '' AS balance, 'CC' as type_doc,
								COALESCE(sum(system_qty),0) as system_qty, COALESCE(sum(scanned_qty),0) as scanned_qty
							FROM
								cycle_counts cc
							JOIN warehouses wh
								ON wh.id=cc.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
							JOIN items itm
								ON itm.id=ccr.item_id
							JOIN users usr
								ON usr.id=cc.user_id
							Where
								ccr.item_id=$idBarang
							AND
								ccr.status='ADJUST TO SCANNED QTY'
							$where
							GROUP BY
								cc.cc_id, usr.user_name, ccr.system_qty, ccr.scanned_qty, cc.time, cc.code
							HAVING
								(system_qty < scanned_qty)
						)UNION(
							SELECT
								TO_CHAR(COALESCE(cc.time), 'DD/MM/YYYY HH24:MI:SS') AS date,
								cc.time AS date_sort,
								COALESCE(SUM(ccr.qty),0) AS qty,
								'IN' AS type, COALESCE(user_name, '-') AS user_name, cc.code as doc,
								cc.cc_id  AS id_doc, '' AS balance, 'CC' as type_doc,
								COALESCE(sum(system_qty),0) as system_qty, COALESCE(sum(scanned_qty),0) as scanned_qty
							FROM
								cycle_counts cc
							JOIN warehouses wh
								ON wh.id=cc.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
							JOIN items itm
								ON itm.id=ccr.item_id
							JOIN users usr
								ON usr.id=cc.user_id
							Where
								ccr.item_id=$idBarang
							AND
								ccr.status='ADJUST TO SCANNED QTY'
							$where
							GROUP BY
								cc.cc_id, usr.user_name, ccr.system_qty, ccr.scanned_qty, cc.time, cc.code
							HAVING
								(system_qty > scanned_qty)
						)
					) AS tbl
					ORDER BY
						date_sort DESC";

		$row = $this->db->query($sql)->result_array();

		$this->db->select(array("name as nama_barang","code as kd_barang"));
		$this->db->from('items');
		$this->db->where('id', $post['id_barang']);

		$item = $this->db->get()->row_array();

		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['item_name'] = $item['nama_barang'];
		$result['item_code'] = $item['kd_barang'];

		return $result;
	}

	public function getLowQty($params=array()){
		$result = array();

		$where = "";
		$userId = $this->session->userdata('user_id');

		if(!empty($params['warehouse'])){
			if(is_array($params['warehouse'])){
				$wh = implode(',', $params['warehouse']);
			}else{
				$wh = $params['warehouse'];
			}

			$where .= "AND wh.id IN ($wh)";
		}

		$sql = "
				SELECT COALESCE(tbl.total,0) as total
				FROM
				(
					SELECT
						COUNT(*) AS total
					FROM
						items itm
					JOIN items_warehouses itmwh
						ON itmwh.item_id=itm.id
					JOIN warehouses wh
						ON wh.id=itmwh.warehouse_id
					JOIN users_warehouses usrwh
						ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
					LEFT JOIN
						(
								SELECT ird.item_id, sum(ird.last_qty) AS qty
						FROM
							item_receiving_details ird
						JOIN locations loc
							ON loc.id=ird.location_id
						JOIN location_areas loc_area
							ON loc_area.id=loc.location_area_id
						JOIN warehouses wh
							ON wh.id=loc_area.warehouse_id
						JOIN users_warehouses usrwh
							ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
						$where
						GROUP BY ird.item_id
						) as tbl
						ON tbl.item_id=itm.id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					WHERE
						COALESCE(tbl.qty, 0) <= minimum_stock
					$where
					GROUP BY wh.id
				) as tbl
			";

		$row = $this->db->query($sql)->row_array();

		$result = (!empty($row)) ? $row['total'] : 0;

		return $result;
	}

	public function update_lok_outbound(){

		$this->db->start_cache();
		$this->db->where('location_id',101);
		$this->db->where('last_qty >',1);
		$this->db->stop_cache();


		$this->db->select('unique_code');
		$this->db->from('item_receiving_details');
		$query = $this->db->get();

		foreach ($query->result() as $row) {

			$data_where = array('unique_code' => $row->unique_code);
			$data_update = array('location_id' => 103);
			$this->db->update('item_receiving_details',$data_update);

		}

		$this->db->flush_cache();

	}

	public function getListQty($post = array()){
		$result = array();

		$userId = $this->session->userdata('user_id');

		$where = '';
		if(!empty($post['id_barang'])){
			$where .= ' AND ir.item_id='.$post['id_barang'];
		}

		if(!empty($post['warehouse'])){

			if(is_array($post['warehouse'])){
				$wh = implode(',', $post['warehouse']);
			}else{
				$wh = $post['warehouse'];
			}

			$where .= " AND wh.id IN ($wh)";

		}

		$sql = "SELECT
				COALESCE((
					SELECT
						COALESCE(sum((CASE r.is_cross_doc WHEN 0 THEN ird.first_qty ELSE ird.last_qty END)),0)
					FROM
						item_receiving_details ird
					JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					JOIN receivings r
						ON r.id=ir.receiving_id
					JOIN hp_receiving_inbound hpi
						ON hpi.receiving_id=r.id
					JOIN inbound inb
						ON inb.id_inbound=hpi.inbound_id
					JOIN warehouses wh
						ON wh.id=inb.warehouse_id
					JOIN users_warehouses uw
						ON uw.warehouse_id=wh.id
					WHERE
						uw.user_id = $userId
					AND
						location_id IN (100,107)
					$where
				),0) AS rec_qty,
				COALESCE((
					SELECT
						sum(last_qty)AS onhand_qty
					FROM
						item_receiving_details ird
					JOIN locations loc ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					left JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					left JOIN users_warehouses uw ON wh.id=uw.warehouse_id AND uw.user_id=$userId
					JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					JOIN items itm
						ON itm.id=ir.item_id
					WHERE
						location_id NOT IN (100,101,104)
					AND
						(CASE WHEN itm.has_qty=1 THEN ird.last_qty > 0 ELSE ird.picking_id IS NULL END)
					$where
				),0	) AS onhand_qty,
				COALESCE( (
					SELECT
						sum(pr.qty)
					FROM
						picking_recomendation pr
					JOIN (SELECT pl_id, min(id_outbound) as id_outbound FROM picking_list_outbound GROUP BY pl_id) as plo
						ON plo.pl_id=pr.picking_id
					JOIN outbound outb
						ON outb.id=plo.id_outbound
					JOIN warehouses wh
						ON wh.id=outb.warehouse_id
					JOIN users_warehouses uw
						ON uw.warehouse_id=wh.id
					LEFT JOIN item_receiving_details ird
						ON ird.unique_code=pr.unique_code
					LEFT JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					WHERE
						uw.user_id = $userId
					AND
						pr.location_id <> 104
					$where
				),0) AS allocated_qty,
				COALESCE((
					SELECT
						COALESCE(sum(last_qty),0)
					FROM
						item_receiving_details ird
					JOIN locations loc ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id AND uw.user_id=$userId
					JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					JOIN qc
						ON qc.id=ird.qc_id
					WHERE
						ird.qc_id IN(2,4)
					$where
				),0) AS suspend_qty,
				COALESCE((
					SELECT
						COALESCE(SUM(last_qty)) AS damage_qty
					FROM
						item_receiving_details ird
					JOIN locations loc ON loc.id=ird.location_id
					JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
					JOIN warehouses wh ON wh.id=loc_area.warehouse_id
					JOIN users_warehouses uw ON wh.id=uw.warehouse_id AND uw.user_id=$userId
					JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					JOIN qc
						ON qc.id=ird.qc_id
					WHERE
						ird.qc_id IN (3)
					$where
				),0) AS damage_qty";

		$row = $this->db->query($sql)->row_array();
		$len = count($row);

		if($len > 0){

			$recQty = $row['rec_qty'];
			$onhandQty = $row['onhand_qty'] + $row['allocated_qty'];
			$allocQty = $row['allocated_qty'];
			$spnQty = $row['suspend_qty'];
			$dmgQty = $row['damage_qty'];
			// $availQty = $row['available_qty'];
			$availQty = $onhandQty - ($allocQty + $spnQty + $dmgQty);
			$availQty = ($availQty > 0) ? $availQty : 0;
		}else{

			$recQty = 0;
			$onhandQty = 0;
			$allocQty = 0;
			$spnQty = 0;
			$dmgQty = 0;
			$availQty = 0;

		}

		if($recQty > 0)
			$recQty = '<a href="javascript:;" data-params="receiving" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$recQty.'</a>';

		if($onhandQty > 0)
			$onhandQty = '<a href="javascript:;" data-params="onhand" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$onhandQty.'</a>';

		if($allocQty > 0)
			$allocQty = '<a href="javascript:;" data-params="allocated" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$allocQty.'</a>';

		if($spnQty > 0)
			$spnQty = '<a href="javascript:;" data-params="suspend" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$spnQty.'</a>';

		if($availQty > 0)
			$availQty = '<a href="javascript:;" data-params="available" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$availQty.'</a>';

		if($dmgQty > 0)
			$dmgQty = '<a href="javascript:;" data-params="damage" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$dmgQty.'</a>';

		$result['rec_qty'] = $recQty;
		$result['onhand_qty'] = $onhandQty;
		$result['allocated_qty'] = $allocQty;
		$result['suspend_qty'] = $spnQty;
		$result['available_qty'] = $availQty;
		$result['damage_qty'] = $dmgQty;
		$result['total_qty'] = $row['onhand_qty'] + $row['rec_qty'];

		return $result;
	}

	public function getListQtyDataTable($post = array()){

		//THIS METHOD NOT UPDATED

		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'rec_qty',
			2 => 'onhand_qty',
			3 => 'allocated_qty',
			4 => 'suspend_qty',
			5 => '(onhand.onhand_qty - pick.allocated_qty - suspend.suspend_qty)'
		);

		$where = '';
		$where1 = '';
		$wheres = '';
		if(!empty($post['id_barang'])){
			$where1 .= ' AND tbl.id_barang = \''.$post['id_barang'].'\' ';
			$wheres .= ' AND r.id_barang = \''.$post['id_barang'].'\' ';
			$where .= ' AND rcvbrg.id_barang = \''.$post['id_barang'].'\' ';
		}

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
				(
					SELECT
						COUNT(*)
					FROM
					(
						SELECT
							rcvbrg.id_barang
						FROM
							receiving_barang rcvbrg
						LEFT JOIN
						(
							SELECT
								id_barang, COUNT(*) AS rec_qty
							FROM
								m_loc loc
							JOIN receiving_barang r
								ON r.loc_id=loc.loc_id
							WHERE
								loc.loc_type IN('.$this->config->item('rec_location_qty').')'.$wheres.'
						) AS rec
							ON rec.id_barang=rcvbrg.id_barang
						LEFT JOIN
						(
							SELECT
								id_barang, COUNT(*) AS onhand_qty
							FROM
								m_loc loc
							JOIN receiving_barang r
								ON r.loc_id=loc.loc_id
							JOIN receiving rcv
								ON rcv.id_receiving=r.id_receiving
							WHERE
								loc.loc_type IN('.$this->config->item('onhand_location_qty').')'.$wheres.'
						) AS onhand
							ON onhand.id_barang=rcvbrg.id_barang
						LEFT JOIN
						(
							SELECT
								id_barang, COUNT(*) AS allocated_qty
							FROM
								picking_list pl
							JOIN receiving_barang r
								ON r.pl_id=pl.pl_id
							WHERE r.loc_id != \'104\' AND r.loc_id != \'101\''.$wheres.'
						) AS pick
							ON pick.id_barang=rcvbrg.id_barang
						LEFT JOIN
						(
							SELECT
								id_barang, COUNT(*) AS suspend_qty
							FROM
								m_qc qc
							JOIN receiving_barang r
								ON r.id_qc=qc.id_qc
							WHERE
								r.id_qc = \'4\''.$wheres.'
						) AS suspend
							ON suspend.id_barang=rcvbrg.id_barang
					) AS tbl
					WHERE 1=1'.$where1.'
					GROUP BY
						tbl.id_barang
				) AS tbl2';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					IFNULL(rec.rec_qty, 0) AS rec_qty, IFNULL(onhand.onhand_qty, 0) AS onhand_qty,
					IFNULL(pick.allocated_qty, 0) AS allocated_qty, IFNULL(suspend.suspend_qty, 0) AS suspend_qty,
					IFNULL((onhand.onhand_qty - pick.allocated_qty - suspend.suspend_qty), 0) AS available_qty
				FROM
					receiving_barang rcvbrg
				LEFT JOIN
				(
					SELECT
						id_barang, COUNT(*) AS rec_qty
					FROM
						m_loc loc
					JOIN receiving_barang r
						ON r.loc_id=loc.loc_id
					JOIN receiving rcv
						ON rcv.id_receiving=r.id_receiving
					WHERE
						loc.loc_type IN('.$this->config->item('rec_location_qty').')'.$wheres.'
				) AS rec
					ON rec.id_barang=rcvbrg.id_barang
				LEFT JOIN
				(
					SELECT
						id_barang, COUNT(*) AS onhand_qty
					FROM
						m_loc loc
					JOIN receiving_barang r
						ON r.loc_id=loc.loc_id
					WHERE
						loc.loc_type IN('.$this->config->item('onhand_location_qty').')
					AND
						r.id_qc != \'3\''.$wheres.'
				) AS onhand
					ON onhand.id_barang=rcvbrg.id_barang
				LEFT JOIN
				(
					SELECT
						id_barang, COUNT(*) AS allocated_qty
					FROM
						picking_list pl
					JOIN receiving_barang r
						ON r.pl_id=pl.pl_id
					WHERE r.loc_id != \'104\' AND r.loc_id != \'101\''.$wheres.'
				) AS pick
					ON pick.id_barang=rcvbrg.id_barang
				LEFT JOIN
				(
					SELECT
						id_barang, COUNT(*) AS suspend_qty
					FROM
						m_qc qc
					JOIN receiving_barang r
						ON r.id_qc=qc.id_qc
					WHERE
						r.id_qc = \'4\''.$wheres.'
				) AS suspend
					ON suspend.id_barang=rcvbrg.id_barang
				WHERE 1=1'.$where.'
				GROUP BY
					rcvbrg.id_barang';

		//$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$recQty = $row[$i]['rec_qty'];
			$onhandQty = $row[$i]['onhand_qty'];
			$allocQty = $row[$i]['allocated_qty'];
			$spnQty = $row[$i]['suspend_qty'];
			$availQty = $row[$i]['available_qty'];

			if($recQty > 0)
				$recQty = '<a href="javascript:;" data-params="receiving" alt="Click to see detail">'.$recQty.'</a>';

			if($onhandQty > 0)
				$onhandQty = '<a href="javascript:;" data-params="onhand" alt="Click to see detail">'.$onhandQty.'</a>';

			if($allocQty > 0)
				$allocQty = '<a href="javascript:;" data-params="allocated" alt="Click to see detail">'.$allocQty.'</a>';

			if($spnQty > 0)
				$spnQty = '<a href="javascript:;" data-params="suspend" alt="Click to see detail">'.$spnQty.'</a>';

			if($availQty > 0)
				$availQty = '<a href="javascript:;" data-params="available" alt="Click to see detail">'.$availQty.'</a>';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $recQty;
			$nested[] = $onhandQty;
			$nested[] = $allocQty;
			$nested[] = $spnQty;
			$nested[] = $availQty;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getStatus(){
		$result = array();

		$this->db
			->select('id as id_qc, code as kd_qc')
			->from('qc')
			->order_by('code');

		$result = $this->db->get();

		return $result->result_array();
	}

	public function getCategory(){
		$result = array();

		$sql = 'SELECT
					id_kategori, kd_kategori, CONCAT(kd_kategori, \' - \', nama_kategori) AS nama_kategori
				FROM
					kategori';

		$this->db
			->select([
					"id as id_kategori","code as kd_kategori","CONCAT(code, ' - ', name) as nama_kategori"
			])
			->from('categories');

		$result = $this->db->get();

		return $result->result_array();
	}

	public function getLocation(){

		$result = array();

		$this->db
			->select('id as loc_id, name as loc_name')
			->from('locations loc')
			->where('name IS NOT NULL', NULL)
			->where('loc_type <>','SENTOUT')
			->order_by('id');

		$result = $this->db->get();
		return $result->result_array();

	}

	public function getLocationType(){
		$result = array();

		$result = array(
			'INBOUND'	=> 'INBOUND',
			'TRANSIT'	=> 'TRANSIT',
			'BINLOC'	=> 'BINLOC',
			'OUTBOUND'	=> 'OUTBOUND',
			'QC'		=> 'QC',
			'NG'		=> 'NG',
		);

		return $result;
	}

	public function getUom(){
		$result = array();

		$this->db
			->select('id as id_satuan, name as nama_satuan')
			->from('units');

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getItem(){
		$result = array();

		$this->db
			->select(['id as id_barang',"CONCAT(code,' - ', name) as nama_barang"])
			->from('items');

		$result = $this->db->get()->result_array();

		return $result;
	}

    public function get_receiving_by_id_barang($id_barang){
		$condition['a.loc_id != \'104\''] = null;
		$condition['a.loc_id != \'101\''] = null;
        $condition['a.id_barang'] = $id_barang;
        return $this->data_detail($condition);
    }

    public function data($condition = array(),$tahun_aktif='') {

        $this->db->select("a.id as id_barang, a.name as nama_barang, a.code as kd_barang, b.code as kd_kategori, b.name as nama_kategori, c.name as nama_satuan, sh.code as shipment_type, a.fifo_period, a.has_qty, a.def_qty,
        	'' as kd_kategori_2, '' as nama_kategori_2", false);
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', 'b.id = a.category_id','left');
        $this->db->join($this->table3 . ' c', 'c.id = a.unit_id','left');
        $this->db->join('shipment sh','sh.id=a.shipment_id','left');
		$this->db->join('(SELECT ir.item_id , count(unique_code) as qty FROM item_receiving_details ird LEFT JOIN item_receiving ir ON ir.id=ird.item_receiving_id WHERE pl_status IS NULL and location_id NOT IN (104,101) GROUP BY ir.item_id) as rb ','a.id = rb.item_id','left');
        $this->db->order_by('a.code');
        $this->db->where_condition($condition);

        return $this->db;

    }

	public function getDetailList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		//NOTE :
		//CHANGE JOIN TO LEFT RECEIVING TO GET STOCK FROM CYCLE COUNT NOT FROM RECEIVING PROCESS

		$columns = array(
			0 => '',
			1 => '',
			2 => 'kd_unik',
			3 => 'kd_parent',
			4 => 'last_qty',
			5 => '',
			6 => 'tgl_exp',
			7 => 'tgl_in',
			8 => 'loc_name',
			9 => 'loc_type',
			10 => 'kd_qc'
		);

		$where = '';
		$params = '';

		$this->db->start_cache();

		if(!empty($post['id_barang'])){
			$this->db->where('ird.item_id',$post['id_barang']);
		}

		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if($params == 'receiving'){
			$this->db->where('loc.loc_type','INBOUND');
		}

		if($params == 'onhand'){
			$locType = "(loc.loc_type IS NULL OR loc.loc_type='QC' OR loc.loc_type='NG')";
			$this->db->where($locType, NULL);
		}

		if($params == 'allocated'){
		}

		if($params == 'suspend'){
			$this->db->where('ird.qc_id', 2);
		}

		if($params == 'damage'){
			$this->db->where('ird.qc_id', 3);
		}

		if($params == 'available'){
			$this->db->where('ird.qc_id', 1);
			$this->db->where('loc.loc_type', NULL);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select('DISTINCT(ird.unique_code) as kd_unik')
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id','left')
			->join('qc','qc.id=ird.qc_id','left')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('receivings r','r.id=ir.receiving_id','left')
			->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id','left')
			->join('inbound inb','inb.id_inbound=hpi.inbound_id','left')
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('(SELECT pl_id, min(id_outbound) as id_outbound FROM picking_list_outbound GROUP BY pl_id) as plo','plo.pl_id=p.pl_id','left')
			->join('outbound outb','outb.id=plo.id_outbound','left')
			->join('cycle_counts cc','cc.cc_id=ird.cycle_count_id','left')
			->join('repack_orders ro','ro.id=ird.repack_order_id','left')
			->join('warehouses wh',"wh.id=loc_area.warehouse_id OR (wh.id=inb.warehouse_id and loc.id IN (100,101,102,103,105,106,107))",'left')
			->join('items', 'items.id=ird.item_id', 'left')
			->join('units uom', 'uom.id=items.unit_id', 'left')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'),'left')

      ->where_not_in('ird.location_id', [101,104])
			->where('ird.last_qty>0',null)
			->group_by('ird.unique_code');

		$this->db->stop_cache();

		$row = $this->db->get();
		$totalData = $row->num_rows();

		// $this->db->select('inb.id_inbound, inb.code as kd_inbound, ro.id as id_order_kitting, ro.code as kd_order_kitting, r.id as id_receiving, cc.cc_id, cc.code as cc_code, loc.loc_type, ird.shipping_id, outb.id as id_outbound, outb.code as kd_outbound, p.pl_id, p.name as pl_name, sum(last_qty) as last_qty, loc.name as loc_name, parent_code as kd_parent, qc.code as kd_qc, ird.tgl_exp, ird.tgl_in');
		$this->db->select(["string_agg(DISTINCT CAST(inb.id_inbound AS VARCHAR),',') as id_inbound", "string_agg(DISTINCT CAST(inb.code AS VARCHAR),',') as kd_inbound", 'ro.id as id_order_kitting','ro.code as kd_order_kitting','r.id as id_receiving','cc.cc_id','cc.code as cc_code','loc.loc_type','ird.shipping_id','outb.id as id_outbound','outb.code as kd_outbound','p.pl_id','p.name as pl_name','last_qty','uom.name as uom','loc.name as loc_name','parent_code as kd_parent','qc.code as kd_qc','ird.tgl_exp','ird.tgl_in']);
		// $this->db->group_by("inb.id_inbound, inb.code, ro.id, ro.code, r.id, cc.cc_id, cc.code, loc.loc_type, ird.shipping_id, outb.id, outb.code, p.pl_id, p.name, last_qty, loc.name, parent_code, qc.code, ird.tgl_exp, ird.tgl_in");
		$this->db->group_by("ro.id, ro.code, r.id, cc.cc_id, cc.code, loc.loc_type, ird.shipping_id, outb.id, outb.code, p.pl_id, p.name, last_qty, uom.name, loc.name, parent_code, qc.code, ird.tgl_exp, ird.tgl_in");
		$this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
		$this->db->limit($requestData['length'],  $requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$this->db->flush_cache();

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			if($row[$i]['kd_inbound']){

	        	$outboundCode = explode(",", $row[$i]['kd_inbound']);
	        	$outboundId = explode(",", $row[$i]['id_inbound']);

	        	// if($outboundCode){
            if(count($outboundCode) > 1){
	        		$docRef = 'CONSOLIDATE';
					$idRef = 'CONSOLIDATE';
					$ctrlRef = 'inbound/detail/';
					$locType = $row[$i]['loc_type'];
	        	} else {
					$docRef = $row[$i]['kd_inbound'];
					$idRef = $row[$i]['id_inbound'];
					$ctrlRef = 'inbound/detail/';
					$locType = $row[$i]['loc_type'];
				}

	        }

			if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('inbound_name'))){

				$docRef = $row[$i]['kd_inbound'];
				$idRef = $row[$i]['id_inbound'];
				$ctrlRef = 'inbound/detail/';

				if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

					$docRef = $row[$i]['kd_order_kitting'];
					$idRef = $row[$i]['id_order_kitting'];
					$ctrlRef = 'order_kitting/detail/';

				}

			}else if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

				$docRef = $row[$i]['kd_order_kitting'];
				$idRef = $row[$i]['id_order_kitting'];
				$ctrlRef = 'order_kitting/detail/';

			}else if(!empty($row[$i]['cc_id']) && empty($row[$i]['id_receiving'])){

				$docRef = $row[$i]['cc_code'];
				$idRef = $row[$i]['cc_id'];
				$ctrlRef = 'cycle_count/detail/';

			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('outbound_name'))){

				$docRef = $row[$i]['kd_outbound'];
				$idRef = $row[$i]['id_outbound'];
				$ctrlRef = 'outbound/detail/';

			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('transit_location'))){

				$this->db
					->select('ir.receiving_id as id_receiving, picking_id as pl_id, shipping_id, location_id as loc_id')
					->from('item_receiving_details ird')
					->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
					->where('unique_code', $row[$i]['kd_unik']);

				$r = $this->db->get()->row_array();

				if(empty($r['pl_id'])){

					$docRef = $row[$i]['kd_inbound'];
					$idRef = $row[$i]['id_inbound'];
					$ctrlRef = 'inbound/detail/';

					$locType = $locType;

				}else if(!empty($r['cc_id']) && $r['id_qc'] == '4'){

					$docRef = $row[$i]['cc_code'];
					$idRef = $row[$i]['cc_id'];
					$ctrlRef = 'cycle_count/detail/';

				}else if(!empty($r['pl_id']) && empty($r['shipping_id'])){

					$docRef = $row[$i]['kd_outbound'];
					$idRef = $row[$i]['id_outbound'];
					$ctrlRef = 'outbound/detail/';

					$locType = $locType;

				}

			}

			$exp = '-';
			$dIn = '-';

			if(!empty($row[$i]['tgl_exp']) && $row[$i]['tgl_exp'] != '0000-00-00')
				$exp = date('d/m/Y', strtotime($row[$i]['tgl_exp']));

			if(!empty($row[$i]['tgl_in']) && $row[$i]['tgl_in'] != '0000-00-00 00:00:00')
				$dIn = date('d/m/Y', strtotime($row[$i]['tgl_in']));
			$kd_parent = !empty($row[$i]['kd_parent']) ? $row[$i]['kd_parent'] : '-';
			$nested = array();
			$nested[] = '<input type="checkbox" name="sn[]" value="'.$row[$i]['kd_unik'].'" data-lp="'.$kd_parent.'">';
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $kd_parent;
			$nested[] = $row[$i]['last_qty'].' '.$row[$i]['uom'];
			$nested[] = '<a href="'.base_url().$ctrlRef.$idRef.'">'.$docRef.'</a>';
			$nested[] = $exp;
			$nested[] = $dIn;
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $locType;
			$nested[] = $row[$i]['kd_qc'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailListExport($post=array()){

		$where = '';
		$params = '';

		if(!empty($post['id_barang'])){
			$this->db->where('ir.item_id',$post['id_barang']);
		}

		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if($params == 'receiving'){
			$this->db->where('loc.loc_type','INBOUND');
		}

		if($params == 'onhand'){
			$locType = "(loc.loc_type IS NULL OR loc.loc_type='QC' OR loc.loc_type='NG')";
			$this->db->where($locType, NULL);
		}

		if($params == 'allocated'){
		}

		if($params == 'suspend'){
			$this->db->where('ird.qc_id', 2);
		}

		if($params == 'damage'){
			$this->db->where('ird.qc_id', 3);
		}

		if($params == 'available'){
			$this->db->where('ird.qc_id', 1);
			$this->db->where('loc.loc_type', NULL);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db->select('rfid_tag as kd_unik, itm.code as kd_barang, inb.id_inbound, inb.code as kd_inbound, ro.id as id_order_kitting, ro.code as kd_order_kitting, r.id as id_receiving, cc.cc_id, cc.code as cc_code, loc.loc_type, ird.shipping_id, outb.id as id_outbound, outb.code as kd_outbound, p.pl_id, p.name as pl_name, last_qty, loc.name as loc_name, parent_code as kd_parent, qc.code as kd_qc, ird.rfid_tag');

		$this->db
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('qc','qc.id=ird.qc_id','left')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('items itm','itm.id=ird.item_id','left')
			->join('receivings r','r.id=ir.receiving_id','left')
			->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id','left')
			->join('inbound inb','inb.id_inbound=hpi.inbound_id','left')
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id','left')
			->join('outbound outb','outb.id=plo.id_outbound','left')
			->join('cycle_counts cc','cc.cc_id=ird.cycle_count_id','left')
			->join('repack_orders ro','ro.id=ird.repack_order_id','left')
			->where_not_in('ird.location_id', [101,104]);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$docRef = $row[$i]['kd_inbound'];
			$locType = $row[$i]['loc_type'];

			if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('inbound_name'))){

				$docRef = $row[$i]['kd_inbound'];

				if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

					$docRef = $row[$i]['kd_order_kitting'];

				}

			}else if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

				$docRef = $row[$i]['kd_order_kitting'];

			}else if(!empty($row[$i]['cc_id']) && empty($row[$i]['id_receiving'])){

				$docRef = $row[$i]['cc_code'];

			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('outbound_name'))){

				$docRef = $row[$i]['kd_outbound'];

			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('transit_location'))){

				$sql = 'SELECT
							id_receiving, cc_id, pl_id, shipping_id, id_qc, loc_id
						FROM
							receiving_barang
						WHERE
							kd_unik = \''.$row[$i]['kd_unik'].'\'';

				$r = $this->db->query($sql)->row_array();

				if(empty($r['pl_id'])){

					$docRef = $row[$i]['kd_inbound'];

					$locType = $locType;

				}else if(!empty($r['cc_id']) && $r['id_qc'] == '4'){

					$docRef = $row[$i]['cc_code'];

				}else if(!empty($r['pl_id']) && empty($r['shipping_id'])){

					$docRef = $row[$i]['kd_outbound'];

					$locType = $locType;

				}

			}

			$exp = '-';
			$dIn = '-';

			if(!empty($row[$i]['tgl_exp']) && $row[$i]['tgl_exp'] != '0000-00-00'){
				$exp = date('d/m/Y', strtotime($row[$i]['tgl_exp']));
			}

			if(!empty($row[$i]['tgl_in']) && $row[$i]['tgl_in'] != '0000-00-00 00:00:00'){
				$dIn = date('d/m/Y', strtotime($row[$i]['tgl_in']));
			}

			$kd_parent = !empty($row[$i]['kd_parent']) ? $row[$i]['kd_parent'] : '-';

			$nested = array();

			$nested[] = $i + 1;
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $kd_parent;
			$nested[] = $row[$i]['last_qty'];
			$nested[] = $docRef;
			$nested[] = $exp;
			$nested[] = $dIn;
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $locType;
			$nested[] = $row[$i]['kd_qc'];
			$nested[] = $row[$i]['rfid_tag'];

			$data[] = $nested;
		}

		return $data;

	}

	public function getList($post=[]){

		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'nama_kategori',
			4 => 'nama_satuan',
			5 => 'loc_name',
			6 => 'loc_type',
			7 => 'qty',
			8 => 'minimum_stock'
		);

		$params = '';
		$getQty = false;
		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if($params == 'receiving'){

			$this->db->where_in('l.id',[100,107]);
			// $this->db->where('l.loc_type','INBOUND');
		}

		if($params == 'onhand'){

			$this->db->where_not_in('l.id',[100,107]);

			// $locType = "(l.loc_type IS NULL OR l.loc_type='QC' OR l.loc_type='NG')";
			// $this->db->where($locType, NULL);
			// $this->db->select(["COALESCE( (sum(ird.last_qty) + COALESCE(sum(pr.qty),0) ),0) as qty"]);

			// $getQty = true;

		}

		if($params == 'allocated'){

			$this->db->select(["COALESCE(sum(pr.qty),0) as qty"]);
				$this->db->where('pr.st_shipping IS NULL or pr.st_shipping = 0',NULL);

			$getQty = true;

		}

		if($params == 'suspend'){
			$this->db->where('ird.qc_id', 2);
		}

		if($params == 'damage'){
			$this->db->where('ird.qc_id', 3);
		}

		if($params == 'available'){
			$this->db->where('ird.qc_id', 1);
			$this->db->where('l.loc_type', NULL);
			$this->db->where_not_in('l.id',[100,107]);
		}

		if(!empty($post['id_qc'])){
			$this->db->where_in('qc_id', $post['id_qc']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		if(!$getQty){
			$this->db->select(["COALESCE( (sum(ird.last_qty) + COALESCE(sum(pr.qty),0) ),0) as qty"]);
		}

		if(!empty($post['id_qc'])){
			$this->db->where_in('ird.qc_id',$post['id_qc']);
		}

		if(!empty($post['loc_id'])){
			$this->db->where_in('ird.location_id',$post['loc_id']);
		}

		if(!empty($post['loc_type'])){
			$this->db->where_in('l.loc_type',$post['loc_type']);
		}

		$this->db
			->select([
				"ird.item_id","SUM(COALESCE(p.pl_id, 0)) as pl_id",
				"string_agg(DISTINCT CAST(qc_id as VARCHAR), ',') as id_qc_all",
				"string_agg(DISTINCT CAST(ird.location_id as VARCHAR), ', ') as loc_id_all",
				"string_agg(DISTINCT l.loc_type, ', ') as loc_type_all",
				"string_agg(DISTINCT l.name, ',') as loc_name"
			])
			->from('item_receiving_details ird')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('receivings r','r.id=ir.receiving_id','left')
			->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id','left')
			->join('inbound inb','inb.id_inbound=hpi.inbound_id','left')
			->join("(
					SELECT
						pr.st_shipping, pr.unique_code, pr.location_id, o.warehouse_id, COALESCE(SUM(pr.qty),0) as qty
					FROM
						picking_recomendation pr
					JOIN pickings p
						ON p.pl_id=pr.picking_id
					JOIN (SELECT pl_id, min(id_outbound) as id_outbound FROM picking_list_outbound GROUP BY pl_id) as plo
						ON plo.pl_id=p.pl_id
					JOIN outbound o
						ON o.id=plo.id_outbound
					WHERE
						pr.location_id <> 104
					GROUP BY pr.unique_code, pr.location_id, o.warehouse_id, pr.st_shipping
				) as pr
				",'pr.unique_code=ird.unique_code','left')
			->join('locations l','l.id=ird.location_id')
			->join('location_areas loc_area','loc_area.id=l.location_area_id','left')
			->join('warehouses wh','wh.id=loc_area.warehouse_id and l.id NOT IN (100,101,102,103,105,106,107) or l.id IN (100,101,102,103,105,106,107) and wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('items itm','itm.id=ird.item_id','left')
			->where_not_in('ird.location_id', [104])
			->group_by(['ird.item_id','itm.has_qty']);

		$rb = $this->db->get_compiled_select();

		$this->db->start_cache();

		if(!empty($post['id_barang']))
			$this->db->where_in('itm.id', $post['id_barang']);

		if(!empty($post['id_satuan']))
			$this->db->where_in('itm.unit_id', $post['id_satuan']);

		if(!empty($post['id_qc'])){
			$qcLen = count($post['id_qc']);

			$qc ='(';

			for ($i=0; $i < $qcLen; $i++) {
				if($i == 0)
					$qc .= " rb.id_qc_all like '%".$post['id_qc'][$i]."%'";
				else
					$qc .= " OR rb.id_qc_all like '%".$post['id_qc'][$i]."%'";
			}

			$qc .= ')';

			$this->db->where($qc, NULL);
		}

		if(!empty($post['id_kategori']))
			$this->db->where_in('itm.category_id', $post['id_kategori']);

		if(!empty($post['loc_id'])){
			$locId = $post['loc_id'];
			$len = count($locId);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_id_all', $locId[$i]);
			}
		}

		if(!empty($post['loc_type'])){
			$locType = $post['loc_type'];
			$len = count($locType);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_type_all', $locType[$i]);
			}
		}

		if($params != ''){
			if($params == 'low'){
				$this->db->where("COALESCE(rb.qty,0) <= minimum_stock", NULL);
			}else{
				$this->db->where("rb.qty >", 0);
			}
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select(["itm.id as id_barang", "itm.name as nama_barang", "itm.code as kd_barang", "cat.name as nama_kategori", "unt.name as nama_satuan", "minimum_stock", "loc_name","loc_type_all","COALESCE(rb.qty, 0) as qty"])
			->from('items itm')
			->join('items_warehouses iw','iw.item_id=itm.id')
			->join('warehouses wh','wh.id=iw.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('categories cat','cat.id=itm.category_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->join("($rb) as rb",'rb.item_id=itm.id','left')
			->group_by('itm.id, itm.name, itm.code, itm.minimum_stock, cat.name, unt.name, rb.loc_name, rb.loc_type_all, rb.qty');

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
		$this->db->limit($requestData['length'],  $requestData['start']);

		$row = $this->db->get()->result_array();

		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'item_stok/detail/'.$row[$i]['id_barang'].'">' . $row[$i]['kd_barang'] . '</a>';
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['nama_kategori'];
			$nested[] = $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['loc_type_all'];
			$nested[] = $row[$i]['qty'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['minimum_stock'] . ' ' . $row[$i]['nama_satuan'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;

	}

	public function getListRaw($post = array()){
		$result = array();

		$params = '';
		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if($params == 'receiving'){
			$this->db->where('l.loc_type','INBOUND');
		}

		if($params == 'onhand'){
			$locType = "(l.loc_type IS NULL OR l.loc_type='QC' OR l.loc_type='NG')";
			$this->db->where($locType, NULL);
		}

		if($params == 'allocated'){
		}

		if($params == 'suspend'){
			$this->db->where('ird.qc_id', 2);
		}

		if($params == 'damage'){
			$this->db->where('ird.qc_id', 3);
		}

		if($params == 'available'){
			$this->db->where('ird.qc_id', 1);
			$this->db->where('l.loc_type', NULL);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select([
				"itm.id as item_id","SUM(COALESCE(p.pl_id, 0)) as pl_id",
				"string_agg(DISTINCT CAST(qc_id as CHAR), ',') as id_qc_all",
				"string_agg(DISTINCT CAST(location_id as CHAR), ', ') as loc_id_all",
				"string_agg(DISTINCT l.loc_type, ', ') as loc_type_all",
				"string_agg(DISTINCT l.name, ',') as loc_name",
				"COALESCE((CASE WHEN itm.has_qty=1 THEN sum(ird.last_qty) ELSE COUNT(ird.unique_code) END),0) as qty",
				"ird.unique_code"
			])
			->from('item_receiving_details ird')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('locations l','l.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=l.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('items itm','itm.id=ir.item_id','left')
			->where_not_in('ird.location_id', [101,104])
			->group_by('itm.id, has_qty, ird.unique_code');

		$rb = $this->db->get_compiled_select();

		$this->db->start_cache();

		if(!empty($post['id_barang']))
			$this->db->where_in('itm.id', $post['id_barang']);

		if(!empty($post['id_satuan']))
			$this->db->where_in('itm.unit_id', $post['id_satuan']);

		if(!empty($post['id_qc']))
			$this->db->where_in('rb.qc_id', $post['id_qc']);

		if(!empty($post['id_kategori']))
			$this->db->where_in('itm.category_id', $post['id_kategori']);

		if(!empty($post['loc_id'])){
			$locId = $post['loc_id'];
			$len = count($locId);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_id_all', $locId[$i]);
			}
		}

		if(!empty($post['loc_type'])){
			$locType = $post['loc_type'];
			$len = count($locType);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_type_all', $locType[$i]);
			}
		}

		if($params != ''){
			if($params == 'low'){
				$this->db->where("COALESCE(rb.qty,0) <= minimum_stock", NULL);
			}else{
				$this->db->where("rb.qty >", 0);
			}
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select(["itm.id as id_barang", "itm.name as nama_barang", "itm.code as kd_barang", "cat.name as nama_kategori", "unt.name as nama_satuan", "minimum_stock", "loc_name","loc_type_all","COALESCE(rb.qty, 0) as qty","rb.unique_code"])
			->from('items itm')
			->join('items_warehouses iw','iw.item_id=itm.id')
			->join('warehouses wh','wh.id=iw.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('categories cat','cat.id=itm.category_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->join("($rb) as rb",'rb.item_id=itm.id','left')
			->group_by('itm.id, cat.name, unt.name, rb.loc_name, rb.unique_code, rb.loc_type_all, rb.qty, itm.name, itm.code, itm.minimum_stock');

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getListRawFormat2($post = array()){
		$result = array();

		$params = '';
		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if($params == 'receiving'){
			$this->db->where('l.loc_type','INBOUND');
		}

		if($params == 'onhand'){
			$locType = "(l.loc_type IS NULL OR l.loc_type='QC' OR l.loc_type='NG')";
			$this->db->where($locType, NULL);
		}

		if($params == 'allocated'){
		}

		if($params == 'suspend'){
			$this->db->where('ird.qc_id', 2);
		}

		if($params == 'damage'){
			$this->db->where('ird.qc_id', 3);
		}

		if($params == 'available'){
			$this->db->where('ird.qc_id', 1);
			$this->db->where('l.loc_type', NULL);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select([
				"itm.id as item_id","SUM(COALESCE(p.pl_id, 0)) as pl_id",
				"string_agg(DISTINCT CAST(qc_id as CHAR), ',') as id_qc_all",
				"string_agg(DISTINCT CAST(location_id as CHAR), ', ') as loc_id_all",
				"string_agg(DISTINCT l.loc_type, ', ') as loc_type_all",
				"string_agg(DISTINCT l.name, ',') as loc_name",
				"COALESCE((CASE WHEN itm.has_qty=1 THEN sum(ird.last_qty) ELSE COUNT(ird.unique_code) END),0) as qty"
			])
			->from('item_receiving_details ird')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('locations l','l.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=l.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('items itm','itm.id=ir.item_id','left')
			->where_not_in('ird.location_id', [101,104])
			->group_by(['itm.id','has_qty','location_id']);

		$rb = $this->db->get_compiled_select();

		$this->db->start_cache();

		if(!empty($post['id_barang']))
			$this->db->where_in('itm.id', $post['id_barang']);

		if(!empty($post['id_satuan']))
			$this->db->where_in('itm.unit_id', $post['id_satuan']);

		if(!empty($post['id_qc']))
			$this->db->where_in('rb.qc_id', $post['id_qc']);

		if(!empty($post['id_kategori']))
			$this->db->where_in('itm.category_id', $post['id_kategori']);

		if(!empty($post['loc_id'])){
			$locId = $post['loc_id'];
			$len = count($locId);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_id_all', $locId[$i]);
			}
		}

		if(!empty($post['loc_type'])){
			$locType = $post['loc_type'];
			$len = count($locType);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_type_all', $locType[$i]);
			}
		}

		if($params != ''){
			if($params == 'low'){
				$this->db->where("COALESCE(rb.qty,0) <= minimum_stock", NULL);
			}else{
				$this->db->where("rb.qty >", 0);
			}
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select(["itm.id as id_barang", "itm.name as nama_barang", "itm.code as kd_barang", "cat.name as nama_kategori", "unt.name as nama_satuan", "minimum_stock", "loc_name","loc_type_all","COALESCE(rb.qty, 0) as qty"])
			->from('items itm')
			->join('items_warehouses iw','iw.item_id=itm.id')
			->join('warehouses wh','wh.id=iw.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('categories cat','cat.id=itm.category_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->join("($rb) as rb",'rb.item_id=itm.id','left')
			->group_by('itm.id, cat.name, unt.name, rb.loc_name, rb.loc_type_all, rb.qty, itm.name, itm.code, itm.minimum_stock')
			->order_by('itm.code');

		$result = $this->db->get()->result_array();

		return $result;
	}

    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        //-----------end filtering-------------------

        $this->db->select('*');
        $this->db->from('receiving_barang a');//receiving_barang
        $this->db->join($this->loc . ' b',' a.loc_id = b.loc_id','left');//receiving_barang
        $this->db->join("receiving c", "a.id_receiving = c.id_receiving");
		$this->db->join('m_qc d', 'd.id_qc = a.id_qc', 'left'); //adji
        $this->db->order_by('a.kd_unik ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }
    private function data_list($condition = array(),$tahun_aktif='') {
        $this->db->select('*');
        $this->db->from($this->table  . ' a');

        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_jumlah_awal($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_jumlah_son($condition = array()) {
        $this->db->select('sum(minus) as minus_son, sum(surplus) as surplus_son,id_barang');
        $this->db->from($this->table11  . ' a');
        $this->db->where_condition($condition);
        $this->db->group_by('id_barang');
        return $this->db;
    }

    public function get_by_id($id,$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $condition['a.id'] = $id;
        $this->data($condition,$tahun_aktif);
        return $this->db->get();
    }

    public function get_data($condition = array(),$tahun_aktif) {
        $this->data($condition,$tahun_aktif);
        return $this->db->get();
    }


    public function data_table() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["a.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["a.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"] = null;
        }

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        //-----------end tahunaktif-------------------

        // Total Record
        $total = $this->data($condition,$tahun_aktif)->count_all_results();

        // List Data

        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition,$tahun_aktif)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('item_stok/edit/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('item_stok/delete/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';
            //$jumlah = $value->jumlah_awal + $value->jumlah_masuk - $value->jumlah_keluar + $value->surplus_son - $value->minus_son;
             switch ($value->shipment_type) {
                case 'FIFON':
                    $shipment_type = 'FIFO WITHOUT HAS EXP DATE';
                    break;
                case 'FIFOY':
                    $shipment_type = 'FIFO AND HAS EXP DATE';
                    break;
                case 'FEFOY':
                    $shipment_type = 'FEFO AND HAS EXPIRE DATE';
                    break;
                default:
                    $shipment_type = '';
                    # code...
                    break;
            };/*(
                'FIFON'=>'FIFO WITHOUT HAS EXP DATE',
                'FIFOY'=>'FIFO AND HAS EXP DATE',
                'FEFOY'=>'FEFO AND HAS EXP DATE'
                );*/
            $rows[] = array(
                    'primary_key' =>$value->id_barang,
                    'kd_barang' => anchor(null, $value->kd_barang, array('id' => 'drildown_key_t_item_stok_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_item_stok', 'data-source' => base_url('item_stok/get_detail_item_stok/' . $id))) ,
                    'nama_barang' => anchor(null, $value->nama_barang, array('id' => 'drildown_key_t_item_stok_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_item_stok', 'data-source' => base_url('item_stok/get_detail_item_stok/' . $id))),
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'qty' => $value->qty
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }


    public function data_table_excel() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["a.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["a.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"] = null;
        }

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        //-----------end tahunaktif-------------------

        // Total Record
        $total = $this->data($condition,$tahun_aktif)->count_all_results();

        // List Data

        $data = $this->data($condition,$tahun_aktif)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;
            $action = '';
            $jumlah = $value->jumlah_awal + $value->jumlah_masuk - $value->jumlah_keluar + $value->surplus_son - $value->minus_son;
                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'jumlah'=>$jumlah,
                    'jumlah_po'=>$value->jumlah_po
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

     public function data_table_detail() {
        $id_barang = $this->input->post('id_barang');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();


        foreach ($data_detail->result() as $value) {
            $id = $value->id_receiving_barang;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">';
            if ($this->access_right->otoritas('print')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('item_stok/print_single_label/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            $status = $value->pl_status==1 ? 'alocated':'';
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_unik' => $value->kd_unik,
                    'loc_name' => $value->loc_name,
                    'exp_date' => $value->tgl_exp,
                    'status' => $status,
                    'action' => $action
                );

        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_bbm_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbm <= '$tahun_aktif-12-31' and e.tanggal_bbm >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbm($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbm');
        $data_detail_bbm = $this->data_detail_bbm($condition)->get();
        $rows = array();

        foreach ($data_detail_bbm->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbm),
                    'kd_bbm' => $value->kd_bbm,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_bbk_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbk <= '$tahun_aktif-12-31' and e.tanggal_bbk >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbk');
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbk),
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_po_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbk <= '$tahun_aktif-12-31' and e.tanggal_bbk >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbk');
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbk),
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_barang' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_barang' => $id));
    }

    public function options_filter($default = '--Pilih Jenis Perawatan--', $key = '') {
        $this->db->order_by('a.id_barang');
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->jenis_perawatan;
        }
        return $options;
    }


    public function options($default = '--Choose Item--', $key = '') {
        $data = $this->data_list()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->kd_barang.' / '.$row->nama_barang ;
        }
        return $options;
    }

    public function get_location_list($post=array()){

    	if(!empty($post['keyword'])){
	   		$this->db->like('name', $post['keyword']);
    	}

	   	$this->db->where_not_in("(loc_type IS NULL OR loc_type NOT IN ('OUTBOUND','SENTOUT'))", NULL);

    	$data= $this->db->get('locations');

    	return $data->result_array();

    }

    public function get_by_location($location=''){

    	$where = '';

    	if(!empty($location)){
    		$where .= "AND lower(loc.name) = '".strtolower($location)."'";
    	}


    	$sql = "
    		SELECT
    			loc.id as location_id, loc.name as location_name, rb.item_code_all, COALESCE(rb.qty,0) as qty
    		FROM
    			locations loc
    		LEFT JOIN
    			(
    				SELECT
    					ird.location_id,
    					string_agg(DISTINCT itm.code, ', ') as item_code_all,
    					sum(last_qty) as qty
    				FROM item_receiving_details ird
    				LEFT JOIN items itm
    					ON itm.id=ird.item_id
    				GROUP BY ird.location_id
    			) as rb
    			ON rb.location_id=loc.id
    		WHERE 1=1
    		$where
    	";

    	return $this->db->query($sql);
    }

    public function get_by_warehouse($warehouse=''){

    	$where = '';

    	if(!empty($warehouse)){
    		$where .= "AND lower(wh.name) = '".strtolower(urldecode($warehouse))."'";
    	}

    	$sql = "
    		SELECT
    			wh.id as warehouse_id, wh.name as warehouse_name, itm.code as item_code, COALESCE(sum(rb.qty),0) as qty
    		FROM
	    		items itm
	    	JOIN items_warehouses itmwh
	    		on itmwh.item_id=itm.id
	    	JOIN warehouses wh
	    		ON wh.id=itmwh.warehouse_id
    		LEFT JOIN
    			(
    				SELECT
    					wh.id as warehouse_id,
    					itm.id as item_id,
    					sum(last_qty) as qty
    				FROM item_receiving_details ird
    				JOIN locations loc
    					ON loc.id=ird.location_id
		    		JOIN location_areas loc_area
		    			ON loc_area.id=loc.location_area_id
		    		JOIN warehouses wh
		    			ON wh.id=loc_area.warehouse_id
    				LEFT JOIN items itm
    					ON itm.id=ird.item_id
    				GROUP BY wh.id,itm.id
    			) as rb
    			ON rb.warehouse_id=wh.id AND itm.id=rb.item_id
    		WHERE 1=1
    		$where
    		GROUP BY wh.id, itm.id, wh.name, itm.code
    	";

    	return $this->db->query($sql);
    }

	public function get_by_group_warehouse($warehouseGroup=''){

    	$where = '';

    	if(!empty($warehouseGroup)){
    		$where .= "AND lower(wg.name) = '".strtolower(urldecode($warehouseGroup))."'";
    	}

    	$sql = "
    		SELECT
				wg.id as warehouse_group_id, wg.name as warehouse_group_name,
				COALESCE(tbl.qty,0) as qty
    		FROM
	    		items itm
	    	JOIN items_warehouses itmwh
	    		on itmwh.item_id=itm.id
	    	JOIN warehouses wh
				ON wh.id=itmwh.warehouse_id
			JOIN warehouses_warehouse_groups wwg
				ON wwg.warehouse_id=wh.id
			JOIN warehouse_groups wg
				ON wg.id=wwg.warehouse_group_id
			LEFT JOIN (
					SELECT
						wg.id as warehouse_id,
						sum(last_qty) as qty
					FROM item_receiving_details ird
					JOIN locations loc
						ON loc.id=ird.location_id
					JOIN location_areas loc_area
						ON loc_area.id=loc.location_area_id
					JOIN warehouses wh
						ON wh.id=loc_area.warehouse_id
					JOIN warehouses_warehouse_groups wwg
						ON wwg.warehouse_id=wh.id
					JOIN warehouse_groups wg
						ON wg.id=wwg.warehouse_group_id
					GROUP BY wg.id
			) as tbl ON tbl.warehouse_id=wh.id
    		WHERE 1=1
    		$where
    		GROUP BY wg.id, wg.name, tbl.qty
		";

    	return $this->db->query($sql);
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

    	$date = date('Y-m-d h:i:s');

    	$inbDocName = 'IMPORT-'.date("Y/m/d h:i:s");

    	// Inbound Create
        $inbData = array(
        	'code'					=> $inbDocName,
        	'date'					=> $date,
        	'supplier_id'			=> NULL,
        	'document_id'			=> NULL,
        	'user_id'				=> $this->session->userdata('user_id'),
			'remark'				=> '',
			'warehouse_id'			=> ''
     	);

     	$this->db->insert('inbound',$inbData);

        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->select('count(*) as total')
                ->from('item_receiving_details')
                ->where('LOWER(unique_code)', strtolower($data[$i]['unique_code']));

            $check = $this->db->get()->row()->total;

            if($check > 0){
                continue;
            }else{

                $inserts[] = array(
                	'item_receiving_id'	=> '',
                	'unique_code'		=> bin2hex($data[$i]['unique_code']),
                	'parent_code'		=> $data[$i]['parent_code'],
                	'kd_batch'			=> $data[$i]['kd_batch'],
                	'tgl_exp'			=> $data[$i]['tgl_exp'],
                	'tgl_in'			=> $data[$i]['tgl_in'],
                	'location_id'		=> $data[$i]['location'],
                	'putaway_time'		=> date('Y-m-d h:i:s'),
                	'first_qty'			=> $data[$i]['qty'],
                	'last_qty'			=> $data[$i]['qty'],
                	'qc_id'				=> $data[$i]['qc'],
                	'item_id'			=> $data[$i]['item'],
                	'rfid_tag'			=> $data[$i]['unique_code']
                );

            }

        }

        $this->db->insert_batch($this->table,$inserts);

        $this->db->trans_complete();

        return $this->db->trans_status();

    }

}

?>

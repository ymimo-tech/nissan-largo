<?php
class referensi_lokasi_kategori_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'location_categories';

    public function data($condition = array()) {

        $this->db->select('a.id as loc_category_id, a.name as loc_category_name, a.description as loc_category_desc');
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function getLocationById($post = array()){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_loc_category
				WHERE
					loc_category_id IN('.implode(',', $post['loc_category_id']).')';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getLocation(){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_loc_category
				WHERE
					loc_category_name IS NOT NULL';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getQtyLocation(){
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					m_loc_category';

		$row = $this->db->query($sql)->row_array();
		$result['qty'] = $row['total'];

		return $result;
	}

    public function get_by_id($id) {
        $condition['a.loc_category_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('loc_category_id' => $id));
    }

    public function delete($id) {

		$result = array();

		$in = explode(',', $this->config->item('hardcode_location_id'));

		if(in_array($id, $in)){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete master location';
		}else{

            // $sql = 'SELECT
            //          COUNT(*) AS total
            //      FROM
            //          receiving_barang
            //      WHERE
            //          loc_category_id=\''.$id.'\'';
            
            // $row = $this->db->query($sql)->row_array();
            
            // if($row['total'] > 0){
            //  $result['status'] = 'ERR';
            //  $result['message'] = 'Cannot delete location, because data have relation to another table';
            // }else{
				$this->db->delete($this->table, array('loc_category_id' => $id));

				$result['status'] = 'OK';
				$result['message'] = 'Delete data success';
            // }
		}

		return $result;

    }

    public function options($default = '--Pilih Nama Lokasi Kategori--', $key = '') {
        $data = $this->data()->get();
        $options = array();
    
        if (!empty($default))
            $options[$key] = $default;
    
        foreach ($data->result() as $row) {
            $options[$row->loc_category_id] = $row->loc_category_name ;
        }
        return $options;
    }

}

?>

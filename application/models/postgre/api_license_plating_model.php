<?php

class Api_license_plating_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function validateSN($sn){

    	$sn = (string)$sn;

		$this->db
			->from('item_receiving_details')
			->where('unique_code', $sn);

		$r = $this->db->get()->result_array();
		
		if(count($r) > 0){
			return true;
		} else{
			return false;
		}

    }
	
	function validatePC($pc){

		$pc = (string)$pc;

		$this->db
			->from('item_receiving_details')
			->where('unique_code', $pc);

		$r = $this->db->get()->result_array();

		if(count($r) > 0){
			return false;
		} else{
			return true;
		}

    }
	
	function submit($data){

   		$parentCode = (string)$data['kd_parent'];

		if(is_array($data['kd_unik'])){

			foreach ($data['kd_unik'] as $sn) {

	    		$uniqueCode[] = (string)$sn;

			}

		}else{

			$kdUnik = json_decode($data['kd_unik'], true);

			if(is_array($kdUnik)){

				foreach ($kdUnik as $sn) {

					$uniqueCode[] = (string)$sn;
	
				}

			}else{
	    		$uniqueCode[] = (string)$data['kd_unik'];
			}

		}

    	if(!empty($uniqueCode)){

	        $username    = (!empty($this->input->get_request_header('User', true))) ? $this->input->get_request_header('User', true) : $this->session->userdata('username');
	        $user = $this->db->get_where('users',['user_name'=>$username])->row_array();

			$this->db
				->from('item_receiving_details')
				->where_in('unique_code', $uniqueCode)
				->where('last_qty >',0);

			$serialNumber = $this->db->get()->result_array();


			if($serialNumber){

				$data = array();

				foreach ($serialNumber as $sn) {

					$data[] = array(
						'item_id'	=> $sn['item_id'],
						'user_id_pick' => $user['id'],
						'process_name' => "Outer Label Move",
						'unique_code'	=> $sn['unique_code'],
						'description'	=> "Move Outer Label from ". $sn['parent_code']." to ".$parentCode
					);

				}

				$this->db->insert_batch('transfers',$data);

				$this->db
					->set('parent_code', $parentCode)
					->where_in('unique_code', $uniqueCode)
					->where('last_qty >',0)
					->update('item_receiving_details');

				return true;

	    	}

    	}

    	return false;

    }

    function checkparentcode($pc = ''){

    	$pc = (string)$pc;

    	$this->db
    		->from('item_receiving_details')
    		->where('unique_code', $pc);

    	$r = $this->db->get()->result_array();

    	return $r;

    }

}
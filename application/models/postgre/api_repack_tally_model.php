<?php

class Api_repack_tally_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

	function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
		return $this->db->get();
	}

    function loadDocument(){
    	$q = 	"
    		SELECT 
				ro.id as id_order_kitting, ro.code as kd_order_kitting, ro.date as tanggal_order_kitting,
				ro.status_id as status_order_kitting, ro.user_id, ro.type as order_kitting_type_id,
				rro.qty AS total
			FROM 
				repack_orders ro
			LEFT JOIN  repacks_repack_orders rro
				ON rro.repack_order_id=ro.id
			WHERE
				ro.st_repack = 0
					AND
				(ro.status_id <> 3 OR ro.status_id <> 5)
			GROUP BY ro.id, rro.qty
    	";

		$r = $this->db->query($q, false)->result_array();
		
		return $r;
    }

	function loadDetail($data=array()){

		$id_order_kitting = array();

		foreach ($data as $d) {
			$id_order_kitting[] = $d['id_order_kitting'];
		}

		$id_order_kitting = implode(',', $id_order_kitting);

		$sql = "

			SELECT * FROM (
				(
					SELECT 
						ro.id as id_order_kitting, r.id as id_kitting, itm.id as id_barang, itm.code as kd_barang, itm.name as nama_barang, sum(rro.qty) as jumlah_kitting,
						COALESCE(
							(
								SELECT
									sum(first_qty)
								FROM
									item_receiving_details
								WHERE
									repack_order_id=ro.id
								AND
									item_id=itm.id
							)
						,0) as jumlah_tally
					FROM 
						repack_orders ro
					LEFT JOIN 
						repacks_repack_orders rro
						ON rro.repack_order_id=ro.id
					LEFT JOIN 
						repacks r
						ON r.id=rro.repack_id
					LEFT JOIN
						items itm
						ON itm.id=r.item_id
					WHERE ro.id IN ($id_order_kitting)
					GROUP BY itm.id, ro.id, r.id
				)
				UNION
				(
					SELECT 
						ro.id as id_order_kitting, ri.repack_id as id_kitting, itm.id as id_barang, itm.code as kd_barang, itm.name as nama_barang, COALESCE((sum(rro.qty)*ri.qty),0) as jumlah_kitting,
						COALESCE(
							(
								SELECT
									sum(first_qty)
								FROM
									item_receiving_details
								WHERE
									repack_order_id=ro.id
								AND
									item_id=itm.id
							)
						,0) as jumlah_tally
					FROM 
						repack_orders ro
					LEFT JOIN 
						repacks_repack_orders rro
						ON rro.repack_order_id=ro.id
					LEFT JOIN 
						repack_items ri
						ON ri.repack_id=rro.repack_id
					LEFT JOIN
						items itm
						ON itm.id=ri.item_id
					WHERE ro.id IN ($id_order_kitting)
					GROUP BY itm.id, ro.id, ri.repack_id, ri.qty, ri.item_id
				)
			) AS tbl
			ORDER BY id_order_kitting
		";

		$r = $this->db->query($sql, false);

		return $r->result_array();

	}

	function validSN($data = array()){
		$r = $this->db
			->where('unique_code', $data['kd_unik'])
			->or_where('parent_code', $data['kd_unik'])
			->get('item_receiving_details')
			->num_rows();

		return $r;
    }
	
    function validQty($data=array()){
    	return true;
    }

	function postSN($data = array()){

		$barang = $this->get_id("id as id_barang","code",$data['kd_barang'],"items")->row_array();
		$qc = $this->get_id("id as id_qc","code",$data['kd_qc'],"qc")->row_array();
		$user = $this->get_id("id as user_id","user_name",$data['uname'],"users")->row_array();

        $data['tgl_exp'] = ($data['tgl_exp'] === "0000-00-00") ? NULL : $data['tgl_exp'];

        $data_insert    = array(
        					"item_id"	=>	$barang['id_barang'],
                            "kd_batch" => $data["kd_batch"],
                            "location_id" => 100,
                            "tgl_exp" => $data["tgl_exp"],
                            "tgl_in" => $data["tgl_in"],
                            "st_receive" => 1,
                            "first_qty" => $data["qty"],
                            "last_qty" => $data["qty"],
                            "qc_id" => $qc["id_qc"],
                            "unique_code" => $data['kd_unik'],
                            "parent_code" => $data['kd_parent'],
                            "repack_order_user_id" => $user["user_id"],
                            "repack_order_id" => $data["id_order_kitting"]
                        );
        
        $this->db->insert("item_receiving_details", $data_insert);
		
		$data_masukan	= array(
									"unique_code"	=> $data["kd_unik"],
									"loc_id_new"	=> 100,
									"loc_id_old"	=> 100,
									"pick_time"		=> $data["tgl_in"],
									"put_time"		=> $data["tgl_in"],
									"user_id_pick"	=> $user["user_id"],
									"user_id_put"	=> $user["user_id"],
									"process_name"	=> "REPACK TALLY"
									);
        $this->db->insert("transfers", $data_masukan);

    }

    function get_item($kd_barang=''){
    	$this->db->select("itm.code as kd_barang,has_batch,has_expdate,has_qty,def_qty, unt.code as kd_satuan")
    		->from('items itm')
    		->join('units unt','unt.id=itm.unit_id','left')
    		->where('itm.code', $kd_barang);

    	return $this->db->get();
    }


}
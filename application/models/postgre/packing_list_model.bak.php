<?php
class packing_list_model extends CI_Model {

    public function __construct() {
		parent::__construct();
        $this->load->library("randomidgenerator");
	}
	
	public function getDetailPacking($params){
		$packing_number = $params["packing_id"];
		// $pc_code = substr($packing_number, 0, 15);
		// $inc = substr($packing_number, 15, 17);
		$pc_code = $packing_number;

		$requestData = $_REQUEST;

		$data = [];
		$result_packing = [];

		$results = $this->db
					// ->select('oird.id_outbound_receiving_barang, oird.id_item_receiving_details, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, SUM(pr.qty) as qty, unt.name as uom_name')
					->select('oird.id_outbound_receiving_barang, oird.id_item_receiving_details, oird.id_outbound AS outbound_id, o.code AS outbound_doc, p.pl_id AS picking_id, p.name AS picking_doc, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, SUM(pr.qty) as picked_qty, SUM(oird.qty) as packed_qty, unt.name as uom_name')
					->from('outbound_item_receiving_details oird')
					->join('outbound o', 'o.id=oird.id_outbound')
					->join('item_receiving_details ird', 'ird.id=oird.id_item_receiving_details')
					->join('picking_recomendation pr', 'pr.item_receiving_detail_id=ird.id')
					->join('pickings p', 'pr.picking_id=p.pl_id')
					->join('items itm', 'itm.id=ird.item_id', 'LEFT')
					->join('units unt','unt.id=itm.unit_id','left')
					->where('oird.packing_number', $pc_code)
					// ->where('oird.inc', $inc)
					->group_by('oird.id_outbound_receiving_barang, oird.id_item_receiving_details, oird.id_outbound, o.code, p.pl_id, p.name, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, pr.qty, unt.name')
					->get();
		$result = $results->result_array();
		// var_dump($this->db->last_query());exit;

		for($i=0;$i<$results->num_rows();$i++){
			$unique_code = '';
			$r_unique_code = $result[$i]['unique_code'];
			if(strpos($r_unique_code, 'BYSYS') > 0) {
				if(strpos($r_unique_code, 'XX')) {
					$xsn = explode('XX', $r_unique_code);
					$ysn = explode('YY', $xsn[1]);
					$unique_code = $ysn[0];
				} else {
					$unique_code = substr($r_unique_code,9,strlen($r_unique_code));
				}
			} else {
				$unique_code = $r_unique_code;
			}
			
			$nested = array();
			$nested[] = $i + 1 ;
			$nested[] = $result[$i]['code'];
			$nested[] = $result[$i]['name'];
			$nested[] = $unique_code;
			$nested[] = '<a href="'.base_url().'outbound/detail/'.$result[$i]['outbound_id'].'" target="_blank">'.$result[$i]['outbound_doc'].'</a>';
			$nested[] = '<a href="'.base_url().'picking/detail/'.$result[$i]['picking_id'].'" target="_blank">'.$result[$i]['picking_doc'].'</a>';
			$nested[] = $result[$i]['picked_qty'].' '.$result[$i]['uom_name'];
			$nested[] = $result[$i]['packed_qty'].' '.$result[$i]['uom_name'];
			$nested[] = '';
			$nested[] = '';
			$result_packing[] = $nested;
		}
		
		$results_new = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( count($result_packing) ),
			"recordsFiltered" => intval( count($result_packing) ),
			"data"            => $result_packing
		);

		return $results_new;

	}

	public function getPackingList($post) {
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'packing_number',
			1 => 'packing_number',
			2 => 'packing_number',
			3 => 'inc',
			4 => 'user_packing',
			5 => 'packing_time',
			6 => 'outbound_doc',
			7 => 'loading_doc',
			8 => 'weight',
			9 => 'volume'
		);

		$where = '';

		if(!empty($post['id_shipping'])){
			$where .= " AND loading_doc = '" . $post['id_shipping'] . "' ";
		}
		
		if(!empty($post['outbound'])){
			$where .= " AND outbound_doc = '" . $post['outbound'] . "' ";
		}

		if(!empty($post['pccode'])){
			$pc_code = substr($post['pccode'], 0, 15);
			$inc = substr($post['pccode'], 15, 17);
			$where .= " AND (packing_number = '" . $pc_code . "' AND inc = " . $inc .") ";
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= " AND packing_time >= '" . DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). " 00:00:00'";
			$where .= " AND packing_time <= '" . DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). " 23:59:59'";
		}

		if(!empty($post['warehouse'])){
			$this->db->join('outbound','outbound_item_receiving_details.id_outbound = outbound.id', 'left');
			$this->db->where_in('outbound.warehouse_id', $post['warehouse']);
		}

		$this->db->select([
							"packing_number",
							"inc"
						])
					->from("outbound_item_receiving_details")
					->group_by("packing_number, inc");
					
		$totalData = $this->db->get()->num_rows();
		
		// $query = "SELECT packing_number, inc, STRING_AGG(user_packing, ', ') AS user_packing, packing_time, outbound_id, outbound_doc, loading_id, loading_doc, weight, volume, total_item_packed FROM (
		$query = "SELECT packing_number, inc, STRING_AGG(user_packing, ',') AS user_packing, packing_time, loading_id, loading_doc, weight, volume, total_item_packed FROM (
				SELECT
					oird.packing_number AS packing_number,
					oird.inc AS inc,
					usr.user_name AS user_packing,
					(SELECT MIN(time) FROM outbound_item_receiving_details WHERE packing_number = oird.packing_number AND inc = oird.inc) as packing_time,
					outb.id AS outbound_id,
					outb.code AS outbound_doc,
					ship.shipping_id AS loading_id,
					COALESCE(ship.code, '-') AS loading_doc,
					oird.weight AS weight,
					oird.volume AS volume,
					(
						SELECT
							COUNT(irdx.item_id)
						FROM
							outbound_item_receiving_details oirdx
							JOIN item_receiving_details irdx ON oirdx.id_item_receiving_details = irdx.id
						WHERE
							packing_number = oird.packing_number
							AND inc = oird.inc
					) AS total_item_packed
				FROM
					outbound_item_receiving_details oird
					JOIN outbound outb ON outb.id = oird.id_outbound
					LEFT JOIN shippings ship ON ship.shipping_id = oird.shipping_id
					JOIN users usr ON usr.id = oird.user_id
				GROUP BY
					oird.packing_number,
					oird.inc,
					usr.user_name,
					outb.id,
					outb.code,
					ship.shipping_id,
					ship.code,
					oird.weight,
					oird.volume
			) a
			WHERE TRUE
			".$where."
			--GROUP BY packing_number, inc, packing_time, outbound_id, outbound_doc, loading_id, loading_doc, weight, volume, total_item_packed
			GROUP BY packing_number, inc, packing_time, loading_id, loading_doc, weight, volume, total_item_packed
			ORDER BY
				".$columns[$requestData['order'][0]['column']]."
				".$requestData['order'][0]['dir'].",
				packing_number DESC,
				inc ASC
			LIMIT ".$requestData['length']." OFFSET ".$requestData['start'];
		
		// var_dump($query);exit;
		
		$row = $this->db->query($query)->result_array();
		
		/*$this->db->select([
							"oird.packing_number AS packing_number",
							"oird.inc AS inc",
							"usr.user_name AS user_packing",
							"MIN(oird.time) AS packing_time",
							"outb.id AS outbound_id",
							"outb.code AS outbound_doc",
							"COALESCE(ship.code, '-') AS loading_doc",
							"oird.weight AS weight",
							"oird.volume AS volume",
							"(SELECT COUNT(irdx.item_id) FROM outbound_item_receiving_details oirdx JOIN item_receiving_details irdx ON oirdx.id_item_receiving_details=irdx.id WHERE packing_number = oird.packing_number AND inc = oird.inc) AS total_item_packed"
						])
					->from("outbound_item_receiving_details oird")
					->join("outbound outb","outb.id = oird.id_outbound")
					->join("shippings ship","ship.shipping_id = oird.shipping_id","LEFT")
					->join("users usr","usr.id = oird.user_id")
					->order_by('oird.packing_number', 'DESC')
					->group_by('oird.packing_number, oird.inc, usr.user_name, oird.time, outb.id, outb.code, ship.code, oird.weight, oird.volume')
					->limit($requestData['length'],$requestData['start']);
		
		$row = $this->db->get()->result_array();*/

		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if($row[$i]['loading_doc'] == '-') {
				$action .= '<li>';
				$action .= '<a class="data-table-add '.$class.'" href="'.base_url().'packing/edit/'.$row[$i]['packing_number'].'" target="_blank"><i class="fa fa-pencil"></i> Add More Items</a>';
				$action .= '</li>';
			}

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				// $action .= '<a class="data-table-print '.$class.'" data-packing-number="'.$row[$i]['packing_number'].'" data-outbound="'.$row[$i]['outbound_id'].'"><i class="fa fa-print"></i> Print Packing Label</a>';
				$action .= '<a class="data-table-print '.$class.'" data-packing-number="'.$row[$i]['packing_number'].'"><i class="fa fa-print"></i> Print Packing Label</a>';
				$action .= '</li>';
            }

            /*if ($this->access_right->otoritas('delete')) {

				$disabledClass = "";
				if($row[$i]['loading_doc'] != "-")
				{
					$disabledClass = "disabled-link";
				}

                $action .= '<li>';
				$action .= '<a class="data-table-cancel '.$disabledClass.'" data-id="'.$row[$i]['packing_number'].'"><i class="fa fa-trash"></i> Cancel</a>';
				$action .= '</li>';

            }*/

			$action .= '</ul>';

			$nested = array();

			$packing_time = $row[$i]['packing_time'];

			if($packing_time != '-'){
				$packing_time = explode(' ', $packing_time);
				$packing_time = substr($packing_time[1],0,8) . '<br>' . '<span class="small-date">' . $packing_time[0] . '</span>';
			}
			
			$user = $row[$i]['user_packing'];
			$users = explode(',',$row[$i]['user_packing']);
			if(count($users) > 1) {
				$user = '';
				for($x=0;$x<count($users);$x++){
                	if($x==0){
                    	$user = $users[0].', ';
                    }
                    
					if(strpos($user, $users[$x]) === false){
                    	$user = $user . $users[$x].', ';
                    }
				}
				$user = substr($user,0,strlen($user)-2);
			}

			$nested[] = $row[$i]['packing_number'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a class="detail_packing">'.$row[$i]['packing_number'].'</a>';
			$nested[] = '<a href="'.base_url().'loading/detail/'.$row[$i]['loading_id'].'" target="_blank">'.$row[$i]['loading_doc'].'</a>';
			// $nested[] = '<a href="'.base_url().'outbound/detail/'.$row[$i]['outbound_id'].'" target="_blank">'.$row[$i]['outbound_doc'].'</a>';
			$nested[] = $row[$i]['weight'] . ' ' . WEIGHT_UOM;
			$nested[] = $row[$i]['volume'] . ' ' . VOLUME_UOM;
			// $nested[] = $row[$i]['user_packing'];
			$nested[] = $user;
			$nested[] = $row[$i]['total_item_packed'];
			$nested[] = $packing_time;
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}
	
	public function get_data_label($data){

    	$sql = "SELECT
					o.code as kd_outbound, o.date as tanggal_outbound, o.destination_name as \"DestinationName\", o.phone as \"Phone\", o.note as \"ShippingNote\", o.address as \"DestinationAddress\", o.address_1 as \"DestinationAddressL1\", address_2 as \"DestinationAddressL2\", address_3 as \"DestinationAddressL3\", orb_temp.packing_number, 
					TO_CHAR(o.delivery_date, 'DD-MM-YYYY') as delivery_date, 
					m_priority_name as priority,
					CONCAT((
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(qty)* CAST(itm.weight as float8)),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=o.id group by itm.weight
						) as tbl
					),' KG') as weight,
					CONCAT((
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=o.id group by itm.height, itm.width, itm.length
						) as tbl
					),' M3') as volume,
					d.destination_city, p.name as picking_code, o.note, o.ekspedisi
				FROM
					outbound o
				LEFT JOIN(
					SELECT
						packing_number,
						id_outbound
					FROM
						outbound_item_receiving_details
					WHERE
						id_outbound = ".$data['id_outbound']."
					GROUP BY
						id_outbound, packing_number
				)orb_temp ON orb_temp.id_outbound = o.id
				LEFT JOIN m_priority mp ON mp.m_priority_id=o.m_priority_id
				LEFT JOIN destinations d ON d.destination_id=o.destination_id
				JOIN picking_list_outbound plo on o.id=plo.id_outbound
				JOIN pickings p on p.pl_id=plo.pl_id
				WHERE
					o.id = ".$data['id_outbound'];
		$query = $this->db->query($sql);
		return $query->row();

    }
	
	public function getPcCode($post = array()){
		$result = array();
	
		$sql = "SELECT * FROM (
					SELECT
					CONCAT(packing_number,LPAD(CAST(inc AS VARCHAR), '3', '0')) AS packing_number FROM outbound_item_receiving_details
					GROUP BY packing_number,inc
				) a
				WHERE LOWER(packing_number) LIKE LOWER('%".$post['query']."%')";

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}
	
	public function getLoadingCode($post = array()){
		$result = array();
	
		$sql = "SELECT * FROM (
					SELECT code FROM shippings
				) a
				WHERE LOWER(code) LIKE LOWER('%".$post['query']."%')";

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}
	
	public function getOutboundCode($post = array()){
		$result = array();
	
		$sql = "SELECT * FROM (
					SELECT code FROM outbound
				) a
				WHERE LOWER(code) LIKE LOWER('%".$post['query']."%')";

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

}

<?php

class Api_picking_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    function load(){
        $this->db->select("p.name as pl_name, qty");
        $this->db->select(["CONCAT(SPLIT_PART(itm.brand,'-',1),' - ',itm.code,' - ',itm.oem,' - ',itm.name,' - ',itm.packing_description) as kd_barang"]);
        $this->db->select(["CAST(SPLIT_PART(p.name,'-',2) AS INTEGER) AS sort"]);
        $this->db->from("picking_qty pq");
        $this->db->join("pickings p", "p.pl_id = pq.picking_id", "left");
        $this->db->join("items itm", "itm.id=pq.item_id", "left");
        $this->db->where("p.pl_status", 0);
        $this->db->order_by('sort','desc');
		$this->db->order_by('p.name');
        return $this->db->get();
    }

    function get($picking_code){
        $this->db->select("pl_status");
        $this->db->from("pickings");
        $this->db->where("name", $picking_code);
        return $this->db->get();
    }

    //old :: document item
    // public function get_document_item($pickingCode, $outboundCode, $itemCode){

    //   $outboundCode = str_replace('%7C', '/', $outboundCode);



    //   $this->db->select(['p.name as receiving_code','itm.code as item_code','itm.name as item_name','p.pl_status as status',
    //                       "(SELECT sum(qty) FROM picking_qty WHERE picking_id = p.pl_id and item_id = itm.id) as qty",'unt.name as nama_satuan',
    //                       "itm.has_qty","pq.qty as qty","COALESCE(SUM(ird.last_qty),0) as available_qty",
    //                       "COALESCE(
    //             						(SELECT SUM(qty) FROM picking_recomendation WHERE picking_id=p.pl_id AND order_item_id=pq.item_id),0) as received_qty",]);

    //   // $this->db->select(array('p.name as receiving_code','itm.code as item_code','itm.name as item_name',"(SELECT sum(qty) FROM item_receiving WHERE picking_id = p.pl_id and inbound_id = i.id_inbound and item_id = itm.id) as qty",'unt.name as nama_satuan',"COALESCE(s.source_name,'-') as source",'r.st_receiving as status',"COALESCE(NULL,'') as image","COALESCE(def_qty,'1') as default_qty","COALESCE(SUM(first_qty),0) as received_qty","itm.has_expdate","itm.has_qty","itm.has_batch"));
    //       $this->db->from('pickings p');
    //       $this->db->join('picking_list_outbound plo','plo.pl_id = p.pl_id');
    //       $this->db->join('picking_qty pq','pq.picking_id=p.pl_id');
    //       $this->db->join('item_receiving_details ird','ird.picking_id=pq.id','left');
    //       $this->db->join('outbound o','o.id=plo.id_outbound','left');
    //       // $this->db->join('destinations d','d.destination_id=o.destination_id','left');
    //       $this->db->join('items itm','itm.id=pq.item_id','left');
    //       $this->db->join('units unt','unt.id=itm.unit_id','left');
    //       $this->db->where('p.name',$pickingCode);
    //       $this->db->where('o.code',$outboundCode);
    //       $this->db->where('pq.qty != 0',null);
    //       $this->db->group_by('"itm"."id", p.pl_id, pq.qty, pq.item_id, p.name, itm.code, itm.name, unt.name, itm.name');
    //       $this->db->order_by('itm.code','asc');
    //       $this->db->order_by('received_qty','asc');

    //       if(!empty($itemCode)){
    //           $this->db->where('itm.code', $itemCode);
    //       }

    //       $rec  = $this->db->get()->result_array();

    //       return $rec;

    // }

    public function get_document_item_old($pickingCode, $item_area){

        // $outboundCode = str_replace('%7C', '/', $outboundCode);
        $pickingCode = str_replace('%2C', ',', $pickingCode);
        $pickingCode = str_replace('%27', "'", $pickingCode);
        // dd($item_area);
  
        $this->db->select(['p.name as receiving_code', 'loc.name as location', 'o.code as outbound_code', 'itm.code as item_code','itm.name as item_name','p.pl_status as status',
                            "(SELECT sum(qty) FROM picking_qty WHERE picking_id = p.pl_id and item_id = itm.id) as qty",'unt.name as nama_satuan',
                            "itm.has_qty","pq.qty as qty","COALESCE(SUM(ird.last_qty),0) as available_qty",
                            "COALESCE(
                                          (SELECT SUM(qty) FROM picking_recomendation WHERE picking_id=p.pl_id AND order_item_id=pq.item_id),0) as received_qty",]);
  
        // $this->db->select(array('p.name as receiving_code','itm.code as item_code','itm.name as item_name',"(SELECT sum(qty) FROM item_receiving WHERE picking_id = p.pl_id and inbound_id = i.id_inbound and item_id = itm.id) as qty",'unt.name as nama_satuan',"COALESCE(s.source_name,'-') as source",'r.st_receiving as status',"COALESCE(NULL,'') as image","COALESCE(def_qty,'1') as default_qty","COALESCE(SUM(first_qty),0) as received_qty","itm.has_expdate","itm.has_qty","itm.has_batch"));
            $this->db->from('pickings p');
            $this->db->join('picking_list_outbound plo','plo.pl_id = p.pl_id');
            $this->db->join('picking_qty pq','pq.picking_id=p.pl_id');
            $this->db->join('item_receiving_details ird','ird.picking_id=pq.id','left');
            $this->db->join('outbound o','o.id=plo.id_outbound','left');
            // $this->db->join('destinations d','d.destination_id=o.destination_id','left');
            $this->db->join('items itm','itm.id=pq.item_id','left');
            $this->db->join('location_areas la','itm.item_area = la.name','left');
            $this->db->join('locations loc','loc.id = ird.location_id', 'left');
            $this->db->join('units unt','unt.id=itm.unit_id','left');
            $this->db->where_in('p.name',$pickingCode);
            $this->db->like('itm.item_area',$item_area,'after');
            $this->db->where('pq.qty != 0',null);
            $this->db->group_by('"itm"."id", o.code, p.pl_id, pq.qty, pq.item_id, p.name, itm.code, itm.name, unt.name, itm.name');
            $this->db->order_by('itm.code','asc');
            $this->db->order_by('received_qty','asc');
            // dd($this->db->get_compiled_select());

            // if(!empty($itemCode)){
            //     $this->db->where('itm.code', $itemCode);
            // }
  
            // dd($this->db->get_compiled_select());
  
        //   $sql = $this->db->get_compiled_select().'union all SELECT "p"."name" as receiving_code, o.code as outbound_code,"itm"."code" as item_code, "itm"."name" as item_name, "p"."pl_status" as status, (SELECT sum(qty) FROM picking_qty WHERE picking_id = p.pl_id and item_id = itm.id) as qty, "unt"."name" as nama_satuan, "itm"."has_qty", "pq"."qty" as qty, COALESCE(SUM(ird.last_qty),0) as available_qty, COALESCE(
        //       (SELECT SUM(qty) FROM picking_recomendation WHERE picking_id=p.pl_id AND order_item_id=pq.item_id),0) as received_qty
        //      FROM "pickings" p
        //      JOIN "picking_list_outbound" plo ON "plo"."pl_id" = "p"."pl_id"
        //      JOIN "picking_qty" pq ON "pq"."picking_id"="p"."pl_id"
        //      LEFT JOIN "item_receiving_details" ird ON "ird"."picking_id"="pq"."id"
        //      LEFT JOIN "outbound" o ON "o"."id"="plo"."id_outbound"
        //      LEFT JOIN "items" itm ON "itm"."id"="pq"."item_id"
        //      LEFT JOIN "units" unt ON "unt"."id"="itm"."unit_id"
        //      AND pq.qty != 0
        //      GROUP BY "itm"."id", "p"."pl_id", "pq"."qty", "pq"."item_id", "p"."name", "itm"."code", "itm"."name", "unt"."name", "itm"."name", o.code';
  
  
            $rec  = $this->db->get()->result_array();

            for($i = 0; $i<count($rec); $i++){
				$d = $this->retrieve_location($rec[$i]['receiving_code'],$rec[$i]['outbound_code'],$rec[$i]['item_code']);
				if(count($d)>0){
					if(!$d){
						$rec[$i]['location'] = '-';
					}else{
						$rec[$i]['location'] = $d[0]['recomendation_location'];
					}
				}else{
					$rec[$i]['location'] = '-';
				}
				
			}
        //   $rec  = $this->db->query($sql)->result_array();
  
            return $rec;
  
      }

      public function get_document_item($pickingCode, $item_area){

        // $outboundCode = str_replace('%7C', '/', $outboundCode);
        $pickingCode = str_replace('%2C', ',', $pickingCode);
        $pickingCode = str_replace('%27', "'", $pickingCode);
  
        $this->db->select(['p.name as receiving_code', 'loc.name as location', 'itm.id item_id','itm.id item_ori','o.code as outbound_code', 'itm.code as item_code','itm.name as item_name','p.pl_status as status',
                            "(SELECT sum(qty) FROM picking_qty WHERE picking_id = p.pl_id and item_id = itm.id) as qty",'unt.name as nama_satuan',
                            "itm.has_qty","pq.qty as qty","COALESCE(SUM(ird.last_qty),0) as available_qty",
                            "COALESCE(
                                          (SELECT SUM(qty) FROM picking_recomendation WHERE picking_id=p.pl_id AND order_item_id=pq.item_id),0) as received_qty"
                        ]);
  
        // $this->db->select(array('p.name as receiving_code','itm.code as item_code','itm.name as item_name',"(SELECT sum(qty) FROM item_receiving WHERE picking_id = p.pl_id and inbound_id = i.id_inbound and item_id = itm.id) as qty",'unt.name as nama_satuan',"COALESCE(s.source_name,'-') as source",'r.st_receiving as status',"COALESCE(NULL,'') as image","COALESCE(def_qty,'1') as default_qty","COALESCE(SUM(first_qty),0) as received_qty","itm.has_expdate","itm.has_qty","itm.has_batch"));
            $this->db->from('pickings p');
            $this->db->join('picking_list_outbound plo','plo.pl_id = p.pl_id');
            $this->db->join('picking_qty pq','pq.picking_id=p.pl_id');
            $this->db->join('item_receiving_details ird','ird.picking_id=pq.id','left');
            $this->db->join('outbound o','o.id=plo.id_outbound','left');
            $this->db->join('destinations d','d.destination_id=o.destination_id','left');
            $this->db->join('items itm','itm.id=pq.item_id','left');
			$this->db->join('locations loc','loc.id = itm.def_loc_id', 'left');
            $this->db->join('location_areas la','itm.item_area = la.name','left');
            $this->db->join('units unt','unt.id=itm.unit_id','left');
            $this->db->where_in('p.name',$pickingCode);
            $this->db->like('itm.item_area',$item_area,'after');
            $this->db->where('pq.qty != 0',null);
            $this->db->where('(pq.is_sub is null or pq.is_sub = 0)',null);
            $this->db->group_by('"itm"."id", o.code, p.pl_id, pq.qty, pq.item_id, p.name, loc.name, itm.code, itm.name, unt.name, itm.name');
              
            $sql = "select * from (".$this->db->get_compiled_select()." union all
            select p.name as receiving_code, 
            case
                when loc.name is null then loc1.name
                when loc.name is not null then loc.name
            end as locations, 
            case
                when itm2.id is null then itm1.id 
                when itm2.id is not null then itm2.id
            end as item_id, 
            itm1.id as item_ori, o.code as outbound_code,
            case
                when itm2.code is null then itm1.code
                when itm2.code is not null then itm2.code
            end as item_code,
            case
                when itm2.name is null then itm1.name
                when itm2.name is not null then itm2.name
            end as item_name,
            p.pl_status as status,
            sum(pqs.qty) as qty, u.name as nama_satuan, itm1.has_qty, sum(pqs.qty) qty,
            COALESCE(SUM(ird.last_qty),0) as available_qty,
            case
                when itm2.id is null 
                then
                COALESCE((SELECT SUM(qty) FROM picking_recomendation WHERE picking_id = p.pl_id AND order_item_id=itm1.id),0)
                when itm2.id is not NULL
                then
                COALESCE((SELECT SUM(qty) FROM picking_recomendation WHERE picking_id = p.pl_id AND order_item_id=itm1.id and item_id = itm2.id),0)
                end as received_qty
            from picking_qty_substitute pqs
            left join picking_qty pq on pq.id = pqs.picking_qty_id
            left join pickings p on p.pl_id = pq.picking_id
            left join picking_list_outbound plo on plo.pl_id = p.pl_id
            left join outbound o on o.id = plo.id_outbound
            left join items itm1 on itm1.id = pq.item_id
            left join item_substitute is2 ON is2.id = pqs.item_substitute_id
            left join items itm2 on itm2.id = is2.item_id_subs 
            left join locations loc on loc.id = itm2.def_loc_id
            left join locations loc1 on loc1.id = itm1.def_loc_id 
            left join units u on u.id = itm1.unit_id 
            LEFT JOIN item_receiving_details ird ON ird.picking_id=pq.id
            WHERE p.name IN ('".implode('\', \'',$pickingCode)."')
            group by p.name, p.pl_id, loc.name, pq.item_id, loc1.name, itm2.id, itm1.id, itm1.name, itm2.name, o.code, p.pl_status, u.name) as a
            order by a.location asc";
            // dd($sql);
            $rec  = $this->db->query($sql)->result_array();

            return $rec;
  
      }

    function get_count($data,$return_data=false){

        // $serialize  = $this->get_serial($data["kd_unik"])->row_array();

        // if(!$serialize){
        //     $serialize  = $this->get_serial_lp($data["kd_unik"],$data['pl_name'])->row_array();
        // }

        // $this->db
        //     ->select(["pq.qty","COALESCE(SUM(pr.qty),0) as picked"])
        //     ->from('picking_qty pq')
        //     ->join('pickings p','p.pl_id=pq.picking_id','left')
        //     ->join('picking_recomendation pr','pr.picking_id=p.pl_id and pr.item_id=pq.item_id','left')
        //     ->join('item_receiving_details ird','ird.unique_code=pr.unique_code and picking_id=')
        //     ->where('p.name', $data['pl_name'])
        //     ->where('pq.item_id', $serialize['item_id'])
        //     ->group_by('pq.qty');

        // $count = $this->db->get()->result_array();
        // print_r($count);
        // exit;

        // $picked = $count['picked'] + $params['qty'];
        // if ($picked <= $count['qty']) {
        //     return true;
        // }else{
        //     return false;
        // }

        $this->db
            ->select('item_id')
            ->from('item_receiving_details ird')
            ->where("(unique_code ='".$data['kd_unik']."' or parent_code='".$data['kd_unik']."' or kd_batch='".$data['kd_unik']."')")
            ->group_by('item_id');

        $items = $this->db->get()->result_array();

        $status = true;

        if($items){
            $item = array();
            foreach ($items as $itm) {
                $item[] = $itm['item_id'];
            }

            $this->db
                // ->select(["pq.qty","COALESCE(SUM(pr.qty),0) as picked","pq.item_id"])
                ->select([
							"pq.qty",
							"COALESCE((
								SELECT
									SUM(qty)
								FROM picking_recomendation
								WHERE
									picking_id=p.pl_id
								AND
									(item_id=pq.item_id OR item_id IN (SELECT item_id_subs FROM item_substitute WHERE item_id = pq.item_id))
							),0) as picked",
							"pq.item_id"
						])
                ->from('picking_qty pq')
                ->join('pickings p','p.pl_id=pq.picking_id','left')
                // ->join('picking_recomendation pr','pr.picking_id=p.pl_id and pr.item_id=pq.item_id','left')
                ->where('p.name', $data['pl_name'])
                ->where_in('pq.item_id', $item)
                ->group_by('pq.qty,pq.item_id');

            $check = $this->db->get()->result_array();

            $checkItem=array();
            foreach ($check as $c) {
                if($c['qty'] > $c['picked']){
                    if($c['qty'] >= ($c['picked']+$data['qty'])){
                        $checkItem[] = $c['item_id'];
                    }
                }
            }

            if(!$return_data){

                if(count($checkItem) > 0){
                    return true;
                }else{
                    return false;

                }

            }else{

                return $checkItem;

           }
        }

        return false;

    }

    function get_picked($picking_code){

        $this->db
            ->select('item_id, sum(qty) as q_picked')
            ->from('picking_recomendation pr')
            ->join('pickings p_','p_.pl_id=pr.picking_id','left')
            ->where('p_.name', $picking_code)
            ->group_by('pr.item_id');

        $sp = $this->db->get_compiled_select();

        $this->db
            ->select('item_id, sum(last_qty) as q_picked')
            ->from('item_receiving_details ird_')
            ->join('pickings pl_','pl_.pl_id=ird_.picking_id','left')
            ->where('pl_.name', $picking_code)
            ->group_by('ird_.item_id');

        $sp2 = $this->db->get_compiled_select();

        $this->db
            ->select([
                "itm.code as kd_barang",
                "COALESCE(string_agg(DISTINCT l.name, ','), '-- No Stok --') as location",
                "pq.qty as qty",
                "COALESCE(sp.q_picked, 0) as single_picked",
                "COALESCE(sp2.q_picked, 0) as multiple_picked",
                "unt.code as unit_code"
            ])
            ->from('picking_qty pq')
            ->join("($sp) as sp",'sp.item_id=pq.item_id','left')
            ->join("($sp2) as sp2",'sp2.item_id=pq.item_id','left')
            ->join('pickings p','p.pl_id=pq.picking_id','left')
            ->join('item_receiving_details ird','ird.item_id=pq.item_id and ird.location_id not in (100,101,102,103,104,105,106)','left')
            ->join('items itm','itm.id=pq.item_id','left')
            ->join('locations l','l.id=ird.location_id','left')
            ->join('units unt','unt.id=pq.unit_id','left')
            ->where('p.name', $picking_code)
            ->where('(ird.picking_id IS NULL or ird.last_qty > 0)', NULL)
            ->group_by(['pq.item_id','itm.code','pq.qty','sp.q_picked','sp2.q_picked','unt.code']);

        $r = $this->db->get();

        return $r;
    }

    function retrieve($picking_code = '',$outbound_code = '', $item_code = ''){


		$this->db->select(['plo.pl_id', 'count(id_outbound) as consol'])
				->from('picking_list_outbound plo')
				->join('pickings p','p.pl_id=plo.pl_id')
				->where('p.name', $picking_code)
				->group_by('plo.pl_id');

		$consol = $this->db->get()->row_array()['consol'];

		if($consol > 1) {
			$this->db->select("p.name as pl_name, outbitm.qty");
			$this->db->select(["CONCAT(SPLIT_PART(itm.brand,'-',1),' - ',itm.code,' - ',itm.oem,' - ',itm.name,' - ',itm.packing_description) as kd_barang"]);
			$this->db->from("pickings p");
			$this->db->join('picking_list_outbound plo','p.pl_id=plo.pl_id');
			$this->db->join('outbound outb','outb.id=plo.id_outbound');
			$this->db->join('outbound_item outbitm','outbitm.id_outbound=outb.id');
			$this->db->join("items itm", "itm.id=outbitm.item_id", "left");
			$this->db->where("p.name", $picking_code);
			if($outbound_code != ''){
				$this->db->where("outb.code",$outbound_code);
			}
		} else {
			$this->db->select("p.name as pl_name, pq.qty");
			$this->db->select(["CONCAT(SPLIT_PART(itm.brand,'-',1),' - ',itm.code,' - ',itm.oem,' - ',itm.name,' - ',itm.packing_description) as kd_barang"]);
			$this->db->from("picking_qty pq");
			$this->db->join("pickings p", "p.pl_id = pq.picking_id", "left");
			$this->db->join('picking_list_outbound plo','p.pl_id=plo.pl_id');
			$this->db->join('outbound outb','outb.id=plo.id_outbound');
			$this->db->join("items itm", "itm.id=pq.item_id", "left");
			$this->db->where("p.name", $picking_code);
		}

        return $this->db->get();

    }

    function retrieve_location($picking_code = '', $outbound_code = '', $item_code = ''){



		$this->db->select(['plo.pl_id', 'count(id_outbound) as consol'])
				->from('picking_list_outbound plo')
				->join('pickings p','p.pl_id=plo.pl_id')
				->where('p.name', $picking_code)
				->group_by('plo.pl_id');

		$consol = $this->db->get()->row_array()['consol'];

    if($item_code != '' || strlen($item_code) > 0) {
      $item_id = $this->db->select('id')->from('items')->where('code', $item_code)->get()->row_array()['id'];
      $this->db->where('itm.id', $item_id);
    }

		if($consol > 1) {
			$this->db
				->select([
					"p.name as pl_name",
					"CONCAT(SPLIT_PART(itm.brand,'-',1),' - ',itm.code,' - ',itm.oem,' - ',itm.name,' - ',itm.packing_description) as kd_barang",
					"COALESCE(string_agg(DISTINCT l.name , ','), '-- No Stok --') as loc_name",
					"itm.has_qty","outbitm.qty as qty","COALESCE(SUM(ird.last_qty),0) as available_qty",
					"COALESCE(
						(SELECT SUM(qty) FROM picking_recomendation WHERE picking_id=p.pl_id),0) AND (item_id=outbitm.item_id OR item_id IN (SELECT item_id_subs FROM item_substitute WHERE item_id = outbitm.item_id)) AS picked_qty",
					"itm.id as item_id"
				])
				->from('pickings p')
				->join('picking_list_outbound plo','p.pl_id=plo.pl_id')
				->join('outbound outb','outb.id=plo.id_outbound')
				->join('outbound_item outbitm','outbitm.id_outbound=outb.id')
				->join('items itm','itm.id=outbitm.item_id','left')
				->join('item_receiving ir','ir.item_id=outbitm.item_id','left')
				->join('item_receiving_details ird','ird.item_receiving_id=ir.id and ird.location_id not in (100,101,102,103,104,105,106)','left')
				->join('locations l','l.id=ird.location_id','left')
				->join('location_areas loc_area','loc_area.id=l.location_area_id and loc_area.warehouse_id=outb.warehouse_id','left')
				->where('p.name',$picking_code)
        // ->where('itm.id', $item_id)
				->group_by(['outbitm.item_id','p.name', 'outbitm.qty','p.pl_id','itm.code','itm.name','itm.brand','itm.oem','itm.packing_description','itm.has_qty','itm.id']);

			if($outbound_code != ''){
				$this->db->where("outb.code",$outbound_code);
			}
		} else {
			$this->db
				->select([
					"p.name as pl_name",
					"CONCAT(SPLIT_PART(itm.brand,'-',1),' - ',itm.code,' - ',itm.oem,' - ',itm.name,' - ',itm.packing_description) as kd_barang",
					"COALESCE(string_agg(DISTINCT l.name , ','), '-- No Stok --') as loc_name",
					"itm.has_qty","pq.qty as qty","COALESCE(SUM(ird.last_qty),0) as available_qty",
					"COALESCE(
						(SELECT SUM(qty) FROM picking_recomendation WHERE picking_id=p.pl_id AND order_item_id=pq.item_id),0) as picked_qty",
					"itm.id as item_id"
				])
				->from('picking_qty pq')
				->join('pickings p','p.pl_id=pq.picking_id','left')
				->join('picking_list_outbound plo','p.pl_id=plo.pl_id')
				->join('outbound outb','outb.id=plo.id_outbound')
				->join('outbound_item outbitm','outbitm.id_outbound=outb.id')
				->join('items itm','itm.id=pq.item_id','left')
				->join('item_receiving ir','ir.item_id=pq.item_id','left')
				->join('item_receiving_details ird','ird.item_receiving_id=ir.id and ird.location_id not in (100,101,102,103,104,105,106)','left')
				->join('locations l','l.id=ird.location_id','left')
				->join('location_areas loc_area','loc_area.id=l.location_area_id and loc_area.warehouse_id=outb.warehouse_id','left')
				->where('p.name',$picking_code)
				// ->where('itm.id', $item_id)
				->group_by(['pq.item_id','p.name', 'pq.qty','p.pl_id','itm.code','itm.name','itm.brand','itm.oem','itm.packing_description','itm.has_qty','itm.id']);
		}

        $items = $this->db->get()->result_array();

        foreach ($items as &$itm) {

            $this->db
                ->select('itm.code, ird.unique_code, loc.name as location_name, (CASE WHEN itm.shipment_id=1 THEN min(tgl_in) ELSE min(tgl_exp) END) as date')
                ->from('item_receiving_details ird')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('items itm','itm.id=ird.item_id')
                ->where('ird.item_id',$itm['item_id'])
                ->where_not_in('ird.location_id',[100,101,102,103,104,105,106])
                ->where('last_qty >',0)
                ->group_by('itm.code, ird.unique_code,loc.name, itm.shipment_id')
                ->limit(1);

            $d = $this->db->get()->row_array();

            if($d){
                $itm['recomendation'] = $d['unique_code'];
                $itm['recomendation_location'] = $d['location_name'];
            }else{
                $itm['recomendation'] = '-';
                $itm['recomendation_location'] = '-';
            }

        }

        return $items;
    }

    function get_batch($item_code, $batch_code){
        $this->db->select("*");
        $this->db->from("item_receiving_details ird");
        $this->db->join('item_receiving ir','ir.id=ird.item_receiving_id','left');
        $this->db->join("items", "itm.id=ir.item_id", "left");
        $this->db->where('itm.code', $item_code);
        $this->db->where("ird.kd_batch", $batch_code);
        return $this->db->get();
    }

    function get_serial($serial_number){

        $ignore_location = array(100, 101, 102, 103, 104, 105, 106);

        $avail_location = '2 , 3';

        $this->db->select("*, ird.id as item_receiving_detail_id");
        $this->db->from("item_receiving_details ird");
        $this->db->join("locations loc", "loc.id = ird.location_id", "left");
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
        $this->db->join('receivings r','r.id=ir.receiving_id','left');
        $this->db->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL);
        $this->db->where('(loc.location_type_id NOT IN (6) OR loc.location_type_id IS NULL)',NULL);
        $this->db->where("ird.unique_code", $serial_number);
        $this->db->where("ird.last_qty > 0",NULL);
        $this->db->order_by('ird.id','ASC');

        return $this->db->get();

    }

    function get_serial_lp($serial_number,$pl_name="",$itemId=array()){

        $ignore_location = array(100, 101, 102, 103, 104, 105, 106);

        $avail_location = '2 , 3';

        if(!empty($pl_name)){
            $pickDoc = $this->getPickingWarehouse($pl_name);
            $this->db->where('(loc_area.warehouse_id='.$pickDoc['warehouse_id'].' OR (inb.warehouse_id='.$pickDoc['warehouse_id'].' and r.is_cross_doc=1) )',NULL);
        }

        if(!empty($pl_name)){
            $this->db->join("picking_qty pq", "pq.item_id=ird.item_id");
            $this->db->join("pickings p",'p.pl_id=pq.picking_id');
            $this->db->where('p.name',$pl_name);
        }

        if(!empty($itemId)){
            $this->db->where_in('ird.item_id',$itemId);
        }

        $this->db->select("*, ird.id as item_receiving_detail_id");
        $this->db->from("item_receiving_details ird");
        $this->db->join("locations loc", "loc.id = ird.location_id");
        $this->db->join('location_areas loc_area','loc_area.id=loc.location_area_id','left');
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
        $this->db->join('receivings r','r.id=ir.receiving_id','left');
        $this->db->join('inbound inb','inb.id_inbound=r.po_id','left');
        $this->db->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL);
        $this->db->where("ird.parent_code", $serial_number);
        $this->db->where("(ird.picking_id IS NULL or ird.last_qty >0)");
        $this->db->where_not_in('loc.location_type_id',[6]);
        $this->db->order_by('ird.id','ASC');

        return $this->db->get();

    }

    function get_serial_batch($serial_number,$pl_name="",$itemId=array()){

        $ignore_location = array(100, 101, 102, 103, 104, 105, 106);

        $avail_location = '2 , 3';

        if(!empty($pl_name)){
            $pickDoc = $this->getPickingWarehouse($pl_name);
            $this->db->where('(loc_area.warehouse_id='.$pickDoc['warehouse_id'].' OR (inb.warehouse_id='.$pickDoc['warehouse_id'].' and r.is_cross_doc=1) )',NULL);
        }

        if(!empty($pl_name)){
            $this->db->join("picking_qty pq", "pq.item_id=ird.item_id");
            $this->db->join("pickings p",'p.pl_id=pq.picking_id');
            $this->db->where('p.name',$pl_name);
        }

        if(!empty($itemId)){
            $this->db->where_in('ird.item_id',$itemId);
        }

        $this->db->select("*, ird.id as item_receiving_detail_id");
        $this->db->from("item_receiving_details ird");
        $this->db->join("locations loc", "loc.id = ird.location_id");
        $this->db->join('location_areas loc_area','loc_area.id=loc.location_area_id','left');
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
        $this->db->join('receivings r','r.id=ir.receiving_id','left');
        $this->db->join('inbound inb','inb.id_inbound=r.po_id','left');
        $this->db->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL);
        $this->db->where("ird.kd_batch", $serial_number);
        $this->db->where("(ird.picking_id IS NULL or ird.last_qty >0)");
        $this->db->where_not_in('loc.location_type_id',[6]);
        $this->db->order_by('ird.id','ASC');

        return $this->db->get();

    }

    function retrieve_serial($serial_number,$pickingCode=''){

        $pickDoc = $this->getPickingWarehouse($pickingCode);

        $this->db->select("itm.id as item_id, itm.code as kd_barang, itm.has_qty");
        $this->db->select(["COALESCE(SUM(ird.last_qty),0) as last_qty","COALESCE(loc_area.warehouse_id,inb.warehouse_id) as warehouse_id"]);
        $this->db->select(["ird.last_qty as last_qty"]);
        $this->db->from("item_receiving_details ird");
        $this->db->join('locations loc','loc.id=ird.location_id');
        $this->db->join('location_areas loc_area','loc_area.id=loc.location_area_id','left');
        $this->db->join("items itm", "itm.id=ird.item_id", "left");
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
        $this->db->join('receivings r','r.id=ir.receiving_id','left');
        $this->db->join('inbound inb','inb.id_inbound=r.po_id','left');
        $this->db->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL);
        $this->db->where('(loc_area.warehouse_id='.$pickDoc['warehouse_id'].' OR (inb.warehouse_id='.$pickDoc['warehouse_id'].' and r.is_cross_doc=1) )',NULL);
        $this->db->where("ird.unique_code", $serial_number );
        // $this->db->where("(ird.picking_id IS NULL or ird.last_qty >0)",NULL);
        $this->db->where_not_in('loc.location_type_id',[6]);
        $this->db->group_by('itm.id, itm.code, itm.has_qty, ird.last_qty, loc_area.warehouse_id, inb.warehouse_id');

        return $this->db->get();

    }

    function retrieve_serial_lp($params=array()){

        $pickDoc = $this->getPickingWarehouse($params['pl_name']);

        $this->db->select("itm.id as item_id, itm.code as kd_barang, itm.has_qty");
        $this->db->select(["COALESCE(SUM(ird.last_qty),0) as last_qty","COALESCE(loc_area.warehouse_id,inb.warehouse_id) as warehouse_id"]);
        $this->db->from("item_receiving_details ird");
        $this->db->join('locations loc','loc.id=ird.location_id');
        $this->db->join('location_areas loc_area','loc_area.id=loc.location_area_id','left');
        $this->db->join("items itm", "itm.id=ird.item_id", "left");
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
        $this->db->join('receivings r','r.id=ir.receiving_id','left');
        $this->db->join('inbound inb','inb.id_inbound=r.po_id','left');
        $this->db->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL);
        $this->db->where('(loc_area.warehouse_id='.$pickDoc['warehouse_id'].' OR (inb.warehouse_id='.$pickDoc['warehouse_id'].' and r.is_cross_doc=1) )',NULL);
        $this->db->where("ird.parent_code", $params['kd_unik'] );
        // $this->db->where("(ird.picking_id IS NULL or ird.last_qty >0)",NULL);
        $this->db->group_by('itm.id, itm.code, itm.has_qty, ird.last_qty, loc_area.warehouse_id, inb.warehouse_id');

        $olItem = $this->db->get()->result_array();

        $i = 0;

        if($olItem){

            foreach ($olItem as $ol) {

                $this->db->select("pq.item_id, pq.qty as doc_qty");
                $this->db->select(["COALESCE(SUM(pr.qty),0) as act_qty"]);
                $this->db->from("pickings p");
                $this->db->join('picking_qty pq','pq.picking_id=p.pl_id');
                $this->db->join('picking_recomendation pr','pr.picking_id=p.pl_id and pr.item_id=pq.item_id','left');
                $this->db->where("pq.item_id",$ol['item_id']);
                $this->db->where("p.name",$params['pl_name']);
                $this->db->group_by('pq.item_id, pq.qty');

                $check = $this->db->get()->row_array();

                if($check){
                    if(($check['doc_qty'] - $check['act_qty']) < $ol['last_qty']){
                        $i = $i+1;
                    }else{
                        return true;
                    }

                }else{
                    return false;
                }

            }

            if(count($olItem) == $i){
                return false;
            }

        }

        return false;

    }

    function retrieve_serial_batch($params=array()){

        $pickDoc = $this->getPickingWarehouse($params['pl_name']);

        $this->db->select("itm.id as item_id, itm.code as kd_barang, itm.has_qty");
        $this->db->select(["COALESCE(SUM(ird.last_qty),0) as last_qty","COALESCE(loc_area.warehouse_id,inb.warehouse_id) as warehouse_id"]);
        $this->db->from("item_receiving_details ird");
        $this->db->join('locations loc','loc.id=ird.location_id');
        $this->db->join('location_areas loc_area','loc_area.id=loc.location_area_id','left');
        $this->db->join("items itm", "itm.id=ird.item_id", "left");
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
        $this->db->join('receivings r','r.id=ir.receiving_id','left');
        $this->db->join('inbound inb','inb.id_inbound=r.po_id','left');
        $this->db->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL);
        $this->db->where('(loc_area.warehouse_id='.$pickDoc['warehouse_id'].' OR (inb.warehouse_id='.$pickDoc['warehouse_id'].' and r.is_cross_doc=1) )',NULL);
        $this->db->where("ird.kd_batch", $params['kd_unik'] );
        $this->db->where("(ird.picking_id IS NULL or ird.last_qty >0)",NULL);
        $this->db->group_by('itm.id, itm.code, itm.has_qty, ird.last_qty, loc_area.warehouse_id, inb.warehouse_id');

        $olItem = $this->db->get()->result_array();

        $i = 0;

        if($olItem){

            foreach ($olItem as $ol) {

                $this->db->select("pq.item_id, pq.qty as doc_qty");
                $this->db->select(["COALESCE(SUM(pr.qty),0) as act_qty"]);
                $this->db->from("pickings p");
                $this->db->join('picking_qty pq','pq.picking_id=p.pl_id');
                $this->db->join('picking_recomendation pr','pr.picking_id=p.pl_id and pr.item_id=pq.item_id','left');
                $this->db->where("pq.item_id",$ol['item_id']);
                $this->db->where("p.name",$params['pl_name']);
                $this->db->group_by('pq.item_id, pq.qty');

                $check = $this->db->get()->row_array();

                if($check){
                    if(($check['doc_qty'] - $check['act_qty']) < $ol['last_qty']){
                        $i = $i+1;
                    }else{
                        return true;
                    }

                }else{
                    return false;
                }

            }

            if(count($olItem) == $i){
                return false;
            }

        }

        return false;

    }

/*    function post_serial($data){
        $id_user    = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $id_picking = $this->get_id("pl_id", "name", $data["pl_name"], "pickings")->row_array();
        $id_barang  = $this->get_id("item_id as id_barang", "unique_code", $data["kd_unik"], "item_receiving_details")->row_array();
        $item       = $this->get_id("has_qty", "id", $id_barang["id_barang"], "items")->row_array();
        $serialize  = $this->get_serial($data["kd_unik"])->row_array();
        switch ($item['has_qty']) {
            case 0:
                $data_insert = array(
                                    'unique_code'   => $data["kd_unik"],
                                    'loc_id_old'    => $serialize["location_id"],
                                    'user_id_pick'  => $id_user["user_id"],
                                    'pick_time'     => $data["pick_time"],
                                    'process_name'  => 'PICKING',
                                    'picking_id' => $id_picking["pl_id"]
                                );

                $this->db->insert("transfers", $data_insert);

                $qty_after = $serialize["last_qty"] - $data["qty"];

                $this->db->set("last_qty", $qty_after);
                $this->db->where("id", $serialize["item_receiving_detail_id"]);
                $this->db->where('(putaway_time IS NOT NULL or location_id=107)', NULL);
                $this->db->update("item_receiving_details");


                $this->db->set("picking_id", $id_picking["pl_id"]);
                $this->db->set("location_id", 103);
                $this->db->where("id", $serialize["item_receiving_detail_id"]);
                $this->db->where('(putaway_time IS NOT NULL or location_id=107)', NULL);
                $this->db->update("item_receiving_details");
                break;
            case 1:
                $data_insert = array(
                                    'picking_id'     => $id_picking["pl_id"],
                                    'unique_code'   => $data["kd_unik"],
                                    'first_qty' => $serialize["first_qty"],
                                    'qty_bef'   => $serialize["last_qty"],
                                    'qty_pick'  => $data["qty"],
                                    'qty_after' => $serialize["last_qty"] - $data["qty"],
                                    'user_id'  => $id_user["user_id"],
                                    'picking_time'  => $data["kit_time"]
                                );

                $this->db->insert("picking_history", $data_insert);

                if($data_insert["qty_after"] == 0){
                    $this->db->set('location_id',101);
                }
                $this->db->set("last_qty", $data_insert["qty_after"]);
                $this->db->set("picking_id", $id_picking["pl_id"]);
                $this->db->where("id", $serialize["item_receiving_detail_id"]);
                $this->db->where('(putaway_time IS NOT NULL or location_id=107)', NULL);
                $this->db->update("item_receiving_details");
                break;
            default:
                # nothing to do.
                break;
        }

        if($data['qty']==0 || $data['qty'] == NULL){
            $qty_insert = 1;
        }else{
            $qty_insert = $data['qty'];
        }

        $data_insert = [
            "picking_id" => $id_picking['pl_id'],
            "unique_code" => $data['kd_unik'],
            "item_id" => $id_barang['id_barang'],
            "old_location_id" => $serialize['location_id'],
            "location_id" => 101,
            "qty" => $qty_insert,
            'user_id'   => $id_user['user_id'],
            'st_picking'    => 0,
            'item_receiving_detail_id' => $serialize['item_receiving_detail_id']
        ];

        $this->db->insert('picking_recomendation',$data_insert);
    }

    function post_serial_LP($data){

        $id_user    = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $id_picking = $this->get_id("pl_id", "name", $data["pl_name"], "pickings")->row_array();

        $getItemAvailable = $this->get_count($data,true);
        $serialize  = $this->get_serial_lp($data["kd_unik"],$data['pl_name'],$getItemAvailable)->result_array();
        if($serialize){
            foreach($serialize as $serial){

                $item = $this->get_id("has_qty", "id", $serial["item_id"], "items")->row_array();

                if($item['has_qty']){

                    $data_insert = array(
                                        'picking_id'     => $id_picking["pl_id"],
                                        'unique_code'   => $serial["unique_code"],
                                        'first_qty' => $serial["first_qty"],
                                        'qty_bef'   => $serial["last_qty"],
                                        'qty_pick'  => $serial["last_qty"],
                                        'qty_after' => 0,
                                        'user_id'  => $id_user["user_id"],
                                        'picking_time'  => $data["kit_time"]
                                    );

                    $this->db->insert("picking_history", $data_insert);

                    if($data_insert["qty_after"] == 0){
                        $this->db->set('location_id',101);
                    }

                    $this->db->set("last_qty", $data_insert["qty_after"]);
                    $this->db->set("picking_id", $id_picking["pl_id"]);
                    $this->db->where("id", $serial["item_receiving_detail_id"]);
                    $this->db->update("item_receiving_details");


                }else{

                    $data_insert = array(
                                        'unique_code'   => $serial["unique_code"],
                                        'loc_id_old'    => $serial["location_id"],
                                        'user_id_pick'  => $id_user["user_id"],
                                        'pick_time'     => $data["pick_time"],
                                        'process_name'  => 'PICKING',
                                        'picking_id' => $id_picking["pl_id"]
                                    );

                    $this->db->insert("transfers", $data_insert);

                    $this->db->set("last_qty", 0);
                    $this->db->set("picking_id", $id_picking["pl_id"]);
                    $this->db->set("location_id", 103);
                    $this->db->where("id", $serial["item_receiving_detail_id"]);
                    $this->db->update("item_receiving_details");
                    break;

                }

                if($data['qty']==0 || $data['qty'] == NULL){
                    $qty_insert = 1;
                }else{
                    $qty_insert = $data['qty'];
                }

                $data_insert = [
                    "picking_id" => $id_picking['pl_id'],
                    "unique_code" => $serial['unique_code'],
                    "item_id" => $serial['item_id'],
                    "old_location_id" => $serial['location_id'],
                    "location_id" => 101,
                    "qty" => $qty_insert,
                    'user_id'   => $id_user['user_id'],
                    'st_picking'    => 0,
                    'item_receiving_detail_id' => $serial['item_receiving_detail_id']
                ];

                $this->db->insert('picking_recomendation',$data_insert);


            }
        }

    }*/

    function get_item($item_code){
        $this->db->select("*");
        $this->db->where("code", $item_code);
        $this->db->from("items");
        return $this->db->get();
    }

    function post($data){

        $id_picker  = $this->get_id("id as user_id", "user_name", $data["uname_pick"], "users")->row_array();
        $id_putter  = $this->get_id("id as user_id", "user_name", $data["uname_put"], "users")->row_array();
        $loc_new    = $this->get_id("id as loc_id", "lower(name)", strtolower($data["new_loc"]), "locations")->row_array();
        // dd($loc_new['loc_id']);

        $this->db
            ->select('ird.id as item_receiving_detail_id, p.pl_id, pr.unique_code, pr.old_location_id as location_id, pr.item_id')
            ->from('picking_recomendation pr')
            ->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id')
            ->join('pickings p','p.pl_id=pr.picking_id')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
            ->join("users usr",'usr.id=pr.user_id')
            ->where('p.name',$data['picking_code'])
            // ->where('pr.location_id <>',104)
            //->where('usr.user_name',$this->input->get_request_header('User', true));
			->where('usr.id',$id_picker['user_id']);
            // ->where("(pr.unique_code='".$data['kd_unik']."' or ird.parent_code='".$data['kd_unik']."')",NULL);
		
        $serialize = $this->db->get()->result_array();

        if($serialize){
            foreach($serialize as $serial){

                $item    = $this->get_id("has_qty", "id", $serial["item_id"], "items")->row_array();

                if ($item["has_qty"] == 1) {

                    $this->db->set("picking_id", $serial["pl_id"]);
                    $this->db->set("pl_status", 1);
                    $this->db->set("picking_time", "NOW()");
                    // $this->db->set("picking_time", $data["pick_time"]);
                    $this->db->set("user_id_picking", $id_picker["user_id"]);
                    $this->db->where('id',$serial['item_receiving_detail_id']);
                    $this->db->update("item_receiving_details");

                } else{

                    $this->db->set("location_id", $loc_new["loc_id"]);
                    $this->db->set("picking_id", $serial["pl_id"]);
                    $this->db->set("pl_status", 1);
                    $this->db->set("picking_time", "NOW()");
                    // $this->db->set("picking_time", $data["pick_time"]);
                    $this->db->set("user_id_picking", $id_picker["user_id"]);
                    $this->db->where('id',$serial['item_receiving_detail_id']);
                    $this->db->update("item_receiving_details");

                }

                $this->db->set("loc_id_new", $loc_new["loc_id"]);
                $this->db->set("user_id_put", $id_putter["user_id"]);
                $this->db->set("put_time", "NOW()");
                $this->db->set("process_name", "PICKING");
                $this->db->where("unique_code", $serial["unique_code"]);
                $this->db->where("loc_id_old", $serial["location_id"]);
                $this->db->where("user_id_pick", $id_picker["user_id"]);
                $this->db->where("loc_id_new IS NULL");
                $this->db->where("user_id_put IS NULL");
                $this->db->update("transfers");

                $this->db->set('st_picking',1);
                $this->db->set('location_id',$loc_new['loc_id']);
                $this->db->where('st_picking',0);
                $this->db->where('unique_code',$serial['unique_code']);
                $this->db->where('picking_id',$serial['pl_id']);
                $this->db->where('user_id',$id_picker['user_id']);
                $this->db->update('picking_recomendation');

            }
        }
    }

    function post_LP($data){

        $id_picker  = $this->get_id("id as user_id", "user_name", $data["uname_pick"], "users")->row_array();
        $id_putter  = $this->get_id("id as user_id", "user_name", $data["uname_put"], "users")->row_array();
        $loc_new    = $this->get_id("id as loc_id", "lower(name)", strtolower($data["new_loc"]), "locations")->row_array();

        $this->db
            ->from('picking_recomendation pr')
            ->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id')
            ->join('pickings p','p.pl_id=pr.picking_id')
            ->where('p.name',$data['picking_code'])
            ->where("(pr.unique_code='".$data['kd_unik']."' or ird.parent_code='".$data['kd_unik']."')",NULL);

        $serialize = $this->db->get()->result_array();

        if($serialize){
            foreach($serialize as $serial){

                $item    = $this->get_id("has_qty", "id", $serial["item_id"], "items")->row_array();

                if ($item["has_qty"] == 1) {

                    $this->db->set("picking_id", $serial["pl_id"]);
                    $this->db->set("pl_status", 1);
                    // $this->db->set("picking_time", $data["pick_time"]);
                    $this->db->set("picking_time", "NOW()");
                    $this->db->set("user_id_picking", $id_picker["user_id"]);
                    $this->db->where("item_id", $serial['item_id']);
                    $this->db->where("picking_id", $serial["pl_id"]);
                    $this->db->where("unique_code", $serial["unique_code"]);
                    $this->db->update("item_receiving_details");

                } else{

                    $this->db->set("location_id", $loc_new["loc_id"]);
                    $this->db->set("picking_id", $serial["pl_id"]);
                    $this->db->set("pl_status", 1);
                    $this->db->set("picking_time", "NOW()");
                    // $this->db->set("picking_time", $data["pick_time"]);
                    $this->db->set("user_id_picking", $id_picker["user_id"]);
                    $this->db->where("picking_id", $id_picking["pl_id"]);
                    $this->db->where("unique_code", $serial["unique_code"]);
                    $this->db->update("item_receiving_details");

                }

                $this->db->set("loc_id_new", $loc_new["loc_id"]);
                $this->db->set("user_id_put", $id_putter["user_id"]);
                $this->db->set("put_time", $data["put_time"]);
                $this->db->set("process_name", "PICKING");
                $this->db->where("unique_code", $serial["unique_code"]);
                $this->db->where("loc_id_old", $serial["location_id"]);
                $this->db->where("user_id_pick", $id_picker["user_id"]);
                $this->db->where("loc_id_new IS NULL");
                $this->db->where("user_id_put IS NULL");
                $this->db->update("transfers");

                $this->db->set('st_picking',1);
                $this->db->set('location_id',$loc_new['loc_id']);
                $this->db->where('st_picking',0);
                $this->db->where('unique_code',$serial['unique_code']);
                $this->db->where('picking_id',$serial['pl_id']);
                $this->db->where('user_id',$id_picker['user_id']);
                $this->db->update('picking_recomendation');

            }
        }

    }

    function update_location($picking_code,$data){
        $id_picking = $this->get_id("pl_id", "name", $picking_code, "pickings")->row_array();
        $id_picker  = $this->get_id("id as user_id", "user_name", $data["uname_pick"][0], "users")->row_array();
        $loc_new    = $this->get_id("id as loc_id", "name", $data["new_loc"][0], "locations")->row_array();
        $id_putter  = $this->get_id("id as user_id", "user_name", $data["uname_put"][0], "users")->row_array();

        $this->db->set("pl_status", 1);
        $this->db->set("picking_time", "NOW()");
        // $this->db->set("picking_time", $data["pick_time"][0]);
        $this->db->set("user_id_picking", $id_picker["user_id"]);

        $this->db->set("location_id", 101);
        $this->db->where("picking_id", $id_picking["pl_id"]);
        $this->db->where("first_qty",1);
        $this->db->update("item_receiving_details");

        $this->db->set("loc_id_new", 101);
        $this->db->set("user_id_put", $id_putter["user_id"]);
        $this->db->set("put_time", $data["put_time"][0]);
        $this->db->set("process_name", "PICKING");
        $this->db->where("picking_id",$id_picking["pl_id"]);
        $this->db->update("transfers");
    }

    function submit_recomendation($data){
        $id_picking = $this->get_id("pl_id", "pl_name", $data["picking_code"], "picking_list")->row_array();
        $id_barang  = $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
        $loc_new    = $this->get_id("loc_id", "loc_name", $data["new_loc"], "m_loc")->row_array();

        $q =    "INSERT INTO
                    picking_recomendation
                    (id_picking,kd_unik,id_barang,loc_id,qty)
                VALUES
                    ('". $id_picking["pl_id"] ."','". $data['kd_unik'] ."','". $id_barang["id_barang"] ."','". $loc_new["loc_id"] ."','". $data['qty'] ."')
                ";

        $this->db->query($q, false);
    }

    function get_location($location){
        $this->db->select("id as loc_id, name as loc_name");
        $this->db->from("locations");
        $this->db->where("lower(name)", strtolower($location));
        $this->db->where("loc_type", "OUTBOUND");
        return $this->db->get();
    }

    function loc_name($serial_number){

        $this->db
            ->select('l.name as loc_name')
            ->from('item_receiving_details ird')
            ->join('locations l','l.id=ird.location_id','left')
            ->where('unique_code', $serial_number);

        $row    = $this->db->get()->row_array();

        $result = (isset($row)) ? $row['loc_name'] : NULL;

        return $result;

    }

    function start($picking_code){
        $this->db->set("start_time", "COALESCE(start_time, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->set('status_id',7);
        $this->db->where("name", $picking_code);
        $this->db->update("pickings");
    }

    function finish($picking_code){
        $update = false;
        $sql = "select sum(a.pq_qty) pq_qty,sum(picked_qty) as picked_qty from (select pq.qty pq_qty, coalesce(sum(pr.qty), 0) picked_qty from picking_qty pq
        left join picking_recomendation pr on pr.order_item_id = pq.item_id and pq.picking_id = pr.picking_id
        left join pickings p on p.pl_id = pq.picking_id 
        left join items itm on itm.id = pq.item_id 
        left join outbound o on o.id = pr.outbound_id 
        where p.name = '".$picking_code."'
        group by p.name, o.code, pq.qty) a";

        $balance = $this->db->query($sql)->row_array();
        if($balance['pq_qty'] == $balance['picked_qty']){
            $this->db->set("pl_status", 1);
            $this->db->set("status_id", 8);
            $this->db->set("end_time", date("Y-m-d H:i:s"));
            $this->db->where("name", $picking_code);
            $this->db->where("pl_status", 0);
            $update = $this->db->update("pickings");
        }

        if($update){
            return true;
        }else{
            return false;
        }
    }

    function get_document($username = ''){

        /*$this->db->select([
            "DISTINCT(pl.name) as pl_name",
            "pl.pl_id as pl_id",
	    "l.location_area_id",
            "sum(qty) as qty",
            "COALESCE(
                (SELECT
                    string_agg(DISTINCT d.destination_name, ', ') as destination_name
                FROM picking_list_outbound plo
                JOIN outbound outb ON outb.id=plo.id_outbound
                JOIN destinations d ON d.destination_id=outb.destination_id
                WHERE plo.pl_id=pl.pl_id
                GROUP BY pl_id
                HAVING count(*)=1)
            ,'Consolidate Picking') as destination
            "
        ]);*/
		$user_id = $this->db
							->select('zone_id')
							->from('users_zone uz')
							->join('users u', 'u.id = uz.user_id')
							->where('u.user_name', $username)
							->get()
							->result_array();
        $zone_id = array_column($user_id, 'zone_id');
                            

		$this->db->select([
            "DISTINCT(pl.name) as pl_name",
            "pl.pl_id as pl_id",
			"LEFT(itm.item_area,1) AS item_area",
            "COALESCE(d.destination_name,s.source_name) as destination",
            "COALESCE(g.gate_name,'-') as gate_id",
        ]);
        $this->db->from("pickings pl");
        $this->db->join("picking_qty pq","pq.picking_id = pl.pl_id");
        $this->db->join("items itm", "itm.id = pq.item_id", "left");
		$this->db->join('item_receiving_details ird','ird.item_id=itm.id','left');
        $this->db->join('locations l','l.id=ird.location_id','left');
		$this->db->join('picking_list_outbound plo','plo.pl_id=pl.pl_id');
		$this->db->join('outbound outb','outb.id=plo.id_outbound');
		$this->db->join('destinations d','outb.destination_id=d.destination_id','LEFT');
		$this->db->join('gate g','d.gate = g.id','LEFT');
		$this->db->join('sources s','outb.destination_id=s.source_id','LEFT');
        $this->db->where("pl.pl_status", 0);
		// $this->db->where_in("l.location_area_id", $zone_id);
        $this->db->group_by('pl.name,itm.item_area,g.id,pl.pl_id,d.destination_name,s.source_name,l.location_area_id');
        $this->db->order_by('pl.pl_id','desc');
		
		// $this->db->get()->result_array();
		// dd($this->db->last_query());

        return $this->db->get()->result_array();

    }

    function getSnByPl($username,$pickingCode){
      // dd([$username, $pickingCode]);

        $this->db
            ->select('pr.*, itm.code as item_code')
            ->from('picking_recomendation pr')
            ->join('pickings p','p.pl_id=pr.picking_id')
            ->join('users usr','usr.id=pr.user_id')
            ->join('items itm','itm.id=pr.item_id')
            ->where('usr.user_name',$username)
            ->where('p.name',$pickingCode)
            ->where('st_picking',0);

        $data = $this->db->get();
        return $data;
    }
/*
    function sendToStaging($pickingCode){

        $user    = (!empty($this->input->get_request_header('User', true))) ? $this->input->get_request_header('User', true) : $this->session->userdata('username');

        $this->load_model('api_staging_model');

        $this->db
            ->select(['outb.id as outbound_id','outb.code as outbound_code','outb.date as outbound_date','outb.note as shipping_note', 'itm.id as item_id', 'p.pl_id as picking_id',
                'p.name as picking_code','doc.name as document_name','doc.movement_type',"doc.code as document_code","outb.document_id as outbound_document","itm.sku as item_sku", "unt.code as unit_code", "cost.code as cost_center_code", "COALESCE(SUM(pr.qty),0) as qty", "wg.plant_sap",
                "wg.storage_location_sap","spl.code as supplier_code","dwg.plant_sap as plant_sap_dest","dwg.storage_location_sap as storage_location_sap_dest","usr.user_name as outbound_user","usr.id as outbound_user_id","outb.destination_id as outbound_destination","outb.warehouse_id as outbound_warehouse","outb.agent_app_transaction_category_id","itm.preorder"])
            ->from('pickings p')
            ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
            ->join('outbound outb','outb.id=plo.id_outbound')
            ->join('documents doc','doc.document_id=outb.document_id')
            ->join('suppliers spl',"spl.id=outb.destination_id and doc.movement_type IN ('909')",'left')
            ->join('warehouses wh','wh.id=outb.warehouse_id','left')
            ->join('warehouses_warehouse_groups wwg','wwg.warehouse_id=wh.id','left')
            ->join('warehouse_groups wg','wg.id=wwg.warehouse_group_id','left')
            ->join('warehouses dwh',"dwh.id=outb.destination_id and doc.movement_type IN ('351')",'left')
            ->join('warehouses_warehouse_groups dwwg','dwwg.warehouse_id=dwh.id','left')
            ->join('warehouse_groups dwg','dwg.id=dwwg.warehouse_group_id','left')
            ->join('cost_center cost','cost.id=outb.cost_center_id','left')
            ->join('picking_recomendation pr','pr.picking_id=p.pl_id','left')
            ->join("items itm",'itm.id=pr.item_id','left')
            ->join("units unt",'unt.id=itm.unit_id','left')
            ->join('users usr','usr.id=outb.user_id','left')
            ->where('p.name',$pickingCode)
            ->group_by('outb.id, outb.date, outb.code, outb.note, p.pl_id, p.name, doc.name, doc.movement_type, itm.sku, unt.code, cost.code, wg.plant_sap, wg.storage_location_sap, outb.document_id, spl.code, doc.code, dwg.plant_sap, dwg.storage_location_sap, usr.user_name, itm.id, usr.id')
            ->order_by('itm.sku');

        $data = $this->db->get()->result_array();

        if($data){

            if($data[0]['movement_type'] == 351 || $data[0]['movement_type'] == 311){

                $this->load_model('inbound_model');

                $dataInbound = array(
                    'kd_inbound'            =>  $data[0]['outbound_code'],
                    'tanggal_inbound'       =>  $data[0]['outbound_date'],
                    'id_supplier'           =>  $data[0]['outbound_warehouse'],
                    'id_inbound_document'   =>  $data[0]['outbound_document'],
                    'user_id'               =>  $data[0]['outbound_user_id'],
                    'remark'                =>  '',
                    'warehouse'             =>  $data[0]['outbound_destination']
                );

                date_default_timezone_set('UTC');

                $pstngDate = strtotime("now");
                $pstngDate = $pstngDate*1000;
                $pstngDate = number_format($pstngDate, 0, '.', '');

                $outboundDate = strtotime($data[0]['outbound_date']);
                $outboundDate = $outboundDate*1000;
                $outboundDate = number_format($outboundDate, 0, '.', '');

                $post['Extid']      = $data[0]['picking_code']."#".$user."##";
                $post['DocType']    = "X006"; // Ex: X006 harus di mapping
                $post['CreatDate']  = "/Date($pstngDate)/";
                $post['PurchOrg']  = "ID06"; // Ex: ID06 harus dimapping
                $post['PurGroup']  = "011"; // Ex: 011 harus di mapping
                $post['DocDate']  = "/Date($outboundDate)/";
                $post['SupplPlnt']  = $data[0]['plant_sap']; // Ex: 0601

                $poItem = 10;
                $i=0;
                foreach ($data as $d) {

                    $post['NV_STOITEM'][$i]['Extid']     = $d['picking_code']."#".$user;
                    $post['NV_STOITEM'][$i]['PoItem']    = "$poItem" ;
                    $post['NV_STOITEM'][$i]['Material']  = $d['item_sku'];
                    $post['NV_STOITEM'][$i]['Plant']     = $d['plant_sap_dest'];
                    $post['NV_STOITEM'][$i]['StgeLoc']   = $d['storage_location_sap_dest'];
                    $post['NV_STOITEM'][$i]['Quantity']  = $d['qty'];
                    $post['NV_STOITEM'][$i]['PoUnit']    = $d['unit_code'];
                    $post['NV_STOITEM'][$i]['ValType']   = 'NORMAL';

                    $post['NV_STOSCHEDULE'][$i]['Extid']        = $d['picking_code']."#".$user;
                    $post['NV_STOSCHEDULE'][$i]['PoItem']       = "$poItem" ;
                    $post['NV_STOSCHEDULE'][$i]['SchedLine']    = "1"; // pasti 1
                    $post['NV_STOSCHEDULE'][$i]['DelDatcatExt'] = "D" ; // Pasti D
                    $post['NV_STOSCHEDULE'][$i]['DeliveryDate'] = date("d.m.Y") ; // ? Ex: 31.08.2019

                    $dataInboundItems[] = array(
                        'jumlah_barang' => $d['qty'],
                        'id_barang'     => $d['item_id'],
                        'po_item'       => $poItem
                    );

                    $poItem += 10;
                    $i++;
                }

                $api = "stock_mvt/STOHEADERSet";

                $ref = array(
                    'refCode'   => $data[0]['picking_code']." - PO_STO",
                    'refId'     => $data[0]['picking_id']
                );

                if($data[0]['movement_type'] == 351){

                    $stagingId = $this->api_staging_model->sendToStaging(json_encode($post),$api,$ref);
                    $this->api_staging_model->sync($stagingId);

                    $staging = $this->db->get_where('staging',['refCode'=>$d['picking_code']." - PO_STO"])->row_array();
                    if($staging){
                        $dataInbound['po_number'] = $staging['po_number'];
                    }

                }

                $inbound = $this->inbound_model->check_inbound($data[0]['outbound_code'])->row();
                if(count($inbound) > 0){

                    $this->inbound_model->update($dataInbound,$dataInboundItems,$inbound->id_inbound);

                }else{
                    $this->inbound_model->create($dataInbound,$dataInboundItems);
                }

            }

            $pstngDate = strtotime("now");
            $pstngDate = $pstngDate*1000;
            $pstngDate = number_format($pstngDate, 0, '.', '');

            $outboundDate = strtotime($data[0]['outbound_date']);
            $outboundDate = $outboundDate*1000;
            $outboundDate = number_format($outboundDate, 0, '.', '');

            $HeaderTxt = explode("#", $data[0]['shipping_note']);
            $HeaderTxt = (is_array($HeaderTxt)) ? $HeaderTxt[0] : $data[0]['shipping_note'];
            $HeaderTxt = substr($HeaderTxt, 0,25);

            $post = array(
                'Extid'     => $data[0]['picking_code']."#".$user,
                'PstngDate' => "/Date($pstngDate)/",
                'DocDate'   => "/Date($outboundDate)/",
            );

            $post['HeaderTxt'] = "GI ".$data[0]['document_name'];

            $poItem = 10;
            $i=0;
            foreach ($data as $d) {

                if($d['movement_type'] == 911 or $d['movement_type'] == 913){
                    if($d['preorder'] == 1){
                        continue;
                    }
                }

                $post['NV_GIITEM'][$i]['Extid']     = $d['picking_code'];
                $post['NV_GIITEM'][$i]['Material']  = $d['item_sku'];
                $post['NV_GIITEM'][$i]['Plant']     = $d['plant_sap'];
                $post['NV_GIITEM'][$i]['StgeLoc']   = $d['storage_location_sap'];
                $post['NV_GIITEM'][$i]['MoveType']  = $d['movement_type'];
                $post['NV_GIITEM'][$i]['ValType']   = 'NORMAL';
                $post['NV_GIITEM'][$i]['EntryQnt']  = $d['qty'];
                $post['NV_GIITEM'][$i]['EntryUom']  = $d['unit_code'];
                $post['NV_GIITEM'][$i]['ItemText']  = (empty($d['shipping_note'])) ? "" : substr($d['shipping_note'],0,50);
                $post['NV_GIITEM'][$i]['Costcenter'] = $d['cost_center_code'];

                if($d['movement_type'] == 913){
                    $post['NV_GIITEM'][$i]['Costcenter'] = 'ID06RIDE01';
                    $post['NV_GIITEM'][$i]['Dname'] = 'DRIVER';
                }

                if($d['movement_type'] == 911){
                    $post['NV_GIITEM'][$i]['Costcenter'] = 'ID06RIDE01';
                    $post['NV_GIITEM'][$i]['Dname'] = 'DRIVER';
                }

                if($d['movement_type']  == 913 || $d['movement_type'] == 911){
                    if($d['agent_app_transaction_category_id'] == 1){
                        $post['Extid'] = $d['picking_code']."#".$user."#".$d['outbound_code']."##X";
                        $post['NV_GIITEM'][$i]['Extid']     = $d['picking_code']."#".$user."#".$d['outbound_code']."##X";
                    }else if($d['agent_app_transaction_category_id'] == 2){
                        $post['Extid'] = $d['picking_code']."#".$user."#".$d['outbound_code']."#X#";
                        $post['NV_GIITEM'][$i]['Extid']     = $d['picking_code']."#".$user."#".$d['outbound_code']."#X#";
                    }else{
                        $post['Extid'] = $d['picking_code']."#".$user."#".$d['outbound_code']."##";
                        $post['NV_GIITEM'][$i]['Extid']     = $d['picking_code']."#".$user."#".$d['outbound_code']."##";
                    }

                }

                if($d['movement_type'] == 909){
                    $post['NV_GIITEM'][$i]['VendorId'] = $d['supplier_code'];
                }

                if($d['movement_type'] == 901){
                    $post['HeaderTxt'] = "GI Replacement#";
                }

                if($d['movement_type'] == 905){

                    if($d['document_code'] == "IMM"){
                        $post['HeaderTxt'] = "GI Internal MM#";
                    }else{
                        $post['HeaderTxt'] = "GI Operational#";
                    }
                }

                if($d['movement_type'] == 907){
                    $post['HeaderTxt'] = "GI Collateral#";
                }

                if($d['movement_type'] == 351){

                    $staging = $this->db->get_where('staging',['refCode'=>$d['picking_code']." - PO_STO"])->row_array();

                    $post['NV_GIITEM'][$i]['PONumber'] = $staging['po_number'];
                    $post['NV_GIITEM'][$i]['POItem']   = "$poItem";

                    unset($post['NV_GIITEM'][$i]['Costcenter']);


                    $poItem += 10;
                }

                $i++;
            }

            $api = "stock_mvt/GIHEADERSet";

            if($d['movement_type'] != 311){

                if(isset($post['NV_GIITEM'])){

                    $ref = array(
                        'refCode'   => $data[0]['picking_code'],
                        'refId'     => $data[0]['picking_id']
                    );

                    $stagingId = $this->api_staging_model->sendToStaging(json_encode($post),$api,$ref);
                    $this->api_staging_model->sync($stagingId);

                }

            }

        }

    }

*/

    function sendToStaging(){
        return true;
    }

    function getPickingWarehouse($pickingCode){

        $this->db
            ->select('p.*, wh.id as warehouse_id')
            ->from('pickings p')
            ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
            ->join('outbound outb','outb.id=plo.id_outbound')
            ->join('warehouses wh','wh.id=outb.warehouse_id')
            ->where('p.name',$pickingCode);

        $picking = $this->db->get();

        return $picking->row_array();
    }

    function getPickingRecomendation($pickingCode=''){

        $this->db
            ->select("pq.item_id")
            ->from('pickings p')
            ->join('picking_qty pq','pq.picking_id=p.pl_id')
            ->where('p.name',$pickingCode);

        $items = $this->db->get()->result_array();

        $d = array();

        foreach ($items as $itm) {

            $this->db
                ->select('itm.code, ird.unique_code, loc.name as location_name, (CASE WHEN itm.shipment_id=1 THEN min(tgl_in) ELSE min(tgl_exp) END) as date')
                ->from('item_receiving_details ird')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('items itm','itm.id=ird.item_id')
                ->where('ird.item_id',$itm['item_id'])
                ->where('last_qty >',0)
                ->group_by('itm.code, ird.unique_code,loc.name, itm.shipment_id')
                ->limit(1);

            $d[] = $this->db->get()->row_array();

        }

        return $d;

        // $data = $this->db->get()->result_array();

        // return $data;

    }

    function get_picking_recomendation_by_sn($uniqueCode=""){

        $data = array(
            'unique_code'   => '',
            'location'      => '',
            'status'        => false
        );

        if($uniqueCode != ""){

            $this->db->start_cache();

            $this->db
                ->select(["itm.code","ird.unique_code","ird.parent_code","ird.kd_batch","loc.name as location_name","rfid_tag","TO_CHAR( ( CASE WHEN itm.shipment_id=1 THEN min(tgl_in) ELSE min(tgl_exp) END ), 'YYYY-MM' )  as date"])
                ->from('item_receiving_details ird')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('items itm','itm.id=ird.item_id')
                ->where_not_in('loc.id',[100,102,101,103,104,106])
                ->where_not_in('loc.location_type_id',[4,1,6])
                ->where("ird.item_id IN (SELECT item_id FROM item_receiving_details where unique_code='$uniqueCode' or parent_code='$uniqueCode' or kd_batch='$uniqueCode')",NULL)
                ->where('last_qty >',0)
                ->group_by('itm.code, ird.unique_code,loc.name, itm.shipment_id,rfid_tag, ird.parent_code, ird.kd_batch')
                ->order_by('date');

            $this->db->stop_cache();

            $this->db->limit(1);
            $dr = $this->db->get()->row_array();

            $tbl = $this->db->get_compiled_select();

            $this->db->flush_cache();

            $sql = "SELECT * FROM ($tbl) as tbl WHERE (unique_code='$uniqueCode' or parent_code='$uniqueCode' or kd_batch='$uniqueCode') and date='".$dr['date']."' LIMIT 5";

            $d = $this->db->query($sql)->row_array();

            if($d){

                $data = array(
                    'unique_code'   => $d['unique_code'],
                    'status'        => true
                );

            }else{

                $sql = "SELECT * FROM ($tbl) as tbl WHERE date='".$dr['date']."' LIMIT 5";
                $ds = $this->db->query($sql)->result_array();

                $data = array(
                    'data'          => $ds,
                    'status'        => false
                );

            }

        }

        return $data;

    }


    /* Hady Pratama */

    function getType($serialNumber=''){

        $result = false;

        if(!empty($serialNumber)){

            $this->db->select("ird.*");
            $this->db->from("item_receiving_details ird");
            $this->db->join("items itm", "itm.id=ird.item_id", "left");
            $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
            $this->db->join('receivings r','r.id=ir.receiving_id','left');
            $this->db->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL);
            $this->db->where("(ird.picking_id IS NULL or ird.last_qty >0)");
            $this->db->where("(ird.unique_code='$serialNumber' or ird.parent_code='$serialNumber' or kd_batch='$serialNumber')",NULL);
            $this->db->limit(1);

            $data = $this->db->get()->row_array();

            if($data){

                switch ($serialNumber) {
                    case $data['unique_code']:
                        $result="serial_number";
                        break;

                    case $data['parent_code']:
                        $result="outer_label";
                        break;

                    case $data['kd_batch']:
                        $result="batch";
                        break;

                    default:
                        # code...
                        break;
                }

            }

        }

        return $result;

    }

    function post_serial($data){

        $this->db->trans_start();
        $res = array();


        $id_user    = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $id_picking = $this->get_id("pl_id", "name", $data["pl_name"], "pickings")->row_array();
        $id_barang  = $this->get_id("item_id as id_barang", "unique_code", $data["kd_unik"], "item_receiving_details")->row_array();
        $item       = $this->get_id("*", "id", $id_barang["id_barang"], "items")->row_array();
        $actual_item_req = $data["item_ori"];

        switch ($data['unit_id']) {
            case $item['unit_id']:
                $data['qty'] = $data['qty'];
                break;

            case $item['convert_qty_unit_id_1']:
                $data['qty'] = $data['qty']/$item['convert_qty_1'];
                break;

            case $item['convert_qty_unit_id_2']:
                $data['qty'] = $data['qty']/$item['convert_qty_2'];
                break;

            default:
                # code...
                break;
        }

        if(!empty($data['outbound_code'])){
                $outboundId     = $this->get_id("id","code",$data['outbound_code'],"outbound")->row()->id;
        }else{
                $outboundId     = NULL;
        }

        if(!empty($data['tray_location'])){
            //$trayLocation = $this->get_id("id","name",$data['tray_location'],"locations")->row_array();
			$trayLocation = $this->db->select('id')->from('locations')->where('name',$data['tray_location'])->where('status','ACTIVE')->get()->row_array();
			$inTransit = (!empty($trayLocation)) ? $trayLocation['id'] : 'NONE';
        }else{
            $inTransit = 'NONE';
        }

		if($inTransit !== 'NONE'){
			$serialize  = $this->get_serial($data["kd_unik"])->row_array();
			if($serialize){
				switch ($item['has_qty']) {
					case 0:
						$data_insert = array(
											'unique_code'   => $data["kd_unik"],
											'loc_id_old'    => $serialize["location_id"],
											'user_id_pick'  => $id_user["user_id"],
											// 'pick_time'     => $data["pick_time"],
											'pick_time'     => "NOW()",
											'process_name'  => 'PICKING',
											'picking_id' => $id_picking["pl_id"]
										);

						$this->db->insert("transfers", $data_insert);

						$qty_after = $serialize["last_qty"] - $data["qty"];

						$this->db->set("last_qty", $qty_after);
						$this->db->where("id", $serialize["item_receiving_detail_id"]);
						$this->db->where('(putaway_time IS NOT NULL or location_id=107)', NULL);
						$this->db->update("item_receiving_details");


						$this->db->set("picking_id", $id_picking["pl_id"]);
						$this->db->set("location_id", $inTransit);
						$this->db->where("id", $serialize["item_receiving_detail_id"]);
						$this->db->where('(putaway_time IS NOT NULL or location_id=107)', NULL);
						$this->db->update("item_receiving_details");
						break;
					case 1:
						$data_insert = array(
											'picking_id'     => $id_picking["pl_id"],
											'unique_code'   => $data["kd_unik"],
											'first_qty' => $serialize["first_qty"],
											'qty_bef'   => $serialize["last_qty"],
											'qty_pick'  => $data["qty"],
											'qty_after' => $serialize["last_qty"] - $data["qty"],
											'user_id'  => $id_user["user_id"],
											// 'picking_time'  => $data["kit_time"]
											'picking_time'  => "NOW()"
										);

						$this->db->insert("picking_history", $data_insert);

						if($data_insert["qty_after"] == 0){
							$this->db->set('location_id',$inTransit);
						}

						$this->db->set("last_qty", $data_insert["qty_after"]);
						$this->db->set("picking_id", $id_picking["pl_id"]);
						$this->db->where("id", $serialize["item_receiving_detail_id"]);
						$this->db->where('(putaway_time IS NOT NULL or location_id=107)', NULL);
						$this->db->update("item_receiving_details");
						break;
					default:
						# nothing to do.
						break;
				}

				if($data['qty']==0 || $data['qty'] == NULL){
					$qty_insert = 1;
				}else{
					$qty_insert = $data['qty'];
				}
				$check = $this->db->get_where('picking_recomendation',['unique_code' => $data['kd_unik'], 'picking_id' => $id_picking['pl_id'], 'outbound_id'   => $outboundId, 'order_item_id' => $actual_item_req]);
				
				if($check->num_rows() > 0) {
					$datas = $check->row_array();
					$qty = $datas['qty'] + $qty_insert;
					$this->db->set("qty",$qty);
					$this->db->where("id", $datas['id']);
					$this->db->update("picking_recomendation");
				} else {
					$data_insert = [
						"picking_id" => $id_picking['pl_id'],
						"unique_code" => $data['kd_unik'],
						"item_id" => $id_barang['id_barang'],
						"old_location_id" => $serialize['location_id'],
						"location_id" => $inTransit,
						"qty" => $qty_insert,
						'user_id'   => $id_user['user_id'],
						'st_picking'    => 0,
						'item_receiving_detail_id' => $serialize['item_receiving_detail_id'],
						"outbound_id"   => $outboundId,
						'unit_id'   => $data['unit_id'],
						'order_item_id' => $actual_item_req
					];
					
					$this->db->insert('picking_recomendation',$data_insert);
				}
				
				$res['status'] = true;
				$this->load_model('api_integration_model');
				$this->api_integration_model->post_picking($data["pl_name"], $data['kd_unik']);

			}else{

				$res['status'] = false;
				$res['message'] = $data["kd_unik"].' not available';

			}
		}
        
        $this->db->trans_complete();

        return $res;

    }

    function post_serial_by_outerlabel($data){

        $res = array();

        $id_user    = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $id_picking = $this->get_id("pl_id", "name", $data["pl_name"], "pickings")->row_array();

        $getItemAvailable = $this->get_count($data,true);

        if(!empty($data['outbound_code'])){
            $outboundId     = $this->get_id("id","code",$data['outbound_code'],"outbound")->row()->id;
        }else{
            $outboundId     = NULL;
        }

        if(!empty($data['tray_location'])){
            $trayLocation = $this->get_id("id","name",$data['tray_location'],"locations")->row_array();
            $inTransit = (!empty($trayLocation)) ? $trayLocation['id'] : 103;
        }else{
            $inTransit = 103;
        }

        $serialize  = $this->get_serial_lp($data["kd_unik"],$data['pl_name'],$getItemAvailable)->result_array();
        if($serialize){
            $this->db->trans_start();
            foreach($serialize as $serial){

                $item = $this->get_id("has_qty", "id", $serial["item_id"], "items")->row_array();

                if($item['has_qty']){

                    $data_insert = array(
                                        'picking_id'    => $id_picking["pl_id"],
                                        'unique_code'   => $serial["unique_code"],
                                        'first_qty'     => $serial["first_qty"],
                                        'qty_bef'       => $serial["last_qty"],
                                        'qty_pick'      => $serial["last_qty"],
                                        'qty_after'     => 0,
                                        'user_id'       => $id_user["user_id"],
                                        'picking_time'  => $data["kit_time"]
                                    );

                    $this->db->insert("picking_history", $data_insert);

                    if($data_insert["qty_after"] == 0){
                        $this->db->set('location_id',$inTransit);
                    }

                    $this->db->set("last_qty", $data_insert["qty_after"]);
                    $this->db->set("picking_id", $id_picking["pl_id"]);
                    $this->db->where("id", $serial["item_receiving_detail_id"]);
                    $this->db->update("item_receiving_details");


                }else{

                    $data_insert = array(
                                        'unique_code'   => $serial["unique_code"],
                                        'loc_id_old'    => $serial["location_id"],
                                        'user_id_pick'  => $id_user["user_id"],
                                        'pick_time'     => "NOW()",
                                        // 'pick_time'     => $data["pick_time"],
                                        'process_name'  => 'PICKING',
                                        'picking_id'    => $id_picking["pl_id"]
                                    );

                    $this->db->insert("transfers", $data_insert);

                    $this->db->set("last_qty", 0);
                    $this->db->set("picking_id", $id_picking["pl_id"]);
                    $this->db->set("location_id", $inTransit);
                    $this->db->where("id", $serial["item_receiving_detail_id"]);
                    $this->db->update("item_receiving_details");
                    break;

                }

                $data_insert = [
                    "picking_id" => $id_picking['pl_id'],
                    "unique_code" => $serial['unique_code'],
                    "item_id" => $serial['item_id'],
                    "old_location_id" => $serial['location_id'],
                    "location_id" => $inTransit,
                    "qty" => $serial['last_qty'],
                    'user_id'   => $id_user['user_id'],
                    'st_picking'    => 0,
                    'item_receiving_detail_id' => $serial['item_receiving_detail_id'],
                    "outbound_id"   => $outboundId,
                    'unit_id'   => $data['unit_id'],
                    'tray_location_id'  => ($inTransit == 103) ? NULL : $inTransit
                ];

                $this->db->insert('picking_recomendation',$data_insert);


            }

            $res['status'] = true;

            $this->db->trans_complete();

        }else{

            $res['status'] = false;
            $res['message'] = $data["kd_unik"].' not available';

        }

        return $res;

    }

    function post_serial_by_batch($data){

        $res = array();

        $id_user    = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $id_picking = $this->get_id("pl_id", "name", $data["pl_name"], "pickings")->row_array();

        $getItemAvailable = $this->get_count($data,true);

        if(!empty($data['tray_location'])){
            $trayLocation = $this->get_id("id","name",$data['tray_location'],"locations")->row_array();
            $inTransit = (!empty($trayLocation)) ? $trayLocation['id'] : 103;
        }else{
            $inTransit = 103;
        }

        $serialize  = $this->get_serial_batch($data["kd_unik"],$data['pl_name'],$getItemAvailable)->result_array();
        if($serialize){
            foreach($serialize as $serial){

                $item = $this->get_id("has_qty", "id", $serial["item_id"], "items")->row_array();

                if($item['has_qty']){

                    $data_insert = array(
                                        'picking_id'     => $id_picking["pl_id"],
                                        'unique_code'   => $serial["unique_code"],
                                        'first_qty' => $serial["first_qty"],
                                        'qty_bef'   => $serial["last_qty"],
                                        'qty_pick'  => $serial["last_qty"],
                                        'qty_after' => 0,
                                        'user_id'  => $id_user["user_id"],
                                        'picking_time'  => $data["kit_time"]
                                    );

                    $this->db->insert("picking_history", $data_insert);

                    if($data_insert["qty_after"] == 0){
                        $this->db->set('location_id',$inTransit);
                    }

                    $this->db->set("last_qty", $data_insert["qty_after"]);
                    $this->db->set("picking_id", $id_picking["pl_id"]);
                    $this->db->where("id", $serial["item_receiving_detail_id"]);
                    $this->db->update("item_receiving_details");


                }else{

                    $data_insert = array(
                                        'unique_code'   => $serial["unique_code"],
                                        'loc_id_old'    => $serial["location_id"],
                                        'user_id_pick'  => $id_user["user_id"],
                                        'pick_time'     => "NOW()",
                                        // 'pick_time'     => $data["pick_time"],
                                        'process_name'  => 'PICKING',
                                        'picking_id' => $id_picking["pl_id"]
                                    );

                    $this->db->insert("transfers", $data_insert);

                    $this->db->set("last_qty", 0);
                    $this->db->set("picking_id", $id_picking["pl_id"]);
                    $this->db->set("location_id", $inTransit);
                    $this->db->where("id", $serial["item_receiving_detail_id"]);
                    $this->db->update("item_receiving_details");
                    break;

                }

                if($data['qty']==0 || $data['qty'] == NULL){
                    $qty_insert = 1;
                }else{
                    $qty_insert = $data['qty'];
                }

                $data_insert = [
                    "picking_id" => $id_picking['pl_id'],
                    "unique_code" => $serial['unique_code'],
                    "item_id" => $serial['item_id'],
                    "old_location_id" => $serial['location_id'],
                    "location_id" => $inTransit,
                    "qty" => $qty_insert,
                    'user_id'   => $id_user['user_id'],
                    'st_picking'    => 0,
                    'item_receiving_detail_id' => $serial['item_receiving_detail_id'],
                    "outbound_id"       => $outboundId,
                    'unit_id'   => $data['unit_id']
                ];

                $this->db->insert('picking_recomendation',$data_insert);


            }

            $res['status'] = true;

        }else{

            $res['status'] = false;
            $res['message'] = $data["kd_unik"].' not available';

        }

        return $res;

    }


    function getLocationByItem($itemCode=''){

        $result = array();

        if($itemCode){

            $this->db
                ->select('loc.name as location_name')
                ->from('locations loc')
                ->join('item_receiving_details ird','ird.location_id=loc.id')
                ->join('items itm','itm.id=ird.item_id')
                ->where_not_in('loc.id',[100,101,102,103,104,105,106,107])
                ->where('itm.code',$itemCode)
                ->where('ird.last_qty >',0)
                ->group_by('loc.name');

            $result = $this->db->get()->result_array();

        }

        return $result;

    }

    function getSerialNumberByLocation($locationCode='',$itemCode=''){

        $result = array();

        if($locationCode){

            $sql = "
                SELECT *
                FROM
                    (
                        SELECT ird.unique_code as serial_number,  (CASE WHEN itm.shipment_id=1 THEN DATE(tgl_in) ELSE DATE(tgl_exp) END) as tgl, COALESCE(SUM(ird.last_qty),0) as qty
                        FROM item_receiving_details ird
                        JOIN locations loc ON loc.id=ird.location_id
                        JOIN items itm ON itm.id=ird.item_id
                        WHERE
                            loc.id NOT IN (100,101,102,103,104,105,106,107)
                        AND loc.name='$locationCode'
                        AND itm.code='$itemCode'
                        AND last_qty > 0
                        GROUP BY ird.unique_code, tgl
                    )
                    as tbl
                JOIN
                    (
                        select
                            (CASE WHEN itm.shipment_id=1 THEN DATE(tgl_in) ELSE DATE(tgl_exp) END) as tgl
                        FROM item_receiving_details ird
                        JOIN locations loc ON loc.id=ird.location_id
                        JOIN items itm ON itm.id=ird.item_id
                        WHERE
                            loc.id NOT IN (100,101,102,103,104,105,106,107)
                        AND loc.name='$locationCode'
                        AND itm.code='$itemCode'
                        AND last_qty > 0
                        GROUP BY tgl
                        ORDER BY tgl
                        LIMIT 1
                    )
                    as tbl1
                    ON tbl.tgl=tbl1.tgl
                ORDER BY qty asc
            ";

            $result = $this->db->query($sql)->result_array();

        }

        return $result;

    }

    function getTrayLocation($locationCode=""){

        $result = array();

        if($locationCode){

            $this->db
                ->select('name as location_name')
                ->from('locations')
                ->where('name',$locationCode)
                ->where('status','ACTIVE');

            $result = $this->db->get()->result_array();

        }

        return $result;

    }

    function getOutboundByPicking($pickingCode=""){

        $result = array();

        if($pickingCode){

            $this->db
                ->select("outb.code as outbound_code")
                ->from('pickings p')
                ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
                ->join('outbound outb','outb.id=plo.id_outbound')
                ->where('p.name',$pickingCode);

            $result = $this->db->get()->result_array();

        }

        return $result;

    }

    function getPickingByArea($pickingCode=""){
        $result = array();
        if($pickingCode){
            $sql = "SELECT p.name as outbound_code, o.code ,itm.code as item_code, itm.name as item_name, p.pl_status as status, (SELECT sum(qty) FROM picking_qty WHERE picking_id = p.pl_id and item_id = itm.id) as qty, unt.name as nama_satuan, itm.has_qty, pq.qty as qty, COALESCE(SUM(ird.last_qty),0) as available_qty, COALESCE(
                (SELECT SUM(qty) FROM picking_recomendation WHERE picking_id=p.pl_id AND order_item_id=pq.item_id),0) as received_qty
               FROM pickings p
               JOIN picking_list_outbound plo ON plo.pl_id = p.pl_id
               JOIN picking_qty pq ON pq.picking_id=p.pl_id
               LEFT JOIN item_receiving_details ird ON ird.picking_id=pq.id
               LEFT JOIN outbound o ON o.id=plo.id_outbound
               LEFT JOIN items itm ON itm.id=pq.item_id
               left join categories c2 on c2.id = itm.category_id 
               left join locations l on l.location_category_id = c2.id
               left join location_areas la on itm.item_area = la.name 
               LEFT JOIN units unt ON unt.id=itm.unit_id
               AND pq.qty != 0
               where la.id in (
               select id from location_areas la2 where la2.name in (
               select i1.item_area from items i1 where i1.id in (
               select item_id from picking_qty pq1 where pq1.picking_id IN (
               select p1.pl_id from pickings p1 where p1.name IN ('".$pickingCode."')))))
               GROUP BY itm.id, p.pl_id, pq.qty, pq.item_id, p.name, itm.code, itm.name, unt.name, itm.name, o.code";

            // dd($sql);
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }

    function getUnitBySerial($params=array()){

        $result = array();

        if($params['unit_code'] && $params['kd_unik']){

            $this->db
                ->select('unt.id as unit_id')
                ->from('item_receiving_details ird')
                ->join('items itm','itm.id=ird.item_id')
                ->join('units unt','unt.id=itm.unit_id or unt.id=itm.convert_qty_unit_id_1 or unt.id=itm.convert_qty_unit_id_2')
                ->where("(ird.unique_code='".$params['kd_unik']."' or  ird.parent_code='".$params['kd_unik']."' or ird.kd_batch='".$params['kd_unik']."')",NULL)
                ->where('unt.code',$params['unit_code']);

            $result = $this->db->get()->row();

        }

        return $result;

    }

    function getUnitCodeBySerial($params=array()){

        $result = array();

        if($params['kd_unik']){

            $this->db
                ->select('unt.name as unit_code')
                ->from('item_receiving_details ird')
                ->join('items itm','itm.id=ird.item_id')
                ->join('units unt','unt.id=itm.unit_id or unt.id=itm.convert_qty_unit_id_1 or unt.id=itm.convert_qty_unit_id_2')
                ->where("(ird.unique_code='".$params['kd_unik']."' or  ird.parent_code='".$params['kd_unik']."' or ird.kd_batch='".$params['kd_unik']."')",NULL);
                

            $result = $this->db->get()->row_array()['unit_code'];

        }

        return $result;

    }

    function checkStockSerialNumber($params=array()){

        $result = false;

        $this->db->select([
            "itm.unit_id",
            "itm.convert_qty_unit_id_1 as unit_id_1",
            "itm.convert_qty_unit_id_2 as unit_id_2",
            "COALESCE(SUM(last_qty),0) as qty",
            "COALESCE((SUM(last_qty)*itm.convert_qty_1),0) as qty_1",
            "COALESCE((SUM(last_qty)*itm.convert_qty_2),0) as qty_2"
        ])
        ->from("item_receiving_details ird")
        ->join("locations loc", "loc.id = ird.location_id", "left")
        ->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left")
        ->join('receivings r','r.id=ir.receiving_id','left')
        ->join('items itm','itm.id=ird.item_id')
        ->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL)
        ->where('(loc.location_type_id NOT IN (6) OR loc.location_type_id IS NULL)',NULL)
        ->where("(ird.unique_code='".$params['kd_unik']."' or  ird.parent_code='".$params['kd_unik']."' or ird.kd_batch='".$params['kd_unik']."')",NULL)
        ->where("ird.last_qty >", 0)
        ->group_by('itm.convert_qty_1, itm.convert_qty_2, itm.unit_id, itm.convert_qty_unit_id_1, itm.convert_qty_unit_id_2');

        $data = $this->db->get()->row_array();

        if($data){
            switch ($params['unit_id']) {
                case $data['unit_id']:

                    $result = ($params['qty'] <= $data['qty']) ? true : false;
                    break;

                case $data['unit_id_1']:

                    $result = ($params['qty'] <= $data['qty_1']) ? true : false;
                    break;

                case $data['unit_id_2']:

                    $result = ($params['qty'] <= $data['qty_2']) ? true : false;
                    break;

                default:
                    $result = false;
                    break;
            }
        }

        return $result;

    }

   function checkStockOuterLabel($params=array()){

        $result = false;

        $this->db->select([
            "itm.unit_id",
            "itm.convert_qty_unit_id_1 as unit_id_1",
            "itm.convert_qty_unit_id_2 as unit_id_2",
            "COALESCE(SUM(last_qty),0) as qty",
            "COALESCE((SUM(last_qty)*itm.convert_qty_1),0) as qty_1",
            "COALESCE((SUM(last_qty)*itm.convert_qty_2),0) as qty_2"
        ])
        ->from("item_receiving_details ird")
        ->join("locations loc", "loc.id = ird.location_id", "left")
        ->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left")
        ->join('receivings r','r.id=ir.receiving_id','left')
        ->join('items itm','itm.id=ird.item_id')
        ->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL)
        ->where('(loc.location_type_id NOT IN (6) OR loc.location_type_id IS NULL)',NULL)
        ->where("(ird.unique_code='".$params['kd_unik']."' or  ird.parent_code='".$params['kd_unik']."' or ird.kd_batch='".$params['kd_unik']."')",NULL)
        ->where("ird.last_qty >", 0)
        ->group_by('itm.convert_qty_1, itm.convert_qty_2, itm.unit_id, itm.convert_qty_unit_id_1, itm.convert_qty_unit_id_2');

        $data = $this->db->get()->row_array();

        if($data){
            $result = true;
        }

        return $result;

    }

    function checkOrderQtySerialNumber($params=array()){
      $item_id = $this->db->select('id')->from('items')->where('code', $params['item_id'])->get()->row_array()['id'];
      // dd($item_id);

        $result = false;

        $getItem = $this->get_serial($params['kd_unik'])->row_array();
        $getDataItemSubs = $this->db
                                  ->select('its.item_id')
                                  ->from("item_substitute its")
                                  ->join('item_receiving_details ird', 'ird.item_id = its.item_id_subs', 'LEFT')
                                  ->where('ird.unique_code', $params['kd_unik'])
                                  ->get()
                                  ->result_array();

          $getDataItemSubs = array_column($getDataItemSubs, 'item_id');

        $dataItemsPick = $this->db
                                ->select('pq.item_id')
                                ->from('picking_qty pq')
                                ->join('pickings p', 'p.pl_id = pq.picking_id', 'LEFT')
                                ->where('p.name',$params['pl_name'])
                                ->get()
                                ->result_array();
            $dataItemsPick = array_column($dataItemsPick, 'item_id');
            // dd($dataItemsPick);
            //
            // $dataItemsPick[0] = '90192';
            // $dataItemsPick[1] = '74230';

            $isTrue = FALSE;
            foreach ($dataItemsPick as $key => $value) {
              if(in_array($value, $getDataItemSubs)){
                $isTrue = true;
              }else {
                continue;
              }
            }
            // dd($isTrue);

            if($isTrue){
              $this->db
                      ->select(['outbitm.qty as order_qty',"COALESCE(SUM(pr.qty),0) as picked_qty","itm.*"])
                      ->from('pickings p')
          			->join('picking_list_outbound plo','p.pl_id=plo.pl_id')
          			->join('outbound outb','outb.id=plo.id_outbound')
          			->join('outbound_item outbitm','outbitm.id_outbound=outb.id')
                      ->join('picking_recomendation pr','pr.outbound_id=outbitm.id_outbound and pr.order_item_id=outbitm.item_id','left')
                      ->join('items itm','itm.id=outbitm.item_id')
                      ->where('p.name',$params['pl_name'])
                        ->where('outb.code',$params['outbound_code'])
                      ->where('itm.id',$params['item_ori'])
                      // ->where('itm.id',$item_id)

                      // ->where_in('pr.item_id',$getDataItemSubs)

                      // ->where('itm.unit_id',$params['unit_id'])
                      ->group_by('outbitm.qty, itm.id');

                  $data = $this->db->get()->row_array();
                  if($data){

                      switch ($params['unit_id']) {
                          case $data['unit_id']:
                              $data['picked_qty'] = $data['picked_qty'];
                              break;

                          case $data['convert_qty_unit_id_1']:
                              $data['picked_qty'] = $data['picked_qty']*$data['convert_qty_1'];
                              break;

                          case $data['convert_qty_unit_id_2']:
                              $data['picked_qty'] = $data['picked_qty']*$data['convert_qty_2'];
                              break;

                          default:
                              # code...
                              break;
                      }
                      // dd($data['order_qty']);
                      // dd($data['picked_qty']);

                      $discrepencies = $data['order_qty'] - $data['picked_qty'];
                      // dd($discrepencies);

                      if($params['qty'] <= $discrepencies){

                          $result = true;
                      }
                  }else{
                    $this->db
                    ->select(['outbitm.qty as order_qty',"COALESCE(SUM(pr.qty),0) as picked_qty","itm.*"])
                    ->from('pickings p')
                    ->join('picking_list_outbound plo','p.pl_id=plo.pl_id')
                    ->join('outbound outb','outb.id=plo.id_outbound')
                    ->join('outbound_item outbitm','outbitm.id_outbound=outb.id')
                    ->join('picking_recomendation pr','pr.outbound_id=outbitm.id_outbound and pr.order_item_id=outbitm.item_id','left')
                    ->join('items itm','itm.id=outbitm.item_id')
                    ->where('p.name',$params['pl_name'])
                      ->where('outb.code',$params['outbound_code'])
                    ->where('itm.id',$params['item_ori'])
                    // ->where('itm.id',$item_id)

                    // ->where_in('pr.item_id',$getDataItemSubs)

                    // ->where('itm.unit_id',$params['unit_id'])
                    ->group_by('outbitm.qty, itm.id');
                    $data = $this->db->get()->row_array();

                    if($data){

                      switch ($params['unit_id']) {
                          case $data['unit_id']:
                              $data['picked_qty'] = $data['picked_qty'];
                              break;

                          case $data['convert_qty_unit_id_1']:
                              $data['picked_qty'] = $data['picked_qty']*$data['convert_qty_1'];
                              break;

                          case $data['convert_qty_unit_id_2']:
                              $data['picked_qty'] = $data['picked_qty']*$data['convert_qty_2'];
                              break;

                          default:
                              # code...
                              break;
                      }
                      // dd($data['order_qty']);
                      // dd($data['picked_qty']);

                      $discrepencies = $data['order_qty'] - $data['picked_qty'];
                      // dd($discrepencies);

                      if($params['qty'] <= $discrepencies){
                          $result = true;
                        }
                    }
                }
            }else {
              // code...
              $this->db
                      ->select(['outbitm.qty as order_qty',"COALESCE(pr.qty,0) as picked_qty","itm.*"])
                      ->from('pickings p')
          			->join('picking_list_outbound plo','p.pl_id=plo.pl_id')
          			->join('outbound outb','outb.id=plo.id_outbound')
          			->join('outbound_item outbitm','outbitm.id_outbound=outb.id')
                      ->join('picking_recomendation pr','pr.outbound_id=outbitm.id_outbound and pr.item_id=outbitm.item_id','left')
                      ->join('items itm','itm.id=outbitm.item_id')
                      ->where('p.name',$params['pl_name'])
          			->where('outb.code',$params['outbound_code'])
                      ->where('itm.id',$getItem['item_id'])
                      ->where('itm.unit_id',$params['unit_id'])
                      ->group_by('outbitm.qty, itm.id, pr.qty');

                  $data = $this->db->get()->row_array();
                //   dd($data);


                $this->db
                      ->select(['outbitm.qty as order_qty',"COALESCE(SUM(pr.qty),0) as picked_qty","itm.*"])
                      ->from('pickings p')
          			->join('picking_list_outbound plo','p.pl_id=plo.pl_id')
          			->join('outbound outb','outb.id=plo.id_outbound')
          			->join('outbound_item outbitm','outbitm.id_outbound=outb.id')
                      ->join('picking_recomendation pr','pr.outbound_id=outbitm.id_outbound and pr.order_item_id=outbitm.item_id','left')
                      ->join('items itm','itm.id=outbitm.item_id')
                      ->where('p.name',$params['pl_name'])
                        ->where('outb.code',$params['outbound_code'])
                      ->where('itm.id',$params['item_ori'])
                      // ->where('itm.id',$item_id)

                      // ->where_in('pr.item_id',$getDataItemSubs)

                      // ->where('itm.unit_id',$params['unit_id'])
                      ->group_by('outbitm.qty, itm.id');

                  $data_ori = $this->db->get()->row_array();
                //   dd($data_ori);

                  if($data){
                      switch ($params['unit_id']) {
                          case $data['unit_id']:
                              $data['picked_qty'] = $data['picked_qty'];
                              break;

                          case $data['convert_qty_unit_id_1']:
                              $data['picked_qty'] = $data['picked_qty']*$data['convert_qty_1'];
                              break;

                          case $data['convert_qty_unit_id_2']:
                              $data['picked_qty'] = $data['picked_qty']*$data['convert_qty_2'];
                              break;

                          default:
                              # code...
                              break;
                      }

                      $discrepencies = $data_ori['order_qty'] - $data_ori['picked_qty'];

                      if($params['qty'] <= $discrepencies){

                          $result = true;

                      }

                      $sql = "select * from picking_qty pq
                        join pickings p on p.pl_id = pq.picking_id 
                        left join picking_qty_substitute pqs on pqs.picking_qty_id = pq.id
                        left join item_substitute is2 on is2.id = pqs.item_substitute_id 
                        left join items itm on itm.id = is2.item_id_subs
                        where p.name = '".$params['pl_name']."' and is2.item_id_subs = '".$getItem['item_id']."' and pqs.qty >=".$params['qty'];

                        $check = $this->db->query($sql)->result_array();
                        
                        if(count($check)>0){
                            $result = false;
                        }
                  }
            }

        // $this->db
            // ->select(['pq.qty as order_qty',"COALESCE(SUM(pr.qty),0) as picked_qty","itm.*"])
            // ->from('pickings p')
            // ->join('picking_qty pq','pq.picking_id=p.pl_id')
            // ->join('picking_recomendation pr','pr.picking_id=pq.picking_id and pr.item_id=pq.item_id','left')
            // ->join('items itm','itm.id=pq.item_id')
            // ->where('p.name',$params['pl_name'])
            // ->where('itm.id',$getItem['item_id'])
            // ->where('itm.unit_id',$params['unit_id'])
            // ->group_by('pq.qty, itm.id');
            // dd($result);
        return $result;
    }

    function checkOrderQtyOuterLabel($params=array()){

        $result = false;

        $this->db->select([
            "itm.id as item_id",
            "itm.unit_id",
            "itm.convert_qty_unit_id_1 as unit_id_1",
            "itm.convert_qty_unit_id_2 as unit_id_2",
            "COALESCE(SUM(last_qty),0) as qty",
            "COALESCE((SUM(last_qty)*itm.convert_qty_1),0) as qty_1",
            "COALESCE((SUM(last_qty)*itm.convert_qty_2),0) as qty_2"
        ])
        ->from("item_receiving_details ird")
        ->join("locations loc", "loc.id = ird.location_id", "left")
        ->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left")
        ->join('receivings r','r.id=ir.receiving_id','left')
        ->join('items itm','itm.id=ird.item_id')
        ->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL)
        ->where('(loc.location_type_id NOT IN (6) OR loc.location_type_id IS NULL)',NULL)
        ->where("(ird.unique_code='".$params['kd_unik']."' or  ird.parent_code='".$params['kd_unik']."' or ird.kd_batch='".$params['kd_unik']."')",NULL)
        ->where("ird.last_qty >", 0)
        ->group_by('itm.convert_qty_1, itm.convert_qty_2, itm.unit_id, itm.convert_qty_unit_id_1, itm.convert_qty_unit_id_2, itm.id');

        $outerLabel = $this->db->get()->row_array();
        if($outerLabel){

            $this->db
                ->select(['pq.qty as order_qty',"COALESCE(SUM(pr.qty),0) as picked_qty","itm.*"])
                ->from('pickings p')
                ->join('picking_qty pq','pq.picking_id=p.pl_id')
                ->join('picking_recomendation pr','pr.picking_id=pq.picking_id and pr.item_id=pq.item_id','left')
                ->join('items itm','itm.id=pq.item_id')
                ->where('p.name',$params['pl_name'])
                ->where('itm.id',$outerLabel['item_id'])
                ->where('itm.unit_id',$params['unit_id'])
                ->group_by('pq.qty, itm.id');

            $data = $this->db->get()->row_array();

            if($data){
                switch ($params['unit_id']) {
                    case $data['unit_id']:
                        $data['picked_qty'] = $data['picked_qty'];
                        break;

                    case $data['convert_qty_unit_id_1']:
                        $data['picked_qty'] = $data['picked_qty']*$data['convert_qty_1'];
                        break;

                    case $data['convert_qty_unit_id_2']:
                        $data['picked_qty'] = $data['picked_qty']*$data['convert_qty_2'];
                        break;

                    default:
                        # code...
                        break;
                }

                $discrepencies = $data['order_qty'] - $data['picked_qty'];

                if($data){
                    switch ($params['unit_id']) {
                        case $outerLabel['unit_id']:

                            $result = ($outerLabel['qty'] <= $discrepencies) ? true : false;
                            break;

                        case $outerLabel['unit_id_1']:

                            $result = ($outerLabel['qty_1'] <= $discrepencies) ? true : false;
                            break;

                        case $outerLabel['unit_id_2']:

                            $result = ($outerLabel['qty_2'] <= $discrepencies) ? true : false;
                            break;

                        default:
                            $result = false;
                            break;
                    }
                }

            }

        }

        return $result;

    }

    function getPickingRecomendationBySerialNumber($params=array()){
		//sementara
		$data = array(
                    'unique_code'   => $params['kd_unik'],
                    'status'        => true
                );
		return $data;

        $uniqueCode = $params['kd_unik'];

        $data = array(
            'unique_code'   => '',
            'location'      => '',
            'status'        => false
        );

        if($uniqueCode != ""){

            $this->db->start_cache();

            $this->db
                ->select(["itm.code","ird.unique_code","ird.parent_code","ird.kd_batch","loc.name as location_name","rfid_tag","TO_CHAR( ( CASE WHEN itm.shipment_id=1 THEN min(tgl_in) ELSE min(tgl_exp) END ), 'YYYY-MM' )  as date"])
                ->from('item_receiving_details ird')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('items itm','itm.id=ird.item_id')
                ->where_not_in('loc.id',[100,102,101,103,104,106])
                ->where('(loc.location_type_id NOT IN (4,1,6) or loc.location_type_id IS NULL)',NULL)
                ->where("ird.item_id IN (SELECT item_id FROM item_receiving_details where unique_code='$uniqueCode' or parent_code='$uniqueCode' or kd_batch='$uniqueCode')",NULL)
                ->where('last_qty >',0)
                ->group_by('itm.code, ird.unique_code,loc.name, itm.shipment_id,rfid_tag, ird.parent_code, ird.kd_batch')
                ->order_by('date');

            $this->db->stop_cache();

            $this->db->limit(1);
            $dr = $this->db->get()->row_array();

            $tbl = $this->db->get_compiled_select();

            $this->db->flush_cache();

            $sql = "SELECT * FROM ($tbl) as tbl WHERE (unique_code='$uniqueCode' or parent_code='$uniqueCode' or kd_batch='$uniqueCode') and date='".$dr['date']."' LIMIT 5";

            $d = $this->db->query($sql)->row_array();

            if($d){

                $data = array(
                    'unique_code'   => $d['unique_code'],
                    'status'        => true
                );

            }else{

                $sql = "SELECT * FROM ($tbl) as tbl WHERE date='".$dr['date']."' LIMIT 5";
                $ds = $this->db->query($sql)->result_array();

                $data = array(
                    'data'          => $ds,
                    'status'        => false
                );

            }

        }

        return $data;

    }

    function getPickingRecomendationByOuterLabel($params=array()){
		//sementara
		$data = array(
                    'unique_code'   => $params['kd_unik'],
                    'status'        => true
                );
		return $data;

        $uniqueCode = $params['kd_unik'];

        $data = array(
            'unique_code'   => '',
            'location'      => '',
            'status'        => false
        );

        if($uniqueCode != ""){

            $this->db->start_cache();

            $this->db
                ->select(["itm.code","ird.parent_code as unique_code","ird.parent_code","ird.kd_batch","loc.name as location_name","rfid_tag","TO_CHAR( ( CASE WHEN itm.shipment_id=1 THEN min(tgl_in) ELSE min(tgl_exp) END ), 'YYYY-MM' )  as date"])
                ->from('item_receiving_details ird')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('items itm','itm.id=ird.item_id')
                ->where_not_in('loc.id',[100,102,101,103,104,106])
                ->where('(loc.location_type_id NOT IN (4,1,6) or loc.location_type_id IS NULL)',NULL)
                ->where("ird.item_id IN (SELECT item_id FROM item_receiving_details where unique_code='$uniqueCode' or parent_code='$uniqueCode' or kd_batch='$uniqueCode')",NULL)
                ->where('last_qty >',0)
                ->group_by('itm.code, loc.name, itm.shipment_id,rfid_tag, ird.parent_code, ird.kd_batch')
                ->order_by('date');

            $this->db->stop_cache();

            $this->db->limit(1);
            $dr = $this->db->get()->row_array();

            $tbl = $this->db->get_compiled_select();

            $this->db->flush_cache();

            $sql = "SELECT * FROM ($tbl) as tbl WHERE (parent_code='$uniqueCode' or kd_batch='$uniqueCode') and date='".$dr['date']."' LIMIT 5";

            $d = $this->db->query($sql)->row_array();

            if($d){

                $data = array(
                    'unique_code'   => $d['parent_code'],
                    'status'        => true
                );

            }else{

                $sql = "SELECT * FROM ($tbl) as tbl WHERE date='".$dr['date']."' LIMIT 5";
                $ds = $this->db->query($sql)->result_array();

                $data = array(
                    'data'          => $ds,
                    'status'        => false
                );

            }

        }

        return $data;

    }

    function getUomBySerialNumber($params=array()){
        // dd($params);
        $result = array();
        // $items_subs = $this->db->select('itms.item_id_subs')
        // $items_subs = $this->db->select('itms.item_id')
        //                        ->from('item_receiving_details ird')
        //                        ->join('item_substitute itms', 'itms.item_id = ird.item_id')
        //                        ->where("(ird.unique_code='".$params['serial_number']."' or ird.parent_code='".$params['serial_number']."')",NULL)
        //                        ->get();
        //                        // ->row_array()['item_id_subs'];

        $items_subs = $this->db->select('itms.item_id_subs')
                               ->from('items itm')
                               ->join('item_substitute itms', 'itms.item_id = itm.id')
                               ->where('itm.code', $params['item_code'])
                               ->get();
        if($items_subs->num_rows()==0){
            $items_subs = $this->db->select('itms.item_id_subs')
                               ->from('items itm')
                               ->join('item_substitute itms', 'itms.item_id_subs = itm.id')
                               ->where('itm.id', $params['item_code'])
                               ->get();
        }

        $item_code_by_sn = $this->db->select('itm.code')
                                    ->from('item_receiving_details ird')
                                    ->join('items itm','itm.id=ird.item_id')
                                    ->where("(ird.unique_code='".$params['serial_number']."' or ird.parent_code='".$params['serial_number']."')",NULL)
                                    ->get()
                                    ->row_array();
        if(isset($item_code_by_sn['code'])){
            $item_code_by_sn = $item_code_by_sn['code'];
        }else{
            $item_code_by_sn = 0;
        }

        if($item_code_by_sn == $params['item_code']){
          $this->db
                ->select("DISTINCT(unt.code) as unit_code")
                ->from('item_receiving_details ird')
                ->join('items itm','itm.id=ird.item_id')
                ->join('picking_qty pq','pq.item_id=itm.id')
                ->join('pickings p','p.pl_id=pq.picking_id')
                ->join('units unt',"unt.id=itm.unit_id")
                // ->where('p.name',$params['picking_code'])
                ->where("(ird.unique_code='".$params['serial_number']."' or ird.parent_code='".$params['serial_number']."')",NULL);
        }else {
        if($items_subs->num_rows() > 0){
            $this->db
                ->select("DISTINCT(unt.code) as unit_code")
                ->from('item_receiving_details ird')
                ->join('items itm','itm.id=ird.item_id','left')
                ->join('item_substitute itms', 'itms.item_id_subs = ird.item_id','left')
                ->join('picking_qty pq','pq.item_id=itms.item_id','left')
                ->join('pickings p','p.pl_id=pq.picking_id','left')
                ->join('units unt',"unt.id=itm.unit_id",'left')
                ->where("(ird.unique_code='".$params['serial_number']."' or ird.parent_code='".$params['serial_number']."')",NULL);
                // ->where('p.name', $params['pl_name']);
              }else{
                $this->db
                ->select("DISTINCT(unt.code) as unit_code")
                ->from('item_receiving_details ird')
                ->join('items itm','itm.id=ird.item_id')
                ->join('picking_qty pq','pq.item_id=itm.id')
                ->join('pickings p','p.pl_id=pq.picking_id')
                ->join('units unt',"unt.id=itm.unit_id")
                // ->where('p.name',$params['picking_code'])
                ->where("(ird.unique_code='".$params['serial_number']."' or ird.parent_code='".$params['serial_number']."')",NULL);
              }
            }

        // dd($this->db->get_compiled_select());
        $result = $this->db->get()->result_array();
        return $result;

    }

    function getTrayByOutbound($outboundCode=""){

        $result = array();

        $this->db
            ->select('loc.name as tray_name')
            ->from('outbound outb')
            ->join('picking_list_outbound plo','plo.id_outbound=outb.id')
            ->join('picking_recomendation pr','pr.picking_id=plo.pl_id')
            ->join('locations loc','loc.id=pr.location_id')
            ->where('loc.location_type_id',6)
            ->where('outb.code',$outboundCode)
            ->group_by('loc.name')
            ->order_by('loc.name','ASC');

        $result = $this->db->get()->result_array();

        return $result;
    }

    function getOutboundByTray($locationCode=""){

        $result = array();

        $this->db
            ->select(["outb.code as outbound_code"])
            ->from('locations loc')
            ->join('picking_recomendation pr','pr.tray_location_id=loc.id')
            ->join('outbound outb','outb.id=pr.outbound_id')
            ->where('loc.name',$locationCode)
            ->where('pr.st_packing IS NULL',NULL)
            ->group_by('outb.code');

        $result = $this->db->get()->row_array();

        return $result;

    }

    function checkOrderItem($params=array()){

        $result = array();

        if($params){

          // $items_subs = $this->db->select('itms.item_id_subs')
          //                        ->from('item_receiving_details ird')
          //                        ->join('item_substitute itms', 'itms.item_id = ird.item_id')
          //                        ->where("(ird.unique_code='".$params['kd_unik']."' or ird.parent_code='".$params['kd_unik']."')",NULL)
          //                        ->get();
          //                        // ->row_array()['item_id_subs'];

          $items_subs = $this->db->select('itms.item_id_subs')
                                 ->from('items itm')
                                 ->join('item_substitute itms', 'itms.item_id_subs = itm.id')
                                 ->where('itm.code', $params['item_id'])
                                 ->get();

          $item_code_by_sn = $this->db->select('itm.code')
                                      ->from('item_receiving_details ird')
                                      ->join('items itm','itm.id=ird.item_id')
                                      ->where("(ird.unique_code='".$params['kd_unik']."' or ird.parent_code='".$params['kd_unik']."')",NULL)
                                      ->where('itm.code', $params['item_id'])
                                      ->get()
                                      ->row_array();

            if(!isset($item_code_by_sn['code'])){
                $item_code_by_sn = false;
            }else{
                $item_code_by_sn = $item_code_by_sn['code'];
            }

            $item_id_param = $this->db->select('id')->from('items')->where('code',$params['item_id'])->get()->row_array()['id'];
            // dd($items_subs->num_rows());

          if($item_code_by_sn == $params['item_id']){
            $this->db->select('ird.*, pq.*')
                ->from('item_receiving_details ird')
                // ->join('item_substitute itms', 'itms.item_id_subs = ird.item_id')
                ->join('picking_qty pq','pq.item_id=ird.item_id')
                ->join('pickings p','p.pl_id=pq.picking_id')
                ->where("(ird.unique_code='".$params['kd_unik']."' or ird.parent_code='".$params['kd_unik']."')",NULL)
                ->where("pq.item_id",$item_id_param)
                ->where('p.name', $params['pl_name']);
                $result = $this->db->get()->row_array();
                
                if(count($result)<=0){
                    if($items_subs->num_rows() > 0){

                        $this->db->select('ird.*, pq.*')
                            ->from('item_receiving_details ird')
                            ->join('item_substitute itms', 'itms.item_id_subs = ird.item_id')
                            ->join('picking_qty pq','pq.item_id=itms.item_id')
                            ->join('pickings p','p.pl_id=pq.picking_id')
                            ->where("(ird.unique_code='".$params['kd_unik']."' or ird.parent_code='".$params['kd_unik']."')",NULL)
                            ->where('p.name', $params['pl_name']);
                            $result = $this->db->get()->row_array();
                          }else{
                            $this->db->select('ird.*, pq.*')
                                ->from('item_receiving_details ird')
                                // ->join('item_substitute itms', 'itms.item_id_subs = ird.item_id')
                                ->join('picking_qty pq','pq.item_id=ird.item_id')
                                ->join('pickings p','p.pl_id=pq.picking_id')
                                ->where("(ird.unique_code='".$params['kd_unik']."' or ird.parent_code='".$params['kd_unik']."')",NULL)
                                ->where("pq.item_id",$item_id_param)
                                ->where('p.name', $params['pl_name']);
                            $result = $this->db->get()->row_array();
            
                          }
                }
                // dd($result);
          }else{
            $sql = "select * from picking_qty pq
            join pickings p on p.pl_id = pq.picking_id 
            left join picking_qty_substitute pqs on pqs.picking_qty_id = pq.id
            left join item_substitute is2 on is2.id = pqs.item_substitute_id 
            left join items itm on itm.id = is2.item_id_subs
            where p.name = '".$params['pl_name']."' and is2.item_id_subs = '".$params['item_code']."' and pqs.qty >=".$params['qty'];
            // dd($sql);

            $check = $this->db->query($sql)->result_array();
            if($items_subs->num_rows() > 0){
                if(count($check)>0){
                        $this->db->select('ird.*, pq.*')
                        ->from('item_receiving_details ird')
                        ->join('item_substitute itms', 'itms.item_id_subs = ird.item_id')
                        ->join('picking_qty pq','pq.item_id=itms.item_id')
                        ->join('pickings p','p.pl_id=pq.picking_id')
                        ->where("(ird.unique_code='".$params['kd_unik']."' or ird.parent_code='".$params['kd_unik']."')",NULL)
                        ->where('p.name', $params['pl_name']);
                        $result = $this->db->get()->row_array();
                }else{
                    $result = false;
                }
            }else{
                $this->db->select('ird.*, pq.*')
                        ->from('item_receiving_details ird')
                        // ->join('item_substitute itms', 'itms.item_id_subs = ird.item_id')
                        ->join('picking_qty pq','pq.item_id=ird.item_id')
                        ->join('pickings p','p.pl_id=pq.picking_id')
                        ->where("(ird.unique_code='".$params['kd_unik']."' or ird.parent_code='".$params['kd_unik']."')",NULL)
                        ->where("pq.item_id",$item_id_param)
                        ->where('p.name', $params['pl_name']);
                    $result = $this->db->get()->row_array();
            }    
        }
        }

        return $result;

    }

    public function check_part_number($params=[]){
        
        $id_sub = $this->db->select('item_id_subs')->from('item_substitute')->where('item_id',$params['unique'])->get()->result_array();
        $data=array();
        for($i=0;$i<count($id_sub);$i++){
            array_push($data,$id_sub[$i]['item_id_subs']);
        }
        array_push($data,$params['unique']);
        
        $result = $this->db->select(['ird.unique_code','itm.code','itm.sku'])
            ->from('item_receiving_details ird')
            ->join('items itm','itm.id = ird.item_id','left')
            ->where('itm.code',$params['part_number'])
            ->where_in('ird.item_id',$data)
            ->get()
            ->result_array();
        // dd($result);
        if(count($result)>0){
            return true;
        }else if(count($result==0)){
            return false;
        }
    }

    public function check_sub($params){
        $id = $this->db->select('id')->from('items')->where('code',$params['item_ori'])->get()->row_array();
        $result = array();
        if(isset($id['id'])){
            $sql = "select concat('<b>',i.code,'</b> - ',i.name,' (',sum(ird.last_qty),') ',loc.name) from item_receiving_details ird
                    left join items i on i.id = ird.item_id 
                    left join locations loc on loc.id = i.def_loc_id
                    where location_id not in (101,103,103,106,105)
                    and ird.item_id in (
                        select is2.item_id_subs from item_substitute is2
                        left join items i2 on i2.id = is2.item_id 
                        left join items i3 on i3.id = is2.item_id_subs 
                        where is2.item_id =".$id['id']."
                    )
                    group by i.name,i.code, loc.name
                    union all
                    select concat('<b>',i.code,'</b> - ',i.name,' (',sum(ird.last_qty),') ',loc.name) from item_receiving_details ird
                    left join items i on i.id = ird.item_id 
                    left join locations loc on loc.id = i.def_loc_id
                    where location_id not in (101,103,103,106,105)
                    and ird.item_id =".$id['id']."
                    group by i.name,i.code,loc.name";
            $result['message'] = $this->db->query($sql)->result_array(); 
            $result['status'] = true;
        }else{
            $result['status'] = false;
            $result['message'] = 'ITEM TIDAK ADA';
        }
        return $result;
    }

}

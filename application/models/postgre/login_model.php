<?php

class login_model extends CI_Model {

    function login_model() {
        parent::__construct();
    }

    public function cek_store(){
    	$username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $where = array('username' =>$username, 'password'=>md5($password));
        $this->db->select('*');
        $this->db->from('toko t');
        $this->db->join('hr_grup g','g.grup_id = t.group_id','left');
        $this->db->where($where);
        $hasil = $this->db->get();
        if($hasil->num_rows()>0){
        	$data = $hasil->row_array();
        	$array_login = array(                   
                    'kd_roles' => $data['grup_id'],
                    'user_id' => $data['id_toko'],
                    'nama_session' => $data['nama_toko'],
                    'username' => $username,
                    'pass_session' => $this->input->post('password'),
                    'id_menu' => 0,
                    'login' => 'TRUE',
                    'status_login' => true,
                    'st_toko' =>TRUE
                );
        	$this->session->set_userdata($array_login);
            redirect('receiving_store');
        }
        else {
        	return FALSE;
        }
    }
	
	function cek_auth(){
		$username = $this->input->post('username');
        $password = md5($this->input->post('password'));
		$jumlah="";
		$q = $this->db->query("select count(hr_user.user_name) as jumlah FROM  hr_user WHERE user_name='$username'");
		  if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
				return $jumlah=$data->jumlah;
			}
		}			 
	}
	function cek_pass(){
		$username = $this->input->post('username');
        $password = md5($this->input->post('password'));
		$jumlah="";
		$q = $this->db->query("select count(1) as jumlah FROM  hr_user WHERE user_name='$username' and  user_password='$password'");
		  if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
				return $jumlah=$data->jumlah;
			}
		}	
	}

	function cek_active(){
		$username = $this->input->post('username');
        $password = md5($this->input->post('password'));
		$jumlah="";
		$q = $this->db->query("select count(1) as jumlah FROM  hr_user WHERE user_name='$username' and  user_password='$password' and is_active = 't'");
		  if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
				return $jumlah=$data->jumlah;
			}
		}	
	}
	function get_max_login(){
		$q = $this->db->query("select max_login FROM hr_ref_parameter");
		  if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
				return $data->max_login;
			}
		}	
	}
    function cek_login() {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $q = $this->db->query("SELECT hr_user.user_id, hr_user.last_update, hr_user.nik, hr_user.grup_id,
                hr_pegawai.nama, hr_pegawai.kd_cabang, hr_pegawai.kd_jabatan,
                hr_pegawai.kd_unit_kerja, hr_pegawai.kd_level,m.id_mapping_jabatan
                FROM  hr_user
                LEFT JOIN hr_pegawai ON hr_pegawai.nik = hr_user.nik
                LEFT JOIN hr_mapping_jabatan m ON hr_pegawai.kd_jabatan=m.kd_jabatan
			and hr_pegawai.kd_unit_kerja=m.kd_unit_kerja
			and hr_pegawai.kd_cabang=m.kd_cabang
                WHERE user_name='$username' and  user_password='$password' and is_active = 't'
                ");

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $data) {
                $array_login = array(
                    'id_mapping_jabatan' => $data['id_mapping_jabatan'],
                    'kd_unit_kerja' => $data['kd_unit_kerja'],
                    'nik_session' => $data['nik'],
                    'kd_roles' => $data['grup_id'],
                    'unit_kerja_ses' => $data['kd_unit_kerja'],
                    'user_id' => $data['user_id'],
                    'cabang_user' => $data['kd_cabang'],
                    'kd_cabang' => $data['kd_cabang'],
                    'kd_jabatan' => $data['kd_jabatan'],
                    'nama_session' => $data['nama'],
                    'kd_level' => $data['kd_level'],
                    'username' => $username,
                    'pass_session' => $this->input->post('password'),
                    'id_menu' => 0,
                    'login' => 'TRUE',
                    'last_update' => $data['last_update'],
                    'status_login' => true,
                    'st_toko' => FALSE
                );
                //Update field untuk Last Login
                $this->db->update('hr_user', array('last_login' => date('Y-m-d')), array('hr_user.nik' => $data['nik']));
            }

            // Untuk Mengecek SDM Atau Bukan
            $this->load->model('parameter_model');
            $parameter = $this->parameter_model->get();
            if ($array_login['unit_kerja_ses'] == $parameter->unit_sdm) {
                $is_sdm = true;
            } else {
                $is_sdm = false;
            }
            
            $array_login['is_sdm'] = $is_sdm;

            $this->session->set_userdata($array_login);
			$this->reset_salah_login();
            if($this->session->userdata('st_toko'))
                redirect('receiving_store');
            redirect('dashboard');
        } else {
            redirect('login');
        }
    }

    function cek_login_store() {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $q = $this->db->query("SELECT hr_user.user_id, hr_user.last_update, hr_user.nik, hr_user.grup_id,
                hr_pegawai.nama, hr_pegawai.kd_cabang, hr_pegawai.kd_jabatan,
                hr_pegawai.kd_unit_kerja, hr_pegawai.kd_level,m.id_mapping_jabatan
                FROM  hr_user
                LEFT JOIN hr_pegawai ON hr_pegawai.nik = hr_user.nik
                LEFT JOIN hr_mapping_jabatan m ON hr_pegawai.kd_jabatan=m.kd_jabatan
			and hr_pegawai.kd_unit_kerja=m.kd_unit_kerja
			and hr_pegawai.kd_cabang=m.kd_cabang
                WHERE user_name='$username' and  user_password='$password' and is_active = 't'
                ");

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $data) {
                $array_login = array(
                    'id_mapping_jabatan' => $data['id_mapping_jabatan'],
                    'kd_roles' => $data['grup_id'],
                    'user_id' => $data['user_id'],
                    'nama_session' => $data['nama'],
                    'username' => $username,
                    'pass_session' => $this->input->post('password'),
                    'id_menu' => 0,
                    'login' => 'TRUE',
                    'last_update' => $data['last_update'],
                    'status_login' => true
                );
                //Update field untuk Last Login
                $this->db->update('hr_user', array('last_login' => date('Y-m-d')), array('hr_user.nik' => $data['nik']));
            }

            // Untuk Mengecek SDM Atau Bukan
            $this->load->model('parameter_model');
            $parameter = $this->parameter_model->get();
            if ($array_login['unit_kerja_ses'] == $parameter->unit_sdm) {
                $is_sdm = true;
            } else {
                $is_sdm = false;
            }
            
            $array_login['is_sdm'] = $is_sdm;

            $this->session->set_userdata($array_login);
			$this->reset_salah_login();
            redirect('dashboard');
        } else {
            redirect('login');
        }
    }
	
	/*----------- EDITED BY AGNI ---------------*/
	function cek_max_login(){
		$username = $this->input->post('username');
			$q=$this->db->query("select jumlah from hr_salah_password   where username='$username'");
			if ($q->num_rows() > 0) {
				  foreach ($q->result() as $data) {
					$jumlah=$data->jumlah;
				}
				return $jumlah;
			}
	}
	function update_salah_pass(){
		$check_exist="";
		$jumlah='';
		$date='';
		$username = $this->input->post('username');
		/* Check IF Data Exist Or 0 */
			$q=$this->db->query("select a.username,a.date,(select jumlah from hr_salah_password where username='$username') as jumlah from hr_salah_password a where username='$username'");
			if ($q->num_rows() > 0) {
				  foreach ($q->result() as $data) {
					$check_exist=$data->username;
					$jumlah=$data->jumlah;
					$date=$data->date;
				}
			}		
		/* ----- END OF CHECK DATA */
		/* Insert IF Data Exist Or 0 and Update Id Jumlah Data > 0*/
		if($check_exist==''){
			$this->insert_fail();
		} else {
			/* CEK BILA TANGGAL TERAK HIR LOGIN != HARI INI */
			if($date!=date("Y-m-d")){
				$this->reset_salah_login();
				$this->insert_fail();
			} else {
				$this->up_fail($jumlah);
			}
		}
		/*-----------------------------------------------------*/
		return $jumlah;
	}
	function insert_fail(){
		/* insert to db */
		$username = $this->input->post('username');
			$data=array(
				'username'=>$username,
				'jumlah'=>'1',
				'date'=>date("Y-m-d")
			);
			$this->db->trans_start();
			$this->db->insert('hr_salah_password',$data);
			$this->db->trans_complete(); 	
	}
	function up_fail($jumlah=''){
		/*update to db */
		$username = $this->input->post('username');
			$data=array(
				'username'=>$username,
				'jumlah'=>$jumlah+1,
			);
			$this->db->trans_start();
			$this->db->where('username',$username);
			$this->db->update('hr_salah_password', $data); 
			$this->db->trans_complete();
	}
	function reset_salah_login(){
		$username = $this->input->post('username');
		$q=$this->db->query("delete from hr_salah_password where username='$username'");
	}
	
}

?>

<?php
class stock_opname_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'cycle_count_h1';

    private $table2 = 'pl_do';
    private $table4 = 'do_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $do = 'do';

    public function get_cc_code(){
        $this->db->select('pre_code_cc,inc_cc');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_cc.$result->inc_cc;
    }

    public function add_cc_code(){
        $this->db->set('inc_cc',"inc_cc+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

	public function finishCc($post = array()){
		$result = array();
		$rectime = date('Y-m-d H:i:s');

		$sql = 'SELECT
					cc_finish
				FROM
					cycle_count
				WHERE
					cc_id=\''.$post['cc_id'].'\'';

		$r = $this->db->query($sql)->row_array();

		if(!empty($r['cc_finish'])){

			$result['status'] = 'OK';
			$result['message'] = 'Cycle count already finished';

		}else{

			$sql = 'UPDATE
						cycle_count
					SET
						cc_finish=\''.$rectime.'\'
					WHERE
						cc_id=\''.$post['cc_id'].'\'';

			$q = $this->db->query($sql);

			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Finish stock opname success';
			}else{
				$result['status'] = 'OK';
				$result['message'] = 'Finish stock opname failed';
			}

		}

		return $result;
	}

	public function checkStatus($id){
		$result = 0;

		$sql = 'SELECT
					status
				FROM
					cycle_count
				WHERE
					cc_id=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		if($row)
			$result = $row['status'];
		else
			redirect(base_url() . 'cycle_count');

		return $result;
	}

	public function setItemLog($post = array()){
		$result = array();

		if($post['params'] == 'NEGATIF'){

			$rectime = date('Y-m-d H:i:s');
			$sql = 'SELECT
						id_barang, loc_id
					FROM
						receiving_barang
					WHERE
						kd_unik=\''.$post['serial_number'].'\'';

			$r = $this->db->query($sql)->row_array();

			$sql = 'INSERT INTO int_transfer_detail(
						kd_unik,
						loc_id_old,
						user_id_pick,
						pick_time,
						loc_id_new,
						user_id_put,
						put_time,
						process_name
					)VALUES(
						\''.$post['serial_number'].'\',
						\''.$r['loc_id'].'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\',
						\'104\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\',
						\''.$this->config->item('cycle_count_name').'\'
					)';

			$this->db->query($sql);

		}else{

			$sql = 'SELECT
						id_barang, loc_id, cc_time, user_id_cc
					FROM
						cycle_count_detail
					WHERE
						kd_unik = \''.$post['serial_number'].'\'';

			$r = $this->db->query($sql)->row_array();

			$sql = 'INSERT INTO int_transfer_detail(
						kd_unik,
						loc_id_old,
						user_id_pick,
						pick_time,
						loc_id_new,
						user_id_put,
						put_time,
						process_name
					)VALUES(
						\''.$post['serial_number'].'\',
						\'100\',
						\''.$r['user_id_cc'].'\',
						\''.$r['cc_time'].'\',
						\''.$r['loc_id'].'\',
						\''.$r['user_id_cc'].'\',
						\''.$r['cc_time'].'\',
						\''.$this->config->item('cycle_count_name').'\'
					)';

			$this->db->query($sql);

		}

		return $result;
	}

	public function adjustment($post = array()){
		$result = array();

		$data = json_decode($post['data'], true);

		$len = count($data);
		$ccId = 0;

		$snTemp = array();

		for($i = 0; $i < $len; $i++){

			$ccId = $data[$i]['cc_id'];
			$locId = $data[$i]['loc_id'];
			$idBarang = $data[$i]['id_barang'];
			$idQc = $data[$i]['id_qc'];
			$tglIn = $data[$i]['tgl_in'];
			$tglExp = $data[$i]['tgl_exp'];
			$sn = $data[$i]['sn'];
			$params = $data[$i]['params'];
			$status = $data[$i]['status'];
			$sys = $data[$i]['system_qty'];
			$scanned = $data[$i]['scanned_qty'];
			$qty = $data[$i]['cc_qty'];

			$sql = 'INSERT INTO cycle_count_result(
						cc_id,
						id_barang,
						loc_id,
						id_qc,
						kd_unik,
						tgl_in,
						tgl_exp,
						status,
						system_qty,
						scanned_qty,
						user_id
					)VALUES(
						\''.$ccId.'\',
						\''.$idBarang.'\',
						\''.$locId.'\',
						\''.$idQc.'\',
						\''.$sn.'\',
						\''.$tglIn.'\',
						\''.$tglExp.'\',
						\''.$status.'\',
						\''.$sys.'\',
						\''.$scanned.'\',
						\''.$this->session->userdata('user_id').'\'
					)';

			$this->db->query($sql);

			if($status != 'CANCEL'){
				if($params == 'NEGATIF'){

					//ITEM LOG
					$p = array(
						'params'		=> 'NEGATIF',
						'serial_number'	=> $sn
					);

					$this->setItemLog($p);

					$sql = 'UPDATE receiving_barang SET loc_id=\'104\', id_qc=\''.$idQc.'\' WHERE kd_unik=\''.$sn.'\'';
					$this->db->query($sql);
				}

				if($params == 'POSITIF'){

					//ITEM LOG
					$p = array(
						'params'		=> 'POSITIF',
						'serial_number'	=> $sn
					);


					//AWAS NIH
					$sql = 'SELECT
								COUNT(*) AS total
							FROM
								receiving_barang
							WHERE
								kd_unik=\''.$sn.'\'
							AND
								id_barang IS NOT NULL';

					$r = $this->db->query($sql)->row_array();
					if($r['total'] > 0){
						$snTemp[] = $sn;
						continue;
					}

					$sql = 'SELECT
								COUNT(*) AS total
							FROM
								receiving_barang
							WHERE
								kd_unik=\''.$sn.'\'
							AND
								id_barang IS NULL';

					$r = $this->db->query($sql)->row_array();

					if($r['total'] > 0){

						$sql = 'UPDATE
									receiving_barang
								SET
									id_barang=\''.$idBarang.'\',
									loc_id=\''.$locId.'\',
									id_qc=\''.$idQc.'\',
									tgl_in=\''.$tglIn.'\',
									tgl_exp=\''.$tglExp.'\',
									cc_id=\''.$ccId.'\',
									st_cc=\'1\',
									first_qty=\''.$qty.'\',
									last_qty=\''.$qty.'\'
								WHERE
									kd_unik=\''.$sn.'\'';

						$this->db->query($sql);

					}else{

						$sql = 'INSERT INTO receiving_barang(
									id_barang,
									loc_id,
									id_qc,
									tgl_in,
									tgl_exp,
									kd_unik,
									cc_id,
									st_cc,
									first_qty,
									last_qty
								)VALUES(
									\''.$idBarang.'\',
									\''.$locId.'\',
									\''.$idQc.'\',
									\''.$tglIn.'\',
									\''.$tglExp.'\',
									\''.$sn.'\',
									\''.$ccId.'\',
									\'1\',
									\''.$qty.'\',
									\''.$qty.'\'
								)';

						$this->db->query($sql);
					}

					$this->setItemLog($p);
				}
			}else{

				//if($params == 'NEGATIF'){
					$sql = 'UPDATE receiving_barang SET id_qc=\''.$idQc.'\', loc_id=\''.$locId.'\' WHERE kd_unik=\''.$sn.'\'';
					$this->db->query($sql);
				//}

			}

		}

		$sql = 'UPDATE cycle_count SET status = \'1\', remark=\''.$post['remark'].'\' WHERE cc_id=\''.$ccId.'\'';
		$this->db->query($sql);

		$result['status'] = 'OK';
		$result['message'] = 'Adjustment proccess success';
		$result['sn'] = $snTemp;

		return $result;
	}

	public function checkIsFinish($id){
		$result = array();

		$sql = 'SELECT
					cc_finish
				FROM
					cycle_count
				WHERE
					cc_id=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();

		if(empty($row['cc_finish'])){
			$result = 'false';
		}else{
			$result = 'true';
		}

		return $result;
	}

	public function checkIsUsed($id){
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					cycle_count_detail
				WHERE
					cc_id=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();

		if($row['total'] > 0){
			$result['is_used'] = true;
		}else{
			$result['is_used'] = false;
		}

		return $result;
	}

	public function getDetailCheck($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'cc_id',
			2 => 'loc_id',
			3 => 'id_barang',
			4 => 'id_qc',
			5 => 'tgl_in',
			6 => 'tgl_exp',
			7 => '',
			8 => 'loc_name',
			9 => 'nama_barang',
			10 => 'kd_unik',
			11 => 'status',
			12 => 'system_qty',
			13 => 'scanned_qty'
		);

		if($post['cc_type'] == 'SN'){

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
					(
						SELECT
							COUNT(*)
						FROM
						(
							(
								SELECT
									cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
									cctq.id_qc, rcvbrg.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
									rcvbrg.tgl_in, rcvbrg.tgl_exp
								FROM
									cycle_count cc1
								JOIN cycle_count_h2 cc2
									ON cc1.cc_id=cc2.cc_id_h1
								JOIN receiving_barang rcvbrg
									ON rcvbrg.id_barang=cc2.item_id
								JOIN barang brg
									ON brg.id_barang=cc2.item_id
								JOIN m_loc loc
									ON loc.loc_id=rcvbrg.loc_id
								LEFT JOIN cycle_count_temp_qc cctq
									ON cctq.kd_unik=rcvbrg.kd_unik
										AND cctq.cc_id=cc1.cc_id
								LEFT JOIN cycle_count_detail ccd
									ON ccd.kd_unik=rcvbrg.kd_unik
										AND ccd.cc_id=cc1.cc_id
								LEFT JOIN satuan sat
									ON sat.id_satuan=brg.id_satuan
								WHERE
									cc1.cc_id=\''.$post['cc_id'].'\'
								AND
									loc.loc_type NOT IN('.$this->config->item('cc_location').')
							)
							UNION
							(
								SELECT
									cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
									ccd.id_qc, ccd.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
									ccd.tgl_in, ccd.tgl_exp
								FROM
									cycle_count cc1
								JOIN cycle_count_h2 cc2
									ON cc1.cc_id=cc2.cc_id_h1
								JOIN cycle_count_detail ccd
									ON ccd.cc_id=cc1.cc_id
								LEFT JOIN receiving_barang rcvbrg
									ON rcvbrg.kd_unik=ccd.kd_unik
										AND rcvbrg.id_barang=ccd.id_barang
								LEFT JOIN barang brg
									ON brg.id_barang=ccd.id_barang
								LEFT JOIN m_loc loc
									ON loc.loc_id=ccd.loc_id
								LEFT JOIN satuan sat
									ON sat.id_satuan=brg.id_satuan
								WHERE
									cc1.cc_id=\''.$post['cc_id'].'\'
								AND
									loc.loc_type NOT IN('.$this->config->item('cc_location').')
							)
						) AS tbl
						GROUP BY
							kd_unik
					) AS tbl2';

			$row = $this->db->query($sql)->row_array();
			$totalData = $row['total'];

			$sql = 'SELECT
						cc_id, cc_finish, loc_id, id_barang, loc_name, kd_barang, nama_barang, id_qc, kd_unik, status, cc_qty,
						(
							CASE
								WHEN
									id_receiving_barang IS NULL
								AND
									cc_detail_id IS NOT NULL
								THEN
									\'NO\'
								ELSE
								\'YES\'
							END
						) AS system_qty,
						(
							CASE
								WHEN
									id_receiving_barang IS NOT NULL
								AND
									cc_detail_id IS NULL
								THEN
									\'NO\'
								ELSE
									\'YES\'
							END
						) AS scanned_qty,
						tgl_in, tgl_exp
					FROM
					(
						(
							SELECT
								cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
								cctq.id_qc, rcvbrg.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
								rcvbrg.tgl_in, rcvbrg.tgl_exp, ccd.cc_qty
							FROM
								cycle_count cc1
							JOIN cycle_count_h2 cc2
								ON cc1.cc_id=cc2.cc_id_h1
							JOIN receiving_barang rcvbrg
								ON rcvbrg.id_barang=cc2.item_id
							JOIN barang brg
								ON brg.id_barang=cc2.item_id
							JOIN m_loc loc
								ON loc.loc_id=rcvbrg.loc_id
							LEFT JOIN cycle_count_temp_qc cctq
								ON cctq.kd_unik=rcvbrg.kd_unik
									AND cctq.cc_id=cc1.cc_id
							LEFT JOIN cycle_count_detail ccd
								ON ccd.kd_unik=rcvbrg.kd_unik
									AND ccd.cc_id=cc1.cc_id
							LEFT JOIN satuan sat
								ON sat.id_satuan=brg.id_satuan
							WHERE
								cc1.cc_id=\''.$post['cc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						)
						UNION
						(
							SELECT
								cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
								ccd.id_qc, ccd.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
								ccd.tgl_in, ccd.tgl_exp, ccd.cc_qty
							FROM
								cycle_count cc1
							JOIN cycle_count_h2 cc2
								ON cc1.cc_id=cc2.cc_id_h1
							JOIN cycle_count_detail ccd
								ON ccd.cc_id=cc1.cc_id
							LEFT JOIN receiving_barang rcvbrg
								ON rcvbrg.kd_unik=ccd.kd_unik
									AND rcvbrg.id_barang=ccd.id_barang
							LEFT JOIN barang brg
								ON brg.id_barang=ccd.id_barang
							LEFT JOIN m_loc loc
								ON loc.loc_id=ccd.loc_id
							LEFT JOIN satuan sat
								ON sat.id_satuan=brg.id_satuan
							WHERE
								cc1.cc_id=\''.$post['cc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						)
					) AS tbl
					GROUP BY
						kd_unik';

		}else{

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
					(
						SELECT
							COUNT(*) AS total
						FROM
						(
							(
								SELECT
									cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
									cctq.id_qc, rcvbrg.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
									rcvbrg.tgl_in, rcvbrg.tgl_exp
								FROM
									cycle_count cc1
								JOIN cycle_count_h2 cc2
									ON cc1.cc_id=cc2.cc_id_h1
								JOIN receiving_barang rcvbrg
									ON rcvbrg.loc_id=cc2.loc_id
								JOIN barang brg
									ON brg.id_barang=rcvbrg.id_barang
								JOIN m_loc loc
									ON loc.loc_id=rcvbrg.loc_id
								LEFT JOIN cycle_count_detail ccd
									ON ccd.kd_unik=rcvbrg.kd_unik
										AND ccd.cc_id=cc1.cc_id
								LEFT JOIN cycle_count_temp_qc cctq
									ON cctq.kd_unik=rcvbrg.kd_unik
										AND cctq.cc_id=cc1.cc_id
								LEFT JOIN satuan sat
									ON sat.id_satuan=brg.id_satuan
								WHERE
									cc1.cc_id=\''.$post['cc_id'].'\'
								AND
									loc.loc_type NOT IN('.$this->config->item('cc_location').')
							)
							UNION
							(
								SELECT
									cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
									ccd.id_qc, ccd.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
									ccd.tgl_in, ccd.tgl_exp
								FROM
									cycle_count cc1
								JOIN cycle_count_h2 cc2
									ON cc1.cc_id=cc2.cc_id_h1
								JOIN cycle_count_detail ccd
									ON ccd.cc_id=cc1.cc_id
								LEFT JOIN receiving_barang rcvbrg
									ON rcvbrg.kd_unik=ccd.kd_unik
								LEFT JOIN barang brg
									ON brg.id_barang=ccd.id_barang
								LEFT JOIN m_loc loc
									ON loc.loc_id=ccd.loc_id
								LEFT JOIN satuan sat
									ON sat.id_satuan=brg.id_satuan
								WHERE
									cc1.cc_id=\''.$post['cc_id'].'\'
								AND
									loc.loc_type NOT IN('.$this->config->item('cc_location').')
							)
						) AS tbl
						GROUP BY
							kd_unik
					) AS tbl2';

			$row = $this->db->query($sql)->row_array();
			$totalData = $row['total'];

			$sql = 'SELECT
						cc_id, cc_finish, loc_id, id_barang, loc_name, kd_barang, nama_barang, id_qc, kd_unik, status, cc_qty,
						(
							CASE
								WHEN
									id_receiving_barang IS NULL
								AND
									cc_detail_id IS NOT NULL
								THEN
									\'NO\'
								ELSE
								\'YES\'
							END
						) AS system_qty,
						(
							CASE
								WHEN
									id_receiving_barang IS NOT NULL
								AND
									cc_detail_id IS NULL
								THEN
									\'NO\'
								ELSE
									\'YES\'
							END
						) AS scanned_qty,
						tgl_in, tgl_exp
					FROM
					(
						(
							SELECT
								cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
								cctq.id_qc, rcvbrg.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
								rcvbrg.tgl_in, rcvbrg.tgl_exp, ccd.cc_qty
							FROM
								cycle_count cc1
							JOIN cycle_count_h2 cc2
								ON cc1.cc_id=cc2.cc_id_h1
							JOIN receiving_barang rcvbrg
								ON rcvbrg.loc_id=cc2.loc_id
							JOIN barang brg
								ON brg.id_barang=rcvbrg.id_barang
							JOIN m_loc loc
								ON loc.loc_id=rcvbrg.loc_id
							LEFT JOIN cycle_count_detail ccd
								ON ccd.kd_unik=rcvbrg.kd_unik
									AND ccd.cc_id=cc1.cc_id
							LEFT JOIN cycle_count_temp_qc cctq
								ON cctq.kd_unik=rcvbrg.kd_unik
									AND cctq.cc_id=cc1.cc_id
							LEFT JOIN satuan sat
								ON sat.id_satuan=brg.id_satuan
							WHERE
								cc1.cc_id=\''.$post['cc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						)
						UNION
						(
							SELECT
								cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
								ccd.id_qc, ccd.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
								ccd.tgl_in, ccd.tgl_exp, ccd.cc_qty
							FROM
								cycle_count cc1
							JOIN cycle_count_h2 cc2
								ON cc1.cc_id=cc2.cc_id_h1
							JOIN cycle_count_detail ccd
								ON ccd.cc_id=cc1.cc_id
							LEFT JOIN receiving_barang rcvbrg
								ON rcvbrg.kd_unik=ccd.kd_unik
							LEFT JOIN barang brg
								ON brg.id_barang=ccd.id_barang
							LEFT JOIN m_loc loc
								ON loc.loc_id=ccd.loc_id
							LEFT JOIN satuan sat
								ON sat.id_satuan=brg.id_satuan
							WHERE
								cc1.cc_id=\''.$post['cc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						)
					) AS tbl
					GROUP BY
						kd_unik';

		}

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];

		//$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';
			/*
			$disabled = '';
			if($row[$i]['total_scanned'] > 0)
				$disabled = '';
			else
				$disabled = 'disabled-link';

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$disabled.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Close CC</a>';
			$action .= '</li>';
			*/

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-adjust" data-id="'.$row[$i]['cc_id'].'" data-name="ADJUSTMENT"><i class="fa fa-edit"></i> Adjustment</a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-cancel" data-id="'.$row[$i]['cc_id'].'" data-name="CANCEL"><i class="fa fa-close"></i> Cancel</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			if(empty($row[$i]['cc_finish']))
				$action = '';

			$nested = array();
			$nested[] = '<input type="checkbox" name="sn[]" value="">';
			$nested[] = $row[$i]['cc_id'];
			$nested[] = $row[$i]['loc_id'];
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $row[$i]['id_qc'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['kd_unik'];

			$status = $row[$i]['status'];

			if($row[$i]['system_qty'] == 'YES' && $row[$i]['scanned_qty'] == 'YES'){
				$action = '';
				$status = 'CANCEL';
			}

			$nested[] = $status;
			$nested[] = $row[$i]['system_qty'];
			$nested[] = $row[$i]['scanned_qty'];
			$nested[] = $row[$i]['cc_qty'];

			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItem($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'loc_name',
			2 => 'nama_barang',
			3 => 'qty',
			4 => '( qty_number - check_result_number )'
		);

		if($post['cc_type'] == 'SN'){

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
					(
						SELECT
							cc1.cc_id
						FROM
							cycle_count cc1
						JOIN cycle_count_h2 cc2
							ON cc1.cc_id=cc2.cc_id_h1
						LEFT JOIN receiving_barang rcvbrg
							ON rcvbrg.id_barang=cc2.item_id
						LEFT JOIN barang brg
							ON brg.id_barang=cc2.item_id
						LEFT JOIN m_loc loc
							ON loc.loc_id=rcvbrg.loc_id
						LEFT JOIN satuan sat
							ON sat.id_satuan=brg.id_satuan
						WHERE
							cc1.cc_id=\''.$post['cc_id'].'\'
						AND
							loc.loc_type NOT IN('.$this->config->item('cc_location').')
						GROUP BY
							rcvbrg.loc_id, rcvbrg.id_barang
					) AS tbl';

			$row = $this->db->query($sql)->row_array();
			$totalData = $row['total'];

			$sql = 'SELECT
						cc1.cc_id, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
						CONCAT(COUNT(rcvbrg.id_barang), \' \', IFNULL(nama_satuan, \'\')) AS qty,
						COUNT(rcvbrg.id_barang) AS qty_number,
						CONCAT(IFNULL((
							SELECT
								SUM(cc_qty)
							FROM
								cycle_count_detail cc_detail
							WHERE
								cc_id=\''.$post['cc_id'].'\'
						), \'0\'), \' \', IFNULL(nama_satuan, \'\')) AS check_result,
						IFNULL((
							SELECT
								SUM(cc_qty)
							FROM
								cycle_count_detail cc_detail
							WHERE
								cc_id=\''.$post['cc_id'].'\'
						), \'0\') AS check_result_number,
						IFNULL(nama_satuan, \'\') AS nama_satuan
					FROM
						cycle_count cc1
					JOIN cycle_count_h2 cc2
						ON cc1.cc_id=cc2.cc_id_h1
					JOIN receiving_barang rcvbrg
						ON rcvbrg.id_barang=cc2.item_id
					JOIN barang brg
						ON brg.id_barang=cc2.item_id
					JOIN m_loc loc
						ON loc.loc_id=rcvbrg.loc_id
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						cc1.cc_id=\''.$post['cc_id'].'\'
					AND
						loc.loc_type NOT IN('.$this->config->item('cc_location').')
					GROUP BY
						rcvbrg.loc_id, rcvbrg.id_barang';

		}else{

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
					(
						SELECT
							cc1.cc_id
						FROM
							cycle_count cc1
						JOIN cycle_count_h2 cc2
							ON cc1.cc_id=cc2.cc_id_h1
						LEFT JOIN receiving_barang rcvbrg
							ON rcvbrg.loc_id=cc2.loc_id
						LEFT JOIN barang brg
							ON brg.id_barang=cc2.item_id
						LEFT JOIN m_loc loc
							ON loc.loc_id=rcvbrg.loc_id
						LEFT JOIN satuan sat
							ON sat.id_satuan=brg.id_satuan
						WHERE
							cc1.cc_id=\''.$post['cc_id'].'\'
						AND
							loc.loc_type NOT IN('.$this->config->item('cc_location').')
						GROUP BY
							rcvbrg.loc_id, rcvbrg.id_barang
					) AS tbl';

			$row = $this->db->query($sql)->row_array();
			$totalData = $row['total'];

			$sql = 'SELECT
						cc1.cc_id, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
						CONCAT(COUNT(rcvbrg.id_barang), \' \', IFNULL(nama_satuan, \'\')) AS qty,
						COUNT(rcvbrg.id_barang) AS qty_number,
						CONCAT(IFNULL((
							SELECT
								SUM(cc_qty)
							FROM
								cycle_count_detail cc_detail
							WHERE
								cc_id=\''.$post['cc_id'].'\'
						), \'0\'), \' \', IFNULL(nama_satuan, \'\')) AS check_result,
						IFNULL((
							SELECT
								SUM(cc_qty)
							FROM
								cycle_count_detail cc_detail
							WHERE
								cc_id=\''.$post['cc_id'].'\'
						), \'0\') AS check_result_number,
						IFNULL(nama_satuan, \'\') AS nama_satuan
					FROM
						cycle_count cc1
					JOIN cycle_count_h2 cc2
						ON cc1.cc_id=cc2.cc_id_h1
					JOIN receiving_barang rcvbrg
						ON rcvbrg.loc_id=cc2.loc_id
					JOIN barang brg
						ON brg.id_barang=rcvbrg.id_barang
					JOIN m_loc loc
						ON loc.loc_id=rcvbrg.loc_id
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						cc1.cc_id=\''.$post['cc_id'].'\'
					AND
						loc.loc_type NOT IN('.$this->config->item('cc_location').')
					GROUP BY
						rcvbrg.loc_id, rcvbrg.id_barang';

		}

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';
			/*
			$disabled = '';
			if($row[$i]['total_scanned'] > 0)
				$disabled = '';
			else
				$disabled = 'disabled-link';

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$disabled.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Close CC</a>';
			$action .= '</li>';
			*/

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['qty'];
			$nested[] = $row[$i]['check_result'];
			$nested[] = $row[$i]['qty_number'] - $row[$i]['check_result_number'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailDataTable($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'name'
		);

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
				(
					SELECT
						COUNT(*)
					FROM
						cycle_count h1
					JOIN cycle_count_h2 h2
						ON h1.cc_id=h2.cc_id_h1
					LEFT OUTER JOIN m_loc loc
						ON loc.loc_id=h2.loc_id
							AND h1.cc_type=\'LOC\'
					LEFT OUTER JOIN barang brg
						ON brg.id_barang=h2.item_id
							AND h1.cc_type=\'SN\'
					WHERE
						h1.cc_id=\''.$post['cc_id'].'\'
					GROUP BY
						IFNULL(loc_name, h2.item_id)
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					IF(h1.cc_type=\'LOC\' AND h2.loc_id=0,concat(\'ALL AREA \',loc_area.loc_area_name),IFNULL(loc_name, CONCAT(kd_barang, \' - \', nama_barang))) AS name
				FROM
					cycle_count h1
				JOIN cycle_count_h2 h2
					ON h1.cc_id=h2.cc_id_h1
				LEFT OUTER JOIN m_loc loc
					ON loc.loc_id=h2.loc_id
						AND h1.cc_type=\'LOC\'
                LEFT OUTER JOIN m_loc_area loc_area
					ON loc_area.loc_area_id=h2.area_id
						AND h1.cc_type=\'LOC\'
				LEFT OUTER JOIN barang brg
					ON brg.id_barang=h2.item_id
						AND h1.cc_type=\'SN\'
				WHERE
					h1.cc_id=\''.$post['cc_id'].'\'
				GROUP BY
					IFNULL(loc_name, h2.item_id)';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['name'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function closeCc($post = array()){
		$result = array();

		$sql = 'UPDATE cycle_count SET remark=\''.$post['remark'].'\', status=\'1\' WHERE cc_id=\''.$post['cc_id'].'\'';
		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Close stock opname success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Close stock opname failed';
		}

		return $result;
	}

    public function getScans($post = array()){
        $this->db->select('count(kd_unik) as qty, hr_user.nama, id_barang');
        $this->db->group_by('id_barang');
        $this->db->group_by('cycle_count_result.user_id');
        $this->db->join('hr_user','hr_user.user_id=cycle_count_result.user_id','LEFT');
        $result = $this->db->get_where('cycle_count_result',$post);
        return $result->result_array();
    }

	public function getItems($post = array()){

		$result = array();

		if($post['cc_type'] == 'SN'){

			$sql = 'SELECT
						IFNULL(loc_name, \'-\') AS loc_name,
						IFNULL(kd_barang, \'\') AS kd_barang,
						CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
						nama_barang AS nama_barang_ori,
                        brg.id_barang,
						(
							SELECT
								COUNT(*)
							FROM
								receiving_barang
							WHERE
								loc_id=rcvbrg.loc_id
							AND
								id_barang=rcvbrg.id_barang
						) AS qty, nama_satuan
					FROM
						cycle_count cc1
					JOIN cycle_count_h2 cc2
						ON cc1.cc_id=cc2.cc_id_h1
					JOIN barang brg
						ON brg.id_barang=cc2.item_id
					LEFT JOIN receiving_barang rcvbrg
						ON rcvbrg.id_barang=cc2.item_id
					LEFT JOIN m_loc loc
						ON loc.loc_id=rcvbrg.loc_id
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						cc1.cc_id=\''.$post['cc_id'].'\'
					AND
						CASE
							loc.loc_type
						WHEN loc.loc_type IS NOT NULL AND loc.loc_type != '.$this->config->item('sentout_location').'
							THEN
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						ELSE
							1=1
						END
					AND
						CASE
							rcvbrg.loc_id
						WHEN rcvbrg.loc_id IS NOT NULL
							THEN
								rcvbrg.loc_id NOT IN(
									SELECT
										loc_id
									FROM
										cycle_count_h2 cc2
									JOIN cycle_count cc
										ON cc.cc_id=cc2.cc_id_h1
									WHERE
										cc.status != \'1\'
									AND
										cc.cc_type = \'LOC\'
									AND
										cc.cc_id < \''.$post['cc_id'].'\'
								)
						ELSE
							1=1
						END
					AND
						CASE
							rcvbrg.id_barang
						WHEN rcvbrg.id_barang IS NOT NULL
							THEN
								rcvbrg.id_barang NOT IN(
									SELECT
										item_id
									FROM
										cycle_count_h2 cc2
									JOIN cycle_count cc
										ON cc.cc_id=cc2.cc_id_h1
									WHERE
										cc.status != \'1\'
									AND
										cc.cc_type = \'SN\'
									AND
										cc.cc_id < \''.$post['cc_id'].'\'
								)
						ELSE
							1=1
						END
					AND
						CASE
							rcvbrg.kd_unik
						WHEN rcvbrg.kd_unik IS NOT NULL
							THEN
								rcvbrg.kd_unik NOT IN(
									SELECT
										kd_unik
									FROM
										cycle_count_h2 cc2
									JOIN cycle_count cc
										ON cc.cc_id=cc2.cc_id_h1
									JOIN receiving_barang rcvbrg
										ON rcvbrg.loc_id=cc2.loc_id
									WHERE
										cc.status != \'1\'
									AND
										cc.cc_type = \'LOC\'
									AND
										cc.cc_id < \''.$post['cc_id'].'\'
								)
						ELSE
							1=1
						END
					GROUP BY
						rcvbrg.loc_id, rcvbrg.id_barang
					ORDER BY
						brg.kd_barang ASC';

		}else{

			$sql = 'SELECT
						IFNULL(loc_name, \'-\') AS loc_name,
						IFNULL(kd_barang, \'\') AS kd_barang,
						CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
						nama_barang AS nama_barang_ori,
                        brg.id_barang,
						(
							SELECT
								COUNT(*)
							FROM
								receiving_barang
							WHERE
								loc_id=rcvbrg.loc_id
							AND
								id_barang=rcvbrg.id_barang
						) AS qty, nama_satuan
					FROM
						cycle_count cc1
					JOIN cycle_count_h2 cc2
						ON cc1.cc_id=cc2.cc_id_h1
					LEFT JOIN receiving_barang rcvbrg
						ON rcvbrg.loc_id=cc2.loc_id
					LEFT JOIN barang brg
						ON brg.id_barang=rcvbrg.id_barang
					LEFT JOIN m_loc loc
						ON loc.loc_id=cc2.loc_id
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						cc1.cc_id=\''.$post['cc_id'].'\'
					AND
						loc.loc_type NOT IN ('.$this->config->item('cc_location').')
					AND
						CASE
							rcvbrg.loc_id
						WHEN rcvbrg.loc_id IS NOT NULL
							THEN
								rcvbrg.loc_id NOT IN(
									SELECT
										loc_id
									FROM
										cycle_count_h2 cc2
									JOIN cycle_count cc
										ON cc.cc_id=cc2.cc_id_h1
									WHERE
										cc.status != \'1\'
									AND
										cc.cc_type = \'LOC\'
									AND
										cc.cc_id < \''.$post['cc_id'].'\'
								)
						ELSE
							1=1
						END
					AND
						CASE
							rcvbrg.id_barang
						WHEN rcvbrg.id_barang IS NOT NULL
							THEN
								rcvbrg.id_barang NOT IN(
									SELECT
										item_id
									FROM
										cycle_count_h2 cc2
									JOIN cycle_count cc
										ON cc.cc_id=cc2.cc_id_h1
									WHERE
										cc.status != \'1\'
									AND
										cc.cc_type = \'SN\'
									AND
										cc.cc_id < \''.$post['cc_id'].'\'
								)
						ELSE
							1=1
						END
					AND
						CASE
							rcvbrg.kd_unik
						WHEN rcvbrg.kd_unik IS NOT NULL
							THEN
								rcvbrg.kd_unik NOT IN(
									SELECT
										kd_unik
									FROM
										cycle_count_h2 cc2
									JOIN cycle_count cc
										ON cc.cc_id=cc2.cc_id_h1
									JOIN receiving_barang rcvbrg
										ON rcvbrg.loc_id=cc2.loc_id
									WHERE
										cc.status != \'1\'
									AND
										cc.cc_type = \'LOC\'
									AND
										cc.cc_id < \''.$post['cc_id'].'\'
								)
						ELSE
							1=1
						END
					GROUP BY
						rcvbrg.loc_id, rcvbrg.id_barang
					ORDER BY
						brg.kd_barang ASC';

		}

		$result = $this->db->query($sql)->result_array();
		//var_dump($result);
		return $result;
	}

	public function delete($post = array()){
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					cycle_count_detail
				WHERE
					cc_id=\''.$post['cc_id'].'\'';

		$row = $this->db->query($sql)->row_array();

		if($row['total'] > 0){

			$result['status'] = 'ERR';
			$result['message'] = 'Delete failed. Because data has been used';

		}else{

			$sql = 'SELECT
						*
					FROM
						cycle_count_temp_qc
					WHERE
						cc_id=\''.$post['cc_id'].'\'';

			$row = $this->db->query($sql)->result_array();
			$len = count($row);

			for($i = 0; $i < $len; $i++){
				$sql = 'UPDATE
							receiving_barang
						SET
							id_qc=\''.$row[$i]['id_qc'].'\'
						WHERE
							kd_unik=\''.$row[$i]['kd_unik'].'\'';

				$this->db->query($sql);
			}

			$sql = 'DELETE FROM cycle_count WHERE cc_id=\''.$post['cc_id'].'\'';
			$this->db->query($sql);

			$sql = 'DELETE FROM cycle_count_h2 WHERE cc_id_h1=\''.$post['cc_id'].'\'';
			$this->db->query($sql);

			$result['status'] = 'OK';
			$result['message'] = 'Delete success';

		}

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$sql = 'SELECT
					loc_id, item_id, area_id
				FROM
					cycle_count_h2
				WHERE
					cc_id_h1=\''.$post['cc_id'].'\'
				GROUP BY
					loc_id, item_id';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getEdit($post = array()){
		$result = array();

		$sql = 'SELECT
					cc_id, cc_code, DATE_FORMAT(cc_time, \'%d\/%m\/%Y\') AS cc_time,
					cc_type, status, no_person,
					(
						CASE cc_type
							WHEN \'SN\'
								THEN \'By Item\'
							WHEN \'LOC\'
								THEN \'By Location\'
						END
					) AS cc_type_alias, user_name, nama_status
				FROM
					cycle_count cc
				JOIN hr_user hu
					ON hu.user_id=cc.user_id
				JOIN m_status_cycle_count sts
					ON sts.id_status_cc=cc.status
				WHERE
					cc_id=\''.$post['cc_id'].'\'';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function getCcCode($post = array()){
		$result = array();

		$sql = 'SELECT
					cc_id, cc_code
				FROM
					cycle_count
				WHERE
					cc_code LIKE \'%'.$post['query'].'%\'';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getCcStatus(){
		$result = array();

		$sql = 'SELECT * FROM m_status_cycle_count';
		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function create($data, $detail) {
        $this->db->trans_start();

        $data['kategori'] = 'stock_opname';

        $this->db->insert('cycle_count', $data);
        $ccId = $this->db->insert_id();

        if(!empty($detail)) {
            foreach ($detail as &$row) {
                $row['cc_id_h1'] = $ccId;

				if($data['cc_type'] == 'SN'){

					$sql = 'SELECT
								kd_unik,id_qc
							FROM
								receiving_barang
							WHERE
								id_barang = \''.$row['item_id'].'\'';

					$rows = $this->db->query($sql)->result_array();
					$len = count($rows);

					for($i = 0; $i < $len; $i++){
						$sql = 'INSERT INTO cycle_count_temp_qc(
									cc_id,
									kd_unik,
									id_qc
								)VALUES(
									\''.$ccId.'\',
									\''.$rows[$i]['kd_unik'].'\',
									\''.$rows[$i]['id_qc'].'\'
								)';

						$this->db->query($sql);
					}

					//$sql = 'UPDATE receiving_barang SET cc_id=\''.$ccId.'\', st_cc=\'0\' WHERE id_barang=\''.$row['item_id'].'\'';

					$sql = 'UPDATE
								receiving_barang rcvbrg
							JOIN m_loc loc
								ON loc.loc_id=rcvbrg.loc_id
							SET
								cc_id=\''.$ccId.'\',
								st_cc=\'0\',
								id_qc=\'4\'
							WHERE
								id_barang=\''.$row['item_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')';

					$this->db->query($sql);

					// $sql = 'UPDATE
								// receiving_barang rcvbrg
							// JOIN m_loc loc
								// ON loc.loc_id=rcvbrg.loc_id
							// SET
								// id_qc=\'4\'
							// WHERE
								// id_barang=\''.$row['item_id'].'\'
							// AND
								// loc.loc_type NOT IN('.$this->config->item('cc_location').')';

					// $this->db->query($sql);

                    date_default_timezone_set('Asia/Jakarta');

                    $detail2=array(
                        'cc_id'=>$ccId,
                        'id_barang'=>$row['item_id'],
                        'user_id_cc'=>$this->session->userdata('user_id'),
                        'cc_time'=>date('Y-m-d H:i:s')
                    );

                    $this->db->insert('cycle_count_detail', $detail2);

				}else{

					$sql = 'SELECT
								kd_unik,id_qc
							FROM
								receiving_barang
							WHERE
								loc_id = \''.$row['loc_id'].'\'';

					$rows = $this->db->query($sql)->result_array();
					$len = count($rows);

					for($i = 0; $i < $len; $i++){
						$sql = 'INSERT INTO cycle_count_temp_qc(
									cc_id,
									kd_unik,
									id_qc
								)VALUES(
									\''.$ccId.'\',
									\''.$rows[$i]['kd_unik'].'\',
									\''.$rows[$i]['id_qc'].'\'
								)';

						$this->db->query($sql);
					}

					$sql = 'UPDATE receiving_barang SET cc_id=\''.$ccId.'\', st_cc=\'0\' WHERE loc_id=\''.$row['loc_id'].'\'';
					$this->db->query($sql);

					$sql = 'UPDATE
								receiving_barang rcvbrg
							JOIN m_loc loc
								ON loc.loc_id=rcvbrg.loc_id
							SET
								cc_id=\''.$ccId.'\',
								st_cc=\'0\',
								id_qc=\'4\'
							WHERE
								rcvbrg.loc_id=\''.$row['loc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')';

					$this->db->query($sql);

                    date_default_timezone_set('Asia/Jakarta');

                    $detail2=array(
                        'cc_id'=>$ccId,
                        'loc_id'=>$row['loc_id'],
                        'loc_area_id'=>$row['area_id'],
                        'user_id_cc'=>$this->session->userdata('user_id'),
                        'cc_time'=>date('Y-m-d H:i:s')
                    );

                    $this->db->insert('cycle_count_detail', $detail2);
				}

            }

            $this->db->insert_batch('cycle_count_h2', $detail);
        }

        $this->db->trans_complete();
		return $this->db->trans_status();
    }

    public function update($data, $detail, $id) {
        $this->db->trans_start();

        $sql = 'UPDATE
					cycle_count
				SET
					cc_time=\''.$data['cc_time'].'\',
					cc_type=\''.$data['cc_type'].'\',
					user_id=\''.$data['user_id'].'\'
				WHERE
					cc_id=\''.$id.'\'';

		$this->db->query($sql);

        $this->db->where("cc_id_h1", $id);
        $this->db->delete('cycle_count_h2');

        $this->db->where("cc_id", $id);
        $this->db->delete('cycle_count_detail');

        if(!empty($detail)) {
            foreach ($detail as &$row) {
                $row['cc_id_h1'] = $id;

                date_default_timezone_set('Asia/Jakarta');

                if(isset($row['item_id'])){
                    $detail2=array(
                        'cc_id'=>$id,
                        'id_barang'=>$row['item_id'],
                        'user_id_cc'=>$this->session->userdata('user_id'),
                        'cc_time'=>date('Y-m-d H:i:s')
                    );
                }else{

                    $detail2=array(
                        'cc_id'=>$id,
                        'loc_id'=>$row['loc_id'],
                        'loc_area_id'=>$row['area_id'],
                        'user_id_cc'=>$this->session->userdata('user_id'),
                        'cc_time'=>date('Y-m-d H:i:s')
                    );
                }

                $this->db->insert('cycle_count_detail', $detail2);
            }

            $this->db->insert_batch('cycle_count_h2', $detail);
        }

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

	public function getChooseItem($post = array()){
		$result = array();

		$where = '';
		if(isset($post['cc_id'])){
			$where .= ' AND cc.cc_id != \''.$post['cc_id'].'\' ';
		}

		$sql = 'SELECT
					id_barang, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang
				FROM
					barang
				WHERE
					id_barang NOT IN(
						SELECT
							item_id
						FROM
							cycle_count_h2 cc2
						JOIN cycle_count cc
							ON cc.cc_id=cc2.cc_id_h1
						WHERE
							cc.status != \'1\'
						AND
							cc.cc_type = \'SN\''.$where.'
					)';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getLocation($post = array()){
		$result = array();

		$where = '';
		if(isset($post['cc_id'])){
			$where .= ' AND cc.cc_id != \''.$post['cc_id'].'\' ';
		}

		$sql = 'SELECT
					*
				FROM
					m_loc
				WHERE
					loc_id NOT IN('.$this->config->item('cc_location_id').')
				AND
					loc_name IS NOT NULL
				AND
					loc_id NOT IN(
						SELECT
							loc_id
						FROM
							cycle_count_h2 cc2
						JOIN cycle_count cc
							ON cc.cc_id=cc2.cc_id_h1
						WHERE
							cc.status != \'1\'
						AND
							cc.cc_type = \'LOC\''.$where.'
					)
				ORDER BY loc_name ASC';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    public function getLocation2($post = array()){
		$result = array();

        $where = '';
		if(isset($post['cc_id'])){
			$where .= ' AND cc.cc_id != \''.$post['cc_id'].'\' ';
		}

        if($post['loc_area_id']==''){
            $loc_area_id=0;
        }else{
            $loc_area_id=$post['loc_area_id'];
        }

		$sql = 'SELECT
					*
				FROM
					m_loc
				WHERE
					loc_id NOT IN('.$this->config->item('cc_location_id').')
				AND
					loc_name IS NOT NULL
                AND
                    loc_area_id = '.$loc_area_id.'

				ORDER BY loc_name ASC';

		/*$sql = 'SELECT
					*
				FROM
					m_loc
				WHERE
					loc_id NOT IN('.$this->config->item('cc_location_id').')
				AND
					loc_name IS NOT NULL
                AND
                    loc_area_id = '.$loc_area_id.'
				AND
					loc_id NOT IN(
						SELECT
							loc_id
						FROM
							cycle_count_h2 cc2
						JOIN cycle_count cc
							ON cc.cc_id=cc2.cc_id_h1
						WHERE
							cc.status != \'1\'
						AND
                            cc.cc_type = \'LOC\''.$where.'
					)
				ORDER BY loc_name ASC';*/

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    public function getLocationArea($post = array()){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_loc_area
				WHERE
					loc_area_name IS NOT NULL
				ORDER BY loc_area_name ASC';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getList($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => 'cc_id',
			1 => '',
			2 => 'cc_code',
			3 => 'cc_type',
			4 => 'cc_time',
			5 => 'remark',
			6 => 'cc_start',
			7 => 'cc_finish',
			8 => 'status'
		);

		$where = '';

		if(isset($post['cc_id'])){
			if(!empty($post['cc_id']))
				$where .= sprintf(' AND cc_id_h1 = \'%s\' ', $post['cc_id']);
		}

		if($post['status'] != '')
			$where .= sprintf(' AND status = \'%s\' ', $post['status']);


		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(cc_time) BETWEEN \'%s\' AND \'%s\' ',
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					cycle_count cc
				JOIN m_status_cycle_count mcc
					ON mcc.id_status_cc=cc.status
				WHERE kategori="stock_opname"'.$where;

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					cc_id, cc_code,
					(
						CASE cc_type
							WHEN \'SN\'
								THEN \'By Item\'
							ELSE
								\'By Location\'
						END
					) AS cc_type,
					DATE_FORMAT(cc_time, \'%d\/%m\/%Y\') AS cc_time,
					IFNULL(remark, \'-\') AS remark, status, nama_status,
					(
						SELECT
							COUNT(*)
						FROM
							cycle_count_detail
						WHERE
							cc_id=cc.cc_id
					) AS total_scanned,
					(
						CASE
							WHEN cc_start IS NULL
								THEN \'-\'
							ELSE DATE_FORMAT(cc_start, \'%H:%i<br>%d\/%m\/%Y\')
						END
					) AS cc_start,
					(
						CASE
							WHEN cc_finish IS NULL
								THEN \'-\'
							ELSE DATE_FORMAT(cc_finish, \'%H:%i<br>%d\/%m\/%Y\')
						END
					) AS cc_finish
				FROM
					cycle_count cc
				JOIN m_status_cycle_count mcc
					ON mcc.id_status_cc=cc.status
				WHERE kategori="stock_opname"'.$where;

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			$dis = '';
			$disabled = '';
			$disabled2 = '';
			$disabled3 = '';

			if($row[$i]['total_scanned'] > 0){
				if($row[$i]['status'] == '1')
					$disabled = 'disabled-link';
				else
					$disabled = '';
			}else{
				$disabled = 'disabled-link';
			}

			if($row[$i]['total_scanned'] > 0){
				$disabled2 = 'disabled-link';
			}else{
				if($row[$i]['status'] == '1')
					$disabled2 = 'disabled-link';
				else
					$disabled2 = '';
			}

			if($row[$i]['status'] == '1'){
				//$disabled3 = 'disabled-link';
			}else{
				$disabled3 = '';
			}

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$disabled3.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-print"></i> Print CC Document</a>';
				$action .= '</li>';
                $action .= '<li>';
				$action .= '<a class="data-table-print2 '.$disabled3.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-print"></i> Print CC Document 2</a>';
				$action .= '</li>';
            }

			if($row[$i]['status'] == '1')
				$dis = 'disabled-link';
			else{
				if($row[$i]['total_scanned'] > 0){

					if(!empty($row[$i]['cc_finish']) && $row[$i]['cc_finish'] != '-'){
						$dis = 'disabled-link';
					}else{
						$dis = '';
					}
				}else{
					$dis = 'disabled-link';
				}

				/*
				if($row[$i]['cc_start'] == '-')
					$dis = 'disabled-link';
				else
					$dis = '';
				*/
			}

			$action .= '<li>';
			$action .= '<a class="data-table-finish '.$dis.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Finish Count</a>';
			$action .= '</li>';

			$action .= '<li>';
			$action .= '<a class="data-table-check '.$disabled.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Check Result</a>';
			$action .= '</li>';

			/*
			$action .= '<li>';
			$action .= '<a class="data-table-close '.$disabled.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Close CC</a>';
			$action .= '</li>';
			*/

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$disabled2.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$disabled2.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();

			$s = $row[$i]['cc_start'];
			$f = $row[$i]['cc_finish'];

			if($s != '-'){
				$s = explode('<br>', $s);
				$s1 = '<br><span class="small-date">'.$s[1].'</span>';

				$s = $s[0] . $s1;
			}

			if($f != '-'){
				$f = explode('<br>', $f);
				$f1 = '<br><span class="small-date">'.$f[1].'</span>';

				$f = $f[0] . $f1;
			}

			$nested[] = $row[$i]['cc_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'stock_opname/detail/'.$row[$i]['cc_id'].'">' . $row[$i]['cc_code'] . '</a>';
			$nested[] = $row[$i]['cc_time'];
			$nested[] = $row[$i]['cc_type'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $s;
			$nested[] = $f;
			$nested[] = $row[$i]['nama_status'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    private function data($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $cc_code= $this->input->post("cc_code");
        $tanggal_awal = $this->input->post('tanggal_awal_pl');
        $tanggal_akhir = $this->input->post('tanggal_akhir_pl');

        if(!empty($cc_code)){
            $condition["cc.cc_code like '%$cc_code%'"]=null;
        }
        if(!empty($nama_customer)){
            $condition["a.nama_customer like '%$nama_customer%'"]=null;
        }

        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["cc.cc_time >= '$tanggal_awal'"] = null ;
            $condition["cc.cc_time <= '$tanggal_akhir'"] = null;
        }


        $this->db->from('cycle_count_h1 cc');
        $this->db->order_by('cc.cc_id_h1 DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_list_do($condition = array()) {
        // ===========Filtering=================
        $condition["cc_id_h1"]=null;


        $this->db->from($this->do  . ' a');
        $this->db->order_by('a.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $cc_code= $this->input->post("cc_code");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $cc_id_h1 = $this->input->post('cc_id_h1');
        if(!empty($cc_code)){
            $condition["a.cc_code like '%$cc_code%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($cc_id_h1)){
            $condition["h2.cc_id_h1"]=$cc_id_h1;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

        $this->db->select('*');
        $this->db->from('cycle_count_h2 h2');
        $this->db->join('cycle_count_h1 h1','h1.cc_id_h1 = h2.cc_id_h1','left');
        $this->db->join('m_loc l','l.loc_id = h2.loc_id','left');
        $this->db->join('barang b','b.id_barang = h2.item_id','left');
        $this->db->order_by('cc_id_h2');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_hasil($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $cc_code= $this->input->post("cc_code");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $cc_id_h1 = $this->input->post('cc_id_h1');
        if(!empty($cc_code)){
            $condition["a.cc_code like '%$cc_code%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($cc_id_h1)){
            $condition["h2.cc_id_h1"]=$cc_id_h1;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

        $this->db->from("cycle_count_h2 h2");
        $this->db->join('cycle_count_detail d','d.cc_id_h2 = h2.cc_id_h2','left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_databarcode_by_id($cc_id_h1) {
        $condition['h1.cc_id_h1'] = $cc_id_h1;
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");

        $this->db->select('cc_code, loc_name,d.id_barang, count(d.id_barang) as qty,cc_time, kd_barang, nama_barang, nama_satuan, cc_type');
        $this->db->from('cycle_count_h1 h1');
        $this->db->join('cycle_count_h2 h2','h1.cc_id_h1 = h2.cc_id_h1','left');
        $this->db->join('cycle_count_detail d','d.cc_id_h2 = h2.cc_id_h2','left');
        $this->db->join('barang b','b.id_barang = d.id_barang','left');
        $this->db->join('satuan sat','sat.id_satuan = b.id_satuan','left');
        $this->db->join('m_loc l','l.loc_id = d.loc_id','left');
        $this->db->group_by('d.loc_id');
        $this->db->group_by('d.id_barang');
        $this->db->where_condition($condition);

        return $this->db->get();
    }

    public function get_by_id($id) {
        $condition['cc.cc_id_h1'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['h2.cc_id_h2'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
    //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["cc.cc_time >= '$tahun_aktif_awal'"] = null ;
        $condition["cc.cc_time <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->cc_id_h1;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">';

            $action .= '<li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>View Stock Opname', array('id' => 'drildown_key_p_Pl_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_cycle_count', 'data-source' => base_url('stock_opname/get_detail_hasil_cycle_count/' . $id))) . ' ';
            $action .= '</li>';


            if($value->status == 0){
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print CC Doc', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('stock_opname/print_doc/' . $id)));
                $action .= '</li>';
                if ($this->access_right->otoritas('edit')) {
                    $action .= '<li>';
                    $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('stock_opname/edit/'. $id))) . ' ';
                    $action .= '</li>';
                }

                if ($this->access_right->otoritas('delete')) {
                    $action .= '<li>';
                    $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('stock_opname/delete/'. $id)));
                    $action .= '</li>';
                }
            }
            $action .= '</ul></div>';

            $status = $value->status == 0 ? "Open" : "Close";
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->cc_id_h1,
                    'cc_time' => hgenerator::switch_tanggal($value->cc_time),
                    'cc_code' => anchor(null, $value->cc_code, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_cycle_count', 'data-source' => base_url('stock_opname/get_detail_cycle_count/' . $id))) ,
                    'cc_type' => $value->cc_type,
                    'remark' => $value->remark,
                    'status' => $status,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->cc_id_h1,
                    'cc_time' => hgenerator::switch_tanggal($value->cc_time),
                    'cc_code' => $value->cc_code,
                    'cc_type' => $value->cc_type,
                    'remark' => $value->remark,
                    'status' => $status,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail() {
        $cc_id_h1 = $this->input->post('cc_id_h1');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->cc_id_h2;
            $action = '';
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('stock_opname/edit_detail/' .$cc_id_h1.'/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('stock_opname/delete_detail/' .$cc_id_h1.'/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            if (!empty($value->loc_name)) {
                $rows[] = array(
                    'primary_key' => $value->cc_id_h2,
                    'loc_name' => $value->loc_name,
                    'aksi' => $action

                );
            }else{
                $rows[] = array(
                    'primary_key' => $value->cc_id_h2,
                    'kd_barang' => $value->kd_barang,
                    'item_name' => $value->nama_barang,
                    'aksi' => $action

                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function init_clean($cc_id_h1){
        $this->db->from('cycle_count_h2 h2');
        $this->db->join('cycle_count_h1 h1','h1.cc_id_h1 = h2.cc_id_h1','left');
        $this->db->where('h2.cc_id_h1',$cc_id_h1);
        $hasil = $this->db->get();
        foreach ($hasil->result() as $row) {
            if($row->status==0){
                $this->db->where('cc_id_h2',$row->cc_id_h2);
                $this->db->delete('cycle_count_detail');
                # code...
                $st_clean = TRUE;
            }else{
                $st_clean = FALSE;
            }
        }

        return $st_clean;
    }

    public function sync_cc_detail($cc_id_h1){
        $st_clean = $this->init_clean($cc_id_h1);
        if($st_clean){
            $this->db->select('h2.cc_id_h2,rb.kd_unik as sn_by_item,rb2.kd_unik as sn_by_loc,rb.id_barang as id_barang_sn, rb2.id_barang as id_barang_loc, rb.loc_id as loc_id_sn , rb2.loc_id as loc_id_loc');
            $this->db->from('cycle_count_h1 h1');
            $this->db->join('cycle_count_h2 h2','h1.cc_id_h1 = h2.cc_id_h1','left');
            $this->db->join('receiving_barang rb','rb.id_barang = h2.item_id and rb.shipping_id IS NULL','left');
            $this->db->join('receiving_barang rb2','rb2.loc_id = h2.loc_id and rb2.shipping_id IS NULL','left');
            $this->db->where('h1.cc_id_h1',$cc_id_h1);
            $hasil = $this->db->get();
            foreach ($hasil->result() as $row) {
                if(!empty($row->sn_by_item)){
                    $data = array('cc_id_h2'=>$row->cc_id_h2,'kd_unik'=>$row->sn_by_item,'id_barang' => $row->id_barang_sn,'loc_id' => $row->loc_id_sn);
                    //$this->db->insert('cycle_count_detail',$data);
                }else{
                    $data = array('cc_id_h2'=>$row->cc_id_h2,'kd_unik'=>$row->sn_by_loc,'id_barang' => $row->id_barang_loc,'loc_id' => $row->loc_id_loc);
                    //$this->db->insert('cycle_count_detail',$data);
                }
                # code...
            }
        }

    }

    public function data_table_detail_hasil() {
        $cc_id_h1 = $this->input->post('cc_id_h1');
        $this->sync_cc_detail($cc_id_h1);

        // Total Record
        $total = $this->data_detail_hasil()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_hasil = $this->data_detail_hasil()->get();
        $rows = array();

        foreach ($data_detail_hasil->result() as $value) {

            $status = $value->cc_status == 0 ? 'Uncheked':'Cheked';
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_unik'=>$value->kd_unik,
                    'cc_status' => $status,
                );
            }else{
                $rows[] = array(
                    'kd_unik'=>$value->kd_unik,
                    'cc_status' => $status,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.cc_time >= '$tahun_aktif_awal'"] = null ;
        $condition["a.cc_time <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->cc_id_h1;
            $action = '';
                $rows[] = array(
                    'cc_time' => hgenerator::switch_tanggal($value->cc_time),
                    'cc_code' => $value->cc_code,
                    'proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                );
        }
        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->cc_id_h1_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create1($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update1($data, $id) {
        return $this->db->update($this->table, $data, array('cc_id_h1' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert('cycle_count_h2', $data);
    }

    public function update_detail($data, $id) {
        $this->db->from('do');
        $this->db->join('do_barang db','do.id_do = db.id_do','left');
        $this->db->where('do.id_do',$id);
        $hasil = $this->db->get();

        foreach ($hasil->result() as $row) {

            if($row->jenis_do == 'FIFO' || $row->jenis_do==''){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_in','ASC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'FEFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_exp','ASC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'LIFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_in','DESC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'LEFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_exp','DESC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }
            # code...
        }
        return $this->db->update($this->do, $data, array('id_do' => $id));
    }

    public function delete1($id) {
        $this->db->delete($this->table4, array('cc_id_h1' => $id));
        return $this->db->delete($this->table, array('cc_id_h1' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id_do' => $id));
    }


    public function options($default = '--Pilih Kode bbk--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->cc_id_h1] = $row->cc_code ;
        }
        return $options;
    }

    public function options_do($default = '--Choose DO--', $key = '') {
        $data = $this->data_list_do()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_do] = $row->kd_do ;
        }
        return $options;
    }

}

?>

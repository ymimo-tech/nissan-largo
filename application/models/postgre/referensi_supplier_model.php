<?php
class referensi_supplier_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'suppliers';

    public function check_supplier($kd_supplier=''){
        $result = $this->db->get_where($this->table,array('code'=>$kd_supplier));
        return $result;
    }

    public function update_supplier($data=array(),$id=0){
        $result = $this->db->update($this->table,$data,array('id'=>$id));
        return $result;
    }

    public function data($condition = array()) {
        $this->db->select('a.id as id_supplier, a.name as nama_supplier, a.code as kd_supplier, a.address as alamat_supplier, a.phone as telepon_supplier, a.contact_person as cp_supplier, a.email as email_supplier');
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $this->db->where('a.id',$id);
        $this->data();
        return $this->db->get();
    }

    private function data_insert($data){

        $data = array(
            'code'          => $data['kd_supplier'],
            'name'          => $data['nama_supplier'],
            'address'       => $data['alamat_supplier'],
            'phone'         => $data['telepon_supplier'],
            'contact_person'=> $data['cp_supplier'],
            'email'         => $data['email_supplier']
        );

        return $data;
    }


    public function get_data($condition = array()) {
        if(!empty($condition['id_supplier'])){
            $condition['id'] = $condition['id_supplier'];
            unset($condition['id_supplier']);
        }
        $this->data($condition);
        return $this->db->get();
    }

	public function getSupplier($post = array()){
		$result = array();

        $sql = "SELECT code as kd_supplier, name as nama_supplier, address as alamat_supplier, phone as telepon_supplier, contact_person as cp_supplier, email as email_supplier FROM ".$this->table." ORDER BY (CASE WHEN split_part(code, '-',1)~E'^\\\d+$' THEN CAST (split_part(code, '-',1) AS INTEGER) ELSE 0 END)";
		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    public function create($data) {
        $data = $this->data_insert($data);
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        $data = $this->data_insert($data);
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function getSupplierWarehouse($supplierId){
        $this->db
            ->select('warehouse_id')
            ->from('supplier_warehouses')
            ->where('supplier_id', $supplierId);

        return $this->db->get()->result_array();
    }

    public function supplWarehouse($id,$warehouses){

        $this->db->trans_start();

        $this->db->where('supplier_id',$id);
        $this->db->delete('supplier_warehouses');

        if(!empty($warehouses)){

            $data2 = array();

            if(in_array('all',$warehouses)){

                $this->db->select('id');
                $warehouses = $this->db->get('warehouses')->result_array();

                foreach ($warehouses as $wh) {

                    $data2[] = [
                        'supplier_id' => $id,
                        'warehouse_id' => $wh['id']
                    ];

                }

            }else{

                foreach ($warehouses as $wh) {

                    $data2[] = [
                        'supplier_id' => $id,
                        'warehouse_id' => $wh
                    ];

                }

            }

            $this->db->insert_batch('supplier_warehouses',$data2);

        }

        $this->db->trans_complete();

    }

    public function delete($id) {

		$result = array();

        $this->db
            ->select('count(*)')
            ->from('inbound')
            ->where('supplier_id',$id)
            ->where('document_id <>', 2);

        $t1 = $this->db->get_compiled_select();

        $this->db
            ->select('count(*)')
            ->from('outbound')
            ->where('customer_id',$id)
            ->where('document_id <>', 2);

        $t2 = $this->db->get_compiled_select();

        $this->db->select("($t1) as t1");
        $this->db->select("($t2) as t2");
		$row = $this->db->get()->row_array();
		if(($row['t1'] + $row['t2']) > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('id' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}

        return $result;
    }

	public function options2($default = '--Choose Supplier--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_supplier] = $row->nama_supplier.' - '.$row->nama_supplier;
        }
        return $options;
    }

    public function options($default = '--Pilih Kode Supplier--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_supplier] = $row->nama_supplier ;
        }
        return $options;
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);


        for ($i=0; $i < $dataLen ; $i++) {

            $insert= array(
                'code'          => $data[$i]['supplier_code'],
                'name'          => $data[$i]['supplier_name'],
                'address'       => $data[$i]['supplier_addr'],
                'phone'         => $data[$i]['supplier_phone'],
                'email'         => $data[$i]['supplier_email'],
                'contact_person'=> $data[$i]['supplier_contact_person']
            );

            $this->db
                ->from($this->table)
                ->where('LOWER(code)', strtolower($data[$i]['supplier_code']))
                ->or_where('lower(name)', strtolower($data[$i]['supplier_name']));

            $check = $this->db->get()->row_array();

            if($check){

                $id = $check['id'];
                $this->db->where('id',$id);
                $this->db->update($this->table,$insert);

            }else{

                $this->db->insert($this->table,$insert);
                $id = $this->db->insert_id();

            }

            if(!empty($data[$i]['warehouse'])){

                if(strtolower($data[$i]['warehouse']) == "all"){
    
                    $sql = "INSERT INTO supplier_warehouses(supplier_id,warehouse_id) SELECT spl.id as supplier_id, wh.id as warehouse_id FROM suppliers spl CROSS JOIN warehouses wh where spl.id=$id";
                    $this->db->query($sql);
    
                }else{

                    $warehouses = str_replace(", ", ",", $data[$i]['warehouse']);
                    $warehouses = str_replace(" ,", ",", $warehouses);

                    $warehouse = explode(",", $warehouses);

                    $postWarehouse= array();
                    foreach ($warehouse as $wh) {

                        $getWarehouse = $this->db->get_where('warehouses',['lower(name)'=>strtolower($wh)])->row_array();
                        if(!$getWarehouse){

                            return array(
                                'status'    => false,
                                'message'   => "Warehouse '".$wh."' doesn't not exists"
                            );

                        }else{

                            $postWarehouse[] = array(
                                'supplier_id' => $id,
                                'warehouse_id' => $getWarehouse['id']
                            );
                        }

                    }

                    $this->db->insert_batch('suppliers_warehouses',$postWarehouse);

                }

            }else{

                return array(
                    'status'    => false,
                    'message'   => "Warehouse column must be filled!"
                );

            }

        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );
    }

    // START Staging Supplier

    public function get_new_supplier(){
        $result = $this->fina->get_where('m_t_supplier',array('SyncStatus'=>'N'));
        return $result;
    }

    public function create_supplier($data=array()){
        $result = $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

    public function set_supplier_status($SupplierCode=0,$SyncDate=''){
        $result = $this->fina->update('m_t_supplier',array('SyncStatus'=>'Y','SyncDate'=>$SyncDate),array('SupplierCode'=>$SupplierCode));
        return $result;
    }

    // END Staging Supplier

}

?>

<?php
class referensi_barang_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'items';

    public function data($condition = array()) {

        if(!empty($condition)){
            if(!empty($condition['a.id_barang'])){
                $condition['a.id'] = $condition['a.id_barang'];
                unset($condition['a.id_barang']);
            }

            if(!empty($condition['a.id_kategori'])){
                $condition['a.category_id'] = $condition['a.id_kategori'];
                unset($condition['a.id_kategori']);
            }

            if(!empty($condition['a.shipment_type'])){
                $condition['e.code'] = $condition['a.shipment_type'];
                unset($condition['a.shipment_type']);
            }
        }

        $this->db->select('a.id as id_barang,a.quarantineduration, a.quarantine, a.autorelease, a.parent_child, a.id_item_subtitute, a.sku, a.name as nama_barang, a.code kd_barang, a.has_batch, b.id as id_kategori,b.code as kd_kategori, b.name as nama_kategori, c.id as id_satuan, c.code as kd_satuan, c.name as nama_satuan, a.def_qty, a.has_qty, a.minimum_stock, e.id as shipment_type_code, e.type as shipment_type, a.fifo_period, a.min_reorder, a.has_expdate, a.preorder, a.weight, a.length, a.width, a.height, a.convert_qty_1, a.convert_qty_2, a.convert_qty_unit_id_1, a.convert_qty_unit_id_2, a.status, a.item_area as item_area_f, l.name as item_area');

        // $this->db->select(["(CASE WHEN a.is_active = 1 THEN 'Active' ELSE 'Inactive' END) as status"]);

        $this->db->from($this->table  . ' a');
        $this->db->join('categories b', 'a.category_id= b.id', 'left');
        $this->db->join('units c', 'a.unit_id = c.id', 'left');
		$this->db->join('shipment e', 'a.shipment_id = e.id', 'left');
		$this->db->join('fifo_periode f', 'a.fifo_periode_id = f.id', 'left');
        $this->db->join('locations l','l.id = a.def_loc_id','left');
        $this->db->where_condition($condition);
        $this->db->order_by('a.id');
        return $this->db;
    }

    public function get_by_id($id) {
        $this->db->where('a.id', $id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id_barang'])){
            $condition['a.id'] = $condition['id_barang'];
            unset($condition['id_barang']);
        }

        $this->data($condition);
        $this->db->select('a.fifo_periode_id as fifo_period, a.fifo_period as fifo_periode_code');
       return $this->db->get();

    }

    public function data_insert($data){

        $data = array(
            'sku'          => $data['sku'],
            'code'          => $data['kd_barang'],
            'name'          => $data['nama_barang'],
            'category_id'   => $data['id_kategori'],
            'unit_id'       => $data['id_satuan'],
            'shipment_id'   => $data['shipment_type'],
            'fifo_periode_id'   => $data['fifo_period'],
            'def_qty'       => $data['def_qty'],
            'child_satuan'  => $data['child_satuan'],
            'minimum_stock' => $data['minimum_stock'],
            'min_reorder'   => $data['min_reorder'],
            'has_qty'       => $data['has_qty'],
            'has_batch'     => $data['has_batch'],
            'has_expdate'   => $data['has_expdate'],
            'preorder'      => $data['preorder'],
            'height'        => $data['height'],
            'width'         => $data['width'],
            'length'        => $data['length'],
            'weight'        => $data['weight'],
            'quarantineduration' => $data['quarantineduration'],
            'autorelease'        => $data['autorelease'],
            'quarantine'     => $data['quarantine'],
            'convert_qty_1'   => $data['convert_2'],
            'convert_qty_unit_id_1'   => $data['unit_id_2'],
            'convert_qty_2'   => $data['convert_3'],
            'convert_qty_unit_id_2'   => $data['unit_id_3'],
            'parent_child'  => $data['parent_child'],
            'id_item_subtitute' => $data['item_subtitute'],
            'item_area' => $data['item_area'],
            'def_loc_id' => $data['def_loc_id']
        );

        return $data;

    }

    public function create($data) {
        $data = $this->data_insert($data);
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        $data = $this->data_insert($data);
        $data['updated_at'] = date('Y-m-d h:i:s');
        return $this->db->update($this->table, $data, array('id' => $id));
    }

	public function getPickingStrategy(){
		$result = array();

        $this->db->select('id as shipment_type_code, type as shipment_type');
		$result = $this->db->get('shipment')->result_array();

		return $result;
	}

	public function getPickingPeriod(){
		$result = array();

        $this->db->select('code as periode_code, periode');
        $result = $this->db->get('fifo_periode')->result_array();

		return $result;
	}

    public function delete($id) {
		$result = array();

        $this->db
            ->select('count(*) as t1')
            ->from('item_receiving')
            ->where('item_id', $id);

        $t1 = $this->db->get_compiled_select();

        $this->db
            ->select('count(*) as t1')
            ->from('item_kitting ik')
            ->join('kittings k','k.id=ik.kitting_id','left')
            ->where('ik.item_id', $id)
            ->or_where('k.item_id', $id);

        $t2 = $this->db->get_compiled_select();

        $this->db->select("($t1) as t1, ($t2) as t2");
		$row = $this->db->get()->row_array();
		if(($row['t1'] + $row['t2']) >  0){

            $this->db->set('is_active',0);
            $this->db->where('id',$id);
            $this->db->update('items');

            $result['status'] = 'warning';
            $result['message'] = 'Cannot delete data because this data have relation to another table, we will deactive this item !';

		}else{
			$this->db->delete($this->table, array('id' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}


        return $result;
    }

    public function options($default = '--Pilih Kode Barang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->kd_barang.' - '.$row->nama_barang;
        }
        return $options;
    }

    function shipmentTypeOptions() {
        $data = $this->db->get('shipment')->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt['id']] = $dt['code'];
        }
        return $options;
    }

    function shipmentPeriodOptions() {
        $data = $this->db->get('fifo_periode')->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt['id']] = $dt['code'];
        }
        return $options;
    }

    public function is_unique($itemCode){

        $this->db->where('a.code',$itemCode);

        $dataLength = $this->data()->get()->num_rows();;
        if($dataLength){
            return 0;
        }else{
            return 1;
        }
    }

    // Staging Start

    public function get_new_barang(){
        $result = $this->fina->get_where('m_t_item',array('SyncStatus'=>'N'));
        return $result;
    }

    public function set_barang_status($SKU=0,$SyncDate=''){
        $result = $this->fina->update('m_t_item',array('SyncStatus'=>'Y','SyncDate'=>$SyncDate),array('SKU'=>$SKU));
        return $result;
    }

    // Staging End

    public function get_by_code($code){
        $this->db->where('a.code', $code);
        $this->data();
        return $this->db->get();
    }

    public function addItemWarehouse($itemId,$warehouseId){

        $this->db->trans_start();

        $this->db
            ->where('item_id',$itemId)
            ->delete('items_warehouses');

        if(!empty($warehouseId)){

            $dataInsert = array();

            for ($i=0; $i < count($warehouseId); $i++) {
                $dataInsert[$i]['item_id'] = $itemId;
                $dataInsert[$i]['warehouse_id'] = $warehouseId[$i];
            }

            $this->db->insert_batch('items_warehouses',$dataInsert);
        }

        $this->db->trans_complete();

    }

    public function getItemWarehouse($itemId){
        $this->db
            ->select('warehouse_id')
            ->from('items_warehouses')
            ->where('item_id', $itemId);

        return $this->db->get()->result_array();
    }


    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        $shipments=array();
        $periods = array();
        $uoms = array();
        $categories = array();

        for ($i=0; $i < $dataLen ; $i++) {

            if(array_key_exists($data[$i]['shipment_type'], $shipments)){

                $shipment = $shipments[$data[$i]['shipment_type']];

            }else{

                $this->db
                    ->select('id')
                    ->from('shipment')
                    ->where('lower(code)', strtolower($data[$i]['shipment_type']));
                $shipment = $this->db->get()->row_array();

                if(!$shipment){

                    return array(
                        'status'    => false,
                        'message'   => "Shipment '".$data[$i]['shipment_type']."' doesn't not exists"
                    );

                }

                $shipments[$data[$i]['shipment_type']] = $shipment;

            }

            if(array_key_exists($data[$i]['fifo_period'], $periods)){

                $period = $periods[$data[$i]['fifo_period']];

            }else{

                $this->db
                    ->select('id,code')
                    ->from('fifo_periode')
                    ->where('lower(code)', strtolower($data[$i]['fifo_period']));
                $period = $this->db->get()->row_array();

                if(!$period){

                    return array(
                        'status'    => false,
                        'message'   => "Picking Periode Type '".$data[$i]['fifo_period']."' doesn't not exists"
                    );

                }

                $periods[$data[$i]['fifo_period']] = $period;

            }

            if(array_key_exists($data[$i]['unit'], $uoms)){

                $unit = $uoms[$data[$i]['unit']];

            }else{

                $unit = $this->db->get_where('units',['lower(code)'=>strtolower($data[$i]['unit'])])->row_array();
                if(!$unit){

                    return array(
                        'status'    => false,
                        'message'   => "UOM '".$data[$i]['unit']."' doesn't not exists"
                    );

                }

                $uoms[$data[$i]['unit']] = $unit;

            }

            if(array_key_exists($data[$i]['category'], $categories)){

                $category = $categories[$data[$i]['category']];

            }else{

                $this->db
                    ->select('id')
                    ->from('categories')
                    ->where('lower(code)', strtolower($data[$i]['category']))
                    ->or_where('lower(name)', strtolower($data[$i]['category']));

                $category = $this->db->get()->row_array();

                if(!$category){

                    return array(
                        'status'    => false,
                        'message'   => "Category '".$data[$i]['category']."' doesn't not exists"
                    );

                }

                $categories[$data[$i]['category']] = $category;

            }

            $has_qty = (strtoupper($data[$i]['has_qty']) == "YES") ? 1 : 0;
            $has_expdate = (strtoupper($data[$i]['has_expdate']) == "YES") ? 1 : 0;
            $minimum_stock = (!empty($data[$i]['minimum_stock']) && is_integer($data[$i]['minimum_stock'])) ? $data[$i]['minimum_stock'] : 0;
            $min_reorder = (!empty($data[$i]['min_reorder']) && is_integer($data[$i]['min_reorder'])) ? $data[$i]['min_reorder'] : 0;
            $def_qty = (!empty($data[$i]['def_qty']) && is_integer($data[$i]['def_qty'])) ? $data[$i]['def_qty'] : 0;

            $inserts = array(
                // 'sku'              => $data[$i]['sku'],
                // 'sap_reject_code'  => $data[$i]['sap_reject_code'],
                'name'             		=> $data[$i]['item_name'],
                'code'             		=> $data[$i]['item_code'],
                'brand'             	=> $data[$i]['brand'],
                'oem'             		=> $data[$i]['oem'],
                'packing_description'	=> $data[$i]['packing_description'],
                'def_qty'          		=> $def_qty,
                'minimum_stock'    		=> $minimum_stock,
                'min_reorder'      		=> $min_reorder,
                'has_qty'          		=> $has_qty,
                'has_expdate'      		=> $has_expdate,
                'shipment_id'      		=> $shipment['id'],
                'fifo_period'      		=> $period['code'],
                'fifo_periode_id'  		=> $period['id'],
                'unit_id'          		=> $unit['id'],
                'category_id'      		=> $category['id'],
                'width'            		=> $data[$i]['width'],
                'height'            	=> $data[$i]['height'],
                'length'            	=> $data[$i]['length'],
                'weight'            	=> $data[$i]['weight']
            );

            $this->db
                ->from($this->table)
                ->where('LOWER(code)', strtolower($data[$i]['item_code']));
                //->or_where('lower(name)', strtolower($data[$i]['item_name']));

            $check = $this->db->get()->row_array();

            if($check){

                $id= $check['id'];

                $this->db->where('id',$id);
                $this->db->update($this->table,$inserts);

            }else{
                $this->db->insert($this->table,$inserts);
                $id= $this->db->insert_id();
            }

            if(!empty($data[$i]['warehouse'])){

                if(strtolower($data[$i]['warehouse']) == "all"){

                    $sql = "INSERT INTO items_warehouses(item_id,warehouse_id) SELECT itm.id as item_id, wh.id as warehouse_id FROM items itm CROSS JOIN warehouses wh where itm.id=$id";
                    $this->db->query($sql);

                }else{

                    $warehouses = str_replace(", ", ",", $data[$i]['warehouse']);
                    $warehouses = str_replace(" ,", ",", $warehouses);

                    $warehouse = explode(",", $warehouses);

                    $postWarehouse= array();
                    foreach ($warehouse as $wh) {

                        $getWarehouse = $this->db->get_where('warehouses',['lower(name)'=>strtolower($wh)])->row_array();
                        if(!$getWarehouse){

                            return array(
                                'status'    => false,
                                'message'   => "Warehouse '".$wh."' doesn't not exists"
                            );

                        }else{

                            $postWarehouse[] = array(
                                'item_id' => $id,
                                'warehouse_id' => $getWarehouse['id']
                            );
                        }

                    }

                    $this->db->insert_batch('items_warehouses',$postWarehouse);

                }

            }else{

                return array(
                    'status'    => false,
                    'message'   => "Warehouse column must be filled!"
                );

            }


        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );
    }

    public function addToWarehouse($params=array()){

    	$result = array();

        $data = array();

        foreach ($params['warehouses'] as $wh) {
            foreach ($params['elements'] as $el) {

            	$this->db->or_where('item_id='.$el.'and warehouse_id='.$wh,NULL);

                $data[] = array(
                    "item_id" => $el,
                    "warehouse_id" => $wh
                );

            }
        }

        $this->db->trans_start();

        $this->db->delete('items_warehouses');
        $this->db->insert_batch('items_warehouses',$data);

        $this->db->trans_complete();

        return true;

    }

    public function removeFromWarehouse($params=array()){

        $data = array();

        foreach ($params['warehouses'] as $wh) {
            foreach ($params['elements'] as $el) {

            	$this->db->or_where('item_id='.$el.'and warehouse_id='.$wh,NULL);

                $data[] = array(
                    "item_id" => $el,
                    "warehouse_id" => $wh
                );

            }
        }

        $this->db->trans_start();
        $this->db->delete('items_warehouses');
        $this->db->trans_complete();

        return true;

    }
}

?>

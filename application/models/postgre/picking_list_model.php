<?php
class picking_list_model extends CI_Model {

	CONST MONTHLY = 'MONTHLY';
	CONST YEARLY = 'YEARLY';

	CONST FIFO = 'FIFO';
	CONST FEFO = 'FEFO';
	CONST FIFOFEFO = 'FIFOFEFO';
	CONST FEFOFIFO = 'FEFOFIFO';

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'pickings';
    private $table2 = 'pl_do';
    private $table4 = 'do_barang';
    private $table5 = 'items';
    private $table6 = 'units';
    private $table7 = 'categories';
    private $do = 'do';

	public function printed($param=array()) {
		$this->db->set("printed", 1);
		$this->db->where("pl_id", $param['pl_id']);
		$this->db->update("pickings");
	}

    public function get_pl_code(){
        $this->db->select('pre_code_pl,inc_pl');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_pl.$result->inc_pl;
    }

    public function add_pl_code(){
        $this->db->set('inc_pl',"inc_pl+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

    private function data($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $pl_name= $this->input->post("pl_name");
        $tanggal_awal = $this->input->post('tanggal_awal_pl');
        $tanggal_akhir = $this->input->post('tanggal_akhir_pl');

        if(!empty($pl_name)){
            $condition["a.pl_name like '%$pl_name%'"]=null;
        }
        if(!empty($nama_customer)){
            $condition["a.nama_customer like '%$nama_customer%'"]=null;
        }

        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.pl_date >= '$tanggal_awal'"] = null ;
            $condition["a.pl_date <= '$tanggal_akhir'"] = null;
        }


        $this->db->from($this->table  . ' a');
        $this->db->join('do','do.pl_id = a.pl_id','left');
        $this->db->join('toko t','t.id_toko = do.id_toko','left');
        $this->db->order_by('a.pl_name DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function getStatus(){
		$result = array();

		$this->db
			->select('status_id as id_status_picking, code as kd_status, name as nama_status')
			->from('status')
			->like('type', 'PICKING');

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getScannedList($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_unik',
			2 => 'parent_code',
			3 => 'tgl_exp',
			4 => 'tgl_in',
			5 => 'loc_name',
			6 => 'user_name'
		);

		$where = '';

		$this->db->start_cache();

		$this->db
			->from('picking_recomendation pr')
			->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id','left')
			->join('items itm','itm.id=pr.item_id','left')
			->join('locations loc','loc.id=pr.location_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->join('users usr','usr.id=pr.user_id','left')
			->join('pickings p','p.pl_id=pr.picking_id','left')
			// ->where('pr.item_id = '.$post['id_barang'].' OR pr.item_id in (SELECT item_id_subs FROM item_substitute WHERE item_id = '.$post['id_barang'].')')
			->where('pr.order_item_id = '.$post['id_barang'].'')
			->where('pr.picking_id', $post['pl_id'])
			->group_by('pr.unique_code, ird.tgl_exp, ird.tgl_in, loc.name, pr.pick_time, usr.user_name, unt.name, ird.parent_code, p.status_id');


		$this->db->stop_cache();

		$this->db->select('pr.unique_code');

		$row = $this->db->get();
		$totalData = $row->num_rows();

		$this->db
			->select([
				"pr.unique_code as kd_unik","COALESCE(TO_CHAR(tgl_exp, 'DD/MM/YYYY'), '-') as tgl_exp",
				"COALESCE(TO_CHAR(pr.pick_time, 'DD/MM/YYYY HH24:MI:SS'), '-') as tgl_in","loc.name as loc_name", "COALESCE(user_name, '-') as user_name", "CONCAT(COALESCE(sum(pr.qty),0), ' ', unt.name) as qty","ird.parent_code",
				"COALESCE(SUM(pr.qty),0) as  qty","p.status_id"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$disabled ='';
			if(!in_array($row[$i]['status_id'], array(7,8))){
				$disabled='disabled';
			}

            $action = '<button class="btn blue btn-xs" type="button" id="data-table-cancel" data-id="'.$row[$i]['kd_unik'].'" data-qty="'.$row[$i]['qty'].'" '.$disabled.'>
            			Cancel
                       </button>';

			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['parent_code'];
			$nested[] = $row[$i]['qty'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['user_name'];
			$nested[] = $action;
			$nested[] = $row[$i]['qty'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$this->db
			->select([
				"p.pl_id", "p.name as pl_name", "outb.id as id_outbound", "outb.code as kd_outbound", "TO_CHAR(p.date, 'DD/MM/YYYY') as pl_date",
				"COALESCE(CONCAT(spl.code, ' - ', spl.name), cust.name) AS customer_name",
				'destination_name as "DestinationName"','address_1 as "DestinationAddressL1"','address_2 as "DestinationAddressL2"','address_3 as "DestinationAddressL3"',
				"spl.address as alamat_supplier","spl.phone as telepon_supplier", "spl.contact_person as cp_supplier", "cust.address", "outb.phone",
				"COALESCE(remark, '-') AS remark", "COALESCE(TO_CHAR(p.start_time, 'DD/MM/YYYY HH24:MI'), '-') AS pl_start",
				"COALESCE(TO_CHAR(p.end_time, 'DD/MM/YYYY HH24:MI'), '-') AS pl_finish",
				"COALESCE(TO_CHAR(shipping_start, 'DD/MM/YYYY HH24:MI'), '-') AS shipping_start",
				"COALESCE(TO_CHAR(shipping_finish, 'DD/MM/YYYY HH24:MI'), '-') AS shipping_finish",
				"p.status_id as id_status_picking", "user_name", "outb.document_id as id_outbound_document",
				"st.name AS nama_status"
			])
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('suppliers spl','spl.id=outb.customer_id AND outb.document_id=2','left outer')
			->join('customers cust','cust.id=outb.customer_id AND outb.document_id <>2','left outer')
			->join('status st','st.status_id=p.status_id','left')
			->join('shipping_picking sp','sp.pl_id=p.pl_id','left')
			->join('shippings s', 's.shipping_id=sp.shipping_id','left')
			->join('users usr','usr.id=p.user_id','left')
			->where('p.pl_id', $post['pl_id'])
			->group_by('p.pl_id');

		$result = $this->db->get()->row_array();

		return $result;
	}

	public function getDetail2($post = array()){
		$result = array();

		$this->db
			->select([
				"p.pl_id", "p.name as pl_name", "outb.id as id_outbound", "outb.code as kd_outbound", "TO_CHAR(p.date, 'DD/MM/YYYY') as pl_date",
				"COALESCE(CONCAT(spl.code, ' - ', spl.name), cust.name) AS customer_name",
				'destination_name as "DestinationName"','address_1 as "DestinationAddressL1"','address_2 as "DestinationAddressL2"','address_3 as "DestinationAddressL3"',
				"spl.address as alamat_supplier","spl.phone as telepon_supplier", "spl.contact_person as cp_supplier", "cust.address", "outb.phone",
				"COALESCE(remark, '-') AS remark", "COALESCE(TO_CHAR(p.start_time, 'DD/MM/YYYY HH24:MI'), '-') AS pl_start",
				"COALESCE(TO_CHAR(p.end_time, 'DD/MM/YYYY HH24:MI'), '-') AS pl_finish",
				"COALESCE(TO_CHAR(shipping_start, 'DD/MM/YYYY HH24:MI'), '-') AS shipping_start",
				"COALESCE(TO_CHAR(shipping_finish, 'DD/MM/YYYY HH24:MI'), '-') AS shipping_finish",
				"p.status_id as id_status_picking", "user_name", "outb.document_id as id_outbound_document",
				"st.name AS nama_status","wh.name as warehouse_name","whs.name as warehouse_name_to","outb.destination_name"
			])
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('warehouses whs','whs.id=outb.destination_id AND outb.document_id IN (3,14)','left outer')
			->join('suppliers spl','spl.id=outb.customer_id AND outb.document_id=2','left outer')
			->join('customers cust','cust.id=outb.customer_id AND outb.document_id <>2','left outer')
			->join('status st','st.status_id=p.status_id','left')
			->join('shipping_picking sp','sp.pl_id=p.pl_id','left')
			->join('shippings s', 's.shipping_id=sp.shipping_id','left')
			->join('users usr','usr.id=p.user_id','left')
			->where('p.pl_id', $post['pl_id'])
			->group_by('p.pl_id, outb.id, spl.code, spl.name, cust.name, spl.address, spl.phone, spl.contact_person, cust.address, s.shipping_start, s.shipping_finish, usr.user_name, st.name, p.name, outb.code, p.date, outb.destination_name, outb.address_1, outb.address_2, outb.address_3, outb.phone, p.remark, p.start_time, p.end_time, p.status_id, outb.document_id, st.name,wh.name, whs.name, outb.destination_name');

		$result = $this->db->get();

		return $result;
	}

	public function getWidget(){
		$result = array();
		$today = date('Y-m-d');

		$sql = 'SELECT
					COALESCE(SUM(total_item), 0) AS total_item, COALESCE(SUM(total_qty), 0) AS total_qty,
					COALESCE(SUM(total_picking), 0) AS total_picking, COALESCE(SUM(total_qty_picking), 0) AS total_qty_picking,
					COALESCE(SUM(total_loading), 0) AS total_loading, COALESCE(SUM(total_qty_loading), 0) AS total_qty_loading,
					AVG(extract (epoch from (p.end_time - p.created_at))::integer/60) AS average
				FROM
					pickings p
				LEFT JOIN shipping_picking sp
					ON sp.pl_id=p.pl_id
				LEFT JOIN shippings s
					ON s.shipping_id=sp.shipping_id
				LEFT JOIN
				(
					SELECT
						table_1.picking_id, COUNT(total_item) AS total_item, SUM(total_qty) AS total_qty
					FROM
					(
						SELECT
							pqty.picking_id, COUNT(*) AS total_item, SUM(qty) AS total_qty
						FROM
							picking_qty pqty
						JOIN items itm
							ON itm.id=pqty.item_id
						GROUP BY
							pqty.item_id, pqty.picking_id
					) AS table_1
					GROUP BY
						picking_id
				) AS rec_qty
					ON rec_qty.picking_id=p.pl_id
				LEFT JOIN
				(
					SELECT
						table_2.picking_id, COUNT(total_picking) AS total_picking, SUM(total_qty_picking) AS total_qty_picking
					FROM
					(
						SELECT
							pqty.picking_id, COUNT(*) AS total_picking, SUM(qty) AS total_qty_picking
						FROM
							picking_qty pqty
						JOIN items itm
							ON itm.id=pqty.item_id
						JOIN pickings spl
							ON spl.pl_id=pqty.picking_id
						WHERE
							spl.start_time IS NULL
						AND
							spl.end_time IS NULL
						GROUP BY
							pqty.item_id, pqty.picking_id
					) AS table_2
					GROUP BY
						picking_id
				) AS rec_picking
					ON rec_picking.picking_id=p.pl_id
				LEFT JOIN
				(
					SELECT
						table_3.picking_id, COUNT(total_loading) AS total_loading, SUM(total_qty_loading) AS total_qty_loading
					FROM
					(
						SELECT
							pqty.picking_id, COUNT(*) AS total_loading, SUM(qty) AS total_qty_loading
						FROM
							picking_qty pqty
						JOIN items itm
							ON itm.id=pqty.item_id
						JOIN pickings spl
							ON spl.pl_id=pqty.picking_id
						WHERE
							spl.start_time IS NOT NULL
						AND
							spl.end_time IS NOT NULL
						GROUP BY
							pqty.item_id, pqty.picking_id
					) AS table_3
					GROUP BY
						picking_id
				) AS rec_loading
					ON rec_loading.picking_id=p.pl_id
				WHERE
					DATE(p.date) = \''.$today.'\'';

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

	public function insertRelationTable2($post = array()){
		$result = array();

		$this->db
			->set('pl_id', $post['pl_id'])
			->set('id_outbound', $post['id_outbound']);

		$q = $this->db->insert('picking_list_outbound');

		return $result;
	}

	public function insertRelationTable($post = array()){
		$result = array();

		$sql = 'DELETE FROM picking_list_outbound WHERE pl_id=\''.$post['pl_id'].'\'';
		$this->db->query($sql);

		$sql = 'INSERT INTO picking_list_outbound(
					pl_id,
					id_outbound
				)VALUES(
					\''.$post['pl_id'].'\',
					\''.$post['id_outbound'].'\'
				)';


		$q = $this->db->query($sql);

		return $result;
	}

	public function closePicking($post = array()){
		$result = array();

		$sql = 'SELECT
					id_status_picking
				FROM
					picking_list
				WHERE
					pl_id=\''.$post['pl_id'].'\'';

		$row = $this->db->query($sql)->row_array();
		if($row['id_status_picking'] > 2){

			$result['status'] = 'ERR';
			$result['message'] = 'Picking already finish.';

		}else{
			$rectime = date('Y-m-d H:i:s');

			$sql = 'UPDATE
						picking_list
					SET
						pl_status=\'1\'
						id_status_picking=\'3\',
						pl_finish=\''.$rectime.'\'
					WHERE
						pl_id=\''.$post['pl_id'].'\'';

			$q = $this->db->query($sql);

			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Finish picking success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Finish picking failed';
			}

		}

		return $result;
	}

	public function getPlCode($post = array()){
		$result = array();

		$this->db
			->select('pl_id, name as pl_name')
			->from('pickings')
			->like('name', $post['query']);

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getDnList($post = array()){
		$result = array();

		$this->db
			->select('id pl_id, name as pl_name')
			->from('delivery_notes')
			->like('name', $post['query']);

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

    private function data_list_do($condition = array()) {
        // ===========Filtering=================
        $condition["pl_id"]=null;


        $this->db->from($this->do  . ' a');
        $this->db->order_by('a.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function updateOutboundStatus($id){
		$result = array();

		$this->db
			->set('status_id', 5)
			->where('id', $id)
			->update('outbound');

		return $result;
	}

	public function getEdit($post = array()){
		$result = array();

		$this->db
			->select([
				"p.pl_id", "p.name as pl_name", "outb.id as id_outbound", "outb.code as kd_outbound", "TO_CHAR(p.date, 'DD/MM/YYYY') as pl_date", "prior.m_priority_name as priority",
				// "wht.name as warehouse_to",
				"whf.name as warehouse_from",
/*				"
					(
						CASE
							WHEN spl.name IS NOT NULL THEN spl.name
							WHEN cust.name IS NOT NULL THEN cust.name
							WHEN wht.name  IS NOT NULL THEN wht.name
							WHEN dest.destination_name IS NOT NULL THEN dest.destination_name
						END
					) as destination_name
				"
*/
				"COALESCE(d.destination_name, s.source_name) as destination_name",
				"COALESCE(d.destination_address, s.source_address) as destination_address",
				"outb.note","outb.ekspedisi"
			])
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('warehouses whf','whf.id=outb.warehouse_id')
			->join('documents doc','doc.document_id=outb.document_id')
 		// 	->join('suppliers spl','spl.id=outb.destination_id and doc.m_source_type_id=1','left outer')
 		// 	->join('customers cust','cust.id=outb.destination_id and doc.m_source_type_id=2','left outer')
			// ->join('warehouses wht','wht.id=outb.destination_id and doc.m_source_type_id=3','left outer')
 		// 	->join('destination dest','dest.destination_id=outb.destination_id and doc.m_source_type_id=4','left outer')
			->join('destinations d','d.destination_id=outb.destination_id','left')
			->join('sources s','s.source_id=outb.destination_id','left')
			->join('m_priority prior','prior.m_priority_id=outb.m_priority_id','left')
			->where('p.pl_id',$post['pl_id']);

		$result = $this->db->get()->row_array();

		return $result;
	}

	public function delete_pl_qty($id,$id_barang,$qty,$batch){
		$this->db->trans_start();
		$id_pq = $this->db->select('id')->from('picking_qty')->where('picking_id',$id)->where('item_id',$id_barang)->get()->row_array();
		if(isset($id_pq['id'])){
			$this->db->where('picking_qty_id',$id_pq['id']);
			$this->db->delete('picking_qty_substitute');
		}
		$this->db->where('picking_id',$id);
		$this->db->where('item_id',$id_barang);
		$this->db->delete('picking_qty');
		$this->db->trans_complete();
	}

	public function update_pl_qty($id,$id_barang,$qty,$batch){
		$this->db->trans_start();
        $this->db->where('picking_id',$id);
		$this->db->where('item_id',$id_barang);
		$this->db->set('qty',$qty);
		$this->db->update('picking_qty');
		$this->db->trans_complete();
        //$condition['item_id'] = $id_barang;
		
		

		/*
        $this->db->from('picking_qty pq');
        $this->db->where($condition);
        $cek_eksis = $this->db->get();

		
        if($cek_eksis->num_rows() > 0){

            // $update = array(
				// 'qty' 		=> $qty,
				// 'batch'		=> $batch
			// );
            // $this->db->where($condition);
            // $this->db->update('picking_qty', $update);

        }else{

            $insert = array(
				'picking_id' 	=> $id,
                'item_id' => $id_barang,
                'qty' 		=> $qty,
				'batch'		=> $batch
			);

            $this->db->insert('picking_qty', $insert);

        }
		*/
    }

	public function update_pl_qty_old($id,$id_barang,$qty,$batch){
        $condition['picking_id'] = $id;
        $condition['item_id'] = $id_barang;

        $this->db->from('picking_qty pq');
        $this->db->where($condition);
        $cek_eksis = $this->db->get();

        if($cek_eksis->num_rows() > 0){

            // $update = array(
				// 'qty' 		=> $qty,
				// 'batch'		=> $batch
			// );
            // $this->db->where($condition);
            // $this->db->update('picking_qty', $update);

        }else{

            $insert = array(
				'picking_id' 	=> $id,
                'item_id' => $id_barang,
                'qty' 		=> $qty,
				'batch'		=> $batch
			);

            $this->db->insert('picking_qty', $insert);

        }
    }

    public function update_pl_qty2($pickingId='',$data=array()){

    	if($data){

    		$insData = array();
    		$updData  = array();

            foreach($data as $kk => $dd){

	        	$check_exist = $this->db->get_where('picking_qty',['item_id'=>$dd['id_barang'],'picking_id'=>$pickingId])->row_array();

	        	if(!$check_exist){

                    if(array_key_exists($kk,$insData)){

                        $insData[$kk]['qty'] + $dd['qty'];

		        	}else{

                        $insData[$kk] = array(
                            'picking_id'=> $pickingId,
                            'item_id'    => $dd['id_barang'],
                            'qty'        => $dd['qty'],
                            'batch'        => '',
                            'unit_id'    => $dd['unit_id']
                        );

			        }

			    }else{

                    if(array_key_exists($kk,$updData)){

                        $updData[$kk]['qty'] + $dd['qty'];

		        	}else{

                        $updData[$kk] = array(
                            'picking_id'=> $pickingId,
                            'item_id'    => $dd['id_barang'],
                            'qty'        => $dd['qty'],
                            'batch'        => '',
                            'unit_id'    => $dd['unit_id']
                        );
			        }

			    }

	        }

	        if ($updData) {
	        	foreach ($updData as $upd) {
	        		$this->db->update('picking_qty',$upd);
	        	}
	        }

	        $this->db->insert_batch('picking_qty',$insData);

	    }

    }

	public function getItems($post = array()){
		$result = array();

		$this->db
			->select([
				"p.pl_id",
				"itm.id as id_barang",
				"loc.name as locations",
				"itm.code as kd_barang",
				"itm.brand as item_brand",
				"CONCAT('<b>', itm.name, '</b>') as nama_barang",
				"CONCAT('<b>', itm.code, '</b><br/>', itm.oem) as item_code",
				"COALESCE(unt.name, '') as nama_satuan",
				"COALESCE(SUM(pq.qty),0) as qty",
				"outb.warehouse_id as warehouse_doc"
			])
			->from('pickings p')
			->join('picking_qty pq','pq.picking_id=p.pl_id')
			->join('items itm','itm.id=pq.item_id')
			->join('locations loc','itm.def_loc_id = loc.id','left')
			->join('units unt','unt.id=itm.unit_id')
			->join("(SELECT * FROM picking_list_outbound WHERE pl_id=".$post['pl_id']." LIMIT 1) as plo",'p.pl_id=plo.pl_id','left')
			->join('outbound outb','outb.id=plo.id_outbound','left')
			->group_by('p.pl_id, itm.id, itm.code, itm.name, pq.batch, unt.name, outb.warehouse_id, loc.name')
			->order_by('loc.name','ASC')
			->where('p.pl_id',$post['pl_id']);

		$row = $this->db->get()->result_array();
		// dd($row);
		$len = count($row);

		// for($i = 0; $i < $len; $i++){

		// 	$locations = array();

		// 	$this->db
		// 		->select('loc.name as loc_name')
		// 		->from('item_receiving_details ird')
		// 		->join('locations loc','loc.id=ird.location_id','left')
		// 		->join('location_areas loc_area','loc_area.id=loc.location_area_id','left')
		// 		->where_not_in('ird.location_id',[100,101,102,103,104])
		// 		->where('ird.item_id', $row[$i]['id_barang'])
		// 		// ->where('loc_area.warehouse_id', $row[$i]['warehouse_doc'])
		// 		->group_by('loc.name','ASC');

		// 		// dd($this->db->get_compiled_select());
		// 	$rows = $this->db->get()->result_array();
		// 	$lens = count($rows);

		// 	for($j = 0; $j < $lens; $j++){
		// 		array_push($locations, $rows[$j]['loc_name']);
		// 	}

		// 	$row[$i]['locations'] = implode(',', $locations);
		// }

		$result['data'] = $row;

		return $result;
	}

	public function setRecomendation($post = array()){
		$result = array();

		//EDIT THIS METHOD CAREFULY

		$sql = 'SELECT
					brg.id_barang, CONCAT(nama_barang, \' - \', kd_barang) AS nama_barang, kd_barang, qty,
					has_batch, shipment_type, fifo_period, nama_satuan, batch
				FROM
					barang brg
				JOIN picking_qty pq
					ON pq.id_barang=brg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					pl_id=\''.$post['pl_id'].'\'';

		$row = $this->db->query($sql)->result_array();
		$len = count($row);

		$sql = 'DELETE FROM picking_recomendation WHERE id_picking=\''.$post['pl_id'].'\'';
		$this->db->query($sql);

		for($i = 0; $i < $len; $i++){

			$idBarang = $row[$i]['id_barang'];
			$shipmentType = $row[$i]['shipment_type'];
			$periode = $row[$i]['fifo_period'];
			$hasBatch = $row[$i]['has_batch'];

			if($hasBatch == 1){

				$sql = 'SELECT
							id_barang, kd_unik, rcvbrg.loc_id, loc_name
						FROM
							receiving_barang rcvbrg
						JOIN m_loc loc
							ON loc.loc_id=rcvbrg.loc_id
						WHERE
							id_barang=\''.$idBarang.'\'
						AND
							kd_batch = \''.$row[$i]['batch'].'\'
						AND
							id_qc = \'1\'
						AND
							loc.loc_type NOT IN('.$this->config->item('hardcode_location').')
						ORDER BY
							rcvbrg.tgl_in ASC
						LIMIT ' . $row[$i]['qty'];

				$rows = $this->db->query($sql)->result_array();
				$lens = count($rows);

				for($j = 0; $j < $lens; $j++){

					$sql = 'INSERT INTO picking_recomendation(
								id_picking,
								kd_unik,
								id_barang,
								loc_id
							)VALUES(
								\''.$post['pl_id'].'\',
								\''.$rows[$j]['kd_unik'].'\',
								\''.$rows[$j]['id_barang'].'\',
								\''.$rows[$j]['loc_id'].'\'
							)';

					$this->db->query($sql);

				}

			}else{

				$orderBy = '';
				$groupBy = '';

				if($periode == self::MONTHLY && $shipmentType == self::FIFO)
					$groupBy .= ' GROUP BY YEAR(tgl_in), MONTH(tgl_in), loc_id, kd_unik ';

				if($periode == self::MONTHLY && $shipmentType == self::FEFO)
					$groupBy .= ' GROUP BY YEAR(tgl_exp), MONTH(tgl_exp), loc_id, kd_unik ';

				if($periode == self::YEARLY && $shipmentType == self::FIFO)
					$groupBy .= ' GROUP BY YEAR(tgl_in), loc_id, kd_unik ';

				if($periode == self::YEARLY && $shipmentType == self::FEFO)
					$groupBy .= ' GROUP BY YEAR(tgl_exp), loc_id, kd_unik ';

				if($periode == self::MONTHLY && $shipmentType == self::FIFOFEFO)
					$groupBy .= ' GROUP BY YEAR(tgl_in), MONTH(tgl_in), YEAR(tgl_exp), MONTH(tgl_exp), loc_id, kd_unik ';

				if($periode == self::MONTHLY && $shipmentType == self::FEFOFIFO)
					$groupBy .= ' GROUP BY YEAR(tgl_exp), MONTH(tgl_exp), YEAR(tgl_in), MONTH(tgl_in), loc_id, kd_unik ';

				if($periode == self::YEARLY && $shipmentType == self::FIFOFEFO)
					$groupBy .= ' GROUP BY YEAR(tgl_in), YEAR(tgl_exp), loc_id, kd_unik ';

				if($periode == self::YEARLY && $shipmentType == self::FEFOFIFO)
					$groupBy .= ' GROUP BY YEAR(tgl_exp), YEAR(tgl_in), loc_id, kd_unik ';

				if($periode == self::MONTHLY)
					$groupBy .= '';

				if($shipmentType == self::FIFO)
					$orderBy .= ' ORDER BY tgl_in ASC ';

				if($shipmentType == self::FEFO)
					$orderBy .= ' ORDER BY tgl_exp ASC ';

				if($shipmentType == self::FIFOFEFO)
					$orderBy .= ' ORDER BY tgl_in, tgl_exp ASC ';

				if($shipmentType == self::FEFOFIFO)
					$orderBy .= ' ORDER BY tgl_exp, tgl_in ASC ';

				//TODO id_qc

				$sql = 'SELECT
							id_barang, kd_unik, rcvbrg.loc_id, loc_name, COUNT(*) AS total
						FROM
							receiving_barang rcvbrg
						JOIN m_loc loc
							ON loc.loc_id=rcvbrg.loc_id
						WHERE
							id_barang=\''.$idBarang.'\'
						AND
							id_qc = \'1\'
						AND
							loc.loc_type NOT IN('.$this->config->item('hardcode_location').')'.$groupBy.$orderBy.'  LIMIT ' . $row[$i]['qty'];

				$rows = $this->db->query($sql)->result_array();

				//file_put_contents('debug-query', $this->db->last_query());
				//echo '<pre>', var_dump($this->db->last_query());
				//exit();

				$lens = count($rows);
				//$locations = array();

				$total = 1;
				$brg = 0;

				for($j = 0; $j < $lens; $j++){

					//array_push($locations, $rows[$j]['loc_name']);

					// $sql = 'SELECT
								// COUNT(*) AS total
							// FROM
								// picking_recomendation
							// WHERE
								// kd_unik=\''.$rows[$j]['kd_unik'].'\'
							// AND
								// id_picking=\''.$post['pl_id'].'\'';

					// $q = $this->db->query($sql)->row_array();

					// if($q['total'] > 0){

						// $sql = 'UPDATE
									// picking_recomendation
								// SET
									// kd_unik=\''.$rows[$j]['kd_unik'].'\',
									// id_barang=\''.$rows[$j]['id_barang'].'\',
									// loc_id=\''.$rows[$j]['loc_id'].'\'
								// WHERE
									// id_picking=\''.$post['pl_id'].'\'';

						// $this->db->query($sql);

					// }else{

					$sql = 'INSERT INTO picking_recomendation(
								id_picking,
								kd_unik,
								id_barang,
								loc_id
							)VALUES(
								\''.$post['pl_id'].'\',
								\''.$rows[$j]['kd_unik'].'\',
								\''.$rows[$j]['id_barang'].'\',
								\''.$rows[$j]['loc_id'].'\'
							)';

					$this->db->query($sql);

					// echo 'total<br>';
					// echo $total.'-';
					// echo $row[$i]['qty'].'<br>';

					// echo 'barang<br>';
					// echo $brg.'-';
					// echo $rows[$j]['id_barang'].'<br>';

					// if($total >= $row[$i]['qty'] && $brg == $rows[$j]['id_barang']){
						// //file_put_contents('debug-id', $rows[$j]['loc_id']);
						// continue;
					// }

					// if($brg != $rows[$j]['id_barang'])
						// $total = 0;

					// //$total .= $rows[$j]['total'];
					// $total++;
					// $brg = $rows[$j]['id_barang'];

					//}

				}
			}
			//$row[$i]['locations'] = implode(',', $locations);

		}

		//exit();

		//$result['data'] = $row;

		return $result;
	}

	public function getDetailItemPickedDataTable($post = array()){
		$result = array();

		//TODO id_qc
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_barang',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
			4 => 'jumlah_barang',
			5 => 'available_qty',
			6 => 'previous_pick',
			7 => 'picked_qty',
			8 => 'jumlah_barang - (previous_pick + picked_qty)'
		);

		$where = '';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						outbbrg.id_barang
					FROM
						picking_list pl
					JOIN picking_list_outbound plo
						ON plo.pl_id=pl.pl_id
					JOIN outbound outb
						ON outb.id_outbound=plo.id_outbound
					LEFT JOIN outbound_barang outbbrg
						ON outbbrg.id_outbound=outb.id_outbound
					LEFT JOIN picking_qty plq
						ON plq.id_barang=outbbrg.id_barang
							AND plq.pl_id=pl.pl_id
					LEFT JOIN barang brg
						ON brg.id_barang=outbbrg.id_barang
					LEFT JOIN (
						SELECT
							id_barang, COUNT(*) AS available_qty
						FROM
							receiving_barang r
						JOIN m_loc l
							ON l.loc_id=r.loc_id
						WHERE
							l.loc_type NOT IN('.$this->config->item('hardcode_location').')
						AND
							id_qc = \'1\'
						GROUP BY
							id_barang
					) AS tbl_1
						ON tbl_1.id_barang=outbbrg.id_barang
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						pl.pl_id=\''.$post['pl_id'].'\'
					GROUP BY
						brg.id_barang
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					outbbrg.id_barang, kd_barang, nama_barang, jumlah_barang,
					IFNULL(tbl_1.available_qty, 0) AS available_qty,
					IFNULL((
						SELECT
							SUM(qty)
						FROM
							picking_qty plqs
						JOIN picking_list pls
							ON pls.pl_id=plqs.pl_id
						JOIN picking_list_outbound plos
							ON plos.pl_id=pls.pl_id
						JOIN outbound outbs
							ON outbs.id_outbound=plos.id_outbound
						WHERE
							id_barang = outbbrg.id_barang
						AND
							pls.pl_id < \''.$post['pl_id'].'\'
					), 0) AS previous_pick, IFNULL(plq.qty, 0) AS picked_qty, nama_satuan
				FROM
					picking_list pl
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				LEFT JOIN outbound_barang outbbrg
					ON outbbrg.id_outbound=outb.id_outbound
				LEFT JOIN picking_qty plq
					ON plq.id_barang=outbbrg.id_barang
						AND plq.pl_id=pl.pl_id
				LEFT JOIN barang brg
					ON brg.id_barang=outbbrg.id_barang
				LEFT JOIN (
					SELECT
						id_barang, COUNT(*) AS available_qty
					FROM
						receiving_barang r
					JOIN m_loc l
						ON l.loc_id=r.loc_id
					WHERE
						l.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						id_qc = \'1\'
					GROUP BY
						id_barang
				) AS tbl_1
					ON tbl_1.id_barang=outbbrg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					pl.pl_id=\''.$post['pl_id'].'\'
				GROUP BY
					brg.id_barang';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_barang'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['available_qty'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['previous_pick'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['picked_qty'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = ($row[$i]['jumlah_barang'] - $row[$i]['previous_pick']) . ' ' . $row[$i]['nama_satuan'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItemPicked($post = array()){
		$result = array();
		$this->db
			->select([
				"outbitm.id","itm.id as id_barang","itm.code as kd_barang","itm.name as nama_barang", "sum(pq.qty) as jumlah_barang", "sum(outbitm.qty) as jumlah_barang", "COALESCE(tbl_1.available_qty) as available_qty",
				"COALESCE((
					SELECT qty
					FROM
						picking_qty pqs
					JOIN pickings ps
						ON ps.pl_id=pqs.picking_id
					JOIN picking_list_outbound plos
						ON plos.pl_id=ps.pl_id
					JOIN outbound outbs
						ON outbs.id=plos.id_outbound
					WHERE
						ps.pl_id = ".$post['pl_id']."
					AND
						outbs.id IN (".$post['id_outbound'].") and pqs.picking_id = itm.id
				), 0) as previous_pick", "COALESCE(pq.qty, 0) as picked_qty","unt.name as nama_satuan","pq.batch"
			])
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('picking_qty pq','pq.picking_id=p.pl_id','left')
			->join('items itm','itm.id=pq.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id and outbitm.item_id = pq.item_id','left')
			->group_by('outbitm.id, itm.id, tbl_1.available_qty, pq.qty, unt.name, pq.batch, itm.code, itm.name, outbitm.qty');

		$tbl_1 = "(SELECT
					ir.item_id, sum(last_qty) AS available_qty
				FROM
					item_receiving_details ird
				JOIN locations loc
					ON loc.id=ird.location_id
				LEFT JOIN item_receiving ir
					ON ir.id=ird.item_receiving_id
				WHERE
					loc.loc_type NOT IN(".$this->config->item('hardcode_location').")
				AND
					ird.qc_id = 1
				GROUP BY
					ir.item_id) as tbl_1";

		$this->db->join($tbl_1, 'tbl_1.item_id=pq.item_id','left');
		$this->db->where('p.pl_id',$post['pl_id']);

		$row = $this->db->get()->result_array();
		
		$len = count($row);

		for($i = 0; $i < $len; $i++){
			$params = array(
				'id_barang' => $row[$i]['id_barang']
			);

			$r = $this->outbound_model->getBatch($params);

			$row[$i]['batchs'] = $r['data'];
			$row[$i]['remaining_qty'] = $row[$i]['jumlah_barang'] - ($row[$i]['previous_pick'] + $row[$i]['picked_qty']);
		}

		$result['data'] = $row;
		

		return $result;
	}

	public function getDetailItemList($post = array()){
		$result = array();

		//TODO id_qc
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'itm.id',
			1 => 'pl_id',
			2 => 'id_barang',
			3 =>  '',
			4 => 'kd_barang',
			5 => 'nama_barang',
			6 => 'jumlah_barang',
			7 => 'available_qty',
			8 => 'previous_pick',
			9 => 'remaining_qty',
			10 => 'picked_qty'
			// 10 => 'jumlah_barang - previous_pick',
			// 11 => 'batch',
			// 12 => 'actual_picked_qty'
		);

		$where = '';

		$this->db->start_cache();


		$this->db
			->select([
				"p.pl_id",
				"itm.id as item_id",
				"itm.code as item_code",
				"itm.name as item_name",
				"unt.code as unit_code",
				"COALESCE((
					SELECT
						SUM(oi.qty)
					FROM picking_list_outbound plo
					JOIN outbound_item oi
						ON oi.id_outbound=plo.id_outbound
					WHERE
						plo.pl_id=p.pl_id
					AND
						oi.item_id=itm.id
				),0) as doc_qty",
				"COALESCE((
					SELECT
						SUM(qty)
					FROM picking_recomendation
					WHERE
						picking_id=p.pl_id
					AND
						(order_item_id=pq.item_id)
				),0) as scanned_qty",
				"COALESCE(pq.batch, '-') as batch",
				"COALESCE(NULL,0) as available_qty",
				"COALESCE(NULL,0) as previous_qty",
				"COALESCE(SUM(pq.qty),0) as pick_qty",
				"pq.is_sub",
				"pq.id as pq_id",
				"p.status_id"
			])
			->from('pickings p')
			->join('picking_qty pq','pq.picking_id=p.pl_id')
			->join('items itm','itm.id=pq.item_id')
			->join('units unt','unt.id=itm.unit_id')
			->group_by('p.pl_id, itm.id, pq.item_id, itm.code, itm.name, pq.batch, unt.code, pq.is_sub, pq.id, p.status_id')
			->where('p.pl_id',$post['pl_id']);

		$this->db->stop_cache();

		$row = $this->db->get();
		// var_dump($this->db->last_query());exit;
		$totalData = $row->num_rows();

		$this->db
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			if($row[$i]['is_sub']==0){
				$item_code = $row[$i]['item_code'];
			}else{
				$item_code = "<a class='substitute'  data-qty=".$row[$i]['pick_qty']." data-id=".$row[$i]['item_id']." data-pq=".$row[$i]['pq_id']."><span class='badge badge-warning' style='font-size:15px!important;color:black;'>".$row[$i]['item_code']."</span></a>";
			}

			$nested = array();
			$nested[] = '';
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $row[$i]['item_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $item_code;
			$nested[] = $row[$i]['item_name'];
			$nested[] = $row[$i]['doc_qty'] . ' ' . $row[$i]['unit_code'];
			$nested[] = $row[$i]['available_qty'] . ' ' . $row[$i]['unit_code'];
			$nested[] = $row[$i]['previous_qty'] . ' ' . $row[$i]['unit_code'];
			$nested[] = ($row[$i]['doc_qty'] - $row[$i]['scanned_qty']). ' ' . $row[$i]['unit_code'];
			$nested[] = $row[$i]['pick_qty'] . ' ' . $row[$i]['unit_code'];
			$nested[] = $row[$i]['batch'];
			$nested[] = $row[$i]['scanned_qty'] . ' ' . $row[$i]['unit_code'];
			// $nested[] = $row[$i]['actual_picked_qty'] . ' ' . $row[$i]['nama_satuan'];
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getSubstitute($params){
		$sql = "select is2.*, itm.name, itm.code, loc.name as loc_name,
		(select pqs.id from picking_qty_substitute pqs where item_substitute_id = is2.id and pqs.picking_qty_id = ".$params['pq_id'].") as check,
		(select pqs.qty from picking_qty_substitute pqs where item_substitute_id = is2.id and pqs.picking_qty_id = ".$params['pq_id'].") as qty,
		(SELECT sum(ird.last_qty) AS available_qty FROM item_receiving_details ird JOIN locations loc ON loc.id=ird.location_id WHERE ird.item_id =itm.id and ird.qc_id = '1' and ird.location_id not in (100,101,102,103,104,105,106) GROUP BY ird.item_id) as available_qty
		from item_substitute is2
		left join items itm on itm.id = is2.item_id_subs
		left join locations loc on loc.id = itm.def_loc_id
		where is2.item_id =".$params['item_id'];

		$pq = $this->db->query($sql)->result_array();

		$sql = "select * from picking_qty_substitute pqs where is_ori is not null and pqs.picking_qty_id = ".$params['pq_id'];
		$check= $this->db->query($sql)->result_array();
		if(count($check)<=0){
			$sql = "SELECT '-' as id, itm.id as item_id, '' item_id_subs, 'Active' as status, itm.name, itm.code, 
					loc.name as loc_name, ird.id as check, 0 as qty, sum(ird.last_qty) AS available_qty 
					FROM item_receiving_details ird 
					JOIN locations loc ON loc.id=ird.location_id 
					JOIN items itm on itm.id = ird.item_id
					WHERE ird.item_id =".$params['item_id']." and ird.qc_id = '1' 
					and ird.location_id not in (100,101,102,103,104,105,106) 
					GROUP BY ird.item_id, itm.id, itm.name, itm.code, loc.name, ird.id";
			$initial = $this->db->query($sql)->result_array();
		}else{
			$sql ="select '-' as id, itm.id as item_id, '' item_id_subs, 'Active' as status, itm.name, itm.code,
			loc.name as loc_name, pqs.id as check, pqs.qty as qty,  
			(SELECT sum(ird.last_qty) AS available_qty FROM item_receiving_details ird JOIN locations loc ON loc.id=ird.location_id WHERE ird.item_id =itm.id and ird.qc_id = '1' and ird.location_id not in (100,101,102,103,104,105,106) GROUP BY ird.item_id) as available_qty
			from picking_qty_substitute pqs
			join picking_qty pq on pqs.picking_qty_id = pq.id
			join items itm on itm.id = pq.item_id
			join locations loc on loc.id = itm.def_loc_id 
			where pq.id = ".$params['pq_id']." and itm.id = ".$params['item_id']." and pqs.is_ori is not null";
			$initial = $this->db->query($sql)->result_array();
		}
		if(isset($initial)){
			for($i=0;$i<count($initial);$i++){
				array_push($pq,$initial[$i]);
			}
		}

		return $pq;

	}

	public function checkSubstitute($params){
		$this->db->trans_start();

		$this->db->where('picking_qty_id',$params['pq_id'])->delete('picking_qty_substitute');

		if($params['item_substitute']){
			for($i=0;$i<count($params['item_substitute']); $i++){
				if($params['qty'][$i]>0){
					$this->db->set('picking_qty_id',$params['pq_id']);
					if($params['item_substitute'][$i]!=='-'){
						$this->db->set('item_substitute_id',$params['item_substitute'][$i]);
					}else{
						$this->db->set('is_ori',1);
					}
					$this->db->set('create_at','NOW()');
					$this->db->set('user_id',$params['user_id']);
					$this->db->set('qty',$params['qty'][$i]);
					$this->db->insert('picking_qty_substitute');
				}
			}
		}

		$this->db->trans_complete();
		return true;
	}

	public function getBatchRow($post = array()){
		$result = array();

		$sql = 'SELECT
					outbbrg.id_barang, kd_barang, nama_barang, jumlah_barang,
					IFNULL(tbl_1.available_qty, 0) AS available_qty,
					IFNULL((
						SELECT
							SUM(qty)
						FROM
							picking_qty plqs
						JOIN picking_list pls
							ON pls.pl_id=plqs.pl_id
						JOIN picking_list_outbound plos
							ON plos.pl_id=pls.pl_id
						JOIN outbound outbs
							ON outbs.id_outbound=plos.id_outbound
						WHERE
							id_barang = outbbrg.id_barang
						AND
							outbs.id_outbound=\''.$post['id_outbound'].'\'
					), 0) AS previous_pick, IFNULL(plq.qty, 0) AS picked_qty, outbbrg.batch, nama_satuan
				FROM
					outbound outb
				LEFT JOIN outbound_barang outbbrg
					ON outbbrg.id_outbound=outb.id_outbound
				LEFT JOIN barang brg
					ON brg.id_barang=outbbrg.id_barang
				LEFT JOIN (
					SELECT
						id_barang, kd_batch, COUNT(*) AS available_qty
					FROM
						receiving_barang r
					JOIN m_loc l
						ON l.loc_id=r.loc_id
					WHERE
						l.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						id_qc = \'1\'
					AND
						kd_batch=\''.$post['kd_batch'].'\'
					GROUP BY
						id_barang, kd_batch
				) AS tbl_1
					ON tbl_1.id_barang=outbbrg.id_barang
				LEFT JOIN picking_list_outbound plo
					ON plo.id_outbound=outb.id_outbound
				LEFT JOIN picking_list pl
					ON pl.pl_id=plo.pl_id
				LEFT JOIN picking_qty plq
					ON plq.id_barang=outbbrg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					outb.id_outbound=\''.$post['id_outbound'].'\'
				AND
					outbbrg.id_barang=\''.$post['id_barang'].'\'
				GROUP BY
					brg.id_barang';

		$row = $this->db->query($sql)->row_array();
		$row['remaining_qty'] = $row['jumlah_barang'] - $row['previous_pick'];

		$result['data'] = $row;

		return $result;
	}

	public function getDetailItem($post = array()){
		$result = array();
		
		$where = '';

		if($post['id_outbound'] !== ''){
			$where .= 'and outb.id IN ('.$post['id_outbound'].')';
		}
		if($post['id_area'] !== ''){
			// $where .= ' AND larea.id IN ('.$post['id_area'].') ';
			$where .= " and itm.item_area IN ('".$post['id_area']."') ";
		}


		$sql = 'SELECT
					DISTINCT ON(outbitm.id) outbitm.id, outbitm.item_id as id_barang, itm.code as kd_barang,
					(CASE itm.preorder WHEN 1 THEN CONCAT(itm.name, \'- PREORDER\') ELSE itm.name END) as nama_barang,
					outbitm.qty as jumlah_barang,
					outb.code as outbound_code,
					(
						CASE
							WHEN outbitm.batch IS NULL
								THEN COALESCE(tbl_1.available_qty, 0)
							ELSE COALESCE(tbl_2.available_qty, 0)
						END
					) AS available_qty,
					COALESCE((
						SELECT
							SUM(pr.qty)
						FROM
							picking_recomendation pr
						JOIN outbound outbs
							ON outbs.id=pr.outbound_id
						WHERE
							pr.item_id = outbitm.item_id
						AND
							outbs.id IN ('.$post['id_outbound'].')
					), 0) AS previous_pick, COALESCE(plq.qty, 0) AS picked_qty, outbitm.batch, unt.name as nama_satuan, itm.item_area as item_area
				FROM
					outbound outb
				LEFT JOIN outbound_item outbitm
					ON outbitm.id_outbound=outb.id
				LEFT JOIN items itm
					ON itm.id=outbitm.item_id
				LEFT JOIN (
					SELECT
						ird.item_id, sum(ird.last_qty) AS available_qty
					FROM
						item_receiving_details ird
					JOIN locations loc
						ON loc.id=ird.location_id
					WHERE
						'.//loc.loc_type NOT IN('.$this->config->item('hardcode_location').') AND
						'ird.qc_id = \'1\'
					GROUP BY
						ird.item_id
				) AS tbl_1
					ON tbl_1.item_id=outbitm.item_id
				LEFT JOIN (
					SELECT
						ird.item_id, ird.kd_batch, sum(ird.last_qty) AS available_qty
					FROM
						item_receiving_details ird
					JOIN locations loc
						ON loc.id=ird.location_id
					WHERE
						loc.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						qc_id = \'1\'
					GROUP BY
						ird.item_id, ird.kd_batch
				) AS tbl_2
					ON tbl_2.item_id=outbitm.item_id
						AND tbl_2.kd_batch=outbitm.batch
				LEFT JOIN picking_list_outbound plo
					ON plo.id_outbound=outb.id
				LEFT JOIN pickings p
					ON p.pl_id=plo.pl_id
				LEFT JOIN picking_qty plq
					ON plq.item_id=outbitm.item_id
				LEFT JOIN units unt
					ON unt.id=itm.unit_id
				LEFT JOIN locations loc
					ON loc.location_category_id = itm.category_id
				JOIN location_areas larea
					ON larea.id = loc.location_area_id
				WHERE
					1 = 1
					'.$where.'
				GROUP BY
					itm.id, outbitm.id, outbitm.item_id, itm.code, itm.name, outb.code, outbitm.qty, outbitm.batch, tbl_1.available_qty, tbl_2.available_qty,
					plq.qty, unt.name
				ORDER BY
					outbitm.id ASC';

		// dd($sql);
		$row = $this->db->query($sql)->result_array();
		$len = count($row);

		for($i = 0; $i < $len; $i++){

			$params = array(
				'id_barang' => $row[$i]['id_barang']
			);

			$r = $this->outbound_model->getBatch($params);

			$row[$i]['batchs'] = $r['data'];
			$row[$i]['remaining_qty'] = $row[$i]['jumlah_barang'] - $row[$i]['previous_pick'];

		}

		$result['data'] = $row;
		
		return $result;
	}

	public function getDetailItem_BAK($post = array()){
		$result = array();

		$sql = 'SELECT
					outbitm.id, outbitm.item_id as id_barang, itm.code as kd_barang,
					(CASE itm.preorder WHEN 1 THEN CONCAT(itm.name, \'- PREORDER\') ELSE itm.name END) as nama_barang,
					outbitm.qty as jumlah_barang,
					(
						CASE
							WHEN outbitm.batch IS NULL
								THEN COALESCE(tbl_1.available_qty, 0)
							ELSE COALESCE(tbl_2.available_qty, 0)
						END
					) AS available_qty,
					COALESCE((
						SELECT
							SUM(qty)
						FROM
							picking_qty plqs
						JOIN pickings pls
							ON pls.pl_id=plqs.picking_id
						JOIN picking_list_outbound plos
							ON plos.pl_id=pls.pl_id
						JOIN outbound outbs
							ON outbs.id=plos.id_outbound
						WHERE
							plqs.item_id = outbitm.item_id
						AND
							outbs.id=\''.$post['id_outbound'].'\'
					), 0) AS previous_pick, COALESCE(plq.qty, 0) AS picked_qty, outbitm.batch, unt.name as nama_satuan
				FROM
					outbound outb
				LEFT JOIN outbound_item outbitm
					ON outbitm.id_outbound=outb.id
				LEFT JOIN items itm
					ON itm.id=outbitm.item_id
				LEFT JOIN (
					SELECT
						ird.item_id, sum(ird.last_qty) AS available_qty
					FROM
						item_receiving_details ird
					JOIN locations loc
						ON loc.id=ird.location_id
					WHERE
						'.//loc.loc_type NOT IN('.$this->config->item('hardcode_location').') AND
						'ird.qc_id = \'1\'
					GROUP BY
						ird.item_id
				) AS tbl_1
					ON tbl_1.item_id=outbitm.item_id
				LEFT JOIN (
					SELECT
						ird.item_id, ird.kd_batch, sum(ird.last_qty) AS available_qty
					FROM
						item_receiving_details ird
					JOIN locations loc
						ON loc.id=ird.location_id
					WHERE
						loc.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						qc_id = \'1\'
					GROUP BY
						ird.item_id, ird.kd_batch
				) AS tbl_2
					ON tbl_2.item_id=outbitm.item_id
						AND tbl_2.kd_batch=outbitm.batch
				LEFT JOIN picking_list_outbound plo
					ON plo.id_outbound=outb.id
				LEFT JOIN pickings p
					ON p.pl_id=plo.pl_id
				LEFT JOIN picking_qty plq
					ON plq.item_id=outbitm.item_id
				LEFT JOIN units unt
					ON unt.id=itm.unit_id
				WHERE
					outb.id=\''.$post['id_outbound'].'\'
				GROUP BY
					itm.id, outbitm.id, outbitm.item_id, itm.code, itm.name, outbitm.qty, outbitm.batch, tbl_1.available_qty, tbl_2.available_qty,
					plq.qty, unt.name
				ORDER BY
					outbitm.id ASC';

		$row = $this->db->query($sql)->result_array();
		$len = count($row);

		for($i = 0; $i < $len; $i++){

			$params = array(
				'id_barang' => $row[$i]['id_barang']
			);

			$r = $this->outbound_model->getBatch($params);

			$row[$i]['batchs'] = $r['data'];
			$row[$i]['remaining_qty'] = $row[$i]['jumlah_barang'] - $row[$i]['previous_pick'];

		}

		$result['data'] = $row;

		return $result;
	}

	public function getOutboundCodeList(){
		$result = array();

		$this->db
			->select(["outb.id as id_outbound", "outb.code as kd_outbound", "TO_CHAR(outb.date, 'DD/MM/YYYY') as tanggal_outbound", "COALESCE(CONCAT(spl.code, ' - ', spl.name), cust.name) as customer_name"])
			->from('outbound outb')
			->join('suppliers spl','spl.id=outb.customer_id AND outb.document_id=2','left outer')
			->join('customers cust','cust.id=outb.customer_id AND outb.document_id <>2','left outer')
			->join('status st','st.status_id=outb.status_id','left')
			->where('outb.status_id <>', 2)
			->order_by('outb.id','DESC');

		$result = $this->db->get()->result_array();

		return $result;
	}
	
	public function getPickingAreaList() {
		$result = array();

		$this->db
			->select(['item_area'])
			->from('items')
			->where('item_area is not null')
			->order_by('item_area','ASC')
			->group_by('item_area');

		$result = $this->db->get()->result_array();

		return $result;
	}

    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $pl_name= $this->input->post("pl_name");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $pl_id = $this->input->post('pl_id');
        if(!empty($pl_name)){
            $condition["a.pl_name like '%$pl_name%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($pl_id)){
            $condition["b.pl_id"]=$pl_id;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

        $this->db->select('*');
        //$this->db->from($this->table  . ' a');//pl
        //$this->db->from($this->table2 . ' f');//pl_do
        $this->db->from($this->do . ' b');
        $this->db->join('toko t','t.id_toko = b.id_toko','left');
        $this->db->order_by('b.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_hasil($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $pl_name= $this->input->post("pl_name");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $pl_id = $this->input->post('pl_id');
        if(!empty($pl_name)){
            $condition["a.pl_name like '%$pl_name%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($pl_id)){
            $condition["b.pl_id"]=$pl_id;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

        $this->db->select('*');
        //$this->db->from($this->table  . ' a');//pl
        //$this->db->from($this->table2 . ' f');//pl_do
        $this->db->from($this->do . ' b');
        $this->db->join('receiving_barang rb','rb.id_do = b.id_do','left');
        $this->db->join('m_loc','m_loc.loc_id = rb.loc_id','left');
        $this->db->join('barang ','barang.id_barang = rb.id_barang','left');
        $this->db->join('satuan s','s.id_satuan = barang.id_satuan','left');
        $this->db->order_by('b.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_databarcode_by_id($pl_id) {
        $condition['a.pl_id'] = $pl_id;
        // ===========Filtering=================
        //$condition = array();
        $this->load->model('bbk_model');
        return $this->bbk_model->data_detail($condition)->get();

    }

    public function get_by_id($id) {
        $condition['a.pl_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
    //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.pl_date >= '$tahun_aktif_awal'"] = null ;
        $condition["a.pl_date <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->pl_id;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">';

            $action .= '<li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Proses Picking List', array('id' => 'drildown_key_p_Pl_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_picking_list', 'data-source' => base_url('picking_list/get_detail_hasil_picking_list/' . $id))) . ' ';
            $action .= '</li>';
            $action .= '<li>';
            $action .= anchor(null, '<i class="fa fa-print"></i>Print Picking List', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('picking_list/print_barcode/' . $id)));
            $action .= '</li>';


            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('picking_list/edit/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('picking_list/delete/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->pl_id,
                    'pl_date' => hgenerator::switch_tanggal($value->pl_date),
                    'pl_name' => anchor(null, $value->pl_name, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_picking_list', 'data-source' => base_url('picking_list/get_detail_hasil_picking_list/' . $id))) ,
                    'do_code' => $value->kd_do,
                    'store' => $value->nama_toko,
                    'address' => $value->alamat,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->pl_id,
                    'pl_date' => hgenerator::switch_tanggal($value->pl_date),
                    'pl_name' => anchor(null, $value->pl_name, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_picking_list', 'data-source' => base_url('picking_list/get_detail_hasil_picking_list/' . $id))) ,
                    'do_code' => $value->kd_do,
                    'store' => $value->nama_toko,
                    'address' => $value->alamat,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail() {
        $pl_id = $this->input->post('pl_id');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_do;
            $action = '';
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbk/edit_detail/' .$pl_id.'/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbk/delete_detail/' .$pl_id.'/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $value->id_do,
                    'tanggal_do' => $value->tanggal_do,
                    'kd_do'=>$value->kd_do,
                    'nama_customer' => $value->nama_toko,
                    'alamat' => $value->alamat,
                    'aksi' => $action

                );
            }else{
                $rows[] = array(
                    'primary_key' => $value->id_do,
                    'tanggal_do' => $value->tanggal_do,
                    'kd_do'=>$value->kd_do,
                    'jenis_do' => $value->jenis_do,
                    'nama_customer' => $value->nama_customer,
                    'alamat' => $value->alamat,
                    'aksi' => $action

                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function qty_fefo($id_barang,$tahun=NULL){
        $this->db->select('sum(qty) as jumlah, min(tahun) as tahun');
        $this->db->from('v_pl_loc');
        $this->db->where('id_barang',$id_barang);
        if($tahun){
            $this->db->where('tahun <='.$tahun,NULL);
        }else{
            $this->db->where("tahun = (select min(tahun) from v_pl_loc where id_barang=$id_barang)",NULL);
        }
        $hasil = $this->db->get()->row();
        return $hasil;
    }

    public function list_loc_fefo($id_barang,$tahun){
        $this->db->select('loc_name');
        $this->db->from('v_pl_loc vl');
        $this->db->join('m_loc l','l.loc_id = vl.loc_id','left');
        $this->db->where('id_barang',$id_barang);
        $this->db->where('tahun <='.$tahun,NULL);
        $this->db->where('l.loc_id <> 100',NULL);
        $this->db->where('l.loc_id <> 101',NULL);
        $hasil = $this->db->get();
        $data = '';
        $i=1;
        foreach ($hasil->result() as $row) {
            if($i!=1)
                $data .= ', ';
            $data .= $row->loc_name;
            $i++;
            # code...
        }
        return $data;
    }

    function get_list_loc_fefo($id_barang,$jumlah_barang){
        $cek_qty_fefo = $this->qty_fefo($id_barang);
        $list_loc_fefo = '';
        if($jumlah_barang <= $cek_qty_fefo->jumlah){
            $list_loc_fefo = $this->list_loc_fefo($id_barang,$cek_qty_fefo->tahun);
        }else{
            $tahun = $cek_qty_fefo->tahun;
            $i = 1;
            while($i<=10){
                $cek_qty_fefo = $this->qty_fefo($id_barang,$tahun+$i);
                if($jumlah_barang <= $cek_qty_fefo->jumlah){
                    $list_loc_fefo = $this->list_loc_fefo($id_barang,$cek_qty_fefo->tahun + $i);
                    $i=$i+10;
                }
                $i++;
            }
        }
        return $list_loc_fefo;
    }

    public function data_table_detail_hasil() {
        $pl_id = $this->input->post('pl_id');
        // Total Record
        $this->load->model('bbk_model');
        $where = array('a.pl_id'=>$pl_id);
        $total = $this->bbk_model->data_detail_pl($where)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);

        $data_detail_hasil = $this->bbk_model->data_detail_pl($where)->get();
        $rows = array();


        foreach ($data_detail_hasil->result() as $value) {
            $list_loc_fefo = $this->get_list_loc_fefo($value->id_barang,$value->jumlah_barang);
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'qty' => $value->jumlah_barang.' '.$value->nama_satuan,
                    'loc' => $list_loc_fefo,

                );
            }else{
                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'qty' => $value->jumlah_barang.' '.$value->nama_satuan,
                    'loc' => $list_loc_fefo,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function export_excel($post=array()){

    	if(!empty($post['picking_id'])){
    		$this->db->where('p.pl_id',$post['picking_id']);
    	}

    	$this->db
    		->select([
    			"itm.code as item_code",
    			"itm.name as item_name",
    			"unt.code as unit_code",
    			"COALESCE(pq.qty,0) as doc_qty",
    			"COALESCE(SUM(pr.qty),0) as pick_qty"
    		])
    		->from('pickings p')
    		->join('picking_qty pq','pq.picking_id=p.pl_id')
    		->join('picking_recomendation pr','pr.picking_id=p.pl_id and pr.item_id=pq.item_id','left')
    		->join('items itm','itm.id=pq.item_id','left')
    		->join('units unt','unt.id=itm.unit_id','left')
    		->group_by('itm.code, itm.name, pq.qty, unt.code');

    	$data = $this->db->get()->result_array();

    	return $data;

    }

    public function export_excel_detail($post=array()){

    	if(!empty($post['picking_id'])){
    		$this->db->where('p.pl_id',$post['picking_id']);
    	}

    	$this->db
    		->select([
    			"itm.code as item_code",
    			"itm.name as item_name",
    			"pr.unique_code",
    			"COALESCE(ird.parent_code, '-') as parent_code",
    			"COALESCE(CAST(ird.tgl_exp as CHAR), '-') as expired_date",
    			"COALESCE(pr.qty,0) as qty",
    			"COALESCE(usr.user_name,'-') as user_picked",
    			"loc.name as pick_locator"
    		])
    		->from('pickings p')
    		->join('picking_recomendation pr','pr.picking_id=p.pl_id')
    		->join('item_receiving_details ird','ird.unique_code=pr.unique_code and pr.picking_id=ird.picking_id and ird.item_id=pr.item_id','left')
    		->join('transfers tf','tf.picking_id=p.pl_id and tf.unique_code=pr.unique_code','left')
    		->join('locations loc','loc.id=tf.loc_id_old','left')
    		->join('items itm','itm.id=pr.item_id','left')
    		->join('units unt','unt.id=itm.unit_id','left')
    		->join('users usr','usr.id=pr.user_id','left');

    	$data = $this->db->get()->result_array();

    	return $data;

    }

    public function data_table_excel() {
        //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.pl_date >= '$tahun_aktif_awal'"] = null ;
        $condition["a.pl_date <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->pl_id;
            $action = '';
                $rows[] = array(
                    'pl_date' => hgenerator::switch_tanggal($value->pl_date),
                    'pl_name' => $value->pl_name,
                    'proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                );
        }
        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->pl_id_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    function data_insert($data){
    	$data = array(
    		'name' 		=> $data['pl_name'],
    		'date'		=> $data['pl_date'],
    		'pl_status'	=> $data['pl_status'],
    		'user_id'	=> $data['user_id'],
    		'priority'	=> $data['priority']
    	);

    	return $data;
    }

    public function create($data) {
    	$data = $this->data_insert($data);
    	$data['status_id'] = 6;
		$this->db->trans_start();
		$this->db->insert($this->table, $data);
		$this->db->trans_complete();

		$this->db->limit(1);
		$this->db->order_by('pl_id','desc');
		$id = $this->db->get($this->table)->row_array()['pl_id'];
        // return $this->db->insert($this->table, $data);
        return $id;
    }

    public function update($data, $id) {
    	$data = $this->data_insert($data);
        return $this->db->update($this->table, $data, array('pl_id' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        $this->db->from('do');
        $this->db->join('do_barang db','do.id_do = db.id_do','left');
        $this->db->where('do.id_do',$id);
        $hasil = $this->db->get();

        foreach ($hasil->result() as $row) {

            if($row->jenis_do == 'FIFO' || $row->jenis_do==''){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_in','ASC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'FEFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_exp','ASC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'LIFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_in','DESC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'LEFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_exp','DESC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }
            # code...
        }
        return $this->db->update($this->do, $data, array('id_do' => $id));
    }

    public function delete($id) {

		$result = array();

		$this->db
			->select('count(*) as total')
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('item_receiving_details ird','ird.picking_id=p.pl_id')
			->where('p.pl_id',$id);

		$row = $this->db->get()->row_array();
		if($row['total'] <= 0){

			$this->db->trans_start();

			$pq_id = $this->db->select('id')->from('picking_qty')->where('picking_id',$id)->get()->result_array();
			
			for($z=0;$z<count($pq_id);$z++){
				$this->db->where('picking_qty_id',$pq_id[$z]['id'])->delete('picking_qty_substitute');
			}

			$this->db->where('picking_id',$id)->delete('picking_qty');

			$this->db
				->select('id_outbound')
				->from('picking_list_outbound')
				->where('pl_id',$id);

			$r = $this->db->get()->row_array();

			$this->db->delete('picking_list_outbound',['pl_id'=>$id]);
			$this->db->delete('picking_recomendation',['picking_id'=>$id]);

			$this->db
				->select('count(*) as total')
				->from('picking_list_outbound')
				->where('id_outbound', $r['id_outbound']);

			$rows = $this->db->get()->row_array();
			if($rows['total'] <= 0){
				$this->db
					->set('status_id', 3)
					->where('id', $r['id_outbound'])
					->update('outbound');
			}


			$this->db->where('pl_id',$id)->delete('pickings');

			$this->db->trans_complete();

			if($this->db->trans_status()){
				$result['status'] = 'OK';
				$result['message'] = 'Delete success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Delete failed';
			}

		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';
		}

		return $result;
		/*
        $this->db->delete($this->table4, array('pl_id' => $id));
        return $this->db->delete($this->table, array('pl_id' => $id));
		*/
    }

    public function force_delete($id) {

		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					picking_list pl
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				JOIN receiving_barang rcvbrg
					ON rcvbrg.pl_id=pl.pl_id
				WHERE
					pl.pl_id=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		//if($row['total'] <= 0){

			$sql = 'DELETE FROM picking_list WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'DELETE FROM picking_qty WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'SELECT
						id_outbound
					FROM
						picking_list_outbound
					WHERE
						pl_id=\''.$id.'\'';

			$r = $this->db->query($sql)->row_array();
			$id_outbound = $r['id_outbound'];

			$sql = 'DELETE FROM outbound_barang_picked WHERE id_outbound=\''.$id_outbound.'\'';
			$this->db->query($sql);

			$sql = 'DELETE FROM picking_list_outbound WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);
			$sql = 'DELETE FROM picking_recomendation WHERE id_picking=\''.$id.'\'';
			$this->db->query($sql);
			$sql = 'UPDATE
						receiving_barang
					SET
						pl_id = NULL, pl_status = 0, st_shipping = 0,loc_id = 171
					WHERE
						pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
						picking_list_outbound
					WHERE
						id_outbound=\''.$id_outbound.'\'';

			$rows = $this->db->query($sql)->row_array();
			if($rows['total'] <= 0){
				$sql = 'UPDATE outbound SET id_status_outbound=\'1\' WHERE id_outbound=\''.$id_outbound.'\'';
				$this->db->query($sql);
			}

			$result['status'] = 'OK';
			$result['message'] = 'Delete success';
		/*}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';
		}*/

		return $result;
		/*
        $this->db->delete($this->table4, array('pl_id' => $id));
        return $this->db->delete($this->table, array('pl_id' => $id));
		*/
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id_do' => $id));
    }


    public function options($default = '--Pilih Kode bbk--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->pl_id] = $row->pl_name ;
        }
        return $options;
    }

    public function options_do($default = '--Choose DO--', $key = '') {
        $data = $this->data_list_do()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_do] = $row->kd_do ;
        }
        return $options;
	}

	public function check_picking($id_outbound=array()){
		$this->db->where_in('id_outbound',$id_outbound);
		$result = $this->db->get('picking_list_outbound');
		return $result->num_rows();
	}

    public function statusOptions($default = '--Pilih Status--', $key = '') {

    	$this->db->like('type','OUTBOUND');
        $data = $this->db->get('status');
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->status_id] = $row->name ;
        }
        return $options;
    }


    public function cancel($params=array()){

    	if(!empty($params)){

	    	$this->db->trans_start();

	    	$this->db
	    		->select(['pr.picking_id', 'pr.unique_code', 'pr.old_location_id', "COALESCE(SUM(qty),0) as qty"])
	    		->from('picking_recomendation pr')
	    		// ->join('transfers tf','tf.unique_code=pr.unique_code and tf.picking_id=pr.picking_id')
	    		->where('pr.picking_id',$params['picking_id'])
	    		->group_by('pr.picking_id, pr.unique_code, pr.old_location_id');

	    	$data = $this->db->get()->result_array();

	    	foreach ($data as $d) {

		    	$this->db
		    		->set('last_qty','last_qty+'.$d['qty'],FALSE)
		    		->set('picking_id',NULL)
		    		->set('user_id_picking',NULL)
		    		->set('location_id',"(CASE WHEN (((SELECT location_type_id FROM locations WHERE id=location_id) <> 6 ) AND location_id NOT IN (101,104,103)) THEN location_id ELSE ".$data[0]['old_location_id']." END)",FALSE)
		    		->where('unique_code',$d['unique_code'])
		    		->where('picking_id',$d['picking_id'])
		    		->update('item_receiving_details');

		    }

	    	$updateOutbound = "update outbound set status_id=17 where id IN (SELECT id_outbound FROM picking_list_outbound WHERE pl_id=".$params['picking_id'].")";
	    	$this->db->query($updateOutbound);

	        $this->db->set("pl_status", 1);
	        $this->db->set("status_id", 17);
	        $this->db->set("end_time", date("Y-m-d h:i:s"));
	        $this->db->where("pl_id", $params['picking_id']);
	        $this->db->update("pickings");

	       	$this->db
	       		->where('picking_id',$params['picking_id'])
	       		->delete('picking_recomendation');

	    	$this->db->trans_complete();

	    	return true;

    	}

    	return false;

    }

    function resetPicking($params=array()){

    	if(!empty($params)){

	    	$this->db->trans_start();

	    	$this->db
	    		->select('pr.picking_id, pr.unique_code, tf.loc_id_old')
	    		->from('picking_recomendation pr')
	    		->join('transfers tf','tf.unique_code=pr.unique_code and tf.picking_id=pr.picking_id')
	    		->where('pr.picking_id',$params['picking_id']);

	    	$data = $this->db->get()->result_array();

	    	foreach ($data as $d) {

		    	$this->db
		    		->set('last_qty',1)
		    		->set('picking_id',NULL)
		    		->set('user_id_picking',NULL)
		    		->set('location_id',$d['loc_id_old'])
		    		->where('unique_code',$d['unique_code'])
		    		->where('picking_id',$d['picking_id'])
		    		->update('item_receiving_details');

		    }

	    	$this->db
	    		->where('picking_id',$params['picking_id'])
	    		->delete('picking_recomendation');

	        $this->db->set("status_id", 6);
	        $this->db->where("pl_id", $params['picking_id']);
	        $this->db->update("pickings");

	    	$this->db->trans_complete();

	    	return true;

    	}

    	return false;

    }

    function cancel_item_picked($params=array()){

    	if(!empty($params)){

	    	$this->db->trans_start();

	    	$this->db
	    		->select(["unique_code","old_location_id","COALESCE(sum(qty),0) as qty"])
	    		->from('picking_recomendation')
	    		->where('picking_id',$params['picking_id'])
	    		->where('unique_code',$params['serial_number'])
	    		->group_by('unique_code, old_location_id');

	    	$data = $this->db->get()->row_array();

	    	if($data['qty'] == $params['quantity']){

		    	$this->db
		    		->set("last_qty","last_qty + ".$data['qty'],FALSE)
		    		->set('picking_id',NULL)
		    		->set('user_id_picking',NULL)
		    		->set('location_id',"(CASE WHEN (((SELECT location_type_id FROM locations WHERE id=location_id) <> 6 ) AND location_id NOT IN (101,104,103)) THEN location_id ELSE ".$data['old_location_id']." END)",FALSE)
		    		->where('picking_id',$params['picking_id'])
		    		->where('unique_code',$params['serial_number'])
		    		->update('item_receiving_details');

		    	$this->db
		    		->where('picking_id',$params['picking_id'])
		    		->where('unique_code',$params['serial_number'])
		    		->delete('picking_recomendation');

		    }else{

		    	$this->db
		    		->set("last_qty","last_qty + ".$params['quantity'],FALSE)
		    		->set('user_id_picking',NULL)
		    		->set('location_id',"(CASE WHEN (((SELECT location_type_id FROM locations WHERE id=location_id) <> 6 ) AND location_id NOT IN (101,104,103)) THEN location_id ELSE ".$data['old_location_id']." END)",FALSE)
		    		->where('picking_id',$params['picking_id'])
		    		->where('unique_code',$params['serial_number'])
		    		->update('item_receiving_details');

		    	$this->db
		    		->set("qty","qty - ".$params['quantity'],FALSE)
		    		->where('picking_id',$params['picking_id'])
		    		->where('unique_code',$params['serial_number'])
		    		->update('picking_recomendation');

		    }

	    	$this->db
	    		->set('pl_status',0)
	    		->set('status_id',7)
	    		->set('end_time',NULL)
	    		->where('pl_id',$params['picking_id'])
	    		->update('pickings');

	    	$this->db->trans_complete();

	    	return true;

    	}

    	return false;

    }

	function setComplete($pickingId=""){

    	if(!empty($pickingId)){

    		$dateTime = date('Y-m-d h:i:s');

    		$this->db
    			->set('end_time',$dateTime)
    			->set('pl_status',1)
        		->set("status_id", 8)
        		->where('pl_id',$pickingId)
				->where('status_id',7)
        		->update('pickings');

			$this->db->select(['sum(qty) as qty','item_id'])
					->from('picking_recomendation')
					->where('picking_id',$pickingId)
					->group_by('item_id');

			$datas = $this->db->get();
			$result = $datas->result_array();
			for($i = 0; $i < $datas->num_rows(); $i++) {
				$this->db->set('qty',$result[$i]['qty'])
						->where('picking_id',$pickingId)
						->where('item_id',$result[$i]['item_id'])
						->update('picking_qty');
			}

        	return true;

    	}

    	return false;

    }
	
	//old in transit
	// public function getInTransit($post = array())
	// {
	// 	$this->db
	// 	->select(['pr.unique_code as u_code', 'i.name as item_name', 'pr.qty as qty','l.name as location_name'])
	// 	->from('picking_recomendation pr')
	// 	->join('items i','i.id = pr.item_id','left')
	// 	->join('locations l','l.id = pr.location_id','left')
	// 	->where('pr.location_id',103)
	// 	->where('pr.picking_id',$post['pl_id']);
		
	// 	$rows = $this->db->get()->result_array();

	// 	return $rows;
	// }

	//new 
	public function getInTransit($post = array())
	{
		$this->db
		->select(['pr.unique_code as u_code', 'p.pl_id as pl_id','p.name as picking', 'i.name as item_name', 'pr.qty as qty','l.name as location_name'])
		->from('picking_recomendation pr')
		->join('items i','i.id = pr.item_id','left')
		->join('locations l','l.id = pr.location_id','left')
		->join('location_types lt','lt.id = l.location_type_id','left')
		->join('pickings p','p.pl_id = pr.picking_id','left');
		// ->where('p.end_time !=',null);
		
		if($post['pl_id'] !== 'null'){
			$this->db->where('pr.picking_id',$post['pl_id']);
		}

		if($post['outbound_id']!=='null'){
			$this->db->where('pr.outbound_id',$post['outbound_id']);
		}

		$loc = '';

		if($post['location_id']!=='null'){
			$this->db->where('l.id',$post['location_id']);
		}else{
			if($post['pl_id'] == 'null' && $post['outbound_id'] =='null'){
				$loc .= ' WHERE ';
			}else{
				$loc .= ' AND ';
			}
			$loc .= " (lt.name = 'TRAY') order by p.name";
		}

		// dd($this->db->get_compiled_select());
		$sql = $this->db->get_compiled_select().$loc;
		// dd($sql);
		$rows = $this->db->query($sql)->result_array();
		// dd($rows);
		return $rows;
	}
}

?>

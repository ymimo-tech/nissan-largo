<?php

class Api_repack_putaway_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($serial_number){
        $this->db->select("ird.location_id as loc_id, ird.repack_order_id as id_order_kitting");
        $this->db->from("item_receiving_details ird");
        $this->db->join("repack_orders ro", "ro.id = ird.repack_order_id", "left");
        $this->db->where("st_repack", 1);
        $this->db->where("ird.unique_code", $serial_number);
        return $this->db->get();
    }

    function get_lp($serial_number){
        $this->db->select("ird.location_id as loc_id, ird.repack_order_id as id_order_kitting");
        $this->db->from("item_receiving_details ird");
        $this->db->join("repack_orders ro", "ro.id = ird.repack_order_id", "left");
        $this->db->where("st_repack", 1);
        $this->db->where("ird.parent_code", $serial_number);
        return $this->db->get();
    }

    function set($data){
        $id_user = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $this->db->set("location_id", 103);
        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->update("item_receiving_details");
    }

    function set_lp($data){
        $id_user = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $this->db->set("location_id", 103);
        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->update("item_receiving_details");
    }

    function start($data){
        $this->db->set("start_putaway", "COALESCE(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("id", $data["id_receiving"]);
        $this->db->update("receivings");
    }

    function start_lp($data){
        $this->db->set("start_putaway", "COALESCE(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("id_receiving", $data["id_receiving"]);
        $this->db->update("receiving");
    }

    
    function send_staging($kd_receiving=''){
        $this->load->model('api_staging_model');
        $sql = "
            SELECT
                kd_barang,
                rb.id_barang,
                sum(first_qty)AS qty,
                r.id_receiving,
                i.id_staging,
                i.kd_inbound,
                kd_receiving,
                delivery_doc,
                max(tgl_in)AS rcv_date,
                FinaCode,
                max(id_receiving_barang) as max_id
            FROM
                receiving_barang rb
            LEFT JOIN barang b ON b.id_barang = rb.id_barang
            LEFT JOIN receiving r ON r.id_receiving = rb.id_receiving
            LEFT JOIN inbound i ON i.id_inbound = r.id_po
            LEFT JOIN(
                SELECT
                    id_barang,
                    id_inbound,
                    FinaCode
                FROM
                    inbound_barang
                GROUP BY
                    id_inbound,
                    id_barang
            )t_ib ON t_ib.id_barang = b.id_barang
            AND t_ib.id_inbound = i.id_inbound 
            WHERE
                1=1
            AND i.id_staging > 0 
            AND rb.st_staging = 0 
            AND rb.id_qc = 1
            GROUP BY
                id_receiving,
                kd_barang,
                i.id_staging,
                i.kd_inbound";
        $query = $this->db->query($sql);
        $Line = 0;
        foreach ($query->result() as $row) {
            $Line++;
            //================= start update page code 6542========
            //== L2 no longer update, will continue insert row

            $NoLine = $Line;
            $sql = "select max(LineNum) as maxline 
                    from staging_inbound_l2
                    where RcvDoc = '".$row->delivery_doc."'";
            $query = $this->db->query($sql);
            $hasil = $query->row();
            if(!empty($hasil->maxline)){
                $NoLine = $hasil->maxline + 100;
            }else{
                $NoLine = 100;
            }
            $data = array(
                'DocEntry' =>$row->id_staging,
                'LineNum' =>$NoLine,
                'SKU'=>$row->kd_barang,
                'ReceiveQty'=>$row->qty,
                'SyncStatus'=>'N',
                'CreateDate'=>date('Y-m-d H:i:s'),
                'RcvDoc'=>$row->delivery_doc,
                'RcvDate'=>$row->rcv_date,
                'InbNo'=> $row->kd_inbound,
                'FinaCode' => $row->FinaCode
                );
            
            $result = $this->api_staging_model->_curl_process('insert','f_t_inbound_l2',array('data'=>$data));
            if(!empty($result)){
                if($result->status == '001'){
                    $this->db->insert('staging_inbound_l2',$data);
                    $data_update = array(
                        'st_staging' => 1);
                    $data_where = array(
                        'id_receiving_barang <= '.$row->max_id => NULL,
                        'st_staging' => 0,
                        'id_barang' => $row->id_barang,
                        'id_receiving' => $row->id_receiving);
                    $this->db->where($data_where);
                    $this->db->update('receiving_barang',$data_update);
                    //echo '1212';
                }
            }
        }

        $this->load->model('api_sync_model');
        $this->api_sync_model->sync_qty();

    }

    function finish($repack_order_id){
        $this->db->set("status_id", 4);
        $this->db->where("id", $repack_order_id);
        $this->db->update("repack_orders");
    }

    function count_putaway($data){

        $this->db->select('repack_order_id as id_order_kitting')
            ->from('item_receiving_details')
            ->where('unique_code',$data['kd_unik']);

        $idOrderKitting = $this->db->get()->row_array();

        $this->db->select('repack_order_id')
            ->from('item_receiving_details')
            ->where('location_id',100)
            ->where('repack_order_id',$idOrderKitting['id_order_kitting']);

        $res = $this->db->get()->num_rows();

        if($res > 0){
            return false;
        }

        return $idOrderKitting;
    }

    function retrieve($serial_number){
    	$this->db->select("itm.code as kd_barang, unique_code as kd_unik");
    	$this->db->from("item_receiving_details ird");
    	$this->db->join("items itm", "itm.id=ird.item_id", "left");
    	$this->db->where("unique_code", $serial_number);
    	return $this->db->get();
    }

    function retrieve_lp($serial_number){
        $this->db->select("itm.code as kd_barang, unique_code as kd_unik");
        $this->db->from("item_receiving_details ird");
        $this->db->join("items itm", "itm.id=ird.item_id", "left");
        $this->db->where("parent_code", $serial_number);
        return $this->db->get();
    }

    function post($data){

        $this->db->trans_start();

    	$id_barang 		= $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();
    	$id_loc 		= $this->get_id("id as loc_id", "name", $data["loc_name"], "locations")->row_array();
    	$id_user 		= $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
		$id_user_pick 	= $this->get_id("id as user_id", "user_name", $data["user_pick"], "users")->row_array();

        $this->db
            ->from('item_receiving_details ird')
            ->where('unique_code', $data['kd_unik']);

        $id_receiving= $this->db->get();
        if($id_receiving->num_rows()>0){
            $st_single = TRUE;
        }else{
            $st_single = FALSE;
        }
   	
        $this->db->set("location_id", $id_loc["loc_id"]);
        $this->db->set("putaway_time", $data["putaway_time"]);
    	$this->db->set("user_id_putaway", $id_user["user_id"]);

        if($st_single){
    	   $this->db->where("unique_code", $data["kd_unik"]);
        }else{
           $this->db->where("parent_code", $data["kd_unik"]);
        }
    	$this->db->update("item_receiving_details");

        $data_insert	= array(
									"unique_code"		=> $data["kd_unik"],
									"loc_id_old"	=> 100,
									"user_id_pick"	=> $id_user_pick["user_id"],
									"pick_time"		=> $data["pick_time"],
									"loc_id_new"	=> $id_loc["loc_id"],
									"put_time"		=> $data["putaway_time"],
									"user_id_put"	=> $id_user["user_id"],
									"process_name"	=> "PUTAWAY"
									);
        $this->db->insert("transfers", $data_insert);

        $this->db->trans_complete();

        return $this->db->trans_status();

    }

  

    function post_cancel($data){
        $id_barang = $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();
        $this->db->set("location_id", 100);
        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->where("item_id", $id_barang["id_barang"]);
        $this->db->update("item_receiving_details");
		
		$this->db->where("unique_code", $data["kd_unik"]);
		$this->db->where("process_name", "PUTAWAY");
		$this->db->delete("transfers");
    }

    function get_location($location){
        $avail_location = array("1", "3", "4"); // Putaway, put n pick, QC
        $this->db->select("id as loc_id, name as loc_name, location_category_id as loc_category_id, location_type_id as loc_type_id, location_area_id as loc_area_id");
        $this->db->from("locations");
        $this->db->where("name", $location);
        $this->db->where_in("location_type_id", $avail_location);
        return $this->db->get();
    }

    function get_location_serial($location, $serial_number){
        $type   = $this->get_id("location_type_id as loc_type_id", "name", $location, "locations")->row_array();
        $id_qc  = $this->get_id("qc_id as id_qc", "unique_code", $serial_number, "item_receiving_details");
        $id_qc_lp  = $this->get_id("qc_id as id_qc", "unique_code", $serial_number, "item_receiving_details");

        if($id_qc->num_rows() > 0){
           $id_qc = $id_qc->row_array();
        }else{
           $id_qc = $id_qc_lp->row_array();
        }
        if(($type["loc_type_id"] == "1" || $type["loc_type_id"] == "3") && $id_qc["id_qc"] == 1){ // item good
            return true;
        } elseif ($type["loc_type_id"] == "4" && $id_qc["id_qc"] == 2) { // item QC
            return true;
        } elseif ($type["loc_type_id"] == "5" && $id_qc["id_qc"] == 3) { // item NG
            return true;
        } else{
            return false;
        }
        
    }

    function cek_kategori($location, $serial_number){
        $this->db->select('category_id as id_kategori');
        $this->db->from('items itm');
        $this->db->join('item_receiving_details ird','ird.item_id=itm.id','left');
        $this->db->where('ird.unique_code',$serial_number);
        $id_kategori = $this->db->get();
        if($id_kategori->num_rows()>0){
            $id_kategori = $id_kategori -> row_array();
        }else{
            $this->db->select('category_id as id_kategori');
            $this->db->from('items itm');
            $this->db->join('item_receiving_details ird','ird.item_id=itm.id','left');
            $this->db->where('ird.parent_code',$serial_number);
            $id_kategori = $this->db->get()->row_array();
        }

        $id_loc_kategori  = $this->get_id("location_category_id as loc_category_id", "name", $location, "locations")->row_array();
        
        if($id_kategori['id_kategori'] == $id_loc_kategori['loc_category_id']){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
<?php
class packing_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

	public function getList($post){
        $result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'pl_id',
			1 => '',
			2 => 'pl_name',
			3 => 'sort_date',
            4 => 'remark',
			5 => 'nama_status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select('DISTINCT(p.pl_id) as pl_id')
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('sources s','outb.destination_id=s.source_id','left')
			->join('destinations d','outb.destination_id=d.destination_id','left')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('status st','st.status_id=p.status_id','left')
			->where('(p.status_id=8 OR p.status_id=9)', NULL)
			->group_by('p.pl_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get();
		$totalData = $row->num_rows();

		$this->db->select([
			"p.pl_id", "p.name as pl_name","p.date as sort_date", "TO_CHAR(p.date, 'DD/MM/YYYY') as pl_date","COALESCE(remark, '') as remark","p.status_id as id_status_picking","st.name as nama_status",
			"s.source_name as source","d.destination_name as destination"
		])
		->group_by('st.name, p.name, p.date, p.remark, p.status_id, d.destination_name, s.source_name')
		->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
		->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);
		
		// qty scanned
		//

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<span class="pl_name">'.$row[$i]['pl_name'].'</span>';
			if($row[$i]['destination'] != null) {
				$nested[] = $row[$i]['destination'];	
			} else {
				$nested[] = $row[$i]['source'];
			}
			$nested[] = $row[$i]['pl_date'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['nama_status'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    public function getOutboundList($param=array()){
        $result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_outbound',
			1 => '',
			2 => 'kd_outbound',
			3 => 'DestinationName',
            4 => 'DestinationAddressL1'
		);

		$this->db->start_cache();

		if(!empty($param['pl_id'])){
			$this->db->where('pl_id', $param['pl_id']);
		}

		$this->db
			->select('DISTINCT(outb.id) as id_outbound')
			->from('outbound outb')
			->join('picking_list_outbound plo','plo.id_outbound=outb.id')
			->group_by('outb.id');

		$this->db->stop_cache();

		$row = $this->db->get();
		$totalData = $row->num_rows();

		$this->db
			->select('outb.code as kd_outbound, outb.destination_name as "DestinationName", outb.address as "DestinationAddressL1"')
			->group_by('outb.id, outb.code, outb.destination_name, outb.address_1')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id_outbound'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<span class="kd_outbound" data-id="'.$row[$i]['id_outbound'].'">'.$row[$i]['kd_outbound'].'</span>';
			$nested[] = '<span class="DestinationName">'.$row[$i]['DestinationName'].'</span>';
			$nested[] = '<span class="DestinationAddressL1">'.$row[$i]['DestinationAddressL1'].'</span>';

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    public function getRecList($param=array()){
        $result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'itm.id',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
            4 => 'jumlah_barang',
            5 => 'actual_picked_qty',
            6 => 'status'
		);

		$this->db->start_cache();

		$t_obp = "SELECT
					sum(qty) as qty,
					item_id
				FROM
					outbound_item_picked
				WHERE
					outbound_id = ".$param['id_outbound']."
				GROUP BY
					item_id";

		$tbl_2 = "(SELECT
					id_outbound, itm.id as item_id, sum(outbitm.qty*COALESCE(kititm.qty,1)) as qty
					FROM outbound_item outbitm
					LEFT JOIN kittings kit ON kit.item_id=outbitm.item_id 
					LEFT JOIN kitting_items kititm ON kititm.kitting_id=kit.id
					LEFT JOIN items itm ON itm.id=COALESCE(kititm.item_id, outbitm.item_id)
					GROUP BY itm.id, outbitm.id_outbound)";

		$this->db
			->select('DISTINCT(itm.id) as item_id ')
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join($tbl_2.' as outbitm','outbitm.id_outbound=outb.id','left')
			->join('picking_qty pq','pq.picking_id=p.pl_id AND pq.item_id=outbitm.item_id','left')
			->join('items itm','itm.id=outbitm.item_id','left')
			->join("($t_obp) as t_obp",'t_obp.item_id=pq.item_id','left')
			->where('p.pl_id', $param['pl_id'])
			->where('outb.id', $param['id_outbound'])
			->where('pq.qty <>', 0)
			->where('pq.qty IS NOT NULL', NULL)
			->group_by('itm.id');

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->select(['p.pl_id','itm.code as kd_barang','itm.name as nama_barang','sum(outbitm.qty) as jumlah_barang',"COALESCE(sum(t_obp.qty),0) as actual_picked_qty",
				"(CASE WHEN sum(outbitm.qty) >= sum(t_obp.qty) THEN 'Scanned' ELSE 'In Progress' END) as status"])
			->group_by('p.pl_id, itm.code, itm.name, outbitm.qty, t_obp.qty')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
        $scanned = $totalFiltered>0?1:0;
        $packing_numbers='';
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = '';
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_barang'];
			$nested[] = $row[$i]['actual_picked_qty'];
			$nested[] = $row[$i]['status'];

            if(!$row[$i]['actual_picked_qty']>=$row[$i]['jumlah_barang']){
                $scanned=0;
            }

			$data[] = $nested;
		}

        $this->load->library('enc');

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data,
            "scanned"         => $scanned,
            "enc"             => $this->enc->encode($param['id_outbound'])
		);

		return $result;
	}

    public function getOutboundPack($param=array()){
        $this->db
        	->select('
        		outb.id as id_outbound, outb.code as kd_outbound, destination_name as "DestinationName", outb.address as "DestinationAddress", address_1 as "DestinationAddressL1", address_2 as "DestinationAddressL2", address_3 as "DestinationAddressL3", itm.name as nama_barang, itm.code as kd_barang, packing_number, inc, outbitm.qty
        	')
        	->from('outbound outb')
        	->join('outbound_item_receiving_details oird','oird.id_outbound=outb.id','left')
        	// ->join('item_receiving_details ird','ird.id=oird.id_item_receiving_details','left')
        	// ->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
        	->join("outbound_item outbitm",'outbitm.id_outbound=outb.id')
        	->join('items itm','itm.id=outbitm.item_id','left')
        	// ->join('picking_list_outbound plo','plo.id_outbound=outb.id','left')
        	// ->join('picking_qty pq','pq.picking_id=plo.pl_id AND pq.item_id=itm.id','left')
        	->where('outb.id', $param['id_outbound'])
        	->group_by('outb.id, outb.code, outb.destination_name, outb.address, outb.address_1, outb.address_2, address_3, itm.name, itm.code, packing_number, inc, outbitm.qty');

        $result = $this->db->get();

        return $result;
    }

    public function get_packing_number($id_outbound=0){
        //$sql = 'SELECT group_concat(concat(packing_number,"-",inc,"/x") separator ", ") as pc_string,packing_number,count(inc) as inc from outbound_receiving_barang where id_outbound='.$id_outbound.' group by packing_number';
        $sql = 'SELECT packing_number,max(inc) as inc from outbound_item_receiving_details where id_outbound='.$id_outbound.' group by packing_number';
        $result = $this->db->query($sql);
        return $result;
    }

    public function get_pc_code(){
        $this->db->select('pre_code_pc,inc_pc');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_pc.date('ymd').str_pad($result->inc_pc,4,0,STR_PAD_LEFT);
    }

    public function add_pc_code(){
        $this->db->set('inc_pc',"inc_pc+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

    public function get_picking_id($pl_name=''){
        $result = $this->db->get_where('pickings',array('name'=>$pl_name));
        return $result;
    }

    public function check_kd_unik($kd_unik=''){
        $result = $this->db->get_where('picking_recomendation',array('unique_code'=>$kd_unik));
        return $result->num_rows();
    }

    public function checkQty($id_outbound='', $id_picking='', $kd_unik=''){

    	$t_obp = "SELECT
                    sum(qty) as qty,
                    item_id
                FROM
                    outbound_item_picked opb
                WHERE
                    outbound_id = $id_outbound
                AND 
                    item_id IN (SELECT item_id FROM item_receiving_details WHERE unique_code='$kd_unik' or parent_code='$kd_unik' GROUP BY item_id)
                GROUP BY
                    item_id";

		$tbl_2 = "(SELECT
					id_outbound, itm.id as item_id, sum(outbitm.qty*COALESCE(kititm.qty,1)) as qty
					FROM outbound_item outbitm
					LEFT JOIN kittings kit ON kit.item_id=outbitm.item_id 
					LEFT JOIN kitting_items kititm ON kititm.kitting_id=kit.id
					LEFT JOIN items itm ON itm.id=COALESCE(kititm.item_id, outbitm.item_id)
					GROUP BY itm.id, outbitm.id_outbound)";

        $this->db
        	->select([
        		"COALESCE(sum(outbitm.qty), 0) as doc_qty",
        		"COALESCE(sum(t_obp.qty), 0) as act_qty"
        	])
        	->from('pickings p')
        	->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
        	->join('outbound outb','outb.id=plo.id_outbound')
        	->join($tbl_2. ' as outbitm','outbitm.id_outbound=outb.id','left')
        	->join('picking_qty pq','pq.picking_id=p.pl_id AND pq.item_id=outbitm.item_id','left')
        	->join('items itm','itm.id=outbitm.item_id','left')
        	->join("($t_obp) as t_obp",'t_obp.item_id=pq.item_id','left')
        	->where('p.pl_id', $id_picking)
        	->where('outb.id', $id_outbound)
        	->where('pq.qty <>', 0)
        	->where('pq.qty IS NOT NULL', NULL)
        	->where("itm.id IN (SELECT item_id FROM item_receiving_details WHERE unique_code='$kd_unik' or parent_code='$kd_unik' GROUP BY item_id)",NULL)
        	->group_by('itm.id');

        $res = $this->db->get()->result_array();

        foreach ($res as $r) {
	        if($r['doc_qty'] > $r['act_qty']){
	            return true;
	        }
        }

        return false;
    }

    public function get_avail_qty($kd_unik='',$id_picking=''){
    	//=========get maks available qty from scaned picking=======
    	$this->db->select(["COALESCE((qty-COALESCE(picked_qty,0)),0) as avail_qty"]);
    	$this->db->from('picking_recomendation pr');
    	$this->db->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id');
    	$this->db->where('pr.picking_id',$id_picking);
    	$this->db->where('pr.unique_code',$kd_unik);
    	$this->db->or_where('pr.picking_id',$id_picking);
    	$this->db->or_where('ird.parent_code',$kd_unik);
    	$hasil = $this->db->get();

    	if($hasil->num_rows()>0){
    		$avail_qty=0;
    		foreach ($hasil->result_array() as $h) {
    			$avail_qty += $h['avail_qty'];
    		}
    		return $avail_qty;
    	}else{
    		return 0;
    	}
    	
    	//-------------------end get maks------------------------------
    }
    public function update_location_outbound($kd_unik){
    	$data_where = array(
    		'unique_code'=>$kd_unik,
    		'first_qty' => 1);
    	$data_update = array('location_id'=>'101');
    	$this->db->where($data_where);
    	$this->db->update('item_receiving_details',$data_update);
    }

    public function update_picking_recomendation($kd_unik='',$id_outbound='',$id_picking=''){

    	$sql = "UPDATE 
    				picking_recomendation
				SET 
					st_packing = 1,
					picked_qty = qty
				WHERE
					unique_code IN (SELECT unique_code FROM item_receiving_details WHERE unique_code='$kd_unik' or parent_code='$kd_unik' GROUP BY unique_code)
				AND picking_id = $id_picking";

		$result = $this->db->query($sql);

		$deleteOutboundItemPicked = "DELETE FROM outbound_item_picked WHERE unique_code IN (SELECT unique_code FROM item_receiving_details WHERE unique_code='$kd_unik' or parent_code='$kd_unik' GROUP BY unique_code) and outbound_id=$id_outbound";

		$this->db->query($deleteOutboundItemPicked);

    	$insertOutboundItemPicked = "
    			INSERT INTO 
    				outbound_item_picked(outbound_id,item_id,unique_code,qty)
    			SELECT
    				$id_outbound,item_id,unique_code,SUM(qty)
    			FROM
    				picking_recomendation
				WHERE
					unique_code IN (SELECT unique_code FROM item_receiving_details WHERE unique_code='$kd_unik' or parent_code='$kd_unik' GROUP BY unique_code)
				AND picking_id = $id_picking
				GROUP BY item_id, unique_code";

		$this->db->query($insertOutboundItemPicked);

    }

    public function updatePicking($picking){
    	$sql = "SELECT 
					COALESCE((SELECT sum(picked_qty) FROM picking_recomendation WHERE picking_id=p.pl_id), 0) as qty_picked,
					COALESCE((SELECT sum(qty) FROM picking_recomendation WHERE picking_id=p.pl_id), 0) as qty
				FROM pickings p
				where p.pl_id=$picking";

    	$data = $this->db->query($sql)->row_array();

    	if($data['qty'] === $data['qty_picked']){
    		$this->db->set('status_id', 10);
    	}else{
    		$this->db->set('status_id', 9);
    	}

		$this->db
			->where('pl_id', $picking)
			->update('pickings');
    }

    public function update_receiving_barang($kd_unik=''){
        $result = $this->db->update('item_receiving_details',array('st_shipping'=>1),array('unique_code'=>$kd_unik));
        return $result;
    }

    public function get_receiving_barang($kd_unik=''){
        $result = $this->db->get_where('item_receiving_details',array('unique_code'=>$kd_unik));
        return ($result->num_rows()>0?$result->row()->id:0);
    }

    public function get_picking($id_outbound){
    	$result = $this->db->get_where('picking_list_outbound',array('id_outbound' =>$id_outbound));
    	return $result->row()->pl_id;
    }

    public function insert_outbound_receiving($id_outbound,$id_picking,$pc_code,$inc,$kd_unik){

    	$delOutboundReceiving = "DELETE FROM outbound_item_receiving_details WHERE id_item_receiving_details IN (
				SELECT id FROM item_receiving_details WHERE unique_code IN (
	    			SELECT
	    				unique_code
	    			FROM
	    				picking_recomendation
					WHERE
						unique_code IN (SELECT unique_code FROM item_receiving_details WHERE unique_code='$kd_unik' or parent_code='$kd_unik' GROUP BY unique_code)
					AND picking_id = $id_picking
				)
			) and id_outbound=$id_outbound";

		$this->db->query($delOutboundReceiving);

		$insOutboundReceiving = "
			INSERT INTO outbound_item_receiving_details(id_outbound,id_item_receiving_details,packing_number,inc)
			SELECT $id_outbound,id,'$pc_code',$inc FROM item_receiving_details WHERE unique_code IN (
    			SELECT
    				unique_code
    			FROM
    				picking_recomendation
				WHERE
					unique_code IN (SELECT unique_code FROM item_receiving_details WHERE unique_code='$kd_unik' or parent_code='$kd_unik' GROUP BY unique_code)
				AND picking_id = $id_picking
			)
		";

		$this->db->query($insOutboundReceiving);

    }

    public function get_qty_print($data){
    	$sql = "select COALESCE(max(inc),0) as qty_print from outbound_item_receiving_details where id_outbound = ".$data['id_outbound'];
    	$query = $this->db->query($sql);
    	return $query->row()->qty_print;
    }

    public function get_data_label($data){

    	$sql = "SELECT
					o.code as kd_outbound, o.date as tanggal_outbound, o.destination_name as \"DestinationName\", o.phone as \"Phone\", o.note as \"ShippingNote\", o.address as \"DestinationAddress\", o.address_1 as \"DestinationAddressL1\", address_2 as \"DestinationAddressL2\", address_3 as \"DestinationAddressL3\", orb_temp.packing_number, 
					TO_CHAR(o.delivery_date, 'DD-MM-YYYY') as delivery_date, 
					m_priority_name as priority,
					CONCAT((
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(qty)* CAST(itm.weight as float8)),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=o.id group by itm.weight
						) as tbl
					),' KG') as weight,
					CONCAT((
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=o.id group by itm.height, itm.width, itm.length
						) as tbl
					),' M3') as volume,
					d.destination_city, p.name as picking_code, o.note, o.ekspedisi
				FROM
					outbound o
				LEFT JOIN(
					SELECT
						packing_number,
						id_outbound
					FROM
						outbound_item_receiving_details
					WHERE
						id_outbound = ".$data['id_outbound']."
					GROUP BY
						id_outbound, packing_number
				)orb_temp ON orb_temp.id_outbound = o.id
				LEFT JOIN m_priority mp ON mp.m_priority_id=o.m_priority_id
				LEFT JOIN destinations d ON d.destination_id=o.destination_id
				JOIN picking_list_outbound plo on o.id=plo.id_outbound
				JOIN pickings p on p.pl_id=plo.pl_id
				WHERE
					o.id = ".$data['id_outbound'];
		$query = $this->db->query($sql);
		return $query->row();

    }

    public function update_staging_outbound(){
    	$this->load_model('api_staging_model');
    	$sql = "
    			SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					count(kd_unik)AS qty,
					mx_inc
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						id_outbound
					FROM
						outbound_receiving_barang orb
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				GROUP BY
					kd_outbound,
					kd_barang,
					inc";
		$sql = "
				SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					mx_inc,
					sum(pr.qty) as qty,
					pl_finish
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				left join picking_list_outbound plo on plo.id_outbound = o.id_outbound
				LEFT JOIN picking_list pl ON pl.pl_id=plo.pl_id 
				left join picking_recomendation pr on pr.kd_unik = rb.kd_unik and pr.id_picking = plo.pl_id
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						id_outbound
					FROM
						outbound_receiving_barang orb
					
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				
				GROUP BY
					kd_outbound,
					kd_barang,
					inc,
					o.id_staging";
		$query = $this->db->query($sql);
		$line=0;
		foreach ($query->result() as $row) {
			$line++;
			if($row->inc<10){
                $pad=0;
            }else{
                $pad='';
            }
            if($row->mx_inc<10){
                $pad2=0;
            }else{
                $pad2='';
            }
			$PackingNo = $row->packing_number.$pad.$row->inc.$pad2.$row->mx_inc;

			/*$this->fina->from('f_t_outbound_h');
			$this->fina->where('OutbondNo',$row->kd_outbound);
			$hasil = $this->fina->get();*/

			$data_where = array(
                'OutbondNo' => $row->kd_outbound);
            $hasil = $this->api_staging_model->_curl_process('get','f_t_outbound_h',array('where'=>$data_where));

			
			if($hasil->status=='001'){
				foreach ($hasil->data as $hasil_t) {
					# code...
					$DocEntry = $hasil_t->DocEntry;
				}
				
				
				/*$this->fina->from('f_t_outbound_l2');
				$this->fina->where($data_where);
				$cek_PackingNo = $this->fina->get();*/

				$data_where = array(
					'PackingNo' => $PackingNo,
					'SKU' => $row->kd_barang);
	            $cek_PackingNo = $this->api_staging_model->_curl_process('get','f_t_outbound_l2',array('where'=>$data_where));
				if($cek_PackingNo ->status == '001'){
					//$data_update = $cek_PackingNo->row_array();
					foreach ($cek_PackingNo->data as $data_t2) {
						# code...
					
						if($row->qty <> $data_t2->PackingQty){
							$data_update['PackingQty'] = $row->qty;
							$data_update['SyncStatus'] = 'N';
							/*$this->fina->where('PackingNo',$PackingNo);
							$this->fina->update('f_t_outbound_l2',$data_update);*/
							$data_where = array('PackingNo'=>$PackingNo);
							$this->api_staging_model->_curl_process('update','f_t_outbound_l2',array('where'=>$data_where,'data'=>$data_update));
						}
					}
				}else{
					$data_insert = array(
						'DocEntry' => $DocEntry,
						'LineNum'=>$line,
						'SKU'=>$row->kd_barang,
						'PackingQty' =>$row->qty,
						'PackingNo' => $PackingNo,
						'SyncStatus' => 'N',
						'ShippingDate' => $row->pl_finish
					);
					//$this->fina->insert('f_t_outbound_l2',$data_insert);
					$this->api_staging_model->_curl_process('insert','f_t_outbound_l2',array('data'=>$data_insert));

					$data_where = array(
		                'SKU' => $row->kd_barang);
		            //$cek_val = $this->fina->get_where('f_v_stok_h',$data_where);

	            	$cek_val = $this->api_staging_model->_curl_process('get','f_v_stok_h',array('where'=>$data_where));
		            if($cek_val->status=='001'){
		            	foreach ($cek_val->data as $data_cek) {
		            		# code...
		            	
			                $qty = $data_cek->Qty;
			                $new_qty = $qty-$row->qty;
			                $data_update = array(
			                    'Qty' => $new_qty,
			                    'SyncStatus'=> 'N');
			                /*$this->fina->where($data_where);
			                $this->fina->update('f_v_stok_h',$data_update);*/
			                $this->api_staging_model->_curl_process('update','f_v_stok_h',array('where'=>$data_where,'data'=>$data_update));
			            }
		            }else{
		                $data_insert = array(
		                    'SKU'=>$row->kd_barang,
		                    'Qty'=>$row->qty,
		                    'SyncStatus'=> 'N'
		                    );
		                //$this->fina->insert('f_v_stok_h',$data_insert);
		                $this->api_staging_model->_curl_process('insert','f_v_stok_h',array('data'=>$data_insert));
		            }
				}
			}
			# code...
		}
    }

    public function update_staging_outbound_by_id_outbound($id_outbound){
    	$this->load_model('api_staging_model');

		$sql ="
		 		SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					mx_inc,
					o.id_staging,
					sum(pr.qty) as qty,
					pl_finish
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				left join picking_list_outbound plo on plo.id_outbound = o.id_outbound
				LEFT JOIN picking_list pl ON pl.pl_id=plo.pl_id 
				left join picking_recomendation pr on pr.kd_unik = rb.kd_unik and pr.id_picking = plo.pl_id
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						id_outbound
					FROM
						outbound_receiving_barang orb
					WHERE
						orb.id_outbound = ".$id_outbound."
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				WHERE
					orb.id_outbound = ".$id_outbound."
				GROUP BY
					kd_outbound,
					kd_barang,
					inc,
					o.id_staging";
		$query = $this->db->query($sql);
		$line=0;
		foreach ($query->result() as $row) {
			$line++;
			if($row->inc<10){
                $pad=0;
            }else{
                $pad='';
            }
            if($row->mx_inc<10){
                $pad2=0;
            }else{
                $pad2='';
            }
			$PackingNo = $row->packing_number.$pad.$row->inc.$pad2.$row->mx_inc;

			$DocEntry = $row->id_staging;
			
			$data_where = array(
				'PackingNo' => $PackingNo,
				'SKU' => $row->kd_barang);
            $cek_PackingNo = $this->api_staging_model->_curl_process('get','f_t_outbound_l2',array('where'=>$data_where));
			if($cek_PackingNo ->status == '001'){
				foreach ($cek_PackingNo->data as $data_t2) {
					# code...
				
					if($row->qty <> $data_t2->PackingQty){
						$data_update['PackingQty'] = $row->qty;
						$data_update['SyncStatus'] = 'N';
						$data_where = array('PackingNo'=>$PackingNo);
						$this->api_staging_model->_curl_process('update','f_t_outbound_l2',array('where'=>$data_where,'data'=>$data_update));
					}
				}
			}else{
				$data_insert = array(
					'DocEntry' => $DocEntry,
					'LineNum'=>$line,
					'SKU'=>$row->kd_barang,
					'PackingQty' =>$row->qty,
					'PackingNo' => $PackingNo,
					'SyncStatus' => 'N',
					'ShippingDate' => $row->pl_finish
				);
				$this->api_staging_model->_curl_process('insert','f_t_outbound_l2',array('data'=>$data_insert));

				$data_where = array(
	                'SKU' => $row->kd_barang,
	                'WhsCode' => 'KRG');
				$cek_val = $this->db->get_where('staging_stok_h',$data_where);
	            if($cek_val->num_rows > 0){
	            	$data_cek = $cek_val->row();
	                $qty = $data_cek->Qty;
	                $new_qty = $qty-$row->qty;
	                $data_update = array(
	                    'Qty' => $new_qty,
	                    'SyncStatus'=> 'N');
	                $this->db->where($data_where);
	                $this->db->update('staging_stok_h',$data_update);
	                $this->api_staging_model->_curl_process('update','f_v_stok_h',array('where'=>$data_where,'data'=>$data_update));
		            
	            }else{
	                $data_insert = array(
	                    'SKU'=>$row->kd_barang,
	                    'Qty'=>$row->qty,
	                    'SyncStatus'=> 'N',
	                    'WhsCode' => 'KRG'
	                    );
	                $this->db->insert('staging_stok_h',$data_insert);
	                $this->api_staging_model->_curl_process('insert','f_v_stok_h',array('data'=>$data_insert));
	            }
			}
		}
    }

    public function check_pc_code($post=array()){

    	$this->db
    		->select('count(*) as total')
    		->from('outbound_item_receiving_details')
    		->where('id_outbound', $post['id_outbound'])
    		->where('packing_number', $post['pc_code'])
    		->where('inc', $post['inc']);

    	$row = $this->db->get()->row_array();

    	return $row;
    }

    public function update_staging_outbound_by_id_outbound_min(){
    	$this->load_model('api_staging_model');
    	

		$sql ="
		 		SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					mx_inc,
					o.id_staging,
					sum(pr.qty) as qty,
					pl_finish
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				left join picking_list_outbound plo on plo.id_outbound = o.id_outbound
				LEFT JOIN picking_list pl ON pl.pl_id=plo.pl_id 
				left join picking_recomendation pr on pr.kd_unik = rb.kd_unik and pr.id_picking = plo.pl_id
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						o.id_outbound
					FROM
						outbound_receiving_barang orb
					left join outbound o on o.id_outbound = orb.id_outbound
					WHERE
						o.id_staging > 1221
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				WHERE
					o.id_staging > 1221
				GROUP BY
					kd_outbound,
					kd_barang,
					inc,
					o.id_staging";
		$query = $this->db->query($sql);
		$line=0;
		foreach ($query->result() as $row) {
			$line++;
			if($row->inc<10){
                $pad=0;
            }else{
                $pad='';
            }
            if($row->mx_inc<10){
                $pad2=0;
            }else{
                $pad2='';
            }
			$PackingNo = $row->packing_number.$pad.$row->inc.$pad2.$row->mx_inc;

			$DocEntry = $row->id_staging;


			
			$data_where = array(
				'PackingNo' => $PackingNo,
				'SKU' => $row->kd_barang);
            $cek_PackingNo = $this->api_staging_model->_curl_process('get','f_t_outbound_l2',array('where'=>$data_where));
			if($cek_PackingNo ->status == '001'){
				foreach ($cek_PackingNo->data as $data_t2) {
					# code...
				
					if($row->qty <> $data_t2->PackingQty){
						$data_update['PackingQty'] = $row->qty;
						$data_update['SyncStatus'] = 'N';
						$data_where = array('PackingNo'=>$PackingNo);
						$this->api_staging_model->_curl_process('update','f_t_outbound_l2',array('where'=>$data_where,'data'=>$data_update));
						echo 'Update - '.$DocEntry.'<br/>';
						echo 'No Packing - '.$PackingNo.'<br/>';
					}
				}
			}else{
				$data_insert = array(
					'DocEntry' => $DocEntry,
					'LineNum'=>$line,
					'SKU'=>$row->kd_barang,
					'PackingQty' =>$row->qty,
					'PackingNo' => $PackingNo,
					'SyncStatus' => 'N',
					'ShippingDate' => $row->pl_finish
				);
				
				$this->api_staging_model->_curl_process('insert','f_t_outbound_l2',array('data'=>$data_insert));
				echo 'insert - '.$DocEntry.'<br/>';
				echo 'No Packing - '.$PackingNo.'<br/>';
				
				$data_where = array(
	                'SKU' => $row->kd_barang,
	                'WhsCode' => 'KRG');
				$cek_val = $this->db->get_where('staging_stok_h',$data_where);
	            if($cek_val->num_rows > 0){
	            	$data_cek = $cek_val->row();
	                $qty = $data_cek->Qty;
	                $new_qty = $qty-$row->qty;
	                $data_update = array(
	                    'Qty' => $new_qty,
	                    'SyncStatus'=> 'N');
	                $this->db->where($data_where);
	                $this->db->update('staging_stok_h',$data_update);
	                $this->api_staging_model->_curl_process('update','f_v_stok_h',array('where'=>$data_where,'data'=>$data_update));
		            
	            }else{
	                $data_insert = array(
	                    'SKU'=>$row->kd_barang,
	                    'Qty'=>$row->qty,
	                    'SyncStatus'=> 'N',
	                    'WhsCode' => 'KRG'
	                    );
	                $this->db->insert('staging_stok_h',$data_insert);
	                $this->api_staging_model->_curl_process('insert','f_v_stok_h',array('data'=>$data_insert));
	            }
			}

			echo $DocEntry.'<br/>';
			echo $PackingNo.'<br/>';
		}
    }

    public function get_item_id($kd_unik,$idPicking){

    	$this->db
    		->select('ir.item_id as id_barang')
    		->from('item_receiving_details ird')
    		->join('item_receiving ir','ir.id=ird.item_receiving_id')
    		->join('picking_recomendation pr','pr.unique_code=ird.unique_code')
    		->join('pr.picking_id',$idPicking)
    		->where('ird.unique_code', $kd_unik)
    		->or_where('ird.parent_code',$kd_unik);

    	$res = $this->db->get()->result_array();

    	return $res->id_barang;
    }

    public function closePacking($idPicking=''){
    	$this->db->select('sum(qty) as doc_qty, sum(picked_qty) as pack_qty');
    	$this->db->from('picking_recomendation');
    	$this->db->where('picking_id',$idPicking);
    	$this->db->group_by("picking_id");

    	$packing = $this->db->get()->row();

    	if($packing->doc_qty == $packing->pack_qty){

    		$this->db->set('status_id',10);
    		$this->db->where('pl_id', $idPicking);
    		$this->db->update('pickings');

    	}
    }

}

?>


<?php

class Api_bin_transfer_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function getItem($binTransferCode='',$itemCode=''){

        $result = array();

        if($binTransferCode){

            $this->db
                ->select([
                	"COALESCE(CONCAT(itm.code,' - ', itm.name),'-') as item_name",
                	"COALESCE(SUM(bti.quantity),0) as order_qty",
                	"unt.code as unit_code",
                	"COALESCE((SELECT COALESCE(SUM(quantity),0) FROM bin_transfer_scanned where bin_transfer_id=bt.bin_transfer_id and item_id=bti.item_id GROUP BY item_id),0) as scanned_qty"
                ])
                ->from('bin_transfer bt')
                ->join('bin_transfer_item bti','bti.bin_transfer_id=bt.bin_transfer_id')
                ->join('items itm','itm.id=bti.item_id')
                ->join('units unt','unt.id=bti.unit_id')
                ->where('bt.bin_transfer_code',$binTransferCode)
                ->group_by('itm.code, itm.name, unt.code, bti.item_id, bt.bin_transfer_id');

			if(!empty($itemCode)){
				$this->db
					->select('unt.id as unit_id')
					->where('itm.code',$itemCode)
					->group_by('unt.id');
			}

            $result = $this->db->get();

        }

        return $result;

    }

    function getItemScannedDetail($binTransferCode='',$itemCode=''){

    	$result = array();

    	if(!empty($binTransferCode) || !empty($itemCode)){

    		$this->db
    			->select(['ird.unique_code',"COALESCE(bts.quantity,0) as qty",'from_loc.name as pick_location', 'to_loc.name as put_location'])
    			->from('bin_transfer bt')
    			->join('bin_transfer_scanned bts','bts.bin_transfer_id=bt.bin_transfer_id')
    			->join('item_receiving_details ird','ird.id=bts.item_receiving_detail_id')
    			->join('locations from_loc','from_loc.id=bts.from_location_id')
    			->join('locations to_loc','to_loc.id=bts.to_location_id')
    			->join('items itm','itm.id=bts.item_id')
    			->where('bt.bin_transfer_code',$binTransferCode)
    			->where('itm.code',$itemCode);

    		$result = $this->db->get()->result_array();

    	}

    	return $result;
    }

    function get_location($location="0"){

        $this->db->select("id as loc_id, name as loc_name, location_type_id as loc_type_id, location_area_id as loc_area_id, location_category_id as loc_category_id, loc_type");
        $this->db->from("locations");
        $this->db->where("name", $location);
        $this->db->where_in("location_type_id", array(1,2,3));
        return $this->db->get();

    }

    function get_serial($serialNumber=""){

        $result = array();

        if($serialNumber){

            $this->db->select("*, ird.id as item_receiving_detail_id");
            $this->db->from("item_receiving_details ird");
            $this->db->join("locations loc", "loc.id = ird.location_id", "left");
            $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
            $this->db->join('receivings r','r.id=ir.receiving_id','left');
            $this->db->where('(ird.putaway_time IS NOT NULL or (ird.location_id=107 and r.st_receiving=1) )', NULL);
            $this->db->where('(loc.location_type_id NOT IN (6) OR loc.location_type_id IS NULL)',NULL);
            $this->db->where("(ird.unique_code='".$serialNumber."' or ird.parent_code='".$serialNumber."')",NULL);
            $this->db->where("ird.last_qty > 0",NULL);
            $this->db->order_by('ird.id','ASC');

            $result =  $this->db->get();

        }

        return $result;

    }

    function validateSerial($params=array()){

        $result = array();

        if($params){

            $this->db
                ->select('*, ird.id as item_receiving_detail_id, ird.location_id, bti.unit_id as unit_id_pick, loc.name as location_name')
                ->from('item_receiving_details ird')
                ->join('bin_transfer_item bti','bti.item_id=ird.item_id')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('bin_transfer bt','bt.bin_transfer_id=bti.bin_transfer_id')
                ->join('items itm','itm.id=ird.item_id')
                ->where("(ird.unique_code='".$params['serial_number']."' or parent_code='".$params['serial_number']."')",NULL)
                ->where('bt.bin_transfer_code',$params['bin_transfer_code']);

            $result = $this->db->get()->row_array();

        }

        return $result;

    }

    function getPickingRecomendation($params=array()){

        $data = array();

        if($params){

            $uniqueCode = $params['serial_number'];

            $data = array(
                'unique_code'   => '',
                'location'      => '',
                'status'        => false
            );

            if($uniqueCode != ""){

                $validateSN = $this->get_serial($uniqueCode)->row_array();

                if($validateSN){

                    if($validateSN['unique_code'] == $uniqueCode){

                        $this->db->start_cache();

                        $this->db
                            ->select(["itm.code","ird.unique_code","ird.parent_code","ird.kd_batch","loc.name as location_name","rfid_tag","TO_CHAR( ( CASE WHEN itm.shipment_id=1 THEN min(tgl_in) ELSE min(tgl_exp) END ), 'YYYY-MM' )  as date"])
                            ->from('item_receiving_details ird')
                            ->join('locations loc','loc.id=ird.location_id')
                            ->join('items itm','itm.id=ird.item_id')
                            ->where_not_in('loc.id',[100,102,101,103,104,106])
                            ->where('(loc.location_type_id NOT IN (4,1,6) or loc.location_type_id IS NULL)',NULL)
                            ->where("ird.item_id IN (SELECT item_id FROM item_receiving_details where unique_code='$uniqueCode' or parent_code='$uniqueCode' or kd_batch='$uniqueCode')",NULL)
                            ->where('last_qty >',0)
                            // ->where('loc.id <> itm.pick_face_location_id',NULL)
                            ->where("ird.id NOT IN (SELECT item_receiving_detail_id FROM bin_transfer_scanned bts JOIN bin_transfer bt ON bt.bin_transfer_id=bts.bin_transfer_id WHERE bt.bin_transfer_code='".$params['bin_transfer_code']."')",NULL)
                            ->group_by('itm.code, ird.unique_code,loc.name, itm.shipment_id,rfid_tag, ird.parent_code, ird.kd_batch')
                            ->order_by('date');

                        $this->db->stop_cache();

                        $this->db->limit(1);
                        $dr = $this->db->get()->row_array();

                        $tbl = $this->db->get_compiled_select();

                        $this->db->flush_cache();

                        $sql = "SELECT * FROM ($tbl) as tbl WHERE (unique_code='$uniqueCode' or parent_code='$uniqueCode' or kd_batch='$uniqueCode') and date='".$dr['date']."' LIMIT 5";

                        $d = $this->db->query($sql)->row_array();

                        if($d){

                            $data = array(
                                'unique_code'   => $d['unique_code'],
                                'status'        => true
                            );

                        }else{

                            $sql = "SELECT * FROM ($tbl) as tbl WHERE date='".$dr['date']."' LIMIT 5";
                            $ds = $this->db->query($sql)->result_array();

                            $data = array(
                                'data'          => $ds,
                                'status'        => false
                            );

                        }

                    }else{

                        $this->db->start_cache();

                        $this->db
                            ->select(["itm.code","ird.parent_code as unique_code","ird.parent_code","ird.kd_batch","loc.name as location_name","rfid_tag","TO_CHAR( ( CASE WHEN itm.shipment_id=1 THEN min(tgl_in) ELSE min(tgl_exp) END ), 'YYYY-MM' )  as date"])
                            ->from('item_receiving_details ird')
                            ->join('locations loc','loc.id=ird.location_id')
                            ->join('items itm','itm.id=ird.item_id')
                            ->where_not_in('loc.id',[100,102,101,103,104,106])
                            ->where('(loc.location_type_id NOT IN (4,1,6) or loc.location_type_id IS NULL)',NULL)
                            ->where("ird.item_id IN (SELECT item_id FROM item_receiving_details where unique_code='$uniqueCode' or parent_code='$uniqueCode' or kd_batch='$uniqueCode')",NULL)
                            ->where('last_qty >',0)
                            ->group_by('itm.code, loc.name, itm.shipment_id,rfid_tag, ird.parent_code, ird.kd_batch')
                            ->order_by('date');

                        $this->db->stop_cache();

                        $this->db->limit(1);
                        $dr = $this->db->get()->row_array();

                        $tbl = $this->db->get_compiled_select();

                        $this->db->flush_cache();

                        $sql = "SELECT * FROM ($tbl) as tbl WHERE (parent_code='$uniqueCode' or kd_batch='$uniqueCode') and date='".$dr['date']."' LIMIT 5";

                        $d = $this->db->query($sql)->row_array();

                        if($d){

                            $data = array(
                                'unique_code'   => $d['parent_code'],
                                'status'        => true
                            );

                        }else{

                            $sql = "SELECT * FROM ($tbl) as tbl WHERE date='".$dr['date']."' LIMIT 5";
                            $ds = $this->db->query($sql)->result_array();

                            $data = array(
                                'data'          => $ds,
                                'status'        => false
                            );

                        }

                    }

                }

            }

        }

        return $data;

    }

    function validateQty($params=array()){

        $result = array();

        if($params){

        	$dataDocument = $this->getItem($params['bin_transfer_code'],$params['item_code'])->row_array();
        	$dataItem = $this->validateSerial($params);

        	$stock = 0;
        	switch ($dataDocument['unit_id']) {
        		case $dataItem['unit_id']:
        			$stock = $dataItem['last_qty'];
        			break;

        		case $dataItem['convert_qty_unit_id_1']:
        			$stock = $dataItem['last_qty'] * (int)$dataItem['convert_qty_1'];
        			break;

        		case $dataItem['convert_qty_unit_id_2']:
        			$stock = $dataItem['last_qty'] * (int)$dataItem['convert_qty_2'];
        			break;

        		default:
        			# code...
        			break;
        	}

        	$result['document'] = $dataDocument;
        	$result['item'] = $dataItem;
        	$result['stock'] = $stock;

        	// $result = (($dataDocument['order_qty']-$dataDocument['scanned_qty']) - $stock >= 0 ) ? true : false;
        }

        return $result;

    }

    function post($params=array()){

        $result = false;

        if($params){

        	$user = $this->db->get_where('users',['user_name'=>$this->input->get_request_header('User', true)])->row_array();

        	$now = date('Y-m-d h:i:s');

            $binTransferScannedData = array(
                "bin_transfer_id"           => $params['data']['bin_transfer_id'],
                "item_receiving_detail_id"  => $params['data']['item_receiving_detail_id'],
                "quantity"                  => $params['data']['last_qty'],
                "to_location_id"            => $params['location_id'],
                "item_id"                   => $params['data']['item_id'],
                "unit_id"                   => $params['data']['unit_id_pick'],
                "put_user_id"			    => $user['id'],
                'put_time'                  => $now
            );

			// var_dump($binTransferScannedData);

            // $transferData = array(
            // 	"item_id"			=> $params['data']['item_id'],
            // 	"user_id_pick"		=> $user['id'],
            // 	"pick_time"			=> $now,
            // 	"user_id_put"		=> $user['id'],
            // 	"transfer_date"		=> $now,
            // 	"process_name"		=> "TRANSFER",
            // 	"unique_code"		=> $params['serial_number'],
            // 	"loc_id_new"		=> $params['location_id'],
            // 	"loc_id_old"		=> $params['data']["location_id"],
            // 	"item_receiving_detail_id"	=> $params['data']['item_receiving_detail_id']
            // );

			if(isset($params['data']['bin_transfer_id'])) {
				if($params['data']['bin_transfer_id'] == null || $params['data']['bin_transfer_id'] == '') {
					$irdDataFrom = $this->db->query("
						SELECT ird.unique_code as unique_code, itm.code as itemcode, ird.last_qty as qty, l.absentry as loc_from, w.name as whsfrom
						FROM item_receiving_details ird
						JOIN items itm ON ird.item_id = itm.id
						JOIN locations l ON ird.location_id = l.id
						JOIN location_areas larea ON l.location_area_id = larea.id
						JOIN warehouses w ON larea.warehouse_id = w.id
						WHERE ird.id = '".$params['data']['item_receiving_detail_id']."'
						")->row_array();
				}
			}

            $this->db->trans_start();

            $this->db
                ->where('bin_transfer_id',$params['data']['bin_transfer_id'])
                ->where('item_receiving_detail_id',$params['data']['item_receiving_detail_id'])
                ->update('bin_transfer_scanned',$binTransferScannedData);

            $this->db
                ->set('location_id',$params['location_id'])
                ->where('id',$params['data']['item_receiving_detail_id'])
                ->update('item_receiving_details');

            // $this->db->insert('transfers',$transferData);

            $this->db->trans_complete();
			if(isset($params['data']['bin_transfer_id'])) {
				if($params['data']['bin_transfer_id'] == null || $params['data']['bin_transfer_id'] == '') {
					$irdDataTo = $this->db->query("
						SELECT ird.unique_code as unique_code, itm.code as itemcode, ird.last_qty as qty, l.absentry as loc_to, w.name as whsto
						FROM item_receiving_details ird
						JOIN items itm ON ird.item_id = itm.id
						JOIN locations l ON ird.location_id = l.id
						JOIN location_areas larea ON l.location_area_id = larea.id
						JOIN warehouses w ON larea.warehouse_id = w.id
						WHERE ird.id = '".$params['data']['item_receiving_detail_id']."'
						")->row_array();


					$data_insert_1	= 	array(
						'DOCDATE'			=> date('Y-m-d'),
						'DOCDUEDATE'		=> date('Y-m-d'),
						'TAXDATE'			=> date('Y-m-d'),
						'BASEENTRY'			=> NULL,
						'BASELINE'			=> NULL,
						'BASETYPE'			=> NULL,
						'ITEMCODE'			=> $irdDataTo['itemcode'],
						'QTY'				=> $irdDataTo['qty'],
						'FROM_WAREHOUSE'	=> $irdDataFrom['whsfrom'],
						'TO_WAREHOUSE'		=> $irdDataTo['whsto'],
					);

					$data_insert_2	= 	array(
						'BASEENTRY'			=> NULL,
						'BASELINE'			=> NULL,
						'BINENTRY'			=> $irdDataFrom['loc_from'],
						'BINQTY'			=> $irdDataTo['qty']
					);

					$data_insert_3	= 	array(
						'BASEENTRY'			=> NULL,
						'BASELINE'			=> NULL,
						'BINENTRY'			=> $irdDataTo['loc_to'],
						'BINQTY'			=> $irdDataTo['qty']
					);

					$staging->insert('INVENTORY_TRANSFER', $data_insert_1);
					$staging->insert('ITBINFROM', $data_insert_2);
					$staging->insert('ITBINTO', $data_insert_3);
				}
			}

            $result = true;

        }

        return $result;

    }

    function checkParam($params=array()){

        $result = array(
            "key"       => "",
            "status"    => true
        );

        if($params){

            foreach ($params as $k => $v) {
                if(empty($v)){
                    $result = array(
                        "key"       => $k,
                        "status"    => false
                    );
                    break;
                }
            }
        }

        return $result;
    }

    function start($binTransferCode=""){

        if($binTransferCode){

            $this->db
                ->set("bin_transfer_start", "COALESCE(bin_transfer_start, '" . date("Y-m-d H:i:s") . "')", FALSE)
                ->set('status_id',5)
                ->where('bin_transfer_code',$binTransferCode)
                ->update('bin_transfer');
        }

    }

    function finish($binTransferCode=""){

        if($binTransferCode){

            $this->db
                ->set("bin_transfer_finish", "COALESCE(bin_transfer_finish, '" . date("Y-m-d H:i:s") . "')", FALSE)
                ->set('status_id',4)
                ->where('bin_transfer_code',$binTransferCode)
                ->update('bin_transfer');
        }

    }

    function checkFinish($binTransferCode=""){

        $result = true;

        if($binTransferCode){

            $data = $this->getItem($binTransferCode)->result_array();
            if($data){

                foreach ($data as $d) {
                    if($d['order_qty'] != $d['scanned_qty']){
                        $result = false;
                        break;
                    }
                }

            }

        }

        return $result;

    }

    function pickItem($params=array()){

        $result = false;

        if($params){

            $user = $this->db->get_where('users',['user_name'=>$this->input->get_request_header('User', true)])->row_array();

            $binTransferScannedData = array(
                "bin_transfer_id"           => $params['data']['bin_transfer_id'],
                "item_receiving_detail_id"  => $params['data']['item_receiving_detail_id'],
                "quantity"                  => $params['data']['last_qty'],
                "from_location_id"          => $params['data']["location_id"],
                "to_location_id"            => 103,
                "item_id"                   => $params['data']['item_id'],
                "unit_id"                   => $params['data']['unit_id_pick'],
                "pick_user_id"              => $user['id'],
                'pick_time'                  => date('Y-m-d h:i:s')
            );

            $this->db->trans_start();

            $this->db->insert('bin_transfer_scanned',$binTransferScannedData);

            $this->db
                ->set('location_id',103)
                ->where('id',$params['data']['item_receiving_detail_id'])
                ->update('item_receiving_details');

            $this->db->trans_complete();

            $result = true;

        }

        return $result;

    }

    function cancel($params=array()){

        $result = false;

        if($params){

            $this->db->trans_start();

            $this->db
                ->from('bin_transfer bt')
                ->join('bin_transfer_scanned bts','bts.bin_transfer_id=bt.bin_transfer_id')
                ->join('item_receiving_details ird','ird.id=bts.item_receiving_detail_id')
                ->where('bt.bin_transfer_code',$params['bin_transfer_code'])
                ->where_in('ird.unique_code',$params['serial_number']);

            $dataItem = $this->db->get()->result_array();

            if($dataItem){

                $dataItemReceivingDetail = array();

                $binTransferScannedId = array();

                foreach($dataItem as $d){

                    $dataItemReceivingDetail[] = array(
                        "id"    => $d['item_receiving_detail_id'],
                        "location_id"   => $d['from_location_id']
                    );

                    $binTransferScannedId[] = $d['bin_transfer_scanned_id'];

                }

                if($dataItemReceivingDetail){
                    $this->db->update_batch('item_receiving_details',$dataItemReceivingDetail,"id");
                }

                if($binTransferScannedId){
                    $this->db
                        ->where_in('bin_transfer_scanned_id',$binTransferScannedId)
                        ->delete('bin_transfer_scanned');
                }

                $this->db->trans_complete();

                $result = true;

            }

        }

        return $result;

    }

}

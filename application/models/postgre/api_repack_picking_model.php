<?php

class Api_repack_picking_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function loadDocument(){
    	$q = 	"SELECT 
					ro.id as id_order_kitting, ro.code as kd_order_kitting, ro.date as tanggal_order_kitting,
					ro.status_id as status_order_kitting, ro.user_id, ro.type as order_kitting_type_id,
					rro.qty AS total
				FROM 
					repack_orders ro
				LEFT JOIN  repacks_repack_orders rro
					ON rro.repack_order_id=ro.id
				WHERE
					ro.st_repack = 0
						AND
					(ro.status_id <> 3 OR ro.status_id <> 5)
				GROUP BY ro.id, rro.qty
				";
		$r = $this->db->query($q, false)->result_array();
		
		return $r;
    }
	
	function loadDetail($data = array()){
		$wh1 = "";
		$wh2 = "";
		for($i = 0; $i < count($data); $i++){
			if($data[$i]['order_kitting_type_id'] == 1){
				$wh1 .= "'". $data[$i]['id_order_kitting'] ."',";
			}else{
				$wh2 .= "'". $data[$i]['id_order_kitting'] ."',";
			}
		}
		$wh1 = ($wh1 == "") ? "" : rtrim($wh1, ", ");
		$wh2 = ($wh2 == "") ? "" : rtrim($wh2, ", ");

		$r = array();

		if($wh2 != ''){
			$q =	"SELECT
						rro.id,
						rro.repack_order_id as id_order_kitting,
						rro.repack_id as id_kitting,
						r.item_id as id_barang,
						itm.code as kd_barang,
						itm.name as nama_barang,
						rro.qty AS jumlah_kitting,
						COALESCE((
							SELECT 
								COALESCE(sum(qty),0)
							FROM
								repack_order_picking
							WHERE
								repack_order_id=rro.repack_order_id
							AND
								item_id=itm.id
							GROUP BY
								item_id
						),0) as jumlah_picking,
						rb.kd_all_location
					FROM
						repack_orders ro
					JOIN repacks_repack_orders rro ON ro.id=rro.repack_order_id
					JOIN repacks r ON r.id=rro.repack_id
					JOIN items itm ON itm.id=r.item_id
					LEFT JOIN (
						SELECT
							ir.item_id,
							string_agg(DISTINCT loc.name, ', ') as kd_all_location
						FROM
							item_receiving_details ird
						LEFT JOIN item_receiving ir
							ON ird.item_receiving_id=ir.id
						LEFT JOIN locations loc
							ON loc.id=ird.location_id
						WHERE
							ird.location_id not in (100,101,102,103,104,105,106)
						GROUP BY ir.item_id
					) as rb ON rb.item_id=r.item_id
					WHERE 
						ro.id in (". $wh2 .")
					GROUP BY itm.id, ro.id
					";

			$r = array_merge($r, $this->db->query($q, false)->result_array());
		}

		if($wh1 != ''){

			$q =	"SELECT
						rro.id,
						rro.repack_order_id as id_order_kitting,
						rro.repack_id as id_kitting,
						ri.item_id as id_barang,
						itm.code as kd_barang,
						itm.name as nama_barang,
						sum((ri.qty * rro.qty)) AS jumlah_kitting,
						COALESCE((
							SELECT 
								COALESCE(sum(qty),0)
							FROM
								repack_order_picking
							WHERE
								repack_order_id=rro.repack_order_id
							AND
								item_id=itm.id
							GROUP BY
								item_id
						),0) as jumlah_picking,
						rb.kd_all_location
					FROM
						repack_orders ro
					JOIN repacks_repack_orders rro ON rro.repack_order_id=ro.id
					JOIN repacks r ON r.id=rro.repack_id
					JOIN repack_items ri ON ri.repack_id=r.id
					JOIN items itm ON itm.id=ri.item_id
					LEFT JOIN (
						SELECT
							ir.item_id,
							string_agg(DISTINCT loc.name, ', ') as kd_all_location
						FROM
							item_receiving_details ird
						LEFT JOIN item_receiving ir
							ON ird.item_receiving_id=ir.id
						LEFT JOIN locations loc
							ON loc.id=ird.location_id
						WHERE
							ird.location_id not in (100,101,102,103,104,105,106)
						GROUP BY ir.item_id
					) as rb ON rb.item_id=ri.item_id
					WHERE 
						ro.id in (". $wh1 .")
					GROUP BY rro.id, ri.item_id, itm.code, itm.name, itm.id, rb.kd_all_location, ro.id
					ORDER BY ro.id, itm.id
					";

			$r = array_merge($r, $this->db->query($q, false)->result_array());
		}

		return $r;
    }
	
	function validSN($data = array()){

    	$q = 	"SELECT
					a.*,
					NOW() as server_time
				FROM
					item_receiving_details a
				JOIN item_receiving b
					ON b.id=a.item_receiving_id
				JOIN
					items c ON c.id=b.item_id
				WHERE
					a.unique_code = '". $data['kd_unik'] ."'
						AND
					a.last_qty != 0
						AND
					c.code = '". $data['kd_barang'] ."'
				";
		$r = $this->db->query($q, false)->result_array();
		
		return $r;
    }
	
	function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
		return $this->db->get();
	}
	
	function postSN($data = array()){

		$this->db->trans_start();

		$this->db
			->set('repack_order_id', $data['id_order_kitting'])
			->set('item_id', $data['id_barang'])
			->set('unique_code', $data['kd_unik'])
			->set('qty', $data['qty'])
			->insert('repack_order_picking');

		$this->db->set('status_id', 5)
			->where('status_id', 3)
			->where('id', $data['id_order_kitting'])
			->update('repack_orders');

		$serialize 	= $this->get_id("last_qty", "unique_code", $data["kd_unik"], "item_receiving_details")->row_array();

		$qty = $serialize['last_qty'] - $data['qty'];

		if($qty == 0){
			$this->db->set('loc_id',101);
		}

		$this->db->set("last_qty",$qty);
		$this->db->where('unique_code',$data['kd_unik']);
		$this->db->update('item_receiving_details');

		$this->db->trans_complete();

    }

    function addTallyQty($post=array()){

		$this->db->select(['repack_order_id as id_order_kitting', 'itm.id as id_barang', 'COALESCE(sum(qty),0) as qty']);
		$this->db->from('repack_order_picking rop');
		$this->db->join('items itm','itm.id=rop.item_id','left');
		$this->db->where('repack_order_id',$post['id_order_kitting']);
		$this->db->where('itm.id',$post['id_barang']);
		$this->db->group_by('itm.id, rop.repack_order_id');

		$row = $this->db->get()->row_array();

		$this->db->set('tally_doc_qty', $row['qty']);
		$this->db->where('item_id', $row['id_barang']);
		$this->db->where('repack_order_id', $row['id_order_kitting']);
		$this->db->update('repack_order_details');

    }

	function itemPicked($data = array()){

		$barang 	= $this->get_id("id", "code", $data["kd_barang"], "items")->row_array();

    	$q = 	"SELECT
    				unique_code, qty
				FROM
					repack_order_picking
				WHERE
					id = '".$data['id_order_kitting']."'
						AND
					item_id = '".$barang['id']."'
				";
		$r = $this->db->query($q, false)->result_array();
		return $r;
    }

    function getDocQty($data=array()){

	   	$this->db
			->select(array('COALESCE((CASE WHEN ro.type=1 THEN sum((ri.qty * rro.qty)) ELSE rro.qty END),0) as doc_qty'))
			->from('repack_orders ro')
			->join('repacks_repack_orders rro','rro.repack_order_id=ro.id','left')
			->join('repacks r','r.id=rro.repack_id','left')
			->join('repack_items ri','ri.repack_id=r.id','left')
			->where('ro.id', $data['id_order_kitting'])
			->where('( ri.item_id='.$data['id_barang'].' OR r.item_id='.$data['id_barang'].')',NULL)
			->group_by('ri.id, ro.type, rro.qty');

		$res = $this->db->get()->row();
		return $res->doc_qty;
    }

    function getPickQty($data=array()){

		$this->db
			->select('sum(qty) as actual_bom_picked')
			->from('repack_order_picking')
			->where('repack_order_id', $data['id_order_kitting'])
			->where('item_id', $data['id_barang']);

		$res = $this->db->get()->row();

		return $res->actual_bom_picked;
	}

}
<?php

/**
 * Description of referensi_shift_model
 *
 * @author Warman Suganda
 */
class executiveview_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $table1 = 'bbm';
    private $table2 = 'bbm_barang';

    private $po = 'purchase_order';
    private $po_barang = 'purchase_order_barang';

    private $table3 = 'hr_ref_cabang';
    private $table4 = 'hr_kota';
    private $table5 = 'hr_pegawai_detail';
    private $table6 = 'hr_ref_status_kepegawaian';
    private $table7 = 'hr_pangkat';
    private $table8 = 'hr_jabatan';
    private $table9 = 'hr_hukuman';
    private $table10 = 'hr_penghargaan';
    private $table11 = 'hr_phk';

    public function data_pengeluaran_tahun($data_jenjang=1, $filter=array()) {
        $list_jenjang = array();
        $tahun = $this->input->post('tahun');

        $this->db->select("tanggal_po,year(tanggal_po) tahun_data,month(tanggal_po) bulan_data,sum(jumlah_barang * harga_barang) total");
        $this->db->from($this->po_barang . ' a');
        $this->db->join($this->po . ' b', 'a.id_po = b.id_po');

        $filter["tanggal_po >= "] = $tahun.'-01-01';
        $filter["tanggal_po <= "] = $tahun.'-12-31';

        $this->db->where_condition($filter);
        $this->db->group_by('month(tanggal_po)');
        $this->db->order_by('tanggal_po', 'DESC');
        $data_pengeluaran = $this->db->get();

        if($data_pengeluaran->num_rows >0 )
        {
            foreach ($data_pengeluaran->result() as $pengeluaran) {
                $list_bulan[$pengeluaran->tahun_data.'-'.$pengeluaran->bulan_data] = (int)$pengeluaran->total;
            }
            return $list_bulan;
        }
        
    }

    public function data_pengeluaran_bulan($data_jenjang=1, $filter=array()) {
        $list_jenjang = array();
        $bulanan = $this->input->post('bulanan');
        $bulananth = $this->input->post('bulananth');

        $this->db->select("tanggal_po,sum(jumlah_barang * harga_barang) total");
        $this->db->from($this->po_barang . ' a');
        $this->db->join($this->po . ' b', 'a.id_po = b.id_po');

        $filter["tanggal_po >= "] = $bulananth.'-'.$bulanan.'-01';
        $filter["tanggal_po <= "] = $bulananth.'-'.$bulanan.'-31';

        $this->db->where_condition($filter);
        $this->db->group_by('tanggal_po');
        $this->db->order_by('tanggal_po', 'DESC');
        $data_pengeluaran = $this->db->get();

        if($data_pengeluaran->num_rows >0 )
        {
            foreach ($data_pengeluaran->result() as $pengeluaran) {
                $list_bulan[$pengeluaran->tanggal_po] = (int)$pengeluaran->total;
            }
            return $list_bulan;
        }
        
    }

    public function data_barang($filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table5 . ' z');
        $this->db->join($this->table1 . ' y', 'y.nik = z.nik');
        $this->db->join($this->table3 . ' x', 'x.kd_cabang = y.kd_cabang');

        $filter['x.kd_kota = a.kd_kota'] = NULL;
        $filter['z.apr'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("DISTINCT ON (a.kd_kota) a.kd_kota, b.nama_kota, ({$jumlah_pegawai}) AS jumlah_pegawai", false);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->kd_kota] = array(
                'nama_kota' => $row->nama_kota,
                'jumlah_pegawai' => (int) $row->jumlah_pegawai
            );
        }

        return $data;
    }


    public function status_kepegawaian() {
        $this->db->from($this->table6);
        $data = array();
        foreach ($this->db->get()->result() as $value) {
            $data[$value->kode_status_kepegawaian] = $value->status_kepegawaian;
        }
        return $data;
    }

    public function data_pendidikan($data_jenjang, $filter) {
        $list_jenjang = array();
        foreach ($data_jenjang->result() as $jenjang) {
            $list_jenjang[$jenjang->jenjang_id] = 0;
        }

        $this->db->select('DISTINCT ON (a.nik) a.nik, a.jenjang, a.tahun_keluar', FALSE);
        $this->db->from($this->table2 . ' a');
        $this->db->join($this->table1 . ' b', 'a.nik = b.nik');

        $filter["a.jenjang <> ''"] = NULL;
        $filter["b.status_kerja <>"] = '15';
        $filter["b.status_kerja <>"] = '16';

        $this->db->where_condition($filter);
        $this->db->order_by('a.nik, a.tahun_keluar', 'DESC');
        $data_pendidikan = $this->db->get();

        foreach ($data_pendidikan->result() as $pendidikan) {
            $kode_jenjang = $pendidikan->jenjang;
            if (isset($list_jenjang[$kode_jenjang])) {
                $list_jenjang[$kode_jenjang] += 1;
            }
        }

        return $list_jenjang;
    }

    public function data_pegawai($filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table5 . ' z');
        $this->db->join($this->table1 . ' y', 'y.nik = z.nik');
        $this->db->join($this->table3 . ' x', 'x.kd_cabang = y.kd_cabang');

        $filter['x.kd_kota = a.kd_kota'] = NULL;
        $filter['z.apr'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("DISTINCT ON (a.kd_kota) a.kd_kota, b.nama_kota, ({$jumlah_pegawai}) AS jumlah_pegawai", false);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->kd_kota] = array(
                'nama_kota' => $row->nama_kota,
                'jumlah_pegawai' => (int) $row->jumlah_pegawai
            );
        }

        return $data;
    }

    public function data_pegawai_cabang($kd_kota = '', $filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table5 . ' z');
        $this->db->join($this->table1 . ' y', 'y.nik = z.nik');
        $this->db->join($this->table3 . ' x', 'x.kd_cabang = y.kd_cabang');

        $filter['y.kd_cabang = a.kd_cabang'] = NULL;
        $filter['z.apr'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("a.kd_cabang, a.nama_cabang, ({$jumlah_pegawai}) AS jumlah_pegawai", FALSE);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');
        $this->db->where_condition(array('a.kd_kota' => $kd_kota));

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->nama_cabang] = (int) $row->jumlah_pegawai;
        }

        return $data;
    }

    public function data_pegawai_status($kd_kota = '', $filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table5 . ' z');
        $this->db->join($this->table1 . ' y', 'y.nik = z.nik');
        $this->db->join($this->table3 . ' x', 'x.kd_cabang = y.kd_cabang');

        $filter['y.kd_cabang = a.kd_cabang'] = NULL;
        $filter['z.apr'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("a.kd_cabang, a.nama_cabang, ({$jumlah_pegawai}) AS jumlah_pegawai", FALSE);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');
        $this->db->where_condition(array('a.kd_kota' => $kd_kota));

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->nama_cabang] = (int) $row->jumlah_pegawai;
        }

        return $data;
    }

    public function data_pangkat_kota($filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table7 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_kota = a.kd_kota'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("DISTINCT ON (a.kd_kota) a.kd_kota, b.nama_kota, ({$jumlah_pegawai}) AS jumlah_pegawai", false);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->kd_kota] = array(
                'nama_kota' => $row->nama_kota,
                'jumlah_pegawai' => (int) $row->jumlah_pegawai
            );
        }

        return $data;
    }

    public function data_pangkat_cabang($kd_kota, $filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table7 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_cabang = a.kd_cabang'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("a.kd_cabang, a.nama_cabang, ({$jumlah_pegawai}) AS jumlah_pegawai", FALSE);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');
        $this->db->where_condition(array('a.kd_kota' => $kd_kota));

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->nama_cabang] = (int) $row->jumlah_pegawai;
        }

        return $data;
    }

    public function data_mutasi_kota($filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table8 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_kota = a.kd_kota'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("DISTINCT ON (a.kd_kota) a.kd_kota, b.nama_kota, ({$jumlah_pegawai}) AS jumlah_pegawai", false);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->kd_kota] = array(
                'nama_kota' => $row->nama_kota,
                'jumlah_pegawai' => (int) $row->jumlah_pegawai
            );
        }

        return $data;
    }

    public function data_mutasi_cabang($kd_kota, $filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table8 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_cabang = a.kd_cabang'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("a.kd_cabang, a.nama_cabang, ({$jumlah_pegawai}) AS jumlah_pegawai", FALSE);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');
        $this->db->where_condition(array('a.kd_kota' => $kd_kota));

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->nama_cabang] = (int) $row->jumlah_pegawai;
        }

        return $data;
    }

    public function data_hukuman_kota($filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table9 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_kota = a.kd_kota'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("DISTINCT ON (a.kd_kota) a.kd_kota, b.nama_kota, ({$jumlah_pegawai}) AS jumlah_pegawai", false);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->kd_kota] = array(
                'nama_kota' => $row->nama_kota,
                'jumlah_pegawai' => (int) $row->jumlah_pegawai
            );
        }

        return $data;
    }

    public function data_hukuman_cabang($kd_kota, $filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table9 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_cabang = a.kd_cabang'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("a.kd_cabang, a.nama_cabang, ({$jumlah_pegawai}) AS jumlah_pegawai", FALSE);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');
        $this->db->where_condition(array('a.kd_kota' => $kd_kota));

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->nama_cabang] = (int) $row->jumlah_pegawai;
        }

        return $data;
    }

    public function data_penghargaan_kota($filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table10 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_kota = a.kd_kota'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("DISTINCT ON (a.kd_kota) a.kd_kota, b.nama_kota, ({$jumlah_pegawai}) AS jumlah_pegawai", false);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->kd_kota] = array(
                'nama_kota' => $row->nama_kota,
                'jumlah_pegawai' => (int) $row->jumlah_pegawai
            );
        }

        return $data;
    }

    public function data_penghargaan_cabang($kd_kota, $filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table10 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_cabang = a.kd_cabang'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("a.kd_cabang, a.nama_cabang, ({$jumlah_pegawai}) AS jumlah_pegawai", FALSE);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');
        $this->db->where_condition(array('a.kd_kota' => $kd_kota));

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->nama_cabang] = (int) $row->jumlah_pegawai;
        }

        return $data;
    }

    public function data_phk_kota($filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table11 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_kota = a.kd_kota'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("DISTINCT ON (a.kd_kota) a.kd_kota, b.nama_kota, ({$jumlah_pegawai}) AS jumlah_pegawai", false);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->kd_kota] = array(
                'nama_kota' => $row->nama_kota,
                'jumlah_pegawai' => (int) $row->jumlah_pegawai
            );
        }

        return $data;
    }

    public function data_phk_cabang($kd_kota, $filter = array()) {
        $data = array();

        // Subquery Count Jumlah Pegawai
        $this->db->select('COUNT(z.nik)', FALSE);
        $this->db->from($this->table11 . ' z');
        $this->db->join($this->table3 . ' y', 'z.kd_cabang = y.kd_cabang');

        $filter['y.kd_cabang = a.kd_cabang'] = NULL;
        $filter['z.status'] = 'T';

        $this->db->where_condition($filter);
        $jumlah_pegawai = $this->db->get_compiled_select();

        // Main Query
        $this->db->select("a.kd_cabang, a.nama_cabang, ({$jumlah_pegawai}) AS jumlah_pegawai", FALSE);
        $this->db->from($this->table3 . ' a');
        $this->db->join($this->table4 . ' b', 'a.kd_kota = b.kd_kota');
        $this->db->where_condition(array('a.kd_kota' => $kd_kota));

        $data_cabang = $this->db->get();
        foreach ($data_cabang->result() as $row) {
            $data[$row->nama_cabang] = (int) $row->jumlah_pegawai;
        }

        return $data;
    }

}

/* End of file referensi_shift_model.php */
/* Location: ./application/models/referensi_shift_model.php */
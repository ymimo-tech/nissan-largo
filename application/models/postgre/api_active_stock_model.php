<?php

class Api_active_stock_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($serial_number){
        $this->db->select("itm.name as nama_barang, itm.id as item_id, itm.code as kd_barang, loc.name AS location, ird.kd_batch, itm.has_qty, itm.def_qty, qc.code as kd_qc, unt.name as unit_name");
        $this->db->select(["COALESCE(CAST(ird.tgl_exp as VARCHAR), '0000-00-00') as tgl_exp"]);
        $this->db->select(["(CASE WHEN ird.last_qty = 0 THEN pr.qty ELSE ird.last_qty END) AS last_qty"]);
    	$this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
    	$this->db->join("items itm", "itm.id=ir.item_id", "left");
        $this->db->join("units unt", "unt.id = itm.unit_id", "left");
    	$this->db->join("locations loc", "loc.id=ird.location_id", "left");
        $this->db->join("qc", "qc.id=ird.qc_id", "left");
        $this->db->join("picking_recomendation pr", "pr.item_receiving_detail_id=ird.id", "left");
    	$this->db->where("ird.unique_code", str_replace('%2A','*',$serial_number));
    	$this->db->where("ir.item_id IS NOT NULL");
    	return $this->db->get();
    }

    function get_pl($serial_number){
        $this->db->select("itm.name as nama_barang, itm.id as item_id, itm.code as kd_barang, loc.name AS location, ird.kd_batch, itm.has_qty, itm.def_qty, ird.last_qty, qc.code as kd_qc");
        $this->db->select(["COALESCE(CAST(ird.tgl_exp as VARCHAR), '0000-00-00') as tgl_exp"]);
        $this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
        $this->db->join("items itm", "itm.id=ir.item_id", "left");
        $this->db->join("locations loc", "loc.id=ird.location_id", "left");
        $this->db->join("qc", "qc.id=ird.qc_id", "left");
        $this->db->where("ird.parent_code", str_replace('%2A','*',$serial_number));
        $this->db->where("ir.item_id IS NOT NULL");
        return $this->db->get();
    }

    function get_item($item_code){
        $this->db->select("*");
        $this->db->from("items");
        $this->db->where("code", $item_code);
        return $this->db->get();
    }

    function retrieve_item($item_code){
        $this->db->select("loc.name as loc_name, SUM(last_qty) AS qty");
        $this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
        $this->db->join("items itm", "itm.id=ir.item_id", "left");
        $this->db->join("locations loc", "loc.id=ird.location_id", "left");
        $this->db->where("itm.code", $item_code);
		$this->db->where_not_in("loc.loc_type", "SENTOUT");
        $this->db->group_by("loc.name");
        return $this->db->get();
    }

	function history($serial_number){
		$this->db->select("(SELECT name FROM locations WHERE id = trf.loc_id_old) AS loc_name_old, (SELECT user_name FROM users WHERE id = trf.user_id_pick) AS user_name_old, pick_time, (SELECT name FROM locations WHERE id = trf.loc_id_new) loc_name_new, (SELECT user_name FROM users WHERE id = trf.user_id_put) AS user_name_new, put_time, process_name");
		$this->db->from("transfers trf");
		$this->db->where("unique_code", str_replace('%2A','*',$serial_number));
        return $this->db->get();
	}

}

<?php
class item_movement_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getListRaw($post = array()){
		$result = array();
		
		$where = '';
		
		if(!empty($post['id_barang']))
			$where .= sprintf(' AND brg.id_barang = \'%s\' ', $post['id_barang']);
		
		if(!empty($post['loc_id']))
			$where .= sprintf(' AND ( il.loc_id_old = \'%s\' OR il.loc_id_new = \'%s\') ', $post['loc_id'], $post['loc_id']);
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(pick_time) BETWEEN \'%s\' AND \'%s\' ', 
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}
		
		$sql = 'SELECT 
					brg.kd_barang, nama_barang, il.kd_unik, IFNULL(fr.loc_name, \'-\') AS loc_from,
					DATE_FORMAT(pick_time, \'%d\/%m\/%Y %H:%i\') AS pick_time,
					upick.user_name AS pick_by,
					IFNULL(t.loc_name, \'-\') AS loc_to,
					DATE_FORMAT(put_time, \'%d\/%m\/%Y %H:%i\') AS put_time,
					upick.user_name AS put_by, process_name
				FROM 
					int_transfer_detail il
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.kd_unik=il.kd_unik 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				JOIN m_loc fr
					ON fr.loc_id=il.loc_id_old 
				JOIN hr_user upick 
					ON upick.user_id=il.user_id_pick
				JOIN m_loc t
					ON t.loc_id=il.loc_id_new 
				JOIN hr_user uput 
					ON uput.user_id=il.user_id_put
				WHERE 1=1'.$where.' 
				ORDER BY 
					brg.kd_barang, CAST(rcvbrg.kd_unik AS UNSIGNED), transfer_detail_id ASC';
				
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => 'tf.id',
			1 => '',
			2 => 'itm.code',
			3 => 'itm.name',
			4 => 'ird.unique_code',
			5 => 'loc_from',
			6 => 'pick_by',
			7 => 'pick_time',
			8 => 'loc_to',
			9 => 'put_by',
			10 => 'put_time',
		);
		
		$where = '';
		
		if(!empty($post['id_barang']))
			$where .= sprintf(' AND itm.id = \'%s\' ', $post['id_barang']);
		
		if(!empty($post['loc_id']))
			$where .= sprintf(' AND ( tf.loc_id_old = \'%s\' OR tf.loc_id_new = \'%s\') ', $post['loc_id'], $post['loc_id']);
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(pick_time) BETWEEN \'%s\' AND \'%s\' ', 
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}
		
		if(!empty($post['sn']))
			$where .= sprintf(' AND ird.rfid_tag = \'%s\' ', $post['sn']);
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					transfers tf
				JOIN item_receiving_details ird
					ON ird.unique_code=tf.unique_code
				JOIN items itm
					ON itm.id=ird.item_id
				JOIN locations loc_old
					ON loc_old.id=tf.loc_id_old
				JOIN users upick
					ON upick.id=tf.user_id_pick
				JOIN locations loc_new
					ON loc_new.id=tf.loc_id_new 
				JOIN users uput 
					ON uput.id=tf.user_id_put
				WHERE 1=1'.$where;
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					tf.id as transfer_detail_id, itm.code as kd_barang, itm.name as nama_barang, tf.unique_code as kd_unik, COALESCE(loc_old.name, \'-\') AS loc_from,
					TO_CHAR(pick_time, \'DD\/MM\/YYYY HH24:MI\') AS pick_time,
					upick.user_name AS pick_by,
					COALESCE(loc_new.name, \'\') AS loc_to,
					TO_CHAR(put_time, \'DD\/MM\/YYYY %HH24:MI\') AS put_time,
					upick.user_name AS put_by, process_name
				FROM 
					transfers tf
				JOIN item_receiving_details ird
					ON ird.unique_code=tf.unique_code
				JOIN items itm
					ON itm.id=ird.item_id
				JOIN locations loc_old
					ON loc_old.id=tf.loc_id_old 
				JOIN users upick 
					ON upick.id=tf.user_id_pick
				JOIN locations loc_new
					ON loc_new.id=tf.loc_id_new 
				JOIN users uput 
					ON uput.id=tf.user_id_put
				WHERE 1=1'.$where;
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']].", 
				". $columns[$requestData['order'][1]['column']].",
				". $columns[$requestData['order'][2]['column']]." 
				".$requestData['order'][0]['dir']."  OFFSET ".$requestData['start']." LIMIT ".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $row[$i]['transfer_detail_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['loc_from'];
			$nested[] = $row[$i]['pick_by'];
			$nested[] = $row[$i]['pick_time'];
			$nested[] = $row[$i]['loc_to'];
			$nested[] = $row[$i]['put_by'];
			$nested[] = $row[$i]['put_time'];
			$nested[] = $row[$i]['process_name'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
}
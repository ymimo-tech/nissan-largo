<?php

class Api_packing_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($packing_code){
    	$this->db->select("*");
    	$this->db->from("m_packing");
    	$this->db->where("packing_code", $packing_code);
    	return $this->db->get();
    }

    function add($packing_code){
        $query  = $this->db->query("SELECT packing_code FROM m_packing WHERE packing_code LIKE '" . substr($packing_code, 0, -1) . "%' ORDER BY packing_id DESC LIMIT 1");
        $row    = $query->row_array();
        if(isset($row)){
        	$last = intval(substr($row['packing_code'], -1)) + 1;
            return substr($packing_code, 0, -1).$last;
        } else{
            return null;
        }
    }

    function load(){

        $this->db->select(['pl.name as pl_name', 'outb.code as kd_outbound', 'COALESCE(d.destination_name,s.source_name) as destination_name', 'COALESCE(sum(qty),0) as qty', 'COALESCE(sum(picked_qty),0) as picked_qty']);
		$this->db->select(["CAST(SPLIT_PART(pl.name,'-',2) AS INTEGER) AS sort"]);
        $this->db->from('pickings pl');
        $this->db->join('picking_list_outbound plo','pl.pl_id=plo.pl_id','left');
        $this->db->join('outbound outb','outb.id=plo.id_outbound', 'left');
        $this->db->join('destinations d','outb.destination_id=d.destination_id', 'left');
        $this->db->join('sources s','outb.destination_id=s.source_id', 'left');
        $this->db->join('picking_recomendation pr','pr.picking_id=pl.pl_id','left');
		$this->db->join('(SELECT picking_id, SUM(qty)-SUM(picked_qty) AS qty_packed FROM picking_recomendation GROUP BY picking_id) a','a.picking_id = pl.pl_id','left');
        $this->db->where('start_time IS NOT NULL',NULL);
        $this->db->where_in('pl.status_id',[8,9]);
		$this->db->where('((a.qty_packed > 0 OR qty_packed IS NULL))', NULL);
        $this->db->group_by('pl.pl_id,outb.code,d.destination_name,s.source_name,a.qty_packed');
		$this->db->order_by('sort','DESC');
        $this->db->order_by('pl.pl_id','DESC');

        return $this->db->get();
    }

    function get_picking(){

        $this->db->select('pl_id');
        $this->db->from('pickings');
        $this->db->where('end_time IS NOT NULL',NULL);
        $this->db->where_not_in('status_id',[8,9]);
        return $this->db->get()->result_array();

    }

    function get_outbound($picking_code=''){

        $this->db->select('outb.id as id_outbound, outb.code as kd_outbound');
        $this->db->from('pickings pl');
        $this->db->join('picking_list_outbound plo','pl.pl_id=plo.pl_id','left');
        $this->db->join('outbound outb','outb.id=plo.id_outbound', 'left');
        $this->db->where('pl.name',$picking_code);

        return $this->db->get()->result_array();

    }

    function get_items($outbound_code='',$picking_code=''){
        $sql = 'SELECT
                    CONCAT(SPLIT_PART(itm.brand,\'-\',1),\' - \',itm.code,\' - \',itm.oem,\' - \',itm.name,\' - \',itm.packing_description) as kd_barang,
                    sum(outbitm.qty)AS doc_qty,
                    COALESCE(t_obp.qty,0)AS act_qty
                FROM
                    pickings pl
                JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
                JOIN outbound outb ON outb.id = plo.id_outbound
                LEFT JOIN outbound_item outbitm ON outbitm.id_outbound = outb.id
                LEFT JOIN picking_qty plq ON plq.item_id = outbitm.item_id
                AND plq.picking_id = pl.pl_id
                LEFT JOIN items itm ON itm.id=outbitm.item_id
                LEFT JOIN(
                    SELECT
                        sum(qty) as qty,
                        item_id
                    FROM
                        outbound_item_picked oip
                    LEFT JOIN
                        outbound o ON o.id=oip.outbound_id
                    WHERE
                        o.code = \''.$outbound_code.'\'
                    GROUP BY
                        oip.item_id
                ) as t_obp on t_obp.item_id = plq.item_id
                WHERE
                    pl.name = \''.$picking_code.'\'
                AND outb.code = \''.$outbound_code.'\'
                AND plq.qty <> 0
                AND plq.qty IS NOT NULL
                GROUP BY
                    itm.id, t_obp.qty
                ';
        return $this->db->query($sql)->result_array();
    }

    function checkQty($id_outbound='', $id_picking='', $id_barang=''){

        $sql = "SELECT
                    COALESCE(sum(outbitm.qty), 0) AS doc_qty,
                    COALESCE(t_obp.qty,0) AS act_qty
                FROM
                    pickings pl
                JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
                JOIN outbound outb ON outb.id = plo.id_outbound
                LEFT JOIN outbound_item outbitm ON outbitm.id_outbound = outb.id
                LEFT JOIN picking_qty plq ON plq.item_id = outbitm.item_id
                AND plq.picking_id = pl.pl_id
                LEFT JOIN items itm ON itm.id=outbitm.item_id
                LEFT JOIN(
                    SELECT
                        sum(qty) as qty,
                        item_id
                    FROM
                        outbound_item_picked
                    WHERE
                        item_id=$id_barang
                    GROUP BY
                        item_id
                ) as t_obp on t_obp.item_id = plq.item_id
                WHERE
                    pl.pl_id = $id_picking
                AND outb.id = $id_outbound
                AND plq.qty <> 0
                AND plq.qty IS NOT NULL
                AND 
                    itm.id = $id_barang
                GROUP BY
                    itm.id, t_obp.qty
                ";

        $res = $this->db->query($sql)->row_array();

        if($res['doc_qty'] > $res['act_qty']){
            return true;
        }

        return false;
    }

    function generate($data){
    	$id_user		= $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
    	$data_insert 	= array(	'packing_code'		=> $data["packing_code"], 
                        	        'packing_date'    	=> $data["packing_date"],
                            	    'packing_user_id'	=> $id_user["user_id"]
                                	);
        $this->db->insert("m_packing", $data_insert);
    }
	
	function isTray($param) {
		$this->db
			 ->select('loctype.name')
			 ->from('locations loc')
			 ->join('location_types loctype','loc.location_type_id=loctype.id')
			 ->where('loc.name',$param);
			 
		$data = $this->db->get()->row_array();
		
		return $data;
	}

    function get_serial($serial_number){
    	$this->db->select("*");
    	$this->db->from("receiving_barang");
    	$this->db->where("packing_id IS NULL");
    	$this->db->where("pl_id IS NOT NULL");
    	$this->db->where("id_barang IS NOT NULL");
    	$this->db->where("kd_unik", $serial_number);
    	return $this->db->get();
    }

    function get_outbound_id($outboundCode='',$kd_unik=''){
        $this->db
            ->select('id')
            ->from('outbound')
            ->where('code', $outboundCode)
            ->where('status_id', 5);
			
		// if($kd_unik != '') {
			// $this->db->where('pr.unique_code',$kd_unik);
		// }
		
		// $this->db
			// ->select('outb.id as id')
			// ->from('picking_recomendation pr')
			// ->join('outbound outb','pr.outbound_id=outb.id','LEFT')
			// ->where('outb.code',$outboundCode);
			
		$data = $this->db->get()->row_array();
		
        return $data['id'];
    }

    function get_receiving_barang($unique_code){

        $this->db
            ->select('item_id as id_barang')
            ->from('item_receiving_details')
            ->where('unique_code', $unique_code);

        $data = $this->db->get();

        return $data->row();

    }

    function getSerialNumberbyPicking($pickingCode="",$itemCode="",$outboundCode=""){

        $data = array();

        if(!empty($pickingCode) && !empty($itemCode)){
			if($outboundCode != '') {
				$this->db->where('outb.code',$outboundCode);
			}

            $this->db
                ->select(["pr.unique_code as serial_number","CONCAT(COALESCE(SUM(pr.qty),0), ' ', u.code) as qty"])
                ->from('picking_recomendation pr')
                ->join('pickings p','p.pl_id=pr.picking_id')
				->join('outbound outb','pr.outbound_id=outb.id')
                ->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id')
                ->join('items itm','itm.id=pr.item_id')
				->join('units u','u.id=itm.unit_id')
                ->where('p.name',$pickingCode)
                ->where('itm.code',urldecode(str_replace('%7C','/',str_replace('&#40;','(',str_replace('&#41;',')',$itemCode)))))
                ->where('pr.st_packing',NULL)
                ->group_by('pr.unique_code, u.code');

            $data = $this->db->get()->result_array();

        }

        return $data;
    }
	
	function getSnByTray($tray) {
		$result = array();
		
		$this->db
			 ->select('ird.unique_code')
			 ->from('item_receiving_details ird')
			 ->join('locations loc','ird.location_id=loc.id')
			 ->where('loc.name',$tray);
			 
		$result = $this->db->get()->result_array();
		
		return $result;
	}

    function getSnByTray_BAK(){

        $result = array();

        $this->db
            ->select(["pr.unique_code as serial_number","CONCAT(itm.code, ' - ', itm.name) as item","pr.qty"])
            ->from('picking_recomendation pr')
            ->join('items itm','itm.id=pr.item_id');

        $result = $this->db->get()->result_array();

        return $result;

    }
	
	public function getRecList($param=array()){
        $result = array();
		$requestData = $_REQUEST;
		
		$packing_detail = $this->db->select('*')->from('header_oird')->where('packing_number',$param['packing_number'])->get();
		
		$dn_detail = $this->db->select('*')->from('delivery_notes')->where('name',$param['delivery_note'])->get();
		
		if($dn_detail->num_rows() > 0) {
			$dn_id = $dn_detail->row_array()['id'];
			
			$picking_destination = $this->db->select('outb.destination_id')->from('picking_list_outbound plo')->join('outbound outb','plo.id_outbound=outb.id')->join('picking_recomendation pr','pr.picking_id=plo.pl_id')->join('delivery_pl dp','dp.pl_id=pr.id')->where('dp.dn_id',$dn_id)->get()->row_array()['destination_id'];
			
			if($packing_detail->num_rows() > 0) {
				$packing_destination = $packing_detail->row_array()['destination_id'];
				if($packing_destination != $picking_destination) {
					$destination_detail = $this->db->select('destination_code,destination_name')->from('destinations')->where('destination_id',$packing_destination)->get()->row_array();
					
					$result = array(
						"status"			=> false,
						"message"			=> "This packing number already assigned to destination " . $destination_detail['destination_code'] . "-" . $destination_detail['destination_name'],
					);
					return $result;
				}
			}
			
			$columns = array(
				0 => 'itm.id',
				1 => '',
				2 => 'kd_barang',
				3 => 'nama_barang',
				4 => 'jumlah_barang',
				5 => 'actual_picked_qty',
				6 => 'status'
			);

			$this->db->start_cache();

			$getPickingByDN = $this->db->select('picking_id AS pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','pr.id=dp.pl_id')->where('dp.dn_id',$dn_id)->get()->result_array();
			$picking_ids = '';
			for($i=0;$i<count($getPickingByDN);$i++){
				$picking_ids .= $getPickingByDN[$i]['pl_id'].',';
			}
			$picking_ids = substr($picking_ids, 0, strlen($picking_ids)-1);
			
			$query_count = "SELECT item_id FROM picking_recomendation WHERE picking_id IN (" . $picking_ids . ")";
			
			$getCount = $this->db->query($query_count);

			$totalData = $getCount->num_rows();

			// $query_select = "SELECT * FROM
			// 				(SELECT
			// 					itm.code AS item_code, itm.name AS item_name, outb.id AS outbound_id, outb.code AS outbound_code, d.destination_code, d.destination_name, d.destination_address, p.pl_id AS picking_id, p.name AS picking_code, unt.name AS uom,
			// 					(SELECT COALESCE(SUM(prx.qty),0) FROM picking_recomendation prx WHERE prx.id = dp.pl_id AND prx.item_id = pq.item_id) AS qty_picked,
			// 					(SELECT COALESCE(SUM(oirdx.qty),0) FROM outbound_item_receiving_details oirdx JOIN item_receiving_details irdx ON irdx.id = oirdx.id_item_receiving_details WHERE oirdx.picking_id = p.pl_id AND irdx.item_id = pq.item_id) AS qty_packed
			// 				FROM
			// 					pickings p
			// 				--JOIN
			// 					--delivery_pl dp ON dp.pl_id = p.pl_id
			// 				JOIN
			// 					picking_recomendation pr ON p.pl_id = pr.picking_id
			// 				JOIN
			// 					delivery_pl dp ON dp.pl_id = pr.id
			// 				JOIN
			// 					picking_qty pq ON p.pl_id = pq.picking_id
			// 				JOIN
			// 					items itm ON pq.item_id = itm.id
			// 				JOIN
			// 					picking_list_outbound plo ON plo.pl_id = p.pl_id
			// 				JOIN
			// 					outbound outb ON outb.id = plo.id_outbound
			// 				JOIN
			// 					units unt ON itm.unit_id = unt.id
			// 				JOIN
			// 					destinations d ON d.destination_id = outb.destination_id
			// 				WHERE
			// 					dp.dn_id = ".$dn_id.") a
			// 				WHERE qty_picked > 0
			// 				;";

			// $query_select = "select i2.code as item_code, i2.name item_name, o2.id outbound_id, 
			// o2.code outbound_code, d2.destination_code, d2.destination_name, d2.destination_address, p.pl_id as picking_id, p.name as picking_code, i2.unit_id, u2.name uom, coalesce(pr.qty,0) as qty_picked, 
			// coalesce(oird.qty,0) as qty_packed1,
			// coalesce((select sum(oird2.qty) from outbound_item_receiving_details oird2 where pr_id = pr.id and dn_id = ".$dn_id." group by pr_id, dn_id),0) as qty_packed
			// from delivery_pl dp 
			// left join picking_recomendation pr on dp.pl_id = pr.id
			// left join items i2 on i2.id = pr.item_id
			// left join outbound o2 on o2.id = pr.outbound_id 
			// left join destinations d2 on d2.destination_id = o2.destination_id 
			// left join pickings p on p.pl_id = pr.picking_id 
			// left join outbound_item_receiving_details oird on oird.pr_id = pr.id
			// left join units u2 on u2.id = i2.unit_id 
			// where dp.dn_id =".$dn_id;

			$query_select = "select i2.code as item_code, i2.name item_name, o2.id outbound_id, 
			o2.code outbound_code, d2.destination_code, d2.destination_name, d2.destination_address, p.pl_id as picking_id, p.name as picking_code, i2.unit_id, u2.name uom, coalesce(pr.qty,0) as qty_picked, coalesce(sum(oird.qty),0) as qty_packed, pr.id as susup
			from delivery_pl dp 
			left join picking_recomendation pr on dp.pl_id = pr.id
			left join items i2 on i2.id = pr.item_id
			left join outbound o2 on o2.id = pr.outbound_id 
			left join destinations d2 on d2.destination_id = o2.destination_id 
			left join pickings p on p.pl_id = pr.picking_id 
			left join outbound_item_receiving_details oird on oird.pr_id = pr.id
			left join units u2 on u2.id = i2.unit_id 
			where dp.dn_id =".$dn_id."
			GROUP BY i2.code, i2.name, o2.id, o2.code,d2.destination_code, pr.id, d2.destination_name, d2.destination_address, p.pl_id, p.name, i2.unit_id, u2.name, pr.qty
			";
			
			
			
			$getSelect = $this->db->query($query_select);

			$rows = $getSelect->result_array();
			
			$result = array(
						"status"			=> true,
						"message"			=> $rows,
					);
		} else {
			$result = array(
				"status"			=> false,
				"message"			=> "This " . $param['delivery_note'] . " is not exist.",
			);
		}

		return $result;
	}
	
	public function getItemQtyByDN($param=array()){
		$result = array();
		
		$dn_detail = $this->db->select('*')->from('delivery_notes')->where('name',$param['delivery_note'])->where('status','COMPLETE')->get();
		$item_detail = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get();
		
		if($dn_detail->num_rows() > 0 && $item_detail->num_rows() > 0) {
			$dn_id = $dn_detail->row_array()['id'];
			$getPickingByDN = $this->db->select('picking_id AS pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','pr.id=dp.pl_id')->where('dn_id',$dn_id)->get()->result_array();
			$picking_ids = '';
			for($i=0;$i<count($getPickingByDN);$i++){
				$picking_ids .= $getPickingByDN[$i]['pl_id'].',';
			}
			$picking_ids = substr($picking_ids, 0, strlen($picking_ids)-1);
		
			$query_get = "SELECT COALESCE(SUM(qty),0) AS picked_qty FROM picking_recomendation WHERE item_id = ".$item_detail->row_array()['id']." AND picking_id IN (".$picking_ids.")";
			$get = $this->db->query($query_get)->row_array()['picked_qty'];
			
			$result = array(
				"status"			=> true,
				"qty_picked"		=> $get,
			);			
		} else {
			$result = array(
				"status"			=> false
			);
		}
		
		return $result;
	}
	
	public function savePacking_old($param=array()){
		$result = array();
		
		$id_user = $this->db->select('id')->from('users')->where('user_name',$param['user'])->get()->row_array()['id'];
		
		$packing_number 	= $param['packing_number'];
		$carton 			= $param['carton'];
		$quantity			= $param['quantity'];
		$weight 			= $param['weight'];
		$volume 			= $param['volume'];
		
		$dn_id = $this->db->select('id')->from('delivery_notes')->where('name',$param['delivery_note'])->get()->row_array()['id'];
		//$outbound_id = $this->db->select('id_outbound')->from('picking_list_outbound')->where('pl_id',$picking_id)->get()->row_array()['id_outbound'];
		$item_id = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get()->row_array()['id'];
		$destination_id = $this->db->select('destination_id')->from('destinations')->where('destination_code',$param['destination_code'])->get()->row_array()['destination_id'];
		
		$query_get = "SELECT
							pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
						FROM
							picking_recomendation pr
						JOIN
							delivery_pl dp ON dp.pl_id = pr.id
						LEFT JOIN
							outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND pr.picking_id = oird.picking_id
						WHERE
							dp.dn_id = " . $dn_id . "
						AND
							(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
						AND
							item_id = " . $item_id;
		
		$get = $this->db->query($query_get);
		$getCount = $get->num_rows();
		$getList = $get->result_array();
		
		if($getCount > 0) {
			$query_get = "SELECT
								SUM(pr.qty) AS qty_picked, COALESCE(SUM(oird.qty), 0) AS qty_packed
							FROM
								picking_recomendation pr
							JOIN
								delivery_pl dp ON dp.pl_id = pr.id
							LEFT JOIN
								outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND pr.picking_id = oird.picking_id
							WHERE
								dp.dn_id = " . $dn_id . "
							AND
								(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
							AND
								item_id = " . $item_id;
	
			$get = $this->db->query($query_get);
			$getList = $get->row_array();
			
			if(($getList['qty_picked'] - $getList['qty_packed']) >= $quantity) {
				$counter = ceil($quantity);
				$temp = 0;
				
				for($y=0;$y<$counter;$y++) {
					if($quantity >= 1){
						$qtyx = 1;
						$quantity = $quantity-1;
					} else {
						$qtyx = $quantity;
					}
					
					$temp += $qty;
					$query_get = "SELECT
									pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed, pr.picking_id, pr.outbound_id
								FROM
									picking_recomendation pr
								JOIN
									delivery_pl dp ON dp.pl_id = pr.id
								LEFT JOIN
									outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND pr.picking_id = oird.picking_id
								WHERE
									dp.dn_id = " . $dn_id . "
								AND
									(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
								AND
									(pr.qty - COALESCE(oird.qty,0)) >= ".$temp."
								AND
									item_id = " . $item_id;
				
					$get = $this->db->query($query_get);
					$getList = $get->row_array();
					
					
					
					$ird_id = $getList['item_receiving_detail_id'];
					$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('packing_number',$packing_number)->get();
					
					if($isExist->num_rows() > 0) {
						$qtyExist = $isExist->row_array()['qty'];
						$this->db
								->set('qty',$qtyExist + $qtyx)
								->where('packing_number',$packing_number)
								->where('id_item_receiving_details',$ird_id)
								->update('outbound_item_receiving_details');
					} else {
						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id) VALUES
									(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$packing_number."',1,".$qtyx.",".$id_user.")
									";
									
						$this->db->query($query_insert);
					}
				}
				
				$this->db
						->set('weight',$weight)
						->set('volume',$volume)
						->where('packing_number',$packing_number)
						->update('outbound_item_receiving_details');
					
				$result = array(
					"status"		=> true,
					"message"		=> "Success.",
				);
			} else {
				$result = array(
					"status"		=> false,
					"message"		=> "Quantity is not enough.",
				);
			}
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Each of this item (".$param['item_code'].") is already packed.",
			);
		}
		
		return $result;
	}

	public function savePacking_old1($param=array()){
		$result = array();
		
		// $id_user = $this->session->userdata('user_id');
		$id_user = $this->db->select('id')->from('users')->where('user_name',$param['user'])->get()->row_array()['id'];
		
		$packing_number 	= $param['packing_number'];
		$carton 			= $param['carton'];
		$quantity			= $param['quantity'];
		$weight 			= $param['weight'];
		$volume 			= $param['volume'];
		
		$dn_id = $this->db->select('id')->from('delivery_notes')->where('name',$param['delivery_note'])->get()->row_array()['id'];
		//$outbound_id = $this->db->select('id_outbound')->from('picking_list_outbound')->where('pl_id',$picking_id)->get()->row_array()['id_outbound'];
		$item_id = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get()->row_array()['id'];
		
		//$pr = $this->db->select('pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','dp.pl_id = pr.id','left')->join('items itm','itm.id = pr.item_id','left')->where('dp.dn_id',$dn_id)->where('itm.id',$item_id)->get()->row_array()['pl_id'];
		
		$sql_pr = "select * from (select pr.id as pl_id, pr.qty as pick_qty, coalesce(sum(oird.qty),0) as pack_qty from delivery_pl dp
				left join picking_recomendation pr on dp.pl_id = pr.id
				left join items itm on itm.id = pr.item_id 
				left join outbound_item_receiving_details oird on oird.pr_id = pr.id
				where dp.dn_id = ".$dn_id." and pr.item_id = ".$item_id." 
				GROUP by pr.qty, pr.id) as rb where rb.pick_qty > rb.pack_qty";
		$pr = $this->db->query($sql_pr)->row_array();
		
		if(isset($pr['pl_id'])){
			$pr = $pr['pl_id'];
		}else{
			$pr = false;
		}
		
		$destination_id = $this->db->select('destination_id')->from('destinations')->where('destination_code',$param['destination_code'])->get()->row_array()['destination_id'];
		
		$query_get = "SELECT
							pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
						FROM
							picking_recomendation pr
						JOIN
							delivery_pl dp ON dp.pl_id = pr.id
						LEFT JOIN
							outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
						LEFT JOIN 
							header_oird hoird on hoird.id = oird.packing_number
						WHERE
							dp.dn_id = " . $dn_id . "
						AND
							(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
						AND
							item_id = " . $item_id;
		$get = $this->db->query($query_get);
		$getCount = $get->num_rows();
		$getList = $get->result_array();
		
		$sql_sum = "select coalesce(sum(oird2.qty),0) as qty from outbound_item_receiving_details oird2 where pr_id = ".$pr." and dn_id = ".$dn_id." group by pr_id, dn_id";
		$sum_packed = $this->db->query($sql_sum)->row_array();
		if(isset($sum_packed['qty'])){
			$packed = $sum_packed['qty'];
		}else{
			$packed = 0;
		}

		$sql_pr = "select coalesce(sum(pr.qty),0) as qty from delivery_pl dp 
		left join picking_recomendation pr on dp.pl_id = pr.id
		where pr.id = ".$pr."
		group by pr.id";
		$sum_pr = $this->db->query($sql_pr)->row_array();
		if(isset($sum_pr['qty'])){
			$spr = $sum_pr['qty'];
		}else{
			$spr = 0;
		}

		$disp = $spr - ($packed+$quantity);
		
		if($getCount > 0 && $disp >= 0) {
			$query_get = "SELECT
								SUM(pr.qty) AS qty_picked, COALESCE(SUM(oird.qty), 0) AS qty_packed
							FROM
								picking_recomendation pr
							JOIN
								delivery_pl dp ON dp.pl_id = pr.id
							LEFT JOIN
								outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
							LEFT JOIN 
								header_oird hoird on hoird.id = oird.packing_number
							WHERE
								dp.dn_id = " . $dn_id . "
							AND
								(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
							AND
								item_id = " . $item_id;
	
			$get = $this->db->query($query_get);
			$getList = $get->row_array();

			if(($getList['qty_picked'] - $getList['qty_packed']) >= $quantity) {
				$counter = ceil($quantity);
				
				$this->db->trans_start();
				for($y=0;$y<$counter;$y++) {
					if($quantity >= 1){
						$qtyx = 1;
						$quantity = $quantity-1;
					} else {
						$qtyx = $quantity;
					}
					
					$query_get = "SELECT
									pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed, pr.picking_id, pr.outbound_id
								FROM
									picking_recomendation pr
								JOIN
									delivery_pl dp ON dp.pl_id = pr.id
								LEFT JOIN
									outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
								LEFT JOIN 
									header_oird hoird on hoird.id = oird.packing_number
								WHERE
									dp.dn_id = " . $dn_id . "
								AND
									(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
								AND
									(pr.qty - COALESCE(oird.qty,0)) >= ".$qtyx."
								AND
									item_id = " . $item_id;
				
					$get = $this->db->query($query_get);
					$getList = $get->row_array();

					
					
					$ird_id = $getList['item_receiving_detail_id'];
					
					$header_id = $this->db->select('id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array();
					
					if(count($header_id)>0) {
						$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('dn_id',$dn_id)->where('packing_number',$header_id[0]['id'])->get();
						if(isset($isExist->row_array()['qty'])){
							$qtyExist = $isExist->row_array()['qty'];
							
							$this->db
									->set('qty',$qtyExist + $qtyx)
									->where('packing_number',$header_id[0]['id'])
									->where('id_item_receiving_details',$ird_id)
									->where('dn_id',$dn_id)
									->update('outbound_item_receiving_details');
						}else{
							$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id,dn_id,pr_id) VALUES
									(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$header_id[0]['id']."',1,".$qtyx.",".$id_user.",".$dn_id.",".$pr.")
									";
									
							$this->db->query($query_insert);
						}
					} else {
						// dd('masuk');
						$query_insert_header = "
									INSERT INTO header_oird(destination_id, packing_number, user_id, time) VALUES
									(".$destination_id.",'".$packing_number."',".$id_user.",now())
									";
									
						$this->db->query($query_insert_header);

						$header_id = $this->db->select('id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array();

						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id,dn_id,pr_id) VALUES
									(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$header_id[0]['id']."',1,".$qtyx.",".$id_user.",".$dn_id.",".$pr.")
									";
									
						$this->db->query($query_insert);
					}
				}
				
				$this->db
						->set('weight',$weight)
						->set('volume',$volume)
						->where('id',$header_id[0]['id'])
						->update('header_oird');
					
				$result = array(
					"status"		=> true,
					"message"		=> "Success.",
				);
				$this->db->trans_complete();
			} else {
				$result = array(
					"status"		=> false,
					"message"		=> "Quantity is not enough.",
				);
			}
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Each of this item (".$param['item_code'].") is already packed.",
			);
		}
		
		return $result;
	}
	
	public function savePacking_old2($param=array()){
		$result = array();
		
		// $id_user = $this->session->userdata('user_id');
		$id_user = $this->db->select('id')->from('users')->where('user_name',$param['user'])->get()->row_array()['id'];
		
		$packing_number 	= $param['packing_number'];
		$carton 			= $param['carton'];
		$quantity			= $param['quantity'];
		$weight 			= $param['weight'];
		$volume 			= $param['volume'];
		
		$dn_id = $this->db->select('id')->from('delivery_notes')->where('name',$param['delivery_note'])->get()->row_array()['id'];
		//$outbound_id = $this->db->select('id_outbound')->from('picking_list_outbound')->where('pl_id',$picking_id)->get()->row_array()['id_outbound'];
		$item_id = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get()->row_array()['id'];

		//$pr = $this->db->select('pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','dp.pl_id = pr.id','left')->join('items itm','itm.id = pr.item_id','left')->where('dp.dn_id',$dn_id)->where('itm.id',$item_id)->get()->row_array()['pl_id'];
		
		$sql_pr = "select * from (select pr.id as pl_id, pr.qty as pick_qty, coalesce(sum(oird.qty),0) as pack_qty from delivery_pl dp
				left join picking_recomendation pr on dp.pl_id = pr.id
				left join items itm on itm.id = pr.item_id 
				left join outbound_item_receiving_details oird on oird.pr_id = pr.id
				where dp.dn_id = ".$dn_id." and pr.item_id = ".$item_id." 
				GROUP by pr.qty, pr.id) as rb where rb.pick_qty > rb.pack_qty";
		$pr = $this->db->query($sql_pr)->row_array();
		
		if(isset($pr['pl_id'])){
			$pr = $pr['pl_id'];
		}else{
			$pr = false;
		}
		
		$destination_id = $this->db->select('destination_id')->from('destinations')->where('destination_code',$param['destination_code'])->get()->row_array()['destination_id'];

		$query_get = "SELECT
							pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
						FROM
							picking_recomendation pr
						JOIN
							delivery_pl dp ON dp.pl_id = pr.id
						LEFT JOIN
							outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
						LEFT JOIN 
							header_oird hoird on hoird.id = oird.packing_number
						WHERE
							dp.dn_id = " . $dn_id . "
						AND
							(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
						AND
							item_id = " . $item_id;
		$get = $this->db->query($query_get);
		$getCount = $get->num_rows();
		$getList = $get->result_array();
	
		if($getCount > 0) {
			$query_get = "SELECT
								SUM(pr.qty) AS qty_picked, COALESCE(SUM(oird.qty), 0) AS qty_packed
							FROM
								picking_recomendation pr
							JOIN
								delivery_pl dp ON dp.pl_id = pr.id
							LEFT JOIN
								outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
							LEFT JOIN 
								header_oird hoird on hoird.id = oird.packing_number
							WHERE
								dp.dn_id = " . $dn_id . "
							AND
								(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
							AND
								item_id = " . $item_id;
	
			$get = $this->db->query($query_get);
			$getList = $get->row_array();

			if(($getList['qty_picked'] - $getList['qty_packed']) >= $quantity) {
				$counter = ceil($quantity);
				//dd($counter);
				
				$temp = $quantity;
				
				$this->db->trans_start();
				for($y=0;$y<$counter;$y++) {
					if($quantity >= 1){
						$qtyx = 1;
						$quantity = $quantity-1;
					} else {
						//$qtyx = $quantity;
					}
					
					$query_get = "SELECT
									pr.item_receiving_detail_id, pr.id as pr_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed, pr.picking_id, pr.outbound_id
								FROM
									picking_recomendation pr
								JOIN
									delivery_pl dp ON dp.pl_id = pr.id
								LEFT JOIN
									outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
								LEFT JOIN 
									header_oird hoird on hoird.id = oird.packing_number
								WHERE
									dp.dn_id = " . $dn_id . "
								AND
									(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
								AND
									(pr.qty - COALESCE(oird.qty,0)) >= ".$qtyx."
								AND
									item_id = " . $item_id." order by pr.qty";
				
					$get = $this->db->query($query_get);
					$getList = $get->row_array();
					
					
					$ird_id = $getList['item_receiving_detail_id'];
					
					$header_id = $this->db->select('id, destination_id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array();
					
					if(count($header_id)>0){
						if($header_id[0]['destination_id']==$destination_id){
							$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('packing_number',$header_id[0]['id'])->where('pr_id',$getList['pr_id'])->get();
							if(isset($isExist->row_array()['qty'])){
								$qtyExist = $isExist->row_array()['qty'];
								
								$this->db
										->set('qty',$qtyExist + $qtyx)
										->where('packing_number',$header_id[0]['id'])
										->where('id_item_receiving_details',$ird_id)
										->where('pr_id',$getList['pr_id'])
										->update('outbound_item_receiving_details');
							}else{
								$query_insert = "
										INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id,dn_id,pr_id) VALUES
										(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$header_id[0]['id']."',1,".$qtyx.",".$id_user.",".$dn_id.",".$getList['pr_id'].")
										";
										
								$this->db->query($query_insert);
							}
						}else{
							$result = array(
								"status"		=> false,
								"message"		=> "this item (".$param['item_code'].") is different destination.",
							);
							
							return $result;
						}
					} else {
						// dd('masuk');
						$query_insert_header = "
									INSERT INTO header_oird(destination_id, packing_number, user_id, time) VALUES
									(".$destination_id.",'".$packing_number."',".$id_user.",now())
									";
									
						$this->db->query($query_insert_header);

						$header_id = $this->db->select('id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array();

						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id,dn_id,pr_id) VALUES
									(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$header_id[0]['id']."',1,".$qtyx.",".$id_user.",".$dn_id.",".$getList['pr_id'].")
									";
									
						$this->db->query($query_insert);
					}
				}
				
				$this->db
						->set('weight',$weight)
						->set('volume',$volume)
						->where('id',$header_id[0]['id'])
						->update('header_oird');
					
				$result = array(
					"status"		=> true,
					"message"		=> "Success.",
				);
				$this->db->trans_complete();
			} else {
				$result = array(
					"status"		=> false,
					"message"		=> "Quantity is not enough.",
				);
			}
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Each of this item (".$param['item_code'].") is already packed.",
			);
		}
		
		return $result;
	}

	public function savePacking($param=array()){
		$result = array();
		
		// $id_user = $this->session->userdata('user_id');
		$id_user = $this->db->select('id')->from('users')->where('user_name',$param['user'])->get()->row_array()['id'];
		
		$packing_number 	= $param['packing_number'];
		$carton 			= $param['carton'];
		$quantity			= $param['quantity'];
		$weight 			= $param['weight'];
		$volume 			= $param['volume'];
		
		$dn_id = $this->db->select('id')->from('delivery_notes')->where('name',$param['delivery_note'])->get()->row_array()['id'];
		//$outbound_id = $this->db->select('id_outbound')->from('picking_list_outbound')->where('pl_id',$picking_id)->get()->row_array()['id_outbound'];
		$item_id = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get()->row_array()['id'];

		//$pr = $this->db->select('pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','dp.pl_id = pr.id','left')->join('items itm','itm.id = pr.item_id','left')->where('dp.dn_id',$dn_id)->where('itm.id',$item_id)->get()->row_array()['pl_id'];
		
		$sql_pr = "select * from (select pr.id as pl_id, pr.qty as pick_qty, coalesce(sum(oird.qty),0) as pack_qty from delivery_pl dp
				left join picking_recomendation pr on dp.pl_id = pr.id
				left join items itm on itm.id = pr.item_id 
				left join outbound_item_receiving_details oird on oird.pr_id = pr.id
				where dp.dn_id = ".$dn_id." and pr.item_id = ".$item_id." 
				GROUP by pr.qty, pr.id) as rb where rb.pick_qty > rb.pack_qty";
		$pr = $this->db->query($sql_pr)->row_array();
		
		if(isset($pr['pl_id'])){
			$pr = $pr['pl_id'];
		}else{
			$pr = false;
		}
		
		$destination_id = $this->db->select('destination_id')->from('destinations')->where('destination_code',$param['destination_code'])->get()->row_array()['destination_id'];

		// $query_get = "SELECT
		// 					pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
		// 				FROM
		// 					picking_recomendation pr
		// 				JOIN
		// 					delivery_pl dp ON dp.pl_id = pr.id
		// 				LEFT JOIN
		// 					outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
		// 				LEFT JOIN 
		// 					header_oird hoird on hoird.id = oird.packing_number
		// 				WHERE
		// 					dp.dn_id = " . $dn_id . "
		// 				AND
		// 					(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
		// 				AND
		// 					item_id = " . $item_id;

		$query_get = "select (select sum(oird.qty) as qty_picked
		from delivery_pl dp
		left join outbound_item_receiving_details oird on dp.pl_id = oird.pr_id 
		left join picking_recomendation pr on oird.pr_id = pr.id
		where dp.dn_id = " . $dn_id . " and pr.item_id = " . $item_id . "
		group by dp.dn_id) as qty_packed,
		(select sum(pr.qty) as qty_picked
		from delivery_pl dp
		left join picking_recomendation pr on dp.pl_id = pr.id
		where dp.dn_id = " . $dn_id . " and pr.item_id = " . $item_id . "
		group by dp.dn_id) as qty_picked";
		$get = $this->db->query($query_get);
		$getCount = $get->num_rows();
		$getList = $get->result_array();
	
		if($getCount > 0) {
			// $query_get = "SELECT
			// 					SUM(pr.qty) AS qty_picked, COALESCE(SUM(oird.qty), 0) AS qty_packed
			// 				FROM
			// 					picking_recomendation pr
			// 				JOIN
			// 					delivery_pl dp ON dp.pl_id = pr.id
			// 				LEFT JOIN
			// 					outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
			// 				LEFT JOIN 
			// 					header_oird hoird on hoird.id = oird.packing_number
			// 				WHERE
			// 					dp.dn_id = " . $dn_id . "
			// 				AND
			// 					(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
			// 				AND
			// 					item_id = " . $item_id;

			$query_get = "select coalesce((select sum(oird.qty) as qty_picked
						from delivery_pl dp
						left join outbound_item_receiving_details oird on dp.pl_id = oird.pr_id 
						left join picking_recomendation pr on oird.pr_id = pr.id
						where dp.dn_id = " . $dn_id . " and pr.item_id = " . $item_id . "
						group by dp.dn_id),0) as qty_packed,
						(select sum(pr.qty) as qty_picked
						from delivery_pl dp
						left join picking_recomendation pr on dp.pl_id = pr.id
						where dp.dn_id = " . $dn_id . " and pr.item_id = " . $item_id . "
						group by dp.dn_id) as qty_picked";
	
			$get = $this->db->query($query_get);
			$getList = $get->row_array();

			if(($getList['qty_picked'] - $getList['qty_packed']) >= $quantity) {
				// $counter = ceil($quantity);
				$counter = $quantity;
				
				$temp = $quantity;
				$pr_temp = array();
				$qty_temp = array(); 
				$unique = -1;
				
				$this->db->trans_start();
				for($y=0;$y<$counter;$y++) {
					if($quantity >= 1){
						$qtyx = 1;
						$quantity = $quantity-1;
					} else {
						//$qtyx = $quantity;
					}
					
					// $query_get = "SELECT
					// 				pr.item_receiving_detail_id, pr.id as pr_id, pr.qty AS qty_picked, COALESCE(sum(oird.qty), 0) AS qty_packed, pr.picking_id, pr.outbound_id
					// 			FROM
					// 				picking_recomendation pr
					// 			JOIN
					// 				delivery_pl dp ON dp.pl_id = pr.id
					// 			LEFT JOIN
					// 				outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
					// 			LEFT JOIN 
					// 				header_oird hoird on hoird.id = oird.packing_number
					// 			WHERE
					// 				dp.dn_id = " . $dn_id . "
					// 			AND
					// 				(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
					// 			AND
					// 				(pr.qty - (select sum(oird2.qty) from outbound_item_receiving_details oird2 where oird2.pr_id = pr.id group by oird2.pr_id)) >= ".$qtyx."
					// 			AND
					// 				item_id = " . $item_id." 
					// 			group by pr.item_receiving_detail_id, pr.id, pr.qty,pr.picking_id,pr.outbound_id
					// 			order by pr.qty";

					$query_get = "select * from (
											SELECT
												pr.item_receiving_detail_id, pr.id as pr_id, coalesce(pr.qty) AS qty_picked, coalesce((select sum(oird2.qty) from outbound_item_receiving_details oird2 where oird2.pr_id = pr.id group by oird2.pr_id),0) AS qty_packed, pr.picking_id, pr.outbound_id
											FROM
												delivery_pl dp 
											left JOIN
												picking_recomendation pr ON dp.pl_id = pr.id
											WHERE
												dp.dn_id = " . $dn_id . "
											AND
												pr.item_id = " . $item_id." 
										group by pr.item_receiving_detail_id, pr.id,pr.picking_id,pr.outbound_id
										order by pr.qty
								) rb where (rb.qty_picked - rb.qty_packed)>=".$qtyx;
				
					$get = $this->db->query($query_get);
					$getList = $get->row_array();
					// dd($getList);
					
					$ird_id = $getList['item_receiving_detail_id'];
					
					$header_id = $this->db->select('id, destination_id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array();
					
					if(count($header_id)>0){
						if($header_id[0]['destination_id']==$destination_id){
							$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('packing_number',$header_id[0]['id'])->where('pr_id',$getList['pr_id'])->get();
							if(isset($isExist->row_array()['qty'])){
								$qtyExist = $isExist->row_array()['qty'];
								$this->db
										->set('qty',$qtyExist + $qtyx)
										->where('packing_number',$header_id[0]['id'])
										->where('id_item_receiving_details',$ird_id)
										->where('pr_id',$getList['pr_id'])
										->update('outbound_item_receiving_details');
							}else{
								$query_insert = "
										INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id,dn_id,pr_id) VALUES
										(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$header_id[0]['id']."',1,".$qtyx.",".$id_user.",".$dn_id.",".$getList['pr_id'].")
										";
										
								$this->db->query($query_insert);
							}
						}else{
							$result = array(
								"status"		=> false,
								"message"		=> "this item (".$param['item_code'].") is different destination.",
							);
							
							return $result;
						}
					} else {
						// dd('masuk');
						$query_insert_header = "
									INSERT INTO header_oird(destination_id, packing_number, user_id, time) VALUES
									(".$destination_id.",'".$packing_number."',".$id_user.",now())
									";
									
						$this->db->query($query_insert_header);

						$header_id = $this->db->select('id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array();

						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id,dn_id,pr_id) VALUES
									(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$header_id[0]['id']."',1,".$qtyx.",".$id_user.",".$dn_id.",".$getList['pr_id'].")
									";
									
						$this->db->query($query_insert);
					}

					if(!in_array($getList['pr_id'],$pr_temp)){
						array_push($pr_temp, $getList['pr_id']);
						$unique += 1;
						$qty_temp[$unique] = [
							'qty' => $qtyx
						];
					}else{
						$qty_temp[$unique]['qty'] += $qtyx;
					}
				}

				for($u=0;$u<count($pr_temp);$u++){
					$picking_set = $this->db
					->select('*')
					->from('picking_recomendation')
					->where('id',$pr_temp[$u])
					->order_by('id','ASC')
					->get()->result_array();
					
                	if($picking_set[0]['qty'] !== $qty_temp[$u]['qty']){
						$qty_after = $picking_set[0]['qty'] - $qty_temp[$u]['qty'];

						if($qty_after>0){
							$this->db->where('id',$pr_temp[$u]);
							$this->db->set('qty',$qty_temp[$u]['qty']);
							$this->db->update('picking_recomendation');
							
							$new_pr = array(
								'picking_id' => $picking_set[0]['picking_id'],
								'unique_code' => $picking_set[0]['unique_code'],
								'item_id' => $picking_set[0]['item_id'],
								'location_id' => $picking_set[0]['location_id'],
								'qty' => $qty_after,
								'user_id' => $picking_set[0]['user_id'],
								'st_picking' => $picking_set[0]['st_picking'],
								'old_location_id' => $picking_set[0]['old_location_id'],
								'item_receiving_detail_id' => $picking_set[0]['item_receiving_detail_id'],
								'st_shipping' => $picking_set[0]['st_shipping'],
								'unit_id' => $picking_set[0]['unit_id'],
								'outbound_id' => $picking_set[0]['outbound_id'],
								'tray_location_id' => $picking_set[0]['tray_location_id'],
								'pick_time' => $picking_set[0]['pick_time'],
								'order_item_id' => $picking_set[0]['order_item_id']
							);
	
							$this->db->insert('picking_recomendation',$new_pr);
	
							// $id_pr = $this->db->select('id')
							// 					->from('picking_recomendation')
							// 					->where('unique_code',$picking_set[0]['unique_code'])
							// 					->where('picking_id',$picking_set[0]['picking_id'])
							// 					->order_by('id','desc')
							// 					->limit(1)
							// 					->get()->row_array()['id'];

							$id_pr = $this->db->insert_id();
							
							// $dn_id = $this->db->select('id')->from('delivery_notes')->where('name',$dn)->get()->row_array()['id'];
							$dp_insert = [
								'dn_id' => $dn_id,
								'pl_id' => $id_pr
							];
	
							$this->db->insert('delivery_pl',$dp_insert);
						}
					}else{
						// do nothing
					}
				}
				
				$this->db
						->set('weight',$weight)
						->set('volume',$volume)
						->where('id',$header_id[0]['id'])
						->update('header_oird');
					
				$result = array(
					"status"		=> true,
					"message"		=> "Success.",
				);
				$this->db->trans_complete();
			} else {
				$result = array(
					"status"		=> false,
					"message"		=> "Quantity is not enough.",
				);
			}
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Each of this item (".$param['item_code'].") is already packed.",
			);
		}
		
		return $result;
	}
    
}


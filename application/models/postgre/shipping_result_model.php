<?php
class shipping_result_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'shipping';
    private $table2 = 'pl_do';
    private $table4 = 'do_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $do = 'do';
    private $controllers = 'shipping_result';

    private function data($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $shipping_code= $this->input->post("shipping_code");
        $driver_name= $this->input->post("driver_name");
        $tanggal_awal = $this->input->post('tanggal_awal_shipping');
        $tanggal_akhir = $this->input->post('tanggal_akhir_shipping');

        if(!empty($shipping_code)){
            $condition["a.shipping_code like '%$shipping_code%'"]=null;
        }
        if(!empty($driver_name)){
            $condition["a.driver_name like '%$driver_name%'"]=null;
        }

        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.shipping_date >= '$tanggal_awal'"] = null ;
            $condition["a.shipping_date <= '$tanggal_akhir'"] = null;
        }
        
        
        $this->db->from($this->table  . ' a');
        $this->db->order_by('a.shipping_id DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_list_do($condition = array()) {
        // ===========Filtering=================
        $condition["shipping_id"]=null;
        $this->db->from($this->do  . ' a');
        $this->db->order_by('a.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $shipping_code= $this->input->post("shipping_code");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $shipping_id = $this->input->post('shipping_id');
        if(!empty($shipping_code)){
            $condition["a.shipping_code like '%$shipping_code%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($shipping_id)){
            $condition["b.shipping_id"]=$shipping_id;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

       $this->db->select('*');
        //$this->db->from($this->table  . ' a');//pl
        //$this->db->from($this->table2 . ' f');//pl_do
        $this->db->from($this->do . ' b');
        $this->db->join('receiving_barang rb','rb.id_do = b.id_do','left');
        $this->db->join('m_loc','m_loc.loc_id = rb.loc_id','left');
        $this->db->join('barang ','barang.id_barang = rb.id_barang','left');
        $this->db->join('satuan s','s.id_satuan = barang.id_satuan','left');
        $this->db->order_by('b.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.shipping_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
    //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.shipping_date >= '$tahun_aktif_awal'"] = null ;
        $condition["a.shipping_date <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->shipping_id;
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_'.$this->controllers, 'data-source' => base_url($this->controllers.'/get_detail_shipping/' . $id))) . ' ';
            $action .= '</li>';
            $action .= '</ul></div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->shipping_id,
                    'shipping_date' => hgenerator::switch_tanggal($value->shipping_date),
                    'shipping_code' => $value->shipping_code,
                    'driver_name' => $value->driver_name,
                    'license_plate' => $value->license_plate,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->shipping_id,
                    'shipping_date' => hgenerator::switch_tanggal($value->shipping_date),
                    'shipping_code' => $value->shipping_code,
                    'driver_name' => $value->driver_name,
                    'license_plate' => $value->license_plate,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function data_table_detail() {
        $shipping_id = $this->input->post('shipping_id');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        $update = array('shipping_id'=>NULL);
        $this->db->where('shipping_id',$shipping_id);
        $this->db->update('receiving_barang',$update);


        foreach ($data_detail->result() as $value) {
            $id = $value->id_do;

            $update = array();
            $update = array('shipping_id'=>$shipping_id);
            $this->db->where('kd_unik',$value->kd_unik);
            $this->db->update('receiving_barang',$update);

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'satuan' => $value->nama_satuan,
                    'exp_date' => hgenerator::switch_tanggal($value->tgl_exp),
                    'kd_unik' => $value->kd_unik,
                    'loc_name' => $value->loc_name,
                    
                );
            }else{
                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'satuan' => $value->nama_satuan,
                    'exp_date' => hgenerator::switch_tanggal($value->tgl_exp),
                    'kd_unik' => $value->kd_unik,
                    'loc_name' => $value->loc_name,
                    
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function data_table_excel() {
        //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.shipping_date >= '$tahun_aktif_awal'"] = null ;
        $condition["a.shipping_date <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->shipping_id;
            $action = '';
                $rows[] = array(
                    'shipping_date' => hgenerator::switch_tanggal($value->shipping_date),
                    'shipping_code' => $value->shipping_code,
                    'proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                );
        }
        return array('rows' => $rows, 'total' => $total);
    }   

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->shipping_id_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('shipping_id' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->do, $data, array('id_do' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table4, array('shipping_id' => $id));
        return $this->db->delete($this->table, array('shipping_id' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id_do' => $id));
    }
    
    
    public function options($default = '--Pilih Kode bbk--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->shipping_id] = $row->shipping_code ;
        }
        return $options;
    }

    public function options_do($default = '--Pilih Nama DO--', $key = '') {
        $data = $this->data_list_do()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_do] = $row->kd_do ;
        }
        return $options;
    }



    
}

?>
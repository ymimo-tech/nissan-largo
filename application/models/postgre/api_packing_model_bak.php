<?php

class Api_packing_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($packing_code){
    	$this->db->select("*");
    	$this->db->from("m_packing");
    	$this->db->where("packing_code", $packing_code);
    	return $this->db->get();
    }

    function add($packing_code){
        $query  = $this->db->query("SELECT packing_code FROM m_packing WHERE packing_code LIKE '" . substr($packing_code, 0, -1) . "%' ORDER BY packing_id DESC LIMIT 1");
        $row    = $query->row_array();
        if(isset($row)){
        	$last = intval(substr($row['packing_code'], -1)) + 1;
            return substr($packing_code, 0, -1).$last;
        } else{
            return null;
        }
    }

    function load(){

        $this->db->select(['pl.name as pl_name', 'outb.code as kd_outbound', 'COALESCE(d.destination_name,s.source_name) as destination_name', 'COALESCE(sum(qty),0) as qty', 'COALESCE(sum(picked_qty),0) as picked_qty']);
		$this->db->select(["CAST(SPLIT_PART(pl.name,'-',2) AS INTEGER) AS sort"]);
        $this->db->from('pickings pl');
        $this->db->join('picking_list_outbound plo','pl.pl_id=plo.pl_id','left');
        $this->db->join('outbound outb','outb.id=plo.id_outbound', 'left');
        $this->db->join('destinations d','outb.destination_id=d.destination_id', 'left');
        $this->db->join('sources s','outb.destination_id=s.source_id', 'left');
        $this->db->join('picking_recomendation pr','pr.picking_id=pl.pl_id','left');
		$this->db->join('(SELECT picking_id, SUM(qty)-SUM(picked_qty) AS qty_packed FROM picking_recomendation GROUP BY picking_id) a','a.picking_id = pl.pl_id','left');
        $this->db->where('start_time IS NOT NULL',NULL);
        $this->db->where_in('pl.status_id',[8,9]);
		$this->db->where('((a.qty_packed > 0 OR qty_packed IS NULL))', NULL);
        $this->db->group_by('pl.pl_id,outb.code,d.destination_name,s.source_name,a.qty_packed');
		$this->db->order_by('sort','DESC');
        $this->db->order_by('pl.pl_id','DESC');

        return $this->db->get();
    }

    function get_picking(){

        $this->db->select('pl_id');
        $this->db->from('pickings');
        $this->db->where('end_time IS NOT NULL',NULL);
        $this->db->where_not_in('status_id',[8,9]);
        return $this->db->get()->result_array();

    }

    function get_outbound($picking_code=''){

        $this->db->select('outb.id as id_outbound, outb.code as kd_outbound');
        $this->db->from('pickings pl');
        $this->db->join('picking_list_outbound plo','pl.pl_id=plo.pl_id','left');
        $this->db->join('outbound outb','outb.id=plo.id_outbound', 'left');
        $this->db->where('pl.name',$picking_code);

        return $this->db->get()->result_array();

    }

    function get_items($outbound_code='',$picking_code=''){
        $sql = 'SELECT
                    CONCAT(SPLIT_PART(itm.brand,\'-\',1),\' - \',itm.code,\' - \',itm.oem,\' - \',itm.name,\' - \',itm.packing_description) as kd_barang,
                    sum(outbitm.qty)AS doc_qty,
                    COALESCE(t_obp.qty,0)AS act_qty
                FROM
                    pickings pl
                JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
                JOIN outbound outb ON outb.id = plo.id_outbound
                LEFT JOIN outbound_item outbitm ON outbitm.id_outbound = outb.id
                LEFT JOIN picking_qty plq ON plq.item_id = outbitm.item_id
                AND plq.picking_id = pl.pl_id
                LEFT JOIN items itm ON itm.id=outbitm.item_id
                LEFT JOIN(
                    SELECT
                        sum(qty) as qty,
                        item_id
                    FROM
                        outbound_item_picked oip
                    LEFT JOIN
                        outbound o ON o.id=oip.outbound_id
                    WHERE
                        o.code = \''.$outbound_code.'\'
                    GROUP BY
                        oip.item_id
                ) as t_obp on t_obp.item_id = plq.item_id
                WHERE
                    pl.name = \''.$picking_code.'\'
                AND outb.code = \''.$outbound_code.'\'
                AND plq.qty <> 0
                AND plq.qty IS NOT NULL
                GROUP BY
                    itm.id, t_obp.qty
                ';
        return $this->db->query($sql)->result_array();
    }

    function checkQty($id_outbound='', $id_picking='', $id_barang=''){

        $sql = "SELECT
                    COALESCE(sum(outbitm.qty), 0) AS doc_qty,
                    COALESCE(t_obp.qty,0) AS act_qty
                FROM
                    pickings pl
                JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
                JOIN outbound outb ON outb.id = plo.id_outbound
                LEFT JOIN outbound_item outbitm ON outbitm.id_outbound = outb.id
                LEFT JOIN picking_qty plq ON plq.item_id = outbitm.item_id
                AND plq.picking_id = pl.pl_id
                LEFT JOIN items itm ON itm.id=outbitm.item_id
                LEFT JOIN(
                    SELECT
                        sum(qty) as qty,
                        item_id
                    FROM
                        outbound_item_picked
                    WHERE
                        item_id=$id_barang
                    GROUP BY
                        item_id
                ) as t_obp on t_obp.item_id = plq.item_id
                WHERE
                    pl.pl_id = $id_picking
                AND outb.id = $id_outbound
                AND plq.qty <> 0
                AND plq.qty IS NOT NULL
                AND 
                    itm.id = $id_barang
                GROUP BY
                    itm.id, t_obp.qty
                ";

        $res = $this->db->query($sql)->row_array();

        if($res['doc_qty'] > $res['act_qty']){
            return true;
        }

        return false;
    }

    function generate($data){
    	$id_user		= $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
    	$data_insert 	= array(	'packing_code'		=> $data["packing_code"], 
                        	        'packing_date'    	=> $data["packing_date"],
                            	    'packing_user_id'	=> $id_user["user_id"]
                                	);
        $this->db->insert("m_packing", $data_insert);
    }
	
	function isTray($param) {
		$this->db
			 ->select('loctype.name')
			 ->from('locations loc')
			 ->join('location_types loctype','loc.location_type_id=loctype.id')
			 ->where('loc.name',$param);
			 
		$data = $this->db->get()->row_array();
		
		return $data;
	}

    function get_serial($serial_number){
    	$this->db->select("*");
    	$this->db->from("receiving_barang");
    	$this->db->where("packing_id IS NULL");
    	$this->db->where("pl_id IS NOT NULL");
    	$this->db->where("id_barang IS NOT NULL");
    	$this->db->where("kd_unik", $serial_number);
    	return $this->db->get();
    }

    function get_outbound_id($outboundCode='',$kd_unik=''){
        $this->db
            ->select('id')
            ->from('outbound')
            ->where('code', $outboundCode)
            ->where('status_id', 5);
			
		// if($kd_unik != '') {
			// $this->db->where('pr.unique_code',$kd_unik);
		// }
		
		// $this->db
			// ->select('outb.id as id')
			// ->from('picking_recomendation pr')
			// ->join('outbound outb','pr.outbound_id=outb.id','LEFT')
			// ->where('outb.code',$outboundCode);
			
		$data = $this->db->get()->row_array();
		
        return $data['id'];
    }

    function get_receiving_barang($unique_code){

        $this->db
            ->select('item_id as id_barang')
            ->from('item_receiving_details')
            ->where('unique_code', $unique_code);

        $data = $this->db->get();

        return $data->row();

    }

    function getSerialNumberbyPicking($pickingCode="",$itemCode="",$outboundCode=""){

        $data = array();

        if(!empty($pickingCode) && !empty($itemCode)){
			if($outboundCode != '') {
				$this->db->where('outb.code',$outboundCode);
			}

            $this->db
                ->select(["pr.unique_code as serial_number","CONCAT(COALESCE(SUM(pr.qty),0), ' ', u.code) as qty"])
                ->from('picking_recomendation pr')
                ->join('pickings p','p.pl_id=pr.picking_id')
				->join('outbound outb','pr.outbound_id=outb.id')
                ->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id')
                ->join('items itm','itm.id=pr.item_id')
				->join('units u','u.id=itm.unit_id')
                ->where('p.name',$pickingCode)
                ->where('itm.code',urldecode(str_replace('%7C','/',str_replace('&#40;','(',str_replace('&#41;',')',$itemCode)))))
                ->where('pr.st_packing',NULL)
                ->group_by('pr.unique_code, u.code');

            $data = $this->db->get()->result_array();

        }

        return $data;
    }
	
	function getSnByTray($tray) {
		$result = array();
		
		$this->db
			 ->select('ird.unique_code')
			 ->from('item_receiving_details ird')
			 ->join('locations loc','ird.location_id=loc.id')
			 ->where('loc.name',$tray);
			 
		$result = $this->db->get()->result_array();
		
		return $result;
	}

    function getSnByTray_BAK(){

        $result = array();

        $this->db
            ->select(["pr.unique_code as serial_number","CONCAT(itm.code, ' - ', itm.name) as item","pr.qty"])
            ->from('picking_recomendation pr')
            ->join('items itm','itm.id=pr.item_id');

        $result = $this->db->get()->result_array();

        return $result;

    }
	
	public function getRecList($param=array()){
        $result = array();
		$requestData = $_REQUEST;
		
		$packing_detail = $this->db->select('*')->from('outbound_item_receiving_details')->where('packing_number',$param['packing_number'])->get();
		
		$dn_detail = $this->db->select('*')->from('delivery_notes')->where('name',$param['delivery_note'])->get();
		
		if($dn_detail->num_rows() > 0) {
			$dn_id = $dn_detail->row_array()['id'];
			
			$picking_destination = $this->db->select('outb.destination_id')->from('picking_list_outbound plo')->join('outbound outb','plo.id_outbound=outb.id')->join('picking_recomendation pr','pr.picking_id=plo.pl_id')->join('delivery_pl dp','dp.pl_id=pr.id')->where('dp.dn_id',$dn_id)->get()->row_array()['destination_id'];
			
			if($packing_detail->num_rows() > 0) {
				$packing_destination = $packing_detail->row_array()['destination_id'];
				if($packing_destination != $picking_destination) {
					$destination_detail = $this->db->select('destination_code,destination_name')->from('destinations')->where('destination_id',$packing_destination)->get()->row_array();
					
					$result = array(
						"status"			=> false,
						"message"			=> "This packing number already assigned to destination " . $destination_detail['destination_code'] . "-" . $destination_detail['destination_name'],
					);
					return $result;
				}
			}
			
			$columns = array(
				0 => 'itm.id',
				1 => '',
				2 => 'kd_barang',
				3 => 'nama_barang',
				4 => 'jumlah_barang',
				5 => 'actual_picked_qty',
				6 => 'status'
			);

			$this->db->start_cache();

			$getPickingByDN = $this->db->select('picking_id AS pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','pr.id=dp.pl_id')->where('dp.dn_id',$dn_id)->get()->result_array();
			$picking_ids = '';
			for($i=0;$i<count($getPickingByDN);$i++){
				$picking_ids .= $getPickingByDN[$i]['pl_id'].',';
			}
			$picking_ids = substr($picking_ids, 0, strlen($picking_ids)-1);
			
			$query_count = "SELECT item_id FROM picking_recomendation WHERE picking_id IN (" . $picking_ids . ")";
			
			$getCount = $this->db->query($query_count);

			$totalData = $getCount->num_rows();

			$query_select = "SELECT * FROM
							(SELECT
								itm.code AS item_code, itm.name AS item_name, outb.id AS outbound_id, outb.code AS outbound_code, d.destination_code, d.destination_name, d.destination_address, p.pl_id AS picking_id, p.name AS picking_code, unt.name AS uom,
								(SELECT COALESCE(SUM(prx.qty),0) FROM picking_recomendation prx WHERE prx.picking_id = p.pl_id AND prx.item_id = pq.item_id) AS qty_picked,
								(SELECT COALESCE(SUM(oirdx.qty),0) FROM outbound_item_receiving_details oirdx JOIN item_receiving_details irdx ON irdx.id = oirdx.id_item_receiving_details WHERE oirdx.picking_id = p.pl_id AND irdx.item_id = pq.item_id) AS qty_packed
							FROM
								pickings p
							--JOIN
								--delivery_pl dp ON dp.pl_id = p.pl_id
							JOIN
								picking_recomendation pr ON p.pl_id = pr.picking_id
							JOIN
								delivery_pl dp ON dp.pl_id = pr.id
							JOIN
								picking_qty pq ON p.pl_id = pq.picking_id
							JOIN
								items itm ON pq.item_id = itm.id
							JOIN
								picking_list_outbound plo ON plo.pl_id = p.pl_id
							JOIN
								outbound outb ON outb.id = plo.id_outbound
							JOIN
								units unt ON itm.unit_id = unt.id
							JOIN
								destinations d ON d.destination_id = outb.destination_id
							WHERE
								dp.dn_id = ".$dn_id.") a
							WHERE qty_picked > 0
							;";
			
			$getSelect = $this->db->query($query_select);

			$rows = $getSelect->result_array();
			
			$result = array(
						"status"			=> true,
						"message"			=> $rows,
					);
		} else {
			$result = array(
				"status"			=> false,
				"message"			=> "This " . $param['delivery_note'] . " is not exist.",
			);
		}

		return $result;
	}
	
	public function getItemQtyByDN($param=array()){
		$result = array();
		
		$dn_detail = $this->db->select('*')->from('delivery_notes')->where('name',$param['delivery_note'])->get();
		$item_detail = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get();
		
		if($dn_detail->num_rows() > 0 && $item_detail->num_rows() > 0) {
			$dn_id = $dn_detail->row_array()['id'];
			$getPickingByDN = $this->db->select('picking_id AS pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','pr.id=dp.pl_id')->where('dn_id',$dn_id)->get()->result_array();
			$picking_ids = '';
			for($i=0;$i<count($getPickingByDN);$i++){
				$picking_ids .= $getPickingByDN[$i]['pl_id'].',';
			}
			$picking_ids = substr($picking_ids, 0, strlen($picking_ids)-1);
			
			$query_get = "SELECT COALESCE(SUM(qty),0) AS picked_qty FROM picking_recomendation WHERE item_id = ".$item_detail->row_array()['id']." AND picking_id IN (".$picking_ids.")";
			$get = $this->db->query($query_get)->row_array()['picked_qty'];
			
			$result = array(
				"status"			=> true,
				"qty_picked"		=> $get,
			);			
		} else {
			$result = array(
				"status"			=> false
			);
		}
		
		return $result;
	}
	
	public function savePacking($param=array()){
		$result = array();
		
		$id_user = $this->db->select('id')->from('users')->where('user_name',$param['user'])->get()->row_array()['id'];
		
		$packing_number 	= $param['packing_number'];
		$carton 			= $param['carton'];
		$quantity			= $param['quantity'];
		$weight 			= $param['weight'];
		$volume 			= $param['volume'];
		
		$dn_id = $this->db->select('id')->from('delivery_notes')->where('name',$param['delivery_note'])->get()->row_array()['id'];
		//$outbound_id = $this->db->select('id_outbound')->from('picking_list_outbound')->where('pl_id',$picking_id)->get()->row_array()['id_outbound'];
		$item_id = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get()->row_array()['id'];
		$destination_id = $this->db->select('destination_id')->from('destinations')->where('destination_code',$param['destination_code'])->get()->row_array()['destination_id'];
		
		$query_get = "SELECT
							pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
						FROM
							picking_recomendation pr
						JOIN
							delivery_pl dp ON dp.pl_id = pr.id
						LEFT JOIN
							outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details
						WHERE
							dp.dn_id = " . $dn_id . "
						AND
							(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
						AND
							item_id = " . $item_id;
		
		$get = $this->db->query($query_get);
		$getCount = $get->num_rows();
		$getList = $get->result_array();
		
		if($getCount > 0) {
			$query_get = "SELECT
								SUM(pr.qty) AS qty_picked, COALESCE(SUM(oird.qty), 0) AS qty_packed
							FROM
								picking_recomendation pr
							JOIN
								delivery_pl dp ON dp.pl_id = pr.id
							LEFT JOIN
								outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details
							WHERE
								dp.dn_id = " . $dn_id . "
							AND
								(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
							AND
								item_id = " . $item_id;
	
			$get = $this->db->query($query_get);
			$getList = $get->row_array();
			
			if(($getList['qty_picked'] - $getList['qty_packed']) >= $quantity) {
				$counter = ceil($quantity);
				
				for($y=0;$y<$counter;$y++) {
					if($quantity >= 1){
						$qtyx = 1;
						$quantity = $quantity-1;
					} else {
						$qtyx = $quantity;
					}
					
					$query_get = "SELECT
									pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed, pr.picking_id, pr.outbound_id
								FROM
									picking_recomendation pr
								JOIN
									delivery_pl dp ON dp.pl_id = pr.id
								LEFT JOIN
									outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details
								WHERE
									dp.dn_id = " . $dn_id . "
								AND
									(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
								AND
									(pr.qty - COALESCE(oird.qty,0)) >= ".$qtyx."
								AND
									item_id = " . $item_id;
				
					$get = $this->db->query($query_get);
					$getList = $get->row_array();
					
					$ird_id = $getList['item_receiving_detail_id'];
					$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('packing_number',$packing_number)->get();
					
					if($isExist->num_rows() > 0) {
						$qtyExist = $isExist->row_array()['qty'];
						$this->db
								->set('qty',$qtyExist + $qtyx)
								->where('packing_number',$packing_number)
								->where('id_item_receiving_details',$ird_id)
								->update('outbound_item_receiving_details');
					} else {
						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id) VALUES
									(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$packing_number."',1,".$qtyx.",".$id_user.")
									";
									
						$this->db->query($query_insert);
					}
				}
				
				$this->db
						->set('weight',$weight)
						->set('volume',$volume)
						->where('packing_number',$packing_number)
						->update('outbound_item_receiving_details');
					
				$result = array(
					"status"		=> true,
					"message"		=> "Success.",
				);
			} else {
				$result = array(
					"status"		=> false,
					"message"		=> "Quantity is not enough.",
				);
			}
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Each of this item (".$param['item_code'].") is already packed.",
			);
		}
		
		return $result;
	}
    
}


<?php
class warehouse_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'warehouses';

    public function data($condition = array()) {

        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function getById($id){
        $this->db->where('id',$id);
        return $this->get_data();
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id' => $id));
    }
    
    function getWarehouseByUser($id){

        $this->data();
        $this->db->join('users_warehouses uw','uw.warehouse_id=a.id');
        $this->db->where('uw.user_id',$id);

        $results = $this->db->get()->result_array();

        return $results;

    }

    public function delete($id) {

        $result = array();

        $this->data();
        $this->db->where('id',$id);

        $row = $this->db->get()->num_rows();
        if($row < 1){
            $result['status'] = 'ERR';
            $result['message'] = 'Cannot delete master warehouse';
        }else{

            $this->db->delete($this->table, array('id' => $id));

            $result['status'] = 'OK';
            $result['message'] = 'Delete data success';

        }

        return $result;

    }

    public function getUserWarehouse($id){

        $results = array();

        $warehouses = $this->db->get_where('users_warehouses',['user_id'=>$id])->result_array();

        foreach($warehouses as $wh){
            $results[] = $wh['warehouse_id'];
        }

        return $results;

    }

    public function getItemsWarehouses($id){

        $results = array();

        $warehouses = $this->db->get_where('items_warehouses',['item_id'=>$id])->result_array();

        foreach($warehouses as $wh){
            $results[] = $wh['warehouse_id'];
        }

        return $results;

    }

    public function getWarehouses($table, $foreign_key, $foreign_value){

        $results = array();

        $where[$foreign_key] = $foreign_value;
        $warehouses = $this->db->get_where($table,$where)->result_array();

        foreach($warehouses as $wh){
            $results[] = $wh['warehouse_id'];
        }

        return $results;

    }

    public function addWarehouse($table, $foreign_key,$foreign_value, $data){

        $this->db->where($foreign_key,$foreign_value);
        $this->db->delete($table);

        if(!empty($data)){

            $data2 = array();

            if(in_array('all',$data)){

                $this->db->select('id');
                $data = $this->db->get('warehouses')->result_array();

                $i=0;
                foreach ($data as $wh) {

                    $data2[$i][$foreign_key]   = $foreign_value;
                    $data2[$i]['warehouse_id'] = $wh['id'];

                    $i++;

                }

            }else{

                $i=0;
                foreach ($data as $wh) {

                    $data2[$i][$foreign_key]   = $foreign_value;
                    $data2[$i]['warehouse_id'] = $wh;

                    $i++;
                }

            }

            $this->db->insert_batch($table,$data2);

        }

    }

    public function options($default_key='',$default_value='',$valid=FALSE,$selectAll=TRUE) {

        $this->db
            ->select('wh.id,wh.name')
            ->from('warehouses wh');

        if(!$valid){
            $this->db
                ->join('users_warehouses whu','whu.warehouse_id=wh.id')
                ->where('whu.user_id',$this->session->userdata('user_id'));
        }

        $data = $this->db->get();
        $options = array();

        if(!empty($default_key)){
            $options[$default_value] = $default_key;
        }

        foreach ($data->result() as $row) {
            $options[$row->id] = $row->name ;
        }
        return $options;
    }

    public function locations($default_key='',$default_value='',$valid=FALSE,$selectAll=TRUE) {

        $this->db
            ->select('loc.id,loc.name')
            ->from('locations loc');

        $data = $this->db->get();
        $options = array();

        if(!empty($default_key)){
            $options[$default_value] = $default_key;
        }

        foreach ($data->result() as $row) {
            $options[$row->id] = $row->name ;
        }
        return $options;
    }

    public function get_by_document($params) {

        if(!empty($params['warehouse_id'])){
            $this->db->where('wh.id',$params['warehouse_id']);
        }

        if(!empty($params['warehouse_id_2'])){
            $this->db->where('wh.id <>',$params['warehouse_id_2']);
        }

        if(!empty($params['document_id'])){
            $this->db->where('document_id',$params['document_id']);
        }

        $this->db
            ->select('wh.id,wh.name')
            ->from('warehouses wh')
            ->join('documents_warehouses whd','whd.warehouse_id=wh.id')
            ->join('users_warehouses whu','whu.warehouse_id=whd.warehouse_id','left')
            ->where('whu.user_id',$this->session->userdata('user_id'))
            ->group_by('wh.id,wh.name');

        $data = $this->db->get();
        return $data;
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        $inserts = array();

        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->select('count(*) as total')
                ->from($this->table)
                ->or_where('lower(name)', strtolower($data[$i]['warehouse_name']));

            $check = $this->db->get()->row()->total;

            if($check > 0){
                continue;
            }else{

                $inserts = array(
                    'name'          => $data[$i]['warehouse_name'],
                    'description'   => $data[$i]['warehouse_desc'],
                    'address'       => $data[$i]['warehouse_addr']
                );

                $this->db->insert($this->table,$inserts);
                $id = $this->db->insert_id();

                $getWarehouseGroup = $this->db->get_where('warehouse_groups',['name'=>$data[$i]['warehouse_group']])->row_array();
                if($getWarehouseGroup){

                    $this->db
                        ->set('warehouse_id',$id)
                        ->set('warehouse_group_id',$getWarehouseGroup['id'])
                        ->insert('warehouses_warehouse_groups');

                }

            }

        }

        $this->db->trans_complete();

        return $this->db->trans_status();

    }
}

?>

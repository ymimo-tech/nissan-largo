<?php

class Api_loading_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    function load(){

    //   $this->db
    //         ->select("DISTINCT ON (man.manifest_number) manifest_number as pl_name", FALSE)
    //         ->from("manifests man")
    //         ->join("shippings ship", "ship.shipping_id = man.shipping_id", "LEFT")
    //         ->join("outbound outb", "outb.id = man.outbound_id", "left")
    //         ->join("destinations d", "outb.destination_id = d.destination_id", "left")
    //         ->join("sources s", "outb.destination_id = s.source_id", "left")
    //         // ->where("ship.st_shipping", 0)
    //         ->where('man.status_manifest', 0)
    //         ->order_by('man.manifest_number', 'desc');


    //         $this->db
    //               ->select(["man.id as id_manifest",
    //               "ship.license_plate as license",
    //               "(SELECT string_agg(d.destination_name,'\n') FROM outbound outb JOIN manifests manf ON manf.outbound_id = outb.id JOIN destinations d ON d.destination_id = outb.destination_id WHERE manf.manifest_number = man.manifest_number) AS destination_name"])
    //               ->order_by('man.id', 'desc');

            // dd($this->db->get()->result_array());



        // $this->db
        //     ->select([
        //         "ship.code as pl_name",
        //         "license_plate AS license",
				// "p.name AS picking",
				// "COALESCE(d.destination_name,s.source_name) AS destination_name",
				// "CAST(SPLIT_PART(p.name,'-',2) AS INTEGER) AS sort"
        //     ])
        //     ->from("shippings ship")
        //     ->join("shipping_picking sp", "sp.shipping_id = ship.shipping_id", "left")
        //     ->join("pickings p", "p.pl_id=sp.pl_id", "left")
        //     ->join("picking_list_outbound plo", "plo.pl_id=p.pl_id", "left")
        //     ->join("outbound outb", "outb.id = plo.id_outbound", "left")
        //     ->join("destinations d", "outb.destination_id = d.destination_id", "left")
        //     ->join("sources s", "outb.destination_id = s.source_id", "left")
        //     ->where("ship.st_shipping", 0)

			// ->order_by("p.name","desc");

      // ->order_by('sort','desc');

			//->order_by("ship.shipping_id","desc");

        
        return $this->db->query("select m.manifest_number as pl_name, m.license_plate as license, string_agg(distinct(d2.destination_name),' ,') as destination_name from manifests m 
        left join manifest_oird mo on mo.manifests_id = m.id 
        left join header_oird ho on ho.id = mo.packing_id 
        left join destinations d2 on d2.destination_id = ho.destination_id 
        where m.status_manifest in (0,1)
        group by m.manifest_number, m.license_plate");

    }

    function get_shipping($shipping_code){
        $this->db->select("st_shipping");
        $this->db->from("shippings");
        $this->db->where("code", $shipping_code);
        return $this->db->get();
    }

    function get($loading_code){
        $this->db->select("st_shipping");
        $this->db->from("shippings");
        $this->db->where("code", $loading_code);
        return $this->db->get();
    }

    function getser($serial_number){
        $this->db->select("*");
        $this->db->from("item_receiving_details");
        $this->db->where("unique_code", $serial_number);
        return $this->db->get();
    }


    function getOutboundId($manifest_number){

      $results = $this->db
                  ->select('outbound_id')
                  ->from('manifests')
                  ->where('manifest_number', $manifest_number)
                  ->get()
                  ->result_array();
      $result = array_column($results, 'outbound_id');

      return $result;
    }

    function checkPacking($outbound_id, $packing_number){

      // $packing_number = substr($serial_number, 0,14);
      // $inc = (int)substr($serial_number, 14);


      $getOutboundId = $this->db
                            ->select("id_outbound")
                            ->from("outbound_item_receiving_details oird")
                            ->WHERE("packing_number", $packing_number)
                            // ->WHERE("inc", $inc)
                            ->get()
                            ->row_array(); 

                            if(!empty($getOutboundId)){
                                if(in_array($getOutboundId["id_outbound"], $outbound_id)){
                                    return TRUE;
                                }else {
                                    return FALSE;
                                }
                            }else{
                                return FALSE;
                            }
    }

    public function getPicking_outboundId($packing_number){

      // $packing_number = substr($serial_number, 0,14);
      // $inc = (int)substr($serial_number, 14);

    //   $data = $this->db
    //                   ->select('ird.picking_id, oird.id_outbound, shipp.shipping_id, shipp.code')
    //                   ->from('item_receiving_details ird')
    //                   ->join('outbound_item_receiving_details oird', 'oird.id_item_receiving_details = ird.id', 'LEFT')
    //                   ->join('shippings shipp','shipp.outbound_id = oird.id_outbound', 'LEFT')
    //                   ->WHERE("oird.packing_number", $packing_number)
    //                   // ->WHERE("oird.inc", $inc)
    //                   ->get()
    //                   ->row_array();

    $sql = "select pr.picking_id, o.id as id_outbound, m2.id, m2.manifest_number from header_oird ho 
    join outbound_item_receiving_details oird on oird.packing_number = ho.id
    left join manifest_oird mo on mo.packing_id = ho.id 
    left join manifests m2 on m2.id = mo.manifests_id 
    left join picking_recomendation pr on pr.id = oird.pr_id 
    left join outbound o on o.id = pr.outbound_id
    where ho.packing_number = '".$packing_number."'";

        $data = $this->db->query($sql)->row_array();
        // dd($data);
      return $data;
    }


    function update_loadingPacking($getPicking_outboundId, $serial_number){

      $this->db->trans_start();

      $this->updatePacking($getPicking_outboundId, $serial_number);

      $this->updateShippingPicking($getPicking_outboundId, $serial_number);
      $this->updateShippingIdInReceivingBarang($getPicking_outboundId['shipping_id']);

      $this->db->trans_complete();

      return $this->db->trans_status();

    }

    public function updatePacking($getPicking_outboundId, $packing_number){

      // $packing_number = substr($serial_number, 0,14);
      // $inc = (int)substr($serial_number, 14);



  		// $count_colly = count($id_colly);
  		// for($i = 0; $i < $count_colly; $i++){
  		// 	if($destination_id == $ids_destination[$i]){
  				$query = 'UPDATE outbound_item_receiving_details oird
  							SET shipping_id=\''.$getPicking_outboundId['shipping_id'].'\'
  							FROM item_receiving_details ird WHERE ird.id = oird.id_item_receiving_details
  							AND packing_number=\''.$packing_number.'\'
  							AND ird.picking_id=\''.$getPicking_outboundId['picking_id'].'\'';

  				$this->db->query($query);
  			// }
  		// }
  	}

    public function updateShippingPicking($getPicking_outboundId, $serial_number){

  		// $this->db->delete('shipping_picking', ['shipping_id'=>$id]);

  		// $this->db
  		// 	->set('shipping_id', $id)
  		// 	->set('pl_id', $idPicking)
  		// 	->insert('shipping_picking');

  		// foreach(array_unique($idPickings) as $key => $idPicking){
  			// $idPicking = $idPickings[$key];

        $id = $getPicking_outboundId['shipping_id'];
        $idPicking = $getPicking_outboundId['picking_id'];

  			$this->db->delete('shipping_picking', ['shipping_id'=>$id]);

  			$this->db
  				->set('shipping_id', $id)
  				->set('pl_id', $idPicking)
  				->insert('shipping_picking');
  		// }

  	}

    public function updateShippingIdInReceivingBarang($shippingId){
  		$result = array();

  		$this->db
  			->select('item_id as id_barang, p.pl_id')
  			->from('shippings s')
  			->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
  			->join('pickings p','p.pl_id=sp.pl_id')
  			->join('picking_qty pq','pq.picking_id=p.pl_id')
  			->where('s.shipping_id', $shippingId);

  		$row = $this->db->get()->result_array();

  		$len = count($row);

  		for($i = 0; $i < $len; $i++){

  			$this->db
  				->set('shipping_id', $shippingId)
  				->where('item_id', $row[$i]['id_barang'])
  				->where('picking_id', $row[$i]['pl_id'])
  				->update('item_receiving_details');

  		}

  		return $result;
  	}



    function get_serial($picking_code, $serial_number){

        // $packing_number = substr($serial_number, 0,14);
        // $inc = (int)substr($serial_number, 14);


        $manifest_number = $this->db
                                ->select('man.shipping_id, man.shipping_code, man.outbound_id')
                                ->from('manifests man')
                                ->where('manifest_number', $picking_code)
                                ->get()
                                ->result_array();

        $outbound_id = array_column($manifest_number, 'outbound_id');
        $shippingId = array_column($manifest_number, 'shipping_id');


		// if(strlen($inc) == 3) {
		// 	$inc = substr($inc,0,1);
		// } else if(strlen($inc) == 4){
		// 	$inc = substr($inc,0,2);
		// }
        // if($inc<10){
            // $inc = (int) substr($inc,1);
        // }

		// $serial_number = str_replace('%2A','*',$serial_number);

        // $where = "(ird.unique_code='$serial_number' OR ird.parent_code='$serial_number')";
        // if( !empty($inc) && is_integer($inc) ){
        // if( !empty($inc) ){
            // $where = "(ird.unique_code='$serial_number' OR ird.parent_code='$serial_number' OR (oird.packing_number='$serial_number' and inc=$inc))";
            $where = "(ird.unique_code='$serial_number' OR ird.parent_code='$serial_number' OR (oird.packing_number='$serial_number'))";

        // }
        // $this->db->select('ird.id, ird.first_qty, pr.location_id as loc_id, ird.item_id, ird.unique_code, sp.pl_id');
        $this->db->select('ird.id, ird.first_qty, pr.location_id as loc_id, ird.item_id, ird.unique_code, sp.pl_id');
        $this->db
            ->select('s.st_shipping')
            ->from('shippings s')
            ->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
            ->join('picking_recomendation pr','pr.outbound_id=s.outbound_id')
            // ->join('picking_recomendation pr','pr.picking_id=sp.pl_id')
            ->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id')
            ->join('outbound_item_receiving_details oird','oird.id_item_receiving_details=ird.id','left')
            ->where("$where",NULL,FALSE)
            // ->where('pr.location_id <>',104)
            // ->where('s.code',$picking_code);
            // ->where_in('s.shipping_id', $shippingId)
            ->where_in('s.outbound_id', $outbound_id);

        return $this->db->get();
    }

    function retrieve($shipping_code){
        $this->db->select("itm.code as kd_barang, unique_code as kd_unik");
        $this->db->from("item_receiving_details ird");
        $this->db->join("items itm", "itm.id=ird.item_id", "left");
        $this->db->join("shippings s", "s.shipping_id=ird.shipping_id", "left");
        $this->db->where("s.code", $shipping_code);
        $this->db->where("ird.st_loading", NULL);
        $this->db->order_by("itm.code");
        return $this->db->get();
    }

    function post_old($data){

        $id_shipping    = $this->get_id("shipping_id", "code", $data["pl_name"], "shippings")->row_array();
        $id_user        = $this->get_id("id as user_id", "user_name", $data["uname_pick"], "users")->row_array();

        $collyManifest = $this->db
                            ->select('man.total_colly')
                            ->from('manifests man')
                            ->where('man.manifest_number', $data['pl_name'])
                            ->get()
                            ->row_array();

        $collyManifest = $collyManifest['total_colly'] + 1;

        $this->db->set("total_colly", $collyManifest);
        $this->db->where('man.manifest_number', $data['pl_name']);
        $this->db->update("manifests man");


        // $this->db->select('ird.id, ird.first_qty, ird.location_id as loc_id, ird.item_id, ird.unique_code, sp.pl_id');
        $serialize = $this->get_serial($data["pl_name"],$data["kd_unik"])->result_array();

        if($serialize){
            $this->db->trans_start();

            foreach ($serialize as $serial) {

                $data_insert = array(   'unique_code'   => $serial["unique_code"],
                                        'loc_id_old'    => $serial["loc_id"],
                                        'user_id_pick'  => $id_user["user_id"],
                                        'pick_time'     => $data["loading_time"],
                                        'loc_id_new'    => 104,
                                        'user_id_put'   => $id_user["user_id"],
                                        'put_time'      => $data["put_time"],
        								                 'process_name'	=> 'LOADING'
                                        );

                $this->db->insert("transfers", $data_insert);

                if($serial["loc_id"] == 101){
                    // TODO: jangan di comment nanti bisa hilang semua
                    // $this->db->set("location_id", 104);
                    // $this->db->set("license_plate", $data["license_plate"]);
                    // $this->db->set("loading_time", $data["loading_time"]);
                    // $this->db->set("user_id_loading", $id_user["user_id"]);
                    // $this->db->set("st_loading", 1);
                    // $this->db->where('id',$serial['id']);
                    // $this->db->update("item_receiving_details");

                } else{

                    // nothing to do.

                }

                $this->db->set("location_id", 104);
                $this->db->where('picking_id',$serial['pl_id']);
                $this->db->where("unique_code",$serial['unique_code']);
                $this->db->update("picking_recomendation");

                // $this->post_loading($data["pl_name"], $serial["unique_code"]);
            }



            $this->db->trans_complete();

            return true;
        }

        return false;

    }

    public function post($data){
        $this->db->trans_start();

        //check if the kd_unik is shipped
        $sql = "select * from header_oird ho where ho.packing_number ='".$data['kd_unik']."' and shipping_id is null";
        // dd($sql);
        $check = $this->db->query($sql)->result_array();

        $sql2 = "select * from manifest_oird mo
        left join manifests m on m.id = mo.manifests_id 
        left join header_oird ho on ho.id = mo.packing_id
        where m.finish is null and m.manifest_number ='".$data['pl_name']."'
        and ho.packing_number ='".$data['kd_unik']."'";
        $check1 = $this->db->query($sql2)->result_array();

        if(count($check) !=0 && count($check1)!=0){
            //UPDATE Packing => shipping_id
            $sql = "select id from manifests where manifest_number = '".$data['pl_name']."'";
            $manifest_id = $this->db->query($sql)->row_array()['id'];

            $this->db->set('start','now()');
            $this->db->set('status_manifest', 1);
            $this->db->where('id',$manifest_id);
            $this->db->update('manifests');

            $this->db->set('shipping_id',$manifest_id);
            $this->db->where('ho.packing_number',$data['kd_unik']);
            $this->db->update('header_oird ho');

            //Keluarin Barang
            $id_user        = $this->get_id("id as user_id", "user_name", $data["uname_pick"], "users")->row_array();
            $sql = "select pr.unique_code, oird.pr_id, pr.location_id as loc_id, pr.picking_id as pl_id from header_oird ho 
            left join outbound_item_receiving_details oird on oird.packing_number = ho.id
            left join picking_recomendation pr on pr.id = oird.pr_id
            where ho.packing_number = '".$data['kd_unik']."'";
            $bulkySerial = $this->db->query($sql)->result_array();

            for($x = 0; $x < count($bulkySerial); $x++){
                $data_insert = array(   
                    'unique_code'   => $bulkySerial[$x]["unique_code"],
                    'loc_id_old'    => $bulkySerial[$x]["loc_id"],
                    'user_id_pick'  => $id_user["user_id"],
                    'pick_time'     => $data["loading_time"],
                    'loc_id_new'    => 104,
                    'user_id_put'   => $id_user["user_id"],
                    'put_time'      => $data["put_time"],
                    'process_name'	=> 'LOADING'
                );
                $this->db->insert("transfers", $data_insert);

                $this->db->set("location_id", 104);
                $this->db->where('picking_id',$bulkySerial[$x]['pl_id']);
                $this->db->where('id',$bulkySerial[$x]['pr_id']);
                $this->db->where("unique_code",$bulkySerial[$x]['unique_code']);
                $this->db->update("picking_recomendation");
            }
            $this->db->trans_complete();
            return true;
        }else{
            return false;
        }
    }

    function allFinish($manifest_number){
      $outboundId = "SELECT outbound_id from manifests WHERE manifest_number ='".$manifest_number."'";
      $outboundId = array_column($this->db->query($outboundId)->result_array(), 'outbound_id');
      // dd(array_column($outboundId, 'outbound_id'));
      // $query = "SELECT * FROM PICK_LIST WHERE DOCNUM = '".$productsData[$j]['docnum']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";


      $result = $this->db
                      ->select('oird.shipping_id, oird.packing_number')
                      ->from('outbound_item_receiving_details oird')
                      ->where_in('oird.id_outbound', $outboundId)
                      ->get()
                      ->result_array();

      return array_search(NULL, array_column($result, 'shipping_id'));
    }

	/* --BEGIN LOADING-- */
	function post_loading($ship_code = '', $unique_code = ''){

		$result = false;

		if($ship_code != '' && $unique_code != '') {
			$unique_code = str_replace('%2A','*',$unique_code);
			$this->db->select(["outb.code AS docnum", "outb.id_staging AS docentry", "outb.date AS docdate", "pick.name AS pl_name", "ship.code AS picking", "u.user_name AS operator", "ship.license_plate AS vehicle", "outb.status_id AS status", "doc.code AS doc_code", "outb.warehouse_id AS destination_id", "COALESCE(COALESCE(dest.destination_code, wdest.name),s.source_code) AS cardcode", "w.name AS warehouse"]);
			$this->db->from('outbound outb');
			$this->db->join('picking_list_outbound plo', 'outb.id = plo.id_outbound');
			$this->db->join('pickings pick', 'plo.pl_id = pick.pl_id');
			$this->db->join('picking_recomendation pr', 'pick.pl_id = pr.picking_id');
			$this->db->join('shipping_picking sp', 'pick.pl_id = sp.pl_id');
			$this->db->join('shippings ship', 'sp.shipping_id = ship.shipping_id');
			$this->db->join('users u', 'pr.user_id = u.id');
			$this->db->join('documents doc', 'outb.document_id = doc.document_id');
			$this->db->join('destinations dest', 'outb.destination_id=dest.destination_id', 'LEFT');
			$this->db->join('sources s', 'outb.destination_id=s.source_id', 'LEFT');
			$this->db->join('warehouses wdest', 'outb.destination_id=wdest.id', 'LEFT');
			$this->db->join('warehouses w', 'outb.warehouse_id=w.id');
			$this->db->where('pr.unique_code', $unique_code);
			$this->db->where('ship.code', $ship_code);
			$this->db->order_by('pr.id', 'DESC');
			$this->db->limit(1);

			$result = $this->db->get();

			if($result->num_rows() > 0) {
				$jsonData = array();
				$data = $result->result_array();

				$this->db->select(["ird.item_id AS product_id", "outb.code AS docnum", "outb.id_staging AS docentry", "itm.code AS itemcode", "COALESCE(pr.qty, 0) AS qty", "pr.unit_id AS uom", "ird.tgl_exp AS date_expired", "pr.unique_code AS pallet", "l.absentry AS absentry"]);
				$this->db->select(["(SELECT count(unique_code) FROM picking_recomendation WHERE picking_id = pr.picking_id AND qty > 0) AS total_line"]);
				$this->db->select(["(SELECT sum(qty) FROM picking_recomendation WHERE picking_id = pr.picking_id AND qty > 0 AND item_id = pr.item_id) AS total_qty"]);
				$this->db->from('picking_recomendation pr');
				$this->db->join('item_receiving_details ird', 'pr.item_receiving_detail_id = ird.id');
				$this->db->join('items itm', 'ird.item_id = itm.id');
				$this->db->join('pickings pick', 'pr.picking_id = pick.pl_id');
				$this->db->join('picking_qty pq', 'pick.pl_id = pq.picking_id');
				$this->db->join('shipping_picking sp', 'pick.pl_id = sp.pl_id');
				$this->db->join('shippings ship', 'sp.shipping_id = ship.shipping_id');
				$this->db->join('locations l', 'pr.old_location_id = l.id');
				$this->db->join('outbound outb', 'pr.outbound_id = outb.id');
				// $this->db->join('outbound_item outbitm','outbitm.item_id=pr.item_id and pr.outbound_id=outbitm.id_outbound');
				$this->db->where('pr.unique_code', $unique_code);
				$this->db->where('ship.code', $ship_code);
				$this->db->group_by('ird.item_id, outb.code, outb.id_staging, itm.code, pr.qty, pr.item_id, pr.unit_id, ird.tgl_exp, pr.unique_code, pr.picking_id, l.absentry');

				$productsGet = $this->db->get();

				for($i = 0; $i < $result->num_rows(); $i++) {
					if($data[0]['doc_code'] == 'SO') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();

							// echo '<pre>';
							// var_dump($productsData);exit;
							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM PICK_LIST WHERE DOCNUM = '".$productsData[$j]['docnum']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();

								$stagingCheckQuery = "SELECT * FROM DELIVERY_ORDER WHERE DOCENTRY = '".$data[$i]['docentry']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingCheckData = $staging->query($stagingCheckQuery)->row_array();
								if(count($stagingCheckData) > 0) {
									// $productsData[$j]['qty'] = $productsData[$j]['qty'] + $stagingCheckData['QTY'];

									// $deleteStagingQuery = "DELETE FROM DELIVERY_ORDER WHERE DOCENTRY = '".$data[$i]['docentry']."' AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
									// $deleteStagingCheck = $staging->query($deleteStagingQuery);
								} else {
									$data_insert	= 	array(
										'DOCENTRY'		=> $data[$i]['docentry'],
										'CARDCODE'		=> $data[$i]['cardcode'],
										'DOCDATE'		=> $data[$i]['docdate'],
										'DOCDUEDATE'	=> $data[$i]['docdate'],
										'TAXDATE'		=> date('Y-m-d'),
										'BASEENTRY'		=> $stagingData['BASEENTRY'],
										'BASELINE'		=> $stagingData['BASELINE'],
										'BASETYPE'		=> $stagingData['BASETYPE'],
										'ITEMCODE'		=> $productsData[$j]['itemcode'],
										'BINQTY'		=> $productsData[$j]['qty'],
										'QTY'			=> $productsData[$j]['total_qty'],
										'TOTAL_LINE'	=> $productsData[$j]['total_line'],
										'WAREHOUSE'		=> $data[$i]['warehouse'],
										'BINENTRY'		=> $productsData[$j]['absentry'],
										'INTERNALNO'	=> $data[$i]['pl_name'],
									);

									$staging = $this->load->database('staging', TRUE);
									$staging->insert('DELIVERY_ORDER', $data_insert);
								}
							}
						}
					} else if($data[0]['doc_code'] == 'APCM') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();

							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM AP_CREDITMEMO_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $stagingData['CARDCODE'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $stagingData['BASEENTRY'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'INVOICE'		=> $stagingData['INVOICE'],
									'UNITPRICE'		=> $stagingData['UNITPRICE'],
									'DISCHEADER'	=> $stagingData['DISCHEADER'],
									'DISCDETAIL'	=> $stagingData['DISCDETAIL'],
									'TAXCODE'		=> $stagingData['TAXCODE'],
									'ACCOUNT'		=> $stagingData['ACCOUNT'],
									'RETURNREASON'	=> $stagingData['RETURNREASON'],
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'QTY'			=> $productsData[$j]['total_qty'],
									'BINQTY'		=> $productsData[$j]['qty'],
									'TOTAL_LINE'	=> $productsData[$j]['total_line'],
									'WAREHOUSE'		=> $data[$i]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['absentry'],
									'INTERNALNO'	=> $data[$i]['pl_name'],
								);

								$staging = $this->load->database('staging', TRUE);
								$staging->insert('AP_CREDITMEMO', $data_insert);
							}
						}
					} else if($data[0]['doc_code'] == 'GRTN') {
						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();

							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$staging = $this->load->database('staging', TRUE);
								$query = "SELECT * FROM GOODS_RETURN_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
								$stagingData = $staging->query($query)->row_array();
								$data_insert	= 	array(
									// 'DOCENTRY'		=> $data[$i]['docentry'],
									'DOCNUM'		=> $data[$i]['docnum'],
									'CARDCODE'		=> $data[$i]['cardcode'],
									'DOCDATE'		=> $data[$i]['docdate'],
									'DOCDUEDATE'	=> $data[$i]['docdate'],
									'TAXDATE'		=> date('Y-m-d'),
									'BASEENTRY'		=> $stagingData['BASEENTRY'],
									'BASELINE'		=> $stagingData['BASELINE'],
									'BASETYPE'		=> $stagingData['BASETYPE'],
									'RETURNREASON'	=> $stagingData['RETURNREASON'],
									'ITEMCODE'		=> $productsData[$j]['itemcode'],
									'QTY'			=> $productsData[$j]['total_qty'],
									'BINQTY'		=> $productsData[$j]['qty'],
									'TOTAL_LINE'	=> $productsData[$j]['total_line'],
									'WAREHOUSE'		=> $data[$i]['warehouse'],
									'BINENTRY'		=> $productsData[$j]['absentry'],
									'INTERNALNO'	=> $data[$i]['pl_name'],
									// 'RETURNDATE'	=> date('Y-m-d')
								);

								$staging = $this->load->database('staging', TRUE);
								$staging->insert('GOODS_RETURN', $data_insert);
							}
						}
					} else {
						$productsArray = array();
						$productsData = $productsGet->result_array();

						for($j = 0; $j < $productsGet->num_rows(); $j++) {
							$staging = $this->load->database('staging', TRUE);
							$query = "SELECT * FROM GOODS_ISSUE_REQUEST WHERE BASEENTRY = ".$data[$i]['docentry']." AND ITEMCODE = '".$productsData[$j]['itemcode']."'";
							$stagingData = $staging->query($query)->row_array();
							$data_insert	= 	array(
								'CARDCODE'		=> $data[$i]['cardcode'],
								'DOCDATE'		=> $data[$i]['docdate'],
								'DOCDUEDATE'	=> $data[$i]['docdate'],
								'TAXDATE'		=> date('Y-m-d'),
								'BASEENTRY'		=> $stagingData['BASEENTRY'],
								'BASELINE'		=> $stagingData['BASELINE'],
								'BASETYPE'		=> $stagingData['BASETYPE'],
								'ITEMCODE'		=> $productsData[$j]['itemcode'],
								// 'BINQTY'		=> $productsData[$j]['qty'],
								'QTY'			=> $productsData[$j]['qty'],
								'TOTAL_LINE'	=> $productsData[$j]['total_line'],
								'WAREHOUSE'		=> $data[$i]['warehouse'],
								// 'BINENTRY'		=> $productsData[$j]['absentry'],
								'INTERNALNO'	=> $data[$i]['pl_name'],
							);

							$staging = $this->load->database('staging', TRUE);
							$staging->insert('GOODS_ISSUE', $data_insert);
						}
					}
				}
			}
		}

		return $result;

	}
	/* --END LOADING-- */

    function post_serial($data){
        $id_shipping    = $this->get_id("shipping_id", "code", $data["pl_name"], "shippings")->row_array();
        $id_user        = $this->get_id("id as user_id", "user_name", $data["uname_pick"], "users")->row_array();
        $id_item        = $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();
        $serialize      = $this->get_id('first_qty, location_id as loc_id','unique_code',$data['kd_unik'],'item_receiving_details')->row_array();

        $this->db->trans_start();

        $data_insert = array(   'unique_code'   => $data["kd_unik"],
                                'loc_id_old'    => $serialize["loc_id"],
                                'user_id_pick'  => $id_user["user_id"],
                                'pick_time'     => $data["loading_time"],
                                'loc_id_new'    => 104,
                                'user_id_put'   => $id_user["user_id"],
                                'put_time'      => $data["put_time"],
                                'process_name'  => 'LOADING'
                                );

        $this->db->insert("transfers", $data_insert);

        if($serialize["loc_id"] == 101){
            $this->db->set("location_id", 104);
            $this->db->set("license_plate", $data["license_plate"]);
            $this->db->set("loading_time", $data["loading_time"]);
            $this->db->set("user_id_loading", $id_user["user_id"]);
            $this->db->set("st_loading", 1);
            $this->db->where("shipping_id", $id_shipping["shipping_id"]);
            $this->db->where("item_id", $id_item["id_barang"]);
            $this->db->where("unique_code", $data["kd_unik"]);
            $this->db->update("item_receiving_details");
        } else{
            // nothing to do.
        }

        $this->db->trans_complete();
    }

    function start($loading_code){
        $this->db->set("shipping_start", "COALESCE(shipping_start, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->set('status_id',5);
        $this->db->where("code", $loading_code);
        $this->db->update("shippings");
    }

    function finish($loading_code){

        $this->db->set("shipping_finish", date("Y-m-d H:i:s"));
        $this->db->set("st_shipping", 1);
        $this->db->set("status_id", 4);
        $this->db->where("code", $loading_code);
        $this->db->update("shippings");

        $this->db
            ->from('shippings s')
            ->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
            ->where('s.code',$loading_code);

        $picking = $this->db->get()->row_array();

        $this->db->set("status_id", 10);
        $this->db->where("pl_id", $picking['pl_id']);
        $this->db->update("pickings");

    }

    function getOuterLabelByPL($loadingCode=""){

        $this->db
            ->select(["DISTINCT(ird.parent_code) as outerlabel"])
            ->from('shippings s')
            ->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
            ->join('picking_recomendation pr','pr.picking_id=sp.pl_id and pr.picking_id=sp.pl_id')
            ->join('item_receiving_details ird','ird.picking_id=sp.pl_id')
            ->where('s.code',$loadingCode)
            ->group_by('ird.parent_code');

        return $this->db->get();
    }

    function getPlateNumber($data=array()){

        $this->db
            ->from('shippings')
            ->where('license_plate',$data['plate_number'])
            ->where('code',$data['loading_code']);

        // return $this->db->get();
		return true;
    }

    function checkFinish($loadingCode=""){

        $this->db
            ->select(["COALESCE(SUM(pr.qty),0) as qty"])
            ->from('shippings s')
            // ->from('shipping_picking sp')
            // ->join('shippings s','s.shipping_id=sp.shipping_id')
            // ->join('picking_recomendation pr','pr.picking_id=sp.pl_id')
            ->join('outbound outb','outb.id = s.outbound_id')
            ->join('picking_recomendation pr','pr.outbound_id = outb.id')
            ->where('s.code',$loadingCode)
            ->group_by('unique_code');

        $q1 = $this->db->get_compiled_select();

        $docQty = $this->db->query("SELECT sum(qty) as qty FROM ($q1) as tbl ")->row_array();

        $this->db
            ->select(["COALESCE(SUM(pr.qty),0) as qty"])
            ->from('shippings s')
            ->join('outbound outb','outb.id = s.outbound_id')
            ->join('picking_recomendation pr','pr.outbound_id = outb.id')
            // ->from('shipping_picking sp')
            // ->join('shippings s','s.shipping_id=sp.shipping_id')
            // ->join('picking_recomendation pr','pr.picking_id=sp.pl_id')
            // ->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.shipping_id=s.shipping_id')
            ->where('pr.location_id',104)
            ->where('s.code',$loadingCode)
            ->group_by('pr.unique_code');

        $q2 = $this->db->get_compiled_select();

        $actQty = $this->db->query("SELECT sum(qty) as qty FROM ($q2) as tbl ")->row_array();

        if($docQty['qty'] == $actQty['qty']){
            return true;
        }else{
            return false;
        }

    }

    function getItem($loadingCode=""){

        $result = array();

        $sql = "select case 
                        WHEN (ho.shipping_id is not null) THEN concat(ho.packing_number, ' (Load)') 
                        WHEN (ho.shipping_id is null) THEN ho.packing_number 
                        END as kd_barang 
        from header_oird ho
        left join manifest_oird mo on mo.packing_id = ho.id
        left join manifests m on m.id = mo.manifests_id
        where m.manifest_number = '".$loadingCode."'";

        // dd($sql);

        $result = $this->db->query($sql)->result_array();

        // dd($result);

        return $result;

    }

}

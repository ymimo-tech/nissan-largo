<?php
class gate_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'gate';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        // $this->db->join('source_categories sc','sc.source_category_id=a.source_category_id');
        $this->db->where_condition($condition);

        return $this->db;

    }

    public function get_by_id($id) {
        $this->db->where('a.source_id',$id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        // if(!empty($condition['id'])){
            $condition['id'] = $condition['id'];
            // unset($condition['id']);
        // }
        $this->data($condition);

        return $this->db->get();

    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {

		$result = array();

        $t1 = $this->db->get_where('inbound',['supplier_id'=>$id])->row_array();

		if($t1){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('source_id' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}

        return $result;

    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        for ($i=0; $i < $dataLen ; $i++) {

                $insert= array(
                    'gate_code'          => $data[$i]['gate_code'],
                    'gate_name'          => $data[$i]['gate_name'],
                    'gate_province'       => $data[$i]['gate_province'],
                    'gate_city'          => $data[$i]['gate_city'],
                );

                $this->db
                    ->from($this->table)
                    ->where('LOWER(gate_code)', strtolower($data[$i]['gate_code']));
                    // ->or_where('lower(gate_name)', strtolower($data[$i]['gate_name']));

                $check = $this->db->get()->row_array();

                if($check){

                    $id = $check['id'];
                    $this->db->where('id',$id);
                    $this->db->update($this->table,$insert);

                }else{

                    $this->db->insert($this->table,$insert);
                    $id = $this->db->insert_id();

                }

        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );
    }

}

?>

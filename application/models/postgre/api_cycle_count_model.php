<?php

class Api_cycle_count_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	//andri
	function temp_post_cc($json, $cc){
		$csv = '/var/www/html/largo-gojek/cycle_count_csv/' . $cc . '.csv';
		$fp = fopen($csv, 'w');
		$data = json_decode($json, true);
		foreach($data as $row){
			fputcsv($fp, $row);
		}
		fclose($fp);
	}

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function load(){
        $this->db->select("code as cc_code, type as cc_type, wh.name as warehouse_name");
        $this->db->from("cycle_counts cc");
        $this->db->join('cycle_counts_users ccu','cc.cc_id=ccu.cycle_count_id');
        $this->db->join('users usr',"usr.id=ccu.user_id or usr.user_name='largo'");
        $this->db->join("warehouses wh",'wh.id=cc.warehouse_id');
        $this->db->where_not_in("status_id", [4,100]);
        $this->db->where("cc.finish",NULL);
        $this->db->where('usr.user_name',$this->input->get_request_header('User', true));
        $this->db->group_by('cc.cc_id, cc.code, cc.type, wh.name');
        return $this->db->get();
    }

    function load_type($type){
        if($type == "SN"){
            $this->db->select("name AS type_name");
            $this->db->from("locations");
            $this->db->where("name IS NOT NULL");
            $this->db->where("loc_type", NULL);
            $this->db->order_by("name", "ASC");
        }else{
            $this->db->select("code AS type_name");
            $this->db->from("items");
            $this->db->where("code IS NOT NULL");
            $this->db->order_by("code", "ASC");
        }
        return $this->db->get();
    }

    function check($cycle_code){
        $this->db->select(["(CASE WHEN status_id=3 THEN '0' WHEN status_id=4 THEN '1' ELSE '2' END) as status"]);
        $this->db->from("cycle_counts");
        $this->db->where("code", $cycle_code);
        return $this->db->get();
    }
	
	function check_positive_type($cycle_code, $type, $type1){
    	if($type1 == "SN"){
			$this->db->select("*");
			$this->db->from("cycle_count_details ccd");
			$this->db->join("cycle_counts cc", "ccd.cycle_count_id = cc.cc_id");
			$this->db->join("items itm", "itm.id=ccd.item_id");
			$this->db->where("cc.code", $cycle_code);
			$this->db->where("itm.code", $type);
			return $this->db->get();
		} else{
			$this->db->select("*");
			$this->db->from("cycle_count_details ccd");
			$this->db->join("cycle_counts cc", "ccd.cycle_count_id = cc.cc_id");
			$this->db->join("locations loc", "ccd.location_id = loc.id");
			$this->db->where("cc.code", $cycle_code);
			$this->db->where("loc.name", $type);
			return $this->db->get();
		}
    }


    function get($cycle_code, $cycle_type){
        if($cycle_type == "SN"){

            $sql = "SELECT
                        cc.code as cc_code,
                        (itm.code)AS type,
                        COALESCE(cc_qty,0) as pick_qty,
                        COALESCE(SUM(last_qty), 0) AS avail_qty
                    FROM
                        cycle_counts cc
                    LEFT JOIN cycle_count_details ccd ON ccd.cycle_count_id=cc.cc_id
                    LEFT JOIN item_receiving_details ird ON ird.item_id=ccd.item_id AND ird.cycle_count_id=cc.cc_id
                    LEFT JOIN items itm ON itm.id=ccd.item_id
                    LEFT JOIN(
                        SELECT
                            item_id,
                            sum(qty)AS cc_qty
                        FROM
                            cycle_counts cc
                        LEFT JOIN cycle_count_scanned ccs ON ccs.cycle_count_id = cc.cc_id
                        WHERE
                            code = '".$cycle_code."'
                        GROUP BY
                            item_id
                    )AS det ON det.item_id = itm.id
                    WHERE
                        cc.code = '".$cycle_code."'
                    GROUP BY
                        itm.id, cc.code, det.cc_qty, itm.code";

            
            return $this->db->query($sql);
        } else{
            $sql = "SELECT
                        cc.code as cc_code,
                        loc.name AS type,
                        COALESCE(cc_qty,0) as pick_qty,
                        COALESCE(SUM(last_qty), 0) AS avail_qty
                    FROM
                        cycle_counts cc
                    JOIN cycle_count_details ccd ON ccd.cycle_count_id=cc.cc_id
                    LEFT JOIN locations loc ON loc.id=ccd.location_id
                    LEFT JOIN (
                        SELECT 
                            ird.location_id,
                            cctq.cycle_count_id,
                            ird.last_qty
                        FROM
                        cycle_count_temp_qc cctq
                        JOIN item_receiving_details ird ON ird.unique_code=cctq.unique_code
                    ) as cctq
                        ON cctq.cycle_count_id=cc.cc_id AND cctq.location_id=ccd.location_id
                    LEFT JOIN(
                        SELECT
                            location_id,
                            sum(qty)AS cc_qty
                        FROM
                            cycle_counts cc
                        LEFT JOIN cycle_count_scanned ccs ON ccs.cycle_count_id = cc.cc_id
                        WHERE
                            code ='".$cycle_code."'
                        GROUP BY
                            location_id
                    )AS det ON det.location_id = loc.id
                    WHERE
                        cc.code = '".$cycle_code."'
                    GROUP BY
                        loc.id, cc.code, det.cc_qty, loc.name";
            return $this->db->query($sql);
        }
    }
	
	function get_serial_item($serial_number){
		$this->db->select("COALESCE(itm.code, 'GENERATED') AS kd_barang", false);
		$this->db->from("item_receiving_details ird");
		$this->db->join("items itm", "itm.id=ird.item_id", "left");
		$this->db->where("unique_code", $serial_number);
		return $this->db->get();
	}

    function get_serial($cycle_code, $item_code, $type, $serial_number){
        if($type == "SN"){
            $this->db->select(["has_qty","(CASE WHEN has_qty = 0 THEN 1 ELSE last_qty END) AS avail_qty", "loc.name AS type1", "qc.code as kd_qc", "tgl_exp", "tgl_in", "putaway_time", "itm.code as kd_barang", "first_qty"]);
            $this->db->from("item_receiving_details ird");
			$this->db->join("qc qc", "ird.qc_id = qc.id", "left");
			$this->db->join("locations loc", "ird.location_id = loc.id", "left");
            $this->db->join("items itm", "ird.item_id = itm.id", "left");
            $this->db->join("cycle_counts cc", "ird.cycle_count_id = cc.cc_id", "left");
            $this->db->where("unique_code", $serial_number);
            $this->db->where("cc.code", $cycle_code);
            $this->db->where("itm.code", $item_code);
            $this->db->where("ird.item_id IS NOT NULL",NULL);
            $this->db->where("ird.location_id IS NOT NULL",NULL);
			$this->db->where_not_in("ird.location_id", 104);
            return $this->db->get();
        } else{
            $this->db->select(["has_qty", "(CASE WHEN has_qty = 0 THEN 1 ELSE last_qty END) AS avail_qty", "itm.code AS type1","qc.code as kd_qc", "tgl_exp", "tgl_in", "putaway_time", "itm.code as kd_barang", "first_qty"]);
            $this->db->from("item_receiving_details ird");
			$this->db->join("qc", "ird.qc_id = qc.id", "left");
            $this->db->join("locations loc", "loc.id = ird.location_id", "left");
            $this->db->join("cycle_counts cc", "ird.cycle_count_id = cc.cc_id", "left");
            $this->db->join("items itm", "ird.item_id = itm.id", "left");
            $this->db->where("unique_code", $serial_number);
            $this->db->where("cc.code", $cycle_code);
            $this->db->where("loc.name", $item_code);
            $this->db->where("ird.item_id IS NOT NULL",NULL);
            $this->db->where("ird.location_id IS NOT NULL",NULL);
            return $this->db->get();
        }
    }
	
	function get_serial_loc($serial_number){
        $this->db->select(["has_qty", "(CASE WHEN has_qty = 0 THEN 1 ELSE last_qty END) AS avail_qty", "loc.name AS type1","qc.code as kd_qc", "tgl_exp", "tgl_in", "putaway_time", "itm.code as kd_barang"]);
        $this->db->from("item_receiving_details ird");
        $this->db->join("qc", "ird.qc_id = qc.id", "left");
        $this->db->join("locations loc", "loc.id = ird.location_id", "left");
        $this->db->join("items itm", "ird.item_id = itm.id", "left");
		$this->db->where("unique_code", $serial_number);
        $this->db->where("ird.item_id IS NOT NULL",NULL);
        $this->db->where("ird.location_id IS NOT NULL",NULL);
		$this->db->where_not_in("ird.location_id", [104]);
		return $this->db->get();
    }

    function get_serial_lp($serial_number){
        $this->db->select(["has_qty", "(CASE WHEN has_qty = 0 THEN 1 ELSE last_qty END) AS avail_qty", "loc.name AS type1","qc.code as kd_qc", "tgl_exp", "tgl_in", "putaway_time", "itm.code as kd_barang"]);
        $this->db->from("item_receiving_details ird");
        $this->db->join("qc", "ird.qc_id = qc.id", "left");
        $this->db->join("locations loc", ".loc.id = ird.location_id", "left");
        $this->db->join("items itm", "ird.item_id = itm.id", "left");
        $this->db->where("parent_code", $serial_number);
        $this->db->where("ird.item_id IS NOT NULL",NULL);
        $this->db->where("ird.location_id IS NOT NULL",NULL);
        $this->db->where_not_in("ird.location_id", [104]);
        return $this->db->get();
    }

    function get_positive_serial($cycle_code, $item_code, $type, $serial_number){
        if($type == "SN"){
            $this->db->select(["has_qty","(CASE WHEN has_qty = 0 THEN 1 ELSE last_qty END) AS avail_qty", "loc.name AS type1", "qc.code as kd_qc", "tgl_exp", "tgl_in", "putaway_time", "itm.code as kd_barang", "first_qty"]);
            $this->db->from("item_receiving_details ird");
            $this->db->join("qc qc", "ird.qc_id = qc.id", "left");
            $this->db->join("locations loc", "ird.location_id = loc.id", "left");
            $this->db->join("items itm", "ird.item_id = itm.id", "left");
            $this->db->join("cycle_counts cc", "ird.cycle_count_id = cc.cc_id", "left");
            $this->db->where("unique_code", $serial_number);
            // $this->db->where("cc.code", $cycle_code);
            // $this->db->where("itm.code", $item_code);
            $this->db->where("ird.item_id IS NOT NULL",NULL);
            $this->db->where("ird.location_id IS NOT NULL",NULL);
            $this->db->where_not_in("ird.location_id", 104);
            return $this->db->get();
        } else{
            $this->db->select(["has_qty", "(CASE WHEN has_qty = 0 THEN 1 ELSE last_qty END) AS avail_qty", "itm.code AS type1","qc.code as kd_qc", "tgl_exp", "tgl_in", "putaway_time", "itm.code as kd_barang", "first_qty"]);
            $this->db->from("item_receiving_details ird");
            $this->db->join("qc", "ird.qc_id = qc.id", "left");
            $this->db->join("locations loc", "loc.id = ird.location_id", "left");
            $this->db->join("cycle_counts cc", "ird.cycle_count_id = cc.cc_id", "left");
            $this->db->join("items itm", "ird.item_id = itm.id", "left");
            $this->db->where("unique_code", $serial_number);
            // $this->db->where("cc.code", $cycle_code);
            // $this->db->where("loc.name", $item_code);
            $this->db->where("ird.item_id IS NOT NULL",NULL);
            $this->db->where("ird.location_id IS NOT NULL",NULL);
            return $this->db->get();
        }
    }

    // function get_positive_serial($serial_number){
    //     $this->db->select(["has_qty", "(CASE WHEN has_qty = 0 THEN 1 ELSE last_qty END) AS avail_qty", "itm.code AS type1","qc.code as kd_qc", "tgl_exp", "tgl_in", "putaway_time", "itm.code as kd_barang"]);
    //     $this->db->from("item_receiving_details ird");
    //     $this->db->join("qc", "ird.qc_id = qc.id", "left");
    //     $this->db->join("locations loc", "loc.id = ird.location_id", "left");
    //     $this->db->join("items itm", "ird.item_id = itm.id", "left");
    //     $this->db->where("unique_code", $serial_number);
    //     $this->db->where("item_id IS NOT NULL",NULL);
    //     $this->db->where("location_id IS NOT NULL",NULL);
    //     $this->db->where_not_in("location_id", 104);
    //     return $this->db->get();
    // }

    function get_item($item_code){
        $this->db->select("has_batch, has_expdate, has_qty, def_qty");
        $this->db->from("items");
        $this->db->where("code", $item_code);
        return $this->db->get();
    }

    function get_cc_to_rcv($cycle_code){
        $this->db->select("*");
        $this->db->from("receivings");
        $this->db->where("code", $cycle_code);
        return $this->db->get();
    }

    function set_cc_to_rcv($cycle_code,$user){
        $id_user  = $this->get_id("id as user_id", "user_name", $user, "users")->row_array();
        $data_insert = array(  'date'  => date("Y-m-d H:i:s"), 
                               'code'       => $cycle_code,
                               'st_receiving'       => 0, 
                               'user_id'            => $id_user["user_id"]
                               );
        $this->db->insert("receivings",$data_insert);
    }

    function is_exist_to_post($data)
    {
        $id_cc = $this->get_id("cc_id", "code", $data["cc_code"], "cycle_counts")->row_array();

        $this->db->select("*");
        $this->db->from("cycle_count_scanned");
        $this->db->where("cycle_count_id", $id_cc["cc_id"]);
        $this->db->where("unique_code", $data["kd_unik"]);
        return $this->db->get();
    }

    function post($data){
        $id_cc      = $this->get_id("cc_id", "code", $data["cc_code"], "cycle_counts")->row_array();
        $id_user    = $this->get_id("id as user_id", "user_name", $data["user"], "users")->row_array();
        $id_loc     = $this->get_id("location_id as loc_id", "unique_code", $data["kd_unik"], "item_receiving_details")->row_array();
        $id_barang  = $this->get_id("item_id as id_barang", "unique_code", $data["kd_unik"], "item_receiving_details")->row_array();

    	$data_insert = array(  'cycle_count_id'      => $id_cc["cc_id"], 
                               'item_id'  => $id_barang["id_barang"],
                               'location_id'     => $id_loc["loc_id"], 
                               'unique_code'    => $data["kd_unik"],
                               'qty'     => $data["cc_qty"],
                               'time'    => $data["cc_time"],
                               'user_id' => $id_user["user_id"],
                               'parent_code' => $id_user["outer_label"]
                               );

        $this->db->insert("cycle_count_scanned",$data_insert);

        $this->db->set("st_cc", 1);
        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->update("item_receiving_details");
    }

    function start($cycle_code){
        $this->db->set("status_id", 5);
		$this->db->set("start", "COALESCE(start, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("code", $cycle_code);
        $this->db->update("cycle_counts");
    }

    function finish($cycle_code){
        $this->db->set("status_id", 4);
        $this->db->set("finish", "COALESCE(finish, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("code", $cycle_code);
        $this->db->update("cycle_counts");
    }
	
	function get_type($cycle_code){
		$this->db->select("*, type as cc_type");
		$this->db->from("cycle_counts");
		$this->db->where("code", $cycle_code);
		return $this->db->get();
	}
	
	function post_loc($data){

        $id_cc      = $this->get_id("cc_id", "code", $data["cc_code"], "cycle_counts")->row_array();
        $id_user    = $this->get_id("id as user_id", "user_name", $data["user"], "users")->row_array();
        $id_loc     = $this->get_id("id as loc_id", "name", $data["type"], "locations")->row_array();
        $id_barang  = $this->get_id("id as id_barang", "code", $data["type1"], "items")->row_array();
		$id_qc      = $this->get_id("id as id_qc", "code", $data["kd_qc"], "qc")->row_array();

        $data['tgl_exp'] = (empty($data['tgl_exp']) || $data['tgl_exp'] === "0000-00-00" || $data['tgl_exp'] === "0000-00-00 00:00:00") ? NULL : $data['tgl_exp'];

    	$data_insert = array(  'cycle_count_id'      => $id_cc["cc_id"], 
                               'item_id'  => $id_barang["id_barang"],
                               'location_id'     => $id_loc["loc_id"], 
                               'unique_code'    => $data["kd_unik"],
                               'qty'     => $data["cc_qty"],
                               'time'    => $data["cc_time"],
                               'user_id' => $id_user["user_id"],
							   'qc_id' 		=> $id_qc["id_qc"],
							   'exp_date' 	=> $data["tgl_exp"],
							   'in_date' 	=> $data["tgl_in"],
                               'parent_code'    => $data["outer_label"]
                               );
        $this->db->insert("cycle_count_scanned",$data_insert);

        $this->db->set("st_cc", 1);
        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->update("item_receiving_details");
    }
	
	function post_sn($data){
        $id_cc      = $this->get_id("cc_id", "code", $data["cc_code"], "cycle_counts")->row_array();
        $id_user    = $this->get_id("id as user_id", "user_name", $data["user"], "users")->row_array();
        $id_loc     = $this->get_id("id as loc_id", "name", $data["type1"], "locations")->row_array();
        $id_barang  = $this->get_id("id as id_barang", "code", $data["type"], "items")->row_array();
        $id_qc      = $this->get_id("id as id_qc", "code", $data["kd_qc"], "qc")->row_array();


        $data['tgl_exp'] = (empty($data['tgl_exp']) || $data['tgl_exp'] === "0000-00-00" || $data['tgl_exp'] === "0000-00-00 00:00:00") ? NULL : $data['tgl_exp'];

        $data_insert = array(  
                           'cycle_count_id' => $id_cc["cc_id"], 
                           'item_id'        => $id_barang["id_barang"],
                           'location_id'    => $id_loc["loc_id"], 
                           'unique_code'    => $data["kd_unik"],
                           'qty'            => $data["cc_qty"],
                           'time'           => $data["cc_time"],
                           'user_id'        => $id_user["user_id"],
                           'qc_id'          => $id_qc["id_qc"],
                           'exp_date'       => $data["tgl_exp"],
                           'in_date'    => $data["tgl_in"],
                           'parent_code'    => $data["outer_label"]
                       );

        $this->db->insert("cycle_count_scanned",$data_insert);

        $this->db->set("st_cc", 1);
        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->update("item_receiving_details");
    }

    function get_item_picked($data=array()){

        if($data['cc_type'] == "SN"){

            $this->db
                ->select(array("cc.code as cc_code","cc.type as cc_type","loc.name as type","COALESCE( sum(last_qty), 0) as avail_qty","COALESCE(sum(ccs.qty),0) as pick_qty"))
                ->from('cycle_counts cc')
                ->join('cycle_count_details ccd','ccd.cycle_count_id=cc.cc_id')
                ->join('item_receiving ir','ir.item_id=ccd.item_id')
                ->join('item_receiving_details ird','ird.item_receiving_id=ir.id')
                ->join('items itm','itm.id=ir.item_id')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('cycle_count_scanned ccs','ccs.cycle_count_id=cc.cc_id and ccs.unique_code=ird.unique_code','left')
                ->where('cc.code',$data['cc_code'])
                ->where('itm.code',$data['code'])
                ->where_not_in('ird.location_id',array(100,101,102,103,104,105,106))
                ->group_by('ird.location_id, cc.code, cc.type, loc.name');

        }else{

            $this->db
                ->select(array("cc.code as cc_code","cc.type as cc_type","itm.code as type","COALESCE( sum(last_qty), 0) as avail_qty","COALESCE(sum(ccs.qty),0) as pick_qty"))
                ->from('cycle_counts cc')
                ->join('cycle_count_details ccd','ccd.cycle_count_id=cc.cc_id')
                ->join('item_receiving_details ird','ird.location_id=ccd.location_id')
                ->join('items itm','itm.id=ird.item_id')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('cycle_count_scanned ccs','ccs.cycle_count_id=cc.cc_id and ccs.unique_code=ird.unique_code','left')
                ->where('cc.code',$data['cc_code'])
                ->where('loc.name',$data['code'])
                ->where_not_in('ird.location_id',array(100,101,102,103,104,105,106))
                ->group_by('itm.id,cc.code,cc.type,itm.code');

        }

        $data = $this->db->get();

        return $data->result_array();

    }

    public function delPost($post=array(),$cc=array()){
        $this->db->where('cycle_count_id',$cc['cc_id']);
        $this->db->where('unique_code',$post['kd_unik']);
        $this->db->delete('cycle_count_scanned');
    }

    public function getOuterLabelItem($params=array()){

        $this->db
            ->select('DISTINCT(unique_code) as unique_code')
            ->from('cycle_counts cc')
            ->join('location_areas loc_area','cc.warehouse_id=loc_area.warehouse_id')
            ->join('locations loc','loc.location_area_id=loc_area.id')
            ->join('item_receiving_details ird','loc.id=ird.location_id')
            // ->join("(SELECT warehouse_id FROM cycle_counts where code='".$params['cc_code']."') as cc",'loc_area.warehouse_id=cc.warehouse_id')
            ->where('last_qty >',0)
            ->where('cc.code',$params['cc_code'])
            ->where_not_in('loc.name',['GH-MIA'])
            ->where('parent_code',$params['parent_code'])
            ->group_by('unique_code');
        
        $data = $this->db->get()->result_array();

        return $data;
    }

}
	<?php
class receiving_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'receivings';
    private $tbl_ir = "item_receiving";
    private $table2 = 'suppliers';
    private $table3 = 'inbound';
    private $table4 = 'item_receiving_details';
    private $table5 = 'items';
    private $table6 = 'units';
    private $table7 = 'categories';
    private $table8 = 'supplier_barang';
    private $po_barang = 'purchase_order_barang';
    private $kurs   = 'kurs';

	public function getStatus(){
		$result = array();

		$this->db
			->select('status_id as id_status, name as nama_status')
			->from('status')
			->like('type','RECEIVING');

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function updateInboundStatus($idInbound){
		$result = array();

		$this->db
			->set('status_id', 5)
			->where_in('id_inbound', $idInbound);

		$q = $this->db->update('inbound');

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Update status inbound success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Update status inbound failed';
		}

		return $result;
	}

	public function finishTally($post = array()){
		$this->db->trans_start();

		$result = array();
		$rectime = date('Y-m-d H:i:s');

		$this->db
			->select('count(*) as total')
			->from('item_receiving_details ird')
			->join('item_receiving ir','ir.id=ird.item_receiving_id')
			->join('receivings r','r.id=ir.receiving_id')
			->where('r.id', $post['id_receiving']);

		$row = $this->db->get()->row_array();

		if($row['total'] <= 0){

			$result['status'] = 'ERR';
			$result['message'] = 'Cannot finish tally, because process tally not started yet.';

		}else{

			$this->db
				->select('start_tally, finish_tally')
				->from('receivings')
				->where('id', $post['id_receiving']);

			$row = $this->db->get()->row_array();

			$start = $row['start_tally'];
			$finish = $row['finish_tally'];

			if(empty($start)){

				$result['status'] = 'ERR';
				$result['message'] = 'Cannot finish tally, because process tally not started yet.';

			}else if(!empty($finish)){

				$result['status'] = 'ERR';
				$result['message'] = 'Process tally already finished.';

			}else{

				if(!empty($post['remark'])){
					$this->db->set('remark', $post['remark']);
				}

				$this->db
					->set('finish_tally', $rectime)
					->set('st_receiving', 1)
					->where('id', $post['id_receiving']);

				$q = $this->db->update('receivings');
				if($q){

					$result['status'] = 'OK';
					$result['message'] = 'Finish tally success';

					$this->db
						->select([
							"COALESCE(SUM(ir.qty),0) rec_qty",
							"
								COALESCE((
									SELECT SUM(first_qty)
									FROM item_receiving_details ird
									JOIN item_receiving irs ON ird.item_receiving_id=irs.id
									WHERE irs.receiving_id=r.id
									AND putaway_time IS NOT NULL
								),0) as put_qty
							"
						])
						->from('receivings r')
						->join('item_receiving ir','ir.receiving_id=r.id')
						->where('r.id', $post['id_receiving'])
						->group_by('r.code,r.id');

					$checkPutaway = $this->db->get()->row_array();
					if($checkPutaway['put_qty'] >= $checkPutaway['rec_qty']){

						$this->load_model('api_putaway_model');
						$this->api_putaway_model->finish($checkPutaway['code']);

						$result['message'] = 'Finish Tally & Putaway Success';

					}

				}else{
					$result['status'] = 'ERR';
					$result['message'] = 'Finish tally failed';
				}

			}

		}

		$this->db->trans_complete();

		return $result;
	}

	public function getPutAwayList($post = array()){

		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_inbound',
			2 => 'kd_receiving',
			3 => 'sort_date',
			4 => 'kd_unik',
			5 => 'kd_barang',
			6 => 'nama_barang',
			7 => 'loc_name'
		);

		$where = '';


		$this->db->start_cache();

		if(!empty($post['id_receiving'])){
			$this->db->where('rcv.id', $post['id_receiving']);
		}

		if(!empty($post['id_po'])){
			$this->db->where('rcv.po_id', $post['id_po']);
		}

		if(!empty($post['id_supplier'])){
			$this->db->where('inb.supplier_id', $post['id_supplier']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('rcv.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). ' 00:00:00');
			$this->db->where('rcv.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). ' 23:59:59');
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('inb.warehouse_id', $post['warehouse']);
		}

		if(!empty($post['location'])){
			$this->db->where_in('ird.location_id', $post['location']);
		}

		$this->db
			->from('inbound inb')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('sources s','s.source_id=inb.supplier_id')
			->join('item_receiving ir','ir.inbound_id=inb.id_inbound')
			->join('receivings rcv','rcv.id=ir.receiving_id')
			->join('item_receiving_details ird','ird.item_receiving_id=ir.id')
			->join('items itm','itm.id=ir.item_id')
			->join('locations loc','loc.id=ird.location_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select([
				"inb.code as kd_inbound", "rcv.code as kd_receiving",
				"rcv.date as sort_date",
				"TO_CHAR(rcv.date, 'DD/MM/YYYY') AS tanggal_receiving",
				"ird.rfid_tag kd_unik", "itm.code as kd_barang", "itm.name as nama_barang", "loc.name as loc_name"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		$action = '';

		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_inbound'];
			$nested[] = $row[$i]['kd_receiving'];
			$nested[] = $row[$i]['tanggal_receiving'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['loc_name'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getWidget(){
		$result = array();
		$today = date('Y-m-d');

		$sql = "SELECT
					 COALESCE(SUM(rec_qty.total_item), 0) AS total_item, COALESCE(SUM(rec_qty.total_qty), 0) AS total_qty,
					 COALESCE(SUM(rec_tally.total_tally), 0) AS total_tally, COALESCE(SUM(rec_tally.total_qty_tally), 0) AS total_qty_tally,
					 COALESCE(SUM(rec_putaway.total_putaway), 0) AS total_putaway, COALESCE(SUM(rec_putaway.total_qty_putaway), 0) AS total_qty_putaway,
					 -- rcv.finish_putaway,rcv.date,
					 AVG(extract (epoch from (rcv.finish_putaway - rcv.date))::integer/60) AS average
				FROM
					receivings rcv
				JOIN inbound inb
					ON inb.id_inbound=rcv.po_id
				JOIN warehouses wh
					ON wh.id=inb.warehouse_id
				JOIN users_warehouses uw
					ON uw.warehouse_id=wh.id
				JOIN users usr
					ON usr.id=uw.user_id AND usr.id=".$this->session->userdata('user_id')."
				LEFT JOIN
				(
					SELECT
						receiving_id, COUNT(total_item) AS total_item, SUM(total_qty) AS total_qty
					FROM
					(
						SELECT
							ir.receiving_id, ir.item_id AS total_item, SUM(ir.qty) AS total_qty
						FROM
							item_receiving ir
						JOIN items itm
							ON itm.id=ir.item_id
						GROUP BY
							ir.item_id, ir.receiving_id
					) AS table_1
					GROUP BY
						receiving_id
				) AS rec_qty
					ON rec_qty.receiving_id=rcv.id
				LEFT JOIN
				(
					SELECT
						receiving_id, COUNT(total_tally) AS total_tally, SUM(total_qty_tally) AS total_qty_tally
					FROM
					(
						SELECT
							ir.receiving_id, COUNT(*) AS total_tally, SUM(qty) AS total_qty_tally
						FROM
							item_receiving ir
						JOIN items itm
							ON itm.id=ir.item_id
						JOIN receivings srcv
							ON srcv.id=ir.receiving_id
						WHERE
							srcv.start_tally IS NULL
						AND
							srcv.start_tally IS NULL
						GROUP BY
							ir.item_id, ir.receiving_id
					) AS table_2
					GROUP BY
						receiving_id
				) AS rec_tally
					ON rec_tally.receiving_id=rcv.id
				LEFT JOIN
				(
					SELECT
						receiving_id, COUNT(total_putaway) AS total_putaway, SUM(total_qty_putaway) AS total_qty_putaway
					FROM
					(
						SELECT
							ir.receiving_id, COUNT(*) AS total_putaway, SUM(qty) AS total_qty_putaway
						FROM
							item_receiving ir
						JOIN items itm
							ON itm.id=ir.item_id
						JOIN receivings srcv
							ON srcv.id=ir.receiving_id
						WHERE
							srcv.start_tally IS NOT NULL
						AND
							srcv.finish_tally IS NOT NULL
						AND
							srcv.finish_putaway IS NULL
						GROUP BY
							ir.item_id, ir.receiving_id
					) AS table_3
					GROUP BY
						receiving_id
				) AS rec_putaway
					ON rec_putaway.receiving_id=rcv.id
				WHERE
					DATE(rcv.date) = '$today'";

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function startTally($post = array()){
		$result = array();
		$rectime = date('Y-m-d H:i:s');

		$sql = 'UPDATE receiving SET start_tally = \''.$rectime.'\' WHERE id_receiving=\''.$post['id_receiving'].'\'';
		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = '';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = '';
		}

		return $result;
	}

	public function getInboundDetail($post = array()){
		$result = array();

		$sql = 'SELECT
					brg.id_barang, kd_barang, nama_barang, jumlah_barang, nama_satuan
				FROM
					inbound inb
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound
				LEFT JOIN barang brg
					ON brg.id_barang=inbbrg.id_barang
				LEFT JOIN satuan stn
					ON stn.id_satuan=brg.id_satuan
				WHERE
					inb.id_inbound=\''.$post['id_inbound'].'\'
				ORDER BY
					kd_barang ASC';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

	public function getRcvCode($post = array()){
		$result = array();

		$sql = 'SELECT
					rcv.id_receiving AS id, kd_receiving AS text,
					(
						CASE
							WHEN id_receiving_qty IS NOT NULL
						THEN
							\'Saved\'
						ELSE
							\'\'
						END
					) AS flag
				FROM
					receiving rcv
				LEFT JOIN receiving_qty rcvqty
					ON rcvqty.id_receiving=rcv.id_receiving
				WHERE
					rcv.id_receiving IN('.$post['id_receiving'].')';

		$result = $this->db->query($sql)->result_array();
		$len = count($result);

		for($i = 0; $i < $len; $i++){
			$result[$i]['text'] = '<div style="">' . $result[$i]['text'] . '<label class="label label-success pull-right">' . $result[$i]['flag'] . '</label></div>';
		}

		return $result;
	}

	public function getDetailItemDataTable($post = array()){

		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'qty_doc',
			4 => 'qty_received'
		);

		$where = '';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
				(
					SELECT
						brg.id_barang
					FROM
						inbound inb
					LEFT JOIN receiving rcv
						ON rcv.id_po=inb.id_inbound
					LEFT JOIN inbound_barang inbbrg
						ON inbbrg.id_inbound=inb.id_inbound
					LEFT JOIN receiving_qty rcvqty
						ON rcvqty.id_receiving=rcv.id_receiving
							AND rcvqty.id_barang=inbbrg.id_barang
					LEFT JOIN barang brg
						ON brg.id_barang=inbbrg.id_barang
					LEFT JOIN satuan st
						ON st.id_satuan=brg.id_satuan
					WHERE
						inb.id_inbound=\''.$post['id_inbound'].'\'
					GROUP BY
						brg.id_barang
				) AS table_1';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					brg.id_barang, brg.kd_barang,
					brg.nama_barang, IFNULL(rcvqty.qty,0) AS qty_doc,
					(
						SELECT
							COUNT(id_barang)
						FROM
							receiving_barang rcvbrg
						WHERE
							id_receiving=rcv.id_receiving
						AND
							id_barang=brg.id_barang
					) AS qty_received,
					nama_satuan
				FROM
					receiving rcv
				LEFT JOIN inbound inb
					ON rcv.id_po=inb.id_inbound
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound
				LEFT JOIN receiving_qty rcvqty
					ON rcvqty.id_receiving=rcv.id_receiving
						AND rcvqty.id_barang=inbbrg.id_barang
				LEFT JOIN barang brg
					ON brg.id_barang=inbbrg.id_barang
				LEFT JOIN satuan st
					ON st.id_satuan=brg.id_satuan
				WHERE
					rcv.id_receiving=\''.$post['id_receiving'].'\'
				GROUP BY
					brg.id_barang';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		$action = '';

		for($i=0;$i<$totalFiltered;$i++){

			$uom = $row[$i]['nama_satuan'];
			if(empty($uom))
				$uom = '';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['qty_doc'] .  ' ' . $uom;
			$nested[] = $row[$i]['qty_received'] .  ' ' . $uom;

			$disc = $row[$i]['qty_doc'] - $row[$i]['qty_received'];

			$nested[] = $disc . ' ' .$uom;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItemInboundReceiving($post = array()){

		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_inbound_barang',
			1 => '',
			2 => '',
			3 => 'id_inbound_barang',
			4 => 'kd_barang',
			5 => 'nama_barang',
			6 => 'ordered',
			7 => 'qty_doc',
			8 => 'qty_received',
			9 => 'qty_putaway',
			10 => 'discrepenci'
		);

		$sql = "SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						COUNT(*)
					FROM
						receivings rcv
					LEFT JOIN hp_receiving_inbound hri
						ON hri.receiving_id=rcv.id
					LEFT JOIN inbound inb
						ON inb.id_inbound=hri.inbound_id
					LEFT JOIN inbound_item inbitm
						ON inbitm.inbound_id=inb.id_inbound
					LEFT JOIN item_receiving ir
						ON ir.receiving_id=rcv.id
							AND ir.item_id=inbitm.item_id
					LEFT JOIN items itm
						ON itm.id=inbitm.item_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					WHERE
						rcv.id=".$post['id_receiving']."
					GROUP BY itm.id
				) AS table_1";

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = "SELECT
					ir.id as id_inbound_barang,
					itm.id as id_barang,
					r.id as id_receiving,
					itm.code as kd_barang,
					(CASE itm.preorder WHEN 1 THEN CONCAT(itm.name,' - PREORDER') ELSE itm.name END) as nama_barang,
					COALESCE(SUM(inbitm.qty), 0)AS ordered,
					COALESCE(ir.qty,0) AS qty_doc,
					COALESCE((SELECT SUM(first_qty) FROM item_receiving_details where item_receiving_id=ir.id),0) as qty_received,
					-- COALESCE(SUM(ird.first_qty),0) as qty_received,
					COALESCE(tput.qty_putaway,0) AS qty_putaway,
				 	COALESCE(unt.name, '') AS nama_satuan,
				 	(COALESCE(ir.qty,0) - COALESCE(tput.qty_putaway)) as discrepencies,
				 	COALESCE(tgood.qty,0) as good_qty, COALESCE(treject.qty,0) as ng_qty
				FROM
					receivings r
				LEFT JOIN hp_receiving_inbound hri ON hri.receiving_id=r.id
				LEFT JOIN inbound inb ON inb.id_inbound=hri.inbound_id
				LEFT JOIN inbound_item inbitm ON inbitm.inbound_id = inb.id_inbound
				LEFT JOIN item_receiving ir ON ir.receiving_id = r.id AND ir.item_id=inbitm.item_id
				LEFT JOIN items itm ON itm.id=inbitm.item_id
				LEFT JOIN units unt ON unt.id=itm.unit_id
				-- LEFT JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
				LEFT JOIN (	SELECT
								ir.item_id,
								rcv.id as receiving_id,
								COALESCE(SUM(ird.first_qty), 0) AS qty_putaway
							FROM
								receivings rcv
							LEFT JOIN inbound inb ON inb.id_inbound = rcv.po_id
							LEFT JOIN item_receiving ir ON ir.receiving_id=rcv.id
							LEFT JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
							WHERE
								rcv.id = ".$post['id_receiving']."
							AND ird.putaway_time IS NOT NULL
							GROUP BY
								ir.item_id, rcv.id) as tput on tput.item_id = itm.id and tput.receiving_id = r.id
				LEFT JOIN (	SELECT
								ir.item_id,
								rcv.id as receiving_id,
								COALESCE(SUM(ird.first_qty), 0) AS qty
							FROM
								receivings rcv
							LEFT JOIN inbound inb ON inb.id_inbound = rcv.po_id
							LEFT JOIN item_receiving ir ON ir.receiving_id=rcv.id
							LEFT JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
							WHERE
								rcv.id = ".$post['id_receiving']."
							AND qc_id=1
							GROUP BY
								ir.item_id, rcv.id) as tgood on tgood.item_id = itm.id and tgood.receiving_id = r.id
				LEFT JOIN (	SELECT
								ir.item_id,
								rcv.id as receiving_id,
								COALESCE(SUM(ird.first_qty), 0) AS qty
							FROM
								receivings rcv
							LEFT JOIN inbound inb ON inb.id_inbound = rcv.po_id
							LEFT JOIN item_receiving ir ON ir.receiving_id=rcv.id
							LEFT JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
							WHERE
								rcv.id = ".$post['id_receiving']."
							AND qc_id IN (2,3)
							GROUP BY
								ir.item_id, rcv.id) as treject on treject.item_id = itm.id and treject.receiving_id = r.id
				WHERE
					r.id = ".$post['id_receiving']."
					AND ir.qty > 0
				GROUP BY
					itm.id, ir.id, r.id, ir.qty, tput.qty_putaway, unt.name, itm.code, itm.name, tgood.qty, treject.qty, itm.preorder
				";

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['length']." OFFSET ".$requestData['start'];

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();

		for($i = 0; $i < $totalFiltered; $i++){

			$qtyDoc = $row[$i]['qty_doc'];
			$rcv = $row[$i]['qty_received'];
			$put = $row[$i]['qty_putaway'];

			$nested = array();
			$nested[] = $row[$i]['id_inbound_barang'];
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $row[$i]['id_receiving'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['ordered'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $qtyDoc . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $rcv . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $put . ' ' . $row[$i]['nama_satuan'];
			$nested[] = abs($qtyDoc - $put) . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['good_qty'].' '.$row[$i]['nama_satuan'];
			$nested[] = $row[$i]['ng_qty'].' '.$row[$i]['nama_satuan'];
			$nested[] = '';

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItemInboundReceiving2($post=array()){

		$this->db
			->select([
					"rfid_tag as kd_unik", "itm.code as kd_barang", "ird.parent_code as kd_parent","COALESCE(TO_CHAR(tgl_exp, 'DD/MM/YYYY'), '-') AS tgl_exp",
					"TO_CHAR(tgl_in, 'DD/MM/YYYY') AS tgl_in",
					"loc.name as loc_name","qc.code as kd_qc", "COALESCE(usr.user_name, usr1.user_name) AS user_name",'kd_batch'
			])
			->from('item_receiving_details ird')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('items itm','itm.id=ir.item_id','left')
			->join('locations loc','loc.id=ird.location_id','left')
			->join('qc','qc.id=ird.qc_id','left')
			->join('users usr','usr.id=ird.user_id_putaway','left')
			->join('users usr1','usr1.id=ird.user_id_receiving','left')
			->where('ir.receiving_id',$post['id_receiving'])
			->order_by('itm.id');

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();

		for($i=0;$i<$totalFiltered;$i++){

			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';

			$nested = array();
			$nested[] = $i + 1;
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['kd_parent'];
			$nested[] = $row[$i]['kd_batch'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['kd_qc'];
			$nested[] = $row[$i]['user_name'];

			$data[] = $nested;
		}

		return $data;

	}

	public function getDetailItemInboundReceivingExcel($post = array()){

		$result = array();

		$requestData = $_REQUEST;

		$sql = "SELECT
					inbitm.id as id_inbound_barang,
					itm.id as id_barang,
					r.id as id_receiving,
					itm.code as kd_barang,
					(CASE itm.preorder WHEN 1 THEN CONCAT(itm.name, ' - PREORDER') ELSE itm.name END) as nama_barang,
					COALESCE(inbitm.qty, 0)AS ordered,
					COALESCE(ir.qty,0) AS qty_doc,
					COALESCE(SUM(first_qty),0) as qty_received,
					COALESCE(tput.qty_putaway,0) AS qty_putaway,
				 	COALESCE(unt.name, '') AS nama_satuan,
				 	(COALESCE(ir.qty,0) - COALESCE(tput.qty_putaway)) as discrepenci
				FROM
					receivings r
				LEFT JOIN inbound inb ON inb.id_inbound = r.po_id
				LEFT JOIN inbound_item inbitm ON inbitm.inbound_id = inb.id_inbound
				LEFT JOIN item_receiving ir ON ir.receiving_id = r.id
				AND ir.item_id=inbitm.item_id
				LEFT JOIN items itm ON itm.id=inbitm.item_id
				LEFT JOIN units unt ON unt.id=itm.unit_id
				LEFT JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
				LEFT JOIN (	SELECT
								ir.item_id,
								rcv.id as receiving_id,
								COALESCE(SUM(first_qty), 0) AS qty_putaway
							FROM
								receivings rcv
							LEFT JOIN inbound inb ON inb.id_inbound = rcv.po_id
							LEFT JOIN item_receiving ir ON ir.receiving_id=rcv.id
							LEFT JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
							WHERE
								rcv.id = ".$post['id_receiving']."
							AND ird.putaway_time IS NOT NULL
							GROUP BY
								ir.item_id, rcv.id) as tput on tput.item_id = itm.id and tput.receiving_id = r.id
				WHERE
					r.id = ".$post['id_receiving']."
				GROUP BY
					itm.id, inbitm.id, r.id, ir.qty, tput.qty_putaway, unt.name, itm.code, itm.name, inbitm.qty
				";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();

		for($i = 0; $i < $totalFiltered; $i++){

			$qtyDoc = $row[$i]['qty_doc'];
			$rcv = $row[$i]['qty_received'];
			$put = $row[$i]['qty_putaway'];

			$nested = array();
			$nested[] = ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['ordered'];
			$nested[] = $qtyDoc;
			$nested[] = $rcv;
			$nested[] = $put;
			$nested[] = abs($qtyDoc - $put) ;
			$nested[] = $row[$i]['nama_satuan'];
			$nested[] = '';

			$data[] = $nested;
		}

		return $data;
	}

	public function getDetailItemReceiving($post = array()){

		$result = array();
		$row 	= array();

		if(!empty($post['id_receiving'])){

			$this->db
				->select([
					"itm.id as id_barang",
					"itm.code as kd_barang",
					"itm.name as nama_barang",
					"COALESCE(SUM(ir.qty),0) as qty_doc",
					"COALESCE((SELECT SUM(last_qty) FROM item_receiving_details where item_receiving_id=ir.id), 0) as qty_received",
					"unt.name as nama_satuan",
					"inb.id_inbound",
					"string_agg(DISTINCT CAST(CONCAT(inb.code,' - ',COALESCE(s.source_name,d.destination_name)) as VARCHAR), ',') as kd_inbound",
					"ir.inbound_id"
				])
				->from('inbound inb')
				->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound','left')
				->join('sources s','s.source_id=inb.supplier_id','left')
				->join('destinations d','d.destination_id=inb.supplier_id','left')
				->join('item_receiving ir','ir.inbound_id=inb.id_inbound and ir.item_id=inbitm.item_id','left')
				->join('receivings r','r.id=ir.receiving_id','left')
				->join('items itm','itm.id=inbitm.item_id','left')
				->join('units unt','unt.id=itm.unit_id','left')
				// ->where_in('inb.id_inbound', $post['id_inbound'])
				->where('r.id', $post['id_receiving'])
				// ->where('ir.qty != 0', null)
				->group_by('itm.id, unt.name, itm.code, itm.name, inb.id_inbound, ir.inbound_id, ir.id')
				->order_by('itm.id','ASC');

			$row = $this->db->get()->result_array();

		}


		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['message'] = '';

		return $result;
	}

	public function getDetailItem($post = array()){

		$row = array();

		if(!empty($post['id_inbound'])){

			if(!is_array($post['id_inbound'])){
				$post['id_inbound'] = array($post['id_inbound']);
			}

			$this->db
				->select([
					"itm.id as id_barang",
					"itm.code as kd_barang",
					"itm.name as nama_barang",
					"COALESCE(SUM(inbitm.qty),0) as inb_qty",
					"(select 0) as qty_doc",
					"COALESCE(sum(first_qty), 0) as qty_received",
					"unt.name as nama_satuan",
					"inb.id_inbound",
					"string_agg(DISTINCT CAST(CONCAT(inb.code,' - ',COALESCE(s.source_name,d.destination_name)) as VARCHAR), ',') as kd_inbound"
					// "string_agg(DISTINCT CAST(CONCAT(inb.id_inbound , '/:*', inb.code,' - ',COALESCE(s.source_name,' - ')) as VARCHAR), ',') as kd_inbound"
				])
				->from('inbound inb')
				->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound','left')
				->join('sources s','s.source_id=inb.supplier_id','LEFT')
				->join('destinations d','d.destination_id=inb.supplier_id','LEFT')
				->join('receivings r','r.po_id=inb.id_inbound','left')
				->join('item_receiving ir','ir.receiving_id=r.id and ir.item_id=inbitm.item_id','left')
				->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left')
				->join('items itm','itm.id=inbitm.item_id','left')
				->join('units unt','unt.id=itm.unit_id','left')
				->where_in('inb.id_inbound', $post['id_inbound'])
				->group_by('itm.id, unt.name, itm.code, itm.name, inb.id_inbound')
				->order_by('itm.id','ASC');

			$row = $this->db->get()->result_array();

/*
			foreach ($row as &$r) {
				$inbound = explode(",", $r['kd_inbound']);
				$dInbound=array();
				foreach ($inbound as $inb) {
					$dataInbound = explode("/:*", $inb);
					$dInbound[$dataInbound[0]] = $dataInbound[1];
				}
				$r['kd_inbound'] = $dInbound;
			}
*/
		}

		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['message'] = '';

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$sql = 'SELECT
					rcv.id_receiving, inb.id_inbound, kd_inbound, kd_receiving,
					IFNULL(vehicle_plate,\'-\') AS vehicle_plate,
					DATE_FORMAT(tanggal_receiving, \'%d\/%m\/%Y\') AS tanggal_receiving,
					DATE_FORMAT(tanggal_receiving, \'%H:%i\') AS time_receiving,
					dock, delivery_doc, kd_supplier, nama_supplier, alamat_supplier, telepon_supplier,
					cp_supplier, nama_status, inbound_document_name,
					DATE_FORMAT(tanggal_inbound, \'%d\/%m\/%Y\') AS tanggal_inbound,
					hui.user_name AS user_name_inbound, hur.user_name AS user_name_receiving,
					start_tally, finish_tally, start_putaway, finish_putaway,
					(
						CASE
							WHEN start_tally IS NULL
								THEN \'-\'
							WHEN start_tally IS NOT NULL AND finish_tally IS NULL
								THEN CONCAT(\'Started\',\' at \',
									(
										CASE
											WHEN DATE(start_tally) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(start_tally, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(start_tally, \'%H:%i\')
										END
									)
								)
							WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL
								THEN CONCAT(\'Finished\',\' at \',
									(
										CASE
											WHEN DATE(finish_tally) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(finish_tally, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(finish_tally, \'%H:%i\')
										END
									)
								)
						END
					) AS tally,
					(
						CASE
							WHEN start_putaway IS NULL
								THEN \'-\'
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
								THEN CONCAT(\'Started\',\' at \',
									(
										CASE
											WHEN DATE(start_putaway) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(start_putaway, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(start_putaway, \'%H:%i\')
										END
									)
								)
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
								THEN CONCAT(\'Finished\',\' at \',
									(
										CASE
											WHEN DATE(finish_putaway) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(finish_putaway, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(finish_putaway, \'%H:%i\')
										END
									)
								)
						END
					) AS putaway,
					(
						SELECT
							nama_status
						FROM
							m_status_receiving
						WHERE
							id_status = (
								CASE
									WHEN start_tally IS NULL
										THEN \'1\'
									WHEN start_tally IS NOT NULL AND finish_tally IS NULL
										THEN \'2\'
									WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
										THEN \'3\'
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
										THEN \'4\'
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
										THEN \'5\'
								END
							)
					) AS status
				FROM
					receiving rcv
				LEFT JOIN hp_receiving_inbound hri
					ON hri.receiving_id=rcv.id
				LEFT JOIN inbound inb
					ON inb.id_inbound=hri.inbound_id
/*				LEFT JOIN supplier spl
					ON spl.id_supplier=inb.id_supplier
*/
				LEFT JOIN sources s
					ON s.dource_id=inb.supplier_id
				LEFT JOIN destinations d
					ON d.destination_id=inb.supplier_id
				LEFT JOIN m_status_inbound msinb
					ON msinb.id_status_inbound=inb.id_status_inbound
				LEFT JOIN m_inbound_document minbd
					ON minbd.id_inbound_document=inb.id_inbound_document
				LEFT JOIN hr_user hui
					ON hui.user_id=inb.user_id
				LEFT JOIN hr_user hur
					ON hur.user_id=rcv.user_id
				WHERE
					id_receiving=\''.$post['id_receiving'].'\'';

		$this->db
			->select([
				"r.id as id_receiving","inb.id_inbound",
				"string_agg(DISTINCT inb.code, ',') as kd_inbound",
				"r.code as kd_receiving",
				"COALESCE(vehicle_plate, '-') as vehicle_plate","TO_CHAR(r.date, 'DD/MM/YYYY') as tanggal_receiving",
				"TO_CHAR(r.date, 'HH24:MI') as time_receiving", "dock", "delivery_doc",
				// "spl.code as kd_supplier", "spl.address as alamat_supplier","spl.phone as telepon_supplier", "spl.contact_person as cp_supplier"
				"COALESCE(s.source_code,d.destination_code) as kd_supplier",
				"COALESCE(s.source_name,d.destination_name) as nama_supplier",
				"COALESCE(s.source_address,d.destination_address) as alamat_supplier",
				"COALESCE(s.source_phone,d.destination_phone) as telepon_supplier",
				"COALESCE(s.source_contact_person,d.destination_contact_person) as cp_supplier",
				"st.name as nama_status", "doc.name as inbound_document_name",
				"TO_CHAR(inb.date, 'DD/MM/YYYY') as tanggal_inbound", "usr_i.user_name as user_name_inbound","usr_r.user_name as user_name_receiving",
				"r.start_tally","r.finish_tally","r.start_putaway","r.finish_putaway",
				"(
					CASE
						WHEN start_tally IS NULL
							THEN '-'
						WHEN start_tally IS NOT NULL AND finish_tally IS NULL
							THEN CONCAT('Started','<br>',
								(
									CASE
										WHEN DATE(start_tally) != DATE(r.date)
											THEN TO_CHAR(start_tally, 'DD/MM/YYYY HH24:MI')
										ELSE
											TO_CHAR(start_tally, 'HH24:MI')
									END
								)
							)
						WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL
							THEN CONCAT(TO_CHAR(finish_tally, 'HH24:MI'),'<br>',
								CONCAT('<span class=\"small-date\">', TO_CHAR(finish_tally, 'DD/MM/YYYY'), '</span>'))
					END
				) AS tally",
				"(
					CASE
						WHEN start_putaway IS NULL
							THEN '-'
						WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
							THEN CONCAT('Started','<br>',
								(
									CASE
										WHEN DATE(start_putaway) != DATE(r.date)
											THEN TO_CHAR(start_putaway, 'DD/MM/YYYY HH24:MI')
										ELSE
											TO_CHAR(start_putaway, 'HH24:MI')
									END
								)
							)
						WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
							THEN CONCAT(TO_CHAR(finish_putaway, 'HH24:MI'),'<br>',
								CONCAT('<span class=\"small-date\">', TO_CHAR(finish_putaway, 'DD/MM/YYYY'), '</span>'))
					END
				) AS putaway",
				"(
					CASE
						WHEN start_putaway IS NULL
							THEN '-'
						WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
							THEN CONCAT('Started','<br>',
								(
									CASE
										WHEN DATE(start_putaway) != DATE(r.date)
											THEN TO_CHAR(start_putaway, 'DD/MM/YYYY HH24:MI:SS')
										ELSE
											TO_CHAR(start_putaway, 'HH24:MI:SS')
									END
								)
							)
						WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
							THEN CONCAT(TO_CHAR(finish_putaway, 'HH24:MI:SS'),'<br>',
								CONCAT('<span class=\"small-date\">', TO_CHAR(finish_putaway, 'DD/MM/YYYY'), '</span>'))
					END
				) AS putaway_second",
				"COALESCE(st.name, (
					SELECT
						name as nama_status
					FROM
						status
					WHERE
						status_id = (
							CASE
								WHEN start_tally IS NULL
									THEN 11
								WHEN start_tally IS NOT NULL AND finish_tally IS NULL
									THEN 12
								WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
									THEN 13
								WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
									THEN 14
								WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
									THEN 15
							END
						)
					)
				) AS status",
				"(
					SELECT
						COALESCE(COUNT(*), 0)
					FROM
						item_receiving_details ird
					LEFT JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					WHERE
						ir.receiving_id=r.id
				) AS scanned"
			])
			->from('receivings r')
			// ->join('hp_receiving_inbound hri','hri.receiving_id=r.id','left')
			->join('item_receiving ir','ir.receiving_id=r.id','left')
			->join('inbound inb','inb.id_inbound=ir.inbound_id','left')
			->join('sources s','s.source_id=inb.supplier_id','left')
			->join('destinations d','d.destination_id=inb.supplier_id','left')
			// ->join('suppliers spl','spl.id=inb.supplier_id','left')
			->join('status st','st.status_id=r.status_id','left')
			->join('documents doc','doc.document_id=inb.document_id','left')
			->join('users usr_i','usr_i.id=inb.user_id','left')
			->join('users usr_r','usr_r.id=r.user_id','left')
			->group_by('r.id, inb.id_inbound, s.source_code, s.source_name, s.source_address, s.source_phone, s.source_contact_person, d.destination_code, d.destination_name, d.destination_address, d.destination_phone, d.destination_contact_person, st.name, doc.name, usr_i.user_name, usr_r.user_name')
			->where('r.id',$post['id_receiving']);

		$result = $this->db->get()->row_array();

		return $result;
	}

	public function getReceivingCode($post = array()){
		$result = array();

		$this->db
			->select('id as id_receiving, code as kd_receiving')
			->from('receivings rcv')
			->like('LOWER(code)', strtolower($post['query']));

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getInboundList($post = array()){
		$result = array();

		if(array_key_exists('id_inbound', $post)){
			if($post['id_inbound']){
				$this->db->where_in('inb.id_inbound',$post['id_inbound']);
			}
		}

		if(array_key_exists('id_barang', $post)){
			if($post['id_barang']){
				$this->db->where_in('inbitm.item_id',$post['id_barang']);
			}
		}

		$this->db
			->select(['inb.id_inbound','inb.code as kd_inbound','COALESCE(s.source_id,d.destination_id)'])
			->select(["CONCAT(inb.code,' - ',COALESCE(s.source_name,d.destination_name) ) as kd_inbound"])
			->from('inbound inb')
			->join('sources s','s.source_id=inb.supplier_id','LEFT')
			->join('destinations d','d.destination_id=inb.supplier_id','LEFT')
			->join('status st','st.status_id=inb.status_id')
			->where_not_in('inb.status_id', [4,100])
			->order_by('id_inbound','desc');

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getInboundCode($post = array()){
		$result = array();

		$where = '';
		if(isset($post['params'])){
			if($post['params'] == 'open'){
				$this->db->where('inb.status_id <>', 4);
			}
		}

		$this->db
			->select('id_inbound, inb.code as kd_inbound')
			->from('inbound inb')
			->join('status st','st.status_id=inb.status_id')
			->like('LOWER(inb.code)',strtolower($post['query']))
			->order_by('id_inbound','DESC');

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_receiving',
			1 => '',
			2 => 'kd_receiving',
			3 => 'kd_inbound',
			4 => 'vehicle_plate'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['id_receiving'])){
			$this->db->where('r.id',$post['id_receiving']);
		}

		if(!empty($post['id_po'])){
			$this->db->where('ir.inbound_id',$post['id_po']);
		}

		if(!empty($post['id_supplier'])){
			$this->db->where('inb.supplier_id', $post['id_supplier']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('r.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'));
			$this->db->where('r.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->modify('+1 day')->format('Y-m-d'));
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('inb.warehouse_id', $post['warehouse']);
		}

		if($post['has_preorder'] != ""){
			$this->db->where_in('itm.preorder', $post['has_preorder']);
		}

		if(!empty($post['status'])){

			$sts = $post['status'];

			$status = array(
				0 => '',
				11 => ' ( start_tally IS NULL )',
				12 => ' ( start_tally IS NOT NULL AND finish_tally IS NULL )',
				13 => ' ( finish_tally IS NOT NULL AND start_putaway IS NULL )',
				14 => ' ( start_putaway IS NOT NULL AND finish_putaway IS NULL )',
				15 => ' ( start_putaway IS NOT NULL AND finish_putaway IS NOT NULL )'
			);

			$where .= '(';

				for ($i=0; $i < count($post['status']); $i++) {

				if($i > 0){

					$where .= 'OR '.$status[$post['status'][$i]];

				}else{
					$where .= $status[$post['status'][$i]];
				}
			}

			$where .= ')';

			$this->db->where($where, NULL);

		}

		$this->db
			->select('DISTINCT(r.id) as id_receiving')
			->from('receivings r')
			->join('item_receiving ir','ir.receiving_id=r.id')
			// ->join('hp_receiving_inbound hri','hri.receiving_id=r.id')
			->join('inbound inb','inb.id_inbound=ir.inbound_id')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('suppliers spl','spl.id=inb.supplier_id','left')
			->join('sources s','inb.supplier_id=s.source_id','left')
			->join('destinations d','inb.supplier_id=d.destination_id','left')
			->join('status st','st.status_id=r.status_id','left')
			->join('documents doc','doc.document_id=inb.document_id','left')
			->join("(SELECT	ir.receiving_id, MAX(itm.preorder) as preorder FROM item_receiving ir JOIN items itm ON itm.id=ir.item_id GROUP BY ir.receiving_id) as itm","itm.receiving_id=r.id",'left')
			->where("doc.code != 'IMPORT'",null)
			->group_by('r.id');

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->group_by('r.code, r.vehicle_plate, r.date, r.start_tally, r.finish_tally, r.start_putaway, r.finish_putaway, r.remark, itm.preorder, st.name')
			->select([
				"string_agg(DISTINCT CAST(inb.id_inbound as VARCHAR), ',') as id_inbound",
				"string_agg(DISTINCT inb.code, ',') as kd_inbound",
				"string_agg(DISTINCT s.source_name, ',') as source",
				"string_agg(DISTINCT d.destination_name, ',') as destination",
				"r.id as id_receiving",
				"r.code as kd_receiving",
				"COALESCE(vehicle_plate, '-') as vehicle_plate","TO_CHAR(r.date, 'DD/MM/YYYY') as tanggal_receiving",
				"TO_CHAR(r.date, 'HH24:MI') as time_receiving","TO_CHAR(r.date, 'HH24:MI:SS') as time_receiving_second",
				"r.start_tally","r.finish_tally","r.start_putaway","r.finish_putaway","COALESCE(r.remark, '<div align=center> - </div>') as remark",
				"(
					CASE
						WHEN start_tally IS NULL
							THEN '-'
						WHEN start_tally IS NOT NULL AND finish_tally IS NULL
							THEN CONCAT('Started','<br>',
								(
									CASE
										WHEN DATE(start_tally) != DATE(r.date)
											THEN TO_CHAR(start_tally, 'DD/MM/YYYY HH24:MI')
										ELSE
											TO_CHAR(start_tally, 'HH24:MI')
									END
								)
							)
						WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL
							THEN CONCAT(TO_CHAR(finish_tally, 'HH24:MI'),'<br>',
								CONCAT('<span class=\"small-date\">', TO_CHAR(finish_tally, 'DD/MM/YYYY'), '</span>'))
					END
				) AS tally",
				"(
					CASE
						WHEN start_putaway IS NULL
							THEN '-'
						WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
							THEN CONCAT('Started','<br>',
								(
									CASE
										WHEN DATE(start_putaway) != DATE(r.date)
											THEN TO_CHAR(start_putaway, 'DD/MM/YYYY HH24:MI')
										ELSE
											TO_CHAR(start_putaway, 'HH24:MI')
									END
								)
							)
						WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
							THEN CONCAT(TO_CHAR(finish_putaway, 'HH24:MI'),'<br>',
								CONCAT('<span class=\"small-date\">', TO_CHAR(finish_putaway, 'DD/MM/YYYY'), '</span>'))
					END
				) AS putaway",
				"(
					CASE
						WHEN start_putaway IS NULL
							THEN '-'
						WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
							THEN CONCAT('Started','<br>',
								(
									CASE
										WHEN DATE(start_putaway) != DATE(r.date)
											THEN TO_CHAR(start_putaway, 'DD/MM/YYYY HH24:MI:SS')
										ELSE
											TO_CHAR(start_putaway, 'HH24:MI:SS')
									END
								)
							)
						WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
							THEN CONCAT(TO_CHAR(finish_putaway, 'HH24:MI:SS'),'<br>',
								CONCAT('<span class=\"small-date\">', TO_CHAR(finish_putaway, 'DD/MM/YYYY'), '</span>'))
					END
				) AS putaway_second",
				"COALESCE(st.name,
					(
						SELECT
							name as nama_status
						FROM
							status
						WHERE
							status_id = (
								CASE
									WHEN start_tally IS NULL
										THEN 11
									WHEN start_tally IS NOT NULL AND finish_tally IS NULL
										THEN 12
									WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
										THEN 13
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
										THEN 14
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
										THEN 15
								END
							)
					)
				) AS status",
				"itm.preorder"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();

		$totalFiltered = count($row);

		$data = array();
		$action = '';

		for($i=0;$i<$totalFiltered;$i++){

			$class = '';
			$class1 = '';
			$class2 = '';
			$class3 = 'disabled-link';

			if($row[$i]['finish_tally'] != NULL || $row[$i]['start_tally'] != NULL){
				$class = 'disabled-link';
			}

			if($row[$i]['finish_tally'] != NULL){
				$class2 = 'disabled-link';
			}


			if($row[$i]['finish_tally'] != NULL){
				$class1 = 'disabled-link';
			}else{
				if($row[$i]['start_tally'] == NULL)
					$class1 = 'disabled-link';
				else
					$class1 = '';
			}

			if($row[$i]['finish_putaway'] != NULL){
				$class3='';
			}

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a class="data-table-tally '.$class2.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-print"></i> Print Tally</a>
							</li>
							<li>
								<a class="data-table-barcode '.$class2.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-barcode"></i> Print Label</a>
							</li>
							<li>
								<a class="data-table-finish-tally '.$class1.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-check-circle"></i> Finish Tally</a>
							</li>
							<li>
								<a class="data-table-print-grn '.$class3.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-check-circle"></i> Print GRN</a>
							</li>
              <li>
                <a class="data-table-cancel '.$class2.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-times"></i> Cancel Receiving </a>
              </li>
							';

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {

                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';

				$disabledClass='';

				if($row[$i]['status'] == 'Cancel'){
					$disabledClass='disabled-link';
				}

				/*
					Author : Hady Pratama
					Date : 2019-11-18
					Module : Cancel Receiving
				*/

        //         $action .= '<li>';
				// $action .= '<a class="data-table-cancel '.$disabledClass.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-times"></i> Cancel Receiving </a>';
				// $action .= '</li>';

            }

            if($this->session->userdata('username') == "largo"){

                $action .= '<li>';
				$action .= '<a class="data-table-setComplete" id="set_complete" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-check"></i> Set to Complete</a>';
				$action .= '</li>';

                $action .= '<li>';
				$action .= '<a class="data-table-backToTally" id="back_to_tally" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-undo"></i> Back To Tally</a>';
				$action .= '</li>';

                $action .= '<li>';
				$action .= '<a class="data-table-resetReceiving" id="reset_receiving" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-refresh"></i> Reset Receiving </a>';
				$action .= '</li>';

            }

	        if($row[$i]['kd_inbound']){

	        	$inboundCode = explode(",", $row[$i]['kd_inbound']);
	        	$inboundId = explode(",", $row[$i]['id_inbound']);

	        	if($inboundCode){

	        		$kd_inbound = "";
	        		$r = 0;
	        		foreach ($inboundCode as $oc) {

	        			$kd_inbound .= "<a href=".base_url().'inbound/detail/'.$inboundId[$r].">".$oc."</a><br/>";
	        			$r++;

	        		}

	        	}

	        }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['id_receiving'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().$post['className'].'/detail/'.$row[$i]['id_receiving'].'">'.$row[$i]['kd_receiving'].'</a>';
			// $nested[] = '<a href="'.base_url().'inbound/detail/'.$row[$i]['id_inbound'].'">'.$row[$i]['kd_inbound'].'</a>';
			$nested[] = $kd_inbound;
			if($row[$i]['source'] != null) {
				$nested[] = $row[$i]['source'];
			} else {
				$nested[] = $row[$i]['destination'];
			}
			$nested[] = $row[$i]['vehicle_plate'];

			$dateReceived = $row[$i]['tanggal_receiving'];
			$timeReceived = $row[$i]['time_receiving'];
			$timeReceivedSecond = $row[$i]['time_receiving_second'];
			$tally = $row[$i]['tally'];
			$putaway = $row[$i]['putaway'];
			$putawaySecond = $row[$i]['putaway_second'];

			$nested[] = $timeReceived.'<br><span class="small-date">'.$dateReceived.'</span>';
			//$nested[] = $timeReceived;
			$nested[] = $tally;
			$nested[] = $putaway;

			$sts = $row[$i]['status'];

			if($row[$i]['finish_putaway'] !=  NULL && $putaway != "-"){

				$this->load->helper('diff_helper');

				preg_match_all('/<span class="small-date">(.*?)<\/span>/s', $putaway, $matches);
				$dt = $matches[1][0];

				$exp = explode('<br>', $putawaySecond);

				$dtr = date('Y-m-d H:i:s', strtotime(str_replace('/','-', $dateReceived . ' ' . $timeReceivedSecond)));
				$dte = date('Y-m-d H:i:s', strtotime(str_replace('/','-', $dt . ' ' . $exp[0])));

				$nested[] = $row[$i]['status'] . ' in  ' . dateDiff($dtr, $dte)."";

			}else{

				$nested[] = $row[$i]['status'];

			}

			// $nested[] = ($row[$i]['preorder']) ? "Yes" : "No";
			$nested[] = $row[$i]['remark'];

			if($post['params'] != 'no_action')
				$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    public function get_rcv_code(){
        $this->db->select('pre_code_rcv,inc_rcv');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);

        $query = $this->db->get();
        $result = $query -> row();
        // return $result->pre_code_rcv.$result->inc_rcv;
        return $result->inc_rcv;
    }

    public function add_rcv_code(){
        $this->db->set('inc_rcv',"inc_rcv+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

    private function get_kd_barang($id_barang){
        $condition["id_barang"]=$id_barang;
        $this->db->select('kd_barang');
        $this->db->from($this->table5 );
        $this->db->where_condition($condition);
        return $this->db->get()->row()->kd_barang;
    }
    public function data($condition = array()) {
        //==========filter============================
        //$condition = array();
        $kd_receiving= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');

        if(!empty($kd_receiving)){
            $this->db->like('a.code', $kd_receiving);
        }
        if(!empty($id_supplier)){
            $this->db->group_start();
            $this->db->like('b.code',$id_supplier);
            $this->db->like('b.name',$id_supplier);
            $this->db->group_end();
        }
        if(!empty($id_purchase_order)){
            $this->db->like('c.kd_po', $id_purchase_order);
        }

        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $this->db->where('a.date >=', $tanggal_awal);
            $this->db->where('a.date <=', $tanggal_akhir);
        }


        //------------end filter-----------------
        $this->db->select('*,a.id as id_receiving, a.code as kd_receiving, s.source_id as id_supplier, s.source_code as kd_supplier, c.code as kd_inbound, s.source_name as nama_supplier,');
        $this->db->select('a.date as tanggal_receiving');
        $this->db->from($this->table  . ' a');
        $this->db->join('hp_receiving_inbound hri','hri.receiving_id=a.id');
        $this->db->join($this->table3 . ' c','hri.inbound_id = c.id_inbound','left');
        $this->db->join('sources s','s.source_id = c.supplier_id','left');
        $this->db->join('destinations d','d.destination_id = c.supplier_id','left');
        $this->db->order_by('a.date DESC,a.date DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function data_barcode($id_rcv = ''){
        $this->db->select('*');
        $this->db->from('receiving_barang rb');
        $this->db->join('barang b','b.id_barang = rb.id_barang','left');
        $this->db->where('id_receiving',$id_rcv);
        return $this->db->get();
    }

    private function data_detail_po($condition = array()) {
        $id_receiving= $this->input->post("id_bbm");
        if(!empty($id_receiving)){
            $condition["e.id_receiving = '$id_receiving' "]=null;
        }
        $this->db->select('*,b.id_receiving_qty,count(f.kd_unik) as qty_doc,b.jumlah_barang as jumlah_po,c.id_barang as item_id');
        $this->db->from($this->table .' e');//receiving
        $this->db->join($this->table3  . ' a','e.id_po = a.id_po');//po
        $this->db->join($this->po_barang . ' b',' b.id_po = a.id_po','left');//po_barang
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');//barang
        $this->db->join($this->table6 . ' d',' d.id_satuan = c.id_satuan','left');
        $this->db->join($this->table4 . ' f',' e.id_receiving = f.id_receiving and f.id_barang = c.id_barang','LEFT');//receiving_barang
        $this->db->group_by('c.id_barang');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail_supplier($condition = array()) {
        $this->db->select('*');
        $this->db->from($this->table3  . ' a');
        //$this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');
        $this->db->join($this->table2 . ' f',' a.id_supplier = f.id_supplier','left');
        $this->db->join($this->table8 . ' g',' g.id_supplier = f.id_supplier','left');
        $this->db->join($this->table5 . ' c',' c.id_barang = g.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function checkExist($kd_unik){
        $this->db->select('kd_unik');
        $this->db->from('receiving_barang');
        $this->db->where('kd_unik',$kd_unik);
        $hasil = $this->db->get();
        if($hasil->num_rows()>0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function cek_rcv_qty($id_barang,$id_receiving){
        $this->db->select('kd_unik');
        $this->db->from('receiving_barang');
        $this->db->where('id_receiving',$id_receiving);
        $this->db->where('id_barang',$id_barang);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function empty_rcv_qty($id_barang,$id_receiving){
        $this->db->where('id_receiving',$id_receiving);
        $this->db->where('id_barang',$id_barang);
        $this->db->delete('receiving_barang');
    }

    public function update_rcv_qty($id_receiving,$id_barang,$qty,$idInbound){
        $condition['receiving_id'] = $id_receiving;
        $condition['item_id'] = $id_barang;
        $condition['inbound_id'] = $idInbound;

        $this->db->from('item_receiving ir');
        $this->db->where($condition);
        $cek_eksis = $this->db->get();

        if($cek_eksis->num_rows() > 0){

            $update = array('qty' => $qty, 'inbound_id'=>$idInbound);
            $this->db->where($condition);
            $this->db->update('item_receiving', $update);

        }else{

            $insert = array(
            	'receiving_id' => $id_receiving,
                'item_id' => $id_barang,
                'qty' => $qty,
	            'inbound_id'=>$idInbound
	        );

            $this->db->insert('item_receiving', $insert);

        }
    }

    public function data_detail_bbm(){
        $id_receiving= $this->input->post("id_bbm");
        if(!empty($id_receiving)){
            $condition["r.id_receiving = '$id_receiving' "]=null;
        }
        $this->db->select('*, rq.qty as qty_doc');
        $this->db->from('receiving r');
        $this->db->join('inbound po','po.id_inbound = r.id_po','left');
        $this->db->join('purchase_order_barang pob','pob.id_po = po.id_po','left');
        $this->db->join('receiving_qty rq','rq.id_receiving = r.id_receiving and pob.id_barang = rq.id_barang','left');
        //$this->db->join('receiving_qtya rq','rq.id_receiving = r.id_receiving','left');
        $this->db->join('v_receiving vr','rq.id_receiving = vr.id_receiving and vr.id_barang = rq.id_barang','left');
        $this->db->join('barang b','b.id_barang = pob.id_barang','left');
        $this->db->join('satuan s','s.id_satuan = b.id_satuan','left');
        $this->db->where($condition);
        return $this->db;
    }

    public function data_table_detail(){
        $id_do = $this->input->post('id_bbm');
        $id_receiving = $id_do;
        $id_barang_arr = $this->input->post('id_barang');
        //$id_po = $this->input->post('id_po');

        if(!empty($id_barang_arr)){
            foreach ($id_barang_arr as $id_barang) {
                $kd_barang = $this->get_kd_barang($id_barang);
                $receiving = $this->input->post('receiving_'.$id_receiving.'_'.$id_barang);
                $tgl_exp = hgenerator::switch_tanggal($this->input->post('tanggal_exp_'.$id_receiving.'_'.$id_barang));
                $this->update_rcv_qty($id_receiving,$id_barang,$receiving);
            }
        }
        //-----------end save data----------------
        // Total Record
        $total = $this->data_detail_bbm()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail_bbm()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $i=0;
            $id = $value->id_po_barang;
            $id_barang = $value->id_barang;
            if(!empty($value->qty_doc)){
                $discrepenci = $value->qty_doc - $value->qty_received;
            }else{
                $discrepenci = 0;
            }
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'receiving' => !empty($value->qty_doc) ? $value->qty_doc.' '. $value->nama_satuan : form_input("receiving_".$id_do.'_'.$id_barang,!empty($value->qty_doc) ?$value->qty_doc:'','id="receiving_'.$id_do."_".$id_barang.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'). form_hidden('id_barang[]',$id_barang) ,
                'received' =>$value->qty_received,
                'discrepenci' => $discrepenci
            );
            $i++;
        }

        return array('rows' => $rows, 'total' => $total);
    }


    public function edit_data_table_detail(){
        $id_do = $this->input->post('id_bbm');
        $id_receiving = $id_do;
        $id_barang_arr = $this->input->post('id_barang');
        //$id_po = $this->input->post('id_po');

/*        if(!empty($id_barang_arr)){
            foreach ($id_barang_arr as $id_barang) {
                $kd_barang = $this->get_kd_barang($id_barang);
                $receiving = $this->input->post('receiving_'.$id_receiving.'_'.$id_barang);
                $tgl_exp = hgenerator::switch_tanggal($this->input->post('tanggal_exp_'.$id_receiving.'_'.$id_barang));

                for($no=1;$no<=$receiving;$no++){

                    $cek_eksis = $this->checkExist(trim($kd_barang).'/'.$id_receiving.'/'.$no);
                    //if(!empty($jumlah_barang)&&!empty($harga_barang)&&$checkExist==0){
                    if($cek_eksis == FALSE){
                        $data_insert = array(
                            'id_receiving' => $id_receiving,
                            'tgl_exp'=> $tgl_exp,
                            'kd_unik' => trim($kd_barang).'/'.$id_receiving.'/'.$no,
                            'tgl_in' =>date('YYYY-mm-dd'),
                            'id_barang' =>$id_barang,
                        );
                        $this->db->insert($this->table4,$data_insert);
                    }

                }
            }
        }*/
        //-----------end save data----------------
        // Total Record
        $total = $this->data_detail_bbm()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail_bbm()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $i=0;
            $id = $value->id_po_barang;
            $id_barang = $value->id_barang;
            if(!empty($value->qty_doc)){
                $discrepenci = $value->qty_doc - $value->qty_received;
            }else{
                $discrepenci = 0;
            }
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'receiving' => form_input("receiving_".$id_do.'_'.$id_barang,!empty($value->qty_doc) ?$value->qty_doc:'','id="receiving_'.$id_do."_".$id_barang.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'). form_hidden('id_barang[]',$id_barang),
                'received' =>$value->qty_received,
                'discrepenci' => $discrepenci
            );
            $i++;
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function get_data_print_doc_rcv($id) {
        $condition['r.id_receiving'] = $id;
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");

        $this->db->select('kd_barang,nama_barang, qty, kd_receiving, tanggal_receiving, kd_po, kd_supplier, nama_supplier,nama_kategori, nama_satuan,r.*,po.*');
        $this->db->from('receiving r');
        $this->db->join('purchase_order po',' r.id_po = po.id_po','left');
        //$this->db->join('purchase_order_barang pob','po.id_po = pob.id_po','left');
        $this->db->join('receiving_qty rq','rq.id_receiving = r.id_receiving','left');
        $this->db->join('supplier s','po.id_supplier = s.id_supplier','left');
        $this->db->join('barang b','b.id_barang = rq.id_barang','left');
        $this->db->join('kategori k','k.id_kategori = b.id_kategori','left');
        $this->db->join('satuan sat','sat.id_satuan = b.id_satuan','left');
        $this->db->order_by('b.id_barang ASC');
        $this->db->where_condition($condition);

        return $this->db->get();
    }

    public function get_qty_print($id_rcv){
		$qty = 0;

        $this->db->select(["COALESCE(sum(qty),0) as qty_print"]);
        $this->db->from('item_receiving');
        $this->db->where('receiving_id',$id_rcv);
        $this->db->group_by('receiving_id');

        if($hasil = $this->db->get()->row()){
			return $qty = $hasil->qty_print;
		}

        return $qty;
    }

    public function get_inc_date(){
        $this->db->select('*');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        return $this->db->get()->row();
    }

    /* Print Label without SKU */

    public function get_next_barcode(){
        $curent_date = date('Y-m-d');
        $get_inc_date = $this->get_inc_date();
        if($curent_date != $get_inc_date->sn_last_date){
            $update = array('sn_last_date'=>$curent_date,'inc_sn'=>1);
            $this->db->where('id_inc',1);
            $this->db->update('m_increment',$update);
            $print_sn = 1;
        }else{
            $last_sn = $get_inc_date->inc_sn;
            $print_sn = $last_sn + 1;
            $update = array('sn_last_date'=>$curent_date,'inc_sn'=>$print_sn);
            $this->db->where('id_inc',1);
            $this->db->update('m_increment',$update);
        }
        $inc_lenght = strlen($print_sn);
        $leading_zero = '';
        for($i=$inc_lenght;$i<6;$i++){
            $leading_zero = $leading_zero.'0';
        }
        $full_sn = date('ymd').$leading_zero.$print_sn;

        // $insert = array('kd_unik'=>$full_sn);
        // $this->db->insert('receiving_barang',$insert);

        return $full_sn;
    }

    /* End - Print Label without SKU */

    /* Print Label with SKU */

/*    public function get_next_barcode($item){

        $curent_date = date('Y-m');
        $get_inc_date = $this->get_inc_date();

        $last_sn = $get_inc_date->inc_ol;
        $print_sn = $last_sn + 1;
        $update = array('sn_last_date'=>date('Y-m-d'),'inc_ol'=>$print_sn);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment',$update);

        $inc_lenght = strlen($print_sn);

        $leading_zero = '';
        for($i=0;$i<(8-$inc_lenght);$i++){
            $leading_zero = $leading_zero.'0';
        }

        $full_sn = $item.$leading_zero.$print_sn;

        return $full_sn;

    }
*/
    /* End - Print Label with SKU */


    public function get_next_barcode_lp($warehouse){
        $curent_date = date('Y-m');
        $get_inc_date = $this->get_inc_date();
        $date = date("Y-m", strtotime($get_inc_date->ol_last_date));
        if($curent_date != $date){
            $update = array('ol_last_date'=>date('Y-m-d'),'inc_ol'=>1);
            $this->db->where('id_inc',1);
            $this->db->update('m_increment',$update);
            $print_sn = 1;
        }else{
            $last_sn = $get_inc_date->inc_ol;
            $print_sn = $last_sn + 1;
            $update = array('ol_last_date'=>date('Y-m-d'),'inc_ol'=>$print_sn);
            $this->db->where('id_inc',1);
            $this->db->update('m_increment',$update);
        }
        $inc_lenght = strlen($print_sn);
        $leading_zero = '';
        for($i=$inc_lenght;$i<4;$i++){
            $leading_zero = $leading_zero.'0';
        }

        $full_sn = date('ym').$warehouse.$leading_zero.$print_sn;

        // $insert = array('kd_unik'=>$full_sn);
        // $this->db->insert('receiving_barang',$insert);

        return $full_sn;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_receiving= $this->input->post("kd_receiving");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_receiving = $this->input->post('id_receiving');
        if(!empty($kd_receiving)){
            $condition["a.kd_receiving like '%$kd_receiving%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_receiving)){
            $condition["a.id_receiving"]=$id_receiving;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_receiving_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->tbl_ir . ' b',' a.id = b.receving_id');
        $this->db->join($this->table4 . ' c',' c.item_receiving_id = b.id');
        $this->db->join($this->table5 . ' d',' d.id= b.item_id','left');
        $this->db->join($this->table6 . ' e',' e.id = d.unit_id','left');
        $this->db->join($this->table7 . ' f',' f.id = d.category_id','left');
        $this->db->order_by('d.code ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_receiving'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_edit($condition = array()) {

        $this->db
        	->select('*,r.id as id_receiving, r.code as kd_receiving, r.date as tanggal_receiving')
	        ->from('receivings r')
	        ->where('id',$condition['id_receiving'])
	        ->order_by('r.date DESC');

        return $this->db->get();
    }

    public function get_inbound_document($receivingId=""){

    	$this->db
    		->from('hp_receiving_inbound')
    		->where('receiving_id',$receivingId);

    	$data = $this->db->get()->result_array();

    	$results = array();

    	foreach ($data as $d) {
    		$results[] = $d['inbound_id'];
    	}

    	return $results;

    }

    public function get_data($condition = array()) {
    	if(!empty($condition['id_receiving'])){
    		$condition['a.id'] = $condition['id_receiving'];
    		unset($condition['id_receiving']);
    	}

        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = $this->session->userdata('tahun_aktif');
            if(empty($tahun_aktif))
                $tahun_aktif = date('Y');
        }else{
            $set_tahun_aktif = array('tahun_aktif' => $tahun_aktif);
            $this->session->set_userdata($set_tahun_aktif);
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_receiving >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_receiving <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            //---------awal button action---------
            $id = $value->id_receiving;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">';
            if ($this->access_right->otoritas('print')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/print_doc_rcv/' . $id)));
                $action .= '</li>';

            }

            if ($this->access_right->otoritas('print')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print Barcode', array('id' => 'button-print-barcode-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/print_barcode/' . $id)));
                //$action .= anchor(base_url('bbm/print_barcode/' . $id), '<i class="fa fa-print"></i>Print Barcode');
                $action .= '</li>';

            }
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';
            //------------end button action---------------
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_receiving,
                    'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                    'kd_receiving' => anchor(null, $value->kd_receiving, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) ,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'vehicle_plate'=> $value->vehicle_plate,
                    'remark'=>$value->remark,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_receiving,
                    'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                    'kd_receiving' => anchor(null, $value->kd_receiving, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) ,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'vehicle_plate'=> $value->vehicle_plate,
                    'remark'=>$value->remark,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_receiving >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_receiving <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $rows[] = array(
                'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                'kd_receiving' => $value->kd_receiving,
                'kd_po' => $value->kd_po,
                'supplier' => $value->nama_supplier,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

/*    public function data_insert($data){

    	$dataNew = array(
    		"code"			=> $data['kd_receiving'],
    		"po_id"			=> $data['id_po'],
    		"vehicle_plate" => $data['vehicle_plate'],
    		"dock"			=> $data['dock'],
    		"delivery_doc" 	=> $data['delivery_doc'],
    		"user_id"		=> $data['user_id'],
    	);

    	if(!empty($data['tanggal_receiving'])){
    		$dataNew["date"] = $data['tanggal_receiving'];
    	}

    	return $dataNew;

    }
*/

	public function importFromExcel($datas, $inbound_data =''){

    	$receivings = array();
    	$receiving_items = array();
		$items = array();
		$result = [];
    	$this->db->trans_start();

    foreach($datas as $data) {
		if(!empty($data['rcv_doc']) || !empty($data['inbound_doc'])){
			$inbound_data = $this->db
									->select('inb.id_inbound, inb_itm.item_id, inb_itm.id, inb_itm.qty')
									->from('inbound inb')
									->join('inbound_item inb_itm', 'inb_itm.inbound_id = inb.id_inbound', 'left')
									->where('inb.code', $data['inbound_doc'])
									->get()
									->result_array();



    		if(!array_key_exists($data['rcv_doc'], $receivings)){
				$unix_date = ($data['rcv_date'] - 25569) * 86400;
				$excel_date = 25569 + ($unix_date / 86400);
				$unix_date = ($excel_date - 25569) * 86400;
				$date = gmdate("Y-m-d", $unix_date);

        		$data_receiving = [
        			'code'					=> $data['rcv_doc'],
        			'date'					=> $date,
        			'vehicle_plate' 		=> $data['vehicle_plate'],
        			'dock'					=> $data['dock'],
        			'is_cross_doc'			=> $data['is_cross_doc'] == 'YES' ? 1 : 0,
					'st_receiving'			=> 0,
					'delivery_doc'			=> $data['delivery_doc'],
        			'user_id'				=> $this->session->userdata('user_id')
				];

				$receivings[$data['rcv_doc']] = $data['rcv_doc'];
				$id_receiving = $this->create_receiving_from_excel($data_receiving);
			}else{
				$id_receiving = $this->db
								->select('id')
								->from($this->table)
								->where('code', $data['rcv_doc'])
								->get()
								->row_array();
				$id_receiving = $id_receiving['id'];
			}

			$this->db->insert('hp_receiving_inbound', array('inbound_id' => $inbound_data[0]['id_inbound'], 'receiving_id' => $id_receiving));

			foreach($inbound_data as $key => $data){
				$receiving_items[$key][] = [
					'inbound_id'=> $data['id_inbound'],
					'item_id'	=> $data['item_id'],
					'qty'		=> $data['qty'],
					'receiving_id' => $id_receiving
				];
			}
		}
	}
		foreach($receiving_items as $arr){
    		$result = array_merge($result , $arr);
		}
		$this->db->insert_batch('item_receiving', $result);
    	$this->db->trans_complete();
    	return $this->db->trans_status();

	}

	public function create_receiving_from_excel($data_receiving){
		$this->db->insert($this->table, $data_receiving);
		$this->db->trans_complete();
		$this->db->order_by('id','desc');
		$this->db->limit(1);
		$id_receiving = $this->db->get($this->table)->row_array()['id'];
        return $id_receiving;
	}

    public function create($data) {

    	$data['code'] = $this->get_rcv_code();
		$this->db->trans_start();
		$this->db->insert($this->table, $data);
		$this->db->trans_complete();

		$this->db->order_by('id','desc');
		$this->db->limit(1);
		$id_receiving = $this->db->get($this->table)->row_array()['id'];
        return $id_receiving;
        // return $this->db->insert($this->table, $data);

    }

/*
	Module : Receiving multiple Inbound
	Creator : Hady Pratama
*/
    public function inbound_receiving_relation($inboundIds=array(),$receivingId=""){

    	if(!is_array($inboundIds)){
    		$inboundIds = array($inboundIds);
    	}

    	if($inboundIds && !empty($receivingId)){

    		$receivingRelation = $this->db->get_where('hp_receiving_inbound',['receiving_id'=>$receivingId])->result_array();

    		$recData = array();
    		$newRelation = array();

    		if($receivingRelation){
	    		foreach ($receivingRelation as $r){
	    			$recData[] = $r['inbound_id'];
	    		}

	    		$existRelation = array();
		    	foreach ($inboundIds as $inboundId) {

		    		if(in_array($inboundId, $recData)){
		    			$existRelation[] = $inboundId;
		    		}

		    		if(!in_array($inboundId, $recData)){

		    			$newRelation[] = array(
		    				'receiving_id'	=> $receivingId,
		    				'inbound_id' => $inboundId
		    			);
		    		}

		    	}

		    	$this->db
		    		->where_not_in('inbound_id',$existRelation)
		    		->where('receiving_id',$receivingId)
		    		->delete('hp_receiving_inbound');

		    }else{

		    	foreach ($inboundIds as $inboundId) {

	    			$newRelation[] = array(
	    				'receiving_id'	=> $receivingId,
	    				'inbound_id' => $inboundId
	    			);

		    	}



		    }

		    if($newRelation){
		    	$this->db->insert_batch('hp_receiving_inbound',$newRelation);
		    }

	    }

	    return false;

    }

/*
	End - Module : Receiving multiple Inbound
*/

    public function update($data, $id) {
    	// $data = $this->data_insert($data);
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {

		$result = array();

		$this->db
			->from('receivings')
			->where('id', $id);

		$row = $this->db->get()->row_array();

		if(!empty($row['start_tally'])){

			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';

		}else{

			$inboundIds = $this->db->get_where('hp_receiving_inbound',['receiving_id'=>$row['id']])->result_array();
			$inbId = array();

			foreach ($inboundIds as $inboundId){
				$inbId[] = $inboundId['inbound_id'];
			}

			$this->db->trans_start();

			$this->db
				->set('status_id',3)
				->where_in('id_inbound', $inbId)
				->update('inbound');

			$this->db->delete('hp_receiving_inbound', array('receiving_id' => $id));

			$this->db->delete($this->tbl_ir, array('receiving_id' => $id));

			$this->db->delete($this->table, array('id' => $id));

			$this->db->trans_complete();

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';

		}

		return $result;
    }

    public function delete_detail($id) {
        return $this->db->delete($this->tbl_ir, array('receiving_id' => $id));
    }


    public function options($default = '--Pilih Kode BBM--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_receiving] = $row->kd_receiving ;
        }
        return $options;
    }



    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_receiving_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function validQty($id_inbound, $barang,$receivingId=""){

    	$dItem = array();
    	foreach ($barang as $brg) {
    		$dItem[$brg['id_barang']]['qty'] = $brg['qty'];
    	}

		// var_dump($dItem);

    	$this->db
    		->select('inb.id_inbound, inbitm.qty as doc_qty, inbitm.item_id, itm.code as item_code, itm.name as item_name, unt.name as unit_name')
    		->select(["COALESCE( (CASE WHEN r.st_receiving=1 THEN sum(first_qty) ELSE SUM(ir.qty) END), 0 ) as rec_qty"])
    		->from('inbound inb')
    		->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound')
    		->join('items itm','itm.id=inbitm.item_id')
    		->join('units unt','unt.id=itm.unit_id','left')
    		// ->where('inb.id_inbound',$id_inbound)
    		->where_in('inbitm.item_id', array_keys($dItem))
    		->group_by('inb.id_inbound, inbitm.item_id, inbitm.qty, itm.code, itm.name, unt.name, r.st_receiving');

    	if(!is_array($id_inbound)){
    		$id_inbound = array($id_inbound);
    	}
   		$this->db->where_in('inb.id_inbound',$id_inbound);

    	if(!empty($receivingId)){

    		$this->db->join('receivings r','r.po_id=inb.id_inbound and r.id <> '.$receivingId,'left');

    	}else{

    		$this->db->join('receivings r','r.po_id=inb.id_inbound','left');

    	}

    	$this->db
			->join('item_receiving ir','ir.receiving_id=r.id and ir.item_id=inbitm.item_id','left')
			->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left');


    	$tbl = $this->db->get_compiled_select();

    	// $sql = "SELECT id_inbound, doc_qty, item_id, item_code, item_name, unit_name, COALESCE(SUM(rec_qty),0) as rec_qty FROM ($tbl) as tbl GROUP BY id_inbound, doc_qty, item_id, item_code, item_name, unit_name";
    	$sql = "SELECT id_inbound, doc_qty, item_id, item_code, item_name, unit_name, COALESCE(SUM(rec_qty),0) as rec_qty FROM ($tbl) as tbl GROUP BY id_inbound, doc_qty, item_id, item_code, item_name, unit_name";

    	$result = $this->db->query($sql)->result_array();

    	foreach ($result as $res) {
			if(array_key_exists($res['item_id'],$dItem)){
				$this->db
					->select(["SUM(qty) as total_doc_qty"])
					->from("inbound_item")
					->where_in('inbound_id',$id_inbound)
					->where('item_id',$res['item_id']);
				$totQty = $this->db->get()->row_array()['total_doc_qty'];

		    	// if($res['doc_qty'] < ($res['rec_qty'] + $dItem[$res['item_id']]['qty'])){

		    		// $Max = $res['doc_qty'] - $res['rec_qty'];

		    		// return array(
		    			// "response" => false,
		    			// "message"  => "Barang ".$res['item_code']." tidak boleh lebih dari ". $Max ." ". $res['unit_name']
		    		// );
		    	// }

				if($totQty < ($res['rec_qty'] + $dItem[$res['item_id']]['qty'])){

		    		$Max = $totQty - $res['rec_qty'];

		    		return array(
		    			"response" => false,
		    			"message"  => "Barang ".$res['item_code']." tidak boleh lebih dari ". $Max ." ". $res['unit_name']
		    		);
		    	}
		    }
	    }

	    return array('response'=>true);
    }

    function inboundReceiving($receivingId){

    	$this->db
    		->select([
    			"r.code as receiving_code",
    			"r.date as receiving_date",
    			"r.delivery_doc",
    			"COALESCE(s.source_name,d.destination_name) as source","COALESCE(r.vehicle_plate,'-') as truck","string_agg(DISTINCT CAST(inb.code as VARCHAR), ',<br/>') as inbound_code"])
    		->from('receivings r')
    		->join("item_receiving ir",'ir.receiving_id=r.id')
    		->join('inbound inb','inb.id_inbound=ir.inbound_id')
    		->join('sources s','s.source_id=inb.supplier_id','LEFT')
    		->join('destinations d','d.destination_id=inb.supplier_id','LEFT')
    		->where('r.id',$receivingId)
    		->group_by('r.code, r.date, r.delivery_doc, s.source_code, s.source_name, d.destination_name, r.vehicle_plate');

    	$data = $this->db->get()->row_array();

    	return $data;

    }

    function receivedItem($receivingId){

    	$this->db
    		->select(
    			[
    				'itm.id as item_id, itm.code as item_code','itm.name as item_name','ir.qty as doc_qty',"COALESCE(SUM(ird.first_qty),0) as rec_qty",'unt.code as unit',"COALESCE(r.remark,'-') as remark",
    				"(CASE itm.preorder WHEN 1 THEN 'PREORDER' ELSE 'REGULER' END) as preorder"
    			]
    		)
    		->from('receivings r')
    		->join('item_receiving ir','ir.receiving_id=r.id')
    		->join('item_receiving_details ird','ird.item_receiving_id=ir.id')
    		->join('items itm','itm.id=ir.item_id')
    		->join('units unt','unt.id=itm.unit_id','left')
    		->where('r.id',$receivingId)
    		->group_by('itm.id, itm.code, itm.name, unt.code, ir.qty, r.remark');

    	$data = $this->db->get()->result_array();
    	return $data;

    }

    function receivedQCItem($receivingId){

    	$this->db
    		->select(
    			[
    				'itm.id as item_id','itm.code as item_code','itm.name as item_name',"COALESCE(SUM(ird.first_qty),0) as qty","COALESCE(loc.name, '-') as location",'qc.code as qc',
    				"(CASE itm.preorder WHEN 1 THEN 'PREORDER' ELSE 'REGULER' END) as preorder"
    			]
    		)
    		->from('receivings r')
    		->join('item_receiving ir','ir.receiving_id=r.id')
    		->join('item_receiving_details ird','ird.item_receiving_id=ir.id')
    		->join('locations loc','loc.id=ird.location_id','left')
    		->join('qc','qc.id=ird.qc_id')
    		->join('items itm','itm.id=ir.item_id')
    		->where('r.id',$receivingId)
    		->group_by('itm.id, itm.code, itm.name, itm.name, qc.code, ird.first_qty, loc.name')
    		->order_by("itm.id");

    	$data = $this->db->get()->result_array();
    	return $data;

    }

    function sendToStaging(){
    	return true;
    }

    function setToComplete($receivingId=""){

    	if(!empty($receivingId)){

    		$date = date('Y-m-d h:i:s');

    		$this->db
    			->set('start_tally',"COALESCE(start_tally,'$date')",FALSE)
    			->set('finish_tally',"COALESCE(finish_tally,'$date')",FALSE)
    			->set('start_putaway',"COALESCE(start_putaway,'$date')",FALSE)
    			->set('finish_putaway',"COALESCE(finish_putaway,'$date')",FALSE)
    			->set('st_receiving',1)
    			->where('id',$receivingId)
    			->update('receivings');

    		return true;
   		}

   		return false;

    }

    function backToTally($receivingId=""){

    	if(!empty($receivingId)){

			$this->db
				->set('finish_tally',NULL)
				->set('start_putaway',NULL)
				->set('finish_putaway',NULL)
				->set('st_receiving',0)
				->where('id',$receivingId)
				->update('receivings');

			return true;

		}

		return false;

    }

    function resetReceiving($receivingId=""){

    	$data = array();

    	if(!empty($receivingId)){

    		$sql = "SELECT * FROM item_receiving_details WHERE item_receiving_id IN (SELECT id FROM item_receiving WHERE receiving_id=$receivingId) and ( picking_id IS NOT NULL or location_id IN (101,104) or last_qty < 1 or shipping_id IS NOT NULL or location_id IS NULL)";

    		$valid = $this->db->query($sql)->num_rows();

    		if(!$valid){

	    		$this->db->trans_start();

		    		$this->db
		    			->where("item_receiving_id IN (SELECT id FROM item_receiving WHERE receiving_id=$receivingId)",NULL)
		    			->delete('item_receiving_details');

		    		$this->backToTally($receivingId);

	    		$this->db->trans_complete();

				$data = array(
					"status" => true,
					"message" => "Success reset receiving"
				);

			}else{

				$data = array(
					"status" => false,
					"message" => "Item di receiving ini, sudah ada yang keluar."
				);

			}

		}else{

			$data = array(
				"status" => false,
				"message" => "Receiving is not available"
			);

		}

		return $data;

    }


    /*
		Author : Hady Pratama
		Module : Cancel Receiving
    */
    function cancel_receiving($receivingId=""){

    	if(!empty($receivingId)){

    		$this->db
    			->where("item_receiving_id IN (SELECT id FROM item_receiving where receiving_id=$receivingId)",NULL)
    			->delete('item_receiving_details');

    		$this->db
    			->set('st_receiving',1)
    			->set('status_id',17)
    			->where('id',$receivingId)
    			->update('receivings');

    		return true;

    	}

    	return false;
    }

    function cancel_receiving_item($receivingId="",$uniqueCode=""){

    	if(!empty($receivingId) && !empty($uniqueCode)){

    		$this->db
    			->where('unique_code',$uniqueCode)
    			->where("item_receiving_id IN (SELECT id FROM item_receiving where receiving_id=$receivingId)",NULL)
    			->delete('item_receiving_details');

    		return true;

    	}

    	return false;

    }

    /* End - Cancel Receiving */

    function checkInboundSource($inboundIds = array()){

    	if(!is_array($inboundIds)){
    		$inboundIds = array($inboundIds);
    	}

    	if(count($inboundIds) > 1){

    		$this->db
    			->select('doc.m_source_type_id, inb.supplier_id')
    			->from('inbound inb')
    			->join('documents doc','inb.document_id=doc.document_id')
    			->where_in('id_inbound',$inboundIds)
    			->group_by('doc.m_source_type_id, inb.supplier_id');

    		$inbound = $this->db->get()->num_rows();

    		if($inbound > 1){
    			return false;
    		}

    	}

    	return true;

    }

    function printTallyInformation($params=array()){

    	$this->db
    		->select([
    			"r.code as kd_receiving",
    			"string_agg(DISTINCT CAST(inb.code as VARCHAR), ', ') as kd_inbound",
    			"r.date as tanggal_receiving",
    			"r.vehicle_plate",
    			"r.delivery_doc",
    			"r.dock",
    			"COALESCE(s.source_name,d.destination_name) as kd_supplier"
    		])
    		->from('receivings r')
    		->join('item_receiving ir','ir.receiving_id=r.id')
    		->join('inbound inb','inb.id_inbound=ir.inbound_id')
    		->join('sources s','s.source_id=inb.supplier_id','LEFT')
    		->join('destinations d','d.destination_id=inb.supplier_id','LEFT')
    		->where('r.id',$params['id_receiving'])
    		->group_by('r.code, r.date, r.vehicle_plate, r.delivery_doc, r.dock, s.source_name, d.destination_name, s.source_code');

    	return $this->db->get();

    }

}

?>

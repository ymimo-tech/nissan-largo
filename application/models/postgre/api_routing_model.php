<?php

/**
 *
 */
class Api_routing_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();

  }

  public function check_serial_number($serial_number){
    $result = FALSE;
    $check = $this->db->select('id, item_id')->from('items_receiving_routing')->where('unique_code', $serial_number)->get();

      if($check->num_rows() > 0){
        $result = TRUE;
      }

    return $result;
  }

  public function post($params = []){
    $result = TRUE;
    $insert_item_routing = [];
    $get_routing_id = $this->db->select('id')->from('routing')->where('name', $params['routing_code'])->get()->row_array()['id'];
    $check_true_routing = $this->db
                                ->select('ir.id')
                                ->from('items_routing ir')
                                ->join('items_receiving_routing irr', 'irr.item_id = ir.item_id', 'LEFT')
                                ->where('ir.routing_id', $get_routing_id)
                                ->where('irr.unique_code', $params['kd_unik'])
                                ->get();

    $insert_item_routing = $this->db->from('items_receiving_routing')->where('unique_code', $params['kd_unik'])->get()->result_array()[0];

    $insert_item_routing['user_routing'] = $this->db->select('id')->from('users')->where('user_name', $params['uname_pick'])->get()->row_array()['id'];

    if($check_true_routing->num_rows() == 0){
      $result =  false;

      return $result;
    }else {
        switch ($get_routing_id){
          case "1":
            $insert_item_routing['status_routing_quality'] = "1";
            break;
          case "2":
            $insert_item_routing['status_routing_label'] = "1";
            break;
          case "3":
            $insert_item_routing['status_routing_repacking'] = "1";
            break;
      }

      $this->db->where("unique_code", $params['kd_unik'])->update("items_receiving_routing", $insert_item_routing);
      $check = $this->db->select('status_routing_label, status_routing_quality, status_routing_repacking')->from('items_receiving_routing')->where("unique_code", $params['kd_unik'])->get()->result_array();
      $implode = implode(", ", reset($check));
      $value = strpos($implode, '0');

      if($value === FALSE){
          $this->db->set('done', 1);
          $this->db->where("unique_code", $params['kd_unik']);
          $this->db->update('items_receiving_routing');
      }

      return $result;
    }


  }

  public function getItem(){

    $row = $this->db->select(["itm.name as name_item","itm.code as code_item", "rcv.code as rcv_code",
                        "itm_ro.unique_code",
                        "(
                          CASE
                            WHEN itm_ro.status_routing_label = '0'
                              THEN 'NONE'
                            WHEN itm_ro.status_routing_label = '1'
                              THEN 'DONE'
                            END) AS status_routing_label",
                        "(
                          CASE
                            WHEN itm_ro.status_routing_quality = '0'
                              THEN 'NONE'
                            WHEN itm_ro.status_routing_quality = '1'
                              THEN 'DONE'
                            END) AS status_routing_quality",
                        "(
                          CASE
                            WHEN itm_ro.status_routing_repacking = '0'
                              THEN 'NONE'
                            WHEN itm_ro.status_routing_repacking = '1'
                              THEN 'DONE'
                            END) AS status_routing_repacking"])
             ->from("items_receiving_routing itm_ro")
             ->join("items itm", "itm.id = itm_ro.item_id", "left")
             ->join("receivings rcv", "rcv.id = itm_ro.receiving_id", "left")
             ->where("itm_ro.done", "0")
             ->or_where("itm_ro.done", NULL)
             ->get();

    $data = $row->result_array();
    $count = count($data);

    for($i=0;$i<$count;$i++){
      $routing = '';

        if($data[$i]['status_routing_label'] != NULL){
            $routing .= 'Label: '. $data[$i]['status_routing_label']."\n";
        }
        if($data[$i]['status_routing_quality'] != NULL){
          $routing .= 'Quality: '. $data[$i]['status_routing_quality']."\n";
        }
        if($data[$i]['status_routing_repacking'] != NULL){
          $routing .= 'Repacking: '. $data[$i]['status_routing_repacking'];
        }

        $data_item[]['list_item'] = $data[$i]['unique_code']."\n".$data[$i]['code_item'].' - '.$data[$i]['name_item'].'( '.$data[$i]['rcv_code']. ' )'."\n".$routing;
    }

    return $data_item;
  }
}

<?php
class cycle_count_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getCcCodeById($id){
		$result = '';

		$this->db
			->select('code as cc_code')
			->from('cycle_counts')
			->where('cc_id', $id);

		$row = $this->db->get()->row_array();
		$result = $row['cc_code'];
		
		return $result;
	}
	
	public function getCcDoc($post = array()){
		$result = array();
		
		$this->db
			->select('cc_id, code as cc_code')
			->from('cycle_counts')
			->where('status_id <>', 100)
			->like('LOWER(code)', strtolower($post['query']));

		$result['result'] = $this->db->get()->result_array();
		
		return $result;
	}
	
	public function getListRaw($post = array()){
		$result = array();
		
		$where = '';
		
		if(!empty($post['cc_id']))
			$where .= sprintf(' AND cc.cc_id = \'%s\' ', $post['cc_id']);
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(cc.time) BETWEEN \'%s\' AND \'%s\' ', 
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}
		
		if(!empty($post['status'])){
			if($post['status'] == 'CANCEL'){
				// $where .= ' AND ccr.system_qty = \'YES\' AND ccr.scanned_qty = \'YES\' ';
				$where .= ' AND ccr.system_qty = 1 AND ccr.scanned_qty = 1 ';

			}
			
			if($post['status'] == 'POSITIVE'){
				// $where .= ' AND ccr.system_qty = \'NO\' AND ccr.scanned_qty = \'YES\' ';
				$where .= ' AND ccr.system_qty = 0 AND ccr.scanned_qty = 1 ';

			}
			
			if($post['status'] == 'NEGATIVE'){
				// $where .= ' AND ccr.system_qty = \'YES\' AND ccr.scanned_qty = \'NO\' ';
				$where .= ' AND ccr.system_qty = 1 AND ccr.scanned_qty = 0 ';

			}
		}
		
		$sql = 'SELECT 
					cc.code, DATE(cc.time) AS cc_time, 
					(
						CASE cc.type 
							WHEN \'SN\' 
								THEN \'By Item\'
							ELSE \'By Location\' 
						END
					) AS cc_type, loc.name, CONCAT(itm.code, \' - \', itm.name) AS nama_barang,
					ccr.unique_code, ccr.status, ccr.system_qty, ccr.scanned_qty 
				FROM 
					cycle_counts cc
				JOIN cycle_count_results ccr 
					ON ccr.cycle_count_id=cc.cc_id
				JOIN items itm 
					ON itm.id=ccr.item_id
				JOIN locations loc
					ON loc.id=ccr.location_id 
				WHERE 1=1'.$where;
				
		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		
		for($i = 0; $i < $len; $i++){
			$status = 'CANCEL';
			if($row[$i]['system_qty'] == 'NO' && $row[$i]['scanned_qty'] == 'YES')
				$status = 'POSITIVE ADJUSTMENT';
			else if($row[$i]['system_qty'] == 'YES' && $row[$i]['scanned_qty'] == 'NO')
				$status = 'NEGATIVE ADJUSTMENT';
			
			$row[$i]['status'] = $status;
		}
		
		$result = $row;
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'cc_code',
			2 => 'cc_time_sort',
			3 => 'cc_type',
			4 => 'loc_name',
			5 => 'nama_barang',
			6 => 'kd_unik',
			7 => 'status',
			8 => 'system_qty',
			9 => 'scanned_qty',
		);
		
		$where = '';
		
		$this->db->start_cache();
		if(!empty($post['cc_id'])){
			$this->db->where('cc.cc_id', $post['cc_id']);
		}
		
		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('cc.time >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'));
			$this->db->where('cc.time <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'));
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id', $post['warehouse']);
		}
		
		// if(!empty($post['status'])){
		// 	if($post['status'] == 'CANCEL'){
		// 		$where .= ' AND system_qty = \'YES\' AND scanned_qty = \'YES\' ';
		// 	}
			
		// 	if($post['status'] == 'POSITIVE'){
		// 		$where .= ' AND system_qty = \'NO\' AND scanned_qty = \'YES\' ';
		// 	}
			
		// 	if($post['status'] == 'NEGATIVE'){
		// 		$where .= ' AND system_qty = \'YES\' AND scanned_qty = \'NO\' ';
		// 	}
		// }

		$this->db
			->from('cycle_counts cc')
			->join('warehouses wh','wh.id=cc.warehouse_id')
			->join('users_warehouses usrwh','usrwh.warehouse_id=wh.id AND usrwh.user_id='.$this->session->userdata('user_id'))
			->join('cycle_count_results ccr','ccr.cycle_count_id=cc.cc_id')
			->join('items itm','itm.id=ccr.item_id')
			->join('locations loc','loc.id=ccr.location_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];
		
		$this->db
			->select(["cc.code as cc_code","cc.time as cc_time_sort","TO_CHAR(cc.time, 'DD/MM/YYYY') as cc_time","(CASE cc.type WHEN 'SN' THEN 'By Item' ELSE 'By Location' END) as cc_type","loc.name as loc_name","CONCAT(itm.code, ' - ', itm.name) as nama_barang","ccr.unique_code as kd_unik","ccr.status","system_qty","scanned_qty"])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);		
		
		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$status = 'CANCEL';
			if($row[$i]['system_qty'] == 'NO' && $row[$i]['scanned_qty'] == 'YES')
				$status = 'POSITIVE ADJUSTMENT';
			else if($row[$i]['system_qty'] == 'YES' && $row[$i]['scanned_qty'] == 'NO')
				$status = 'NEGATIVE ADJUSTMENT';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['cc_code'];
			$nested[] = $row[$i]['cc_time'];
			$nested[] = $row[$i]['cc_type'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $status;
			$nested[] = $row[$i]['system_qty'];
			$nested[] = $row[$i]['scanned_qty'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
}
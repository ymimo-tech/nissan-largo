<?php
class item_low_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getListRaw($post = array()){
		$result = array();

		$where = '';

		if(!empty($post['id_barang'])){
			if($post['id_barang'] != "null"){
				$id_barang = explode(',', $post['id_barang']);
				$this->db->where_in('itm.id', $id_barang);
			}
		}

		if(!empty($post['warehouse'])){
			$this->db->where('wh.id',$post['warehouse']);
			$where .= "AND wh.id=".$post['warehouse'];
		}

		$tbl = "SELECT 
					ird.item_id, COALESCE(sum(last_qty),0) AS qty
				FROM 
                    item_receiving_details ird
                JOIN locations loc ON loc.id = ird.location_id
				JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
				JOIN warehouses wh ON wh.id=loc_area.warehouse_id
				JOIN users_warehouses uw ON uw.warehouse_id=wh.id
					AND uw.user_id=".$this->session->userdata('user_id')."
				WHERE
					putaway_time IS NOT NULL
                AND 
					(st_shipping = 0 or last_qty >0)
				$where
				GROUP BY 
					ird.item_id";

		$this->db
			->select([
				"DISTINCT(itm.id)", "itm.code as kd_barang", "CONCAT(itm.code, ' - ', itm.name) AS nama_barang",
				"COALESCE(tbl.qty, 0) as stock", "min_reorder AS min", "unt.name as nama_satuan"
			])
			->from('items itm')
			->join('items_warehouses iw','iw.item_id=itm.id','left')
			->join('warehouses wh','wh.id=iw.warehouse_id','left')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join("($tbl) as tbl", 'tbl.item_id=itm.id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->where("COALESCE(tbl.qty, 0) <= min_reorder")
			->group_by('itm.id, tbl.qty, unt.name, itm.code, itm.name, itm.min_reorder');

		$result = $this->db->get()->result_array();
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'nama_barang',
			2 => 'stock',
			3 => 'min'
		);
		
		$where = '';

		$this->db->start_cache();

		if(!empty($post['id_barang'])){
			$this->db->where_in('itm.id', $post['id_barang']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where('wh.id',$post['warehouse']);
			$where .= "AND wh.id=".$post['warehouse'];
		}

		$tbl = "SELECT 
					ird.item_id AS item_id, COALESCE(sum(last_qty),0) AS qty
				FROM 
                    item_receiving_details ird
                JOIN locations loc ON loc.id = ird.location_id
				JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
				JOIN warehouses wh ON wh.id=loc_area.warehouse_id
				JOIN users_warehouses uw ON uw.warehouse_id=wh.id
					AND uw.user_id=".$this->session->userdata('user_id')."
				WHERE
					putaway_time IS NOT NULL
                AND 
					(st_shipping = 0 or last_qty >0)
				$where
				GROUP BY 
					ird.item_id";
		$data =$this->db->query($tbl)->result_array();


		$this->db
			->select('DISTINCT(itm.id)')
			->from('items itm')
			->join('items_warehouses iw','iw.item_id=itm.id','left')
			->join('warehouses wh','wh.id=iw.warehouse_id','left')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join("($tbl) as tbl", 'tbl.item_id=itm.id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->where("tbl.item_id=itm.id")
			->where("COALESCE(tbl.qty, 0) <= min_reorder")
			->group_by('itm.id');


		$this->db->stop_cache();

		$row = $this->db->get();
		$totalData = $row->num_rows();
		
		
		$this->db
		->select([
			"itm.code as kd_barang", "CONCAT(itm.code, ' - ', itm.name) AS nama_barang",
			"COALESCE(tbl.qty, 0) as stock", "min_reorder AS min", "unt.name as nama_satuan"
			])
			->group_by('tbl.qty, unt.name, itm.code, itm.name, itm.min_reorder')
            ->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
            ->limit($requestData['length'],$requestData['start']);
			
			$row = $this->db->get()->result_array();
			$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['stock'];
			$nested[] = $row[$i]['min'];
			$nested[] = $row[$i]['nama_satuan'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
}
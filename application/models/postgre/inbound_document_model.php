<?php
class inbound_document_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'documents';
    private $type = 'INBOUND';

    public function data($condition = array()) {
        $this->db->select('document_id as id_inbound_document, code as inbound_document_code, name as inbound_document_name, year_type as year, month_type as month, separator, seq_length, is_active, source_category_id as m_source_type_id, is_auto_generate');
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);
        $this->db->where('type',$this->type);

        return $this->db;
    }

    public function get_by_id($id) {
        $this->db->where('a.document_id',$id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id_inbound_document'])){
            $condition['document_id'] = $condition['id_inbound_document'];
            unset($condition['id_inbound_document']);
        }

        $this->data($condition);
        return $this->db->get();
    }

    public function create($data) { 

        $data['type'] = $this->type;
        return $this->db->insert($this->table, $data);

    }

    public function update($data, $id) {

        return $this->db->update($this->table, $data, array('document_id' => $id));

    }

    public function delete($id) {		
		$result = array();

        $this->db->select('count(*) as t')
            ->from('inbound')
            ->where('document_id', $id);

        $row = $this->db->get()->row_array();
		if($row['t'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('document_id' => $id));
			
			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}
			
        return $result;
    }

    public function get_inbound_num($id){

        $this->db->where('document_id', $id);

        $data = $this->db->get('inbound')->num_rows();

        return $data;
    }

    public function options($default = '--Pilih Dokumen Inbound--', $key = '') {

        $this->load_model("user_model");
        $warehouses = $this->user_model->getUserWarehouse($this->session->userdata('user_id'));
        $warehouse = array();
        if(!empty($warehouses)){
            foreach ($warehouses as $wh) {
                $warehouse[] = $wh['warehouse_id'];
            }
            $this->db->where_in('b.warehouse_id', $warehouse);
        }

        $this->db
            ->from($this->table.' a')
            ->join('documents_warehouses b','a.document_id=b.document_id')
            ->where('a.is_active', 1)
            ->like('a.type',$this->type);


        $data = $this->db->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->document_id] = $row->name ;
        }
        return $options;
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        $inserts = array();

        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->select('count(*) as total')
                ->from('documents')
                ->where('LOWER(code)', strtolower($data[$i]['doc_initial']));

            $check = $this->db->get()->row()->total;

            if($check > 0){
                continue;
            }else{

                $year = strtolower($data[$i]['year']);

                if($year === "yyyy"){

                    $year = 2;

                }else if($year === "yy"){

                    $year = 1;

                }else{

                    $year = 0;
                }

                $month = (strtolower($data[$i]['month']) === "mm") ? 1 : 0 ;
                $sequence = (is_int($data[$i]['sequence'])) ? $data[$i]['sequence'] : 0;
                $separator = (strlen($data[$i]['separator']) == 1) ? $data[$i]['separator'] : '';
                $status = (strtolower($data[$i]['status']) === "active") ? 1 : 0;

                $inserts[] = array(
                    'code'          => $data[$i]['doc_initial'],
                    'name'          => $data[$i]['doc_name'],
                    'type'          => 'INBOUND',
                    'separator'     => $separator,
                    'seq_length'    => $sequence,
                    'year_type'     => $year,
                    'month_type'    => $month,
                    'is_active'     => $status
                );

            }

        }

        if(!$inserts){
            $this->db->trans_complete();
            return false;
        }

        $this->db->insert_batch($this->table,$inserts);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    // Select, Insert, Update of Table documents_warehouses

    public function getDocumentByWarehouse($params=array()){

        if(!empty($params['warehouse'])){
            $this->db->where('wh.id',$params['warehouse']);
        }

        if(!empty($params['document_id'])){
            $this->db->where('doc.document_id',$params['document_id']);
        }

        $this->db
            ->select('doc.document_id, doc.name as document_name, doc.is_auto_generate')
            ->from('documents doc')
            ->join('documents_warehouses docwh','docwh.document_id=doc.document_id')
            ->join('warehouses wh','wh.id=docwh.warehouse_id')
            ->join('users_warehouses usrwh','usrwh.warehouse_id=wh.id')
            ->where('type','INBOUND')
            ->where('usrwh.user_id',$this->session->userdata('user_id'))
            ->group_by('doc.document_id,doc.name');

        $data = $this->db->get();

        return $data;

    }
    
    public function getDocumentWarehouse($docId){

        $this->db
            ->select('warehouse_id')
            ->from('documents_warehouses')
            ->where('document_id', $docId);

        return $this->db->get()->result_array();

    }

    public function getDocumentCostCenter($docId){

        $this->db
            ->select('cost_center_id')
            ->from('documents_cost_center')
            ->where('document_id', $docId);

        return $this->db->get()->result_array();

    }

    public function addDocumentWarehouse($docId,$whId){

        $this->db->trans_start();

        $data = array();

        if(is_array($whId)){
            foreach ($whId as $wh) {
                $data[] = array(
                    "document_id" => $docId,
                    "warehouse_id" => $wh
                );
            }

            $this->db->insert_batch('documents_warehouses', $data);
        }else{
            $data = array(
                "document_id" => $docId,
                "warehouse_id" => $whId
            );

            $this->db->insert('documents_warehouses', $data);
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function updateDocumentWarehouse($docId, $whId){

        $this->db->trans_start();

        $this->db
            ->where('document_id', $docId)
            ->delete('documents_warehouses');

        $this->addDocumentWarehouse($docId,$whId);

        $this->db->trans_complete();

    }

    public function addDocumentCostCenter($docId,$costId){

        $this->db->trans_start();

        $data = array();

        if(is_array($costId)){
            foreach ($costId as $cost) {
                $data[] = array(
                    "document_id" => $docId,
                    "cost_center_id" => $cost
                );
            }

            $this->db->insert_batch('documents_cost_center', $data);
        }else{
            $data = array(
                "document_id" => $docId,
                "cost_center_id" => $costId
            );

            $this->db->insert('documents_cost_center', $data);
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function updateDocumentCostCenter($docId, $costId){

        $this->db->trans_start();

        $this->db
            ->where('document_id', $docId)
            ->delete('documents_cost_center');

        $this->addDocumentCostCenter($docId,$costId);

        $this->db->trans_complete();

    }
}

?>
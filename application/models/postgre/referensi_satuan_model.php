<?php
class referensi_satuan_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'units';

    public function data($condition = array()) {
        $this->db->select('a.id as id_satuan, a.code as kd_satuan, a.name as nama_satuan,is_active as status');
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $this->db->where('a.id', $id);
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id_satuan'])){
            $condition['id'] = $condition['id_satuan'];
            unset($condition['id_satuan']);
        }

        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_satuan= $this->input->post("kd_satuan");
        $nama_satuan= $this->input->post("nama_satuan");

        if(!empty($kd_satuan)){
            $this->db->like('a.code',$kd_satuan);
        }


        if(!empty($nama_satuan)){
            $this->db->like('a.name', $nama_satuan);
        }
        
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.id');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_satuan;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
            $action .= '</li>';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_satuan' => $value->kd_satuan,
                    'nama_satuan' => $value->nama_satuan,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'kd_satuan' => $value->kd_satuan,
                    'nama_satuan' => $value->nama_satuan,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    private function getData($data=[]){
        $data = [
            'code' => $data['kd_satuan'],
            'name' => $data['nama_satuan']
            // 'is_active' => $data['status']
        ];

        return $data;
    }

    public function create($data) { 
        $data = $this->getData($data);
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        $data = $this->getData($data);
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        //return $this->db->delete($this->table, array('id_satuan' => $id));
		
		$result = array();
		
        $this->db->select('count(*) as t')
        ->from('items')
        ->where('unit_id',$id);

        $row = $this->db->get()->row_array();
		if($row['t'] > 0){

            $this->db->set('is_active',0);
            $this->db->where('id', $id);
            $this->db->update($this->table);

			$result['status'] = 'warning';
			$result['message'] = 'Cannot delete data because this data have relation to another table, we will deactive this units';

		}else{
			$this->db->delete($this->table, array('id' => $id));
			
			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}
			
        return $result;
    }
    
    public function options($default = '--Pilih Kode satuan--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_satuan] = $row->nama_satuan ;
        }
        return $options;
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);
        $inserts = array();

        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->select('count(*) as total')
                ->from($this->table)
                ->where('LOWER(code)', strtolower($data[$i]['uom_code']))
                ->or_where('lower(name)', strtolower($data[$i]['uom_name']));

            $check = $this->db->get()->row()->total;

            if($check > 0){
                continue;
            }else{

                $inserts[] = array(
                    'code'          => $data[$i]['uom_code'],
                    'name'          => $data[$i]['uom_name']
                );

            }

        }

        if(!$inserts){
            $this->db->trans_complete();
            return false;
        }

        $this->db->insert_batch($this->table,$inserts);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }
}

?>
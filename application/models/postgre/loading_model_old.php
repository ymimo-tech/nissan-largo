<?php
class loading_model extends CI_Model {

    public function __construct() {
		parent::__construct();
        $this->load->library("randomidgenerator");
	}


	public function manifestPacking($params){
		$requestData = $_REQUEST;

		$data = [];

		$manifest = $this->db
						->select('manifest_number, outbound_id')
						->from('manifests')
						->where('shipping_id', $params['shipping_id'])
						->get()
						->row_array();

		$data_manifest = $this->db
					->select('id, shipping_id, shipping_code, outbound_id')
					->from('manifests')
					->where('manifest_number', $manifest["manifest_number"])
					->get()
					->result_array();
					$params = [];

				foreach($data_manifest as $key => $data_shipping){
					// $params["shipping_id"][] = $data_shipping['shipping_id'];
          $params["shipping_id"][] = $data_shipping['outbound_id'];
				}

		$results = $this->db
					->select(["oird.id_outbound_receiving_barang","oird.id_item_receiving_details","oird.id_outbound","oird.packing_number",
					"oird.inc","itm.code", "itm.name","ird.unique_code", "SUM(pr.qty) as qty", "unt.name as uom_name","des.destination_name as customer","
					(
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(pr.qty)* CAST(itm.weight as float8)),0) as weight FROM shippings shipp
							JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id
							JOIN pickings pl ON pl.pl_id=sp.pl_id
							JOIN picking_qty pq ON pq.picking_id=pl.pl_id
							JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=pq.item_id
							LEFT JOIN items itm ON itm.id=pr.item_id
							WHERE itm.id = 1 group by itm.weight
						) as tbl
					) as weight",
					"(
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(pr.qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM shippings shipp
							JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id
							JOIN pickings pl ON pl.pl_id=sp.pl_id
							JOIN picking_qty pq ON pq.picking_id=pl.pl_id
							JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=pq.item_id
							LEFT JOIN items itm ON itm.id=pr.item_id
							WHERE itm.id =1 group by itm.height, itm.width, itm.length
						) as tbl
					) as volume"])
					->from('outbound_item_receiving_details oird')
					->join('item_receiving_details ird', 'ird.id=oird.id_item_receiving_details', 'LEFT')
					->join('picking_recomendation pr', 'pr.item_receiving_detail_id=ird.id', 'LEFT')
					->join('items itm', 'itm.id=ird.item_id', 'LEFT')
					->join('units unt','unt.id=itm.unit_id','left')
					->join('outbound outb', 'outb.id = oird.id_outbound', 'LEFT')
					->join('destinations des', 'des.destination_id=outb.destination_id', 'LEFT')
					->join('picking_list_outbound plo', 'plo.id_outbound=outb.id')
					->join('pickings p', 'p.pl_id=plo.pl_id')
					// ->join('shipping_picking sp','sp.pl_id=p.pl_id')
					// ->join('shippings shipp', 'shipp.shipping_id=sp.shipping_id')
					// ->where_in('shipp.shipping_id', $params["shipping_id"])
          // ->where_in('shipp.outbound_id', $params["shipping_id"])
          ->where_in('oird.id_outbound', $params["shipping_id"])
					->group_by('oird.id_outbound_receiving_barang,des.destination_name, oird.id_item_receiving_details, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, pr.qty, unt.name')
					->get()
					->result_array();

					foreach($results as $key => $result){
						$packing_number = $result['packing_number'] . sprintf('%03d', $result['inc']);
						$data[$packing_number][$key] = array( 'name' => $result['name'], 'unique_code' => $result['unique_code'], 'item_code' => $result['code'], 'qty' => $result['qty'], 'name_uom' => $result['uom_name'], 'customer' => $result['customer'], 'weight' => $result['weight'], 'volume' => $result['volume']);
					}

					return $data;
	}

	public function getDocSj($params){
		$data_manifest = $this->db
							->select('id, shipping_id, shipping_code, outbound_id')
							->from('manifests')
							->where('shipping_id', $params)
							->get()
							->result_array();
		$data = array_column($data_manifest, 'shipping_code');
		return implode(', ', $data);
	}

	public function getPackingNumber(){
		$result = $this->db
						->select("DISTINCT(oird.packing_number) as packing_number, id_item_receiving_details ,id_outbound,")
						->from('outbound_item_receiving_details oird')
						->get()
						->result_array();
		return $result;
	}

  function getDetail_manifest($post = array()){
    $result = array();

    $manifest_number = $this->db
                            ->select('man.manifest_number')
                            ->from('manifests man')
                            ->where('shipping_id', $post['shipping_id'])
                            ->get()
                            ->result_array();

    $shippingid = $this->db
                        ->select('man.shipping_id, man.shipping_code, man.outbound_id')
                        ->from('manifests man')
                        ->where('manifest_number', $manifest_number[0]['manifest_number'])
                        ->get()
                        ->result_array();

    $shippingId = array_column($shippingid, 'shipping_id');

		$this->db
			->select(["shipp.shipping_id","shipp.code as shipping_code","outb.code as kd_outbound","outb.document_id as id_outbound_document","TO_CHAR(shipp.date, 'DD/MM/YYYY') as shipping_date",
				"d.destination_code as kd_supplier","d.destination_name as nama_supplier",
				"d.destination_address as alamat_supplier","d.destination_phone as telepon_supplier","d.destination_contact_person as cp_supplier",
				"CONCAT(d.destination_code, ' - ', d.destination_name) as customer_name",
				"COALESCE(TO_CHAR(shipping_start, 'DD/MM/YYYY HH24:MI:SS'), '-') as shipping_start","COALESCE(TO_CHAR(shipping_finish, 'DD/MM/YYYY HH24:MI:SS'), '-') as shipping_finish","user_name","st.name as nama_status","driver as driver_name","license_plate"])
			->from('shippings shipp')
			->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
			->join('pickings pl','pl.pl_id=sp.pl_id')
			->join('picking_list_outbound plo','plo.pl_id=pl.pl_id')
			->join('outbound outb','outb.id=shipp.outbound_id')
			->join('destinations d','d.destination_id=outb.destination_id','left')
			->join('users usr','usr.id=shipp.user_id','left')
			->join('status st','st.status_id=shipp.status_id','left')
			// ->where('shipp.shipping_id',$post['shipping_id'])
      ->where_in('shipp.shipping_id', $shippingId)
			->group_by('shipp.shipping_id, outb.code, outb.document_id,
				d.destination_name, d.destination_code, d.destination_address,d.destination_phone, d.destination_contact_person,
				usr.user_name, st.name, shipp.code, shipp.date, shipp.shipping_start, shipp.shipping_finish, shipp.driver, shipp.license_plate');

		$result = $this->db->get();
		return $result;
  }

	public function getDetailPacking($params, $manifest=''){
		$requestData = $_REQUEST;

		$data = [];
    $result_packing = [];
		if($manifest != 'item_packing'){

			// $manifest = $this->db
			// 		->select('manifest_number, outbound_id')
			// 		->from('manifests')
			// 		->where('shipping_code', $params['manifest_code'])
			// 		->get()
			// 		->row_array();


			$data_manifest = $this->db
					->select('id, shipping_id, shipping_code, outbound_id')
					->from('manifests')
					->where('manifest_number', $params['manifest_code'])
					->get()
					->result_array();
					$params = [];

				foreach($data_manifest as $key => $data_shipping){
          $params["shipping_id"][] = $data_shipping['outbound_id'];
					// $params["shipping_id"][] = $data_shipping['shipping_id'];
				}

        $pl_id = $this->db->select('pr.picking_id')
                          ->from('delivery_pl dpl')
                          ->join('picking_recomendation pr', 'pr.id = dpl.pl_id')
                          ->where_in('dpl.dn_id', $params["shipping_id"])
                          ->get()
                          ->result_array();

       $pl_id = array_column($pl_id, 'picking_id');

		}

    $pl_id = $this->db->select('pr.picking_id')
                      ->from('delivery_pl dpl')
                      ->join('picking_recomendation pr', 'pr.id = dpl.pl_id')
                      ->where_in('dpl.dn_id', $params["shipping_id"])
                      ->get()
                      ->result_array();

   $pl_id = array_column($pl_id, 'picking_id');

		$results = $this->db
					->select('oird.id_outbound_receiving_barang, oird.id_item_receiving_details
								, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, SUM(pr.qty) as qty, unt.name as uom_name')
					->from('outbound_item_receiving_details oird')
					->join('item_receiving_details ird', 'ird.id=oird.id_item_receiving_details', 'LEFT')
					->join('picking_recomendation pr', 'pr.item_receiving_detail_id=ird.id', 'LEFT')
					->join('items itm', 'itm.id=ird.item_id', 'LEFT')
					->join('units unt','unt.id=itm.unit_id','left')
					->join('outbound outb', 'outb.id = oird.id_outbound', 'LEFT')
					->join('picking_list_outbound plo', 'plo.id_outbound=outb.id')
					->join('pickings p', 'p.pl_id=plo.pl_id')
					->join('shipping_picking sp','sp.pl_id=p.pl_id')
					->join('shippings shipp', 'shipp.shipping_id=sp.shipping_id')
					->where_in('oird.picking_id', $pl_id)
					->group_by('oird.id_outbound_receiving_barang, oird.id_item_receiving_details, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, pr.qty, unt.name')
					->get()
					->result_array();


					foreach($results as $key => $result){
						$packing_number = $result['packing_number'];
						$data[$packing_number][$key] = array( 'name' => $result['name'], 'unique_code' => $result['unique_code'], 'item_code' => $result['code'], 'qty' => $result['qty'], 'name_uom' => $result['uom_name']);
					}

					if($manifest == 'item_packing'){
							$i = 0;


						foreach($data[$params["packing_id"]] as $key=>$item_packing_data){
							$nested = array();
							$nested[] = $i + 1 ;
							$nested[] = $item_packing_data['item_code'];
							$nested[] = $item_packing_data['name'];
							$nested[] = $item_packing_data['unique_code'];
							$nested[] = $item_packing_data['qty'].' '.$item_packing_data['name_uom'];
							$nested[] = '';
							$nested[] = '';
							$result_packing[] = $nested;
							$i++;
					}
						$results_new = array(
							"draw"            => intval( $requestData['draw'] ),
							"recordsTotal"    => intval( count($result_packing) ),
							"recordsFiltered" => intval( count($result_packing) ),
							"data"            => $result_packing
						);

							return $results_new;
					}

					if($manifest == 'manifest'){
						return $data;
					}

					$i = 0;
					foreach($data as $key=>$packing_data){
						$nested = array();
						$nested[] = $i + 1 ;
						$nested[] = $key;
						$nested[] = '';
						$nested[] = '-';
						$nested[] = $params["shipping_id"];

						$result_packing[] = $nested;
						$i++;
					}

					$results_new = array(
						"draw"            => intval( $requestData['draw'] ),
						"recordsTotal"    => intval( count($result_packing) ),
						"recordsFiltered" => intval( count($result_packing) ),
						"data"            => $result_packing
					);


					return $results_new;

	}

	public function getDoNumber($post = array()){
		$result = array();

		$sql = 'SELECT
					do_number
				FROM
					shipping_delivery_order
				WHERE
					shipping_id=\''.$post['shipping_id'].'\'';

		$row = $this->db->query($sql)->row_array();
		if($row){
			$result = $row;
		}else{
			$result['do_number'] = '';
		}

		return $result;
	}

	public function statusOptions($default = '--Pilih Status--', $key = '') {

    	$this->db
			 ->select(["s.status_id as status_id","s.name as name"])
			 ->from("shippings ship")
			 ->join("status s","ship.status_id=s.status_id")
			 ->group_by("s.status_id");
        $data = $this->db->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->status_id] = $row->name ;
        }
        return $options;
    }

	public function getScannedList($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_unik',
			2 => 'tgl_exp_sort',
			3 => 'tgl_in_sort',
			4 => 'user_name'
		);

		$this->db->start_cache();

		$this->db
			->select(["DISTINCT(ird.unique_code) as kd_unik"])
			// ->from('shippings shipp')
			// ->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
			->from('picking_recomendation pr')
			->join('item_receiving_details ird', 'ird.id=pr.item_receiving_detail_id')
			->join('items itm','itm.id=ird.item_id')
			->join('units unt','unt.id=itm.unit_id','left')
			->join('users usr','usr.id=ird.user_id_loading','left')
			->where('pr.id', $post['shipping_id'])
			->where('pr.item_id',$post['id_barang'])
			->where('pr.location_id=104',NULL)
			->group_by('ird.unique_code');

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->select([
				"tgl_exp as tgl_exp_sort",
				"tgl_in as tgl_in_sort",
				"COALESCE(TO_CHAR(tgl_exp, 'DD/MM/YYYY'), '-') as tgl_exp",
				"COALESCE(TO_CHAR(tgl_in, 'DD/MM/YYYY'), '') as tgl_in", "COALESCE(usr.user_name, '-') as user_name","SUM(pr.qty) as qty",
				// "shipp.status_id"
        "pr.location_id"
			])
			->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir'])
			->limit($requestData['length'],  $requestData['start'])
			// ->group_by('shipp.shipping_id, ird.tgl_exp, ird.tgl_in, usr.user_name, shipp.status_id');
      ->group_by('ird.tgl_exp, pr.location_id, ird.tgl_in, usr.user_name');


		$this->db->select('count(*) as total');

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = " - ";
            if ($this->access_right->otoritas('delete')) {

            	$disabledClass="";
            	if($row[$i]['location_id'] == 104){
    	        	$disabledClass = "disabled-link";
	            }

	            $action = '<button class="btn blue btn-xs '.$disabledClass.'" type="button" id="data-table-cancel" data-id="'.$row[$i]['kd_unik'].'">
        				Cancel
                </button>';

			}

			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['qty'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['user_name'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getSjPacking($shippingId){

		$results = $this->db
					->select('oird.id_outbound_receiving_barang, oird.id_item_receiving_details
								, oird.id_outbound, oird.packing_number,outb.code as outbound_code, oird.inc, itm.code, itm.name, ird.unique_code, SUM(pr.qty) as qty, unt.name as uom_name')
					->from('outbound_item_receiving_details oird')
					->join('item_receiving_details ird', 'ird.id=oird.id_item_receiving_details', 'LEFT')
					->join('picking_recomendation pr', 'pr.item_receiving_detail_id=ird.id', 'LEFT')
					->join('items itm', 'itm.id=ird.item_id', 'LEFT')
					->join('units unt','unt.id=itm.unit_id','left')
					->join('outbound outb', 'outb.id = oird.id_outbound', 'LEFT')
					->join('picking_list_outbound plo', 'plo.id_outbound=outb.id')
					->join('pickings p', 'p.pl_id=plo.pl_id')
					->join('shipping_picking sp','sp.pl_id=p.pl_id')
					->join('shippings shipp', 'shipp.shipping_id=sp.shipping_id')
					->where_in('oird.shipping_id', $shippingId['shipping_id'])
					->group_by('oird.id_outbound_receiving_barang, outb.code,outb.id, oird.id_item_receiving_details, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, pr.qty, unt.name')
					->get()
					->result_array();


					foreach($results as $key => $result){
						$packing_number = $result['packing_number'] . sprintf('%03d', $result['inc']);
						$data[$packing_number][$key] = array( 'name' => $result['name'], 'unique_code' => $result['unique_code'], 'item_code' => $result['code'], 'qty' => $result['qty'], 'name_uom' => $result['uom_name']);
					}

					return $data;

	}

    public function getLoadingPack($param=array()){

		$data = array();

        $this->db
			->select('shipp.shipping_id, shipp.code as sj_code,shipp.manifest_code, outb.code as outbound_code, outb.destination_name, outb.address, outb.address_1, outb.address_2, outb.address_3, itm.id as item_id, itm.name as item_name, itm.code as item_code, shipp.driver as driver_name, shipp.license_plate as plate_number, wh.name as warehouse_name')
        	// ->select('shipp.shipping_id, shipp.code as shipping_code,shipp.sj_code as sj_code, outb.code as outbound_code, outb.destination_name, outb.address, outb.address_1, outb.address_2, outb.address_3, shipp.driver as driver_name, shipp.license_plate as plate_number, wh.name as warehouse_name')
        	->from('shippings shipp')
        	// ->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	// ->join('pickings pl','pl.pl_id=sp.pl_id')
        	// ->join('picking_list_outbound plo','plo.pl_id=pl.pl_id')
        	->join('outbound outb','outb.id=shipp.outbound_id')
        	->join("warehouses wh",'wh.id=outb.warehouse_id')
        	->join('picking_recomendation pr','pr.outbound_id=shipp.outbound_id')
        	// ->join('item_receiving_details ird','ird.picking_id=pr.picking_id and ird.unique_code=pr.unique_code and ird.item_id=pr.item_id')
        	->join('items itm','itm.id=pr.item_id','left')
			// ->where('shipp.outbound_id', $param['outbound_id'])
			->where('shipp.shipping_id', $param['shipping_id'])
			->group_by('shipp.shipping_id, shipp.code, shipp.manifest_code, outb.code, outb.destination_name, outb.address_1, outb.address_2, outb.address_3, itm.name, itm.id, itm.code, outb.address, wh.name');
        	// ->group_by('shipp.shipping_id, shipp.code,shipp.sj_code, outb.code, outb.destination_name, outb.address_1, outb.address_2, outb.address_3, outb.address, wh.name');


		$summary = $this->db->get()->result_array();

        if($summary){

        	$data = array(
        		"shipping_id" 		=> $summary[0]['shipping_id'],
				"shipping_code"		=> $summary[0]['shipping_code'],
				"sj_code"			=> $summary[0]['sj_code'],
        		"outbound_code"		=> $summary[0]['outbound_code'],
        		"destination_name"	=> $summary[0]['destination_name'],
        		"address_1"			=> $summary[0]['address_1'],
        		"address_2"			=> $summary[0]['address_2'],
        		"address_3"			=> $summary[0]['address_3'],
        		"driver_name"		=> $summary[0]['driver_name'],
        		"address"			=> $summary[0]['address'],
        		"plate_number"		=> $summary[0]['plate_number'],
        		'warehouse_name'	=> $summary[0]['warehouse_name']
        	);

	        $i=0;
	        foreach ($summary as $sum) {

	        	$parentI = $i;

	        	$data['items'][$i] = array(
	        		"item_name"		=> $sum['item_name'],
	        		"item_code"		=> $sum['item_code'],
	        		"parent_code"	=> "-",
	        		"qty_per_colly"	=> "-",
	        		"total_colly"	=> 0,
	        		"qty_pcs"		=> 0
				);


	        	$i++;

		        $this->db
					->select(["COALESCE(SUM(pr.qty),0) as qty","ird.parent_code","itm.name as item_name","itm.code as item_code","ird.parent_code",
						"(
							SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
								SELECT COALESCE((SUM(pr.qty)* CAST(itm.weight as float8)),0) as weight FROM shippings shipp
								JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id
								JOIN pickings pl ON pl.pl_id=sp.pl_id
								JOIN picking_qty pq ON pq.picking_id=pl.pl_id
								JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=pq.item_id
								LEFT JOIN items itm ON itm.id=pr.item_id
								WHERE itm.id =".$sum['item_id']." group by itm.weight
							) as tbl
						) as weight",
						"(
							SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
								SELECT COALESCE((SUM(pr.qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM shippings shipp
								JOIN shipping_picking sp ON sp.shipping_id=shipp.shipping_id
								JOIN pickings pl ON pl.pl_id=sp.pl_id
								JOIN picking_qty pq ON pq.picking_id=pl.pl_id
								JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=pq.item_id
								LEFT JOIN items itm ON itm.id=pr.item_id
								WHERE itm.id =".$sum['item_id']." group by itm.height, itm.width, itm.length
							) as tbl
						) as volume",
							])
		        	->from('shippings shipp')
		        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
		        	->join('pickings pl','pl.pl_id=sp.pl_id')
		        	->join('picking_qty pq','pq.picking_id=pl.pl_id')
		        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id and pr.item_id=pq.item_id')
		        	->join('item_receiving_details ird','ird.id=pr.item_receiving_detail_id')
		        	->join('items itm','itm.id=pr.item_id','left')
					->where('shipp.shipping_id', $param['shipping_id'])
					// ->where('shipp.outbound_id', $param['outbound_id'])
		        	->where('itm.id',$sum['item_id'])
		        	->group_by('itm.name, itm.code, pl.pl_id, ird.parent_code');

				$res = $this->db->get()->result_array();

		        if($res){

			        foreach ($res as $det) {

			        	$data['items'][$parentI]['total_colly'] += 1;
						$data['items'][$parentI]['qty_pcs'] += $det['qty'];
						$data['items'][$parentI]['weight'] = $det['weight'].' '.WEIGHT_UOM;
						$data['items'][$parentI]['volume'] = $det['volume'].' '.VOLUME_UOM;


			        	// $data['items'][$i] = array(

			        	// 	"item_name"		=> $det['item_name'],
			        	// 	"item_code"		=> $det['item_code'],
			        	// 	"parent_code"	=> $det['parent_code'],
			        	// 	"qty_per_colly"	=> $det['qty'],
			        	// 	"total_colly"	=> "-",
			        	// 	"qty_pcs"		=> "-"
			        	// );

			        	$i++;

			        }

			    }
			}

		}

        return $data;
    }

    public function getLoadingTruck($param=array()){

        $this->db
        	->select(['shipp.license_plate',"string_agg(DISTINCT parent_code, ', ') as outerlabel","COALESCE(SUM(pr.qty),0) as qty"])
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $param['shipping_id'])
        	->group_by('shipp.license_plate');

        $result = $this->db->get();

        return $result->result_array();

    }

    public function getLoadingPackNew($param=array()){

        $this->db
        	->select('shipp.shipping_id, shipp.code as shipping_code, outb.code as outbound_code, outb.destination_name, outb.address_1, outb.address_2, outb.address_3, itm.name as item_name, itm.code as item_code')
        	->select(["COALESCE(SUM(pr.qty),0) as qty","COALESCE((SELECT string_agg(DISTINCT parent_code, ', ')  FROM item_receiving_details WHERE picking_id=pl.pl_id group by picking_id), '-') as outerlabel"])
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	// ->join('shipping_scanned ss','ss.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_list_outbound plo','plo.pl_id=pl.pl_id')
        	->join('outbound outb','outb.id=plo.id_outbound')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $param['shipping_id'])
        	->group_by('shipp.shipping_id, shipp.code, outb.code, outb.destination_name, outb.address_1, outb.address_2, outb.address_3, itm.name, itm.code, pl.pl_id');

        $result = $this->db->get();

        return $result->result_array();
    }

    public function getLoadingTruckNew($param=array()){

        $this->db
        	->select(['shipp.license_plate',"string_agg(DISTINCT parent_code, ', ') as outerlabel","COALESCE(SUM(pr.qty),0) as qty"])
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $param['shipping_id'])
        	->group_by('shipp.license_plate');

        $result = $this->db->get();

        return $result->result_array();

    }

	public function closeLoading($post = array()){
		$result = array();

		$sql = 'SELECT
					st_shipping
				FROM
					shipping
				WHERE
					shipping_id=\''.$post['shipping_id'].'\'';

		$row = $this->db->query($sql)->row_array();
		if($row['st_shipping'] == 1){

			$result['status'] = 'ERR';
			$result['message'] = 'Loading already finish.';

		}else{
			$rectime = date('Y-m-d H:i:s');

			$sql = 'UPDATE
						shipping
					SET
						st_shipping=\'1\'
						shipping_finish=\''.$rectime.'\'
					WHERE
						shipping_id=\''.$post['shipping_id'].'\'';

			$q = $this->db->query($sql);

			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Finish loading success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Finish loading failed';
			}

		}

		return $result;
	}

	public function checkDoNumber($post = array()){
		$result = array();

		$sql = 'SELECT COUNT(*) AS total
				FROM shipping_delivery_order
				WHERE
					do_number=\''.$post['do_number'].'\'
				AND
					shipping_id!=\''.$post['shipping_id'].'\'';

		$row = $this->db->query($sql)->row_array();

		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Delivery Order number already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}

		return $result;
	}

	public function saveDo($post = array()){
		$result = array();

		$sql = 'SELECT COUNT(*) AS total
				FROM shipping_delivery_order
				WHERE shipping_id=\''.$post['shipping_id'].'\'';

		$row = $this->db->query($sql)->row_array();

		if($row['total'] > 0){
			$rectime = date('Y-m-d H:i:s');
			$sql = 'UPDATE
						shipping_delivery_order
					SET
						do_number=\''.$post['do_number'].'\',
						update_by=\''.$this->session->userdata('user_id').'\',
						modified_date=\''.$rectime.'\'
					WHERE
						shipping_id=\''.$post['shipping_id'].'\'';

			$this->db->query($sql);

		}else{

			$rectime = date('Y-m-d H:i:s');
			$sql = 'INSERT INTO shipping_delivery_order(
						do_number,
						shipping_id,
						created_by,
						created_date,
						update_by,
						modified_date
					)VALUES(
						\''.$post['do_number'].'\',
						\''.$post['shipping_id'].'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\'
					)';

			$this->db->query($sql);
		}

		return $result;
	}

	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 =>  'pl_id_qty',
			1 => 'shipping_id',
			2 => 'id_barang',
			3 => '',
			4 => 'kd_barang',
			5 => 'nama_barang',
			6 => 'picked'
		);

		$where = '';

    $manifest_number = $this->db
                            ->select('man.manifest_number, man.shipping_id, man.shipping_code')
                            ->from('manifests man')
                            ->where('man.shipping_id', $post['shipping_id'])
                            ->get()
                            ->row_array();

    $shippingId  = $this->db
                          ->select('man.shipping_id, man.shipping_code, outbound_id')
                          ->from('manifests man')
                          ->where('man.manifest_number', $manifest_number['manifest_number'])
                          ->get()
                          ->result_array();


    $OutboundIds = array_column($shippingId, 'outbound_id');

    $shippingIds = array_column($shippingId, 'shipping_id');

		$this->db->start_cache();

		$this->db
			->select(["DISTINCT(pr.item_id)"])
      ->from('picking_recomendation pr')
      ->join('delivery_notes dn', 'dn.id = pr.id', 'left')
			// ->from('shippings s')
      // ->join('outbound outb', 'outb.id = pr.outbound_id', 'LEFT')
			// ->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
			// ->join('pickings p','p.pl_id=sp.pl_id')
      ->join('picking_qty pq','pq.picking_id=pr.picking_id AND pq.item_id = pr.item_id')
			// ->join('picking_recomendation pr','pr.outbound_id=s.outbound_id')
			->join('items itm','itm.id=pr.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			// ->where_in('s.shipping_id', $shippingIds)
      ->where_in('pr.id', $OutboundIds)
			->group_by('pr.item_id');


		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();
		$this->db
			->select([
				"pq.id as pl_id_qty","dn.name as delivery_notes_name","pr.id as pr_id", "itm.id as id_barang","SUM(pq.qty) as picked","itm.code as kd_barang","CONCAT(itm.name, ' - ', itm.code) as nama_barang",
				"COALESCE(unt.name, '') as nama_satuan"
			])
			->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir'])
			->limit($requestData['length'],  $requestData['start'])
			->group_by('pq.id, itm.id, unt.name, itm.code,pr.id ,itm.name, dn.name');

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['pl_id_qty'];
			$nested[] = $row[$i]['pr_id'];
      // $nested[] = '';

			$nested[] = $row[$i]['id_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
      // $nested[] = '<a href="'.base_url().'outbound/detail/'.$row[$i]['outbound_id'].'">'.$row[$i]['outbound_code'].'</a>';
      $nested[] = $row[$i]['delivery_notes_name'];

			$nested[] = $row[$i]['picked'] . ' ' . $row[$i]['nama_satuan'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailPicking($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'pl_id',
			1 => '',
			2 => 'pl_name',
			3 => 'pl_date_sort',
			4 => 'remark',
			5 => 'pl_start_sort',
			6 => 'pl_finish_sort',
			7 => 'nama_status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['shipping_id'])){
			$this->db->where('shipp.shipping_id', $post['shipping_id']);
		}

		$this->db
			->from('shippings shipp')
			->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
			->join('pickings pl','pl.pl_id=sp.pl_id')
			->join('users usr','usr.id=shipp.user_id','left')
			->join('status st','st.status_id=pl.status_id','left')
			->where_in('shipp.shipping_id',$post['shipping_id'])
			->group_by('pl.pl_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select([
				"pl.pl_id","pl.name as pl_name",
				"pl.date as pl_date_sort", "start_time as pl_start_sort", "end_time as pl_finish_sort",
				"TO_CHAR(pl.date, 'DD/MM/YYYY') as pl_date",
				"COALESCE(remark, ' - ') as remark",
				"COALESCE(TO_CHAR(start_time, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY</span>'), '-') as pl_start",
				"COALESCE(TO_CHAR(end_time, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY</span>'), '-') as pl_finish",
				"COALESCE(TO_CHAR(shipping_start, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY</span>'), '-') as shipping_start",
				"COALESCE(TO_CHAR(shipping_finish, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY</span>'), '-') as shipping_finish",
				"pl.status_id",
				"st.name as nama_status"
			])
			->group_by('shipp.shipping_start, shipping_finish, st.name, pl.name, pl.date, pl.remark, pl.start_time, pl.end_time, pl.status_id, st.name')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'picking_list/detail/'.$row[$i]['pl_id'].'">'.$row[$i]['pl_name'].'</a>';
			$nested[] = $row[$i]['pl_date'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['pl_start'];
			$nested[] = $row[$i]['pl_finish'];
			$nested[] = $row[$i]['nama_status'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$this->db
			->select(["shipp.shipping_id","shipp.code as shipping_code","outb.code as kd_outbound","outb.document_id as id_outbound_document","TO_CHAR(shipp.date, 'DD/MM/YYYY') as shipping_date",
				"d.destination_code as kd_supplier","d.destination_name as nama_supplier",
				"d.destination_address as alamat_supplier","d.destination_phone as telepon_supplier","d.destination_contact_person as cp_supplier",
				"CONCAT(d.destination_code, ' - ', d.destination_name) as customer_name",
				"COALESCE(TO_CHAR(shipping_start, 'DD/MM/YYYY HH24:MI:SS'), '-') as shipping_start","COALESCE(TO_CHAR(shipping_finish, 'DD/MM/YYYY HH24:MI:SS'), '-') as shipping_finish","user_name","st.name as nama_status","driver as driver_name","license_plate"])
			->from('shippings shipp')
			->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
			->join('pickings pl','pl.pl_id=sp.pl_id')
			->join('picking_list_outbound plo','plo.pl_id=pl.pl_id')
			->join('outbound outb','outb.id=shipp.outbound_id')
			->join('destinations d','d.destination_id=outb.destination_id','left')
			->join('users usr','usr.id=shipp.user_id','left')
			->join('status st','st.status_id=shipp.status_id','left')
			->where('shipp.shipping_id',$post['shipping_id'])
			->group_by('shipp.shipping_id, outb.code, outb.document_id,

				d.destination_name, d.destination_code, d.destination_address,d.destination_phone, d.destination_contact_person,
				usr.user_name, st.name, shipp.code, shipp.date, shipp.shipping_start, shipp.shipping_finish, shipp.driver, shipp.license_plate');

		$result = $this->db->get();
		return $result;
	}

	public function getOutboundCompletePicking(){
		$result = array();

		$sql = 'SELECT
					outb.id_outbound, kd_outbound
				FROM
					outbound outb
				JOIN picking_list_outbound plo
					ON plo.id_outbound=outb.id_outbound
				WHERE
					outb.id_status_outbound != \'2\'
				ORDER BY
					id_outbound DESC';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getPickingDocument($post = array()){
		$result = array();

		$where = '';
		if(isset($post['shipping_id'])){
			$this->db->where('p.status_id', 9);
		}else{
			// $this->db->where('p.pl_id NOT IN ((SELECT pl_id FROM shipping_picking))', NULL);
			$this->db->where('p.status_id', 8);
		}

		if(isset($post['id_outbound'])){
			$this->db->where('outb.id', $post['id_outbound']);
		}


		$this->db
			->select(["p.pl_id", "p.name as pl_name","TO_CHAR(p.date, 'DD/MM/YYYY') as pl_date","COALESCE(CONCAT(spl.code, ' - ', spl.name), cust.name) as customer_name"])
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id','left')
			->join('suppliers spl','spl.id=outb.customer_id AND outb.document_id = 2','left outer')
			->join('customers cust','cust.id=outb.customer_id AND outb.document_id <> 2','left outer')
			->group_by('p.pl_id, spl.code, spl.name, cust.name,p.name, p.date');

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getItems($post = array()){
		$result = array();

		$this->db
			->select(["COALESCE(SUM(pr.qty),0) as qty",'itm.code as kd_barang',"CONCAT(itm.name, ' - ', itm.code) as nama_barang","COALESCE(unt.name, '') as nama_satuan"])
			->from('shippings s')
			->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
			->join('pickings p','p.pl_id=sp.pl_id')
			->join('picking_qty pq','pq.picking_id=p.pl_id')
			->join('picking_recomendation pr','pr.picking_id=p.pl_id and pr.item_id=pq.item_id')
			->join('items itm','itm.id=pq.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->where('s.shipping_id',$post['shipping_id'])
			->group_by('itm.code, itm.name, unt.name')
			;

		$row = $this->db->get()->result_array();
		$result['data'] = $row;

		return $result;
	}

	public function delete($post = array()){
		$result = array();

		$this->db->select('count(*) as total')
			->from('shippings')
			->where('shipping_id', $post['shipping_id'])
			->where('shipping_start IS NOT NULL', NULL);

		$r = $this->db->get()->row_array();
		if($r['total'] > 0){

			$result['status'] = 'ERR';
			$result['message'] = 'Delete data failed, because loading process already started';

		}else{

			$this->db->trans_start();

			$this->db
				->set('status_id',8)
				->where('pl_id IN (SELECT pl_id FROM shipping_picking WHERE shipping_id='.$post['shipping_id'].')', NULL)
				->update('pickings');

			$this->db->delete('shipping_picking',['shipping_id'=>$post['shipping_id']]);
			// $this->db->delete('shippings',['shipping_id'=>$post['shipping_id']]);

			$this->db
				->set('status_id',17)
				->where('shipping_id',$post['shipping_id'])
				->update('shippings');

			$this->db->trans_complete();

			if($this->db->trans_status()){
				$result['status'] = 'OK';
				$result['message'] = 'Delete success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Delete failed';
			}
		}

		return $result;
	}

	public function getOutboundDocument($post = array()){
		$result = array();

		$sql = 'SELECT
					pl.pl_id, pl_name, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS  pl_date,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					IFNULL(alamat_supplier, address) AS address
				FROM
					shipping shipp
				JOIN shipping_picking shipppick
					ON shipppick.shipping_id=shipppick.shipping_id
				JOIN picking_list pl
					ON pl.pl_id=shipppick.pl_id
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				LEFT JOIN m_status_outbound mso
					ON mso.id_status_outbound=outb.id_status_outbound
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				WHERE
					shipp.shipping_id=\''.$post['shipping_id'].'\'';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function shippingGroup(){
		$result = $this->db
				->select('DISTINCT(outb.shipping_group) as shipping_group')
				->from('outbound outb')
				->where('outb.shipping_group !=','')
				->get()
				->result_array();

		return $result;
	}

	public function getDocOutByshipGroup($params){
		$result = $this->db
    ->select(["DISTINCT(dn.id)", "dn.name", "dest.destination_id", "dest.destination_name"])
					// ->select(["DISTINCT(oird.packing_number)","outb.id", "outb.code", "outb.destination_id", "outb.destination_name"])
					->from('delivery_notes dn')
          ->join('delivery_pl dpl', 'dpl.dn_id = dn.id', 'left')
          ->join('picking_recomendation pr', 'pr.id = dpl.pl_id', 'left')
          ->join('outbound outb', 'outb.id = pr.outbound_id', 'left')
          ->join('destinations dest', 'dest.destination_id = outb.destination_id')
          ->join('gate gt', 'gt.id = dest.gate')
					->like('lower(dn.name)', strtolower($params['query']))
          ->or_like('lower(dest.destination_name)', strtolower($params['query']))
					->where('gt.id', $params['shippingGroup'])
					->get()
					->result_array(); 

		$data = array();
		foreach($result as $key => $value){

			$data['results'][] = array(
				'id' => $value['id'],
				'code' => $value['name'].' - '.$value['destination_name'],
				'destination_id' => $value['destination_id'],
				'destination_name' => $value['destination_name']

			);
		}

		return $data;
	}

	public function getEdit($post = array()){
		$result = array();
		$data = [];

		$manifest = $this->db
						->select('manifest_number, outbound_id')
						->from('manifests')
						->where('shipping_id', $post['shipping_id'])
						->get()
						->row_array();

		$data_outbound = $this->db
								->select('outbound_id, shipping_id')
								->from('shippings s')
								->where('s.manifest_code', $manifest['manifest_number'])
								->get()
								->result_array();

		foreach($data_outbound as $key => $value){
			$data['id_outbound'][] = $value['outbound_id'];
      $params["shipping_id"][] = $value['shipping_id'];
		}


		$this->db
			->select([
				"DISTINCT(manf.manifest_number) as manifest_number",
				"TO_CHAR(s.date, 'DD/MM/YYYY') as shipping_date",
				"d.destination_name as customer_name",
				"d.destination_address as address",
				"outb.shipping_group as shipping_route",
				"s.driver as driver_name","license_plate", "s.shipping_id", "s.code as shipping_code", "outb.code as kd_outbound","s.barcode"
			])
			->from('shippings s')
			->join('manifests manf' ,'manf.shipping_id = s.shipping_id')
			// ->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
			// ->join('pickings p','p.pl_id=sp.pl_id')
			// ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=s.outbound_id')
			// ->join('outbound_item_receiving_details oird', 'oird.id_outbound = outb.id', 'left')
			->join('destinations d','d.destination_id=outb.destination_id','left')
			->where_in('s.shipping_id', $params['shipping_id']);

			// ->where('oird.shipping_id', $post['shipping_id']);

		$result = $this->db->get()->row_array();
		array_push($result, $data);
		return $result;
	}

	public function updateOutbound1($idOutbound, $data = array(), $id){

		$sql = 'DELETE FROM shipping_picking WHERE shipping_id=\''.$id.'\'';
		$this->db->query($sql);

		$len = count($data);
		for($i = 0; $i < $len; $i++){

			$sql = 'INSERT INTO shipping_picking(
						shipping_id,
						id_outbound,
						pl_id
					)VALUES(
						\''.$id.'\',
						\''.$idOutbound.'\',
						\''.$data[$i].'\'
					)';

			$this->db->query($sql);

			//update_shipping_id_in_receiving_barang ?
		}
	}

	public function updateShippingPicking($idOutbound, $idPickings, $id, $key = ''){

		// $this->db->delete('shipping_picking', ['shipping_id'=>$id]);

		// $this->db
		// 	->set('shipping_id', $id)
		// 	->set('pl_id', $idPicking)
		// 	->insert('shipping_picking');

		// foreach(array_unique($idPickings) as $key => $idPicking){
			$idPicking = $idPickings[$key];
			$this->db->delete('shipping_picking', ['shipping_id'=>$id]);

			$this->db
				->set('shipping_id', $id)
				->set('pl_id', $idPicking)
				->insert('shipping_picking');
		// }

	}

	public function updateShippingIdInReceivingBarang($shippingId){
		$result = array();

		$this->db
			->select('item_id as id_barang, p.pl_id')
			->from('shippings s')
			->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
			->join('pickings p','p.pl_id=sp.pl_id')
			->join('picking_qty pq','pq.picking_id=p.pl_id')
			->where('s.shipping_id', $shippingId);

		$row = $this->db->get()->result_array();

		$len = count($row);

		for($i = 0; $i < $len; $i++){

			$this->db
				->set('shipping_id', $shippingId)
				->where('item_id', $row[$i]['id_barang'])
				->where('picking_id', $row[$i]['pl_id'])
				->update('item_receiving_details');

		}

		return $result;
	}

	public function updatePicking($picking_ids){
		foreach($picking_ids as $key => $id){
			$sql = 'UPDATE pickings SET status_id = 9 WHERE pl_id = \''.$id.'\'';
			$this->db->query($sql);
		}
	}

	public function setShipmentDoc($pickingId, $shippingId){

		$sql = 'SELECT
					shipment_doc
				FROM
					picking_list_outbound plo
				JOIN shipping_doc sd
					ON sd.outbound_id=plo.id_outbound
				WHERE
					pl_id=\''.$pickingId.'\'';


		$r = $this->db->get()->row_array();
		if($r){

			$rectime = date('Y-m-d H:i:s');

			$sql = 'INSERT INTO shipping_delivery_order(
						shipping_id,
						do_number,
						created_by,
						create_at,
						update_by,
						updated_at
					)VALUES(
						\''.$shippingId.'\',
						\''.$r['shipment_doc'].'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\'
					)';

			$this->db->query($sql);

		}
	}


	private function data_insert($data, $shippingCode = ''){

		$data = array(
			"manifest_code" 			=> $data['shipping_code'],
			"code"			=> $shippingCode,
			"date" => $data['shipping_date'],
			"driver"	=> $data['driver_name'],
			"license_plate" => $data['license_plate'],
			'barcode'	=> $data['barcode'],
			"user_id"		=> $data['user_id'],
			// "outbound_id" => $data['outbound_id'],
		);

		return $data;
	}

  public function update($datas, $idOutbound, $picking,  $id_colly='', $ids_destination, $id) {

    $data_manifest = $this->db->select('manifest_code')->from('shippings')->where('shipping_id', $id)->get()->row_array()['manifest_code'];

    $this->db->delete('shippings', ['manifest_code' => $data_manifest]);
    $this->db->delete('manifests', ['manifest_number' => $data_manifest]);

    foreach($datas['outbound_id'] as $key => $outbound_id){

      $shippingCode = 'SJ-CL-'. $this->randomidgenerator->generateRandomString(4);

      $data = $this->data_insert($datas, $shippingCode);
			$data['outbound_id'] = $outbound_id;
			$data['status_id'] = 3;


			$this->db->trans_start();
			$this->db->insert('shippings', $data);
			$this->db->trans_complete();

      $data['gate'] = $datas['gate'];

			$this->db->order_by('shipping_id','desc');
			$this->db->limit(1);

			$shippingId = $this->db->get('shippings')->row_array()['shipping_id'];


			$this->updateManifest($data, $shippingId, $shippingCode);

      // $data= $this->data_insert($datas, $shippingCode);
      // $this->db->update('shippings', $data, array('shipping_id' => $id));
      // $this->updateShippingPicking($idOutbound, $picking, $id);
      // $this->updateManifest($data, $shippingId, $shippingCode);

      // code...
    }

    return true;

  //INTEGRATION
  // $this->setShipmentDoc($picking, $id);
  }

	public function create($datas, $idOutbound, $picking,  $id_colly='', $ids_destination) {
		// $total_colly = count($id_colly);
    $idOutbound = '';
		foreach($datas['outbound_id'] as $key => $outbound_id){

      $outbound_id = $this->db->select('outbound_id')->from('picking_recomendation pr')->join('delivery_pl dpl', 'dpl.pl_id = pr.id', 'left')->where('dpl.dn_id', $outbound_id)->get()->row_array()['outbound_id'];

			$shippingCode = 'SJ-CL-'. $this->randomidgenerator->generateRandomString(4);
			// if(isset($datas['destination_id'][$key])){
			// 	$destination_id = $datas['destination_id'][$key];
      //
			// }
			$data = $this->data_insert($datas, $shippingCode);
			$data['outbound_id'] = $outbound_id;
			$data['status_id'] = 3;


			$this->db->trans_start();
			$this->db->insert('shippings', $data);
			$this->db->trans_complete();

      $data['gate'] = $datas['gate'];


			$this->db->order_by('shipping_id','desc');
			$this->db->limit(1);

			$shippingId = $this->db->get('shippings')->row_array()['shipping_id'];


			$this->updateManifest($data, $shippingId, $shippingCode);

      /*akan di buat di proses API SCAN PACKING*/

			// $this->updatePacking($id_colly, $picking, $datas, $shippingId, $destination_id, $ids_destination);
			// $this->updateShippingPicking($idOutbound, $picking, $shippingId, $key);
			// $this->updateShippingIdInReceivingBarang($shippingId);
		}
		// $shippingId = $this->db->insert_id();


		// $this->updatePicking($picking);

		//INTEGRATION
		// $this->setShipmentDoc($picking, $shippingId);

		return true;
	}

  public function get_gates(){

    $data = $this->db->select('id, gate_code, gate_province')->from('gate')->get();

    $result = $data->result_array();

    return $result;
  }

	public function updateManifest($data, $shippingId, $shippingCode){
		$data = array(
			'manifest_number' => $data['manifest_code'],
			'shipping_id' => $shippingId,
			'shipping_code' => $shippingCode,
			'outbound_id' => $data['outbound_id'],
			'total_colly'    => 0,
      'gate' => $data['gate']
		);

		$this->db->trans_start();
		$this->db->insert('manifests', $data);
		$this->db->trans_complete();
	}

	public function updatePacking($id_colly, $picking, $data, $shippingId, $destination_id, $ids_destination){

		$count_colly = count($id_colly);
		for($i = 0; $i < $count_colly; $i++){
			if($destination_id == $ids_destination[$i]){
				$query = 'UPDATE outbound_item_receiving_details oird
							SET shipping_id=\''.$shippingId.'\'
							FROM item_receiving_details ird WHERE ird.id = oird.id_item_receiving_details
							AND packing_number=\''.$data['packing'][$i].'\'
							AND inc=\''.$id_colly[$i].'\'
							AND ird.picking_id=\''.$picking[$i].'\'';

				$this->db->query($query);
			}
		}
	}



	public function add_shipping_code(){
        $this->db->set('inc_shipping',"inc_shipping+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

	public function getOutboundNotInShipping(){
		$result = array();

		$sql = 'SELECT
					outb.id_outbound, kd_outbound, DATE_FORMAT(tanggal_outbound, \'%d\/%m\/%Y\') AS  tanggal_outbound,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name
				FROM
					outbound outb
				JOIN picking_list pick
					ON pick.id_outbound=pick.id_outbound
				LEFT JOIN outbound_barang outbrg
					ON outbrg.id_outbound=outb.id_outbound
				LEFT JOIN receiving_barang rcvbrg
					ON rcvbrg.id_barang=outbrg.id_barang
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				WHERE
					rcvbrg.shipping_id IS NULL
				GROUP BY
					outb.id_outbound';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getLoadingCodeIncrement(){

		$result = '';

		$sql = 'SELECT
					CONCAT(pre_code_shipping, \'\', inc_shipping) AS loading_code
				FROM
					m_increment
				WHERE
					id_inc = \'1\'';

		$row = $this->db->query($sql)->row_array();
		$result = $row['loading_code'];

		return $result;

	}

	public function getLoadingCode($post = array()){
		$result = array();

		$sql = 'SELECT
					shipping_id, code as shipping_code
				FROM
					shippings
				WHERE
					code LIKE \'%'.$post['query'].'%\'
					AND status_id != 17';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getOuterLabelbyShipping($shippingId){

        $this->db
        	->select("ird.parent_code")
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $shippingId)
        	->group_by('ird.parent_code');

        $result = $this->db->get();

        return $result->result_array();

	}

	public function getSNByOuterLabel($shippingId,$outerLabel){

        $this->db
        	->select("ird.rfid_tag as serial_number, itm.code")
        	->from('shippings shipp')
        	->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
        	->join('pickings pl','pl.pl_id=sp.pl_id')
        	->join('picking_recomendation pr','pr.picking_id=pl.pl_id')
        	->join('item_receiving_details ird','ird.unique_code=pr.unique_code and ird.picking_id=pl.pl_id')
        	->join('items itm','itm.id=pr.item_id','left')
        	->where('shipp.shipping_id', $shippingId)
        	->where('ird.parent_code', $outerLabel)
        	->group_by('ird.rfid_tag, itm.code');

        $result = $this->db->get();

        return $result->result_array();

	}

    public function cancel($params=array()){

    	if(!empty($params)){

	    	$this->db
	    		->select('s.*, sp.*, sp.pl_id as picking_id')
	    		->from('shippings s')
	    		->join('shipping_picking sp','sp.shipping_id=s.shipping_id')
	    		->where('s.shipping_id',$params['shipping_id']);

	    	$data = $this->db->get()->row_array();

	    	if($data){

		    	$this->db->trans_start();

		    	$this->db
		    		->set('location_id',"(CASE WHEN location_id <> 104 THEN location_id ELSE 101 END)",FALSE)
		    		->where('id IN (SELECT item_receiving_detail_id FROM picking_recomendation where picking_id='.$data['picking_id'].')',NULL)
		    		->update('item_receiving_details');

		       	$this->db
		       		->set('location_id',101)
		       		->where('picking_id',$data['picking_id'])
		       		->update('picking_recomendation');

		        $this->db->set("status_id", 8);
		        $this->db->where("pl_id", $data['picking_id']);
		        $this->db->update("pickings");

		        $this->db
		        	->set("status_id", 17)
		        	->set("st_shipping", 1)
		        	->set('shipping_finish',date('Y-m-d h:i:s'))
		        	->where("shipping_id", $data['shipping_id'])
		        	->update("shippings");

		    	$this->db->trans_complete();

		    	return true;
		    }

    	}

    	return false;

    }


    function complete_loading($params=array()){

      $result = array();

      $result_manifest = $this->db->select('manifest_number')->from('manifests')->where('shipping_id', $params['shipping_id'])->get()->row_array()['manifest_number'];

      $result['manifest'] = $result_manifest;

          $this->db->trans_start();

          $this->db->set('status_id', 4);
          $this->db->set('shipping_finish', date('Y-m-d H:i:s'));
          $this->db->where('manifest_code', $result_manifest);
          $this->db->update('shippings');


          $this->db->set('status_manifest', 1);
          $this->db->where('manifest_number', $result_manifest);
          $this->db->update('manifests');

          $this->db->trans_complete();

          if($this->db->trans_status()){

            $result['status'] = TRUE;

            return $result;
          }else {
            return false;
          }
      }

    function cancel_item($params=array()){

    	if(!empty($params)){

	    	$this->db->trans_start();

	    	$this->db
	    		->select(["pr.picking_id","item_receiving_detail_id","unique_code","old_location_id","COALESCE(sum(qty),0) as qty"])
	    		->from('shipping_picking sp')
	    		->join('picking_recomendation pr','pr.picking_id=sp.pl_id')
	    		->where('sp.shipping_id',$params['shipping_id'])
	    		->where('pr.unique_code',$params['serial_number'])
	    		->group_by('picking_id, unique_code, old_location_id, item_receiving_detail_id')
	    		;

	    	$data = $this->db->get()->row_array();

	    	if($data){

		    	$this->db
		    		->set('shipping_id',NULL)
		    		->set('user_id_shipping',NULL)
		    		->set('location_id',"(CASE WHEN location_id <> 104 THEN location_id ELSE 101 END)",FALSE)
		    		->where('id',$data['item_receiving_detail_id'])
		    		->update('item_receiving_details');

		    	$this->db
		    		->set('location_id',101)
		    		->where('picking_id',$data['picking_id'])
		    		->where('unique_code',$params['serial_number'])
		    		->update('picking_recomendation');

		    	$this->db
		    		->set('st_shipping',0)
		    		->set('shipping_finish',NULL)
		    		->set('status_id',5)
		    		->where('shipping_id',$params['shipping_id'])
		    		->update('shippings');

		    	$this->db->trans_complete();

		    	return true;

		    }

    	}

    	return false;

    }

}

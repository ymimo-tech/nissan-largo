<?php

class Display_model extends CI_Model {

    function __construct() {
        parent::__construct();
        if($this->db->table_exists('display_inbound')){
			$this->db->query("
				CREATE OR REPLACE VIEW  display_inbound AS
                    SELECT
                        1 as id,
                        'INB001' as inb_doc,
                        'RCV-003' as rcv_doc,
                        'DO18009' as dlv_doc,
                        'Bhinneka' as supplier,
                        'B 8009 NJ' as licn_plate,
                        '50/90' as qty_tally_putaway,
                        'Tally' as last_status,
                        '9.30' as start_time
                    UNION ALL
                    SELECT
                        ib.id_inbound as id,
                        ib.kd_inbound as inb_doc,
                        rcv.kd_receiving as rcv_doc,
                        rcv.delivery_doc as dlv_doc,
                        su.nama_supplier as supplier,
                        rcv.vehicle_plate as licn_plate,
                        '50/90' as qty_tally_putaway,
                        'Tally' as last_status,
                        '9.30' as start_time
                    FROM
                        inbound ib
                        LEFT JOIN receiving rcv ON
                            ib.id_inbound = rcv.id_po
                        LEFT JOIN supplier su ON
                            ib.id_supplier = su.id_supplier
                    WHERE ib.tanggal_inbound >= CURDATE()
                        AND ib.tanggal_inbound < CURDATE() + INTERVAL 1 DAY
                        AND id_status_inbound in (1,3)
			");
		}

        if($this->db->table_exists('display_outbound')){
			$this->db->query("
				CREATE OR REPLACE VIEW display_outbound AS
                    SELECT
                        1 as id,
                        'PL-001' as picking_list,
                        '8/10' as qty,
                        'Picking' as last_status,
                        '11.00' as start_time
			");
		}

        if($this->db->table_exists('display_total_outbound')){
			$this->db->query("
				CREATE OR REPLACE VIEW display_total_outbound AS
                    SELECT
                        130 as total_out,
                        80 as process,
                        20 as picking,
                        40 as packing,
                        20 as loading
			");
		}

        if($this->db->table_exists('display_cycle')){
			$this->db->query("
				CREATE OR REPLACE VIEW display_cycle AS
                    SELECT
                        1 as id,
                        'AB' as loc_area,
                        '80/80' as qty,
                        'Done' as last_status,
                        '9.00' as start_time,
                        'Pass' as result
			");
		}
    }

    public function get_display_inbound(){
        $this->db->limit(10);
        $this->db->order_by('id','desc');
        $result = $this->db->get('display_inbound');
        return $result;
    }

    public function get_display_outbound(){
        $this->db->limit(10);
        $this->db->order_by('id','desc');
        $result = $this->db->get('display_outbound');
        return $result;
    }

    public function get_display_total_outbound(){
        $result = $this->db->get('display_total_outbound');
        return $result->row();
    }

    public function get_display_cycle(){
        $this->db->limit(10);
        $this->db->order_by('id','desc');
        $result = $this->db->get('display_cycle');
        return $result;
    }

    public function getWidget(){
		$result = array();
		$today = date('Y-m-d');

		$sql = 'SELECT
					 IFNULL(SUM(total_item), 0) AS total_item, IFNULL(SUM(total_qty), 0) AS total_qty,
					 IFNULL(SUM(total_tally), 0) AS total_tally, IFNULL(SUM(total_qty_tally), 0) AS total_qty_tally,
					 IFNULL(SUM(total_putaway), 0) AS total_putaway, IFNULL(SUM(total_qty_putaway), 0) AS total_qty_putaway,
					 finish_putaway,tanggal_receiving,
					 -- TIME_FORMAT(
						-- SEC_TO_TIME(
							AVG(
								TIME_TO_SEC(
									TIMEDIFF(
										finish_putaway,
										tanggal_receiving
									)
								)
							)
						-- ),
					-- \'%H:%i:%s\') AS average
					AS average
				FROM
					receiving rcv
				LEFT JOIN
				(
					SELECT
						table_1.id_receiving, COUNT(total_item) AS total_item, SUM(total_qty) AS total_qty
					FROM
					(
						SELECT
							rcvqty.id_receiving, rcvqty.id_barang AS total_item, SUM(qty) AS total_qty
						FROM
							receiving_qty rcvqty
						JOIN barang brg
							ON brg.id_barang=rcvqty.id_barang
						GROUP BY
							rcvqty.id_barang, id_receiving
					) AS table_1
					GROUP BY
						id_receiving
				) AS rec_qty
					ON rec_qty.id_receiving=rcv.id_receiving
				LEFT JOIN
				(
					SELECT
						table_2.id_receiving, COUNT(total_tally) AS total_tally, SUM(total_qty_tally) AS total_qty_tally
					FROM
					(
						SELECT
							rcvqty.id_receiving, COUNT(*) AS total_tally, SUM(qty) AS total_qty_tally
						FROM
							receiving_qty rcvqty
						JOIN barang brg
							ON brg.id_barang=rcvqty.id_barang
						JOIN receiving srcv
							ON srcv.id_receiving=rcvqty.id_receiving
						WHERE
							srcv.start_tally IS NULL
						AND
							srcv.start_tally IS NULL
						GROUP BY
							rcvqty.id_barang, id_receiving
					) AS table_2
					GROUP BY
						id_receiving
				) AS rec_tally
					ON rec_tally.id_receiving=rcv.id_receiving
				LEFT JOIN
				(
					SELECT
						table_3.id_receiving, COUNT(total_putaway) AS total_putaway, SUM(total_qty_putaway) AS total_qty_putaway
					FROM
					(
						SELECT
							rcvqty.id_receiving, COUNT(*) AS total_putaway, SUM(qty) AS total_qty_putaway
						FROM
							receiving_qty rcvqty
						JOIN barang brg
							ON brg.id_barang=rcvqty.id_barang
						JOIN receiving srcv
							ON srcv.id_receiving=rcvqty.id_receiving
						WHERE
							srcv.start_tally IS NOT NULL
						AND
							srcv.finish_tally IS NOT NULL
						GROUP BY
							rcvqty.id_barang, id_receiving
					) AS table_3
					GROUP BY
						id_receiving
				) AS rec_putaway
					ON rec_putaway.id_receiving=rcv.id_receiving
				WHERE
					DATE(tanggal_receiving) = \''.$today.'\'';

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

}

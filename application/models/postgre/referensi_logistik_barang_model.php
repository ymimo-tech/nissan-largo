<?php
class referensi_logistik_barang_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'logistik_barang';
    private $table2 = 'kategori';
    private $table3 = 'satuan';

    private function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', 'a.id_kategori = b.id_kategori','left');
        $this->db->join($this->table3 . ' c', 'c.id_satuan = a.id_satuan','left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_barang'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");

        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"]=null;
        }


        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"]=null;
        }

        
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.kd_barang');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;

            if ($this->access_right->otoritas('edit')) {
                $action = anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_logistik_barang/edit/' . $id))) . ' ';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('referensi_logistik_barang/delete/' . $id)));
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'tipe_barang' => $value->tipe_barang,
                    'detail_barang' => $value->detail_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'tipe_barang' => $value->tipe_barang,
                    'detail_barang' => $value->detail_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_barang' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_barang' => $id));
    }
    
    public function options($default = '--Pilih Barang Logistik--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->nama_barang ;
        }
        return $options;
    }

    
}

?>
<?php

class Api_receiving_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    function load(){
        $this->db->select("r.code as kd_receiving, itm.code as kd_barang, itm.has_batch, itm.has_expdate, itm.has_qty, itm.def_qty, ir.qty");
		$this->db->select(["CAST(SPLIT_PART(r.code,'-',2) AS INTEGER) AS sort"]);
        $this->db->from("item_receiving ir");
        $this->db->join("receivings r", "r.id=ir.receiving_id", "left");
        $this->db->join("items itm", "itm.id=ir.item_id", "left");
        $this->db->where("r.st_receiving", 0);
        $this->db->where("ir.qty > 0",NULL);
		$this->db->order_by('sort','DESC');
        return $this->db->get();
    }

    function get($receiving_code){
        $this->db->select("*");
        $this->db->from("receivings");
        $this->db->where("code", $receiving_code);
        return $this->db->get();
    }

    function start_receiving($receiving_code){
        $this->db->set("start_tally", "COALESCE(start_tally, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("code", $receiving_code);
        $this->db->update("receivings");
    }

    function retrieve($receiving_code){

        $this->db->select("b.code as kd_barang, b.st_batch, b.has_expdate, a.qty");
        $this->db->from("item_receiving a");
        $this->db->join("items b", "b.id = a.item_id", "left");
        $this->db->join("receivings c", "c.id = a.receiving_id", "left");
        $this->db->where("c.code", $receiving_code);
        return $this->db->get();

    }

    function post($data){

        $this->db->trans_start();

        // $this->db
            // ->select('ir.id as item_receiving_id, r.is_cross_doc')
            // ->from('receivings r')
            // ->join('item_receiving ir','ir.receiving_id=r.id','left')
            // ->join('items itm','itm.id=ir.item_id','left')
            // ->where('r.code', $data['kd_receiving'])
            // ->where('itm.code', $data['kd_barang']);

		$this->db
            ->select(['ir.id as item_receiving_id', 'r.is_cross_doc', 'ir.qty', 'sum(first_qty) as total_rcv'])
            ->from('receivings r')
            ->join('item_receiving ir','ir.receiving_id=r.id','left')
            ->join('items itm','itm.id=ir.item_id','left')
            ->where('r.code', $data['kd_receiving'])
            ->where('itm.code', $data['kd_barang'])
            ->where('ir.qty > 0', null)
			->group_by('ir.id, r.is_cross_doc having (sum(first_qty) is null or sum(first_qty) < ir.qty')
			->limit(1);

        $receiving = $this->db->get()->row_array();

        $location = ($receiving['is_cross_doc']) ? 107 : 100;

        $id_barang        = $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();
        $id_qc          = $this->get_id("id as id_qc", "code", $data["kd_qc"], "qc")->row_array();
        $id_user        = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();

        $data['tgl_exp'] = ($data['tgl_exp'] === "0000-00-00") ? NULL : $data['tgl_exp'];

        $this->db->set("parent_code", $data['kd_parent']);
        $this->db->set("item_receiving_id", $receiving['item_receiving_id']);
        $this->db->set("item_id", $id_barang['id_barang']);
        $this->db->set("kd_batch", $data["kd_batch"]);
        $this->db->set("location_id", $location);
        // $this->db->set("production_date", $data["production_date"]);
        $this->db->set("tgl_exp", $data["tgl_exp"]);
        $this->db->set("tgl_in", $data["tgl_in"]);
        $this->db->set("st_receive", 1);
        $this->db->set("user_id_receiving", $id_user["user_id"]);
        $this->db->set("first_qty", $data["qty"]);
        $this->db->set("last_qty", $data["qty"]);
        $this->db->set("qc_id", $id_qc["id_qc"]);
        $this->db->where("unique_code", $data['kd_unik']);
        $this->db->update("item_receiving_details");

        $data_insert    =   array(
                                "loc_id_new"    => $location,
                                "loc_id_old"    => $location,
                                "pick_time"     => $data["tgl_in"],
                                "put_time"      => $data["tgl_in"],
                                "user_id_pick"  => $id_user["user_id"],
                                "user_id_put"   => $id_user["user_id"],
                                "process_name"  => "RECEIVING"
                            );

        if(!empty($data['parent_code'])){

            $data_insert    =   array(
                                    "loc_id_new"    => $location,
                                    "loc_id_old"    => $location,
                                    "pick_time"     => $data["tgl_in"],
                                    "put_time"      => $data["tgl_in"],
                                    "unique_code"   => $data["unique_code"]." - ".$data['parent_code'],
                                    "user_id_pick"  => $id_user["user_id"],
                                    "user_id_put"   => $id_user["user_id"],
                                    "process_name"  => "RECEIVING - License Plating"
                                );

        }

        $this->db->insert("transfers", $data_insert);

        $this->db->trans_complete();

        return $this->db->trans_status();

    }

    function update($data){

        $serialize = $this->get_serial($data)->row_array();

        $this->db
            ->select('ir.id as item_receiving_id, r.is_cross_doc')
            ->from('receivings r')
            ->join('item_receiving ir','ir.receiving_id=r.id','left')
            ->join('items itm','itm.id=ir.item_id','left')
            ->where('r.code', $data['kd_receiving'])
            ->where('itm.code', $data['kd_barang']);

        $receiving = $this->db->get()->row_array();

        $stagingLoc = ($receiving['is_cross_doc']) ? 107 : 100;

        $id_barang      = $this->get_id("id as id_barang, has_qty", "code", $data["kd_barang"], "items")->row_array();
        $id_qc          = $this->get_id("id as id_qc", "code", $data["kd_qc"], "qc")->row_array();
        $id_user        = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();

        $data['tgl_exp'] = ($data['tgl_exp'] === "0000-00-00") ? NULL : $data['tgl_exp'];

        $data['qty'] = ($id_barang['has_qty']) ? $data['qty'] : 1;

        $this->db->set("parent_code", $data['kd_parent']);
        $this->db->set("item_receiving_id", $receiving['item_receiving_id']);
        $this->db->set("item_id", $id_barang['id_barang']);
        $this->db->set("kd_batch", $data["kd_batch"]);
        $this->db->set("location_id", $stagingLoc);
        $this->db->set("tgl_exp", $data["tgl_exp"]);
        $this->db->set("tgl_in", $data["tgl_in"]);
        $this->db->set("st_receive", 1);
        $this->db->set("user_id_receiving", $id_user["user_id"]);
        $this->db->set("first_qty", $data["qty"]);
        $this->db->set("last_qty", $data["qty"]);
        $this->db->set("qc_id", $id_qc["id_qc"]);
        $this->db->where('id',$serialize['id']);
        $this->db->where("unique_code", $data['kd_unik']);
        $this->db->update("item_receiving_details");

        $data_insert    =   array(
                                "loc_id_new"    => $stagingLoc,
                                "loc_id_old"    => $stagingLoc,
                                "pick_time"     => $data["tgl_in"],
                                "put_time"      => $data["tgl_in"],
                                "user_id_pick"  => $id_user["user_id"],
                                "user_id_put"   => $id_user["user_id"],
                                "process_name"  => "RECEIVING"
                            );

        $this->db->insert("transfers", $data_insert);
    }

    function insert($data){

        $this->db
            ->select('ir.id as item_receiving_id, r.is_cross_doc, itm.shipment_id, itm.has_expdate, r.id as id_receiving')
            ->from('receivings r')
            ->join('item_receiving ir','ir.receiving_id=r.id','left')
			->join('inbound inb','inb.id_inbound = ir.inbound_id','LEFT')
            ->join('items itm','itm.id=ir.item_id','left')
            ->where('r.code', $data['kd_receiving'])
            ->where('inb.code', $data['kd_inbound'])
            ->where('itm.code', $data['kd_barang']);

        $receiving = $this->db->get()->row_array();
        // dd($receiving);

        if( ($receiving['shipment_id'] == 2 || $receiving['has_expdate'] == 1) && (empty($data['tgl_exp']) || $data['tgl_exp'] == "0000-00-00") ){

            return array(
                "status" => false,
                "message" => "Expired Date tidak boleh kosong"
            );

        }else{

            $stagingLoc = ($receiving['is_cross_doc']) ? 107 : 100;

            $id_barang      = $this->get_id("id as id_barang, has_qty", "code", $data["kd_barang"], "items")->row_array();
            $id_qc          = $this->get_id("id as id_qc", "code", $data["kd_qc"], "qc")->row_array();
            $id_user        = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();

            $data['tgl_exp'] = ($data['tgl_exp'] === "0000-00-00" || empty($data['tgl_exp'])) ? NULL : $data['tgl_exp'];

            $rfidTag = "";
            $uniqueCode = "";

            // $kdUnikLen = strlen($data['kd_unik']);

    /*        if (ctype_xdigit($data['kd_unik'])) {

                $limit = 15;
                if($data['kd_unik'] > 34){
                    $limit=20;
                }

                $uniqueCode = $data['kd_unik'];
                $rfidTag    = substr(hex2bin($data['kd_unik']),0,$limit);
            }else{
                $uniqueCode = $data['kd_unik'];
                $rfidTag    = $data['kd_unik'];
            }
    */
            $uniqueCode = $data['kd_unik'];
            $rfidTag    = $data['kd_unik'];

            $data['qty'] = ($id_barang['has_qty']) ? $data['qty'] : 1;

            $item_routing = $this->db->select('routing_id')->from('items_routing')->where('item_id', $id_barang['id_barang'])->get();
            $insert_item_routing = array();
            if($item_routing->num_rows() > 0){
              $insert_item_routing =  [ "item_id" => $id_barang['id_barang'],
                                        "unique_code" => $uniqueCode,
                                        "receiving_id" => $receiving['id_receiving']
              ];


              foreach ($item_routing->result_array() as $key => $value) {
                  if($value['routing_id'] == 1){
                      $insert_item_routing['status_routing_quality'] = 0;
                    }
                  if($value['routing_id'] == 2){
                      $insert_item_routing['status_routing_label'] = 0;
                    }
                  if($value['routing_id'] == 3){
                      $insert_item_routing['status_routing_repacking'] = 0;
                    }
                }

                $this->db->insert("items_receiving_routing", $insert_item_routing);

            }

            $data_insert    = array(
                                "parent_code" => $data["kd_parent"],
                                "item_id"       => $id_barang['id_barang'],
                                "item_receiving_id" => $receiving['item_receiving_id'],
                                "kd_batch" => $data["kd_batch"],
                                "location_id" => $stagingLoc,
                                "tgl_exp" => $data["tgl_exp"],
                                "tgl_in" => $data["tgl_in"],
                                "st_receive" => 1,
                                "user_id_receiving" => $id_user["user_id"],
                                "first_qty" => $data["qty"],
                                "last_qty"    => $data["qty"],
                                "qc_id"       => $id_qc["id_qc"],
                                "unique_code" => $uniqueCode,
                                "rfid_tag"    => $rfidTag
                            );

            $this->db->insert("item_receiving_details", $data_insert);

            $data_masukan   = array(
                                "unique_code"   => $uniqueCode,
                                "loc_id_new"    => $stagingLoc,
                                "loc_id_old"    => $stagingLoc,
                                "pick_time"     => $data["tgl_in"],
                                "put_time"      => $data["tgl_in"],
                                "user_id_pick"  => $id_user["user_id"],
                                "user_id_put"   => $id_user["user_id"],
                                "process_name"  => "RECEIVING"
                            );

            $this->db->insert("transfers", $data_masukan);

            return array(
                "status" => true
            );

        }
    }

    function get_item($receiving_code, $item_code){
        $id_receiving   = $this->get_id("id as id_receiving", "code", $receiving_code, "receivings")->row_array();
        $id_barang      = $this->get_id("id as id_barang", "code", $item_code, "items")->row_array();
        $this->db->select("st_receive");
        $this->db->from("receiving_qty");
        $this->db->where("kd_receiving", $id_receiving["id_receiving"]);
        $this->db->where("kd_barang", $id_barang["id_barang"]);
        return $this->db->get();
    }

    function lock_item($receiving_code, $item_code){
        $id_receiving   = $this->get_id("id_receiving", "kd_receiving", $receiving_code, "receiving")->row_array();
        $id_barang      = $this->get_id("id_barang", "kd_barang", $item_code, "barang")->row_array();
        $this->db->set("st_receive", 1);
        $this->db->where("id_receiving", $id_receiving["id_receiving"]);
        $this->db->where("id_barang", $id_barang["id_barang"]);
        $this->db->update("receiving_qty");
    }

    function get_serial($data){

        $this->db
            ->select("ird.*, itm.code as item_code")
            ->from("item_receiving_details ird")
            ->join('item_receiving ir','ir.id=ird.item_receiving_id')
            ->join('receivings rcv','rcv.id=ir.receiving_id')
            ->join('items itm','itm.id=ird.item_id')
            ->where('rcv.code',$data['kd_receiving'])
            ->where("unique_code", $data['kd_unik']);

        return $this->db->get();

    }

    function get_serial_lp($receiving_code, $serial_number){

        $this->db
            ->select("ird.item_id as id_barang")
            ->from("item_receiving_details ird")
            ->join('item_receiving ir','ir.id=ird.item_receiving_id')
            ->join('receivings rcv','rcv.id=ir.receiving_id')
            ->where('rcv.code', $receiving_code)
            ->where("unique_code", $serial_number);

        return $this->db->get();
    }

    function get_qc(){
        $this->db->select("code as kd_qc");
        $this->db->from("qc");
        return $this->db->get();
    }

    function validQty($data=array()){

        // $this->db
            // ->select(['SUM(ir.qty) as doc_qty', "COALESCE(sum(first_qty),0) as act_qty"])
            // ->from('receivings r')
            // ->join('item_receiving ir','ir.receiving_id=r.id')
            // ->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left')
            // ->join('items itm','itm.id=ir.item_id','left')
            // ->where('r.code',$data['kd_receiving'])
            // ->where('itm.code',$data['kd_barang'])
            // ->group_by('itm.id, ir.qty');

		$this->db
            ->select(['(SELECT COALESCE(SUM(qty),0) FROM item_receiving where receiving_id = r.id and inbound_id = inb.id_inbound and item_id = itm.id) as doc_qty', "COALESCE(sum(first_qty),0) as act_qty"])
            ->from('receivings r')
            ->join('item_receiving ir','ir.receiving_id=r.id')
            ->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left')
            ->join('items itm','itm.id=ir.item_id')
            ->join('inbound inb','inb.id_inbound = ir.inbound_id','LEFT')
            ->where('r.code',$data['kd_receiving'])
            ->where('inb.code',$data['kd_inbound'])
            ->where('itm.code',$data['kd_barang'])
            ->group_by('r.id, itm.id, inb.id_inbound');

        $row = $this->db->get()->row_array();

        if($row['doc_qty'] >= ($row['act_qty'] + $data['qty'])){
            return true;
        }

        return false;

    }

    /*
        Title  : NEW API
        Author : Hady Pratama
    */

    public function get_document(){

        $this->db->select(array("r.code as kd_receiving","i.code as inbound_code","CONCAT(count(*), ' Items') as qty","COALESCE(s.source_name,'-') as source","COALESCE(SUM(ir.qty),0) as qty_item"));
        $this->db->from('receivings r');
        $this->db->join('item_receiving ir','ir.receiving_id=r.id');
        $this->db->join('inbound i','i.id_inbound=ir.inbound_id','left');
        $this->db->join('sources s','s.source_id=i.supplier_id','left');
        $this->db->where("r.st_receiving", 0);
        $this->db->group_by('r.id, i.code, r.code, s.source_name');
        $this->db->order_by('r.id','desc');

        $rec  = $this->db->get();
        return $rec->result_array();

    }

	public function get_inbound_document_by_rcv($receivingCode=''){
      $receivingCode = str_replace('%2B', '/', $receivingCode);

        $this->db->select(array("inb.id_inbound as id_inbound", "inb.code as inbound_code"));
        $this->db->from('hp_receiving_inbound hri');
		$this->db->join('receivings r','r.id = hri.receiving_id');
		$this->db->join('inbound inb','inb.id_inbound = hri.inbound_id');
        $this->db->where("r.code", $receivingCode);

        $rec  = $this->db->get();
        return $rec->result_array();

    }

    public function get_document_item($receivingCode, $inboundCode, $itemCode){
      $receivingCode = str_replace('%2B', '/', $receivingCode);
      $inboundCode = str_replace('%2B', '/', $inboundCode);


		// $this->db->select(array('r.code as receiving_code','itm.code as item_code','itm.name as item_name','COALESCE(sum(ir.qty),0) as qty','unt.name as nama_satuan',"COALESCE(s.source_name,'-') as source",'r.st_receiving as status',"COALESCE(NULL,'') as image","COALESCE(def_qty,'1') as default_qty","COALESCE((SELECT SUM(first_qty) FROM item_receiving_details where item_receiving_id=ir.id ),0) as received_qty","itm.has_expdate","itm.has_qty","itm.has_batch"));
		// $this->db->from('receivings r');
		// $this->db->join('item_receiving ir','ir.receiving_id=r.id');
		// $this->db->join('inbound i','i.id_inbound=ir.inbound_id','left');
		// $this->db->join('sources s','s.source_id=i.supplier_id','left');
		// $this->db->join('items itm','itm.id=ir.item_id','left');
		// $this->db->join('units unt','unt.id=itm.unit_id','left');
		// $this->db->where('r.code',$receivingCode);
		// $this->db->where('ir.qty != 0',null);
		// $this->db->group_by('itm.id, r.code, unt.name, s.source_name, r.st_receiving, ir.id, itm.code, itm.name, itm.def_qty, itm.has_expdate, itm.has_qty, itm.has_batch');
		// $this->db->order_by('itm.code','asc');
		// $this->db->order_by('received_qty','asc');

		$this->db->select(array('r.code as receiving_code',
		// "CONCAT(itm.code,' => ',COALESCE(itm2.code,'-')) as item_code",
		"itm.code as item_code",
		"COALESCE(itm2.code,'-') as item_code_subs",
		"CONCAT(itm.name,' => ',COALESCE(itm2.name,'-')) as item_name", "COALESCE(itm2.code,'-') as parent_code","COALESCE(itm2.name,'-') as parent_name", "(SELECT sum(qty) FROM item_receiving WHERE receiving_id = r.id and inbound_id = i.id_inbound and item_id = itm.id) as qty",'unt.name as nama_satuan',"COALESCE(s.source_name,'-') as source",'r.st_receiving as status',"COALESCE(NULL,'') as image","COALESCE(itm.def_qty,'1') as default_qty","COALESCE((SELECT SUM(first_qty) FROM item_receiving_details where item_receiving_id=ir.id ),0) as received_qty","itm.has_expdate","itm.has_qty","itm.has_batch"));
        $this->db->from('receivings r');
        $this->db->join('item_receiving ir','ir.receiving_id=r.id');
        $this->db->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left');
        $this->db->join('inbound i','i.id_inbound=ir.inbound_id','left');
        $this->db->join('inbound_item inbitm','inbitm.inbound_id = i.id_inbound','left');
        $this->db->join('item_substitute is3', 'is3.item_id = inbitm.item_id and is3.item_id_subs = ir.item_id','left');
        $this->db->join('items itm2', 'itm2.id = is3.id','left');
        $this->db->join('sources s','s.source_id=i.supplier_id','left');
        $this->db->join('items itm','itm.id=ir.item_id','left');
        $this->db->join('units unt','unt.id=itm.unit_id','left');
        $this->db->where('r.code',$receivingCode);
        $this->db->where('i.code',$inboundCode);
        $this->db->where('ir.qty != 0',null);
        $this->db->group_by('"itm"."id",ir.id, r.id, r.code, itm.code, itm.name, itm2.code, itm2.name, unt.name, "s"."source_name", "r"."st_receiving", "itm"."def_qty", "itm"."has_expdate", "itm"."has_qty", "itm"."has_batch", i.id_inbound');
        $this->db->order_by('itm.code','asc');
        $this->db->order_by('received_qty','asc');

        if(!empty($itemCode)){
            $this->db->where('itm.code', $itemCode);
        }

		//dd($this->db->get_compiled_select());
		$sql = "select r.code as receiving_code, itm1.code as item_code, itm2.code as item_code_subs,
				CONCAT(itm1.name,' => ',COALESCE(itm2.name,'-')) as item_name, COALESCE(itm2.code,'-') as parent_code,
				COALESCE(itm2.name,'-') as parent_name, 
				(SELECT sum(qty) FROM item_receiving WHERE receiving_id = r.id and inbound_id = i.id_inbound and item_id = itm1.id) as qty,
				unt.name as nama_satuan, COALESCE(s.source_name,'-') as source, r.st_receiving as status, COALESCE(NULL,'') as image, COALESCE(itm1.def_qty,'1') as default_qty, COALESCE((SELECT SUM(first_qty) FROM item_receiving_details where item_receiving_id=ir.id ),0) as received_qty, itm1.has_expdate, itm1.has_qty, itm1.has_batch
				from item_receiving ir
				left join receivings r on r.id = ir.receiving_id 
				left join item_receiving_details ird on ird.item_receiving_id = ir.id
				left join items itm1 on itm1.id = ir.item_id
				left join item_substitute is2 on is2.id = ir.substitute_id 
				left join items itm2 on itm2.id = is2.item_id_subs 
				LEFT JOIN inbound i ON i.id_inbound=ir.inbound_id
				LEFT JOIN units unt ON unt.id=itm1.unit_id
				LEFT JOIN sources s ON s.source_id=i.supplier_id
				WHERE r.code =  '".$receivingCode."'";
        //$rec  = $this->db->get()->result_array();
		
		$rec = $this->db->query($sql)->result_array();

        return $rec;

    }

    public function putaway_serial($serial_number){

        $this->db
            ->select(array('itm.code as item_code',"COALESCE(NULL,'') as image","(CASE WHEN r.start_tally = NULL AND r.finish_tally = NULL THEN FALSE ELSE TRUE END) as status"))
            ->from('item_receiving_details ird')
            ->join('item_receiving ir','ir.id=ird.item_receiving_id')
            ->join('receivings r','r.id=ir.receiving_id')
            ->join('items itm','itm.id=ird.item_id')
            ->join('units unt','unt.id=itm.unit_id','left')
            ->where('ird.parent_code',$serial_number)
            ->or_where('ird.unique_code',$serial_number);

        $res = $this->db->get();

        return $res->row_array();

    }

    public function getDocType($receivingCode){

        $this->db
            ->select('inb.document_id')
            ->from('receivings rcv')
            ->join('inbound inb','inb.id_inbound=rcv.po_id')
            ->where('rcv.code',$receivingCode);

        $data = $this->db->get();

        return $data->row_array();

    }

    public function getOuterLabelItem($params=array()){

        // if(!empty($params['receiving_code'])){
        //     $this->db->where('r.code',$params['receiving_code']);
        // }

        $this->db
            ->select('DISTINCT(ird.unique_code) as unique_code')
            ->from('item_receiving_details ird')
            // ->join('picking_recomendation pr','pr.unique_code=ird.unique_code and pr.picking_id=ird.picking_id and pr.item_id=ird.item_id')
            // ->join('pickings p','p.pl_id=pr.picking_id')
            // ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
            // ->join('outbound outb','outb.id=plo.id_outbound')
            // ->join('inbound inb','inb.code=outb.code')
            // ->join('receivings r','r.po_id=inb.id_inbound')
            ->where('ird.parent_code',$params['parent_code']);

        $data = $this->db->get()->result_array();

        return $data;
    }

    function wtValidation($params){

        $this->db
            ->select('inb.*')
            ->from('receivings r')
            ->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id')
            ->join('inbound inb','inb.id_inbound=hpi.inbound_id')
            ->where('r.code',$params['kd_receiving']);

        $docs = $this->db->get()->result_array();

        if($docs){

            foreach ($docs as $doc) {

                if($doc['document_id'] == 3 || $doc['document_id'] == 14){

                    $outbDoc = $this->db->get_where('outbound',['code'=>$doc['code']])->row_array();

                    if($outbDoc){

                        $this->db
                            ->select(['unique_code',"COALESCE(SUM(pr.qty),0) as qty"])
                            ->from('picking_list_outbound plo')
                            ->join('pickings p','p.pl_id=plo.pl_id')
                            ->join('picking_recomendation pr','pr.picking_id=p.pl_id')
                            ->where('plo.id_outbound',$outbDoc['id'])
                            ->where('pr.unique_code',$params['kd_unik'])
                            ->group_by('pr.unique_code');

                        $snAvailable = $this->db->get()->row_array();

                        if($snAvailable){
                            return true;
                        }else{
                            return false;
                        }

                    }else{

                        return false;
                    }

                }

            }

            return true;

        }

        return false;
    }

    function validSN($serialNumber='',$receivingCode=''){

        $this->db
            ->from('item_receiving_details ird')
            ->join('item_receiving ir','ir.id=ird.item_receiving_id')
            ->join('receivings r','r.id=ir.receiving_id')
            ->where('r.code <>',$receivingCode)
            ->where('unique_code',$serialNumber)
            ->where('(picking_id IS NULL or last_qty > 0)');

        $data  = $this->db->get()->result_array();

        return $data;

    }

    function getValidOuterLabel($receivingCode,$outerLabel,$itemCode){

        $this->db
            ->from('item_receiving_details ird')
            ->join('item_receiving ir','ir.id=ird.item_receiving_id')
            ->join('items itm','itm.id=ir.item_id')
            ->join('receivings r','r.id=ir.receiving_id')
            ->where('ird.parent_code',$outerLabel)
            ->where('itm.code <>',$itemCode)
            ->or_where('ird.parent_code',$outerLabel)
            ->where('r.code <>',$receivingCode);

        $outerLabel = $this->db->get()->row_array();

        if($outerLabel){
            return false;
        }

        return true;

    }
}

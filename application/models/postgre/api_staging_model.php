<?php

class Api_staging_model extends CI_Model {

    private $url = "https://gojekstg.apimanagement.ap1.hana.ondemand.com/whapi2/";
    private $urlParam = "/?sap-client=400";
    private $sapSession = "SAP_SESSIONID_GQ1_400";

    function __construct() {
        parent::__construct();
    }

    function _get_token(){

        $curl_handle = curl_init ();

        curl_setopt ( $curl_handle, CURLOPT_URL, $this->url.'stock_mvt'.$this->urlParam );
        curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt ( $curl_handle, CURLOPT_USERPWD, 'WH_API_TEST:Gojek123!' );
        curl_setopt ( $curl_handle, CURLOPT_HTTPHEADER, array('X-CSRF-Token:Fetch'));
        curl_setopt ( $curl_handle, CURLOPT_VERBOSE, 1);
        curl_setopt ( $curl_handle, CURLOPT_HEADER, 1);
        $response = curl_exec ( $curl_handle );

        $header_size = curl_getinfo($curl_handle, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);        
        curl_close ( $curl_handle );

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response, $matches);
        $cookies = array();
        foreach($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }

        $headers = array();

        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $header_text) as $i => $line)
            if ($i === 0)
                $headers['http_code'] = $line;
            else
            {
                list ($key, $value) = explode(': ', $line);

                $headers[$key] = $value;
            }

        return array(
            'CSRF' => $headers['x-csrf-token'],
            'SESSION' => $this->sapSession.'='.$cookies[$this->sapSession]
        );

    }

    function _curl_process_post($data='',$url=''){

        $token = $this->_get_token();
        $header = array("X-CSRF-Token:".$token['CSRF'] , "Accept:application/json", 'Content-type: application/json', 'Cookie:'.$token['SESSION']);

        $curl_handle = curl_init();

        curl_setopt ( $curl_handle, CURLOPT_URL, $url );
        curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $curl_handle, CURLOPT_POST, 1 );
        curl_setopt ( $curl_handle, CURLOPT_COOKIESESSION, true);
        curl_setopt ( $curl_handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt ( $curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt ( $curl_handle, CURLOPT_USERPWD, 'WH_API_TEST:Gojek123!' );
        curl_setopt ( $curl_handle, CURLOPT_HTTPHEADER, $header);

        $buffer = curl_exec ( $curl_handle );
        curl_close ( $curl_handle );

        $apiLog = array(
            'method'    => 'POST',
            'url'       => $url,
            'token'     => $token['CSRF'],
            'content'   => $buffer,
            'body'      => $data,
            'header'    => json_encode($header)
        );

        $this->apiLog($apiLog);

        $data = json_decode($buffer);
        return $data;

    }

    function _curl_process_get($api='',$data=''){

        $staging_ip=$this->url.$api.$this->urlParam.'&$format=json';
        $token = $this->_get_token();
        $header = array("X-CSRF-Token:".$token['CSRF'] , "Accept:application/json", 'Content-type: application/json', 'Cookie:'.$token['SESSION']);

        $curl_handle = curl_init();

        curl_setopt ( $curl_handle, CURLOPT_URL, $staging_ip );
        curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $curl_handle, CURLOPT_COOKIESESSION, true);
        curl_setopt ( $curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt ( $curl_handle, CURLOPT_USERPWD, 'WH_API_TEST:Gojek123!' );
        curl_setopt ( $curl_handle, CURLOPT_HTTPHEADER, $header);

        $buffer = curl_exec ( $curl_handle );
        curl_close ( $curl_handle );

        $apiLog = array(
            'method'    => 'GET',
            'url'       => $staging_ip,
            'token'     => $token['CSRF'],
            'content'   => $buffer,
            'header'    => json_encode($header)
        );

        $this->apiLog($apiLog);

        $data = json_decode($buffer);
        return $data;

    }

    function _curl_process($type='',$table='',$data=array()){
        $curl_handle = curl_init ();

        //supported format ['json','csv','jsonp','serialized','xml']
        //change url suffix ex : .json to .xml for change response format
        $clientid='test';
        $local_ip='::1';
        $staging_ip='localhost';
        $data2 = array (
                'table' => $table,
                'clientid' => $clientid,
                'hash' => sha1 ( $table . $clientid . $local_ip ),
                'xkey' => '0b5581b7ec815ad66a916b4fc5716d2ec84cd45e',
        );

        $data = array_merge($data,$data2);

        curl_setopt ( $curl_handle, CURLOPT_URL, 'http://'.$staging_ip.'/salubritas-staging/api/'.$type.'.json' );
        curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $curl_handle, CURLOPT_POST, 1 );
        curl_setopt ( $curl_handle, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
        curl_setopt ( $curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST );
        curl_setopt ( $curl_handle, CURLOPT_USERPWD, 'test:123456' );

        $buffer = curl_exec ( $curl_handle );
        curl_close ( $curl_handle );

        $data = json_decode($buffer);
        return $data;
    }

    function apiLog($data=array()){

        if(!empty($data)){

            if(!empty($data['body'])){
                $this->db->set('body',$data['body']);
            }

            $this->db
                ->set('header',$data['header'])
                ->set('method',$data['method'])
                ->set('url', $data['url'])
                ->set('token', $data['token'])
                ->set('response', $data['content'])
                ->set('user_id', $this->session->userdata('user_id'))
                ->insert('api_log_integration');

        }

    }

    function sendToStaging($data='',$api='',$ref=array()){
        $urlApi = $this->url.$api.$this->urlParam;

        $this->db
            ->from('staging')
            ->where('refId',$ref['refId'])
            ->where('refCode',$ref['refCode']);

        $staging = $this->db->get()->row_array();

        if($staging){

            $this->db
                ->set('url', $urlApi)
                ->set('body',$data)
                ->set('refId',$ref['refId'])
                ->set('refCode',$ref['refCode'])
                ->where('id',$staging['id'])
                ->update('staging');

            $id = $staging['id'];

        }else{

            $this->db
                ->set('url', $urlApi)
                ->set('body',$data)
                ->set('status',0)
                ->set('refId',$ref['refId'])
                ->set('refCode',$ref['refCode'])
                ->insert('staging');

            $id = $this->db->insert_id();

        }

        return $id;

    }

    function sync($id=''){

        if(!empty($id)){
            $this->db->where('id',$id);
        }

        $this->db->where('status',0);
        $data = $this->db->get('staging')->result_array();

        if($data){

            foreach ($data as $d) {

                $process = $this->_curl_process_post($d['body'],$d['url']);
                if($process){

                    if(isset($process->d)){

                        $req = $process->d;

                        if(empty($req->Message) ){

                            if(isset($Mblnr)){
                                $this->db->set('mblnr',$req->Mblnr);
                            }

                            if(isset($req->Mjahr)){
                                $this->db->set('mjahr',$req->Mjahr);
                            }

                            if(isset($req->PoNumber)){
                                $this->db->set('po_number',$req->PoNumber);
                            }

                            $this->db
                                ->set('sync_date',date('Y-m-d h:i:s'))
                                ->set('status',1)
                                // ->set('sync_code',$req->Mblnr)
                                ->where('id',$d['id'])
                                ->update('staging');

                        }
                    }

                }

            }

        }

    }

}

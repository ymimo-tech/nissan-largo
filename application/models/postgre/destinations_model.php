<?php
class destinations_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'destinations';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->join('source_categories sc','sc.source_category_id=a.source_category_id');
        $this->db->join('gate gt','gt.id=a.gate', 'left'); 
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_gates(){

      $data = $this->db->select('id, gate_code, gate_province')->from('gate')->get();

      $result = $data->result_array();

      return $result;
    }

    public function get_by_id($id) {
        $this->db->where('a.destination_id',$id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id'])){
            $condition['destination_id'] = $condition['id'];
            unset($condition['id']);
        }
        $this->data($condition);

        return $this->db->get();

    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('destination_id' => $id));
    }

    public function delete($id) {

		$result = array();

        $t1 = $this->db->get_where('outbound',['destination_id'=>$id])->row_array();

		if($t1){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('destination_id' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}

        return $result;

    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);


        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->from('source_categories')
                ->where('lower(source_category_code)',strtolower($data[$i]['destination_type']))
                ->or_where('lower(source_category_name)',strtolower($data[$i]['destination_type']));

            $getSourceType = $this->db->get()->row_array();

            $gate = $this->db->select('id')->from('gate')->where('lower(gate_code)', strtolower($data[$i]['gate']))->get()->row_array()['id'];

            if($getSourceType){

                $insert= array(
                    'destination_code'          => $data[$i]['destination_code'],
                    'destination_name'          => $data[$i]['destination_name'],
                    'destination_address'       => $data[$i]['destination_address'],
                    'destination_phone'         => $data[$i]['destination_phone'],
                    'destination_contact_person' => $data[$i]['destination_contact_person'],
                    'destination_email'          => $data[$i]['destination_email'],
                    'destination_city'          => $data[$i]['destination_city'],
                    'source_category_id'          => $getSourceType['source_category_id'],
                    'gate'        => $gate
                );

                $this->db
                    ->from($this->table)
                    ->where('LOWER(destination_code)', strtolower($data[$i]['destination_code']))
                    ->or_where('lower(destination_name)', strtolower($data[$i]['destination_name']));

                $check = $this->db->get()->row_array();

                if($check){

                    $id = $check['destination_id'];
                    $this->db->where('destination_id',$id);
                    $this->db->update($this->table,$insert);

                }else{

                    $this->db->insert($this->table,$insert);
                    $id = $this->db->insert_id();

                }

            }else{

                return array(
                    'status'    => false,
                    'message'   => "Destination Type \"".$data[$i]['destination_type']."\" doesn't exist"
                );

            }

        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );
    }

}

?>

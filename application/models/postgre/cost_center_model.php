<?php
class cost_center_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'cost_center';

    public function data($condition = array()) {

        $this->db->from($this->table);
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id' => $id));
    }
    
    public function delete($id) {

        $result = array();

        $this->data();
        $this->db->where('id',$id);

        $row = $this->db->get()->num_rows();
        if($row < 1){
            $result['status'] = 'ERR';
            $result['message'] = 'Cannot delete master cost center';
        }else{

            $this->db->delete($this->table, array('id' => $id));

            $result['status'] = 'OK';
            $result['message'] = 'Delete data success';

        }

        return $result;

    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        $inserts = array();
        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->select('count(*) as total')
                ->from($this->table)
                ->or_where('lower(code)', strtolower($data[$i]['code']));

            $check = $this->db->get()->row()->total;

            if($check > 0){
                continue;
            }else{

                $inserts[] = array(
                    'code'  => $data[$i]['code'],
                    'name'  => $data[$i]['name']
                );

            }

        }

        $this->db->insert_batch($this->table,$inserts);

        $this->db->trans_complete();

        return $this->db->trans_status();

    }

    public function options($default_key='',$default_value='') {

        $this->db
            ->select('cost.id,cost.code')
            ->from('cost_center cost');

        $data = $this->db->get();
        $options = array();

        if(!empty($default_key)){
            $options[$default_value] = $default_key;
        }

        foreach ($data->result() as $row) {
            $options[$row->id] = $row->code ;
        }
        return $options;
    }

}

?>

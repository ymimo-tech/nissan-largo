<?php
class referensi_lokasi_area_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'location_areas';

    public function data($condition = array()) {

        $this->db->select('a.id as loc_area_id, a.name as loc_area_name, a.description as loc_area_desc, a.warehouse_id');
        $this->db->from($this->table  . ' a');
        $this->db->where('a.name IS NOT NULL', NULL);
        $this->db->where_condition($condition);
        $this->db->order_by('id');

        return $this->db;
    }

    public function getLocationById($post = array()){
        $result = array();

        $this->data();
        $this->db->where_in('id', implode(",", $post['loc_area_id']));

        $result = $this->db->get()->result_array();

        return $result;
    }

    public function getLocation(){
        $result = array();

        $this->data();
        $this->db->where('name IS NOT NULL', NULL);

        $result = $this->db->get()->result_array();

        return $result;
    }

    public function getQtyLocation(){
        $result = array();

        $this->db->select('count(*) as total');

        $row = $this->db->get($this->table)->row_array();
        $result['qty'] = $row['total'];

        return $result;
    }

    public function get_by_id($id) {
        $this->db->where('a.id', $id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(array_key_exists("loc_area_id",$condition)){
            $condition['id'] = $condition['loc_area_id'];
            unset($condition['loc_area_id']);
        }

        $this->data($condition);
        return $this->db->get();
    }

    private function getValue($data=[]){
        $data = [
            "name" => $data['loc_area_name'],
            "description" => $data['loc_area_desc'],
            'warehouse_id' => $data['warehouse_id']
        ];

        return $data;
    }

    public function create($data) {
        $data = $this->getValue($data);
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        $data = $this->getValue($data);
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {

        $result = array();

        $in = explode(',', $this->config->item('hardcode_location_id'));

        if(in_array($id, $in)){
            $result['status'] = 'ERR';
            $result['message'] = 'Cannot delete master location';
        }else{

            // $sql = 'SELECT
            //          COUNT(*) AS total
            //      FROM
            //          receiving_barang
            //      WHERE
            //          loc_category_id=\''.$id.'\'';
            //
            // $row = $this->db->query($sql)->row_array();
            //
            // if($row['total'] > 0){
            //  $result['status'] = 'ERR';
            //  $result['message'] = 'Cannot delete location, because data have relation to another table';
            // }else{
                $this->db->delete($this->table, array('id' => $id));

                $result['status'] = 'OK';
                $result['message'] = 'Delete data success';
            // }
        }

        return $result;

    }

    public function options($default = '--Pilih Nama Lokasi Area--', $key = '') {
        $data = $this->data()->get();
        $options = array();
    
        if (!empty($default))
            $options[$key] = $default;
    
        foreach ($data->result() as $row) {
            $options[$row->loc_area_id] = $row->loc_area_name ;
        }
        return $options;
    }

    public function importFromExcel($data){

        $this->db->trans_complete();

        $dataLen = count($data);

        $inserts = array();

        for ($i=0; $i < $dataLen ; $i++) {

            $warehouse = $this->db->get_where('warehouses',['lower(name)'=>strtolower($data[$i]['warehouse'])])->row_array();
            if(!$warehouse){

                return array(
                    'status'    => false,
                    'message'   => "Warehouse '".$wh."' doesn't not exists"
                );

            }

            $inserts = array(
                'name'          => $data[$i]['name'],
                'description'   => $data[$i]['description'],
                'warehouse_id'  => $warehouse['id']
            );

            $this->db
                ->from($this->table)
                ->or_where('lower(name)', strtolower($data[$i]['name']));

            $check = $this->db->get()->row_array();

            if($check){
                $this->db->where('id',$check['id']);
                $this->db->update($this->table,$inserts);
            }else{
                $this->db->insert($this->table,$inserts);
            }

        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );
    }
}

?>

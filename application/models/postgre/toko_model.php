<?php
class toko_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'toko';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'receiving_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $table8 = 'supplier_barang';
    private $po_barang = 'purchase_order_barang';
    private $kurs   = 'kurs';

    
    private function data($condition = array()) {
        //==========filter============================
        //$condition = array();
        $kd_toko= $this->input->post("kd_toko");
        $nama_toko= $this->input->post("nama_toko");
        $username = $this->input->post('username');
        $alamat = $this->input->post('alamat');
        $owner = $this->input->post('owner');
        $telp = $this->input->post('telp');

        if(!empty($kd_toko)){
            $condition["kd_toko like '%$kd_toko%' "]=null;
        }
        if(!empty($nama_toko)){
            $condition["nama_toko like '%$nama_toko%' "]=null;
        }
        if(!empty($username)){
            $condition["username like '%$username%'"]=null;
        }
        if(!empty($alamat)){
            $condition["alamat like '%$alamat%'"]=null;
        }
        if(!empty($owner)){
            $condition["owner like '%$owner%'"]=null;
        }
        if(!empty($telp)){
            $condition["telp like '%$telp%'"]=null;
        }

        
        //------------end filter-----------------
        $this->db->select('*');
        $this->db->from('toko');
        $this->db->order_by('nama_toko ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['id_toko'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get();
        $rows = array();

        foreach ($data->result() as $value) {
            //---------awal button action---------
            $id = $value->id_toko;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">';
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('toko/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('toko/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';
            //------------end button action---------------
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_toko,
                    'kd_toko' => $value->kd_toko,
                    'nama_toko' => $value->nama_toko ,
                    'alamat' => $value->alamat,
                    'telp' => $value->telp,
                    'owner' => $value->owner,
                    'username'=> $value->username,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_toko,
                    'kd_toko' => $value->kd_toko,
                    'nama_toko' => $value->nama_toko ,
                    'alamat' => $value->alamat,
                    'owner' => $value->owner,
                    'username'=> $value->username,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }   

    public function data_table_excel() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_receiving >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_receiving <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $rows[] = array(
                'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                'kd_receiving' => $value->kd_receiving,
                'kd_po' => $value->kd_po,
                'supplier' => $value->nama_supplier,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }     



   

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_toko' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_toko' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Chose Store--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_toko] = $row->nama_toko ;
        }
        return $options;
    }

    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_toko_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    
}

?>
<?php

class Api_warehouse_transfer_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }


    function get_count($data){

        $serialize  = $this->get_serial($data["kd_unik"])->row_array();

        if($serialize["first_qty"] == 0){

           $this->db->select("pq.qty, (SELECT COUNT(*) FROM item_receiving_details ird_ LEFT JOIN item_receiving ir_ ON ir_.id=ird_.item_receiving_id LEFT JOIN pickings p_ ON p_.pl_id=ird_.picking_id WHERE p_.name = '" . $data["pl_name"] . "' AND ir_.item_id = pq.item_id) AS picked", false);
            $this->db->from("picking_qty pq");
            $this->db->join("pickings p" , "p.pl_id = pq.picking_id", "left");
            $this->db->join("items itm" , "itm.id = pq.item_id", "left");
            $this->db->where("p.name", $data["pl_name"]);
            $this->db->where("pq.item_id", $serialize["item_id"]);

        } else{

            $this->db
                ->select(["COALESCE(SUM(qty_pick), 0) as pick","p_.name","ir_.item_id"])
                ->from('picking_history kit_')
                ->join('pickings p_','kit_.picking_id=p_.pl_id','left')
                ->join('item_receiving_details ird_','ird_.unique_code=kit_.unique_code')
                ->join('item_receiving ir_','ir_.id=ird_.item_receiving_id','left')
                ->join('items itm_','itm_.id=ir_.item_id')
                ->where('p_.name', $data['pl_name'])
                ->where('ir_.item_id', $serialize['item_id'])
                ->group_by('p_.name, ir_.item_id');

            $tpk =$this->db->get_compiled_select();

            $this->db
                ->select(["pq.qty","COALESCE(tpk.pick, 0) as picked"])
                ->from('picking_qty pq')
                ->join('pickings p','p.pl_id=pq.picking_id','left')
                ->join("($tpk) as tpk",'tpk.name=p.name and tpk.item_id=pq.item_id','left')
                ->where('p.name', $data['pl_name'])
                ->where('pq.item_id', $serialize['item_id']);

        }
        
        return $this->db->get();
        
    }

    function get_picked($picking_code){

        $this->db
            ->select('item_id, sum(qty) as q_picked')
            ->from('picking_recomendation pr')
            ->join('pickings p_','p_.pl_id=pr.picking_id','left')
            ->where('p_.name', $picking_code)
            ->group_by('pr.item_id');

        $sp = $this->db->get_compiled_select();

        $this->db
            ->select('item_id, sum(last_qty) as q_picked')
            ->from('item_receiving_details ird_')
            ->join('pickings pl_','pl_.pl_id=ird_.picking_id','left')
            ->where('pl_.name', $picking_code)
            ->group_by('ird_.item_id');

        $sp2 = $this->db->get_compiled_select();

        $this->db
            ->select([
                "itm.code as kd_barang",
                "COALESCE(string_agg(DISTINCT l.name, ','), '-- No Stok --') as location",
                "pq.qty as qty",
                "COALESCE(sp.q_picked, 0) as single_picked",
                "COALESCE(sp2.q_picked, 0) as multiple_picked"
            ])
            ->from('picking_qty pq')
            ->join("($sp) as sp",'sp.item_id=pq.item_id','left')
            ->join("($sp2) as sp2",'sp2.item_id=pq.item_id','left')
            ->join('pickings p','p.pl_id=pq.picking_id','left')
            ->join('item_receiving_details ird','ird.item_id=pq.item_id and ird.location_id not in (100,101,102,103,104,105,106)','left')
            ->join('items itm','itm.id=pq.item_id','left')
            ->join('locations l','l.id=ird.location_id','left')
            ->where('p.name', $picking_code)
            ->where('(ird.picking_id IS NULL or ird.last_qty > 0)', NULL)
            ->group_by(['pq.item_id','itm.code','pq.qty','sp.q_picked','sp2.q_picked']);

        $r = $this->db->get();

        return $r;
    }

    function get_serial($serial_number){

        $ignore_location = array(100, 101, 102, 103, 104, 105, 106);

        $avail_location = '2 , 3';

    	$this->db->select("*");
    	$this->db->from("item_receiving_details ird");
		$this->db->join("locations loc", "loc.id = ird.location_id", "left");
        $this->db->join("item_receiving ir", "ir.id = ird.item_receiving_id", "left");
    	$this->db->where("ird.unique_code", $serial_number);
        $this->db->where("(ird.picking_id IS NULL or ird.last_qty >0)");
    	return $this->db->get();
    }

    function retrieve_serial($serial_number){
        $this->db->select("itm.code as kd_barang, itm.has_qty, ird.last_qty");
        $this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir","ir.id=ird.item_receiving_id", "left");
        $this->db->join("items itm", "itm.id=ir.item_id", "left");
        $this->db->where("unique_code", $serial_number);
        $this->db->where("(ird.picking_id IS NULL or ird.last_qty >0)");
        return $this->db->get();
    }

    function post_serial($data){
        $id_user    = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $id_picking = $this->get_id("pl_id", "name", $data["pl_name"], "pickings")->row_array();
        $ird        = $this->get_id("item_receiving_id as ir_id", "unique_code", $data["kd_unik"], "item_receiving_details")->row_array();
        $id_barang  = $this->get_id("item_id as id_barang", "id", $ird["ir_id"], "item_receiving")->row_array();
        $serialize  = $this->get_serial($data["kd_unik"])->row_array();
        switch ($data["code"]) {
            case 'single':
                $data_insert = array(   
                                    'unique_code'   => $data["kd_unik"], 
                                    'loc_id_old'    => $serialize["location_id"],
                                    'user_id_pick'  => $id_user["user_id"],
                                    'pick_time'     => $data["pick_time"],
                                    'process_name'  => 'PICKING',
                                    'picking_id' => $id_picking["pl_id"]
                                );

                $this->db->insert("transfers", $data_insert);

                $qty_after = $serialize["last_qty"] - $data["qty"];

                $this->db->set("last_qty", $qty_after);
                $this->db->where("unique_code", $data["kd_unik"]);
                $this->db->update("item_receiving_details");


                $this->db->set("picking_id", $id_picking["pl_id"]);
                $this->db->set("location_id", 103);
                $this->db->where("unique_code", $data["kd_unik"]);
                $this->db->update("item_receiving_details");
                break;
            case 'multiple':
                $data_insert = array(   
                                    'picking_id'     => $id_picking["pl_id"],
                                    'unique_code'   => $data["kd_unik"], 
                                    'first_qty' => $serialize["first_qty"],
                                    'qty_bef'   => $serialize["last_qty"],
                                    'qty_pick'  => $data["qty"],
                                    'qty_after' => $serialize["last_qty"] - $data["qty"],
                                    'user_id'  => $id_user["user_id"],
                                    'picking_time'  => $data["kit_time"]
                                );

                $this->db->insert("picking_history", $data_insert);

                if($data_insert["qty_after"] == 0){
                    $this->db->set('location_id',101);
                }
                $this->db->set("last_qty", $data_insert["qty_after"]);
                $this->db->set("picking_id", $id_picking["pl_id"]);
                $this->db->where("unique_code", $data["kd_unik"]);
                $this->db->update("item_receiving_details");
                break;
            default:
                # nothing to do.
                break;
        }

        if($data['qty']==0 || $data['qty'] == NULL){
            $qty_insert = 1;
        }else{
            $qty_insert = $data['qty'];
        }

        $data_insert = [
            "picking_id" => $id_picking['pl_id'],
            "unique_code" => $data['kd_unik'],
            "item_id" => $id_barang['id_barang'],
            "location_id" => 101,
            "qty" => $qty_insert
        ];

        $this->db->insert('picking_recomendation',$data_insert);
    }

    function get_item($item_code){
        $this->db->select("*");
        $this->db->where("kd_barang", $item_code);
        $this->db->from("barang");
        return $this->db->get();
    }

    function post($data){
        $id_picking = $this->get_id("pl_id", "name", $data["picking_code"], "pickings")->row_array();
        $id_barang  = $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();
        $id_picker  = $this->get_id("id as user_id", "user_name", $data["uname_pick"], "users")->row_array();
        $id_putter  = $this->get_id("id as user_id", "user_name", $data["uname_put"], "users")->row_array();
        $loc_old    = $this->get_id("id as loc_id", "name", $data["old_loc"], "locations")->row_array();
        $loc_new    = $this->get_id("id as loc_id", "name", $data["new_loc"], "locations")->row_array();
        $itemize    = $this->get_item($data["kd_barang"])->row_array();

        if ($itemize["has_qty"] == 1) {
            $this->db->set("picking_id", $id_picking["pl_id"]);
            $this->db->set("pl_status", 1);
            $this->db->set("picking_time", $data["pick_time"]);
            $this->db->set("user_id_picking", $id_picker["user_id"]);
            // $this->db->where("item_id", $id_barang["id_barang"]);
            $this->db->where("unique_code", $data["kd_unik"]);
            $this->db->update("item_receiving_details");
        } else{
            $this->db->set("location_id", $loc_new["loc_id"]);
            $this->db->set("picking_id", $id_picking["pl_id"]);
            $this->db->set("pl_status", 1);
            $this->db->set("picking_time", $data["pick_time"]);
            $this->db->set("user_id_picking", $id_picker["user_id"]);
            // $this->db->where("id_barang", $id_barang["id_barang"]);
            $this->db->where("unique_code", $data["kd_unik"]);
            $this->db->update("item_receiving_details");

            $this->db->set("loc_id_new", $loc_new["loc_id"]);
            $this->db->set("user_id_put", $id_putter["user_id"]);
            $this->db->set("put_time", $data["put_time"]);
            $this->db->set("process_name", "PICKING");
            $this->db->where("unique_code", $data["kd_unik"]);
            $this->db->where("loc_id_old", $loc_old["loc_id"]);
            $this->db->where("user_id_pick", $id_picker["user_id"]);
            $this->db->where("loc_id_new IS NULL");
            $this->db->where("user_id_put IS NULL");
            $this->db->update("transfers");
        }
    }

}
<?php

class Api_agent_app_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function getSerialNumber($serialNumber){

        $this->db
        	->select(['lower(substring(unique_code,1,30)) as unique_code'])
            ->from('item_receiving_details ird')
            ->join('locations loc','loc.id=ird.location_id')
            ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
            ->join('warehouses wh','wh.id=loc_area.warehouse_id')
            ->where_not_in('location_id',[100,101,103,102,104])
            ->where_in('lower(substring(unique_code,1,30))',$serialNumber)
            ->group_by('ird.unique_code');

        $res = $this->db->get();

        return $res->result_array();

    }

    function getWarehouseBySerialNumber($serialNumber){

        $this->db
            ->select('wh.*')
            ->from('item_receiving_details ird')
            ->join('locations loc','loc.id=ird.location_id')
            ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
            ->join('warehouses wh','wh.id=loc_area.warehouse_id')
            ->join('warehouses_warehouse_groups wwg','wwg.warehouse_id=wh.id')
            ->join('warehouse_groups wg','wg.id=wwg.warehouse_group_id')
            ->where_not_in('location_id',[100,101,102,104])
            ->where_in('lower(substring(unique_code,1,30))',$serialNumber)
            ->group_by('wh.id, wh.name, wh.description, wh.created_at, wh.updated_at, wh.address');

        $res = $this->db->get();

        return $res->result_array();

    }

    function getSerialNumberOutbound($params){

        $this->db
            ->from('outbound_item_receiving_details oird')
            ->join('outbound outb','outb.id=oird.id_outbound')
            ->join('item_receiving_details ird','ird.id=oird.id_item_receiving_details')
            ->join('items itm','itm.id=ird.item_id')
            ->where_in('lower(substring(ird.unique_code,1,30))',$params['serial_number'])
            ->where('outb.code',$params['transaction_code'])
            ->order_by('itm.id');

        $res = $this->db->get();

        return $res->result_array();

    }

    function getPcCode($id_outbound){
        $this->load_model('packing_model');
        $pn = $this->packing_model->get_packing_number($id_outbound);
        if($pn->num_rows()>0){
            $pc_code=$pn->row()->packing_number;
            //$pc_string_tmp = $pn->row()->pc_string;
            $inc=$pn->row()->inc;
            $pc_string_tmp = '';
            for($i=1;$i<=$inc;$i++){
                if($inc<10){
                    $pad=0;
                }else{
                    $pad='';
                }
                if($i==$inc){
                    $pc_string_tmp = '- <b>'.$pc_code.$pad.$i.$pad.$inc.'</b><br/> '.$pc_string_tmp;
                }else{
                    $pc_string_tmp = '&nbsp;&nbsp;- '.$pc_code.$pad.$i.$pad.$inc.'<br/> '.$pc_string_tmp;
                }
            }
            //$pc_string = str_replace('x',$inc,$pc_string_tmp);
        }else{
            $this->packing_model->add_pc_code();
            $pc_code = $this->packing_model->get_pc_code();
            $pc_string_tmp= '<b>'.$pc_code.'0101</b>';
            $inc=1;
        }
        return array('pc_code'=>$pc_code,'inc'=>$inc,'pc_string'=>$pc_string_tmp);        
    }

    function getPlanBySerialNumber($serialNumber){

        $this->db
            ->select('wg.plant_sap, wg.storage_location_sap')
            ->from('item_receiving_details ird')
            ->join('locations loc','loc.id=ird.location_id')
            ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
            ->join('warehouses wh','wh.id=loc_area.warehouse_id')
            ->join('warehouses_warehouse_groups wwg','wwg.warehouse_id=wh.id')
            ->join('warehouse_groups wg','wg.id=wwg.warehouse_group_id')
            ->join('items itm','itm.id=ird.item_id','left')
            ->where_not_in('location_id',[100,101,102,104])
            ->where_in('lower(substring(unique_code,1,30))',$serialNumber)
            ->where('itm.preorder',0)
            ->group_by('wg.plant_sap, wg.storage_location_sap');

        $res = $this->db->get();

        return $res->result_array();

    }    

    // function deduct_stock($params){

    //     $data2 = array();

    //     $date = date('Y-m-d h:i:s');
    //     if(!is_array($params['serial_number'])){
    //         $params['serial_number'][] = $params['serial_number'];
    //     }

    //     foreach ($params['serial_number'] as &$sn) {
    //         $sn = (string)$sn;
    //         $sn = strtolower(substr($sn, 0,30));
    //     }

    //     $user = $this->db->get_where('users',['user_name'=>$this->input->get_request_header('User', true)])->row();

    //     $warehouse = $this->getWarehouseBySerialNumber($params['serial_number']);
    //     if(count($warehouse) > 1){

    //         $data2['status']     = false;
    //         $data2['message']    = "The serial number from a different warehouse.";

    //         return $data2;

    //     }else{

    //         $serialNumber = $this->getSerialNumber($params['serial_number']);

    //         if(empty($serialNumber)){

    //             $data2['status'] = false;

    //             foreach ($params['serial_number'] as $serial) {
    //                 $data2['message'] = "This serial number '".$serial."' not available";
    //                 $data2['status']     = false;

    //                 return $data2;
    //             }

    //             return $data2;

    //         }else{

    //             $warehouseId = $warehouse[0]['id'];

    //             foreach ($params['serial_number'] as $serial) {

    //                 $match = false;

    //                 foreach ($serialNumber as $serial2) {
    //                     if($serial === $serial2['unique_code']){
    //                         $match = true;
    //                     }
    //                 }

    //                 if(!$match){
    //                     $data2['message'] = "This serial number '".$serial."' not available";
    //                     $data2['status']     = false;

    //                     return $data2;
    //                 }

    //             }

    //             unset($params['serial_number']);

    //             foreach ($serialNumber as $serial) {
    //                 $params['serial_number'][] = $serial['unique_code'];
    //             }

    //             $getSerialNumberOutbound = $this->getSerialNumberOutbound($params);

    //             $params['items'] = array();

    //             foreach ($params['serial_number'] as $serial) {

    //                 $match = false;

    //                 foreach ($getSerialNumberOutbound as $serial2) {
    //                     if($serial === $serial2['unique_code']){
    //                         $match = true;
    //                     }
    //                 }

    //                 if($match){
    //                     $data2['message'] = "This serial number '".$serial."' has been picked before";
    //                     $data2['status']     = false;

    //                     return $data2;
    //                 }else{


    //                     $this->db
    //                         ->select('itm.id as item_id, itm.code as item_code, ird.unique_code as serial_number')
    //                         ->from('item_receiving_details ird')
    //                         ->join('items itm','itm.id=ird.item_id')
    //                         ->where('lower(substring(unique_code,1,30))',$serial);

    //                     $item = $this->db->get()->row_array();

    //                     if(!array_key_exists($item['item_code'], $params['items'])){
    //                         $params['items'][$item['item_code']] = array(
    //                             "item_id"   => $item['item_id'],
    //                             "item_code" => $item['item_code']
    //                         );
    //                     }

    //                     $params['items'][$item['item_code']]['serial_number'][] = $item['serial_number'];

    //                 }
    //             }
    //             unset($params['serial_number']);

    //         }
    //     }

    //     /**/

    //     $agentAppCategory = $this->db->get_where('agent_app_transaction_categories',['lower(code)'=>strtolower($params['transaction_category'])])->row_array();
    //     if(!$agentAppCategory){

    //         $data2['message'] = "This transaction_category '".$params['transaction_category']."' doesn't exist.";
    //         $data2['status']     = false;

    //         return $data2;

    //     }

    //     /**/

    //     /* Create Outbound Document*/

    //     $this->db->trans_start();

    //     $this->load_model('outbound_model');

    //     $this->db
    //         ->from('outbound')
    //         ->where('code',$params['transaction_code']);


    //     $outbound = $this->db->get()->row();
    //     if(empty($outbound)){

    //         if($params['transaction_type'] == '06A'){
    //             $movement_type = '913';
    //         }else{
    //             $movement_type = '911';
    //         }

    //         $data2 = array(
    //             'code'           => $params['transaction_code'],
    //             'date'           => $date,
    //             'user_id'        => $user->id,
    //             'warehouse_id'   => $warehouseId,
    //             'status_id'      => 4,
    //             'agent_app_transaction_category_id' => $agentAppCategory['id']
    //             // 'customer_id'    => $customer->id
    //         );
           
    //          $docId  = $this->db->get_where('documents',['movement_type'=>$movement_type])->row_array();
    //         if($docId){
    //             $data2['document_id'] = $docId['document_id'];
    //         }

    //         $this->db->insert('outbound', $data2);
    //         $idOutbound = $this->db->insert_id();

    //         $data2_item = array();

    //         foreach ($params['items'] as $item) {

    //             $this->db
    //                 ->set('id_outbound',$idOutbound)
    //                 ->set('item_id',$item['item_id'])
    //                 ->set('qty',count($item['serial_number']))
    //                 ->set('batch',NULL)
    //                 ->insert('outbound_item');

    //         }

    //     }else{

    //         $idOutbound = $outbound->id;

    //         foreach ($params['items'] as $item) {

    //             $cSerial = count($item['serial_number']);
    //             $updateOutboundItem = "UPDATE outbound_item SET qty=qty+".$cSerial." WHERE id_outbound=".$idOutbound." AND item_id=".$item['item_id'];
    //             $this->db->query($updateOutboundItem);

    //         }

    //     }

    //     /* END - Create Outbound Document */

    //     /* Create Picking Document */

    //     $this->load_model('picking_list_model');

    //     $this->db
    //         ->from('pickings p')
    //         ->join('picking_list_outbound plo','plo.pl_id=p.pl_id','left')
    //         ->where('plo.id_outbound', $idOutbound);

    //     $picking = $this->db->get()->row();

    //     /*
    //         if(empty){
    //             Create Picking Document & Get Picking ID
    //         }else{
    //             Get Picking ID
    //         }
    //     */

    //     if(empty($picking)){

    //         $pickingCode = $this->picking_list_model->get_pl_code();

    //         $data2Picking = array(
    //             'pl_name'      => $pickingCode,
    //             'pl_date'      => $date,
    //             'pl_status' => 1,
    //             'user_id'   => $user->id
    //         );

    //         $this->picking_list_model->create($data2Picking);
    //         $idPicking = $this->db->insert_id();

    //         $this->db
    //             ->set('pl_id', $idPicking)
    //             ->set('id_outbound', $idOutbound)
    //             ->insert('picking_list_outbound');

    //         $this->picking_list_model->add_pl_code();

    //         $data2_item = array();

    //         foreach ($params['items'] as $item) {

    //             $data2_item[] = array(
    //                 'picking_id' => $idPicking,
    //                 'item_id'     => $item['item_id'],
    //                 'qty'         => count($item['serial_number']),
    //                 'batch'       => NULL
    //             );
    //         }

    //         if(!empty($data2_item)) {
    //             $this->db->insert_batch('picking_qty', $data2_item);
    //         }

    //     }else{

    //         $idPicking = $picking->pl_id;
    //         $pickingCode = $picking->name;

    //         foreach ($params['items'] as $item) {

    //             $cSerial = count($item['serial_number']);
    //             $updatePickingQty = "UPDATE picking_qty SET qty=qty+".$cSerial." WHERE picking_id=".$idPicking." AND item_id=".$item['item_id'];
    //             $this->db->query($updatePickingQty);

    //         }

    //     }

    //     foreach ($params['items'] as $item) {

    //         foreach ($item['serial_number'] as $sn) {

    //             $this->db
    //                 ->select('*, ird.id as item_receiving_details_id')
    //                 ->from('item_receiving_details ird')
    //                 ->join('locations loc','loc.id=ird.location_id')
    //                 ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
    //                 ->join('warehouses wh','wh.id=loc_area.warehouse_id')
    //                 ->where('ird.unique_code',$sn)
    //                 ->where('ird.last_qty >',0)
    //                 ->where_not_in('location_id',[100,101,102,104])
    //                 ->where('wh.id',$warehouseId);

    //             $serial = $this->db->get()->row_array();

    //             if(empty($serial)){

    //                 $data2['message'] = "This serial number '".$serial['unique_code']."' no stock.";
    //                 $data2['status']     = false;

    //                 return $data2;

    //             }else{

    //                 $serial['last_qty'] = (int)$serial['last_qty'];

    //                 if($serial['last_qty'] < 1){
    //                     $data2['message'] = "This serial number '".$serial['unique_code']."' no stock.";
    //                     $data2['status']     = false;

    //                     return $data2;
    //                     continue;
    //                 }else{

    //                     if(($serial['last_qty'] - 1) > 0){

    //                         $RBupdate = "UPDATE item_receiving_details SET picking_id=$idPicking, last_qty=last_qty-1 WHERE unique_code='".$serial['unique_code']."'";
    //                         $this->db->query($RBupdate);

    //                     }else{

    //                         $RBupdate = "UPDATE item_receiving_details SET picking_id=$idPicking, location_id=101, last_qty=last_qty-1 WHERE unique_code='".$serial['unique_code']."'";
    //                         $this->db->query($RBupdate);

    //                     }

    //                     $data2_insert = [
    //                         "picking_id" => $idPicking,
    //                         "unique_code" => $serial['unique_code'],
    //                         "item_id" => $item['item_id'],
    //                         "location_id" => 101,
    //                         "qty" => 1
    //                     ];

    //                     $this->db->insert('picking_recomendation',$data2_insert);
    //                     $prId = $this->db->insert_id();

    //                     $this->db
    //                         ->set('picking_recomendation_id',$prId)
    //                         ->set('agent_app_transaction_category_id',$agentAppCategory['id'])
    //                         ->insert('picking_recomendation_agent_app_transaction_categories');

    //                     $data2_insert = array(   
    //                                         'unique_code'   => $serial['unique_code'],
    //                                         'loc_id_old'    => $serial['location_id'],
    //                                         'user_id_pick'  => $user->id,
    //                                         'pick_time'     => $date,
    //                                         'process_name'  => 'PICKING',
    //                                         'picking_id'    => $idPicking
    //                                     );

    //                     $this->db->insert("transfers", $data2_insert);

    //                     $data2_insert = array(   
    //                                         'unique_code'   => $serial['unique_code'],
    //                                         'loc_id_old'    => $serial['location_id'],
    //                                         'user_id_pick'  => $user->id,
    //                                         'pick_time'     => $date,
    //                                         'process_name'  => 'PICKING',
    //                                         'picking_id'    => $idPicking
    //                                     );

    //                     $this->db->insert("transfers", $data2_insert);

    //                     $pcCode = $this->getPcCode($idOutbound);

    //                     $data2ItemReceivingDetailOutbound = array(
    //                         "id_item_receiving_details" => $serial['item_receiving_details_id'],
    //                         "id_outbound" => $idOutbound,
    //                         "packing_number" => $pcCode['pc_code'],
    //                         'inc' => $pcCode['inc']
    //                     );

    //                     $this->db->insert('outbound_item_receiving_details',$data2ItemReceivingDetailOutbound);

    //                     $data2['message'][] = "This serial number '".$serial['unique_code']."' success";

    //                 }

    //             }

    //         }

    //     }

    //     $sql = "UPDATE picking_recomendation SET picked_qty=qty, st_packing=1 WHERE picking_id=$idPicking";
    //     $this->db->query($sql);

    //     $updatePicking = "UPDATE pickings SET status_id=10, start_time=COALESCE(start_time,now()), end_time='$date' WHERE pl_id=$idPicking";
    //     $this->db->query($updatePicking);

    //     /* END - Create Picking Document*/

    //     // $this->load_model('api_picking_model');
    //     // $this->api_picking_model->sendToStaging($pickingCode);

    //     $this->db->trans_complete();

    //     $data2['status']  = $this->db->trans_status();

    //     return $data2;
    // }

    function warehouse_group_detail($warehouseGroup=''){

        $this->db
            ->select('wh.id as warehouse_id, wh.name as warehouse_name')
            ->from('warehouse_groups wg')
            ->join('warehouses_warehouse_groups wwg','wwg.warehouse_group_id=wg.id')
            ->join('warehouses wh','wh.id=wwg.warehouse_id');

        $data2 = $this->db->get();

        return $data2;

    }

    function log($params=array()){

        if(!is_array($params['serial_number'])){
            $params['serial_number'][] = $params['serial_number'];
        }

        if(is_array($params['message'])){
            $params['message'] = implode(" ", $params['message']);
        }

        foreach ($params['serial_number'] as $sn) {

            $this->db
                ->set('transaction_code',$params['transaction_code'])
                ->set('transaction_type', $params['transaction_type'])
                ->set('transaction_category',$params['transaction_category'])
                ->set('serial_number',$sn)
                ->set('message',$params['message'])
                ->insert('agent_app_log');

        }

    }

    function deduct_stock($params){

        $data2 = array();

        $date = date('Y-m-d h:i:s');
        if(!is_array($params['serial_number'])){
            $params['serial_number'][] = $params['serial_number'];
        }

        foreach ($params['serial_number'] as &$sn) {
            $sn = (string)$sn;
            $sn = strtolower(substr($sn, 0,30));
        }

        $user = $this->db->get_where('users',['user_name'=>$this->input->get_request_header('User', true)])->row();

        $serialNumber = $this->getSerialNumber($params['serial_number']);

        if(empty($serialNumber)){

            $data2['status'] = false;
            $data2['message'] = "Semua serial number tidak tersedia";

            return $data2;

        }else{

            foreach ($params['serial_number'] as $serial) {

                $match = false;

                foreach ($serialNumber as $serial2) {
                    if($serial === $serial2['unique_code']){
                        $match = true;
                    }
                }

                if(!$match){
                    $data2['message']    = "This serial number '".$serial."' not available";
                    $data2['status']     = false;

                    return $data2;
                }

            }

            $planWarehouse = $this->getPlanBySerialNumber($params['serial_number']);
            if(count($planWarehouse) > 1){

                $data2['status']     = false;
                $data2['message']    = "Serial number berada pada kode plant yang berbeda.";

                return $data2;

            }else{

                $getSerialNumberOutbound = $this->getSerialNumberOutbound($params);

                $params['items'] = array();

                foreach ($params['serial_number'] as $serial) {

                    $match = false;

                    foreach ($getSerialNumberOutbound as $serial2) {
                        if($serial === $serial2['unique_code']){
                            $match = true;
                        }
                    }

                    if($match){

                        $data2['status']  = true;
                        $data2['message'] = "This serial number '".$serial."' has been picked before";

                        return $data2;

                    }else{

                        $this->db
                            ->select('itm.id as item_id, itm.code as item_code, ird.unique_code as serial_number, loc_area.warehouse_id')
                            ->from('item_receiving_details ird')
                            ->join('locations loc','loc.id=ird.location_id')
                            ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
                            ->join('items itm','itm.id=ird.item_id')
                            ->where('lower(substring(unique_code,1,30))',$serial);

                        $item = $this->db->get()->row_array();

                        if(!array_key_exists($item['warehouse_id'], $params['items'])){
                            $params['items'][$item['warehouse_id']] = array();
                        }

                        if(!array_key_exists($item['item_code'], $params['items'][$item['warehouse_id']])){
                            $params['items'][$item['warehouse_id']][$item['item_code']] = array(
                                "item_id"   => $item['item_id'],
                                "item_code" => $item['item_code']
                            );
                        }

                        $params['items'][$item['warehouse_id']][$item['item_code']]['serial_number'][] = $item['serial_number'];

                    }

                }

                unset($params['serial_number']);

            }

        }

        /**/

        $agentAppCategory = $this->db->get_where('agent_app_transaction_categories',['lower(code)'=>strtolower($params['transaction_category'])])->row_array();
        if(!$agentAppCategory){

            $data2['message'] = "This transaction_category '".$params['transaction_category']."' doesn't exist.";
            $data2['status']     = false;

            return $data2;

        }

        $movement_type = ($params['transaction_type'] == "06A") ? '913' : '911';

        $docId  = $this->db->get_where('documents',['movement_type'=>$movement_type])->row_array();
        if(!$docId){

            $data2['message'] = "This transaction_type '".$params['transaction_type']."' doesn't exist.";
            $data2['status']     = false;

            return $data2;
        }

        /**/

        $this->db->trans_start();

        foreach ($params['items'] as $dk => $dv) {

            /* Create Outbound Document*/

            $this->load_model('outbound_model');

            $this->db
                ->select(["o.id","o.code"])
                ->from('outbound_agent_apps oap')
                ->join('outbound o','o.id=oap.outbound_id')
                ->where('oap.agent_apps_id',$params['transaction_code'])
                ->where('o.warehouse_id',$dk);

            $outbound = $this->db->get()->row();
            if(empty($outbound)){

                $outboundCode = $this->outbound_model->getOutDocument($docId['document_id']);

                $dataOutbound = array(
                    'code'           => $outboundCode,
                    'date'           => $date,
                    'user_id'        => $user->id,
                    'warehouse_id'   => $dk,
                    'status_id'      => 4,
                    'document_id'    => $docId['document_id']
                );

                $this->outbound_model->addOutDocument(['id_outbound_document'=>$docId['document_id']]);

                $this->db->insert('outbound', $dataOutbound);
                $idOutbound = $this->db->insert_id();

                $this->db
                    ->set('outbound_id',$idOutbound)
                    ->set('agent_apps_id',$params['transaction_code'])
                    ->insert('outbound_agent_apps');

                foreach ($dv as $d) {

                    $this->db
                        ->set('id_outbound',$idOutbound)
                        ->set('item_id',$d['item_id'])
                        ->set('qty',count($d['serial_number']))
                        ->set('batch',NULL)
                        ->insert('outbound_item');

                }

            }else{

                $idOutbound = $outbound->id;
                $outboundCode = $outbound->code;

                foreach ($dv as $d) {

                    $cSerial = count($d['serial_number']);
                    $updateOutboundItem = "UPDATE outbound_item SET qty=qty+".$cSerial." WHERE id_outbound=".$idOutbound." AND item_id=".$d['item_id'];
                    $this->db->query($updateOutboundItem);

                }

            }

            /* END - Create Outbound Document */

            /* Create Picking Document */

            $this->load_model('picking_list_model');

            $this->db
                ->from('pickings p')
                ->join('picking_list_outbound plo','plo.pl_id=p.pl_id','left')
                ->where('plo.id_outbound', $idOutbound);

            $picking = $this->db->get()->row();

            /*
                if(empty){
                    Create Picking Document & Get Picking ID
                }else{
                    Get Picking ID
                }
            */

            if(empty($picking)){

                $pickingCode = $this->picking_list_model->get_pl_code();

                $dataPicking = array(
                    'pl_name'      => $outboundCode,
                    'pl_date'      => $date,
                    'pl_status' => 1,
                    'user_id'   => $user->id
                );

                $this->picking_list_model->create($dataPicking);
                $idPicking = $this->db->insert_id();

                $this->db
                    ->set('pl_id', $idPicking)
                    ->set('id_outbound', $idOutbound)
                    ->insert('picking_list_outbound');

                $this->picking_list_model->add_pl_code();

                $dataPickingItem = array();

                foreach ($dv as $d) {

                    $dataPickingItem[] = array(
                        'picking_id' => $idPicking,
                        'item_id'     => $d['item_id'],
                        'qty'         => count($d['serial_number']),
                        'batch'       => NULL
                    );
                }

                if(!empty($dataPickingItem)) {
                    $this->db->insert_batch('picking_qty', $dataPickingItem);
                }

            }else{

                $idPicking = $picking->pl_id;
                $pickingCode = $picking->name;

                foreach ($dv as $d) {

                    $cSerial = count($d['serial_number']);
                    $updatePickingQty = "UPDATE picking_qty SET qty=qty+".$cSerial." WHERE picking_id=".$idPicking." AND item_id=".$d['item_id'];
                    $this->db->query($updatePickingQty);

                }

            }

            foreach ($dv as $d) {

                foreach ($d['serial_number'] as $sn) {

                    $this->db
                        ->select('*, ird.id as item_receiving_details_id')
                        ->from('item_receiving_details ird')
                        ->join('locations loc','loc.id=ird.location_id')
                        ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
                        ->join('warehouses wh','wh.id=loc_area.warehouse_id')
                        ->where('ird.unique_code',$sn)
                        ->where('ird.last_qty >',0)
                        ->where_not_in('location_id',[100,101,102,104,105,106])
                        ->where('wh.id',$dk);

                    $serial = $this->db->get()->row_array();

                    if(empty($serial)){

                        $data2['message'] = "This serial number '".$serial['unique_code']."' no stock.";
                        $data2['status']     = false;

                        return $data2;

                    }else{

                        $serial['last_qty'] = (int)$serial['last_qty'];

                        if($serial['last_qty'] < 1){
                            $data2['message'] = "This serial number '".$serial['unique_code']."' no stock.";
                            $data2['status']     = false;

                            return $data2;
                            continue;
                        }else{

                            if(($serial['last_qty'] - 1) > 0){

                                // $RBupdate = "UPDATE item_receiving_details SET picking_id=$idPicking, last_qty=last_qty-1 WHERE unique_code='".$serial['unique_code']."'";
                                $RBupdate = "UPDATE item_receiving_details SET picking_id=$idPicking, last_qty=last_qty-1 WHERE id=".$serial['item_receiving_details_id']." and last_qty > 0";
                                $this->db->query($RBupdate);

                            }else{

                                // $RBupdate = "UPDATE item_receiving_details SET picking_id=$idPicking, location_id=101, last_qty=last_qty-1 WHERE unique_code='".$serial['unique_code']."'";
                                $RBupdate = "UPDATE item_receiving_details SET picking_id=$idPicking, location_id=101, last_qty=last_qty-1 WHERE id=".$serial['item_receiving_details_id']." and last_qty > 0";
                                $this->db->query($RBupdate);

                            }

                            $dataPickRecInsert = [
                                "picking_id" => $idPicking,
                                "unique_code" => $serial['unique_code'],
                                "item_id" => $d['item_id'],
                                "location_id" => 101,
                                "qty" => 1
                            ];

                            $this->db->insert('picking_recomendation',$dataPickRecInsert);
                            $prId = $this->db->insert_id();

                            $this->db
                                ->set('picking_recomendation_id',$prId)
                                ->set('agent_app_transaction_category_id',$agentAppCategory['id'])
                                ->insert('picking_recomendation_agent_app_transaction_categories');

                            $dataTransferInsert = array(   
                                                'unique_code'   => $serial['unique_code'],
                                                'loc_id_old'    => $serial['location_id'],
                                                'user_id_pick'  => $user->id,
                                                'pick_time'     => $date,
                                                'process_name'  => 'PICKING',
                                                'picking_id'    => $idPicking
                                            );

                            $this->db->insert("transfers", $dataTransferInsert);

                            $pcCode = $this->getPcCode($idOutbound);

                            $dataItemReceivingDetailOutbound = array(
                                "id_item_receiving_details" => $serial['item_receiving_details_id'],
                                "id_outbound" => $idOutbound,
                                "packing_number" => $pcCode['pc_code'],
                                'inc' => $pcCode['inc']
                            );

                            $this->db->insert('outbound_item_receiving_details',$dataItemReceivingDetailOutbound);

                            $data2['message'][] = "This serial number '".$serial['unique_code']."' success";

                        }

                    }

                }

            }

            $sql = "UPDATE picking_recomendation SET picked_qty=qty, st_packing=1 WHERE picking_id=$idPicking";
            $this->db->query($sql);

            $updatePicking = "UPDATE pickings SET status_id=10, start_time=COALESCE(start_time,now()), end_time='$date' WHERE pl_id=$idPicking";
            $this->db->query($updatePicking);

        }

        $this->db->trans_complete();

        $data2['status']  = $this->db->trans_status();

        return $data2;

    }

    // function sendToStaging($transactionCode=array()){

    //     $this->db
    //         ->select('p.*')
    //         ->from('pickings p')
    //         ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
    //         ->join('outbound outb','outb.id=plo.id_outbound')
    //         ->where_in('outb.code',$transactionCode);

    //     $data2 = $this->db->get()->result_array();

    //     if($data2){

    //         $this->load_model('api_picking_model');
    //         foreach ($data2 as $d) {
    //             $this->api_picking_model->sendToStaging($d['name']);
    //         }

    //     }

    // }

    function getLocation($locationName=""){

        if(!empty($locationName)){
            $this->db->where('name',$locationName);
        }

        $this->db
            ->select(["name as \"warehouse_name\"","address as \"warehouse_address\""])
            ->from('warehouses');

        $data = $this->db->get()->result_array();
        return $data;

    }

    function validateLocationBySn($serialNumber=""){

        if(!empty($serialNumber)){

            $this->db
                ->select('ird.unique_code as serial_number, wh.name as warehouse_name')
                ->from('item_receiving_details ird')
                ->join('locations loc','loc.id=ird.location_id')
                ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
                ->join('warehouses wh','wh.id=loc_area.warehouse_id')
                ->where('last_qty >',0)
                ->where('qc_id',1)
                ->where('unique_code',$serialNumber)
                ->where_not_in('location_id',[100,102,103,104,105,106]);

            $data = $this->db->get()->row_array();

            return $data;
        }

        return false;

    }

    function validateSerialNumber($serialNumber=""){

        if(!empty($serialNumber)){

            $this->db
                ->from('item_receiving_details ird')
                ->where('last_qty >',0)
                ->where('qc_id',1)
                ->where('unique_code',$serialNumber)
                ->where_not_in('location_id',[100,102,103,104,105,106]);

            $data = $this->db->get()->row_array();

            return $data;
        }

        return false;

    }

    function sendToStaging($transactionCode){

        $this->db
            ->select('p.*')
            ->from('pickings p')
            ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
            ->join('outbound outb','outb.id=plo.id_outbound')
            ->join('outbound_agent_apps oap','oap.outbound_id=outb.id')
            ->where_in('oap.agent_apps_id',$transactionCode);

        $data = $this->db->get()->result_array();

        if($data){

            $this->load_model('api_picking_model');
            foreach ($data as $dt) {

                $user    = (!empty($this->input->get_request_header('User', true))) ? $this->input->get_request_header('User', true) : $this->session->userdata('username');

                $this->load_model('api_staging_model');

                $this->db
                    ->select('a.agent_app_transaction_category_id')
                    ->from('picking_recomendation_agent_app_transaction_categories a')
                    ->join('picking_recomendation pr','pr.id=a.picking_recomendation_id')
                    ->join('pickings p','p.pl_id=pr.picking_id')
                    ->where('p.name',$dt['name'])
                    ->group_by('a.agent_app_transaction_category_id');

                $data1 = $this->db->get()->result_array();

                if($data1){

                    foreach ($data1 as $d1) {

                        $this->db
                            ->select(['outb.id as outbound_id','outb.code as outbound_code','outb.date as outbound_date','outb.note as shipping_note', 'itm.id as item_id', 'p.pl_id as picking_id',
                                'p.name as picking_code','doc.name as document_name','doc.movement_type',"doc.code as document_code","outb.document_id as outbound_document","itm.sku as item_sku", "unt.code as unit_code", "cost.code as cost_center_code", "COALESCE(SUM(pr.qty),0) as qty", "wg.plant_sap",
                                "wg.storage_location_sap","spl.code as supplier_code","dwg.plant_sap as plant_sap_dest","dwg.storage_location_sap as storage_location_sap_dest","usr.user_name as outbound_user","usr.id as outbound_user_id","outb.destination_id as outbound_destination","outb.warehouse_id as outbound_warehouse","a.agent_app_transaction_category_id","itm.preorder","aatc.code as agent_app_transaction_category_code"])
                            ->from('pickings p')
                            ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
                            ->join('outbound outb','outb.id=plo.id_outbound')
                            ->join('documents doc','doc.document_id=outb.document_id')
                            ->join('suppliers spl',"spl.id=outb.destination_id and doc.movement_type IN ('909')",'left')
                            ->join('warehouses wh','wh.id=outb.warehouse_id','left')
                            ->join('warehouses_warehouse_groups wwg','wwg.warehouse_id=wh.id','left')
                            ->join('warehouse_groups wg','wg.id=wwg.warehouse_group_id','left')
                            ->join('warehouses dwh',"dwh.id=outb.destination_id and doc.movement_type IN ('351')",'left')
                            ->join('warehouses_warehouse_groups dwwg','dwwg.warehouse_id=dwh.id','left')
                            ->join('warehouse_groups dwg','dwg.id=dwwg.warehouse_group_id','left')
                            ->join('cost_center cost','cost.id=outb.cost_center_id','left')
                            ->join('picking_recomendation pr','pr.picking_id=p.pl_id','left')
                            ->join('picking_recomendation_agent_app_transaction_categories a','a.picking_recomendation_id=pr.id','left')
                            ->join('agent_app_transaction_categories aatc','aatc.id=a.agent_app_transaction_category_id')
                            ->join("items itm",'itm.id=pr.item_id','left')
                            ->join("units unt",'unt.id=itm.unit_id','left')
                            ->join('users usr','usr.id=outb.user_id','left')
                            ->where('p.name',$dt['name'])
                            ->where('a.agent_app_transaction_category_id',$d1['agent_app_transaction_category_id'])
                            ->group_by('outb.id, outb.date, outb.code, outb.note, p.pl_id, p.name, doc.name, doc.movement_type, itm.sku, unt.code, cost.code, wg.plant_sap, wg.storage_location_sap, outb.document_id, spl.code, doc.code, dwg.plant_sap, dwg.storage_location_sap, usr.user_name, itm.id, usr.id, a.agent_app_transaction_category_id, aatc.code')
                            ->order_by('itm.sku');

                            $data2 = $this->db->get()->result_array();

                            if($data2){

                                $pstngDate = strtotime("now");
                                $pstngDate = $pstngDate*1000;
                                $pstngDate = number_format($pstngDate, 0, '.', '');

                                $outboundDate = strtotime($data2[0]['outbound_date']);
                                $outboundDate = $outboundDate*1000;
                                $outboundDate = number_format($outboundDate, 0, '.', '');

                                $HeaderTxt = explode("#", $data2[0]['shipping_note']);
                                $HeaderTxt = (is_array($HeaderTxt)) ? $HeaderTxt[0] : $data2[0]['shipping_note'];
                                $HeaderTxt = substr($HeaderTxt, 0,25);

                                $post = array(
                                    'Extid'     => $data2[0]['picking_code']."#".$user,
                                    'PstngDate' => "\/Date($pstngDate)\/",
                                    'DocDate'   => "\/Date($outboundDate)\/",
                                );

                                $post['HeaderTxt'] = "GI ".$data2[0]['document_name'];

                                $poItem = 10;
                                $i=0;
                                foreach ($data2 as $d) {

                                    if($d['movement_type'] == 911 or $d['movement_type'] == 913){
                                        if($d['preorder'] == 1){
                                            continue;
                                        }
                                    }

                                    $post['NV_GIITEM'][$i]['Extid']     = $d['picking_code'];
                                    $post['NV_GIITEM'][$i]['Material']  = $d['item_sku'];
                                    $post['NV_GIITEM'][$i]['Plant']     = $d['plant_sap'];
                                    $post['NV_GIITEM'][$i]['StgeLoc']   = $d['storage_location_sap'];
                                    $post['NV_GIITEM'][$i]['MoveType']  = $d['movement_type'];
                                    $post['NV_GIITEM'][$i]['ValType']   = 'NORMAL';
                                    $post['NV_GIITEM'][$i]['EntryQnt']  = $d['qty'];
                                    $post['NV_GIITEM'][$i]['EntryUom']  = $d['unit_code'];
                                    $post['NV_GIITEM'][$i]['ItemText']  = (empty($d['shipping_note'])) ? "" : substr($d['shipping_note'],0,50);
                                    $post['NV_GIITEM'][$i]['Costcenter'] = $d['cost_center_code'];

                                    if($d['movement_type'] == 913){
                                        $post['NV_GIITEM'][$i]['Costcenter'] = 'ID06RIDE01';
                                        $post['NV_GIITEM'][$i]['Dname'] = 'DRIVER';
                                    }

                                    if($d['movement_type'] == 911){
                                        $post['NV_GIITEM'][$i]['Costcenter'] = 'ID06RIDE01';
                                        $post['NV_GIITEM'][$i]['Dname'] = 'DRIVER';
                                    }

                                    if($d['movement_type']  == 913 || $d['movement_type'] == 911){
                                        if($d['agent_app_transaction_category_id'] == 1){
                                            $post['Extid'] = $d['picking_code']."#".$user."#".$d['outbound_code']."##X";
                                            $post['NV_GIITEM'][$i]['Extid']     = $d['picking_code']."#".$user."#".$d['outbound_code']."##X";
                                        }else if($d['agent_app_transaction_category_id'] == 2){
                                            $post['Extid'] = $d['picking_code']."#".$user."#".$d['outbound_code']."#X#";
                                            $post['NV_GIITEM'][$i]['Extid']     = $d['picking_code']."#".$user."#".$d['outbound_code']."#X#";
                                        }else{
                                            $post['Extid'] = $d['picking_code']."#".$user."#".$d['outbound_code']."##";
                                            $post['NV_GIITEM'][$i]['Extid']     = $d['picking_code']."#".$user."#".$d['outbound_code']."##";
                                        }

                                    }

                                    if($d['movement_type'] == 909){
                                        $post['NV_GIITEM'][$i]['VendorId'] = $d['supplier_code'];
                                    }

                                    if($d['movement_type'] == 901){
                                        $post['HeaderTxt'] = "GI Replacement#";
                                    }

                                    if($d['movement_type'] == 905){

                                        if($d['document_code'] == "IMM"){
                                            $post['HeaderTxt'] = "GI Internal MM#";
                                        }else{
                                            $post['HeaderTxt'] = "GI Operational#";
                                        }
                                    }

                                    if($d['movement_type'] == 907){
                                        $post['HeaderTxt'] = "GI Collateral#";
                                    }
                                    
                                    if($d['movement_type'] == 351){

                                        $staging = $this->db->get_where('staging',['refCode'=>$d['picking_code']." - PO_STO"])->row_array();

                                        $post['NV_GIITEM'][$i]['PONumber'] = $staging['po_number'];
                                        $post['NV_GIITEM'][$i]['POItem']   = "$poItem";

                                        unset($post['NV_GIITEM'][$i]['Costcenter']);


                                        $poItem += 10;
                                    }

                                    $i++;

                                }

                                $api = "stock_mvt/GIHEADERSet";

                                if($d['movement_type'] != 311){

                                    if(isset($post['NV_GIITEM'])){

                                        $ref = array(
                                            'refCode'   => $data2[0]['picking_code']."-".$data2[0]['agent_app_transaction_category_code'],
                                            'refId'     => $data2[0]['picking_id']
                                        );

                                        $stagingId = $this->api_staging_model->sendToStaging(json_encode($post),$api,$ref);
                                        $this->api_staging_model->sync($stagingId);

                                    }

                                }

                            }

                    }

                }

            }

        }

    }

}
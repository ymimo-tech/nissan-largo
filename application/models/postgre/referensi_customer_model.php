<?php
class referensi_customer_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'm_customer';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_customer'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $nama_customer= $this->input->post("nama_customer");
        $alamat_customer= $this->input->post("alamat_customer");
        $no_telp= $this->input->post("no_telp");

        if(!empty($nama_customer)){
            $condition["a.nama_customer like '%$nama_customer%'"]=null;
        }

        if(!empty($alamat_customer)){
            $condition["a.alamat_customer like '%$alamat_customer%'"]=null;
        }

        if(!empty($no_telp)){
            $condition["a.no_telp like '%$no_telp%'"]=null;
        }
        
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.id_customer');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_customer;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
            $action .= '</li>';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'nama_customer' => $value->nama_customer,
                    'alamat_customer' => $value->alamat_customer,
                    'no_telp' => $value->no_telp,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'nama_customer' => $value->nama_customer,
                    'alamat_customer' => $value->alamat_customer,
                    'no_telp' => $value->no_telp,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) { 
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_customer' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_customer' => $id));
    }
    
    public function options($default = '--Pilih Nama Customer--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_customer] = $row->nama_customer ;
        }
        return $options;
    }
    
}

?>
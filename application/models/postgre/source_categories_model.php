<?php
class source_categories_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'source_categories';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $this->db->where('a.id',$id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id_supplier'])){
            $condition['id'] = $condition['id_supplier'];
            unset($condition['id_supplier']);
        }
        $this->data($condition);

        return $this->db->get();

    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('source_category_id' => $id));
    }

    public function delete($id) {

		$result = array();

        $t1 = $this->db->get_where('destinations',['source_category_id'=>$id])->row_array();
        $t2 = $this->db->get_where('sources',['source_category_id'=>$id])->row_array();

		if($t1 || $t2){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('source_category_id' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}

        return $result;

    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);


        for ($i=0; $i < $dataLen ; $i++) {

            $insert= array(
                'source_category_code'          => $data[$i]['source_category_code'],
                'source_category_name'          => $data[$i]['source_category_name']
            );

            $this->db
                ->from($this->table)
                ->where('LOWER(code)', strtolower($data[$i]['source_category_code']))
                ->or_where('lower(name)', strtolower($data[$i]['source_category_name']));

            $check = $this->db->get()->row_array();

            if($check){

                $id = $check['source_category_id'];
                $this->db->where('source_category_id',$id);
                $this->db->update($this->table,$insert);

            }else{

                $this->db->insert($this->table,$insert);
                $id = $this->db->insert_id();

            }

        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );
    }

}

?>

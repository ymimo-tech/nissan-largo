<?php
class referensi_lokasi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'locations';

    public function _data(){
        $this->db->select('a.id as loc_id, a.name as loc_name, a.description as loc_desc, a.location_category_id as loc_category_id, a.location_type_id as loc_type_id, a.location_area_id as loc_area_id, a.*');
        $this->db->from($this->table  . ' a');
        // $this->db->where('(a.id < 100 or a.id > 150)',NULL);


        return $this->db;
    }

    public function data($condition = array()) {

        foreach ($condition as $key => $value) {
            if(strpos($key,'a.loc_name') !== FALSE){
                $newKey = str_replace('a.loc_name', 'a.name', $key);
                $newKey = str_replace('"', "'", $newKey);
                $condition[$newKey] = NULL;
                unset($condition[$key]);
            }

            if(strpos($key,'a.loc_desc') !== FALSE){
                $newKey = str_replace('a.loc_desc', 'a.description', $key);
                $newKey = str_replace('"', "'", $newKey);
                $condition[$newKey] = NULL;
                unset($condition[$key]);
            }
        }

        if(!empty($condition['loc_id'])){
            $condition['a.id'] = $condition['loc_id'];
            unset($condition['loc_id']);
        }

        if(!empty($condition['a.loc_area_id '])){
            $condition['a.location_area_id'] = $condition['a.loc_area_id '];
            unset($condition['a.loc_area_id ']);
        }

        if(!empty($condition['a.loc_type_id '])){
            $condition['a.location_type_id'] = $condition['a.loc_type_id '];
            unset($condition['a.loc_type_id ']);
        }

        if(!empty($condition['a.loc_category_id '])){
            $condition['a.location_category_id'] = $condition['a.loc_category_id '];
            unset($condition['a.loc_category_id ']);
        }

        $this->_data();
        $this->db->select('c.name as nama_kategori, t.name as loc_type_name, r.name as loc_area_name, loc_type, wh.id as warehouse_id, wh.name as warehouse_name');
        $this->db->join('categories c','c.id = a.location_category_id','left' );
        $this->db->join('location_types t','t.id = a.location_type_id','left' );
        $this->db->join('location_areas r','r.id = a.location_area_id','left' );
        $this->db->join('warehouses wh','wh.id = r.warehouse_id','left' );
        $this->db->where_not_in('a.id',[103,104]);
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function getLocationById($post = array()){
		$result = array();

        $this->_data();
        $this->db->where_in('a.id', $post['loc_id']);

		$result = $this->db->get()->result_array();

		return $result;
	}

    public function getLocationByName($post = array()){
        $result = array();

        $this->_data();
        $this->db->where_in('a.name', $post['name']);

        $result = $this->db->get()->result_array();

        return $result;
    }

	public function getLocation(){
		$result = array();

        $this->data();
        $this->db->where('a.name IS NOT NULL', NULL);

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getQtyLocation(){
		$result = array();

        $this->_data();

		$row = $this->db->get()->num_rows();
		$result['qty'] = $row;

		return $result;
	}

    public function get_by_id($id) {
        $this->db->where('a.id',$id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_insert($data){

        $data = array(
            'name'  => $data['loc_name'],
            'description'  => $data['loc_desc'],
            'location_category_id'   => $data['loc_category_id'],
            'location_area_id'   => $data['loc_area_id'],
            'location_type_id'   => $data['loc_type_id']
        );

        return $data;

    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {

		$result = array();

		$in = explode(',', $this->config->item('hardcode_location_id'));

		if(in_array($id, $in)){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete master location';
		}else{

            $this->db
                ->select('count(*) as total')
                ->from('item_receiving_details')
                ->where('location_id', $id);

			$row = $this->db->get()->row_array();

			if($row['total'] > 0){
				$result['status'] = 'ERR';
				$result['message'] = 'Cannot delete location, because data have relation to another table';
			}else{
				$this->db->delete($this->table, array('id' => $id));

				$result['status'] = 'OK';
				$result['message'] = 'Delete data success';
			}
		}

		return $result;

    }

    public function options($default = '--Pilih Nama Lokasi--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->loc_id] = $row->loc_name ;
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if($table_name == 'm_inbound_document'){

				$r = $this->setting_model->getData();

				if(strtolower($dt['inbound_document_name']) == 'peminjaman' && $r['peminjaman'] == 'N')
					continue;
			}

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);
        $inserts = array();

        for ($i=0; $i < $dataLen ; $i++) {

            $area = $this->db->get_where('location_areas',['lower(name)'=>strtolower($data[$i]['location_area'])])->row_array();
            if(!$area){

                return array(
                    'status'    => false,
                    'message'   => "Bin Area '".$data[$i]['location_area']."' doesn't not exists"
                );

            }

            $this->db
                ->from('categories')
                ->where('lower(code)',strtolower($data[$i]['location_category']))
                ->or_where('lower(name)',strtolower($data[$i]['location_category']));
            $category = $this->db->get()->row_array();
            if(!$category){

                return array(
                    'status'    => false,
                    'message'   => "Category '".$data[$i]['location_category']."' doesn't not exists"
                );

            }

            $type = $this->db->get_where('location_types',['lower(name)'=>strtolower($data[$i]['location_type'])])->row_array();
            if(!$type){

                return array(
                    'status'    => false,
                    'message'   => "Bin Type '".$data[$i]['location_type']."' doesn't not exists"
                );

            }
			
			$warehouse = $this->db->get_where('warehouses',['lower(name)'=>strtolower($data[$i]['warehouse'])])->row_array();
            if(!$warehouse){

                return array(
                    'status'    => false,
                    'message'   => "warehouse '".$data[$i]['warehouse']."' doesn't not exists"
                );

            }

            $inserts = array(
                'name'                  => $data[$i]['location_name'],
                'description'           => $data[$i]['location_description'],
                'location_category_id'  => $category['id'],
                'location_type_id'      => $type['id'],
                'location_area_id'      => $area['id'],
				'rack'                  => isset($data[$i]['location_rack'])?$data[$i]['location_rack']:'',
                'row'                  => isset($data[$i]['location_row'])?$data[$i]['location_row']:'',
                'level'                  => isset($data[$i]['location_level'])?$data[$i]['location_level']:'',
                'bin'                  => isset($data[$i]['location_bin'])?$data[$i]['location_bin']:'',
                'color_code_bin'                  => isset($data[$i]['location_color_code_bin'])?$data[$i]['location_color_code_bin']:'',
                'color_code_level'                  => isset($data[$i]['location_color_code_level'])?$data[$i]['location_color_code_level']:'',
                //'warehouse_id'                  => isset($data[$i]['absentry'])?$data[$i]['absentry']:0,
				'warehouse_id'                  => $warehouse['id'],
            );

            $this->db
                ->from($this->table)
                ->where('LOWER(name)', strtolower($data[$i]['location_name']));

            $check = $this->db->get()->row_array();
            if($check){
                $this->db->where('id',$check['id']);
                $this->db->update('locations',$inserts);
            }else{
                $this->db->insert('locations',$inserts);
            }

        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );

    }

}

?>


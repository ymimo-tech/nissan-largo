<?php
class bbm_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'receiving';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'receiving_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $table8 = 'supplier_barang';
    private $po_barang = 'purchase_order_barang';
    private $kurs   = 'kurs';

    public function get_rcv_code(){
        $this->db->select('pre_code_rcv,inc_rcv');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_rcv.$result->inc_rcv;
    }

    public function add_rcv_code(){
        $this->db->set('inc_rcv',"inc_rcv+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

    private function get_kd_barang($id_barang){
        $condition["id_barang"]=$id_barang;
        $this->db->select('kd_barang');
        $this->db->from($this->table5 );
        $this->db->where_condition($condition);
        return $this->db->get()->row()->kd_barang;
    }
    private function data($condition = array()) {
        //==========filter============================
        //$condition = array();
        $kd_receiving= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');

        if(!empty($kd_receiving)){
            $condition["a.kd_receiving like '%$kd_receiving%' "]=null;
        }
        if(!empty($id_supplier)){
            $condition["(b.kd_supplier like '%$id_supplier%' or b.nama_supplier like '%$id_supplier%') "]=null;
        }
        if(!empty($id_purchase_order)){
            $condition["c.kd_po like '%$id_purchase_order%'"]=null;
        }
        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.tanggal_receiving >= '$tanggal_awal'"] = null ;
            $condition["a.tanggal_receiving <= '$tanggal_akhir'"] = null;
        }

        
        //------------end filter-----------------
        $this->db->select('*,b.id_supplier as id_supplier');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table3 . ' c',' a.id_po = c.id_po','left');
        $this->db->join($this->table2 . ' b',' c.id_supplier = b.id_supplier','left');
        $this->db->order_by('a.tanggal_receiving DESC,a.kd_receiving DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function data_barcode($id_rcv = ''){
        $this->db->select('*');
        $this->db->from('receiving_barang rb');
        $this->db->join('barang b','b.id_barang = rb.id_barang','left');
        $this->db->where('id_receiving',$id_rcv);
        return $this->db->get();
    }

    private function data_detail_po($condition = array()) {
        $id_receiving= $this->input->post("id_bbm");
        if(!empty($id_receiving)){
            $condition["e.id_receiving = '$id_receiving' "]=null;
        }
        $this->db->select('*,b.id_receiving_qty,count(f.kd_unik) as qty_doc,b.jumlah_barang as jumlah_po,c.id_barang as item_id');
        $this->db->from($this->table .' e');//receiving
        $this->db->join($this->table3  . ' a','e.id_po = a.id_po');//po
        $this->db->join($this->po_barang . ' b',' b.id_po = a.id_po','left');//po_barang
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');//barang
        $this->db->join($this->table6 . ' d',' d.id_satuan = c.id_satuan','left');
        $this->db->join($this->table4 . ' f',' e.id_receiving = f.id_receiving and f.id_barang = c.id_barang','LEFT');//receiving_barang
        $this->db->group_by('c.id_barang');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail_supplier($condition = array()) {
        $this->db->select('*');
        $this->db->from($this->table3  . ' a');
        //$this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');       
        $this->db->join($this->table2 . ' f',' a.id_supplier = f.id_supplier','left');
        $this->db->join($this->table8 . ' g',' g.id_supplier = f.id_supplier','left');
        $this->db->join($this->table5 . ' c',' c.id_barang = g.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function checkExist($kd_unik){
        $this->db->select('kd_unik');
        $this->db->from('receiving_barang');
        $this->db->where('kd_unik',$kd_unik);
        $hasil = $this->db->get();
        if($hasil->num_rows()>0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function cek_rcv_qty($id_barang,$id_receiving){
        $this->db->select('kd_unik');
        $this->db->from('receiving_barang');
        $this->db->where('id_receiving',$id_receiving);
        $this->db->where('id_barang',$id_barang);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function empty_rcv_qty($id_barang,$id_receiving){
        $this->db->where('id_receiving',$id_receiving);
        $this->db->where('id_barang',$id_barang);
        $this->db->delete('receiving_barang');
    }

    public function update_rcv_qty($id_receiving,$id_barang,$qty){
        $condition['id_receiving'] = $id_receiving;
        $condition['id_barang'] = $id_barang;
        $this->db->from('receiving_qty rq');
        $this->db->where($condition);
        $cek_eksis = $this->db->get();
        if($cek_eksis->num_rows()>0){
            $update = array('qty'=>$qty);
            $this->db->where($condition);
            $this->db->update('receiving_qty',$update);
        }else{
            $insert = array('id_receiving'=>$id_receiving,
                'id_barang' => $id_barang,
                'qty' => $qty);
            $this->db->insert('receiving_qty',$insert);
        }
    }

    public function data_detail_bbm(){
        $id_receiving= $this->input->post("id_bbm");
        if(!empty($id_receiving)){
            $condition["r.id_receiving = '$id_receiving' "]=null;
        }
        $this->db->select('*, rq.qty as qty_doc');
        $this->db->from('receiving r');
        $this->db->join('purchase_order po','po.id_po = r.id_po','left');
        $this->db->join('purchase_order_barang pob','pob.id_po = po.id_po','left');
        $this->db->join('receiving_qty rq','rq.id_receiving = r.id_receiving and pob.id_barang = rq.id_barang','left');
        //$this->db->join('receiving_qtya rq','rq.id_receiving = r.id_receiving','left');
        $this->db->join('v_receiving vr','rq.id_receiving = vr.id_receiving and vr.id_barang = rq.id_barang','left');
        $this->db->join('barang b','b.id_barang = pob.id_barang','left');
        $this->db->join('satuan s','s.id_satuan = b.id_satuan','left');
        $this->db->where($condition);
        return $this->db;
    }

    public function data_table_detail(){
        $id_do = $this->input->post('id_bbm');
        $id_receiving = $id_do;
        $id_barang_arr = $this->input->post('id_barang');
        //$id_po = $this->input->post('id_po');

        if(!empty($id_barang_arr)){
            foreach ($id_barang_arr as $id_barang) {
                $kd_barang = $this->get_kd_barang($id_barang);
                $receiving = $this->input->post('receiving_'.$id_receiving.'_'.$id_barang);
                $tgl_exp = hgenerator::switch_tanggal($this->input->post('tanggal_exp_'.$id_receiving.'_'.$id_barang));
                $this->update_rcv_qty($id_receiving,$id_barang,$receiving);                
            }
        }
        //-----------end save data----------------
        // Total Record
        $total = $this->data_detail_bbm()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail_bbm()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $i=0;
            $id = $value->id_po_barang;
            $id_barang = $value->id_barang;
            if(!empty($value->qty_doc)){
                $discrepenci = $value->qty_doc - $value->qty_received;
            }else{
                $discrepenci = 0;
            }
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'receiving' => !empty($value->qty_doc) ? $value->qty_doc.' '. $value->nama_satuan : form_input("receiving_".$id_do.'_'.$id_barang,!empty($value->qty_doc) ?$value->qty_doc:'','id="receiving_'.$id_do."_".$id_barang.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'). form_hidden('id_barang[]',$id_barang) ,
                'received' =>$value->qty_received,
                'discrepenci' => $discrepenci
            );
            $i++;
        }

        return array('rows' => $rows, 'total' => $total);
    }
    

    public function edit_data_table_detail(){
        $id_do = $this->input->post('id_bbm');
        $id_receiving = $id_do;
        $id_barang_arr = $this->input->post('id_barang');
        //$id_po = $this->input->post('id_po');

/*        if(!empty($id_barang_arr)){
            foreach ($id_barang_arr as $id_barang) {
                $kd_barang = $this->get_kd_barang($id_barang);
                $receiving = $this->input->post('receiving_'.$id_receiving.'_'.$id_barang);
                $tgl_exp = hgenerator::switch_tanggal($this->input->post('tanggal_exp_'.$id_receiving.'_'.$id_barang));

                for($no=1;$no<=$receiving;$no++){

                    $cek_eksis = $this->checkExist(trim($kd_barang).'/'.$id_receiving.'/'.$no);
                    //if(!empty($jumlah_barang)&&!empty($harga_barang)&&$checkExist==0){
                    if($cek_eksis == FALSE){
                        $data_insert = array(
                            'id_receiving' => $id_receiving,
                            'tgl_exp'=> $tgl_exp,
                            'kd_unik' => trim($kd_barang).'/'.$id_receiving.'/'.$no,
                            'tgl_in' =>date('YYYY-mm-dd'),
                            'id_barang' =>$id_barang,
                        );
                        $this->db->insert($this->table4,$data_insert);
                    }

                }
            }
        }*/
        //-----------end save data----------------
        // Total Record
        $total = $this->data_detail_bbm()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail_bbm()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $i=0;
            $id = $value->id_po_barang;
            $id_barang = $value->id_barang;
            if(!empty($value->qty_doc)){
                $discrepenci = $value->qty_doc - $value->qty_received;
            }else{
                $discrepenci = 0;
            }
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'receiving' => form_input("receiving_".$id_do.'_'.$id_barang,!empty($value->qty_doc) ?$value->qty_doc:'','id="receiving_'.$id_do."_".$id_barang.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'). form_hidden('id_barang[]',$id_barang),
                'received' =>$value->qty_received,
                'discrepenci' => $discrepenci
            );
            $i++;
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function get_data_print_doc_rcv($id) {
        $condition['r.id_receiving'] = $id;
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");

        $this->db->select('kd_barang,nama_barang, qty, kd_receiving, tanggal_receiving, kd_po, kd_supplier, nama_supplier,nama_kategori, nama_satuan,r.*,po.*');
        $this->db->from('receiving r');
        $this->db->join('purchase_order po',' r.id_po = po.id_po','left');
        //$this->db->join('purchase_order_barang pob','po.id_po = pob.id_po','left');
        $this->db->join('receiving_qty rq','rq.id_receiving = r.id_receiving','left');
        $this->db->join('supplier s','po.id_supplier = s.id_supplier','left');
        $this->db->join('barang b','b.id_barang = rq.id_barang','left');
        $this->db->join('kategori k','k.id_kategori = b.id_kategori','left');
        $this->db->join('satuan sat','sat.id_satuan = b.id_satuan','left');
        $this->db->order_by('b.id_barang ASC');
        $this->db->where_condition($condition);

        return $this->db->get();
    }

    public function get_qty_print($id_rcv){
        $this->db->select('sum(qty) as qty_print');
        $this->db->from('receiving_qty');
        $this->db->group_by('id_receiving');
        $this->db->where('id_receiving',$id_rcv);
        $hasil = $this->db->get()->row();
        return $hasil->qty_print;
    }

    public function get_inc_date(){
        $this->db->select('sn_last_date,inc_sn');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        return $this->db->get()->row();
    }

    public function get_next_barcode(){
        $curent_date = date('Y-m-d');
        $get_inc_date = $this->get_inc_date();
        if($curent_date != $get_inc_date->sn_last_date){
            $update = array('sn_last_date'=>$curent_date,'inc_sn'=>1);
            $this->db->where('id_inc',1);
            $this->db->update('m_increment',$update);
            $print_sn = 1;
        }else{
            $last_sn = $get_inc_date->inc_sn;
            $print_sn = $last_sn + 1;
            $update = array('sn_last_date'=>$curent_date,'inc_sn'=>$print_sn);
            $this->db->where('id_inc',1);
            $this->db->update('m_increment',$update);
        }
        $inc_lenght = strlen($print_sn);
        $leading_zero = '';
        for($i=$inc_lenght;$i<6;$i++){
            $leading_zero = $leading_zero.'0';
        }
        $full_sn = date('ymd').$leading_zero.$print_sn;
        $insert = array('kd_unik'=>$full_sn);
        $this->db->insert('receiving_barang',$insert);
        return $full_sn;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_receiving= $this->input->post("kd_receiving");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_receiving = $this->input->post('id_receiving');
        if(!empty($kd_receiving)){
            $condition["a.kd_receiving like '%$kd_receiving%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_receiving)){
            $condition["a.id_receiving"]=$id_receiving;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_receiving_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_receiving = b.id_receiving');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_receiving'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = $this->session->userdata('tahun_aktif');
            if(empty($tahun_aktif))
                $tahun_aktif = date('Y');
        }else{
            $set_tahun_aktif = array('tahun_aktif' => $tahun_aktif);
            $this->session->set_userdata($set_tahun_aktif);
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_receiving >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_receiving <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            //---------awal button action---------
            $id = $value->id_receiving;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">';
            if ($this->access_right->otoritas('print')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/print_doc_rcv/' . $id)));
                $action .= '</li>';

            }

            if ($this->access_right->otoritas('print')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print Barcode', array('id' => 'button-print-barcode-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/print_barcode/' . $id)));
                //$action .= anchor(base_url('bbm/print_barcode/' . $id), '<i class="fa fa-print"></i>Print Barcode');
                $action .= '</li>';

            }
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';
            //------------end button action---------------
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_receiving,
                    'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                    'kd_receiving' => anchor(null, $value->kd_receiving, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) ,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'vehicle_plate'=> $value->vehicle_plate,
                    'remark'=>$value->remark,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_receiving,
                    'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                    'kd_receiving' => anchor(null, $value->kd_receiving, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) ,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'vehicle_plate'=> $value->vehicle_plate,
                    'remark'=>$value->remark,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }   

    public function data_table_excel() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_receiving >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_receiving <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $rows[] = array(
                'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                'kd_receiving' => $value->kd_receiving,
                'kd_po' => $value->kd_po,
                'supplier' => $value->nama_supplier,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }     



   

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_receiving' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table4, array('id_receiving' => $id));
        return $this->db->delete($this->table, array('id_receiving' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Pilih Kode BBM--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_receiving] = $row->kd_receiving ;
        }
        return $options;
    }

    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_receiving_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    
}

?>
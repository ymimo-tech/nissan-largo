<?php
class packing_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;

	public function getRecList($param=array()){
        $result = array();
		$requestData = $_REQUEST;
		
		$packing_detail = $this->db->select('*')->from('header_oird')->where('packing_number',$param['packing_number'])->get();
		
		$dn_detail = $this->db->select('*')->from('delivery_notes')->where('name',$param['delivery_note'])->get();
		
		if($dn_detail->num_rows() > 0) {
			$dn_id = $dn_detail->row_array()['id'];
			
			// $picking_destination = $this->db->select('outb.destination_id')->from('picking_list_outbound plo')->join('outbound outb','plo.id_outbound=outb.id')->join('delivery_pl dp','dp.pl_id=plo.pl_id')->where('dp.dn_id',$dn_id)->get()->row_array()['destination_id'];
			$picking_destination = $this->db->select('outb.destination_id')->from('picking_list_outbound plo')->join('outbound outb','plo.id_outbound=outb.id')->join('picking_recomendation pr','pr.picking_id=plo.pl_id')->join('delivery_pl dp','dp.pl_id=pr.id')->where('dp.dn_id',$dn_id)->get()->row_array()['destination_id'];
			
			if($packing_detail->num_rows() > 0) {
				$packing_destination = $packing_detail->row_array()['destination_id'];
				if($packing_destination != $picking_destination) {
					$destination_detail = $this->db->select('destination_code,destination_name')->from('destinations')->where('destination_id',$packing_destination)->get()->row_array();
					
					$result = array(
						"status"			=> false,
						"message"			=> "This packing number already assigned to destination " . $destination_detail['destination_code'] . "-" . $destination_detail['destination_name'],
						"draw"            	=> 1,
						"recordsTotal"    	=> 0,
						"recordsFiltered" 	=> 0,
						"data"            	=> [],
					);
					return $result;
				}
			}
			
			$columns = array(
				0 => 'itm.id',
				1 => '',
				2 => 'kd_barang',
				3 => 'nama_barang',
				4 => 'jumlah_barang',
				5 => 'actual_picked_qty',
				6 => 'status'
			);

			$this->db->start_cache();

			// $getPickingByDN = $this->db->select('pl_id')->from('delivery_pl')->where('dn_id',$dn_id)->get()->result_array();
			$getPickingByDN = $this->db->select('picking_id AS pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','pr.id=dp.pl_id')->where('dp.dn_id',$dn_id)->get()->result_array();
			$picking_ids = '';
			for($i=0;$i<count($getPickingByDN);$i++){
				$picking_ids .= $getPickingByDN[$i]['pl_id'].',';
			}
			$picking_ids = substr($picking_ids, 0, strlen($picking_ids)-1);
			
			$query_count = "SELECT item_id FROM picking_recomendation WHERE picking_id IN (" . $picking_ids . ")";
			
			$getCount = $this->db->query($query_count);

			$totalData = $getCount->num_rows();

			// $query_select = "SELECT * FROM
			// 				(SELECT
			// 					itm.code AS item_code, itm.name AS item_name, outb.id AS outbound_id, outb.code AS outbound_code, d.destination_code, d.destination_name, d.destination_address, p.pl_id AS picking_id, p.name AS picking_code, unt.name AS uom,
			// 					(SELECT COALESCE(SUM(prx.qty),0) FROM picking_recomendation prx WHERE prx.id = dp.pl_id AND prx.item_id = pq.item_id) AS qty_picked,
			// 					(SELECT COALESCE(SUM(oirdx.qty),0) FROM outbound_item_receiving_details oirdx JOIN item_receiving_details irdx ON irdx.id = oirdx.id_item_receiving_details WHERE oirdx.picking_id = p.pl_id AND irdx.item_id = pq.item_id) AS qty_packed
			// 				FROM
			// 					pickings p
			// 				--JOIN
			// 					--delivery_pl dp ON dp.pl_id = p.pl_id
			// 				JOIN
			// 					picking_recomendation pr ON p.pl_id = pr.picking_id
			// 				JOIN
			// 					delivery_pl dp ON dp.pl_id = pr.id
			// 				JOIN
			// 					picking_qty pq ON p.pl_id = pq.picking_id
			// 				JOIN
			// 					items itm ON pq.item_id = itm.id
			// 				JOIN
			// 					picking_list_outbound plo ON plo.pl_id = p.pl_id
			// 				JOIN
			// 					outbound outb ON outb.id = plo.id_outbound
			// 				JOIN
			// 					units unt ON itm.unit_id = unt.id
			// 				JOIN
			// 					destinations d ON d.destination_id = outb.destination_id
			// 				WHERE
			// 					dp.dn_id = ".$dn_id.") a
			// 				WHERE qty_picked > 0
			// 				;";

			$query_select = "select i2.code as item_code, i2.name item_name, o2.id outbound_id, 
			o2.code outbound_code, d2.destination_code, d2.destination_name, d2.destination_address, p.pl_id as picking_id, p.name as picking_code, i2.unit_id, u2.name uom, pr.qty as qty_picked, 
			oird.qty as qty_packed1,
			coalesce((select sum(oird2.qty) from outbound_item_receiving_details oird2 where pr_id = pr.id and dn_id = ".$dn_id." group by pr_id, dn_id),0) as qty_packed
			from delivery_pl dp 
			left join picking_recomendation pr on dp.pl_id = pr.id
			left join items i2 on i2.id = pr.item_id
			left join outbound o2 on o2.id = pr.outbound_id 
			left join destinations d2 on d2.destination_id = o2.destination_id 
			left join pickings p on p.pl_id = pr.picking_id 
			left join outbound_item_receiving_details oird on oird.pr_id = pr.id
			left join units u2 on u2.id = i2.unit_id 
			where dp.dn_id =".$dn_id;
			
			$getSelect = $this->db->query($query_select);

			$row = $getSelect->result_array();
			$totalFiltered = count($row);

			$data = array();
			$scanned = $totalFiltered>0?1:0;
			$packing_numbers='';
			$uom='';
			$destinations = array();
			for($i=0;$i<$totalFiltered;$i++){

				$nested = array();
				$nested[] = $_REQUEST['start'] + ($i + 1);
				$nested[] = $row[$i]['item_code'];
				$nested[] = $row[$i]['item_name'];
				$nested[] = $row[$i]['outbound_code'];
				$nested[] = $row[$i]['picking_code'];
				$nested[] = $row[$i]['qty_picked'].' '.$row[$i]['uom'];
				$nested[] = $row[$i]['qty_packed'].' '.$row[$i]['uom'];

				$data[] = $nested;
				$uom = $row[$i]['uom'];
				$destinations = array(
					"code" 		=> $row[$i]['destination_code'],
					"name" 		=> $row[$i]['destination_name'],
					"address" 	=> $row[$i]['destination_address']
				);
			}

			$this->load->library('enc');

			$result = array(
				"status"			=> true,
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalFiltered ),
				"recordsFiltered" => intval( $totalData ),
				"data"            => $data,
				"scanned"         => $scanned,
				"enc"             => $this->enc->encode($param['delivery_note']),
				"uom"             => $uom,
				"destinations"	  => $destinations,
			);
		} else {
			$result = array(
				"status"			=> false,
				"message"			=> "This " . $param['delivery_note'] . " is not exist.",
				"draw"            	=> 1,
				"recordsTotal"    	=> 0,
				"recordsFiltered" 	=> 0,
				"data"            	=> [],
			);
		}

		return $result;
	}

    public function getRecList_BAK($param=array()){
        $result = array();
		$requestData = $_REQUEST;
		
		$packing_detail = $this->db->select('*')->from('outbound_item_receiving_details')->where('packing_number',$param['packing_number'])->get();
		$picking_detail = $this->db->select('pl_id')->from('pickings')->where('name',$param['picking_list'])->get();
		
		if($picking_detail->num_rows() > 0) {
			$picking_id = $picking_detail->row_array()['pl_id'];
			$picking_destination = $this->db->select('outb.destination_id')->from('picking_list_outbound plo')->join('outbound outb','plo.id_outbound=outb.id')->where('plo.pl_id',$picking_id)->get()->row_array()['destination_id'];
			
			if($packing_detail->num_rows() > 0) {
				$packing_destination = $packing_detail->row_array()['destination_id'];
				if($packing_destination != $picking_destination) {
					$destination_detail = $this->db->select('destination_code,destination_name')->from('destinations')->where('destination_id',$packing_destination)->get()->row_array();
					
					$result = array(
						"status"			=> false,
						"message"			=> "This packing number already assigned to destination " . $destination_detail['destination_code'] . "-" . $destination_detail['destination_name'],
						"draw"            	=> 1,
						"recordsTotal"    	=> 0,
						"recordsFiltered" 	=> 0,
						"data"            	=> [],
					);
					return $result;
				}
			}
			
			$columns = array(
				0 => 'itm.id',
				1 => '',
				2 => 'kd_barang',
				3 => 'nama_barang',
				4 => 'jumlah_barang',
				5 => 'actual_picked_qty',
				6 => 'status'
			);

			$this->db->start_cache();

			$query_count = "SELECT item_id FROM picking_recomendation WHERE picking_id = " . $picking_id;
			
			$getCount = $this->db->query($query_count);

			$totalData = $getCount->num_rows();

			$query_select = "SELECT
								itm.code AS item_code, itm.name AS item_name, outb.id AS outbound_id, outb.code AS outbound_code, d.destination_code, d.destination_name, d.destination_address, p.pl_id AS picking_id, p.name AS picking_code, unt.name AS uom,
								(SELECT COALESCE(SUM(prx.qty),0) FROM picking_recomendation prx WHERE prx.picking_id = p.pl_id AND prx.item_id = pq.item_id) AS qty_picked,
								(SELECT COALESCE(SUM(oirdx.qty),0) FROM outbound_item_receiving_details oirdx JOIN item_receiving_details irdx ON irdx.id = oirdx.id_item_receiving_details WHERE oirdx.picking_id = p.pl_id AND irdx.item_id = pq.item_id) AS qty_packed
							FROM
								pickings p
							JOIN
								picking_qty pq ON p.pl_id = pq.picking_id
							JOIN
								items itm ON pq.item_id = itm.id
							JOIN
								picking_list_outbound plo ON plo.pl_id = p.pl_id
							JOIN
								outbound outb ON outb.id = plo.id_outbound
							JOIN
								units unt ON itm.unit_id = unt.id
							JOIN
								destinations d ON d.destination_id = outb.destination_id
							WHERE
								p.pl_id = ".$picking_id;
			
			$getSelect = $this->db->query($query_select);

			$row = $getSelect->result_array();
			$totalFiltered = count($row);

			$data = array();
			$scanned = $totalFiltered>0?1:0;
			$packing_numbers='';
			$uom='';
			$destinations = array();
			for($i=0;$i<$totalFiltered;$i++){

				$nested = array();
				$nested[] = $_REQUEST['start'] + ($i + 1);
				$nested[] = $row[$i]['item_code'];
				$nested[] = $row[$i]['item_name'];
				$nested[] = $row[$i]['outbound_code'];
				$nested[] = $row[$i]['picking_code'];
				$nested[] = $row[$i]['qty_picked'].' '.$row[$i]['uom'];
				$nested[] = $row[$i]['qty_packed'].' '.$row[$i]['uom'];

				$data[] = $nested;
				$uom = $row[$i]['uom'];
				$destinations = array(
					"code" 		=> $row[$i]['destination_code'],
					"name" 		=> $row[$i]['destination_name'],
					"address" 	=> $row[$i]['destination_address']
				);
			}

			$this->load->library('enc');

			$result = array(
				"status"			=> true,
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalFiltered ),
				"recordsFiltered" => intval( $totalData ),
				"data"            => $data,
				"scanned"         => $scanned,
				"enc"             => $this->enc->encode($param['picking_list']),
				"uom"             => $uom,
				"destinations"	  => $destinations,
			);
		} else {
			$result = array(
				"status"			=> false,
				"message"			=> "This " . $param['picking_list'] . " is not exist.",
				"draw"            	=> 1,
				"recordsTotal"    	=> 0,
				"recordsFiltered" 	=> 0,
				"data"            	=> [],
			);
		}

		return $result;
	}

    public function get_pc_code(){
        $this->db->select('pre_code_pc,inc_pc');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_pc.rand(3,999).date('ymd').str_pad($result->inc_pc,4,0,STR_PAD_LEFT);
    }

    public function add_pc_code(){
        $this->db->set('inc_pc',"inc_pc+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }
	
	public function getItemQtyByPicking($param=array()){
		$result = array();
		
		$picking_detail = $this->db->select('pl_id')->from('pickings')->where('name',$param['picking_list'])->get();
		$item_detail = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get();
		
		if($picking_detail->num_rows() > 0 && $item_detail->num_rows() > 0) {
			$query_get = "SELECT COALESCE(SUM(qty),0) AS picked_qty FROM picking_recomendation WHERE item_id = ".$item_detail->row_array()['id']." AND picking_id = ".$picking_detail->row_array()['pl_id'];
			$get = $this->db->query($query_get)->row_array()['picked_qty'];
			
			$result = array(
				"status"			=> true,
				"qty_picked"		=> $get,
			);			
		} else {
			$result = array(
				"status"			=> false
			);
		}
		
		return $result;
	}
	
	public function getItemQtyByDN($param=array()){
		$result = array();
		
		$dn_detail = $this->db->select('*')->from('delivery_notes')->where('name',$param['delivery_note'])->get();
		$item_detail = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get();
		
		if($dn_detail->num_rows() > 0 && $item_detail->num_rows() > 0) {
			$dn_id = $dn_detail->row_array()['id'];
			$getPickingByDN = $this->db->select('picking_id AS pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','pr.id=dp.pl_id')->where('dn_id',$dn_id)->get()->result_array();
			$picking_ids = '';
			for($i=0;$i<count($getPickingByDN);$i++){
				$picking_ids .= $getPickingByDN[$i]['pl_id'].',';
			}
			$picking_ids = substr($picking_ids, 0, strlen($picking_ids)-1);
			
			$query_get = "SELECT COALESCE(SUM(qty),0) AS picked_qty FROM picking_recomendation WHERE item_id = ".$item_detail->row_array()['id']." AND picking_id IN (".$picking_ids.")";
			$get = $this->db->query($query_get)->row_array()['picked_qty'];
			
			$result = array(
				"status"			=> true,
				"qty_picked"		=> $get,
			);			
		} else {
			$result = array(
				"status"			=> false
			);
		}
		
		return $result;
	}
	
	public function isPCLoaded($param=array()){
		$result = array();
		
		$packing_number = $param['packing_number'];
		$get = $this->db->select('*')->from('outbound_item_receiving_details')->where('packing_number',$packing_number)->where('shipping_id is not null')->get();
		if($get->num_rows() > 0) {
			$result = array(
				"status"			=> false
			);
		} else {
			$result = array(
				"status"			=> true
			);
		}
		
		return $result;
	}
	
	public function isCarton($param=array()){
		$result = array();
		
		$carton = $param['carton'];
		$get = $this->db->select('ird.id')->from('item_receiving_details ird')->join('items itm','ird.item_id=itm.id')->where('unique_code',$carton)->where('itm.code','CARTON')->get();
		if($get->num_rows() > 0) {
			$result = array(
				"status"			=> true
			);
		} else {
			$result = array(
				"status"			=> false
			);
		}
		
		return $result;
	}
	
	public function get_pc_detail($pc_code) {
		$result = array();
		
		$get =
			$this->db->select(['ho.packing_number','coalesce(ho.weight,0)','coalesce(ho.volume,0)','dest.*'])
			->from('header_oird ho')
			->join('destinations dest','ho.destination_id = dest.destination_id')
			->where('packing_number',$pc_code)->get()->row_array();
		
		if(count($get) > 0) {
			$result = $get;
		}
		
		return $result;
	}
	
	public function savePacking_old($param=array()){
		$result = array();
		
		$id_user = $this->session->userdata('user_id');
		
		$packing_number 	= $param['packing_number'];
		$carton 			= $param['carton'];
		$quantity			= $param['quantity'];
		$weight 			= $param['weight'];
		$volume 			= $param['volume'];
		
		$dn_id = $this->db->select('id')->from('delivery_notes')->where('name',$param['delivery_note'])->get()->row_array()['id'];
		//$outbound_id = $this->db->select('id_outbound')->from('picking_list_outbound')->where('pl_id',$picking_id)->get()->row_array()['id_outbound'];
		$item_id = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get()->row_array()['id'];

		$pr = $this->db->select('pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','dp.pl_id = pr.id','left')->join('items itm','itm.id = pr.item_id','left')->where('dp.dn_id',$dn_id)->where('itm.id',$item_id)->get()->row_array()['pl_id'];

		$destination_id = $this->db->select('destination_id')->from('destinations')->where('destination_code',$param['destination_code'])->get()->row_array()['destination_id'];
		
		$query_get = "SELECT
							pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
						FROM
							picking_recomendation pr
						JOIN
							delivery_pl dp ON dp.pl_id = pr.id
						LEFT JOIN
							outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
						LEFT JOIN 
							header_oird hoird on hoird.id = oird.packing_number
						WHERE
							dp.dn_id = " . $dn_id . "
						AND
							(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
						AND
							item_id = " . $item_id;
		// dd($query_get);
		$get = $this->db->query($query_get);
		$getCount = $get->num_rows();
		$getList = $get->result_array();
		
		if($getCount > 0) {
			$query_get = "SELECT
								SUM(pr.qty) AS qty_picked, COALESCE(SUM(oird.qty), 0) AS qty_packed
							FROM
								picking_recomendation pr
							JOIN
								delivery_pl dp ON dp.pl_id = pr.id
							LEFT JOIN
								outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
							LEFT JOIN 
								header_oird hoird on hoird.id = oird.packing_number
							WHERE
								dp.dn_id = " . $dn_id . "
							AND
								(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
							AND
								item_id = " . $item_id;
	
			$get = $this->db->query($query_get);
			$getList = $get->row_array();
			
			if(($getList['qty_picked'] - $getList['qty_packed']) >= $quantity) {
				$counter = ceil($quantity);
				
				for($y=0;$y<$counter;$y++) {
					if($quantity >= 1){
						$qtyx = 1;
						$quantity = $quantity-1;
					} else {
						$qtyx = $quantity;
					}
					
					$query_get = "SELECT
									pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed, pr.picking_id, pr.outbound_id
								FROM
									picking_recomendation pr
								JOIN
									delivery_pl dp ON dp.pl_id = pr.id
								LEFT JOIN
									outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
								LEFT JOIN 
									header_oird hoird on hoird.id = oird.packing_number
								WHERE
									dp.dn_id = " . $dn_id . "
								AND
									(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
								AND
									(pr.qty - COALESCE(oird.qty,0)) >= ".$qtyx."
								AND
									item_id = " . $item_id;
				
					$get = $this->db->query($query_get);
					$getList = $get->row_array();
					
					$ird_id = $getList['item_receiving_detail_id'];
					$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('packing_number',$packing_number)->get();
					
					$header_id = $this->db->select('id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array()[0]['id'];

					if(isset($header_id)) {
						$qtyExist = $isExist->row_array()['qty'];
						$this->db
								->set('qty',$qtyExist + $qtyx)
								->where('packing_number',$header_id)
								->where('id_item_receiving_details',$ird_id)
								->update('outbound_item_receiving_details');
					} else {
						$query_insert = "
									INSERT INTO header_oird(destination_id, packing_number, user_id, time) VALUES
									(".$destination_id.",'".$packing_number."',".$id_user.",now())
									";
									
						$this->db->query($query_insert);

						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id,dn_id,pr_id) VALUES
									(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$header_id."',1,".$qtyx.",".$id_user.",".$dn_id.",".$pr.")
									";
									
						$this->db->query($query_insert);
					}
				}
				
				$this->db
						->set('weight',$weight)
						->set('volume',$volume)
						->where('packing_number',$header_id)
						->update('outbound_item_receiving_details');
					
				$result = array(
					"status"		=> true,
					"message"		=> "Success.",
				);
			} else {
				$result = array(
					"status"		=> false,
					"message"		=> "Quantity is not enough.",
				);
			}
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Each of this item (".$param['item_code'].") is already packed.",
			);
		}
		
		return $result;
	}

	public function savePacking($param=array()){
		$result = array();
		
		$id_user = $this->session->userdata('user_id');
		// $id_user = $this->db->select('id')->from('users')->where('user_name',$param['user'])->get()->row_array()['id'];
		
		$packing_number 	= $param['packing_number'];
		$carton 			= $param['carton'];
		$quantity			= $param['quantity'];
		$weight 			= $param['weight'];
		$volume 			= $param['volume'];
		
		$dn_id = $this->db->select('id')->from('delivery_notes')->where('name',$param['delivery_note'])->get()->row_array()['id'];
		//$outbound_id = $this->db->select('id_outbound')->from('picking_list_outbound')->where('pl_id',$picking_id)->get()->row_array()['id_outbound'];
		$item_id = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get()->row_array()['id'];

		$pr = $this->db->select('pl_id')->from('delivery_pl dp')->join('picking_recomendation pr','dp.pl_id = pr.id','left')->join('items itm','itm.id = pr.item_id','left')->where('dp.dn_id',$dn_id)->where('itm.id',$item_id)->get()->row_array()['pl_id'];
		
		$destination_id = $this->db->select('destination_id')->from('destinations')->where('destination_code',$param['destination_code'])->get()->row_array()['destination_id'];

		$query_get = "SELECT
							pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
						FROM
							picking_recomendation pr
						JOIN
							delivery_pl dp ON dp.pl_id = pr.id
						LEFT JOIN
							outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
						LEFT JOIN 
							header_oird hoird on hoird.id = oird.packing_number
						WHERE
							dp.dn_id = " . $dn_id . "
						AND
							(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
						AND
							item_id = " . $item_id;
		$get = $this->db->query($query_get);
		$getCount = $get->num_rows();
		$getList = $get->result_array();
		
		$sql_sum = "select coalesce(sum(oird2.qty),0) as qty from outbound_item_receiving_details oird2 where pr_id = ".$pr." and dn_id = ".$dn_id." group by pr_id, dn_id";
		$sum_packed = $this->db->query($sql_sum)->row_array();
		if(isset($sum_packed['qty'])){
			$packed = $sum_packed['qty'];
		}else{
			$packed = 0;
		}

		$sql_pr = "select coalesce(sum(pr.qty),0) as qty from delivery_pl dp 
		left join picking_recomendation pr on dp.pl_id = pr.id
		where pr.id = ".$pr."
		group by pr.id";
		$sum_pr = $this->db->query($sql_pr)->row_array();
		if(isset($sum_pr['qty'])){
			$spr = $sum_pr['qty'];
		}else{
			$spr = 0;
		}

		$disp = $spr - ($packed+$quantity);
		
		if($getCount > 0 && $disp >= 0) {
			$query_get = "SELECT
								SUM(pr.qty) AS qty_picked, COALESCE(SUM(oird.qty), 0) AS qty_packed
							FROM
								picking_recomendation pr
							JOIN
								delivery_pl dp ON dp.pl_id = pr.id
							LEFT JOIN
								outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
							LEFT JOIN 
								header_oird hoird on hoird.id = oird.packing_number
							WHERE
								dp.dn_id = " . $dn_id . "
							AND
								(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
							AND
								item_id = " . $item_id;
	
			$get = $this->db->query($query_get);
			$getList = $get->row_array();

			if(($getList['qty_picked'] - $getList['qty_packed']) >= $quantity) {
				$counter = ceil($quantity);
				
				$this->db->trans_start();
				for($y=0;$y<$counter;$y++) {
					if($quantity >= 1){
						$qtyx = 1;
						$quantity = $quantity-1;
					} else {
						$qtyx = $quantity;
					}
					
					$query_get = "SELECT
									pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed, pr.picking_id, pr.outbound_id
								FROM
									picking_recomendation pr
								JOIN
									delivery_pl dp ON dp.pl_id = pr.id
								LEFT JOIN
									outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details AND oird.picking_id = pr.picking_id
								LEFT JOIN 
									header_oird hoird on hoird.id = oird.packing_number
								WHERE
									dp.dn_id = " . $dn_id . "
								AND
									(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
								AND
									(pr.qty - COALESCE(oird.qty,0)) >= ".$qtyx."
								AND
									item_id = " . $item_id;
				
					$get = $this->db->query($query_get);
					$getList = $get->row_array();

					
					
					$ird_id = $getList['item_receiving_detail_id'];
					
					$header_id = $this->db->select('id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array();
					
					
					if(count($header_id)>0) {
						$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('packing_number',$header_id[0]['id'])->get();
						// dd($isExist);
						$qtyExist = $isExist->row_array()['qty'];
						$this->db
								->set('qty',$qtyExist + $qtyx)
								->where('packing_number',$header_id[0]['id'])
								->where('id_item_receiving_details',$ird_id)
								->update('outbound_item_receiving_details');
					} else {
						// dd('masuk');
						$query_insert_header = "
									INSERT INTO header_oird(destination_id, packing_number, user_id, time) VALUES
									(".$destination_id.",'".$packing_number."',".$id_user.",now())
									";
									
						$this->db->query($query_insert_header);

						$header_id = $this->db->select('id')->from('header_oird')->where('packing_number',$packing_number)->get()->result_array();

						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id,dn_id,pr_id) VALUES
									(".$ird_id.",".$getList['picking_id'].",".$getList['outbound_id'].",".$destination_id.",'".$header_id[0]['id']."',1,".$qtyx.",".$id_user.",".$dn_id.",".$pr.")
									";
									
						$this->db->query($query_insert);
					}
				}
				
				$this->db
						->set('weight',$weight)
						->set('volume',$volume)
						// ->where('packing_number',$header_id[0]['id'])
						->where('id',$header_id[0]['id'])
						->update('header_oird');
					
				$result = array(
					"status"		=> true,
					"message"		=> "Success.",
				);
				$this->db->trans_complete();
			} else {
				$result = array(
					"status"		=> false,
					"message"		=> "Quantity is not enough.",
				);
			}
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Each of this item (".$param['item_code'].") is already packed.",
			);
		}
		
		return $result;
	}
	
	public function savePacking_BAK($param=array()){
		$result = array();
		
		$id_user = $this->session->userdata('user_id');
		
		$packing_number 	= $param['packing_number'];
		$carton 			= $param['carton'];
		$quantity			= $param['quantity'];
		$weight 			= $param['weight'];
		$volume 			= $param['volume'];
		
		$picking_id = $this->db->select('pl_id')->from('pickings')->where('name',$param['picking_list'])->get()->row_array()['pl_id'];
		$outbound_id = $this->db->select('id_outbound')->from('picking_list_outbound')->where('pl_id',$picking_id)->get()->row_array()['id_outbound'];
		$item_id = $this->db->select('id')->from('items')->where('code',$param['item_code'])->get()->row_array()['id'];
		$destination_id = $this->db->select('destination_id')->from('destinations')->where('destination_code',$param['destination_code'])->get()->row_array()['destination_id'];
		
		$query_get = "SELECT
							pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
						FROM
							picking_recomendation pr
						LEFT JOIN
							outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details
						WHERE
							pr.picking_id = " . $picking_id . "
						AND
							(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
						AND
							item_id = " . $item_id;
		
		$get = $this->db->query($query_get);
		$getCount = $get->num_rows();
		$getList = $get->result_array();
		
		if($getCount > 0) {
			if($getCount == 1) {
				if($getList[0]['qty_picked'] < $quantity) {
					$result = array(
						"status"		=> false,
						"message"		=> "Quantity is not enough.",
					);
				} else {
					$ird_id = $getList[0]['item_receiving_detail_id'];
					$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('packing_number',$packing_number)->get();
					
					if($isExist->num_rows() > 0) {
						$qtyExist = $isExist->row_array()['qty'];
						$this->db
								->set('qty',$qtyExist + $quantity)
								->where('packing_number',$packing_number)
								->where('id_item_receiving_details',$ird_id)
								->update('outbound_item_receiving_details');
					} else {
						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, id_outbound, destination_id, packing_number, inc, qty, user_id) VALUES
									(".$ird_id.",".$picking_id.",".$outbound_id.",".$destination_id.",'".$packing_number."',1,".$quantity.",".$id_user.")
									";
									
						$this->db->query($query_insert);
					}
					
					$this->db
							->set('weight',$weight)
							->set('volume',$volume)
							->where('packing_number',$packing_number)
							->update('outbound_item_receiving_details');
							
					$result = array(
						"status"		=> true,
						"message"		=> "Success.",
					);
				}
			} else {
				$query_get = "SELECT
									SUM(pr.qty) AS qty_picked, COALESCE(SUM(oird.qty), 0) AS qty_packed
								FROM
									picking_recomendation pr
								LEFT JOIN
									outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details
								WHERE
									pr.picking_id = " . $picking_id . "
								AND
									(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
								AND
									item_id = " . $item_id;
		
				$get = $this->db->query($query_get);
				$getList = $get->row_array();
				
				if(($getList['qty_picked'] - $getList['qty_packed']) >= $quantity) {
					$query_get = "SELECT
										pr.item_receiving_detail_id, pr.qty AS qty_picked, COALESCE(oird.qty, 0) AS qty_packed
									FROM
										picking_recomendation pr
									LEFT JOIN
										outbound_item_receiving_details oird ON pr.item_receiving_detail_id = oird.id_item_receiving_details
									WHERE
										pr.picking_id = " . $picking_id . "
									AND
										(oird.id_outbound_receiving_barang IS NULL OR pr.qty > oird.qty)
									AND
										(pr.qty - oird.qty) >= " . $quantity . "
									AND
										item_id = " . $item_id;
					
					$get = $this->db->query($query_get);
					$getList = $get->row_array();
					
					$ird_id = $getList['item_receiving_detail_id'];
					$isExist = $this->db->select('*')->from('outbound_item_receiving_details')->where('id_item_receiving_details',$ird_id)->where('packing_number',$packing_number)->get();
					
					if($isExist->num_rows() > 0) {
						$qtyExist = $isExist->row_array()['qty'];
						$this->db
								->set('qty',$qtyExist + $quantity)
								->where('packing_number',$packing_number)
								->where('id_item_receiving_details',$ird_id)
								->update('outbound_item_receiving_details');
					} else {
						$query_insert = "
									INSERT INTO outbound_item_receiving_details(id_item_receiving_details, picking_id, destination_id, packing_number, inc, qty, user_id) VALUES
									(".$ird_id.",".$picking_id.",".$destination_id.",'".$packing_number."',1,".$quantity.",".$id_user.")
									";
									
						$this->db->query($query_insert);
					}
					
					$this->db
							->set('weight',$weight)
							->set('volume',$volume)
							->where('packing_number',$packing_number)
							->update('outbound_item_receiving_details');
					
					
							
					$result = array(
						"status"		=> true,
						"message"		=> "Success.",
					);
				} else {
					$result = array(
						"status"		=> false,
						"message"		=> "Quantity is not enough.",
					);
				}
			}
		} else {
			$result = array(
				"status"		=> false,
				"message"		=> "Each of this item (".$param['item_code'].") is already packed.",
			);
		}
		
		return $result;
	}
}

?>


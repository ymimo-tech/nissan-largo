<?php

class api_integration_temp_model extends MY_Model {

	private $service_url = 'https://dms-api-testing.indomobil.co.id/';
	private $login_info = array('username' => 'largo', 'password' => 'P@ssw0rd');
	private $target_api_login = '/api/v1/login';
	private $target_api_receipt = '/api/v1/receipt';
	private $target_api_return_receipt = '/api/v1/return/receipt';
	private $target_api_picking = '/api/v1/picking';
	private $target_api_loading = '/api/v1/delivery';
	private $target_api_qc_in = '/api/v1/qc/in';
	private $target_api_qc_out = '/api/v1/qc/out';
	private $target_api_bin_transfer = '/api/v1/bintransfer';
	private $target_api_split_items = '/api/v1/split';
	private $token = '';

    function __construct() {
        parent::__construct();

		$api_param['target_api'] = $this->target_api_login;
		$api_param['json_param'] = json_encode($this->login_info);

		$r = $this->call_api($api_param);

		$file = '/var/www/html/largo/indofresh/debug/login_result.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". $r ."\n\n", FILE_APPEND);

		$data = json_decode($r, true);

		if(isset($data['result']['token'])) {
			$this->token = $data['result']['token'];
		} else {
			$this->token = $data['result']['result']['token'];
		}
    }

	/* --BEGIN PRODUCT-- */
	function insert_product($params = array()) {
		$result = false;

		if($params) {
			// $id = $params['id'];
			// $name = $params['name'];
			// $code = $params['default_code'];
			// $weight = $params['weight'];
			// $categ_id = $params['categ_id'];
			// $categ_name = $params['categ_name'];
			// $uom_id = $params['uom_id'];
			// $uom = $params['uom'];
			// $convert_qty_1 = 0;
			// $convert_qty_unit_id_1 = 0;
			// if($params['convert_qty_1'] != '' && $params['convert_qty_unit_id_1'] != '') {
			// 	$convert_qty_1 = $params['convert_qty_1'];
			// 	$convert_qty_unit_id_1 = $params['convert_qty_unit_id_1'];
			// }


			$check_category = $this->check_category_product($params);
			if($check_category->num_rows() == 0) {
				$category_insert    =   array(
                                'code'    => $params['category_id'],
                                'name'    => $params['category_id']
                            );
				$this->db->insert('categories', $category_insert);
				$category_id = $this->db->select('id')->from('categories')->order_by('id', 'DESC')->limit(1)->get()->row_array()['id'];
			} else {
				$category_id = $check_category->row_array()['id'];
			}

			$params['category_id'] = $category_id;

			$check_uom = $this->check_uom_product($params);
			if($check_uom->num_rows() == 0) {
				$uom_insert    =   array(
                                'code'	=> $params['unit_id'],
                                'name'	=> $params['unit_id']
                            );
				$this->db->insert('units', $uom_insert);
				$unit_id = $this->db->select('id')->from('units')->order_by('id', 'DESC')->limit(1)->get()->row_array()['id'];
			} else {
				$unit_id = $check_uom->result_array()[0]['id'];
			}

			$params['unit_id'] = $unit_id;

			// if($convert_qty_1 != 0 && $convert_qty_unit_id_1 != 0) {
			// 	$check_uom2 = $this->check_uom2_product($params);
			// 	if($check_uom2->num_rows() == 0) {
			// 		$uom2_insert    =   array(
			// 						'code'	=> $convert_qty_unit_id_1,
			// 						'name'	=> $convert_qty_unit_id_1
			// 					);
			// 		$this->db->insert('units', $uom2_insert);
			// 		$convert_qty_unit_id_1 = $this->db->insert_id();
			// 	} else {
			// 		$convert_qty_unit_id_1 = $check_uom2->result_array()[0]['id'];
			// 	}
			// }

			$id_item = 1021;

			$this->insert_product_warehouse($id_item, $params['warehouse']);


			$check_product = $this->check_product($params);
			$data_insert	= $params;
			if($check_product->num_rows() == 0) {
			// 	$data_insert	= 	array(
			// 							'id'					=> $id,
			// 							'name'					=> $name,
			// 							'code'					=> $code,
			// 							'weight'				=> $weight,
			// 							'unit_id'				=> $unit_id,
			// 							'category_id'			=> $categ_id,
			// 							'has_qty'				=> 1,
			// 							'def_qty'				=> 0,
			// 							'has_expdate'			=> 1,
			// 							'shipment_id'			=> 2,
			// 							'fifo_period'			=> 'YEARLY',
			// 							'fifo_periode_id'		=> 2,
			// 							'convert_qty_1'			=> intval($convert_qty_1),
			// 							'convert_qty_unit_id_1'	=> intval($convert_qty_unit_id_1),
			// 						);
			//
			unset($data_insert['warehouse']);
				$this->db->insert('items', $data_insert);
			// } else {
			// 	$this->db->set('weight', $weight);
			// 	$this->db->set('unit_id', $unit_id);
			// 	$this->db->set('convert_qty_1', intval($convert_qty_1));
			// 	$this->db->set('convert_qty_unit_id_1', intval($convert_qty_unit_id_1));
			// 	$this->db->set('category_id', $categ_id);
			// 	$this->db->where('id', $id);
			// 	$this->db->update('items');

			$id_item = $this->db->select('id')->from('items')->order_by('id', 'DESC')->limit(1)->get()->row_array()['id'];

			$this->insert_product_warehouse($id_item, $params['warehouse']);
		}else{
			unset($data_insert['warehouse']);


			$this->db->where('code', $data_insert['code']);
			$this->db->update('items', $data_insert);
		}

			$result = true;
		}

		return $result;
	}

	function insert_product_warehouse($item_id = '', $warehouse_id=''){
		$result = false;

		$this->db->trans_start();

        $this->db
            ->where('item_id',$item_id)
            ->delete('items_warehouses');

        $this->db->trans_complete();

		// $this->db->select('id');
		// $this->db->from('warehouses');
		//
		// $result = $this->db->get();


		$this->db->trans_start();
		$warehouse_id = explode(',', $warehouse_id);


		foreach ($warehouse_id as $key => $value) {
			if(!empty($value)){
				$id_warehouse = $this->db->select('id')->from('warehouses')->where('name', $value)->get()->result_array();
			}else{
				$id_warehouse = $this->db->select('id')->from('warehouses')->get()->result_array();
			}


			foreach ($id_warehouse as $key => $value) {
			$item_warehouse    =   array(
															'item_id'	=> $item_id,
															'warehouse_id'	=> $value['id']
													);
			$this->db->insert('items_warehouses', $item_warehouse);
		}
		}

		$this->db->trans_complete();

		// for($i = 0; $i < $result->num_rows(); $i++) {
		// 	$data_insert = array(
		// 					'item_id'	=> $item_id,
		// 					'warehouse_id'	=> $result->result_array()[$i]['id']
		// 				   );
		// 	$this->db->insert('items_warehouses', $data_insert);
		// }

		$result = $this->db->trans_status();

		return $this->db->trans_status();
	}

	function insert_user_warehouse($user_id = '', $warehouse_id=''){
		$result = false;

		$this->db->trans_start();

        $this->db
            ->where('user_id',$user_id)
            ->delete('users_warehouses');

        $this->db->trans_complete();

		// $this->db->select('id');
		// $this->db->from('warehouses');
		//
		// $result = $this->db->get();

		// $warehouse_id = explode(',', $warehouse_id);

		$this->db->trans_start();

		foreach ($warehouse_id as $key => $value) {
			$id_warehouse = $this->db->select('id')->from('warehouses')->where('name', $value)->get()->row_array()['id'];

			// if(isset($id_warehouse['id'])){

				$users_warehouses = array(
															'user_id'	=> $user_id,
															'warehouse_id'	=> $id_warehouse
													);
				$this->db->insert('users_warehouses', $users_warehouses);
		// }else {
		// 	return FALSE;
		// 	}
		}

		$this->db->trans_complete();

		// for($i = 0; $i < $result->num_rows(); $i++) {
		// 	$data_insert = array(
		// 					'item_id'	=> $item_id,
		// 					'warehouse_id'	=> $result->result_array()[$i]['id']
		// 				   );
		// 	$this->db->insert('items_warehouses', $data_insert);
		// }

		$result = $this->db->trans_status();

		return $this->db->trans_status();
	}





	function check_product($params = array()){

		$result = false;

		if($params) {
			$code = $params['code'];

			$this->db->select('*');
			$this->db->from('items');
			$this->db->where('code', $code);

			$result = $this->db->get();
		}

		return $result;

	}

	function check_product_by_name($params = array()){
		$result = false;

		if($params) {
			$name = $params['item_name'];

			$this->db->select('*');
			$this->db->from('items');
			$this->db->where('code', strval($name));

			$result = $this->db->get();
		}

		return $result;
	}

	function check_category_product($params = array()){

		$result = false;

		if($params) {
			$code = $params['category_id'];

			$this->db->select('*');
			$this->db->from('categories');
			$this->db->where("code = '".$params['category_id']."' OR name = '".$params['category_id']."'");


			$result = $this->db->get();
		}

		return $result;

	}

	function check_uom_product($params = array()){

		$result = false;

		if($params) {
			$code = $params['unit_id'];

			$this->db->select('*');
			$this->db->from('units');
			$this->db->where("code = '".$params['unit_id']."' OR name = '".$params['unit_id']."'");

			$result = $this->db->get();
		}

		return $result;

	}

	function check_uom2_product($params = array()){

		$result = false;

		if($params) {
			$code = $params['convert_qty_unit_id_1'];

			$this->db->select('*');
			$this->db->from('units');
			$this->db->where('code', $code);

			$result = $this->db->get();
		}

		return $result;

	}

	function insert_user($params = array()) {
		$result = false;

		if($params) {
			// $source_id = $params['id_source_erp'];
			// $source_code = $params['source_code'];
			// $source_name = $params['source_name'];
			// $source_address = $params['source_address'];
			// $source_city = $params['source_city'];


			$check_roles = $this->check_roles($params);
			if($check_roles->num_rows() == 0){
				$data_insert = array('grup_nama' => $params['grup_id']);
				$this->db->insert('hr_grup', $data_insert);

				$id_roles = $this->db->select('grup_id')->from('hr_grup')->order_by('grup_id', 'DESC')->limit(1)->get()->row_array()['grup_id'];
			}else{
				$id_roles = $check_roles->row_array()['grup_id'];
			}

			$params['grup_id'] = $id_roles;


			$password = md5($params['user_password']);
			$params['user_password'] = $password;

			$id_user = $this->db->select('id')->from('users')->order_by('id', 'DESC')->limit(1)->get()->row_array()['id'];

			$warehouse_insert =$this->insert_user_warehouse($id_user, $params['warehouse']);

			$check_users = $this->check_user($params);
			if($check_users->num_rows() == 0) {
				unset($params['warehouse']);
				// $data_insert	= 	array(
									// 	'id_source_erp'				=> $source_id,
									// 	'source_code'			=> $source_code,
									// 	'source_name'			=> $source_name,
									// 	'source_address'		=> $source_address,
									// 	'source_city'			=> $source_city,
									// 	'source_category_id'	=> 3,
									// );

				$this->db->insert('users', $params);
			} else {
				unset($params['warehouse']);
				// $this->db->set('source_code', $source_code);
				// $this->db->set('source_name', $source_name);
				// $this->db->set('source_address', $source_address);
				// $this->db->set('source_city', $source_city);
				// $this->db->where('id_users_erp', 	$params['id_users_erp']);

				$this->db->where('nik', $params['nik']);

				$this->db->update('users', $params);
			}

			$result = true;
		}

		return $result;
	}


	function insert_doc($params = array()) {
		$result = false;

		if($params) {

$this->db->trans_start();
			foreach ($params['name'] as $key => $name) {

						$words = explode(" ", $name);
						$acronym = "";

						foreach ($words as $w) {
				  					$acronym .= $w[0];
							}


						$params['code'] = $acronym;
						$params['name'] = $name;
						$params['year_type'] = 2;
						$params['month_type'] = 1;
						$params['separator'] = '-';
						$params['seq_length'] = 5;
						$params['is_active'] = 1;
						$params['source_category_id'] = $params['type'] == 'INBOUND' ? 3 : 4;
						$params['is_auto_generate'] = 1;
						$params['to_warehouse'] = 0;

			$check_doc = $this->check_doc($params['type'], $name);
			if($check_doc->num_rows() == 0) {


				$this->db->insert('documents', $params);
				$id_doc = $this->db->select('document_id')->from('documents')->order_by('document_id', 'DESC')->limit(1)->get()->row_array()['document_id'];

				$warehouse_id = $this->db->select('id')->from('warehouses')->get()->result_array();
				foreach ($warehouse_id as $key => $id_warehouse) {
					$doc_warehouses = array(
																'document_id'	=> $id_doc,
																'warehouse_id'	=> $id_warehouse['id']
														);
					$this->db->insert('documents_warehouses', $doc_warehouses);
				}
			} else {
				$this->db->where('name', 	$name);
				$this->db->where('type', $params['type']);
				$this->db->update('documents', $params);
			}

		}
		$this->db->trans_complete();
			$result = $this->db->trans_status();
		}

		return $result;
	}

	function check_doc($type = '', $name = ''){
		$result = FALSE;

		if($name){

			$result =$this->db->select('*')->from('documents')->where('name', $name)->where('type', $type)->get();

		}

		return $result;
	}

	function check_user($params = array()){

		$result = false;

		if($params) {
			// $users_id = $params['id_users_erp'];
			$users_id = $params['nik'];


			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('nik', $users_id);

			$result = $this->db->get();
		}

		return $result;

	}

	function check_roles($params = array()){
		$result = FALSE;

		if($params){
			$roles_id = $params['grup_id'];

			$result = $this->db->select('*')->from('hr_grup')->where('grup_nama', $roles_id)->get();
		}

		return $result;
	}

	/* --END PRODUCT-- */

	/* --BEGIN PRODUCT SUBSTITUTE-- */
	function insert_product_substitute($params = array()) {
		$result = false;

		if($params) {
			$item_id = $this->db->get_where('items',['code'=>$params['item_code']])->row_array()['id'];
			$subs_item_id = $this->db->get_where('items',['code'=>$params['subs_item_code']])->row_array()['id'];
			$status = $params['status'];

			if(count($item_id) > 0) {
				$check_item_subs = $this->check_item_subs($params);
				if($check_item_subs->num_rows() == 0) {
					$data_insert	= 	array(
											'item_id'			=> $item_id,
											'item_id_subs'		=> $subs_item_id,
											'status'			=> $status,
										);

					$this->db->insert('item_substitute', $data_insert);
				} else {
					$this->db->set('status', $status);
					$this->db->where('item_id', $item_id);
					$this->db->where('item_id_subs', $subs_item_id);
					$this->db->update('item_substitute');
				}

				$result = true;
			} else {
				return false;
			}
		}

		return $result;
	}

	function check_item_subs($params = array()){

		$result = false;

		if($params) {
			$item_id = $this->db->get_where('items',['code'=>$params['item_code']])->row_array();
			$subs_item_id = $this->db->get_where('items',['code'=>$params['subs_item_code']])->row_array();

			$this->db->select('*');
			$this->db->from('item_substitute');
			$this->db->where('item_id', $item_id['id']);
			$this->db->where('item_id_subs', $subs_item_id['id']);

			$result = $this->db->get();
		}

		return $result;

	}
	/* --END PRODUCT SUBSTITUTE-- */

	/* --BEGIN VENDOR-- */
	function insert_vendor($params = array()) {
		$result = false;

		if($params) {
			$source_id = $params['id_source_erp'];
			$source_code = $params['source_code'];
			$source_name = $params['source_name'];
			$source_address = $params['source_address'];
			$source_city = $params['source_city'];
			$status = $params['status'];
			$params['source_category_id'] = 3;

			$check_vendor = $this->check_vendor($params);
			if($check_vendor->num_rows() == 0) {
				// $data_insert	= 	array(
				// 						'id_source_erp'			=> $source_id,
				// 						'source_code'			=> $source_code,
				// 						'source_name'			=> $source_name,
				// 						'source_address'		=> $source_address,
				// 						'source_city'			=> $source_city,
				// 						'status'				=> $status,
				// 						'source_category_id'	=> 3,
				// 					);

				$data_insert = $params;

				$this->db->insert('sources', $data_insert);
			} else {
				// $this->db->set('source_code', $source_code);
				// $this->db->set('source_name', $source_name);
				// $this->db->set('source_address', $source_address);
				// $this->db->set('source_city', $source_city);
				// $this->db->set('status', $status);
				$this->db->where('source_code', $source_code);
				$this->db->update('sources', $params);
			}

			$result = true;
		}

		return $result;
	}

	function check_vendor($params = array()){

		$result = false;

		if($params) {
			$source_id = $params['source_code'];

			$this->db->select('*');
			$this->db->from('sources');
			$this->db->where('source_code', $source_id);

			$result = $this->db->get();
		}

		return $result;

	}
	/* --END VENDOR-- */

	/* --BEGIN CUSTOMER-- */
	function insert_customer($params = array()) {
		$result = false;

		if($params) {
			$destination_id = $params['id_destination_erp'];
			$destination_code = $params['destination_code'];
			$destination_name = $params['destination_name'];
			$destination_address = $params['destination_address'];
			$destination_city = $params['destination_city'];
			$destination_phone = $params['destination_phone'];
			$destination_cp = $params['destination_contact_person'];
			$status = $params['status'];
			$params['source_category_id'] = 4;

			$check_customer = $this->check_customer($params);
			if($check_customer->num_rows() == 0){
				// $data_insert	= 	array(
				// 						'id_destination_erp'		=> $destination_id,
				// 						'destination_code'			=> $destination_code,
				// 						'destination_name'			=> $destination_name,
				// 						'destination_address'		=> $destination_address,
				// 						'destination_city'			=> $destination_city,
				// 						''
				// 						'status'			=> $status,
				// 						'source_category_id'		=> 4,
				// 					);
				$data_insert = $params;

				$this->db->insert('destinations', $data_insert);
			} else {
				// $this->db->set('destination_id', $destination_id);
				// $this->db->set('destination_code', $destination_code);
				// $this->db->set('destination_name', $destination_name);
				// $this->db->set('destination_address', $destination_address);
				// $this->db->set('destination_city', $destination_city);
				// $this->db->set('status', $status);
				$this->db->where('destination_code', $destination_code);
				$this->db->update('destinations', $params);
			}

			$result = true;
		}

		return $result;
	}

	function check_customer($params = array()){

		$result = false;

		if($params) {
			$destination_id = $params['destination_code'];

			$this->db->select('*');
			$this->db->from('destinations');
			$this->db->where('destination_code', strval($destination_id));

			$result = $this->db->get();
		}

		return $result;

	}
	/* --END CUSTOMER-- */

	/* --BEGIN WAREHOUSE-- */
	function insert_warehouse($params = array()) {
		$result = false;

		if($params) {
			$id = $params['id'];
			$name = $params['name'];

			$check_warehouse = $this->check_warehouse($params);
			if($check_warehouse->num_rows() == 0) {
				$data_insert	= 	array(
										'id'			=> $id,
										'name'			=> $name,
									);

				$this->db->insert('warehouses', $data_insert);
			} else {
				$this->db->set('id', $id);
				$this->db->set('name', $name);
				$this->db->where('id', $id);
				$this->db->update('warehouses');
			}

			$result = true;
		}

		return $result;
	}

	function check_warehouse($params = array()){

		$result = false;

		if($params) {
			$id = $params['id'];

			$this->db->select('*');
			$this->db->from('warehouses');
			$this->db->where('id', $id);

			$result = $this->db->get();
		}

		return $result;

	}
	/* --END WAREHOUSE-- */

	/* --BEGIN LOCATION-- */
	function insert_location($params = array()) {
		$result = false;

		if($params) {
			// $id = $params['id_location_erp'] + 110;

			// $name = $params['name'];
			// $description = $params['name'];
			// $warehouse_id = $params['warehouse_id'];

			// $pars['id'] = $warehouse_id;
			// $check_warehouse = $this->check_warehouse($pars);
			// if($check_warehouse->num_rows() != 0) {

			$location_type_id = $this->db->select('id')->from('location_types')->where('name', $params['location_type_id'])->get();

				if($location_type_id->num_rows() == 0) {
					$insert_loc_types = array( "name" => $params['location_type_id']);

					$this->db->insert('location_types', $insert_loc_types);
					$id_loc_type = $this->db->select('id')->from('location_types')->order_by('id', 'DESC')->limit(1)->get()->row_array()['id'];

			}else{
				$id_loc_type = $location_type_id->row_array()['id'];
			}


			$location_area_id = $this->db->select('id')->from('location_areas')->where('name', $params['location_area_id'])->get();

			if($location_area_id->num_rows() == 0){
				$insert_area_id = array( "name" => $params['location_area_id'], "warehouse_id" => 978);

				$this->db->insert('location_areas', $insert_area_id);
				$id_loc_area = $this->db->select('id')->from('location_areas')->order_by('id', 'DESC')->limit(1)->get()->row_array()['id'];
			}else{
				$id_loc_area = $location_area_id->row_array()['id'];
			}

			$params['category_id'] = $params['location_category_id'];

			$check_category = $this->check_category_product($params);
			if($check_category->num_rows() == 0) {
				$category_insert    =   array(
																'code'    => $params['category_id'],
																'name'    => $params['category_id']
														);
				$this->db->insert('categories', $category_insert);
				$category_id = $this->db->select('id')->from('categories')->order_by('id', 'DESC')->limit(1)->get()->row_array()['id'];
			} else {
				$category_id = $check_category->row_array()['id'];
			}

						$check_location = $this->check_location($params);
						// dd($check_location);
						if($check_location->num_rows() == 0){

						$data_insert = $params;
						// $data_insert['id'] = $id;
						$data_insert['location_type_id'] = $id_loc_type;
						$data_insert['location_area_id'] = $id_loc_area;
						$data_insert['location_category_id'] = $category_id;
						unset($data_insert['category_id']);

					$this->db->insert('locations', $data_insert);
				} else {

					$data_insert = $params;
					// $data_insert['id'] = $id;
					$data_insert['location_type_id'] = $id_loc_type;
					$data_insert['location_area_id'] = $id_loc_area;
					$data_insert['location_category_id'] = $category_id;
					unset($data_insert['category_id']);

					// $this->db->set('name', $name);
					// $this->db->set('description', $name);
					$this->db->where('name', $params['name']);
					$this->db->update('locations', $data_insert);
				}
			// } else {
			// 	return false;
			// }

			$result = true;
		}

		return $result;
	}

	function check_location($params = array()){

		$result = false;

		if($params) {
			// $id = $params['id'] + 110;
			$id_location_erp = $params['name'];
			// $warehouse_id = $params['warehouse_id'];

			$this->db->select('*');
			$this->db->from('locations');
			$this->db->where('name', $id_location_erp);
			// $this->db->where('warehouse_id', $warehouse_id);

			$result = $this->db->get();
		}

		return $result;

	}
	/* --END LOCATION-- */

	/* --BEGIN INBOUND-- */
	function insert_inbound($params = array()) {
		$result = false;

		if($params) {
			// $id_staging = $params['id_staging'];
			// $code = $params['code'];
			// $date = $params['date'];
			// $id_supplier = $params['supplier_id'];
			// $document_code = $params['document_code'];
			// $user = $params['user'];
			// $remark = $params['remark'];
			// $id_warehouse = $params['id_warehouse'];
			// $no_pallet_original = $params['no_pallet_original'];

			$check_inbound = $this->check_inbound($params);
				$getDocDetail = $this->get_doc_detail($params);
				$document_id = $getDocDetail->result_array()[0]['document_id'];
				$params['document_id'] = $document_id;

				$getUserDetail = $this->get_user_detail($params);
				$user_id = $getUserDetail->result_array()[0]['id'];
				$params['user_id'] = $user_id;

				$getid_warehouse = $this->get_warehouse_id($params['warehouse_id']);
				$id_warehouse = $getid_warehouse->result_array()[0]['id'];
				$params['warehouse_id'] = $id_warehouse;

				$getSupplier_id = $this->get_supplier_id($params['supplier_id']);
				$id_supplier = $getSupplier_id->result_array()[0]['source_id'];
				$params['supplier_id'] = $id_supplier;

				// $data_insert	= 	array(
				// 						// 'id_staging'			=> $id_staging,
				// 						'code'					=> $code,
				// 						'date'					=> $date,
				// 						'supplier_id'			=> $id_supplier,
				// 						'document_id'			=> $document_id,
				// 						'user_id'				=> $user_id,
				// 						'remark'				=> $remark,
				// 						'status_id'				=> 3,
				// 						'warehouse_id'			=> $id_warehouse,
				// 						'no_pallet_original'	=> $no_pallet_original,
				// 					);

				if($check_inbound->num_rows() == 0) {
				$data_insert = $params;


				$this->db->insert('inbound', $data_insert);
				$id_inbound = $this->db->select('id_inbound')->from('inbound')->order_by('id_inbound', 'DESC')->limit(1)->get()->row_array()['id_inbound'];

				$inbound_warehouses_insert = array(
										'inbound_id'			=> $id_inbound,
										'warehouse_id'			=> $id_warehouse,
									);
				$this->db->insert('inbound_warehouses', $inbound_warehouses_insert);

				$result = true;
			} else {
				// $this->db->set('supplier_id', $id_supplier);
				// $this->db->set('no_pallet_original', $no_pallet_original);
				$this->db->where('code', $params['code']);
				$this->db->update('inbound', $params);
				return true;
			}
		}

		return $result;
	}

	function get_warehouse_id($param){

		$result = false;

		if($param) {

			$this->db->select('*');
			$this->db->from('warehouses');
			$this->db->where('name', $param);

			$result = $this->db->get();
		}

		return $result;
	}


	function get_supplier_id($param){

		$result = false;

		if($param) {

			$this->db->select('*');
			$this->db->from('sources');
			$this->db->where('source_code', $param);

			$result = $this->db->get();
		}

		return $result;
	}

	function insert_inbound_item($params = array()) {
		$result = false;

		if($params) {
			// $id_staging = $params['id_staging'];
			$qty = $params['qty'];
			$item_id = $params['item_id'];
			$id_inbound = $params['inbound_id'];

			// $inbound_detail = $this->get_inbound_detail_by_staging_id($params);
			// $id_inbound = $inbound_detail->result_array()[0]['id_inbound'];

			$check_inbound_item = $this->check_inbound_item($params);

			if($check_inbound_item->num_rows() == 0) {
				$data_insert	= 	array(
										'inbound_id'	=> $id_inbound,
										'qty'			=> $qty,
										'item_id'		=> $item_id,
										// 'id_staging'	=> $id_staging,
									);

				$this->db->insert('inbound_item', $data_insert);
			} else {
				$this->db->set('item_id', $item_id);
				$this->db->set('qty', $qty);
				$this->db->where('id', $check_inbound_item->result_array()[0]['id']);
				$this->db->update('inbound_item');
			}
			$result = true;
		}

		return $result;
	}

	function check_inbound($params = array()){

		$result = false;

		if($params) {
			$code = $params['code'];

			$this->db->select('*');
			$this->db->from('inbound');
			$this->db->where('code', $code);

			$result = $this->db->get();
		}

		return $result;

	}

	function check_inbound_item($params = array()){

		$result = false;

		if($params) {
			$id_staging = $params['inbound_id'];
			$item_id = $params['item_id'];

			$this->db->select('*');
			$this->db->from('inbound_item');
			// $this->db->where('id_staging', $id_staging);
			$this->db->where('inbound_id', $id_staging);
			$this->db->where('item_id', $item_id);

			$result = $this->db->get();
		}

		return $result;

	}

	function get_inbound_detail_by_staging_id($params = array()) {

		$result = false;

		if($params) {
			$id_staging = $params['id_staging'];

			$this->db->select('*');
			$this->db->from('inbound');
			$this->db->where('id_staging', $id_staging);

			$result = $this->db->get();
		}

		return $result;

	}

	function get_doc_detail($params = array()){

		$result = false;

		if($params) {
			$code = $params['document_id'];

			$this->db->select('*');
			$this->db->from('documents');
			$this->db->where('lower(name)', strtolower($code));

			$result = $this->db->get();
		}

		return $result;

	}

	function get_user_detail($params = array()){

		$result = false;

		if($params) {
			$user = $params['user_id'];

			$this->db->select('*');
			$this->db->from('users');
			$this->db->where("user_name='$user' or nik='$user'");

			$result = $this->db->get();
		}

		return $result;

	}

	function cancel_inbound($params = array()){

		$result = false;

		if($params) {
			$id_staging = $params['id_staging'];
			$code = $params['code'];

			$this->db->select('ird.unique_code');
			$this->db->from('inbound inb');
			$this->db->join('item_receiving ir', 'inb.id_inbound = ir.inbound_id');
			$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
			$this->db->join('item_receiving_details ird', 'ird.item_receiving_id = ir.id');
			$this->db->where('inb.id_staging', $id_staging);
			$this->db->where('inb.code', $code);

			$result = $this->db->get();

			if($result->num_rows() == 0) {
				$this->db->select('id_inbound');
				$this->db->from('inbound');
				$this->db->where('id_staging', $id_staging);
				$this->db->where('code', $code);

				$id_inbound = $this->db->get()->result_array()[0]['id_inbound'];

				$this->db->where('inbound_id', intval($id_inbound));
				$this->db->delete('inbound_item');

				$this->db->where('inbound_id', intval($id_inbound));
				$this->db->delete('inbound_warehouses');

				$this->db->where('id_inbound', intval($id_inbound));
				$this->db->delete('inbound');

				$result = true;
			} else {
				return false;
			}
		}

		return $result;

	}
	/* --END INBOUND-- */
	
	/* --BEGIN RECEIVING DOCUMENT-- */
	function insert_receiving($params = array()) {
		$result = false;

		if($params) {
			$check_receiving = $this->db->get_where('receivings',['code'=>$params['code']]);

			$getUserDetail = $this->get_user_detail($params);
			$user_id = $getUserDetail->result_array()[0]['id'];
			$params['user_id'] = $user_id;

			$params['warehouse_id'] = $this->db->get_where('warehouses',['name'=>$params['warehouse_id']])->row_array()['id'];

			if($check_receiving->num_rows() == 0) {
				$data_insert = $params;

				$this->db->insert('receivings', $data_insert);

				return true;
			} else {
				// $this->db->where('code', $params['code']);
				// $this->db->update('inbound', $params);
				return true;
			}
		}

		return $result;
	}
	
	function insert_asn_item($params = array()) {
		$result = false;

		if($params) {
			$qty = $params['qty'];
			$item_id = $params['item_id'];
			$id_receiving = $params['id_receiving'];
			$id_inbound = $params['id_inbound'];

			$check_asn_item = $this->check_asn_item($params);

			if($check_asn_item->num_rows() == 0) {
				$data_insert	= 	array(
										'qty'			=> $qty,
										'item_id'		=> $item_id,
										'receiving_id'	=> $id_receiving,
										'inbound_id'	=> $id_inbound,
									);

				$this->db->insert('item_receiving', $data_insert);
			} else {
				$this->db->set('item_id', $item_id);
				$this->db->set('qty', $qty);
				$this->db->where('id', $check_asn_item->row_array()['id']);
				$this->db->update('item_receiving');
			}
			$result = true;
		}

		return $result;
	}
	
	function check_asn_item($params = array()){

		$result = false;

		if($params) {
			$id_receiving = $params['id_receiving'];
			$item_id = $params['item_id'];

			$this->db->select('*');
			$this->db->from('item_receiving');
			$this->db->where('receiving_id', $id_receiving);
			$this->db->where('item_id', $item_id);

			$result = $this->db->get();
		}

		return $result;

	}
	/* --END RECEIVING DOCUMENT-- */

	/* --BEGIN RECEIPT-- */
	function post_receipt($kd_receiving = '', $unique_code = ''){

		$result = false;

		// if($kd_receiving != '') {
		if($unique_code != '') {
			$this->db->select(["doc.code AS doc_code", "COALESCE(outb.code, '-') AS outbound_code", "inb.code AS purchase", "inb.id_staging AS purchase_id", "rcv.code AS receiving", "TO_CHAR(ird.putaway_time::DATE, 'yyyy-mm-dd') AS date", "u.user_name AS operator", "rcv.vehicle_plate AS vehicle", "inb.status_id AS status"]);
			$this->db->from('inbound inb');
			$this->db->join('documents doc', 'inb.document_id = doc.document_id');
			$this->db->join('outbound outb', 'inb.outbound_id = outb.id', 'LEFT');
			$this->db->join('item_receiving ir', 'inb.id_inbound = ir.inbound_id');
			$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
			// $this->db->where('rcv.code', $kd_receiving);
			$this->db->join('item_receiving_details ird', 'ird.item_receiving_id = ir.id');
			$this->db->join('users u', 'ird.user_id_putaway = u.id');
			$this->db->where('ird.unique_code', $unique_code);

			$result = $this->db->get();
			if($result->num_rows() > 0) {
				$jsonData = array();
				$data = $result->result_array();

				$this->db->select(["itm.id AS product_id", "COALESCE(SUM(ird.first_qty), 0) AS qty", "itm.unit_id AS uom", "ird.grade AS grade", "ird.tgl_exp AS date_expired", "ird.unique_code AS pallet", "ird.location_id AS location_id"]);
				$this->db->from('item_receiving_details ird');
				$this->db->join('item_receiving ir', 'ird.item_receiving_id = ir.id');
				$this->db->join('items itm', 'ir.item_id = itm.id');
				$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
				// $this->db->where('rcv.code', $kd_receiving);
				$this->db->where('ird.unique_code', $unique_code);
				$this->db->group_by('itm.id, itm.unit_id, ird.grade, ird.tgl_exp, ird.unique_code, ird.location_id');

				$productsGet = $this->db->get();

				if($data[0]['doc_code'] == 'PO') {
					for($i = 0; $i < $result->num_rows(); $i++) {
						$jsonData['purchase'] = $data[$i]['purchase'];
						$jsonData['purchase_id'] = intval($data[$i]['purchase_id']) - 0;
						$jsonData['receiving'] = $data[$i]['receiving'];
						$jsonData['date'] = $data[$i]['date'];
						$jsonData['operator'] = $data[$i]['operator'];
						$jsonData['vehicle'] = $data[$i]['vehicle'];
						$jsonData['status'] = $data[$i]['status'] == 4 ? true : false;

						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();

							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$productsArray['product_id'] = intval($productsData[$j]['product_id']) - 0;
								$productsArray['qty'] = intval($productsData[$j]['qty']) - 0;
								$productsArray['uom'] = intval($productsData[$j]['uom']) - 0;
								$productsArray['grade'] = $productsData[$j]['grade'];
								$productsArray['date_expired'] = $productsData[$j]['date_expired'];
								$productsArray['pallet'] = $productsData[$j]['pallet'];
								$productsArray['location_id'] = intval($productsData[$j]['location_id']) - 110;
							}

							$jsonData['products'] = array($productsArray);
						}
					}

					$log_param['name'] = 'post_receipt';
					$log_param['api_url'] = $this->service_url . $this->target_api_receipt;
					$api_param['target_api'] = $this->target_api_receipt;
				} else {
					for($i = 0; $i < $result->num_rows(); $i++) {
						$jsonData['delivery_ref'] = $data[$i]['outbound_code'];
						$jsonData['receiving'] = $data[$i]['receiving'];
						$jsonData['date'] = $data[$i]['date'];
						$jsonData['operator'] = $data[$i]['operator'];
						$jsonData['vehicle'] = $data[$i]['vehicle'];

						if($productsGet->num_rows() > 0) {
							$productsArray = array();
							$productsData = $productsGet->result_array();

							for($j = 0; $j < $productsGet->num_rows(); $j++) {
								$productsArray['product_id'] = intval($productsData[$j]['product_id']) - 0;
								$productsArray['qty'] = intval($productsData[$j]['qty']) - 0;
								$productsArray['uom_id'] = intval($productsData[$j]['uom']) - 0;
								$productsArray['pallet'] = $productsData[$j]['pallet'];
								$productsArray['location_id'] = intval($productsData[$j]['location_id']) - 110;
							}

							$jsonData['products'] = array($productsArray);
						}
					}

					$log_param['name'] = 'post_return_receipt';
					$log_param['api_url'] = $this->service_url . $this->target_api_return_receipt;
					$api_param['target_api'] = $this->target_api_return_receipt;
				}


				$file = '/var/www/html/largo/indofresh/debug/postReceipt_param.txt';
				// file_put_contents($file, date('Y-m-d H:i:s') ."\n". json_encode($jsonData) ."\n\n", FILE_APPEND);
				// $log_param['name'] = 'post_receipt';
				// $log_param['api_url'] = $this->service_url . $this->target_api_receipt;
				$log_param['parameter'] = json_encode($jsonData);
				$insert_log = $this->insert_log($log_param);

				// $api_param['target_api'] = $this->target_api_receipt;
				$api_param['json_param'] = json_encode($jsonData);

				$call_receipt_api = $this->call_api($api_param);

				$file = '/var/www/html/largo/indofresh/debug/postReceipt_result.txt';
				// file_put_contents($file, date('Y-m-d H:i:s') ."\n". $call_receipt_api ."\n\n", FILE_APPEND);
				$log_param['id'] = $insert_log;
				$log_param['response'] = $call_receipt_api;
				$this->update_log($log_param);

				$response = json_decode($call_receipt_api, true);

				if(isset($response['error'])) {
					// echo $response['error']['data']['message'];
				}
			}
		}

		return $result;

	}
	/* --END RECEIPT-- */

	/* --BEGIN OUTBOUND DOC-- */
	function insert_outbound($params = array()) {
		$result = false;

		if($params) {
			$id_outbound_erp = $params['id_outbound_erp'];
			$code = $params['code'];
			$date = $params['date'];
			$customer_id = $params['customer_id'];
			$document_code = $params['document_id'];
			$user = $params['user_id'];
			$note = $params['note'];
			$id_warehouse = $this->db->select('id')->from('warehouses')->where('name', $params['id_warehouse'])->get()->row_array()['id'];


			$check_outbound = $this->check_outbound($params);

			if($check_outbound->num_rows() == 0) {
				$getDocDetail = $this->get_doc_detail($params);
				$document_id = $getDocDetail->result_array()[0]['document_id'];

				$getUserDetail = $this->get_user_detail($params);
				$user_id = $getUserDetail->result_array()[0]['id'];

				$getDestinationDetail = $this->get_destination_detail($params);
				$destination_id = $getDestinationDetail->result_array()[0]['destination_id'];
				$destination_name = $getDestinationDetail->result_array()[0]['destination_name'];
				$destination_address = $getDestinationDetail->result_array()[0]['destination_address'];
				$destination_city = $getDestinationDetail->result_array()[0]['destination_city'];

				$data_insert	= 	array(
										'id_outbound_erp'				=> $id_outbound_erp,
										'code'						=> $code,
										'date'						=> $date,
										'delivery_date'				=> $date,
										'destination_id'			=> $destination_id,
										'destination_name'			=> $destination_name,
										'address'					=> $destination_address,
										'document_id'				=> $document_id,
										'user_id'					=> $user_id,
										'note'						=> $note,
										'status_id'					=> 3,
										'warehouse_id'				=> $id_warehouse,
										'm_priority_id'				=> 1,
										'type'						=> 'FIFO',
										'ekspedisi' => $params['ekspedisi'],
										'shipping_group' => $params['shipping_group']
									);

				$this->db->insert('outbound', $data_insert);

				$result = true;
			} else {
				return false;
			}
		}

		return $result;
	}

	function insert_outbound_item($params = array()) {
		$result = false;

		if($params) {
			$id_outbound = $params['id_outbound'];
			$qty = $params['qty'];
			$item_id = $params['item_id'];
			$unit_id = $params['unit_id'];
			// $grade = $params['grade'];

			// $outbound_detail = $this->get_outbound_detail_by_staging_id($params);
			// $id_outbound = $outbound_detail->result_array()[0]['id'];

			$check_outbound_item = $this->check_outbound_item($params);

			if($check_outbound_item->num_rows() == 0) {
				$data_insert	= 	array(
										'id_outbound'			=> intval($id_outbound),
										// 'id_staging'			=> intval($id_staging),
										'qty'					=> intval($qty),
										'item_id'				=> intval($item_id),
										'unit_id'				=> intval($unit_id),
										// 'grade'					=> $grade,
									);

				$this->db->insert('outbound_item', $data_insert);
			} else {
				$this->db->set('item_id', intval($item_id));
				$this->db->set('unit_id', intval($unit_id));
				// $this->db->set('grade', $grade);
				$this->db->set('qty', intval($qty));
				$this->db->where('id', $check_outbound_item->result_array()[0]['id']);
				$this->db->update('outbound_item');
			}
			$result = true;
		}

		return $result;
	}

	function check_outbound($params = array()){

		$result = false;

		if($params) {
			$code = $params['code'];

			$this->db->select('*');
			$this->db->from('outbound');
			$this->db->where('code', $code);

			$result = $this->db->get();
		}

		return $result;

	}

	function check_outbound_item($params = array()){

		$result = false;

		if($params) {
			$id_staging = $params['id_outbound'];
			$item_id = $params['item_id'];

			$this->db->select('*');
			$this->db->from('outbound_item');
			$this->db->where('id_outbound', $id_staging);
			$this->db->where('item_id', $item_id);

			$result = $this->db->get();
		}

		return $result;

	}

	function get_outbound_detail_by_staging_id($params = array()) {

		$result = false;

		if($params) {
			$id_staging = $params['id_staging'];

			$this->db->select('*');
			$this->db->from('outbound');
			$this->db->where('id_staging', $id_staging);

			$result = $this->db->get();
		}

		return $result;

	}

	function get_destination_detail($params = array()) {

		$result = false;

		if($params) {
			$destination_id = $params['destination_id'];

			$this->db->select('*');
			$this->db->from('destinations');
			$this->db->where('destination_code', $destination_id);

			$result = $this->db->get();
		}

		return $result;

	}

	function cancel_outbound($params = array()){

		$result = false;

		if($params) {
			$id_staging = $params['id_staging'];
			$code = $params['code'];

			$this->db->select('pr.unique_code');
			$this->db->from('outbound outb');
			$this->db->join('picking_list_outbound plo', 'outb.id = plo.id_outbound');
			$this->db->join('pickings pick', 'plo.pl_id = pick.pl_id');
			$this->db->join('picking_recomendation pr', 'pick.pl_id = pr.picking_id');
			$this->db->join('users u', 'pr.user_id = u.id');
			$this->db->where('outb.id_staging', $id_staging);
			$this->db->where('outb.code', $code);

			$result = $this->db->get();

			if($result->num_rows() == 0) {
				$this->db->select('id');
				$this->db->from('outbound');
				$this->db->where('id_staging', $id_staging);
				$this->db->where('code', $code);

				$id_outbound = $this->db->get()->result_array()[0]['id'];

				$this->db->where('id_outbound', intval($id_outbound));
				$this->db->delete('outbound_item');

				$this->db->where('id', intval($id_outbound));
				$this->db->delete('outbound');

				$result = true;
			} else {
				return false;
			}
		}

		return $result;

	}
	/* --END OUTBOUND DOC-- */

	/* --BEGIN PICKING-- */
	function post_picking($pl_name = '', $unique_code = ''){

		$result = false;

		if($pl_name != '' && $unique_code != '') {
			$this->db->select(["outb.code AS sale", "outb.id_staging AS sale_id", "pick.name AS picking", "u.user_name AS operator", "pick.plat_number AS vehicle", "outb.status_id AS status"]);
			$this->db->from('outbound outb');
			$this->db->join('picking_list_outbound plo', 'outb.id = plo.id_outbound');
			$this->db->join('pickings pick', 'plo.pl_id = pick.pl_id');
			$this->db->join('picking_recomendation pr', 'pick.pl_id = pr.picking_id');
			$this->db->join('users u', 'pr.user_id = u.id');
			$this->db->where('pr.unique_code', $unique_code);
			$this->db->where('pick.name', $pl_name);
			$this->db->order_by('pr.id', 'DESC');
			$this->db->limit(1);

			$result = $this->db->get();
			if($result->num_rows() > 0) {
				$jsonData = array();
				$data = $result->result_array();

				$this->db->select(["pq.item_id AS product_id", "COALESCE(pr.qty, 0) AS qty", "pr.unit_id AS uom", "pq.grade AS grade", "ird.tgl_exp AS date_expired", "pr.unique_code AS pallet", "pr.old_location_id AS location_id"]);
				$this->db->from('picking_recomendation pr');
				$this->db->join('item_receiving_details ird', 'pr.item_receiving_detail_id = ird.id');
				$this->db->join('pickings pick', 'pr.picking_id = pick.pl_id');
				$this->db->join('picking_qty pq', 'pick.pl_id = pq.picking_id');
				$this->db->where('pr.unique_code', $unique_code);
				$this->db->where('pick.name', $pl_name);
				$this->db->order_by('pr.id', 'DESC');
				$this->db->limit(1);

				$productsGet = $this->db->get();

				for($i = 0; $i < $result->num_rows(); $i++) {
					$jsonData['sale'] = $data[$i]['sale'];
					$jsonData['sale_id'] = intval($data[$i]['sale_id']) - 0;
					$jsonData['picking'] = $data[$i]['picking'];
					$jsonData['date'] = date('Y-m-d');
					$jsonData['operator'] = $data[$i]['operator'];
					$jsonData['vehicle'] = $data[$i]['vehicle'];
					$jsonData['status'] = $data[$i]['status'] == 4 ? true : false;

					if($productsGet->num_rows() > 0) {
						$productsArray = array();
						$productsData = $productsGet->result_array();

						for($j = 0; $j < $productsGet->num_rows(); $j++) {
							$productsArray['product_id'] = intval($productsData[$j]['product_id']) - 0;
							$productsArray['qty'] = intval($productsData[$j]['qty']) - 0;
							$productsArray['uom_id'] = intval($productsData[$j]['uom']) - 0;
							$productsArray['grade'] = $productsData[$j]['grade'];
							$productsArray['date_expired'] = $productsData[$j]['date_expired'];
							$productsArray['pallet'] = $productsData[$j]['pallet'];
							$productsArray['location_id'] = intval($productsData[$j]['location_id']) - 110;
						}

						$jsonData['products'] = array($productsArray);
					}
				}

				$file = '/var/www/html/largo/indofresh/debug/postPicking_param.txt';
				// file_put_contents($file, date('Y-m-d H:i:s') ."\n". json_encode($jsonData) ."\n\n", FILE_APPEND);
				$log_param['name'] = 'post_picking';
				$log_param['api_url'] = $this->service_url . $this->target_api_picking;
				$log_param['parameter'] = json_encode($jsonData);
				$insert_log = $this->insert_log($log_param);

				$api_param['target_api'] = $this->target_api_picking;
				$api_param['json_param'] = json_encode($jsonData);

				$call_picking_api = $this->call_api($api_param);

				$file = '/var/www/html/largo/indofresh/debug/postPicking_result.txt';
				// file_put_contents($file, date('Y-m-d H:i:s') ."\n". $call_picking_api ."\n\n", FILE_APPEND);
				$log_param['id'] = $insert_log;
				$log_param['response'] = $call_picking_api;
				$this->update_log($log_param);

				$response = json_decode($call_picking_api, true);

				if(isset($response['error'])) {
					// echo $response['error']['data']['message'];
				}
			}
		}

		return $result;

	}
	/* --END PICKING-- */

	/* --BEGIN LOADING-- */
	function post_loading($ship_code = '', $unique_code = ''){

		$result = false;

		if($ship_code != '' && $unique_code != '') {
			$this->db->select(["outb.code AS sale", "outb.id_staging AS sale_id", "ship.code AS picking", "u.user_name AS operator", "ship.license_plate AS vehicle", "outb.status_id AS status"]);
			$this->db->from('outbound outb');
			$this->db->join('picking_list_outbound plo', 'outb.id = plo.id_outbound');
			$this->db->join('pickings pick', 'plo.pl_id = pick.pl_id');
			$this->db->join('picking_recomendation pr', 'pick.pl_id = pr.picking_id');
			$this->db->join('shipping_picking sp', 'pick.pl_id = sp.pl_id');
			$this->db->join('shippings ship', 'sp.shipping_id = ship.shipping_id');
			$this->db->join('users u', 'pr.user_id = u.id');
			$this->db->where('pr.unique_code', $unique_code);
			$this->db->where('ship.code', $ship_code);
			$this->db->order_by('pr.id', 'DESC');
			$this->db->limit(1);

			$result = $this->db->get();
			if($result->num_rows() > 0) {
				$jsonData = array();
				$data = $result->result_array();

				$this->db->select(["pq.item_id AS product_id", "COALESCE(SUM(pr.qty), 0) AS qty", "pr.unit_id AS uom", "pq.grade AS grade", "ird.tgl_exp AS date_expired", "pr.unique_code AS pallet"]);
				$this->db->from('picking_recomendation pr');
				$this->db->join('item_receiving_details ird', 'pr.item_receiving_detail_id = ird.id');
				$this->db->join('pickings pick', 'pr.picking_id = pick.pl_id');
				$this->db->join('picking_qty pq', 'pick.pl_id = pq.picking_id');
				$this->db->join('shipping_picking sp', 'pick.pl_id = sp.pl_id');
				$this->db->join('shippings ship', 'sp.shipping_id = ship.shipping_id');
				$this->db->where('pr.unique_code', $unique_code);
				$this->db->where('ship.code', $ship_code);
				$this->db->group_by('pq.item_id, pr.unit_id, pq.grade, ird.tgl_exp, pr.unique_code');

				$productsGet = $this->db->get();

				for($i = 0; $i < $result->num_rows(); $i++) {
					$jsonData['sale'] = $data[$i]['sale'];
					$jsonData['sale_id'] = intval($data[$i]['sale_id']) - 0;
					$jsonData['picking'] = $data[$i]['picking'];
					$jsonData['date'] = date('Y-m-d');
					$jsonData['operator'] = $data[$i]['operator'];
					$jsonData['vehicle'] = $data[$i]['vehicle'];
					$jsonData['status'] = $data[$i]['status'] == 4 ? true : false;

					if($productsGet->num_rows() > 0) {
						$productsArray = array();
						$productsData = $productsGet->result_array();

						for($j = 0; $j < $productsGet->num_rows(); $j++) {
							$productsArray['product_id'] = intval($productsData[$j]['product_id']) - 0;
							$productsArray['qty'] = intval($productsData[$j]['qty']) - 0;
							$productsArray['uom_id'] = intval($productsData[$j]['uom']) - 0;
							$productsArray['grade'] = $productsData[$j]['grade'];
							$productsArray['date_expired'] = $productsData[$j]['date_expired'];
							$productsArray['pallet'] = $productsData[$j]['pallet'];
						}

						$jsonData['products'] = array($productsArray);
					}
				}

				$file = '/var/www/html/largo/indofresh/debug/postLoading_param.txt';
				// file_put_contents($file, date('Y-m-d H:i:s') ."\n". json_encode($jsonData) ."\n\n", FILE_APPEND);
				$log_param['name'] = 'post_picking';
				$log_param['api_url'] = $this->service_url . $this->target_api_loading;
				$log_param['parameter'] = json_encode($jsonData);
				$insert_log = $this->insert_log($log_param);

				$api_param['target_api'] = $this->target_api_loading;
				$api_param['json_param'] = json_encode($jsonData);

				$call_picking_api = $this->call_api($api_param);

				$file = '/var/www/html/largo/indofresh/debug/postLoading_result.txt';
				// file_put_contents($file, date('Y-m-d H:i:s') ."\n". $call_picking_api ."\n\n", FILE_APPEND);
				$log_param['id'] = $insert_log;
				$log_param['response'] = $call_picking_api;
				$this->update_log($log_param);

				$response = json_decode($call_picking_api, true);

				if(isset($response['error'])) {
					// echo $response['error']['data']['message'];
				}
			}
		}

		return $result;

	}
	/* --END LOADING-- */

	/* --BEGIN QC-- */
	function post_qc_in($params){

		$result = false;

		if($params) {
			$jsonData = array();
			$productsArray = array();

			$this->db->select('user_name');
			$this->db->from('users');
			$this->db->where('id', $params['user_id']);
			$operator = $this->db->get()->result_array()[0]['user_name'];

			$jsonData['ref'] = '-';
			$jsonData['date'] = date('Y-m-d');
			$jsonData['vehicle'] = '-';
			$jsonData['operator'] = $operator;

			$this->db->select(['ird.unique_code as pallet', 'ird.last_qty as qty', 'i.unit_id as uom_id', 'ird.item_id as product_id', "COALESCE(loc.warehouse_id, '2') as warehouse_id"]);
			$this->db->from('item_receiving_details ird');
			$this->db->join('items i', 'ird.item_id = i.id');
			$this->db->join('locations loc', 'ird.location_id=loc.id', 'left');
			$this->db->where('ird.unique_code', $params['unique_code']);

			$productsGet = $this->db->get()->result_array();

			$jsonData['warehouse_id'] = intval($productsGet[0]['warehouse_id']);
			$productsArray['product_id'] = intval($productsGet[0]['product_id']);
			$productsArray['qty'] = intval($productsGet[0]['qty']);
			$productsArray['uom_id'] = intval($productsGet[0]['uom_id']);
			$productsArray['pallet'] = $params['unique_code'];
			//$productsArray['grade'] = $params['grade'];
			$productsArray['location_id'] = intval($params['location_id']);

			$jsonData['products'] = array($productsArray);

			$log_param['name'] = 'post_qc_in';
			$log_param['api_url'] = $this->service_url . $this->target_api_qc_in;
			$log_param['parameter'] = json_encode($jsonData);
			$insert_log = $this->insert_log($log_param);

			$api_param['target_api'] = $this->target_api_qc_in;
			$api_param['json_param'] = json_encode($jsonData);

			$call_qc_api = $this->call_api($api_param);

			$log_param['id'] = $insert_log;
			$log_param['response'] = $call_qc_api;
			$this->update_log($log_param);

			$response = json_decode($call_qc_api, true);

			if(isset($response['error'])) {
				// echo $response['error']['data']['message'];
			}
		}

		return $result;

	}

	function post_qc_out($params){

		$result = false;

		if($params) {
			$jsonData = array();
			$productsArray = array();

			$this->db->select('user_name');
			$this->db->from('users');
			$this->db->where('id', $params['user_id']);
			$operator = $this->db->get()->result_array()[0]['user_name'];

			$jsonData['ref'] = '-';
			$jsonData['date'] = date('Y-m-d');
			$jsonData['vehicle'] = '-';
			$jsonData['operator'] = $operator;

			$this->db->select(['ird.unique_code as pallet', 'ird.last_qty as qty', 'ird.tgl_exp as date_expired', 'i.unit_id as uom_id', 'ird.item_id as product_id', "COALESCE(loc.warehouse_id, '2') as warehouse_id"]);
			$this->db->from('item_receiving_details ird');
			$this->db->join('items i', 'ird.item_id = i.id');
			$this->db->join('locations loc', 'ird.location_id=loc.id', 'left');
			$this->db->where('ird.unique_code', $params['unique_code']);

			$productsGet = $this->db->get()->result_array();

			$jsonData['warehouse_id'] = intval($productsGet[0]['warehouse_id']);
			$productsArray['product_id'] = intval($productsGet[0]['product_id']);
			$productsArray['qty'] = intval($productsGet[0]['qty']);
			$productsArray['uom_id'] = intval($productsGet[0]['uom_id']);
			$productsArray['pallet'] = $params['unique_code'];
			$productsArray['grade'] = $params['grade'];
			$productsArray['date_expired'] = $params['date_expired'];
			$productsArray['location_id'] = intval($params['location_id']);

			$jsonData['products'] = array($productsArray);

			$log_param['name'] = 'post_qc_out';
			$log_param['api_url'] = $this->service_url . $this->target_api_qc_out;
			$log_param['parameter'] = json_encode($jsonData);
			$insert_log = $this->insert_log($log_param);

			$api_param['target_api'] = $this->target_api_qc_out;
			$api_param['json_param'] = json_encode($jsonData);

			$call_qc_api = $this->call_api($api_param);

			$log_param['id'] = $insert_log;
			$log_param['response'] = $call_qc_api;
			$this->update_log($log_param);

			$response = json_decode($call_qc_api, true);

			if(isset($response['error'])) {
				// echo $response['error']['data']['message'];
			}
		}

		return $result;

	}
	/* --END QC-- */

	/* --BEGIN BIN TRANSFER-- */
	function post_bin_transfer($params){

		$result = false;

		if($params) {
			$jsonData = array();
			$productsArray = array();

			//$bin_transfer = $this->db->get_where('bin_transfer',['bin_transfer_id'=>$params['ref']])->row_array();
			if(isset($params['location_dest_id'])) {
				$location_data = $this->db->get_where('locations',['id' => intval($params['location_dest_id'])])->row_array();
			}

			if(isset($params['location_dest_name'])) {
				$location_data = $this->db->get_where('locations',['name' => strval($params['location_name'])])->row_array();
				$params['location_id'] = intval($location_data['id']);
				$location_data = $this->db->get_where('locations',['name' => strval($params['location_dest_name'])])->row_array();
				$params['location_dest_id'] = intval($location_data['id']);
			}

			if(isset($params['operator'])) {
				$operator = $params['operator'];
			}
			if(isset($params['user_id'])) {
				$users = $this->db->get_where('users',['id'=>$params['user_id']])->row_array();
				$operator = $users['user_name'];
			}
			if(isset($params['user_name'])) {
				$operator = $params['user_name'];
			}

			$jsonData['ref'] = $params['ref'];
			$jsonData['date'] = date('Y-m-d');
			$jsonData['operator'] = $operator;
			$jsonData['warehouse_id'] = intval($location_data['warehouse_id']);
			// $jsonData['warehouse_id'] = '';

			$this->db->select(['ird.unique_code as pallet', 'ird.last_qty as qty', 'i.unit_id as uom_id', 'ird.item_id as product_id']);
			$this->db->from('item_receiving_details ird');
			$this->db->join('items i', 'ird.item_id = i.id');
			$this->db->where('ird.unique_code', $params['unique_code']);

			$productsGet = $this->db->get()->result_array();

			$productsArray['product_id'] = intval($productsGet[0]['product_id']);
			$productsArray['qty'] = intval($productsGet[0]['qty']);
			$productsArray['uom_id'] = intval($productsGet[0]['uom_id']);
			$productsArray['pallet'] = $params['unique_code'];
			$productsArray['location_id'] = intval($params['location_id']) - 110;
			$productsArray['location_dest_id'] = intval($params['location_dest_id']) - 110;

			$jsonData['products'] = array($productsArray);

			$file = '/var/www/html/largo/indofresh/debug/postBinTransfer_param.txt';
			// file_put_contents($file, date('Y-m-d H:i:s') ."\n". json_encode($jsonData) ."\n\n", FILE_APPEND);
			$log_param['name'] = 'post_bin_transfer';
			$log_param['api_url'] = $this->service_url . $this->target_api_bin_transfer;
			$log_param['parameter'] = json_encode($jsonData);
			$insert_log = $this->insert_log($log_param);

			$api_param['target_api'] = $this->target_api_bin_transfer;
			$api_param['json_param'] = json_encode($jsonData);

			$call_bin_transfer_api = $this->call_api($api_param);

			$file = '/var/www/html/largo/indofresh/debug/postBinTransfer_result.txt';
			// file_put_contents($file, date('Y-m-d H:i:s') ."\n". $call_bin_transfer_api ."\n\n", FILE_APPEND);
			$log_param['id'] = $insert_log;
			$log_param['response'] = $call_bin_transfer_api;
			$this->update_log($log_param);

			$response = json_decode($call_bin_transfer_api, true);

			if(isset($response['error'])) {
				// echo $response['error']['data']['message'];
			}
		}

		return $result;

	}
	/* --END BIN TRANSFER-- */

	/* --BEGIN SPLIT ITEMS-- */
	function post_split_items($serial_number_old = '', $serial_number_new = '', $qty = ''){

		$result = false;

		$jsonData = array();

		$this->db->select(["TO_CHAR(NOW() :: DATE, 'yyyy-mm-dd') AS date", "w.id AS warehouse_id", "ird.location_id AS location_id", "u.user_name AS operator", "itm.id AS product_id", "itm.unit_id AS uom", "ird.grade AS grade", "ird.tgl_exp AS date_expired"]);
		$this->db->from('item_receiving_details ird');
		$this->db->join('item_receiving ir', 'ird.item_receiving_id = ir.id');
		$this->db->join('items itm', 'ir.item_id = itm.id');
		$this->db->join('receivings rcv', 'rcv.id = ir.receiving_id');
		$this->db->join('locations loc', 'ird.location_id = loc.id');
		$this->db->join('warehouses w', 'loc.warehouse_id = w.id');
		$this->db->join('users u', 'ird.user_id_putaway = u.id');
		$this->db->where('ird.unique_code', $serial_number_new);

		$productsGet = $this->db->get();

		if($productsGet->num_rows() > 0) {
			$productsArray = array();
			$productsData = $productsGet->result_array();

			$jsonData['date'] = $productsData[0]['date'];
			$jsonData['warehouse_id'] = intval($productsData[0]['warehouse_id']) - 0;
			$jsonData['ref'] = '-';
			$jsonData['operator'] = $productsData[0]['operator'];

			for($j = 0; $j < $productsGet->num_rows(); $j++) {
				$productsArray['product_id'] = intval($productsData[$j]['product_id']) - 0;
				$productsArray['qty'] = intval($qty) - 0;
				$productsArray['uom_id'] = intval($productsData[$j]['uom']) - 0;
				$productsArray['grade'] = $productsData[$j]['grade'];
				$productsArray['date_expired'] = $productsData[$j]['date_expired'];
				$productsArray['pallet_old'] = $serial_number_old;
				$productsArray['pallet_new'] = $serial_number_new;
				$productsArray['location_id'] = intval($productsData[$j]['location_id']) - 110;
			}

			$jsonData['products'] = array($productsArray);
		}

		$file = '/var/www/html/largo/indofresh/debug/postSplitItems_param.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". json_encode($jsonData) ."\n\n", FILE_APPEND);
		$log_param['name'] = 'post_split_items';
		$log_param['api_url'] = $this->service_url . $this->target_api_split_items;
		$log_param['parameter'] = json_encode($jsonData);
		$insert_log = $this->insert_log($log_param);

		$api_param['target_api'] = $this->target_api_split_items;
		$api_param['json_param'] = json_encode($jsonData);

		$call_receipt_api = $this->call_api($api_param);

		$file = '/var/www/html/largo/indofresh/debug/postSplitItems_result.txt';
		// file_put_contents($file, date('Y-m-d H:i:s') ."\n". $call_receipt_api ."\n\n", FILE_APPEND);
		$log_param['id'] = $insert_log;
		$log_param['response'] = $call_receipt_api;
		$this->update_log($log_param);

		$response = json_decode($call_receipt_api, true);

		if(isset($response['error'])) {
			// echo $response['error']['data']['message'];
		}

		return $result;

	}
	/* --END SPLIT ITEMS-- */

	function call_api($params = array()) {
		$result = false;

		$target_api = $params['target_api'];
		$json_param = $params['json_param'];

		$curl = curl_init($this->service_url . $target_api);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
					array(
						"Authorization: Bearer " . $this->token,
						"Content-Type: application/json"
						)
					);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_param);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

		$result = curl_exec($curl);
		curl_close($curl);

		return $result;
	}

	function insert_log($params = array()) {
		$result = false;

		$name = $params['name'];
		$api_url = $params['api_url'];
		$parameter = $params['parameter'];
		$parameter_time = date('Y-m-d H:i:s.u');

		$log_insert    		=   array(
                                'name'    					=> $name,
                                'api_url'   		 		=> $api_url,
                                'parameter'	    			=> $parameter,
                                'parameter_time'    		=> $parameter_time,
                            );
		$this->db->insert('integration_log', $log_insert);
		$result = $this->db->insert_id();

		return $result;
	}

	function update_log($params = array()) {
		$result = false;

		$id = $params['id'];
		$response = $params['response'];
		$response_time = date('Y-m-d H:i:s.u');

		$this->db->set('response', $response);
		$this->db->set('response_time', $response_time);
		$this->db->where('id', $id);
		$this->db->update('integration_log');

		return $result;
	}
}

<?php
class referensi_kategori_2_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'kategori_2';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_kategori_2'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_kategori_2= $this->input->post("kd_kategori_2");
        $nama_kategori_2= $this->input->post("nama_kategori_2");

        if(!empty($kd_kategori_2)){
            $condition["a.kd_kategori_2 like '%$kd_kategori_2%'"]=null;
        }


        if(!empty($nama_kategori_2)){
            $condition["a.nama_kategori_2 like '%$nama_kategori_2%'"]=null;
        }

        
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.id_kategori_2');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_kategori_2;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
            $action .= '</li>';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_kategori_2' => $value->kd_kategori_2,
                    'nama_kategori_2' => $value->nama_kategori_2,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'kd_kategori_2' => $value->kd_kategori_2,
                    'nama_kategori_2' => $value->nama_kategori_2,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_kategori_2' => $id));
    }

    public function delete($id) {
        //return $this->db->delete($this->table, array('id_kategori_2' => $id));
		
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS t 
				FROM 
					barang 
				WHERE 
					id_kategori_2=\''.$id.'\'';
					
		$row = $this->db->query($sql)->row_array();
		if($row['t'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('id_kategori_2' => $id));
			
			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}
			
        return $result;
    }
    
    public function options($default = '--Pilih Kode Kategori 2--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_kategori_2] = $row->nama_kategori_2 ;
        }
        return $options;
    }

    
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_model extends CI_Model {

	private $table = "users";
	private $table2 = "users_warehouses";

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}

	function zone(){
		$zone_data = array();
		$zones = $this->db
					->select('id,name,description')
					->from('location_areas')
					->get()
					->result();
		foreach($zones as $zone){
			$zone_data[$zone->id] = $zone->name;
		}
		return $zone_data;
	}


	function get_all(){
		$this->db->select('*, id as user_id');
		$this->db->order_by('handheld_session_code');
		return $this->data()->get();
	}

	function get_data($condition=array()){

		if($condition){
			foreach($condition as $cond){
				$this->db->where($cond);
			}
		}
		$this->db->order_by('handheld_session_code');
		return $this->data()->get();

	}

	private function data($condition = array()) {
		$this->db->select("*, id as user_id");
		$this->db->from($this->table." a");
		$this->db->join("hr_grup b", "a.grup_id = b.grup_id");
		$this->db->where_condition($condition);

		return $this->db;
	}

	public function get_user_with_id($id) {
		return $this->data(array('id' => $id))->get();
	}

	public function login($username='',$password=''){
		// $this->db->select('*, id as user_id');
		// $this->db->from($this->table);
		// // $this->db->where('user_name',$username);
		// $this->db->where("user_name='$username' or nik='$username'");
		// $this->db->where('user_password',md5($password));
		// $res = $this->db->get();
		$sql = "select *, id as user_id from users u where (user_name ='".$username."' or nik='".$username."') and user_password='".md5($password)."'";
		$res = $this->db->query($sql);
		return $res->row();
	}

	public function create($data) {
        $this->db->trans_start();

		$warehouses = $data['warehouse'];
		$zones = $data['zone'];

		unset($data['warehouse']);
		unset($data['zone']);

		$this->db->insert($this->table, $data);

        // Get document_id after Insert
        $userId = $this->db->insert_id();

        // Create relation of document & warehouse
		$this->addUserWarehouse($userId, $warehouses);
		$this->addUserZones($userId, $zones);

        $this->db->trans_complete();

        return $this->db->trans_status();
	}



	public function update($data, $id) {
        $this->db->trans_start();

		$warehouses = $data['warehouse'];
		$zones = $data['zone'];

		unset($data['warehouse']);
		unset($data['zone']);

        $this->db->update($this->table, $data, array('id' => $id));

		$this->updateUserWarehouse($id,$warehouses);
		$this->updateUserZone($id, $zones);


        $this->db->trans_complete();

        return $this->db->trans_complete();
	}

	public function delete($id) {
		return $this->db->delete($this->table, array('id' => $id));
	}

	public function checkOldPassword($post = array()){
		$result = array();

		$this->db->select('COUNT(*) AS total');
		$this->db->where('id', $post['user_id']);
		$this->db->where('user_password', md5($post['old']));

		$row = $this->db->get($this->table)->row_array();
		if($row['total'] > 0){
			$result['status'] = 'OK';
			$result['message'] = '';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Invalid old password';
		}

		return $result;
	}

	public function saveNewPassword($post = array()){
		$result = array();

		$this->db->set('user_password', md5($post['new_password']));
		$this->db->where('id',$post['user_id']);
		$q = $this->db->update($this->table);
		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Change password success. Use new password in next login';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Change password failed';
		}

		return $result;
	}

    public function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    // Select, Insert, Update of Table documents_warehouses

    public function getUserWarehouse($userId){

        $this->db
            ->select('warehouse_id')
            ->from($this->table2)
            ->where('user_id', $userId);

        return $this->db->get()->result_array();

	}

	public function getUserZones($userId){
		$this->db
            ->select('zone_id')
            ->from('users_zone')
            ->where('user_id', $userId);

        return $this->db->get()->result_array();
	}

    public function addUserWarehouse($userId,$whId){

    	if(!empty($whId)){

	        $this->db->trans_start();

	        $data = array();

	        if(is_array($whId)){
	            foreach ($whId as $wh) {
	                $data[] = array(
	                    "user_id" => $userId,
	                    "warehouse_id" => $wh
	                );
	            }

	            $this->db->insert_batch($this->table2, $data);
	        }else{
	            $data = array(
	                "user_id" => $userId,
	                "warehouse_id" => $whId
	            );

	            $this->db->insert($this->table2, $data);
	        }

	        $this->db->trans_complete();

	        return $this->db->trans_status();

	    }

	}

	public function addUserZones($userId,$zoneId){

    	if(!empty($zoneId)){

	        $this->db->trans_start();

	        $data = array();

	        if(is_array($zoneId)){
	            foreach ($zoneId as $zone) {
	                $data[] = array(
	                    "user_id" => $userId,
	                    "zone_id" => $zone
	                );
	            }
	            $this->db->insert_batch('users_zone', $data);
	        }else{
	            $data = array(
	                "user_id" => $userId,
	                "zone_id" => $zoneId
	            );

	            $this->db->insert('users_zone', $data);
	        }

	        $this->db->trans_complete();

	        return $this->db->trans_status();

	    }

    }

    public function updateUserWarehouse($userId, $whId){

        $this->db->trans_start();

        $this->db
            ->where('user_id', $userId)
            ->delete($this->table2);

        $this->addUserWarehouse($userId,$whId);

        $this->db->trans_complete();

	}

	function updateUserZone($userId, $zoneId){
		$this->db->trans_start();

		$user = $this->db
					->select('user_id', $userId)
					->from('users_zone')
					->get()
					->result();
		if($user){

			$this->db
			->where('user_id', $userId)
			->delete('users_zone');
		}

		$this->addUserZones($userId, $zoneId);

		$this->db->trans_complete();
	}

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        $insert = array();

        for ($i=0; $i < $dataLen ; $i++) {

        	$role = $this->db->get_where('hr_grup',['grup_nama'=>$data[$i]['role']])->row_array();
        	if(!$role){
    			return array(
    				'status' 	=> false,
    				'message'	=> "Role '".$data[$i]['role']."' doesn't not exists"
    			);
        	}

        	$password = (strtoupper($data[$i]['user_password']) == "DEFAULT") ? "password" : $data[$i]['user_password'];

            $insert = array(
            	'nik'			=>	$data[$i]['nik'],
            	'nama'			=> 	$data[$i]['name'],
            	'user_name'		=> 	$data[$i]['user_name'],
            	'user_password' => 	md5($password),
            	'grup_id'		=>	$role['grup_id'],
            	'is_active'		=>  't'
            );

            $this->db
                ->from($this->table)
                ->or_where('lower(user_name)', strtolower($data[$i]['user_name']));

            $check = $this->db->get()->row_array();

            if($check){

	            $id = $check['id'];

            	$this->db->where('id',$id);
            	$this->db->update($this->table,$insert);

            }else{

            	$this->db->insert($this->table,$insert);
	            $id = $this->db->insert_id();

            }

            if(!empty($data[$i]['warehouse'])){

	            if(strtolower($data[$i]['warehouse']) == "all"){

	            	$sql = "INSERT INTO users_warehouses(user_id,warehouse_id) SELECT usr.id as user_id, wh.id as warehouse_id FROM users usr CROSS JOIN warehouses wh where usr.id=$id";
	            	$this->db->query($sql);

	            }else{

	            	$warehouses = str_replace(", ", ",", $data[$i]['warehouse']);
	            	$warehouses = str_replace(" ,", ",", $warehouses);

	            	$warehouse = explode(",", $warehouses);

	            	$postWarehouse= array();
	            	foreach ($warehouse as $wh) {

	            		$getWarehouse = $this->db->get_where('warehouses',['lower(name)'=>strtolower($wh)])->row_array();
	            		if(!$getWarehouse){

	            			return array(
	            				'status' 	=> false,
	            				'message'	=> "Warehouse '".$wh."' doesn't not exists"
	            			);

	            		}else{

		            		$postWarehouse[] = array(
		            			'user_id' => $id,
		            			'warehouse_id' => $getWarehouse['id']
		            		);
		            	}

	            	}

	            	$this->db->insert_batch('users_warehouses',$postWarehouse);

	            }

            }else{

				return array(
					'status' 	=> false,
					'message'	=> "Warehouse column must be filled!"
				);

            }

        }

        $this->db->trans_complete();

        return array(
        	'status'	=> true
        );
    }

}

<?php
class item_stok_by_location_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $loc     = 'locations';

	public function getStockCardTotal($id){
		$result = array();

		$sql = "SELECT COUNT(*) AS total
				FROM
					(
						(
							SELECT
								TO_CHAR(finish_putaway, 'DD/MM/YYYY') AS date,
								finish_putaway AS date_sort,
								(
									SELECT
										COUNT(*)
									FROM
										item_receiving_details irds
									JOIN item_receiving irs
										ON irs.id=irds.item_receiving_id
									JOIN items itms
										ON itms.id=irs.item_id
									WHERE
										irs.receiving_id=rcv.id
									AND
										irs.item_id=itm.id
								) AS qty, 'IN' AS type, user_name, rcv.code AS doc,
								rcv.id AS id_doc
							FROM
								receivings rcv
							JOIN item_receiving ir
								ON ir.receiving_id=rcv.id
							JOIN item_receiving_details ird
								ON ird.item_receiving_id=ir.id
							JOIN items itm
								ON itm.id=ir.item_id
							LEFT JOIN users usr
								ON usr.id=rcv.user_id
							WHERE
								ir.item_id=$id
						)
						UNION
						(
							SELECT
								TO_CHAR(shipping_finish, 'DD/MM/YYYY') AS date,
								shipping_finish AS date_sort,
								(
									SELECT
										COUNT(*)
									FROM
										item_receiving_details irds
									JOIN item_receiving irs
										ON irs.id=irds.item_receiving_id
									JOIN items itms
										ON itms.id=irs.item_id
									WHERE
										irds.shipping_id=shipp.shipping_id
									AND
										irs.item_id=itm.id
								) AS qty, 'OUT' AS type, user_name, shipp.code AS doc,
								shipp.shipping_id AS id_doc
							FROM
								shippings shipp
							JOIN item_receiving_details ird
								ON ird.shipping_id=shipp.shipping_id
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN items itm
								ON itm.id=ir.item_id
							LEFT JOIN users usr
								ON usr.id=shipp.user_id
							WHERE
								ir.item_id=$id
						)
					) AS tbl";

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

	public function getArea(){
		$this->db
			->select('id as id_qc, name as kd_qc')
			->from('location_areas la')
			->order_by('la.name');

		$result = $this->db->get();
		return $result->result_array();
	}

	public function getStockCard($post = array()){
		$result = array();

		$offset = 10;
		$limit = ' 0, ' . $offset;

		$where = "";

		if(isset($post['start'])){
			if(!empty($post['start']))
				$limit = ' ' . $post['start'].', '.$offset;
		}

		if(!empty($post['warehouse'])){

			if(is_array($post['warehouse'])){
				$wh = implode(',', $post['warehouse']);
			}else{
				$wh = $post['warehouse'];
			}

			$where .= "AND wh.id IN ($wh)";

		}

		$idBarang = $post['id_barang'];

		$userId = $this->session->userdata('user_id');

		$sql = "SELECT *
				FROM
					(
						(
							SELECT
								COALESCE(TO_CHAR(COALESCE(finish_putaway,start_putaway), 'DD/MM/YYYY'), '-') AS date,
								TO_CHAR(COALESCE(finish_putaway, start_putaway), 'DD/MM/YYYY') AS date_sort,
								COALESCE(SUM(first_qty),0) AS qty,
								'IN' AS type, COALESCE(user_name, '-') AS user_name, rcv.code as doc,
								rcv.id  AS id_doc, '' AS balance, 'RCV' as type_doc,
								COALESCE(NULL,0) as system_qty, COALESCE(NULL,0) as scanned_qty
							FROM
								item_receiving_details ird
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN items itm
								ON itm.id=ir.item_id
							JOIN receivings rcv
								ON rcv.id=ir.receiving_id
							JOIN inbound inb
								ON inb.id_inbound=rcv.po_id
							JOIN warehouses wh
								ON wh.id=inb.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							LEFT JOIN users usr
								ON usr.id=rcv.user_id
							WHERE
								ir.item_id=$idBarang
							$where
							group by
								rcv.id, usr.user_name
						)
						UNION
						(
							SELECT
								TO_CHAR(COALESCE(end_time,start_time), 'DD/MM/YYYY') AS date,
								TO_CHAR(COALESCE(end_time,start_time), 'DD/MM/YYYY') AS date_sort,
								COALESCE(SUM(pr.qty),0) AS qty,
								'OUT' AS type, COALESCE(user_name, '-') AS user_name, pl.name as doc,
								pl.pl_id  AS id_doc, '' AS balance, 'PICK' as type_doc,
								COALESCE(NULL,0) as system_qty, COALESCE(NULL,0) as scanned_qty
							FROM
								pickings pl
							JOIN picking_list_outbound plo
								ON plo.pl_id=pl.pl_id
							JOIN outbound outb
								ON outb.id=plo.id_outbound
							JOIN warehouses wh
								ON wh.id=outb.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							JOIN picking_recomendation pr
								ON pr.picking_id=pl.pl_id
							JOIN item_receiving_details ird
								ON ird.unique_code=pr.unique_code
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN items itm
								ON itm.id=ir.item_id
							LEFT JOIN users usr
								ON usr.id=pl.user_id
							WHERE
								ir.item_id=$idBarang
							$where
							group by
								pl.pl_id, usr.user_name
						)
						UNION
						(
							SELECT
								TO_CHAR(COALESCE(ro.date), 'DD/MM/YYYY') AS date,
								TO_CHAR(COALESCE(ro.date), 'DD/MM/YYYY') AS date_sort,
								COALESCE(SUM(rop.qty),0) AS qty,
								'OUT' AS type, COALESCE(user_name, '-') AS user_name, ro.code as doc,
								ro.id  AS id_doc, '' AS balance, 'REPACK' as type_doc,
								COALESCE(NULL,0) as system_qty, COALESCE(NULL,0) as scanned_qty
							FROM
								repack_orders ro
							JOIN warehouses wh
								ON wh.id=ro.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							JOIN repack_order_picking rop
								ON rop.repack_order_id=ro.id
							JOIN item_receiving_details ird
								ON ird.unique_code=rop.unique_code
							JOIN item_receiving ir
								ON ir.id=ird.item_receiving_id
							JOIN items itm
								ON itm.id=ir.item_id
							LEFT JOIN users usr
								ON usr.id=ro.user_id
							WHERE
								ir.item_id=$idBarang
							$where
							group by
								ro.id, usr.user_name
						)UNION(
							SELECT
								TO_CHAR(COALESCE(cc.time), 'DD/MM/YYYY') AS date,
								TO_CHAR(COALESCE(cc.time), 'DD/MM/YYYY') AS date_sort,
								COALESCE(SUM(ccr.qty),0) AS qty,
								'IN' AS type, COALESCE(user_name, '-') AS user_name, cc.code as doc,
								cc.cc_id  AS id_doc, '' AS balance, 'CC' as type_doc,
								COALESCE(sum(system_qty),0) as system_qty, COALESCE(sum(scanned_qty),0) as scanned_qty
							FROM
								cycle_counts cc
							JOIN warehouses wh
								ON wh.id=cc.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
							JOIN items itm
								ON itm.id=ccr.item_id
							JOIN users usr
								ON usr.id=cc.user_id
							Where
								ccr.item_id=$idBarang
							AND
								ccr.status='ADJUST TO SCANNED QTY'
							$where
							GROUP BY
								cc.cc_id, usr.user_name, ccr.system_qty, ccr.scanned_qty
							HAVING
								(system_qty < scanned_qty)
						)UNION(
							SELECT
								TO_CHAR(COALESCE(cc.time), 'DD/MM/YYYY') AS date,
								TO_CHAR(COALESCE(cc.time), 'DD/MM/YYYY') AS date_sort,
								COALESCE(SUM(ccr.qty),0) AS qty,
								'IN' AS type, COALESCE(user_name, '-') AS user_name, cc.code as doc,
								cc.cc_id  AS id_doc, '' AS balance, 'CC' as type_doc,
								COALESCE(sum(system_qty),0) as system_qty, COALESCE(sum(scanned_qty),0) as scanned_qty
							FROM
								cycle_counts cc
							JOIN warehouses wh
								ON wh.id=cc.warehouse_id
							JOIN users_warehouses uw
								ON uw.warehouse_id=wh.id AND  uw.user_id=$userId
							JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
							JOIN items itm
								ON itm.id=ccr.item_id
							JOIN users usr
								ON usr.id=cc.user_id
							Where
								ccr.item_id=$idBarang
							AND
								ccr.status='ADJUST TO SCANNED QTY'
							$where
							GROUP BY
								cc.cc_id, usr.user_name, ccr.system_qty, ccr.scanned_qty
							HAVING
								(system_qty > scanned_qty)
						)
					) AS tbl
					ORDER BY
						date_sort DESC";

		$row = $this->db->query($sql)->result_array();

		$this->db->select(array("name as nama_barang","code as kd_barang"));
		$this->db->from('items');
		$this->db->where('id', $post['id_barang']);

		$item = $this->db->get()->row_array();

		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['item_name'] = $item['nama_barang'];
		$result['item_code'] = $item['kd_barang'];

		return $result;
	}

	public function getStatus(){
		$result = array();

		$this->db
			->select('id as id_qc, code as kd_qc')
			->from('qc')
			->order_by('code');

		$result = $this->db->get();

		return $result->result_array();
	}

	public function getCategory(){
		$result = array();

		$sql = 'SELECT
					id_kategori, kd_kategori, CONCAT(kd_kategori, \' - \', nama_kategori) AS nama_kategori
				FROM
					kategori';

		$this->db
			->select([
					"id as id_kategori","code as kd_kategori","CONCAT(code, ' - ', name) as nama_kategori"
			])
			->from('categories');

		$result = $this->db->get();

		return $result->result_array();
	}

	public function getLocation(){

		$result = array();

		$this->db
			->select('id as loc_id, name as loc_name')
			->from('locations loc')
			->where('name IS NOT NULL', NULL)
			->where('loc_type <>','SENTOUT')
			->order_by('id');

		$result = $this->db->get();
		return $result->result_array();

	}

	public function getLocationType(){
		$result = array();

		$result = array(
			'INBOUND'	=> 'INBOUND',
			'TRANSIT'	=> 'TRANSIT',
			'BINLOC'	=> 'BINLOC',
			'OUTBOUND'	=> 'OUTBOUND',
			'QC'		=> 'QC',
			'NG'		=> 'NG',
		);

		return $result;
	}

	public function getUom(){
		$result = array();

		$this->db
			->select('id as id_satuan, name as nama_satuan')
			->from('units');

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getItem(){
		$result = array();

		$this->db
			->select(['id as id_barang',"CONCAT(code,' - ', name) as nama_barang"])
			->from('items');

		$result = $this->db->get()->result_array();

		return $result;
	}

  public function data($condition = array(),$tahun_aktif='') {

        $this->db->select("a.id as id_barang, a.name as nama_barang, a.code as kd_barang, b.code as kd_kategori, b.name as nama_kategori, c.name as nama_satuan, sh.code as shipment_type, a.fifo_period, a.has_qty, a.def_qty,
        	'' as kd_kategori_2, '' as nama_kategori_2", false);
        $this->db->from('items a');
        $this->db->join('categories b', 'b.id = a.category_id','left');
        $this->db->join('units c', 'c.id = a.unit_id','left');
        $this->db->join('shipment sh','sh.id=a.shipment_id','left');
		    $this->db->join('(SELECT ir.item_id , count(unique_code) as qty FROM item_receiving_details ird LEFT JOIN item_receiving ir ON ir.id=ird.item_receiving_id WHERE pl_status IS NULL and location_id NOT IN (104,101) GROUP BY ir.item_id) as rb ','a.id = rb.item_id','left');
        $this->db->order_by('a.code');
        $this->db->where_condition($condition);

        return $this->db;

    }

	public function getDetailList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		//NOTE :
		//CHANGE JOIN TO LEFT RECEIVING TO GET STOCK FROM CYCLE COUNT NOT FROM RECEIVING PROCESS

		$columns = array(
			0 => '',
			1 => '',
			2 => 'kd_unik',
			3 => 'kd_parent',
			4 => 'last_qty',
			5 => '',
			6 => 'tgl_exp',
			7 => 'tgl_in',
			8 => 'kd_qc'
		);

		$where = '';
		$params = '';

		$this->db->start_cache();

		if(!empty($post['location_id'])){
			$this->db->where('loc.id',$post['location_id']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		if(!empty($post['item_id'])){
			$this->db->where('ird.item_id', $post['item_id']);
		}

		$this->db
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('qc','qc.id=ird.qc_id','left')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('receivings r','r.id=ir.receiving_id','left')
			->join('inbound inb','inb.id_inbound=r.po_id','left')
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id','left')
			->join('outbound outb','outb.id=plo.id_outbound','left')
			->join('cycle_counts cc','cc.cc_id=ird.cycle_count_id','left')
			->join('repack_orders ro','ro.id=ird.repack_order_id','left')
			->join('items', 'items.id=ird.item_id', 'left')
			->join('units uom', 'uom.id=items.unit_id', 'left')
			->where_not_in('ird.location_id', [101,104]);

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db->select('unique_code as kd_unik, inb.id_inbound, inb.code as kd_inbound, ro.id as id_order_kitting, ro.code as kd_order_kitting, r.id as id_receiving, cc.cc_id, cc.code as cc_code, loc.loc_type, ird.shipping_id, outb.id as id_outbound, outb.code as kd_outbound, p.pl_id, p.name as pl_name, last_qty, uom.name as uom, loc.name as loc_name, parent_code as kd_parent, qc.code as kd_qc');

		$this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
		$this->db->limit($requestData['length'],  $requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$this->db->flush_cache();

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$docRef = $row[$i]['kd_inbound'];
			$idRef = $row[$i]['id_inbound'];
			$ctrlRef = 'inbound/detail/';
			$locType = $row[$i]['loc_type'];

			if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('inbound_name'))){

				$docRef = $row[$i]['kd_inbound'];
				$idRef = $row[$i]['id_inbound'];
				$ctrlRef = 'inbound/detail/';

				if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

					$docRef = $row[$i]['kd_order_kitting'];
					$idRef = $row[$i]['id_order_kitting'];
					$ctrlRef = 'order_kitting/detail/';

				}

			}else if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

				$docRef = $row[$i]['kd_order_kitting'];
				$idRef = $row[$i]['id_order_kitting'];
				$ctrlRef = 'order_kitting/detail/';

			}else if(!empty($row[$i]['cc_id']) && empty($row[$i]['id_receiving'])){

				$docRef = $row[$i]['cc_code'];
				$idRef = $row[$i]['cc_id'];
				$ctrlRef = 'cycle_count/detail/';

			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('outbound_name'))){

				$docRef = $row[$i]['kd_outbound'];
				$idRef = $row[$i]['id_outbound'];
				$ctrlRef = 'outbound/detail/';

			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('transit_location'))){

				$this->db
					->select('ir.receiving_id as id_receiving, picking_id as pl_id, shipping_id, location_id as loc_id')
					->from('item_receiving_details ird')
					->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
					->where('unique_code', $row[$i]['kd_unik']);

				$r = $this->db->get()->row_array();

				if(empty($r['pl_id'])){

					$docRef = $row[$i]['kd_inbound'];
					$idRef = $row[$i]['id_inbound'];
					$ctrlRef = 'inbound/detail/';

					$locType = $locType;

				}else if(!empty($r['cc_id']) && $r['id_qc'] == '4'){

					$docRef = $row[$i]['cc_code'];
					$idRef = $row[$i]['cc_id'];
					$ctrlRef = 'cycle_count/detail/';

				}else if(!empty($r['pl_id']) && empty($r['shipping_id'])){

					$docRef = $row[$i]['kd_outbound'];
					$idRef = $row[$i]['id_outbound'];
					$ctrlRef = 'outbound/detail/';

					$locType = $locType;

				}

			}

			$exp = '-';
			$dIn = '-';

			if(!empty($row[$i]['tgl_exp']) && $row[$i]['tgl_exp'] != '0000-00-00')
				$exp = date('d/m/Y', strtotime($row[$i]['tgl_exp']));

			if(!empty($row[$i]['tgl_in']) && $row[$i]['tgl_in'] != '0000-00-00 00:00:00')
				$dIn = date('d/m/Y', strtotime($row[$i]['tgl_in']));
			$kd_parent = !empty($row[$i]['kd_parent']) ? $row[$i]['kd_parent'] : '-';
			$nested = array();
			if(empty($post['item_id'])){
			$nested[] = '<input type="checkbox" name="sn[]" value="'.$row[$i]['kd_unik'].'">';
			}
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $kd_parent;
			$nested[] = $row[$i]['last_qty'].' '.$row[$i]['uom'];
			$nested[] = '<a href="'.base_url().$ctrlRef.$idRef.'">'.$docRef.'</a>';
			$nested[] = $exp;
			$nested[] = $dIn;
			$nested[] = $row[$i]['kd_qc'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailListExport($post=array()){

		$where = '';
		$params = '';

		if(!empty($post['location_id'])){
			$this->db->where('loc.id',$post['location_id']);
		}

		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db->select('unique_code as kd_unik, inb.id_inbound, inb.code as kd_inbound, ro.id as id_order_kitting, ro.code as kd_order_kitting, r.id as id_receiving, cc.cc_id, cc.code as cc_code, loc.loc_type, ird.shipping_id, outb.id as id_outbound, outb.code as kd_outbound, p.pl_id, p.name as pl_name, last_qty, loc.name as loc_name, parent_code as kd_parent, qc.code as kd_qc');

		$this->db
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('qc','qc.id=ird.qc_id','left')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('receivings r','r.id=ir.receiving_id','left')
			->join('inbound inb','inb.id_inbound=r.po_id','left')
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id','left')
			->join('outbound outb','outb.id=plo.id_outbound','left')
			->join('cycle_counts cc','cc.cc_id=ird.cycle_count_id','left')
			->join('repack_orders ro','ro.id=ird.repack_order_id','left')
			->where_not_in('ird.location_id', [101,104]);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$docRef = $row[$i]['kd_inbound'];
			$locType = $row[$i]['loc_type'];

			if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('inbound_name'))){

				$docRef = $row[$i]['kd_inbound'];

				if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

					$docRef = $row[$i]['kd_order_kitting'];

				}

			}else if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

				$docRef = $row[$i]['kd_order_kitting'];

			}else if(!empty($row[$i]['cc_id']) && empty($row[$i]['id_receiving'])){

				$docRef = $row[$i]['cc_code'];

			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('outbound_name'))){

				$docRef = $row[$i]['kd_outbound'];

			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('transit_location'))){

				$sql = 'SELECT
							id_receiving, cc_id, pl_id, shipping_id, id_qc, loc_id
						FROM
							receiving_barang
						WHERE
							kd_unik = \''.$row[$i]['kd_unik'].'\'';

				$r = $this->db->query($sql)->row_array();

				if(empty($r['pl_id'])){

					$docRef = $row[$i]['kd_inbound'];

					$locType = $locType;

				}else if(!empty($r['cc_id']) && $r['id_qc'] == '4'){

					$docRef = $row[$i]['cc_code'];

				}else if(!empty($r['pl_id']) && empty($r['shipping_id'])){

					$docRef = $row[$i]['kd_outbound'];

					$locType = $locType;

				}

			}

			$exp = '-';
			$dIn = '-';

			if(!empty($row[$i]['tgl_exp']) && $row[$i]['tgl_exp'] != '0000-00-00'){
				$exp = date('d/m/Y', strtotime($row[$i]['tgl_exp']));
			}

			if(!empty($row[$i]['tgl_in']) && $row[$i]['tgl_in'] != '0000-00-00 00:00:00'){
				$dIn = date('d/m/Y', strtotime($row[$i]['tgl_in']));
			}

			$kd_parent = !empty($row[$i]['kd_parent']) ? $row[$i]['kd_parent'] : '-';

			$nested = array();

			$nested[] = $i + 1;
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $kd_parent;
			$nested[] = $row[$i]['last_qty'];
			$nested[] = $docRef;
			$nested[] = $exp;
			$nested[] = $dIn;
			$nested[] = $row[$i]['kd_qc'];

			$data[] = $nested;
		}

		return $data;

	}

	public function getDetailListItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		//NOTE :
		//CHANGE JOIN TO LEFT RECEIVING TO GET STOCK FROM CYCLE COUNT NOT FROM RECEIVING PROCESS

		$columns = array(
			0 => '',
			1 => 'itm.code',
			2 => 'itm.name',
			3 => 'qty'
		);

		$where = '';
		$params = '';

		$this->db->start_cache();

		if(!empty($post['location_id'])){
			$this->db->where('loc.id',$post['location_id']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('items itm','itm.id=ird.item_id')
			->join('units unt', 'unt.id=itm.unit_id', 'left')
			->join('qc','qc.id=ird.qc_id','left')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('receivings r','r.id=ir.receiving_id','left')
			->join('inbound inb','inb.id_inbound=r.po_id','left')
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id','left')
			->join('outbound outb','outb.id=plo.id_outbound','left')
			->join('cycle_counts cc','cc.cc_id=ird.cycle_count_id','left')
			->join('repack_orders ro','ro.id=ird.repack_order_id','left')
			->where_not_in('ird.location_id', [101,104])
			->group_by('itm.id,itm.code,itm.name,unt.name');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		if(isset($row['total'])){
			$totalData = $row['total'];
		}else{
			$totalData = 0;
		}

		$this->db->select(["itm.code as item_code","unt.name as uom","itm.id as item_id","itm.name as item_name","COALESCE(sum(ird.last_qty),0) as qty"]);
		$this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
		$this->db->limit($requestData['length'],  $requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$this->db->flush_cache();

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'item_stok/detail/'.$row[$i]['item_id'].'">'.$row[$i]['item_code'].'</a>';
			$nested[] = $row[$i]['item_name'];
			$nested[] = $row[$i]['qty'].' '.$row[$i]['uom'];
			$nested[] = "";
			$nested[] = $row[$i]['item_id'];

			$data[] = $nested;

		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailListItemExport($post = array()){
		$data = array();

		$where = '';
		$params = '';

		if(!empty($post['location_id'])){
			$this->db->where('loc.id',$post['location_id']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select(["itm.code as item_code","itm.name as item_name","COALESCE(sum(ird.last_qty),0) as qty"])
			->from('item_receiving_details ird')
			->join('locations loc','loc.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('items itm','itm.id=ird.item_id')
			->join('qc','qc.id=ird.qc_id','left')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('receivings r','r.id=ir.receiving_id','left')
			->join('inbound inb','inb.id_inbound=r.po_id','left')
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id','left')
			->join('outbound outb','outb.id=plo.id_outbound','left')
			->join('cycle_counts cc','cc.cc_id=ird.cycle_count_id','left')
			->join('repack_orders ro','ro.id=ird.repack_order_id','left')
			->where_not_in('ird.location_id', [101,104])
			->group_by('itm.id,itm.code,itm.name');


		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $i + 1;
			$nested[] = $row[$i]['item_code'];
			$nested[] = $row[$i]['item_name'];
			$nested[] = $row[$i]['qty'];

			$data[] = $nested;

		}

		return $data;

	}


	public function getList($post=[]){

		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'loc.name',
			2 => 'cat.name',
			3 => 'loc.loc_type',
			4 => '',
			5 => 'qty',
		);

		$params = '';
		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if(!empty($post['id_kategori'])){
			$this->db->where_in('l.location_area_id', $post['id_kategori']);
		}

		if(!empty($post['loc_type'])){
			$this->db->where_in('l.loc_type',$post['loc_type']);
		}

		if(!empty($post['id_barang'])){
			$this->db->where_in('ird.item_id', $post['id_barang']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select([
				"ird.location_id",
				"string_agg(DISTINCT itm.id::varchar, ', ') as item_id_all",
				"string_agg(DISTINCT itm.name, ',<br/> ') as item_name_all",
				"COUNT(DISTINCT itm.id) as total_item",
				"COALESCE(sum(ird.last_qty) ,0) as qty"
			])
			->from('item_receiving_details ird')
			->join('locations l','l.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=l.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('items itm','itm.id=ird.item_id','left')
			->where_not_in('ird.location_id', [101,104])
			->group_by(['ird.location_id','itm.has_qty']);

		$rb = $this->db->get_compiled_select();

		$this->db->start_cache();

		if(!empty($post['id_barang'])){
			$idBarang = $post['id_barang'];
			$len = count($idBarang);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.item_id_all', $idBarang[$i]);
			}
		}

		if(!empty($post['id_kategori'])){
			$this->db->where_in('loc.location_area_id', $post['id_kategori']);
		}

		if(!empty($post['loc_id'])){
			$this->db->where_in('loc.id',$post['loc_id']);
		}

		if(!empty($post['loc_type'])){
			$this->db->where_in('loc_type',$post['loc_type']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
		->select(["loc.id as location_id", "loc.name as location_name", "COALESCE(loc.loc_type, 'BIN LOC') as loc_type", "loc_area.name as location_category", "item_name_all","total_item","COALESCE(rb.qty, 0) as qty, item_id_all"])
		->from('locations loc')
		->join('categories cat','cat.id=loc.location_category_id')
		->join('location_types loc_type','loc_type.id=loc.location_type_id')
		->join('location_areas loc_area','loc_area.id=loc.location_area_id')
		->join('warehouses wh','wh.id=loc_area.warehouse_id')
		->join('users_warehouses uw','uw.warehouse_id=wh.id')
		// ->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
		->join("($rb) as rb",'rb.location_id=loc.id','left')
		->group_by('loc.id, rb.item_name_all, rb.total_item, rb.qty, loc_area.name, rb.item_id_all');

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();
		
		$this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
		$this->db->limit($requestData['length'],  $requestData['start']);
		
		$row = $this->db->get()->result_array();
		
		
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'item_stok_by_location/detail/'.$row[$i]['location_id'].'">' . $row[$i]['location_name'] . '</a>';
			$nested[] = $row[$i]['location_category'];
			$nested[] = $row[$i]['loc_type'];
			$nested[] = $row[$i]['item_name_all'];
			// $nested[] = $row[$i]['qty'];
			$nested[] = $row[$i]['total_item'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;

	}

	public function getListRaw($post = array()){
		$result = array();

		$params = '';
		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if($params == 'receiving'){
			$this->db->where('l.loc_type','INBOUND');
		}

		if($params == 'onhand'){
			$locType = "(l.loc_type IS NULL OR l.loc_type='QC' OR l.loc_type='NG')";
			$this->db->where($locType, NULL);
		}

		if($params == 'allocated'){
		}

		if($params == 'suspend'){
			$this->db->where('ird.qc_id', 2);
		}

		if($params == 'damage'){
			$this->db->where('ird.qc_id', 3);
		}

		if($params == 'available'){
			$this->db->where('ird.qc_id', 1);
			$this->db->where('l.loc_type', NULL);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select([
				"itm.id as item_id","SUM(COALESCE(p.pl_id, 0)) as pl_id",
				"string_agg(DISTINCT CAST(qc_id as CHAR), ',') as id_qc_all",
				"string_agg(DISTINCT CAST(location_id as CHAR), ', ') as loc_id_all",
				"string_agg(DISTINCT l.loc_type, ', ') as loc_type_all",
				"string_agg(DISTINCT l.name, ',') as loc_name",
				"COALESCE((CASE WHEN itm.has_qty=1 THEN sum(ird.last_qty) ELSE COUNT(ird.unique_code) END),0) as qty"
			])
			->from('item_receiving_details ird')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('locations l','l.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=l.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('items itm','itm.id=ir.item_id','left')
			->where_not_in('ird.location_id', [101,104])
			->group_by(['itm.id','has_qty']);

		$rb = $this->db->get_compiled_select();

		$this->db->start_cache();

		if(!empty($post['id_barang']))
			$this->db->where_in('itm.id', $post['id_barang']);

		if(!empty($post['id_satuan']))
			$this->db->where_in('itm.unit_id', $post['id_satuan']);

		if(!empty($post['id_qc']))
			$this->db->where_in('rb.qc_id', $post['id_qc']);

		if(!empty($post['id_kategori']))
			$this->db->where_in('itm.category_id', $post['id_kategori']);

		if(!empty($post['loc_id'])){
			$locId = $post['loc_id'];
			$len = count($locId);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_id_all', $locId[$i]);
			}
		}

		if(!empty($post['loc_type'])){
			$locType = $post['loc_type'];
			$len = count($locType);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_type_all', $locType[$i]);
			}
		}

		if($params != ''){
			if($params == 'low'){
				$this->db->where("COALESCE(rb.qty,0) <= minimum_stock", NULL);
			}else{
				$this->db->where("rb.qty >", 0);
			}
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select(["itm.id as id_barang", "itm.name as nama_barang", "itm.code as kd_barang", "cat.name as nama_kategori", "unt.name as nama_satuan", "minimum_stock", "loc_name","loc_type_all","COALESCE(rb.qty, 0) as qty"])
			->from('items itm')
			->join('items_warehouses iw','iw.item_id=itm.id')
			->join('warehouses wh','wh.id=iw.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('categories cat','cat.id=itm.category_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->join("($rb) as rb",'rb.item_id=itm.id','left')
			->group_by('itm.id, cat.name, unt.name, rb.loc_name, rb.loc_type_all, rb.qty');

		$result = $this->db->get()->result_array();
		return $result;
	}

	public function getListRawFormat2($post = array()){
		$result = array();

		$params = '';
		if(!empty($post['params'])){
			$params = $post['params'];
		}

		if($params == 'receiving'){
			$this->db->where('l.loc_type','INBOUND');
		}

		if($params == 'onhand'){
			$locType = "(l.loc_type IS NULL OR l.loc_type='QC' OR l.loc_type='NG')";
			$this->db->where($locType, NULL);
		}

		if($params == 'allocated'){
		}

		if($params == 'suspend'){
			$this->db->where('ird.qc_id', 2);
		}

		if($params == 'damage'){
			$this->db->where('ird.qc_id', 3);
		}

		if($params == 'available'){
			$this->db->where('ird.qc_id', 1);
			$this->db->where('l.loc_type', NULL);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select([
				"itm.id as item_id","SUM(COALESCE(p.pl_id, 0)) as pl_id",
				"string_agg(DISTINCT CAST(qc_id as CHAR), ',') as id_qc_all",
				"string_agg(DISTINCT CAST(location_id as CHAR), ', ') as loc_id_all",
				"string_agg(DISTINCT l.loc_type, ', ') as loc_type_all",
				"string_agg(DISTINCT l.name, ',') as loc_name",
				"COALESCE((CASE WHEN itm.has_qty=1 THEN sum(ird.last_qty) ELSE COUNT(ird.unique_code) END),0) as qty"
			])
			->from('item_receiving_details ird')
			->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
			->join('locations l','l.id=ird.location_id','left')
			->join('location_areas loc_area','loc_area.id=l.location_area_id')
			->join('warehouses wh','wh.id=loc_area.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
			->join('pickings p','p.pl_id=ird.picking_id','left')
			->join('items itm','itm.id=ir.item_id','left')
			->where_not_in('ird.location_id', [101,104])
			->group_by(['itm.id','has_qty','location_id']);

		$rb = $this->db->get_compiled_select();

		$this->db->start_cache();

		if(!empty($post['id_barang']))
			$this->db->where_in('itm.id', $post['id_barang']);

		if(!empty($post['id_satuan']))
			$this->db->where_in('itm.unit_id', $post['id_satuan']);

		if(!empty($post['id_qc']))
			$this->db->where_in('rb.qc_id', $post['id_qc']);

		if(!empty($post['id_kategori']))
			$this->db->where_in('itm.category_id', $post['id_kategori']);

		if(!empty($post['loc_id'])){
			$locId = $post['loc_id'];
			$len = count($locId);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_id_all', $locId[$i]);
			}
		}

		if(!empty($post['loc_type'])){
			$locType = $post['loc_type'];
			$len = count($locType);

			for($i = 0; $i < $len; $i++){
				$this->db->like('rb.loc_type_all', $locType[$i]);
			}
		}

		if($params != ''){
			if($params == 'low'){
				$this->db->where("COALESCE(rb.qty,0) <= minimum_stock", NULL);
			}else{
				$this->db->where("rb.qty >", 0);
			}
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id',$post['warehouse']);
		}

		$this->db
			->select(["itm.id as id_barang", "itm.name as nama_barang", "itm.code as kd_barang", "cat.name as nama_kategori", "unt.name as nama_satuan", "minimum_stock", "loc_name","loc_type_all","COALESCE(rb.qty, 0) as qty"])
			->from('items itm')
			->join('items_warehouses iw','iw.item_id=itm.id')
			->join('warehouses wh','wh.id=iw.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('categories cat','cat.id=itm.category_id','left')
			->join('units unt','unt.id=itm.unit_id','left')
			->join("($rb) as rb",'rb.item_id=itm.id','left')
			->group_by('itm.id, cat.name, unt.name, rb.loc_name, rb.loc_type_all, rb.qty')
			->order_by('itm.code');

		$result = $this->db->get()->result_array();

		return $result;
	}

    public function get_by_id($id) {
        $condition['a.id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array(),$tahun_aktif) {
        $this->data($condition,$tahun_aktif);
        return $this->db->get();
    }

    public function options($default = '--Choose Item--', $key = '') {
        $data = $this->data_list()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->kd_barang.' / '.$row->nama_barang ;
        }
        return $options;
    }

}

?>

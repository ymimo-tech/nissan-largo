<?php
class referensi_kitting_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'repacks';
    private $table2 = 'repack_items';

    public function data($condition = array()) {

        if(!empty($condition['a.id_kitting'])){
            $condition['a.id'] = $condition['a.id_kitting'];
            unset($condition['a.id_kitting']);
        }

        $this->db->select('a.id as id_kitting, i.id as id_barang, i.code as kd_barang, i.name as nama_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join('items i','i.id=a.item_id','LEFT');
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $this->db->where('a.id',$id);
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id_kitting'])){
            $condition['a.id'] = $condition['id_kitting'];
            unset($condition['id_kitting']);
        }

        $this->data($condition);
        return $this->db->get();
    }

    public function export($condition=[]){
        $this->db
            ->select('itm1.code as sn_kitting, itm1.name as nama_kitting')
            ->select(["CONCAT(itm2.code, ' - ', itm2.name) as bom"])
            ->from($this->table.' r')
            ->join($this->table2.' ri','ri.repack_id=r.id')
            ->join('items itm1','itm1.id=r.item_id','left')
            ->join('items itm2','itm2.id=ri.item_id','left');

        return $this->db->get();
    }


    public function create($data) {

        $data = array(
            'item_id' => $data['id_barang']
        );

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function insert_kitting_item($data=array()){

        $data=array(
            'repack_id' =>$data['id_kitting'],
            'item_id'   =>$data['id_barang'],
            'qty'       =>$data['qty']
        );
 
        $result = $this->db->insert($this->table2, $data);
        return $result;
    }

    public function update_kitting_item($data=array(),$id=0){

        $data=array(
            'repack_id' =>$data['id_kitting'],
            'item_id'   =>$data['id_barang'],
            'qty'       =>$data['qty']
        );
 
        $result = $this->db->update($this->table2, $data,array('id'=>$id));
        return $result;
    }

    public function delete_kitting_item($id_kitting_product=0){
        $result = $this->db->delete($this->table2, array('id' => $id_kitting_product));
        return $result;
    }

    public function update($data, $id) {

        $data = array(
            'item_id' => $data['id_barang']
        );

        return $this->db->update($this->table, $data, array('id' => $id));

    }

    public function get_data_kitting_product($id=0){
        $this->db->select('id as id_kitting_product, item_id as id_barang, qty');
        $result = $this->db->get_where($this->table2,array('repack_id'=>$id));
        return $result;
    }

    public function delete($id) {
		$result = array();

        $this->db
            ->select('count(*) as t1')
            ->from('repacks_repack_orders')
            ->where('repack_id', $id);

		$row = $this->db->get()->row_array();
		if(($row['t1']) > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('id' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}


        return $result;
    }

    public function options($default = '--Pilih Kode Kitting--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_kitting] = $row->kd_barang ;
        }
        return $options;
    }

	function create_options_code($table_name, $value_column, $caption_column) {
		if($table_name == 'm_shipment_type')
			$this->db->order_by('id_shipment_type', "ASC");
		else
			$this->db->order_by('id_periode', "ASC");

        $data = $this->db->get($table_name)->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column) {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    function create_options2($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }

    function create_options3($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->select('*, kittings.id as kitting_id');
        $this->db->order_by($value_column, "ASC");
        $this->db->join('items','items.id=kittings.item_id','LEFT');
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }

}

?>

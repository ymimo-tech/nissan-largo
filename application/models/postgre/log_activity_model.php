<?php

class log_activity_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;

    private $table = 'access_log';

    private function data($condition = array()) {
        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.nik = b.nik', 'left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

}

?>
<?php
class report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

	public function getMaterialOut($post = array()){
		$result = array();
		
		$where = '';
		$where2 = '';
		$where3 = '';
		$having = '';
		$groupBy = '';
		$groupBy2 = '';
		$groupBy3 = '';
		$orderBy = '';

		$limit="";
		if(!empty($post['length']) && !empty($post['start'])){
			$limit = "LIMIT ".$post['length']." OFFSET ".$post['start'];
		}

		if(!empty($post['destination_id'])){
			$where .= ' AND outb.destination_name = \''.$post['destination_id'].'\' ';
			$having .= ' AND destination =\''.$post['destination_id'].'\' ';
		}
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= ' AND DATE(pl.date) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';

			$where2 .= ' AND DATE(ro.date) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';
			
			$where3 .= ' AND DATE(cc.time) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';
		}
						
		if(!empty($post['group_by'])){
			if($post['group_by'] == 'DATE'){
				$groupBy = ' DATE(pl.date) ';
				$groupBy2 = ' DATE(ro.date) ';
				$groupBy3 = ' DATE(cc.time) ';
				$orderBy = 'date_sort';
			}else{
				$groupBy = ' outb.destination_name ';
				$groupBy2 = ' destination ';
				$groupBy3 = ' destination ';
				$orderBy = 'destination';
			}
		}

		if(!empty($post['sn'])){
			$where .= " AND ird.unique_code = '".$post['sn']."'";
			$where2 .= " AND okp.kd_unik = '".$post['sn']."'";
			$where3 .= " AND ccr.unique_code = '".$post['sn']."'";
		}

		if(!empty($post['item'])){
			if($post['item'] !== 'null'){
				if(is_array($post['item'])){
					$item = (count($post['item']) > 1) ? implode(',',$post['item']) : $post['item'][0];
				}else{
					$item = $post['item'];
				}

				$where .= " AND itm.id IN ($item)";
				$where2 .= " AND itm.id IN ($item)";
				$where3 .= " itm.id IN ($item)";
			}

		}

		if(!empty($post['warehouse'])){

			if(is_array($post['warehouse'])){
				$wh = implode(",", $post['warehouse']);
			}else{
				$wh = $post['warehouse'];
			}

			$where .= " AND wh.id IN ($wh)";
			$where2 .= " AND wh.id IN ($wh)";
			$where3 .= " AND wh.id IN ($wh)";
		}

		$userId= $this->session->userdata('user_id');
		
		$sql1 = "
			SELECT
				outb.id as id_outbound,
				TO_CHAR(pl.date, 'DD Mon YYYY') as tgl_load,
				outb.code as kd_outbound,
				outb.destination_name as destination,
				case when pl.printed = 1 then 'PRINTED' when pl.printed = 0 then '' end as printed,
				u2.nama as user_picking, pr.qty as picking_qty,
				outbitm.qty as doc_qty,
				COALESCE(outb.note, '-') as remark,
				(select string_agg(distinct(ho.packing_number),', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id)as packing_number,
				coalesce((select sum(qty) from outbound_item_receiving_details oird 
				join header_oird ho on ho.id = oird.packing_number 
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id
				group by oird.pr_id, oird.picking_id),0) as packing_qty,
				(select string_agg(ho.volume::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as packing_volume,
				(select string_agg(ho.weight ::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as packing_weight,
				(select string_agg(u2.nama,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				left join users u2 on u2.id = ho.user_id 
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as user_packing,
				(select string_agg(ho.time::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as packing_start,
				coalesce((select sum(qty) from outbound_item_receiving_details oird 
				join header_oird ho on ho.id = oird.packing_number 
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id and ho.shipping_id is not null
				group by oird.pr_id, oird.picking_id),0) as load_qty,
				(select string_agg(m2.manifest_number::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				left join manifests m2 on m2.id = ho.shipping_id 
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as manifest_number,
				(select string_agg(s2.code::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				left join manifests m2 on m2.id = ho.shipping_id 
				left join shippings s2 on s2.manifest_code = m2.id
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as surat_jalan,
				(select string_agg(u2.nama::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				left join manifests m2 on m2.id = ho.shipping_id 
				left join users u2 on u2.id = m2.user_id 
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as user_loading,
				(select string_agg(m2.start::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				left join manifests m2 on m2.id = ho.shipping_id
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as start_loading,
				(select string_agg(m2.finish::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				left join manifests m2 on m2.id = ho.shipping_id
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as finish_loading,
				(select string_agg(dn.name::character varying,', ') from header_oird ho
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id
				left join manifests m2 on m2.id = ho.shipping_id
				left join delivery_notes dn on dn.id = oird.dn_id 
				where oird.pr_id = pr.id and oird.picking_id = pr.picking_id) as dn,
				pl.name as pl_name,
				pl.date as date_sort,
				pl.start_time as start_picking,
				pl.end_time as finish_picking,
				shipp.shipping_start as start_loading,
				shipp.shipping_finish as finish_loading,
				CONCAT(itm.code, ' - ',itm.name) as item,
				COALESCE(unt.name,' - ') as nama_satuan
			FROM outbound outb
			JOIN outbound_item outbitm ON outbitm.id_outbound=outb.id
			JOIN picking_list_outbound plo ON plo.id_outbound=outb.id
			JOIN pickings pl ON pl.pl_id=plo.pl_id
			JOIN picking_recomendation pr ON pr.picking_id=pl.pl_id and pr.item_id=outbitm.item_id
			LEFT JOIN shipping_picking sp ON sp.pl_id=pl.pl_id
			LEFT JOIN shippings shipp ON shipp.shipping_id=sp.pl_id
			JOIN warehouses wh ON wh.id=outb.warehouse_id
			JOIN users_warehouses uwh ON uwh.warehouse_id=wh.id
			LEFT JOIN documents doc ON doc.document_id=outb.document_id
			LEFT JOIN destinations d ON d.destination_id=outb.destination_id and doc.to_warehouse=NULL
			LEFT JOIN warehouses whs ON wh.id=outb.destination_id and doc.to_warehouse=1
			left join users u2 on u2.id = pr.user_id
			left join m_priority mp on mp.m_priority_id = outb.m_priority_id
			JOIN items itm ON itm.id=pr.item_id
			JOIN units unt ON unt.id=itm.unit_id
			WHERE 1=1 $where
			GROUP BY outb.id, pl.date, outbitm.qty, pl.name, pr.qty, pr.id, mp.m_priority_name, pl.printed, u2.nama, pr.picking_id, pl.start_time, pl.end_time, shipp.shipping_start, shipp.shipping_finish, itm.code, itm.name, unt.name
			order by pl.date ASC
			$limit";

		$sql2 = "
			SELECT
				ro.id as id_outbound,
				TO_CHAR(
					ro.date,
					'DD Mon YYYY'
				)AS tgl_load,
				ro.code as kd_outbound,
				CAST('-' as CHAR) as id_customer,
				'-' as destination,
				'-' as pl_name,
				CONCAT(
					itm.code,
					' - ',
					itm.name
				)AS item,
				rod.picking_doc_qty AS doc_qty,
				COALESCE(sum(rop.qty),0) AS load_qty,
				'-'AS remark,
				COALESCE(nama, '-')AS nama,
				COALESCE(unt.name, '')AS nama_satuan,
				COALESCE(NULL, '') AS packing_number,
				COALESCE(NULL, '') AS inc_packing_number,
				ro.date as date_sort,
				COALESCE(NULL,0) as system_qty,
				COALESCE(NULL,0) as scanned_qty
			FROM
				repack_orders ro
			JOIN warehouses wh ON wh.id=ro.warehouse_id
			JOIN users_warehouses usrwh ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
			JOIN repack_order_picking rop
				ON rop.repack_order_id=ro.id
			JOIN item_receiving_details ird
				ON ird.unique_code=rop.unique_code
			JOIN item_receiving ir
				ON ir.id=ird.item_receiving_id
			JOIN repack_order_details rod
				ON rod.repack_order_id = ro.id AND rod.item_id=ir.item_id
			LEFT JOIN items itm
				ON itm.id=ir.item_id
			LEFT JOIN users usr
				ON usr.id = ird.repack_order_user_id
			LEFT JOIN units unt
				ON unt.id=itm.unit_id
			WHERE
			1=1
				$where2
			GROUP BY 
				$groupBy2 , ro.id, ir.item_id, itm.code, itm.name, rod.picking_doc_qty, usr.nama, unt.name, ro.code
			HAVING
				1=1
				$having 
			".$limit;

		$sql3 = "
			SELECT
				cc.cc_id as id_outbound,
				TO_CHAR(
					cc.time,
					'DD Mon YYYY'
				)AS tgl_load,
				cc.code as kd_outbound,
				CAST('-' as CHAR) as id_customer,
				'-' as destination,
				'-' as pl_name,
				CONCAT(
					itm.code,
					' - ',
					itm.name
				)AS item,
				COALESCE(NULL,0) AS doc_qty,
				COALESCE((sum(system_qty) - sum(scanned_qty)),0) AS load_qty,
				remark AS remark,
				COALESCE(nama, '-')AS nama,
				COALESCE(unt.name, '')AS nama_satuan,
				COALESCE(NULL, '') AS packing_number,
				COALESCE(NULL, '') AS inc_packing_number,
				cc.time as date_sort,
				COALESCE(sum(system_qty),0) as system_qty,
				COALESCE(sum(scanned_qty),0) as scanned_qty
			FROM
				cycle_counts cc
			JOIN warehouses wh ON wh.id=cc.warehouse_id
			JOIN users_warehouses usrwh ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
			JOIN cycle_count_results ccr
				ON ccr.cycle_count_id = cc.cc_id
			JOIN item_receiving_details ird
				ON ird.unique_code=ccr.unique_code
			LEFT JOIN item_receiving ir
				ON ir.id=ird.item_receiving_id
			LEFT JOIN items itm
				ON itm.id=ir.item_id
			LEFT JOIN users usr
				ON usr.id = cc.user_id
			LEFT JOIN units unt
				ON unt.id=itm.unit_id
			WHERE
				1=1
			AND
				ccr.status = 'ADJUST TO SCANNED QTY'
			$where3
			GROUP BY 
				$groupBy3 , cc.cc_id, ccr.item_id, itm.code, itm.name, usr.nama, unt.name, ccr.system_qty, ccr.scanned_qty, cc.time, cc.code, cc.remark
			HAVING
				1=1
			AND
				(system_qty > scanned_qty)
			$having 
			".$limit;


		$sql = "SELECT * FROM ( ($sql1)UNION($sql2)UNION($sql3) ) as tbl ORDER BY $orderBy DESC";

		$row = $this->db->query($sql1)->result_array();
		//dd($sql1);
		$len = count($row);
		$dt = '';
		$j = -1;
		$data = array();
				
		if($post['group_by'] == 'DATE'){
		
			for($i = 0; $i < $len; $i++){
				
				if($dt != $row[$i]['tgl_load']){
					$dt = $row[$i]['tgl_load'];
					$j++;
				}
				
				if($dt == $row[$i]['tgl_load']){
					

					// $name = $row[$i]['nama'];
					//$pcCode = (!empty($row[$i]['packing_number'])) ? $this->get_pc_code($row[$i]['packing_number'], $row[$i]['inc_packing_number']) : '-';
					$data[$j][$dt][] = array(
						'tgl_outbound'		=> $row[$i]['tgl_load'],
						'kd_outbound'		=> $row[$i]['kd_outbound'],
						'destination'		=> $row[$i]['destination'],
						'pl_name'			=> $row[$i]['pl_name'],
						'printed'			=> $row[$i]['printed'],
						'picking_qty'		=> $row[$i]['picking_qty'],
						'start_picking'		=> $row[$i]['start_picking'],
						'finish_picking'	=> $row[$i]['finish_picking'],
						'user_picking'		=> $row[$i]['user_picking'],
						'packing_number'	=> $row[$i]['packing_number'],
						'packing_qty'		=> $row[$i]['packing_qty'],
						'packing_volume'	=> $row[$i]['packing_volume'],
						'packing_weight'	=> $row[$i]['packing_weight'],
						'user_packing'		=> $row[$i]['user_packing'],
						'packing_start'		=> $row[$i]['packing_start'],
						'dn'				=> $row[$i]['dn'],
						'manifest_number'	=> $row[$i]['manifest_number'],
						'surat_jalan'		=> $row[$i]['surat_jalan'],
						'item'				=> $row[$i]['item'],
						'doc_qty'			=> $row[$i]['doc_qty'],
						'load_qty'			=> $row[$i]['load_qty'],
						'start_picking'		=> $row[$i]['start_picking'],
						'start_loading'		=> $row[$i]['start_loading'],
						'finish_loading'	=> $row[$i]['finish_loading'],
						'user_loading'		=> $row[$i]['user_loading'],
						'remark'			=> $row[$i]['remark'],
					);
				}
				
			}
			
		}else{
			
			for($i = 0; $i < $len; $i++){
				
				if($dt != $row[$i]['destination']){
					$dt = $row[$i]['destination'];
					$j++;
				}
				
				if($dt == $row[$i]['destination']){
					
					$name = $row[$i]['nama'];

					$pcCode = (!empty($row[$i]['packing_number'])) ? $this->get_pc_code($row[$i]['packing_number'], $row[$i]['inc_packing_number']) : '-';

					$data[$j][$row[$i]['destination']][] = array(
						'tgl_outbound'	=> $row[$i]['tgl_load'],
						'kd_outbound'	=> $row[$i]['kd_outbound'],
						'destination'	=> $row[$i]['destination'],
						'pl_name'		=> $row[$i]['pl_name'],
						'packing_number'=> $pcCode,
						'item'			=> $row[$i]['item'],
						'doc_qty'		=> $row[$i]['doc_qty'],
						'load_qty'		=> $row[$i]['load_qty'],
						'remark'		=> $row[$i]['remark'],
						// 'nama'			=> $name,
						'nama_satuan'	=> $row[$i]['nama_satuan'],
						'start_picking'		=> $row[$i]['start_picking'],
						'start_loading'		=> $row[$i]['start_loading'],
						'finish_picking'		=> $row[$i]['finish_picking'],
						'finish_loading'		=> $row[$i]['finish_loading']
					);
				}
				
			}
			
		}
			
		$result['status'] = 'OK';
		$result['data'] = $data;
						
		return $result;
	}

    public function get_pc_code($pcCode, $inc){
    	$pc_code = '';
    	$pc_string_tmp = '';
    	$pad = ($inc < 10) ? 0 : '';
    	for($i=1;$i<=$inc;$i++){
	    	$pc_string_tmp = ($i == $inc || ($i == 1 && $inc <= 1 ) )? '' : ',';
            $pc_code .= $pcCode.$pad.$i.$pad.$inc. $pc_string_tmp .'</br>';
    	}
    	return $pc_code;
    }
	
	public function getMaterialIn($post = array()){
		$result = array();
		
		$where = '';
		$wheres = '';

		$where2 = '';
		$where3 = '';
		$groupBy = '';
		$groupBy3 = '';
		$orderBy = '';
		$having = '';
		
		if(!empty($post['source_id'])){
			$wheres .= " AND inb.supplier_id = '".$post['source_id']."' ";
			$having .= " AND id_supplier = '".$post['source_id']."' ";
		}

		if(!empty($post['sn'])){
			$wheres .= " AND ird.unique_code = '".$post['sn']."' ";
			$where2 .= " AND ird.unique_code = '".$post['sn']."' ";
			$where3 .= " AND ccr.unique_code = '".$post['sn']."' ";
		}

		if(!empty($post['item'])){
			if($post['item'] !== 'null'){
				$item = (count($post['item']) > 1) ? implode(',',$post['item']) : $post['item'][0];
				$wheres .= " AND itm.id IN ($item)";
				$where2 .= " AND itm.id IN ($item)";
				$where3 .= " AND itm.id IN ($item)";
			}

		}
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= ' AND DATE(rcv.date) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';

			$where2 .= ' AND DATE(ro.date) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';

			$where3 .= ' AND DATE(cc.time) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';

		}
						
		if(!empty($post['group_by'])){
			if($post['group_by'] == 'DATE'){
				$groupBy = ' ro.id';
				$groupBy3 = 'cc.cc_id';
				$orderBy = 'date_sort';
			}else{
				$groupBy = 'id_supplier';
				$groupBy3 = 'id_supplier';
				$orderBy = 'id_supplier';
			}
		}

		if(!empty($post['warehouse'])){

			if(is_array($post['warehouse'])){
				$wh = implode(",", $post['warehouse']);
			}else{
				$wh = $post['warehouse'];
			}

			$wheres .= " AND wh.id IN ($wh)";
			$where2 .= " AND wh.id IN ($wh)";
			$where3 .= " AND wh.id IN ($wh)";
		}

		$userId= $this->session->userdata('user_id');

		if(!empty($post['group_by'])){
			if($post['group_by'] == 'DATE'){
				$groupBy = 'ro.id';
				$orderBy = 'date_sort';

				$sql1 = "
					SELECT
						TO_CHAR(rcv.date,'DD Mon YYYY')AS tgl_in,
						inb.code as kd_inbound,
						CONCAT(s.source_code, ' - ', s.source_name) as source,
						rcv.code as kd_receiving,
						CONCAT(itm.code,' - ',itm.name) as item,
						itm.code as item_code,
						itm.name as item_name,
						ird.unique_code as lpn,
						ir.qty as doc_qty,
						COALESCE(rcv.remark, '-') as remark,
						DATE(rcv.date) as date_sort,
						COALESCE(unt.name,'-') as nama_satuan,
						COALESCE(SUM(ird.first_qty),0) as rec_qty,
						COALESCE((SUM(ird.first_qty)-ir.qty),0) as disc_qty,
						COALESCE(usr.user_name, '-') as nama,
						rcv.start_tally,
						rcv.finish_tally,
						rcv.start_putaway,
						rcv.finish_putaway
					FROM receivings rcv
					JOIN item_receiving ir ON ir.receiving_id=rcv.id
					JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
					JOIN inbound inb ON inb.id_inbound=ir.inbound_id
					JOIN warehouses wh ON wh.id=inb.warehouse_id
					JOIN items itm ON itm.id=ir.item_id
					JOIN units unt ON unt.id=itm.unit_id
					JOIN sources s ON s.source_id=inb.supplier_id
					JOIN users usr ON usr.id=rcv.user_id
					WHERE
						1 = 1
					".$where." and ir.qty > 0 
					".$wheres."
					GROUP BY rcv.date, inb.code, ird.unique_code, s.source_code, s.source_name, rcv.code, itm.code, itm.name, ir.qty, rcv.remark, unt.name, rcv.start_tally, rcv.finish_tally, rcv.start_putaway, rcv.finish_putaway, usr.user_name
					ORDER BY DATE(rcv.date) DESC
				";

			}else{

				$sql1 = "
				SELECT
					TO_CHAR(
						rcv.date,
						'DD Mon YYYY'
					)AS tgl_in,
					inb.code as kd_inbound,
					CAST(inb.supplier_id as CHAR) as id_supplier,
					CONCAT(
						spl.code,
						' - ',
						spl.name
					)AS source,
					rcv.code as kd_receiving,
					CONCAT(
						itm.code,
						' - ',
						itm.name
					)AS item,
					itm.code as item_code,
					itm.name as item_name,
					ir.qty AS doc_qty,
					COALESCE(tr.qty,0) AS rec_qty,
					(ir.qty - COALESCE(tr.qty,0))AS disc,
					COALESCE(rcv.remark, '')AS remark,
					COALESCE(nama, '-')AS nama,
					COALESCE(unt.name, '')AS nama_satuan,
					DATE(rcv.date) AS date_sort,
					COALESCE(NULL,0) as system_qty,
					COALESCE(NULL,0) as scanned_qty,
					rcv.start_tally,
					rcv.finish_tally,
					rcv.start_putaway,
					rcv.finish_putaway
				FROM
					receivings rcv
				JOIN inbound inb ON inb.id_inbound = rcv.po_id
				JOIN warehouses wh ON wh.id=inb.warehouse_id
				JOIN users_warehouses usrwh ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
				JOIN inbound_item inbitm ON inbitm.inbound_id = inb.id_inbound
				JOIN items itm ON itm.id = inbitm.item_id
				JOIN item_receiving ir ON ir.receiving_id = rcv.id
				AND ir.item_id = itm.id
				LEFT JOIN item_receiving_details ird ON ird.item_receiving_id=ir.id
				LEFT OUTER JOIN suppliers spl ON spl.id = inb.supplier_id
				AND inb.document_id NOT IN(2,7)
				LEFT JOIN units unt ON unt.id=itm.unit_id
				LEFT JOIN(
					SELECT
						inb.supplier_id,
						sum(first_qty)AS qty,
						ir.item_id,
						max(user_id_receiving) as user_id
					FROM
						item_receiving_details ird
					LEFT JOIN item_receiving ir ON ir.id=ird.item_receiving_id
					LEFT JOIN receivings rcv ON rcv.id = ir.receiving_id
					LEFT JOIN inbound inb ON inb.id_inbound=rcv.po_id
					WHERE 1=1
						".$where."
					GROUP BY
						ir.item_id,
						inb.supplier_id

				)tr ON tr.supplier_id = inb.supplier_id and tr.item_id = itm.id
				LEFT JOIN users usr ON usr.id = tr.user_id
				WHERE
					1 = 1
				".$where." and ir.qty > 0 
				".$wheres."
				GROUP BY
					itm.code, rcv.date, inb.code, inb.supplier_id, spl.code, spl.name, rcv.code, itm.name, ir.qty, tr.qty, rcv.remark, usr.nama, unt.name, rcv.start_tally, rcv.finish_tally,rcv.start_putaway,rcv.finish_putaway
				ORDER BY
					inb.supplier_id DESC";
			}
		}

		$sql2 = "
					SELECT
						TO_CHAR(
							ro.date,
							'DD Mon YYYY'
						)AS tgl_in,
						ro.code as kd_inbound,
						CAST('-' as CHAR) as id_supplier,
						'-' AS source,
						ro.code as kd_receiving,
						CONCAT(
							itm.code,
							' - ',
							itm.name
						)AS item,
						itm.code as item_code,
						itm.name as item_name,
						rod.tally_doc_qty AS doc_qty,
						sum(ird.first_qty) AS rec_qty,
						'0' AS disc,
						COALESCE(NULL, '')AS remark,
						COALESCE(nama, '-')AS nama,
						COALESCE(unt.name, '')AS nama_satuan,
						DATE(ro.date) AS date_sort,
						COALESCE(NULL,0) as system_qty,
						COALESCE(NULL,0) as scanned_qty
					FROM
						repack_orders ro
					JOIN warehouses wh ON wh.id=ro.warehouse_id
					JOIN users_warehouses usrwh ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
					JOIN item_receiving_details ird
						ON ird.repack_order_id=ro.id
					JOIN repack_order_details rod
						ON rod.repack_order_id=ro.id AND rod.item_id=ird.item_id
					LEFT JOIN items itm
						ON itm.id=ird.item_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					LEFT JOIN users usr
						ON usr.id=ro.user_id
					WHERE
						ird.location_id NOT IN (100)
					$where2
					GROUP BY 
						$groupBy , ird.item_id, itm.code, itm.name, rod.tally_doc_qty, usr.nama, unt.name, ro.date, ro.code
					HAVING
						1=1
					$having
					";

		$sql3 = "
					SELECT
						TO_CHAR(
							cc.time,
							'DD Mon YYYY'
						)AS tgl_in,
						cc.code as kd_inbound,
						CAST('-' as CHAR) as id_supplier,
						'-' AS source,
						cc.code as kd_receiving,
						CONCAT(
							itm.code,
							' - ',
							itm.name
						)AS item,
						itm.code as item_code,
						itm.name as item_name,
						COALESCE(NULL,0) AS doc_qty,
						COALESCE((sum(scanned_qty) - sum(system_qty)),0) AS rec_qty,
						COALESCE((sum(scanned_qty) - sum(system_qty)),0) AS disc,
						COALESCE(NULL, '')AS remark,
						COALESCE(nama, '-')AS nama,
						COALESCE(unt.name, '')AS nama_satuan,
						DATE(cc.time) AS date_sort,
						COALESCE(sum(system_qty),0) as system_qty,
						COALESCE(sum(scanned_qty),0) as scanned_qty
					FROM
						cycle_counts cc
					JOIN warehouses wh ON wh.id=cc.warehouse_id
					JOIN users_warehouses usrwh ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
					JOIN cycle_count_results ccr
						ON ccr.cycle_count_id=cc.cc_id
					JOIN item_receiving_details ird
						ON ird.unique_code=ccr.unique_code
					LEFT JOIN item_receiving ir
						ON ir.id=ird.item_receiving_id
					LEFT JOIN items itm
						ON itm.id=ir.item_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					LEFT JOIN users usr
						ON usr.id=cc.user_id
					WHERE
						ird.location_id NOT IN (100)
					AND
						ccr.status = 'ADJUST TO SCANNED QTY'
					$where3
					GROUP BY 
						$groupBy3 , ir.item_id, itm.code, itm.name, usr.nama, unt.name, ccr.system_qty, ccr.scanned_qty, cc.time, cc.code
					HAVING
						1=1
					AND
						(system_qty < scanned_qty)
					$having
					";

		$sql = "SELECT * FROM ( ($sql1)UNION($sql2)UNION($sql3) ) as tbl ORDER BY $orderBy DESC";

		$row = $this->db->query($sql1)->result_array();
		$len = count($row);
		$dt = '';
		$j = -1;
		$data = array();
		
		//var_dump($row);
		
		if($post['group_by'] == 'DATE'){
		
			for($i = 0; $i < $len; $i++){
				
				if($dt != $row[$i]['tgl_in']){
					$dt = $row[$i]['tgl_in'];
					$j++;
				}
				
				if($dt == $row[$i]['tgl_in']){
					
					$name = $row[$i]['nama'];

					$data[$j][$dt][] = array(
						'tgl_inbound'	=> $row[$i]['tgl_in'],
						'kd_inbound'	=> $row[$i]['kd_inbound'],
						'source'		=> $row[$i]['source'],
						'kd_receiving'	=> $row[$i]['kd_receiving'],
						'item_code'		=> $row[$i]['item_code'],
						'item_name'		=> $row[$i]['item_name'],
						'item'			=> $row[$i]['item'],
						'lpn'			=> $row[$i]['lpn'],
						'doc_qty'		=> $row[$i]['doc_qty'],
						'rec_qty'		=> $row[$i]['rec_qty'],
						'disc'			=> ($row[$i]['doc_qty'] - $row[$i]['rec_qty']),
						'remark'		=> $row[$i]['remark'],
						'nama'			=> $name,
						'nama_satuan'	=> $row[$i]['nama_satuan'],
						'start_tally'	=> $row[$i]['start_tally'],
						'finish_tally'	=> $row[$i]['finish_tally'],
						'start_putaway'	=> $row[$i]['start_putaway'],
						'finish_putaway'	=> $row[$i]['finish_putaway']
					);
				}
				
			}
			
		}else{
			
			for($i = 0; $i < $len; $i++){
				
				if($dt != $row[$i]['id_supplier']){
					$dt = $row[$i]['id_supplier'];
					$j++;
				}
				
				if($dt == $row[$i]['id_supplier']){
					
					$name = $row[$i]['nama'];

					$data[$j][$row[$i]['source']][] = array(
						'tgl_inbound'	=> $row[$i]['tgl_in'],
						'kd_inbound'	=> $row[$i]['kd_inbound'],
						'source'		=> $row[$i]['source'],
						'kd_receiving'	=> $row[$i]['kd_receiving'],
						'item_code'		=> $row[$i]['item_code'],
						'item_name'		=> $row[$i]['item_name'],
						'item'			=> $row[$i]['item'],
						'doc_qty'		=> $row[$i]['doc_qty'],
						'rec_qty'		=> $row[$i]['rec_qty'],
						'disc'			=> ($row[$i]['doc_qty'] - $row[$i]['rec_qty']),
						'remark'		=> $row[$i]['remark'],
						'nama'			=> $name,
						'nama_satuan'	=> $row[$i]['nama_satuan'],
					);
				}
				
			}
			
		}
			
		$result['status'] = 'OK';
		$result['data'] = $data;
						
		return $result;
	}
	
	public function getSource($type=""){
		$result = array();

		if($type=="in"){

			// $sql = "(
			// 			SELECT 
			// 				id, code, name, 'supplier' AS type
			// 			FROM 
			// 				suppliers spl
			// 			JOIN suppliers_warehouses splwh
			// 				ON splwh.supplier_id=spl.id
			// 			JOIN warehouses wh
			// 				ON wh.id=splwh.warehouse_id
			// 			JOIN warehouses_users wu
			// 				ON wu.warehouse_id=wh.id AND wu.user_id=".$this->session->userdata('user_id')."
			// 			ORDER BY 
			// 				code ASC
			// 		) 
			// 		UNION
			// 		(
			// 			SELECT 
			// 				id, code, name,'customer' AS type						
			// 			FROM 
			// 				customers cust
			// 			JOIN customers_warehouses custwh
			// 				ON custwh.customer_id=cust.id
			// 			JOIN warehouses wh
			// 				ON wh.id=custwh.warehouse_id
			// 			JOIN warehouses_users wu
			// 				ON wu.warehouse_id=wh.id AND wu.user_id=".$this->session->userdata('user_id')."
			// 			ORDER BY 
			// 				id ASC
			// 		)";
	
			$sql = "(
						SELECT 
							id, code, name, 'supplier' AS type
						FROM 
							suppliers
						ORDER BY 
							code ASC
					) 
					UNION
					(
						SELECT 
							id, code, name,'customer' AS type						
						FROM 
							customers
						ORDER BY 
							id ASC
					)";

			$row = $this->db->query($sql)->result_array();
			$len = count($row);
			for($i = 0; $i < $len; $i++){
				if($row[$i]['type'] == 'supplier')
					$result['supplier'][] = $row[$i];
				else
					$result['customer'][] = $row[$i];
			}
				
		}else{

			$sql = "
				SELECT destination_name as destination FROM outbound GROUP BY destination_name ORDER BY destination_name ASC;
			";

			$result = $this->db->query($sql)->result_array();

		}
		
		return $result;
	}

	public function getMaterialOutDetail($post = array()){

		set_time_limit(1);

		$having = '';
		$groupBy = '';
		$groupBy2 = '';
		$groupBy3 = '';
		$orderBy = '';
								
		if(!empty($post['group_by'])){
			if($post['group_by'] == 'DATE'){
				$groupBy = ' DATE(pl.date) ';
				$groupBy2 = ' DATE(ro.date) ';
				$groupBy3 = ' DATE(cc.time) ';
				$orderBy = 'date_sort';
			}else{
				$groupBy = ' outb.destination_name ';
				$groupBy2 = ' destination ';
				$groupBy3 = ' destination ';
				$orderBy = 'destination';
			}
		}

		$userId= $this->session->userdata('user_id');

		$this->db->start_cache();
		if(!empty($post['item'])){
			if($post['item'] !== 'null'){
				if(is_array($post['item'])){
					$item = (count($post['item']) > 1) ? implode(',',$post['item']) : $post['item'][0];
				}else{
					$item = $post['item'];
				}

				$this->db->where("itm.id IN ($item)",NULL);
			}

		}		

		if(!empty($post['warehouse'])){

			if(is_array($post['warehouse'])){
				$wh = implode(",", $post['warehouse']);
			}else{
				$wh = $post['warehouse'];
			}

			$this->db->where("wh.id IN ($wh)",NULL);
		}

		$this->db->stop_cache();


		$result = array();

		/*
			Start - Get Data from Outbound Document
		*/

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where("DATE(p.date) BETWEEN '".date('Y-m-d', strtotime(str_replace('/','-',$post['from'])))."' AND '".date('Y-m-d', strtotime(str_replace('/','-',$post['to'])))."'", NULL );
		}

		if(!empty($post['sn'])){
			$this->db->where('pr.unique_code',$post['sn']);
		}


		$this->db
			->select([
				"COALESCE(NULL,'') as no",
				"outb.code as document_code",
				"p.name as document_picking_code",
				"pr.unique_code as serial_number",
				"COALESCE(ird.parent_code,'-') as outer_label",
				"CONCAT(itm.code,' - ',itm.name) as item_name",
				"pr.qty as picked_qty",
				"COALESCE(usr.user_name, '-') as picked_user"
			])
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses whu',"whu.warehouse_id=wh.id and whu.user_id=$userId")
			->join('picking_list_outbound plo','plo.id_outbound=outb.id')
			->join('pickings p','p.pl_id=plo.pl_id')
			->join('picking_recomendation pr','pr.picking_id=p.pl_id')
			->join('item_receiving_details ird','ird.picking_id=pr.picking_id and ird.unique_code=pr.unique_code and ird.item_id=pr.item_id','left')
			->join('users usr','usr.id=pr.user_id','left')
			->join('items itm','itm.id=pr.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left');

		$sql1 = $this->db->get_compiled_select();		

		// End - Get Data from Outbound Document


		/*
			Start - Get Data from Repack Document
		*/

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where("DATE(ro.date) BETWEEN '".date('Y-m-d', strtotime(str_replace('/','-',$post['from'])))."' AND '".date('Y-m-d', strtotime(str_replace('/','-',$post['to'])))."'", NULL );
		}

		if(!empty($post['sn'])){
			$this->db->where('rop.unique_code',$post['sn']);
		}

		$this->db
			->select([
				"ro.code as document_code",
				"ro.code as document_picking_code",
				"rop.unique_code as serial_number",
				"COALESCE(ird.parent_code,'-') as outer_label",
				"CONCAT(itm.code,' - ',itm.name) as item_name",
				"COALESCE(rop.qty, 0) as picked_qty",
				"COALESCE(usr.user_name, '-') as picked_user"
			])
			->from('repack_orders ro')
			->join('warehouses wh','wh.id=ro.warehouse_id')
			->join('users_warehouses whu',"whu.warehouse_id=wh.id and whu.user_id=$userId")
			->join('repack_order_picking rop','rop.repack_order_id=ro.id')
			->join('item_receiving_details ird','ird.repack_order_id=rop.repack_order_id and ird.unique_code=rop.unique_code and ird.item_id=rop.item_id','left')
			->join('users usr','usr.id=ro.user_id','left')
			->join('items itm','itm.id=rop.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left');

		$sql2 = $this->db->get_compiled_select();

		// End - Get Data from Repack Document

		/*
			Start - Get Data from Repack Document
		*/

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where("DATE(cc.time) BETWEEN '".date('Y-m-d', strtotime(str_replace('/','-',$post['from'])))."' AND '".date('Y-m-d', strtotime(str_replace('/','-',$post['to'])))."'", NULL );
		}
		if(!empty($post['sn'])){
			$this->db->where('ccr.unique_code',$post['sn']);
		}

		$this->db
			->select([
				"cc.code as document_code",
				"cc.code as document_picking_code",
				"ccr.unique_code as serial_number",
				"COALESCE(ird.parent_code,'-') as outer_label",
				"CONCAT(itm.code,' - ',itm.name) as item_name",
				"ccr.qty as picked_qty",
				"COALESCE(usr.user_name, '-') as picked_user"
			])
			->from('cycle_counts cc')
			->join('warehouses wh','wh.id=cc.warehouse_id')
			->join('users_warehouses whu',"whu.warehouse_id=wh.id and whu.user_id=$userId")
			->join('cycle_count_results ccr','ccr.cycle_count_id = cc.cc_id')
			->join('item_receiving_details ird','ird.cycle_count_id=ccr.cycle_count_id and ird.unique_code=ccr.unique_code and ird.item_id=ccr.item_id','left')
			->join('users usr','usr.id=ccr.user_id','left')
			->join('items itm','itm.id=ccr.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left');

		$sql3 = $this->db->get_compiled_select();

		// End - Get Data from Cycle Document

		$sql = "SELECT * FROM ( ($sql1)UNION($sql2)UNION($sql3) ) as tbl";

		$row = $this->db->query($sql1)->result_array();

		return $row;
	}

	public function getMaterialInDetail($post = array()){

		$userId= $this->session->userdata('user_id');

		$this->db->start_cache();
		if(!empty($post['item'])){
			if($post['item'] !== 'null'){
				if(is_array($post['item'])){
					$item = (count($post['item']) > 1) ? implode(',',$post['item']) : $post['item'][0];
				}else{
					$item = $post['item'];
				}

				$this->db->where("itm.id IN ($item)",NULL);
			}

		}		

		if(!empty($post['warehouse'])){

			if(is_array($post['warehouse'])){
				$wh = implode(",", $post['warehouse']);
			}else{
				$wh = $post['warehouse'];
			}

			$this->db->where("wh.id IN ($wh)",NULL);
		}

		$this->db->stop_cache();
		

		$result = array();

		// Start - Get data from inbound document

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where("DATE(r.date) BETWEEN '".date('Y-m-d', strtotime(str_replace('/','-',$post['from'])))."' AND '".date('Y-m-d', strtotime(str_replace('/','-',$post['to'])))."'", NULL );
		}
		if(!empty($post['sn'])){
			$this->db->where('ird.unique_code',$post['sn']);
		}

		$this->db
			->select([
				"COALESCE(NULL,'') as no",
				"inb.code as document_code",
				"r.code as receiving_code",
				"ird.unique_code",
				"ird.rfid_tag",
				"ird.parent_code",
				"itm.code as item_code",
				"itm.name as item_name",
				"COALESCE(ird.last_qty,0) as qty",
				"unt.code as unit_code",
				"usr.user_name as user"
			])
			->from('inbound inb')
			->join('warehouses wh','wh.id=inb.warehouse_id')
			// ->join('users_warehouses whu',"whu.warehouse_id=wh.id and whu.user_id=$userId")
			->join('item_receiving ir','ir.inbound_id=inb.id_inbound')
			->join('receivings r','r.id=ir.receiving_id')
			->join('item_receiving_details ird','ird.item_receiving_id=ir.id')
			->join('users usr','usr.id=ird.user_id_receiving','left')
			->join('items itm','itm.id=ird.item_id','left')
			->join('units unt','unt.id=itm.unit_id','left');

		$sql = $this->db->get_compiled_select();

		// End - Get data from inbound document

		$data = $this->db->query($sql)->result_array();

		return $data;

	}

	public function getItemCode($post = array()){
		$result = array();

		$this->db
			->select('id, code')
			->from('items')
			->like('code', $post['query']);

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}


	
}
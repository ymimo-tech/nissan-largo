<?php
class referensi_lokasi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'm_loc';

    public function data($condition = array()) {

		$condition['loc_id NOT IN(103,104)'] = null;

        $this->db->from($this->table  . ' a');
        $this->db->join('kategori c','c.id_kategori = a.loc_category_id','left' );
        $this->db->join('m_loc_type t','t.loc_type_id = a.loc_type_id','left' );
        $this->db->join('m_loc_area r','r.loc_area_id = a.loc_area_id','left' );
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function getLocationById($post = array()){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_loc
				WHERE
					loc_id IN('.implode(',', $post['loc_id']).')';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getLocation(){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_loc
				WHERE
					loc_name IS NOT NULL';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getQtyLocation(){
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					m_loc';

		$row = $this->db->query($sql)->row_array();
		$result['qty'] = $row['total'];

		return $result;
	}

    public function get_by_id($id) {
        $condition['a.loc_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $loc_name = $this->input->post("loc_name");
        $loc_desc = $this->input->post("loc_desc");

        if(!empty($loc_name)){
            $condition["a.loc_name like '%$loc_name%'"]=null;
        }

        if(!empty($loc_desc)){
            $condition["a.loc_desc like '%$loc_desc%'"]=null;
        }

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.loc_id');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->loc_id;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
            $action .= '</li>';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'loc_name' => $value->loc_name,
                    'loc_desc' => $value->loc_desc,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'loc_name' => $value->loc_name,
                    'loc_desc' => $value->loc_desc,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('loc_id' => $id));
    }

    public function delete($id) {

		$result = array();

		$in = explode(',', $this->config->item('hardcode_location_id'));

		if(in_array($id, $in)){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete master location';
		}else{

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
						receiving_barang
					WHERE
						loc_id=\''.$id.'\'';

			$row = $this->db->query($sql)->row_array();

			if($row['total'] > 0){
				$result['status'] = 'ERR';
				$result['message'] = 'Cannot delete location, because data have relation to another table';
			}else{
				$this->db->delete($this->table, array('loc_id' => $id));

				$result['status'] = 'OK';
				$result['message'] = 'Delete data success';
			}
		}

		return $result;

    }

    public function options($default = '--Pilih Nama Lokasi--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->loc_id] = $row->loc_name ;
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if($table_name == 'm_inbound_document'){

				$r = $this->setting_model->getData();

				if(strtolower($dt['inbound_document_name']) == 'peminjaman' && $r['peminjaman'] == 'N')
					continue;
			}

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }

}

?>

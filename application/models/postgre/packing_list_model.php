<?php
class packing_list_model extends CI_Model {

    public function __construct() {
		parent::__construct();
        $this->load->library("randomidgenerator");
	}
	
	public function getDetailPacking($params){
		$packing_number = $params["packing_id"];
		// $pc_code = substr($packing_number, 0, 15);
		// $inc = substr($packing_number, 15, 17);
		$pc_code = $packing_number;

		$requestData = $_REQUEST;

		$data = [];
		$result_packing = [];

		$results = $this->db
					// ->select('oird.id_outbound_receiving_barang, oird.id_item_receiving_details, oird.id_outbound, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, SUM(pr.qty) as qty, unt.name as uom_name')
					->select('oird.id_outbound_receiving_barang, oird.id_item_receiving_details, ho.shipping_id, d.destination_name, oird.id_outbound AS outbound_id, o.code AS outbound_doc, p.pl_id AS picking_id, p.name AS picking_doc, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, pr.qty as picked_qty, oird.qty as packed_qty, unt.name as uom_name')
					->from('outbound_item_receiving_details oird')
					->join('outbound o', 'o.id=oird.id_outbound','left')
					->join('item_receiving_details ird', 'ird.id=oird.id_item_receiving_details','left')
					->join('destinations d', 'd.destination_id = o.destination_id','left')
					->join('picking_recomendation pr', 'pr.id=oird.pr_id','left')
					->join('pickings p', 'pr.picking_id=p.pl_id AND p.pl_id = oird.picking_id','left')
					->join('items itm', 'itm.id=ird.item_id', 'LEFT')
					->join('units unt','unt.id=itm.unit_id','left')
					->join('header_oird ho','ho.id = oird.packing_number','left')
					->where('ho.packing_number', $pc_code)
					// ->where('oird.inc', $inc)
					//->group_by('oird.id_outbound_receiving_barang, d.destination_name, oird.id_item_receiving_details, oird.id_outbound, o.code, p.pl_id, p.name, oird.packing_number, oird.inc, itm.code, itm.name, ird.unique_code, pr.qty, unt.name')
					->get();
					$result = $results->result_array();
					// dd($result);
		// var_dump($this->db->last_query());exit;

		for($i=0;$i<$results->num_rows();$i++){
			$unique_code = '';
			$r_unique_code = $result[$i]['unique_code'];
			if(strpos($r_unique_code, 'BYSYS') > 0) {
				if(strpos($r_unique_code, 'XX')) {
					$xsn = explode('XX', $r_unique_code);
					$ysn = explode('YY', $xsn[1]);
					$unique_code = $ysn[0];
				} else {
					$unique_code = substr($r_unique_code,9,strlen($r_unique_code));
				}
			} else {
				$unique_code = $r_unique_code;
			}

			if(empty($result[$i]['shipping_id'])){
				$delete = '<a class="delete-packing btn red btn-xs" onclick="javascript:void(0)" data-id="'.$result[$i]['id_outbound_receiving_barang'].'">DELETE</a>';
			}else{
				$delete = '<a class="btn red btn-xs" onclick="javascript:void(0)" disabled>DELETE</a>';
			}
			
			$nested = array();
			$nested[] = $i + 1 ;
			$nested[] = $result[$i]['code'];
			$nested[] = $result[$i]['name'];
			$nested[] = $unique_code;
			$nested[] = '<a href="'.base_url().'outbound/detail/'.$result[$i]['outbound_id'].'" target="_blank">'.$result[$i]['outbound_doc'].'</a>';
			$nested[] = $result[$i]['destination_name'];
			$nested[] = '<a href="'.base_url().'picking/detail/'.$result[$i]['picking_id'].'" target="_blank">'.$result[$i]['picking_doc'].'</a>';
			$nested[] = $result[$i]['picked_qty'].' '.$result[$i]['uom_name'];
			$nested[] = $result[$i]['packed_qty'].' '.$result[$i]['uom_name'];
			$nested[] = $delete;
			$nested[] = '';
			$nested[] = '';
			$result_packing[] = $nested;
		}
		
		$results_new = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( count($result_packing) ),
			"recordsFiltered" => intval( count($result_packing) ),
			"data"            => $result_packing
		);

		return $results_new;

	}

	public function getDimension($params){
		$sql = $this->db->select(['weight','volume'])->from('header_oird')->where('packing_number',$params['id'])->get()->row_array();
		return $sql;
	}

	public function editDimension($params){
		$this->db->trans_start();
		$this->db->where('packing_number',$params['id']);
		$this->db->set('weight',$params['weight']);
		$this->db->set('volume',$params['volume']);
		$this->db->update('header_oird');
		$this->db->trans_complete();
		return true;
	}

	public function delete_packing($params){
		$this->db->trans_start();
		$this->db->where('id_outbound_receiving_barang',$params['oird_id']);
		$this->db->delete('outbound_item_receiving_details');
		$this->db->trans_complete();
		return true;
	}

	public function export_pack($params=array()){
		$where = '';

		if($params['id_shipping'] !== 'null'){
			$where .= ' AND ho.shipping_id ='.$params['id_shipping'];
		}

		if($params['outbound'] !== 'null'){
			$where .= ' AND o.id ='.$params['outbound'];
		}

		if($params['pccode'] !== 'null'){
			$where .= ' AND ho.packing_number =\''.$params['pccode'].'\'';
		}

		if($params['to'] !== '' && $params['from'] !== ''){
			$where .= ' AND (ho.time between \''.$params['from'].' 00:00:00\' and \''.$params['to'].' 23:59:00\')';	
		}

		$sql = "select ho.packing_number, itm.sku as item_sub, itm2.sku as item_ori, pr.unique_code as lpn, date_trunc('second',ho.time) as packing_date,
		dn.name as dn_name, ho.weight, ho.volume, date_trunc('second',oird.time) as pack_time, oird.qty pack_qty, u.nama as user_pack, mp.m_priority_name, d.destination_name,
		o.code as sale_order from outbound_item_receiving_details oird 
		left join header_oird ho on ho.id = oird.packing_number 
		left join picking_recomendation pr on pr.id = oird.pr_id 
		left join delivery_notes dn on dn.id = oird.dn_id 
		left join items itm on itm.id = pr.item_id
		left join items itm2 on itm2.id = pr.order_item_id 
		left join outbound o on o.id = pr.outbound_id 
		left join users u on u.id = oird.user_id 
		left join destinations d on d.destination_id = o.destination_id
		left join m_priority mp on mp.m_priority_id = o.m_priority_id 
		where 1=1".$where;
		// dd($sql);
		// dd($sql);
		$result = $this->db->query($sql)->result_array();

		return $result;
	}


	public function getPackingList($post) {
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'packing_number',
			1 => 'packing_number',
			2 => 'packing_number',
			3 => 'inc',
			4 => 'user_packing',
			5 => 'time',
			6 => 'outbound_doc',
			7 => 'loading_doc',
			8 => 'weight',
			9 => 'volume'
		);

		$where = '';
		$where_o = '';

		if(!empty($post['id_shipping'])){
			$where .= " AND m.manifest_number = '" . $post['id_shipping'] . "' ";
		}
		
		if(!empty($post['outbound'])){
			$where_o .= " AND rb.o_code like '%" . $post['outbound'] . "%' ";
		}

		if(!empty($post['pccode'])){
			/*$pc_code = substr($post['pccode'], 0, 15);
			$inc = substr($post['pccode'], 15, 17);
			$where .= " AND (packing_number = '" . $pc_code . "' AND inc = " . $inc .") ";*/
			$where .= " AND ho.packing_number IN ('". implode("','",$post['pccode'])."')";
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= " AND ho.time >= '" . DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). " 00:00:00'";
			$where .= " AND ho.time <= '" . DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). " 23:59:59'";
		}

		if(!empty($post['warehouse'])){
			$this->db->join('outbound','outbound_item_receiving_details.id_outbound = outbound.id', 'left');
			$this->db->where_in('outbound.warehouse_id', $post['warehouse']);
		}

		$this->db->select([
							"packing_number"
						])
					->from("header_oird")
					->group_by("packing_number");
					
		$totalData = $this->db->get()->num_rows();
		ini_set('memory_limit','6143M'); // boost the memory limit if it's low ;)
        ini_set('max_execution_time', 4000);
		// $query = "SELECT packing_number, inc, STRING_AGG(user_packing, ', ') AS user_packing, packing_time, outbound_id, outbound_doc, loading_id, loading_doc, weight, volume, total_item_packed FROM (
		$query = "select * from (select ho.packing_number, m.id as loading_id, coalesce(m.manifest_number,'-') as loading_doc, coalesce(ho.weight,0) as weight , coalesce(ho.volume,0) as volume , usr.nama as user_packing,
		(select count(*) from outbound_item_receiving_details oird where oird.packing_number = ho.id) as total_item_packed
		,to_char(ho.time, 'DD-MM-YYYY HH24:MI:SS') as time,
		(select string_agg(distinct(o.code),' ,') from outbound_item_receiving_details oird 
		left join picking_recomendation pr on pr.id = oird.pr_id
		left join outbound o on o.id = pr.outbound_id where oird.packing_number= ho.id) as o_code
		from header_oird ho
		left join users usr on ho.user_id = usr.id
		left join manifests m on m.id = ho.shipping_id
		where 1=1 ".$where."
		ORDER BY
			".$columns[$requestData['order'][0]['column']]."
			".$requestData['order'][0]['dir'].",
			ho.packing_number DESC) as rb where 1=1".$where_o."
		LIMIT ".$requestData['length']." OFFSET ".$requestData['start'];
		
		// var_dump($query);exit;
		
		$row = $this->db->query($query)->result_array();

		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if($row[$i]['loading_doc'] == '-') {
				$action .= '<li>';
				$action .= '<a class="data-table-add '.$class.'" href="'.base_url().'packing/edit/'.$row[$i]['packing_number'].'" target="_blank"><i class="fa fa-pencil"></i> Add More Items</a>';
				$action .= '</li>';
				
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" href="javascript:void(0);" onclick="" data-id='.$row[$i]['packing_number'].'><i class="fa fa-pencil"></i> Edit Dimension</a>';
				$action .= '</li>';
			}

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				// $action .= '<a class="data-table-print '.$class.'" data-packing-number="'.$row[$i]['packing_number'].'" data-outbound="'.$row[$i]['outbound_id'].'"><i class="fa fa-print"></i> Print Packing Label</a>';
				$action .= '<a class="data-table-print '.$class.'" data-packing-number="'.$row[$i]['packing_number'].'"><i class="fa fa-print"></i> Print Packing Label</a>';
				$action .= '</li>';
            }

            /*if ($this->access_right->otoritas('delete')) {

				$disabledClass = "";
				if($row[$i]['loading_doc'] != "-")
				{
					$disabledClass = "disabled-link";
				}

                $action .= '<li>';
				$action .= '<a class="data-table-cancel '.$disabledClass.'" data-id="'.$row[$i]['packing_number'].'"><i class="fa fa-trash"></i> Cancel</a>';
				$action .= '</li>';

            }*/

			$action .= '</ul>';

			$nested = array();

			// $packing_time = $row[$i]['packing_time'];

			// if($packing_time != '-'){
			// 	$packing_time = explode(' ', $packing_time);
			// 	$packing_time = substr($packing_time[1],0,8) . '<br>' . '<span class="small-date">' . $packing_time[0] . '</span>';
			// }
			
			$user = $row[$i]['user_packing'];
			$users = explode(',',$row[$i]['user_packing']);
			if(count($users) > 1) {
				$user = '';
				for($x=0;$x<count($users);$x++){
                	if($x==0){
                    	$user = $users[0].', ';
                    }
                    
					if(strpos($user, $users[$x]) === false){
                    	$user = $user . $users[$x].', ';
                    }
				}
				$user = substr($user,0,strlen($user)-2);
			}

			$nested[] = $row[$i]['packing_number'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a class="detail_packing">'.$row[$i]['packing_number'].'</a>';
			$nested[] = '<a href="'.base_url().'loading/detail/'.$row[$i]['loading_id'].'" target="_blank">'.$row[$i]['loading_doc'].'</a>';
			// $nested[] = '<a href="'.base_url().'outbound/detail/'.$row[$i]['outbound_id'].'" target="_blank">'.$row[$i]['outbound_doc'].'</a>';
			$nested[] = $row[$i]['weight'] . ' ' . WEIGHT_UOM;
			$nested[] = $row[$i]['volume'] . ' ' . VOLUME_UOM;
			// $nested[] = $row[$i]['user_packing'];
			$nested[] = $user;
			$nested[] = $row[$i]['total_item_packed'];
			$nested[] = $row[$i]['time'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}
	
	public function get_data_label($data){

    	$sql = "SELECT
					o.code as kd_outbound, o.date as tanggal_outbound, d.destination_code as \"DestinationCode\",o.destination_name as \"DestinationName\", o.phone as \"Phone\", o.note as \"ShippingNote\", o.address as \"DestinationAddress\", o.address_1 as \"DestinationAddressL1\", address_2 as \"DestinationAddressL2\", address_3 as \"DestinationAddressL3\",
					oird.packing_number, CONCAT(oird.weight,' KG') AS weight, CONCAT(oird.volume, ' CBM') AS volume,
					COALESCE(gate_code,'-') AS gate_code, COALESCE(gate_name,'-') AS gate_name,
					TO_CHAR(o.delivery_date, 'DD-MM-YYYY') as delivery_date, 
					m_priority_name as priority,
					"/*CONCAT((
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(qty)* CAST(itm.weight as float8)),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=o.id group by itm.weight
						) as tbl
					),' KG') as weight,
					CONCAT((
						SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
							SELECT COALESCE((SUM(qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=o.id group by itm.height, itm.width, itm.length
						) as tbl
					),' M3') as volume,*/
					."d.destination_city, p.name as picking_code, o.note, o.ekspedisi
				FROM
					outbound o
				"
				/*LEFT JOIN(
					SELECT
						packing_number,
						id_outbound
					FROM
						outbound_item_receiving_details
					--WHERE
						--id_outbound = ".$data['id_outbound']."
					WHERE
						packing_number = '".$data['pc']."'
					GROUP BY
						id_outbound, packing_number
				)orb_temp ON orb_temp.id_outbound = o.id*/
				."
				LEFT JOIN m_priority mp ON mp.m_priority_id=o.m_priority_id
				LEFT JOIN destinations d ON d.destination_id=o.destination_id
				JOIN picking_list_outbound plo on o.id=plo.id_outbound
				JOIN pickings p on p.pl_id=plo.pl_id
				JOIN outbound_item_receiving_details oird ON oird.id_outbound = o.id
				LEFT JOIN gate ON gate.id = d.gate
				WHERE
					"//o.id = ".$data['id_outbound']
					."oird.packing_number = '".$data['pc']."'
				";
		$query = $this->db->query($sql);
		return $query->row();

    }
	
	public function getPcCode($post = array()){
		$result = array();
	
		$sql1 = "SELECT * FROM (
					SELECT
					CONCAT(packing_number,LPAD(CAST(inc AS VARCHAR), '3', '0')) AS packing_number FROM ho_oird
					GROUP BY packing_number,inc
				) a
				WHERE LOWER(packing_number) LIKE LOWER('%".$post['query']."%')";
				
		$sql = "select * from header_oird where lower(packing_number) like lower('%".$post['query']."%')";

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}
	
	public function getLoadingCode($post = array()){
		$result = array();
	
		$sql = "SELECT * FROM (
					SELECT manifest_number as code FROM manifests
				) a
				WHERE LOWER(code) LIKE LOWER('%".$post['query']."%')";

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}
	
	public function getOutboundCode($post = array()){
		$result = array();
	
		$sql = "SELECT * FROM (
					SELECT code FROM outbound
				) a
				WHERE LOWER(code) LIKE LOWER('%".$post['query']."%')";

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

}

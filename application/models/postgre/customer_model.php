<?php 
	
class customer_model extends CI_model{

	private $table = 'customers';

	public function __construct(){
		parent::__construct();
	}

	public function get_data(){

		$this->db
			->select('a.id as id_customer, a.code as customer_code, a.name as customer_name, a.address, a.city, a.postal as postal_code, a.country, a.phone, a.fax, a.contact_person')
			->from($this->table ." a");

        return $this->db;

	}

	public function get_by_id($supplier=''){

		$this->get_data();
		if(!empty($supplier)){
			$this->db->where('id',$idSupplier);
		}
		$data = $this->db->get();

	}
	
	public function getCustomer(){
		$result = array();
		
        $sql = "SELECT a.id as id_customer, a.code as customer_code, a.name as customer_name, a.address, a.city, a.postal as postal_code, a.country, a.phone, a.fax, a.contact_person FROM $this->table a ORDER BY (CASE WHEN split_part(code, '-',1)~E'^\\\d+$' THEN CAST (split_part(code, '-',1) AS INTEGER) ELSE 0 END)";
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function delete($id){
		$result = array();
		
		$this->db->select('count(*) as total');
		$this->db->from('inbound');
		$this->db->where('supplier_id', $id);
		$this->db->where('document_id', 2);
					
		$row = $this->db->get()->row_array();
		
		$this->db->select('count(*) as total');
		$this->db->from('outbound');
		$this->db->where('customer_id', $id);
		$this->db->where('document_id <>', 2);

		$r = $this->db->get()->row_array();
		
		$total = $row['total'] + $r['total'];
		
		if($total > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			
			$this->db->delete($this->table,['id'=>$id]);
			
			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}
		
		return $result;
	}
	
	public function getEdit($post = array()){
		$result = array();
		

		$this->get_data();
		$this->db->where('id',$post['customer_id']);
		$row = $this->db->get()->row_array();

		if($row){
			$result['status'] = 'OK';
			$result['data'] = $row;

			$warehouses = $this->db->get_where('customer_warehouses',['customer_id'=>$post['customer_id']])->result_array();
			foreach($warehouses as $wh){
				$result['data']['warehouses'][] = $wh['warehouse_id'];
			}

			$result['message'] = '';
		}else{
			$result['status'] = 'ERR';
			$result['data'] = array();
			$result['message'] = 'Get edit data failed';
		}
		
		return $result;
	}
	
	public function save($post = array()){


		$result = array();

		$data = [
			'code' => $post['customer_code'],
			'name' => $post['customer_name'],
			'address' => $post['customer_address'],
			'city' => $post['customer_city'],
			'postal' => $post['customer_postcode'],
			'country' => $post['customer_country'],
			'phone' => $post['customer_phone'],
			'fax' => $post['customer_fax'],
			'contact_person' => $post['customer_cp']
		];

		$this->db->trans_start();
		
		if(empty($post['customer_id'])){

			$q = $this->db->insert($this->table, $data);
			$id = $this->db->insert_id();

			$whData = array();

			$warehouse = $post['warehouse'];
			if($warehouse){

				foreach ($warehouse as $wh) {
					$whData[] = array(
						'warehouse_id' => $wh,
						'customer_id' => $id
					);
				}

				$this->db->insert_batch('customer_warehouses',$whData);

			}

		}else{

			$this->db->where('id', $post['customer_id']);
			$q = $this->db->update($this->table, $data);

			$this->db->where('customer_id', $post['customer_id']);
			$this->db->delete('customer_warehouses');

			$id= $post['customer_id'];
		}

		if(!empty($post['warehouse'])){

			$data2 = array();

			if(in_array('all',$post['warehouse'])){
				$this->db->select('id');
				$warehouses = $this->db->get('warehouses')->result_array();
				foreach ($warehouses as $wh) {

					$data2[] = [
						'customer_id' => $id,
						'warehouse_id' => $wh['id']
					];

				}

			}else{


				$warehouses = $post['warehouse'];

				if($warehouses){

					foreach ($warehouses as $wh) {

						$data2[] = [
							'customer_id' => $id,
							'warehouse_id' => $wh
						];

					}
				}

			}

			if($data2){
				$this->db->insert_batch('customer_warehouses',$data2);
			}

		}

		$this->db->trans_complete();
		if($this->db->trans_status()){
			$result['status'] = 'OK';
			$result['message'] = 'Save success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Save failed';
		}
		
		return $result;
	}
	
	public function checkCustomerCode($post = array()){
		$result = array();

		$this->db->select('count(*) as total');		
		if(isset($post['customer_id'])){
			if(!empty($post['customer_id'])){

				$this->db->where('id <>', $post['customer_id']);
			}
		}

		$this->db->where('LOWER(code)', strtolower($post['customer_code']));
		
		$row = $this->db->get($this->table)->row_array();

		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Customer code already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'customer_code',
			2 => 'customer_name',
			3 => 'phone'
		);
		
		$row = $this->db->get($this->table)->num_rows();
		$totalData = $row;

		$this->get_data();
		$sql = $this->db->get_compiled_select();

		$order = $columns[$requestData['order'][0]['column']];
		$order = ($order == 'customer_code') ? "code" : "(CASE WHEN split_part(a.code, '-', 1)~E'^\\\d+$' THEN CAST (split_part(a.code, '-', 1) AS INTEGER) ELSE 0 END)";

		$sql .= " ORDER BY ". $order ." ". $requestData['order'][0]['dir'] ." LIMIT ". $requestData['length'] ." OFFSET ". $requestData['start'];

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i = 0; $i < $totalFiltered; $i++){
			
			$action = '';
			
			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';
			
			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit" data-id="'.$row[$i]['id_customer'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete" data-id="'.$row[$i]['id_customer'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }
			
			$action .= '</ul>';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['customer_code'];
			$nested[] = $row[$i]['customer_name'];
			$nested[] = $row[$i]['phone'];
			$nested[] = $action;
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}

    public function getCustomerWarehouse($customerId){
        $this->db
            ->select('warehouse_id')
            ->from('customer_warehouses')
            ->where('customer_id', $customerId);

        return $this->db->get()->result_array();
    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        $inserts= array();

        for ($i=0; $i < $dataLen ; $i++) {

            $inserts = array(
                'code'        	 => $data[$i]['customer_code'],
                'name'           => $data[$i]['customer_name'],
                'address'      	 => $data[$i]['customer_addr'],
                'city'         	 => $data[$i]['customer_city'],
                'postal'	     => $data[$i]['customer_postal_code'],
                'phone'          => $data[$i]['customer_phone'],
                'country'        => $data[$i]['customer_country'],
                'fax'          	 => $data[$i]['customer_fax'],
                'contact_person' => $data[$i]['customer_contact_person']
            );

            $this->db
                ->from($this->table)
                ->where('LOWER(code)', strtolower($data[$i]['customer_code']))
                ->or_where('lower(name)', strtolower($data[$i]['customer_name']));

            $check = $this->db->get()->row_array();
            if($check){
            	$id = $check['id'];
            	$this->db->where('id',$id);
            	$this->db->update($this->table,$inserts);
            }else{
            	$this->db->insert($this->table,$inserts);
            	$id = $this->db->insert_id();
            }

            print($data[$i]);
            exit;

            if(!empty($data[$i]['warehouse'])){

                if(strtolower($data[$i]['warehouse']) == "all"){
    
                    $sql = "INSERT INTO customer_warehouses(customer_id,warehouse_id) SELECT cust.id as customer_id, wh.id as warehouse_id FROM customers cust CROSS JOIN warehouses wh where cust.id=$id";
                    $this->db->query($sql);
    
                }else{

                    $warehouses = str_replace(", ", ",", $data[$i]['warehouse']);
                    $warehouses = str_replace(" ,", ",", $warehouses);

                    $warehouse = explode(",", $warehouses);

                    $postWarehouse= array();
                    foreach ($warehouse as $wh) {

                        $getWarehouse = $this->db->get_where('warehouses',['lower(name)'=>strtolower($wh)])->row_array();
                        if(!$getWarehouse){

                            return array(
                                'status'    => false,
                                'message'   => "Warehouse '".$wh."' doesn't not exists"
                            );

                        }else{

                            $postWarehouse[] = array(
                                'customer_id' => $id,
                                'warehouse_id' => $getWarehouse['id']
                            );
                        }

                    }

                    $this->db->insert_batch('customer_warehouses',$postWarehouse);

                }

            }else{

                return array(
                    'status'    => false,
                    'message'   => "Warehouse column must be filled!"
                );

            }

        }

        $this->db->trans_complete();

        return $this->db->trans_status();

    }
}
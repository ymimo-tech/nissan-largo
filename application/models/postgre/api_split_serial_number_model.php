<?php

class Api_split_serial_number_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get($serialNumber='',$flag=0,$picking=''){

        $result = array();

        if(!empty($serialNumber)){
			
			if($flag == 1) {
				$pl_id = $this->db->get_where('pickings',['name'=>$picking])->row_array()['pl_id'];
				
				$this->db
				->select(['*','pr.qty as qtyasd'])
                ->from('item_receiving_details ird')
                ->join('locations loc','loc.id=ird.location_id','left')
                ->join('location_areas loc_area','loc_area.id=loc.location_area_id','left')
                ->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
                ->join('inbound inb','inb.id_inbound=ir.inbound_id','left')
                ->join('warehouses wh','wh.id=loc_area.warehouse_id AND loc.id NOT IN (100,102,103,104,105,106,107) or wh.id=inb.warehouse_id AND loc.id IN (100,102,103,104,105,106,107)','left')
                ->join('users_warehouses uw','uw.warehouse_id=wh.id','left')
                ->join('users usr',"usr.id=uw.user_id and usr.user_name='".$this->input->get_request_header('User', true)."'",'left')
				->join('picking_recomendation pr','ird.unique_code=pr.unique_code','left')
                ->where('ird.unique_code',str_replace('%2A','*',$serialNumber))
                ->where('pr.picking_id',$pl_id);
                //->where('last_qty >',0);
			} else {
				$this->db
					->select(['*','(CASE WHEN ird.last_qty = 0 THEN pr.qty ELSE ird.last_qty END) as qtyasd'])
					->from('item_receiving_details ird')
					->join('locations loc','loc.id=ird.location_id','left')
					->join('location_areas loc_area','loc_area.id=loc.location_area_id','left')
					->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
					->join('inbound inb','inb.id_inbound=ir.inbound_id','left')
					->join('warehouses wh','wh.id=loc_area.warehouse_id AND loc.id NOT IN (100,102,103,104,105,106,107) or wh.id=inb.warehouse_id AND loc.id IN (100,102,103,104,105,106,107)','left')
					->join('users_warehouses uw','uw.warehouse_id=wh.id','left')
					->join('users usr',"usr.id=uw.user_id and usr.user_name='".$this->input->get_request_header('User', true)."'",'left')
					->join('picking_recomendation pr','ird.unique_code=pr.unique_code','left')
					->where('ird.unique_code',str_replace('%2A','*',$serialNumber));
					//->where('last_qty >',0);
			}

            $result = $this->db->get()->row_array();
			// dd($this->db->last_query());

        }

        return $result;

    }

    function checkQty($params=array()){
        $data = $this->get($params['serial_number_old'],$params['flag'],$params['picking']);

        if($data){

            /*if($data['last_qty'] >= $params['qty']){
            	return true;
            }*/
			if($data['qtyasd'] >= $params['qty']){
            	return true;
            }

        }

        return false;

    }

    function checkSerialNumber($serialNumber=""){

    	if(!empty($serialNumber)){

    		$data = $this->db->get_where('item_receiving_details',['unique_code'=>$serialNumber])->row_array();

    		if(!$data){
    			return true;
    		}

    	}
    
    	return false;
    }

    function post($params=array()){

        // $data = $this->get($params['serial_number_old']);
		$data = $this->get($params['serial_number_old'],$params['flag'],$params['picking']);

        if($data){
            if($data['qtyasd'] >= $params['qty']){
				$this->db->trans_start();

				if($data['last_qty'] > 0 && $params['flag'] == 0) {
					$this->db
						->select(['item_receiving_id',
							"COALESCE(NULL,'".$params['serial_number_new']."') as unique_code",
							'parent_code','kd_batch','tgl_exp','tgl_in','putaway_time','location_id','do_id','picking_time','picking_id','shipping_time','shipping_id','packingcode','pl_status','ird.st_receive','user_id_receiving','user_id_putaway','user_id_picking','user_id_shipping','cycle_count_id','st_cc',
							"COALESCE(NULL,0) as first_qty",
							"COALESCE(NULL,".$params['qty'].") as last_qty",
							'qc_id','packing_id','packing_time','st_loading','user_id_loading','loading_time','st_shipping','repack_order_id','repack_order_user_id','ird.item_id','license_plate',"COALESCE(NULL,'".$params['serial_number_new']."') as rfid_tag",'production_date'])
						->from('item_receiving_details ird')
						->join('locations loc','loc.id=ird.location_id','left')
						->join('location_areas loc_area','loc_area.id=loc.location_area_id','left')
						->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
						->join('inbound inb','inb.id_inbound=ir.inbound_id','left')
						->where('unique_code',$params['serial_number_old'])
						->where('last_qty >',0);

					$val = $this->db->get_compiled_select();

					$sql = "INSERT INTO item_receiving_details(item_receiving_id, unique_code, parent_code, kd_batch, tgl_exp, tgl_in, putaway_time, location_id, do_id, picking_time, picking_id, shipping_time, shipping_id, packingcode, pl_status, st_receive, user_id_receiving, user_id_putaway, user_id_picking, user_id_shipping, cycle_count_id, st_cc, first_qty, last_qty, qc_id, packing_id, packing_time, st_loading, user_id_loading, loading_time, st_shipping, repack_order_id, repack_order_user_id, item_id, license_plate, rfid_tag, production_date) ".$val;

					$this->db->query($sql);

					$this->db
						->select('ird.id')
						->from('item_receiving_details ird')
						->join('locations loc','loc.id=ird.location_id','left')
						->join('location_areas loc_area','loc_area.id=loc.location_area_id','left')
						->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
						->join('inbound inb','inb.id_inbound=ir.inbound_id','left')
						->where('unique_code',$params['serial_number_old'])
						->where('last_qty >',0);

					$val = $this->db->get_compiled_select();

					$sqlUpdate = "UPDATE item_receiving_details SET last_qty=last_qty-".$params['qty']." WHERE id IN ($val)";


					$this->db->query($sqlUpdate);

					$this->db->trans_complete();

					return true;
					
				} else {
					$this->db
						->select(['item_receiving_id',
							"COALESCE(NULL,'".$params['serial_number_new']."') as unique_code",
							'parent_code','kd_batch','tgl_exp','tgl_in','putaway_time','location_id','do_id','picking_time','picking_id','shipping_time','shipping_id','packingcode','pl_status','ird.st_receive','user_id_receiving','user_id_putaway','user_id_picking','user_id_shipping','cycle_count_id','st_cc',
							"COALESCE(NULL,0) as first_qty",
							"COALESCE(NULL,0) as last_qty",
							'qc_id','packing_id','packing_time','st_loading','user_id_loading','loading_time','st_shipping','repack_order_id','repack_order_user_id','ird.item_id','license_plate',"COALESCE(NULL,'".$params['serial_number_new']."') as rfid_tag",'production_date'])
						->from('item_receiving_details ird')
						->join('locations loc','loc.id=ird.location_id','left')
						->join('location_areas loc_area','loc_area.id=loc.location_area_id','left')
						->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
						->join('inbound inb','inb.id_inbound=ir.inbound_id','left')
						->where('unique_code',$params['serial_number_old']);
						//->where('last_qty >',0);

					$val = $this->db->get_compiled_select();

					$sql = "INSERT INTO item_receiving_details(item_receiving_id, unique_code, parent_code, kd_batch, tgl_exp, tgl_in, putaway_time, location_id, do_id, picking_time, picking_id, shipping_time, shipping_id, packingcode, pl_status, st_receive, user_id_receiving, user_id_putaway, user_id_picking, user_id_shipping, cycle_count_id, st_cc, first_qty, last_qty, qc_id, packing_id, packing_time, st_loading, user_id_loading, loading_time, st_shipping, repack_order_id, repack_order_user_id, item_id, license_plate, rfid_tag, production_date) ".$val;

					$this->db->query($sql);
					$this->db->limit(1);
					$this->db->order_by('id','desc');
					$item_receiving_detail_id = $this->db->get('item_receiving_details')->row_array()['id'];
					// $item_receiving_detail_id = $this->db->insert_id();
					
					if($params['picking'] != null) {
						$pl_id = $this->db->get_where('pickings',['name'=>$params['picking']])->row_array()['pl_id'];
						$outbound_id = $this->db->get_where('outbound',['code'=>$params['outbound']])->row_array()['id'];
						$this->db->where('picking_id',$pl_id);
						$this->db->where('outbound_id',$outbound_id);
					}
					
					$this->db
						->select(['picking_id',"COALESCE(NULL,'".$params['serial_number_new']."') as unique_code",'item_id','location_id',"COALESCE(NULL,".$params['qty'].") as qty",'picked_qty','st_packing','user_id','st_picking','old_location_id',"COALESCE(NULL,".$item_receiving_detail_id.") as item_receiving_detail_id",'st_shipping','unit_id','outbound_id','tray_location_id'])
						->from('picking_recomendation')
						->where('unique_code',$params['serial_number_old'])
						->where('qty != 0', null);
					
					$val = $this->db->get_compiled_select();
					
					$sql = "INSERT INTO picking_recomendation(picking_id,unique_code,item_id,location_id,qty,picked_qty,st_packing,user_id,st_picking,old_location_id,item_receiving_detail_id,st_shipping,unit_id,outbound_id,tray_location_id) ".$val;
					
					$this->db->query($sql);
					
					if($params['picking'] != null) {
						$pl_id = $this->db->get_where('pickings',['name'=>$params['picking']])->row_array()['pl_id'];
						$outbound_id = $this->db->get_where('outbound',['code'=>$params['outbound']])->row_array()['id'];
						$this->db->where('picking_id',$pl_id);
						$this->db->where('outbound_id',$outbound_id);
					}
					
					$this->db
						->select('id')
						->from('picking_recomendation')
						->where('unique_code',$params['serial_number_old'])
						->where('qty > 0',null);

					$val = $this->db->get_compiled_select();

					$sqlUpdate = "UPDATE picking_recomendation SET qty=qty-".$params['qty']." WHERE id IN ($val)";
					
					$this->db->query($sqlUpdate);

					$this->db->trans_complete();

					return true;
				}
            }

        }

        return false;

    }

}

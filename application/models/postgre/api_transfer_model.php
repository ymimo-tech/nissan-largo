<?php

class Api_transfer_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($serial_number){

        $ignore_location = array(100, 101, 102, 104);

        $this->db->select("id as item_receiving_detail_id, location_id as loc_id, item_id");
        $this->db->from("item_receiving_details");
        $this->db->where("unique_code", $serial_number);
        $this->db->where_not_in("location_id", $ignore_location);
        $this->db->where_not_in("qc_id", 4);
        return $this->db->get();
    }

    function get_pl($serial_number){

        $ignore_location = array(100, 101, 102, 104);

        $this->db->select("id as item_receiving_detail_id, location_id as loc_id, item_id");
        $this->db->from("item_receiving_details");
        $this->db->where("parent_code", $serial_number);
        $this->db->where_not_in("location_id", $ignore_location);
        $this->db->where_not_in("qc_id", 4);
        return $this->db->get();

    }

    function set($data,$serialize){

        $serial = $this->get($data['kd_unik'])->row_array();

        if($serial){

            $id_user = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();

            $data_insert = array(   'unique_code'       => $data["kd_unik"], 
                                    'loc_id_old'    => $data["loc_id_old"],
                                    'user_id_pick'  => $id_user["user_id"],
                                    'pick_time'     => $data["time"],
    								'process_name'	=> 'TRANSFER',
                                    'item_receiving_detail_id' => $serialize['item_receiving_detail_id'],
                                    'item_id'       => $serialize['item_id']
                                    );

            $this->db->insert("transfers", $data_insert);

            $ignore_location = array(100, 101, 102, 104);

            $this->db->set("location_id", 103);
            $this->db->where("unique_code", $data["kd_unik"]);
            $this->db->where('last_qty >',0);
            $this->db->where_not_in("location_id", $ignore_location);
            $this->db->where_not_in("qc_id", 4);
            $this->db->update("item_receiving_details");

            return true;

        }

        return false;

    }

    function set_pl($data,$serialize){

        $serials = $this->get($data['kd_unik'])->row_array();

        if($serials){

            foreach ($serials as $serial) {

                $id_user = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();

                $data_insert = array(   'unique_code'       => $data["kd_unik"], 
                                        'loc_id_old'    => $data["loc_id_old"],
                                        'user_id_pick'  => $id_user["user_id"],
                                        'pick_time'     => $data["time"],
                                        'process_name'  => 'TRANSFER',
                                        'item_receiving_detail_id' => $serialize['item_receiving_detail_id']
                                        );

                $this->db->insert("transfers", $data_insert);

                $ignore_location = array(100, 101, 102, 104);

                $this->db->set("location_id", 103);
                $this->db->where("parent_code", $data["kd_unik"]);
                $this->db->where_not_in("location_id", $ignore_location);
                $this->db->where('last_qty >',0);
                $this->db->where_not_in("qc_id", 4);
                $this->db->update("item_receiving_details");
            }

            return true;
        }

        return false;
    }

    function retrieve($serial_number){

        $ignore_location = array(100, 101, 102, 104);

        $this->db->select("unique_code as kd_unik, loc.name as loc_name");
        $this->db->from("item_receiving_details ird");
        $this->db->join("locations loc", "loc.id=ird.location_id", "left");
        $this->db->where("unique_code", $serial_number);
        $this->db->where_not_in("location_id", $ignore_location);
        $this->db->where_not_in("qc_id", 4);

        return $this->db->get();

    }

    function retrieve_pl($serial_number){

        $ignore_location = array(100, 101, 102, 104);

        $this->db->select("parent_code as kd_unik, loc.name as loc_name");
        $this->db->from("item_receiving_details ird");
        $this->db->join("locations loc", "loc.id=ird.location_id", "left");
        $this->db->where("parent_code", $serial_number);
        $this->db->where_not_in("location_id", $ignore_location);
        $this->db->where_not_in("qc_id", 4);
        $this->db->group_by('parent_code');

        return $this->db->get();
    }

    function post($data){

    	$id_loc_old 	= $this->get_id("id as loc_id", "name", $data["loc_name_old"], "locations")->row_array();
    	$id_user_old 	= $this->get_id("id as user_id", "user_name", $data["user_pick"], "users")->row_array();
    	$id_loc_new		= $this->get_id("id as loc_id", "name", $data["loc_name_new"], "locations")->row_array();
    	$id_user_new 	= $this->get_id("id as user_id", "user_name", $data["user_put"], "users")->row_array();

        if(!empty($id_loc_old) && !empty($id_loc_old) && !empty($id_loc_new) && !empty($id_user_new)){

            $serialNumber = $data["kd_unik"];

            $this->db->select(["(SELECT id FROM transfers WHERE item_receiving_detail_id=ird.id AND process_name='TRANSFER' ORDER BY id DESC LIMIT 1 ) as transfer_id","ird.id as item_receiving_detail_id"]);
            $this->db->from("item_receiving_details ird");
            $this->db->where('location_id',103);
            $this->db->where('last_qty >',0);
            $this->db->where("ird.unique_code='$serialNumber' or ird.parent_code='$serialNumber'", NULL);

            $datas = $this->db->get()->result_array();

            $transfers = array();
            $itemReceivingDetails = array();

            if($datas){

                foreach ($datas as $d) {

                    $transfers[] = array(
                        'id' => $d['transfer_id'],
                        'loc_id_new' => $id_loc_new['loc_id'],
                        'put_time' => $data['put_time'],
                        'user_id_put' => $id_user_new['user_id'],
                        'process_name'=> 'TRANSFER'
                    );

                    $itemReceivingDetails[] = array(
                        'id' => $d['item_receiving_detail_id'],
                        'location_id' => $id_loc_new['loc_id']
                    );

                }

            }

            //NOTE :: Update ID untuk transfers

            $this->db->trans_start();

            $this->db->update_batch('item_receiving_details',$itemReceivingDetails,'id');
            $this->db->update_batch('transfers',$transfers,'id');

            $this->db->trans_complete();
			
			/* SEND TO STAGING 
			$staging = $this->load->database('staging', TRUE);
			
			$irdDataFrom = $this->db->query("
						SELECT l.absentry as loc_from, w.name as whsfrom
						FROM locations l
						JOIN location_areas larea ON l.location_area_id = larea.id
						JOIN warehouses w ON larea.warehouse_id = w.id
						WHERE l.id = ".$id_loc_old['loc_id'])->row_array(); 
			$irdDataTo = $this->db->query("
						SELECT ird.unique_code as unique_code, itm.code as itemcode, ird.last_qty as qty, l.absentry as loc_to, w.name as whsto
						FROM item_receiving_details ird
						JOIN items itm ON ird.item_id = itm.id
						JOIN locations l ON ird.location_id = l.id
						JOIN location_areas larea ON l.location_area_id = larea.id
						JOIN warehouses w ON larea.warehouse_id = w.id
						WHERE ird.id = '".$itemReceivingDetails[0]['id']."'
						")->row_array();
			
			$this->db->limit(1);
			$this->db->order_by('id','desc');
			$transfer_id = $this->db->get('transfers')->row_array()['id'];
					
			$data_insert_1	= 	array(
				'DOCDATE'			=> date('Y-m-d'),
				'DOCDUEDATE'		=> date('Y-m-d'),
				'TAXDATE'			=> date('Y-m-d'),
				'BASEENTRY'			=> NULL,
				'BASELINE'			=> NULL,
				'BASETYPE'			=> NULL,
				'ITEMCODE'			=> $irdDataTo['itemcode'],
				'QTY'				=> $irdDataTo['qty'],
				'FROM_WAREHOUSE'	=> $irdDataFrom['whsfrom'],
				'TO_WAREHOUSE'		=> $irdDataTo['whsto'],
				'INTERNALNO'		=> $transfer_id
			);
			
			$data_insert_2	= 	array(
				'BASEENTRY'			=> NULL,
				'BASELINE'			=> NULL,
				'BINENTRY'			=> $irdDataFrom['loc_from'],
				'BINQTY'			=> $irdDataTo['qty'],
				'INTERNALNO'		=> $transfer_id
			);
			
			$data_insert_3	= 	array(
				'BASEENTRY'			=> NULL,
				'BASELINE'			=> NULL,
				'BINENTRY'			=> $irdDataTo['loc_to'],
				'BINQTY'			=> $irdDataTo['qty'],
				'INTERNALNO'		=> $transfer_id
			);
			
			$stagingCheckQuery = "SELECT * FROM INVENTORY_TRANSFER WHERE INTERNALNO = '".$transfer_id."'";
			$stagingCheckData = $staging->query($stagingCheckQuery)->row_array();
			if(count($stagingCheckData) == 0) {
				if($irdDataFrom['whsfrom'] == NULL || $irdDataFrom['whsfrom'] == '' || strlen($irdDataFrom['whsfrom']) == 0) {
					
				} else {
					$staging->insert('INVENTORY_TRANSFER', $data_insert_1);
					$staging->insert('ITBINFROM', $data_insert_2);
					$staging->insert('ITBINTO', $data_insert_3);
				}
			}
			/*---------------*/

            return array(
                "status" => true
            );

        }else{

            return array(
                "status" => false,
                "message" => "Failed to insert data"
            );

        }
    }

    function cancel($data){
        $id_loc         = $this->get_id("id as loc_id", "name", $data["loc_name_old"], "locations")->row_array();
        $id_user        = $this->get_id("id as user_id", "user_name", $data["user_pick"], "users")->row_array();
        
        $this->db->set("location_id", $id_loc["loc_id"]);
        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->where("last_qty >",0);
        $this->db->where("location_id",103);
        $this->db->update("item_receiving_details");

        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->where("user_id_pick", $id_user["user_id"]);
        $this->db->where("loc_id_old", $id_loc["loc_id"]);
        $this->db->where("loc_id_new IS NULL");
        $this->db->where("user_id_put IS NULL");
        $this->db->delete("transfers");
    }


    function get_location($location){
        $this->db->select("id as loc_id, name as loc_name, location_type_id as loc_type_id, location_area_id as loc_area_id, location_category_id as loc_category_id, loc_type");
        $this->db->from("locations");
        $this->db->where("name", $location);
        $this->db->where_in("location_type_id", array(1,2,3));
        return $this->db->get();
    }
	
	function getOuterLabelItem($params=array()){

		$this->db
            ->select('DISTINCT(unique_code) as unique_code')
			->from('item_receiving_details')
			->where('last_qty >',0)
			// ->where('qc_id',1)
			->where('parent_code',$params['parent_code']);
		
		$data = $this->db->get()->result_array();

        return $data;
    }
	

}

<?php
class referensi_lokasi_type_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'location_types';

    public function data($condition = array()) {

        $this->db->select('a.id as loc_type_id, a.name as loc_type_name, a.description as loc_type_desc');
        $this->db->from($this->table  . ' a');  
        $this->db->where('a.name IS NOT NULL', NULL);
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function getLocationById($post = array()){
		$result = array();

        $this->data();
        $this->db->where_in('id', implode(",", $post['loc_type_id']));

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getLocation(){
		$result = array();

        $this->data();
        $this->db->where('name IS NOT NULL', NULL);

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getQtyLocation(){
		$result = array();

        $this->db->select('count(*) as total');

		$row = $this->db->get($this->table)->row_array();
		$result['qty'] = $row['total'];

		return $result;
	}

    public function get_by_id($id) {
        $this->db->where('a.id', $id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    private function getValue($data=[]){
        $data = [
            "name" => $data['loc_type_name'],
            "description" => $data['loc_type_desc']
        ];

        return $data;
    }

    public function create($data) {
        $data = $this->getValue($data);
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        $data = $this->getValue($data);
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {

        $result = array();

        $in = explode(',', $this->config->item('hardcode_location_id'));

        if(in_array($id, $in)){
            $result['status'] = 'ERR';
            $result['message'] = 'Cannot delete master location';
        }else{

            // $sql = 'SELECT
            //          COUNT(*) AS total
            //      FROM
            //          receiving_barang
            //      WHERE
            //          loc_category_id=\''.$id.'\'';
            //
            // $row = $this->db->query($sql)->row_array();
            //
            // if($row['total'] > 0){
            //  $result['status'] = 'ERR';
            //  $result['message'] = 'Cannot delete location, because data have relation to another table';
            // }else{
                $this->db->delete($this->table, array('id' => $id));

                $result['status'] = 'OK';
                $result['message'] = 'Delete data success';
            // }
        }

        return $result;

    }

    public function options($default = '--Pilih Nama Lokasi Type--', $key = '') {
        $data = $this->data()->get();
        $options = array();
    
        if (!empty($default))
            $options[$key] = $default;
    
        foreach ($data->result() as $row) {
            $options[$row->loc_type_id] = $row->loc_type_name ;
        }
        return $options;
    }

}

?>

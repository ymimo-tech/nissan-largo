<?php
class kitting_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'kittings';
    private $table2 = 'kitting_items';

    public function data($condition = array()) {
        $this->db->select('a.id as id_kitting, i.id as id_barang, i.code as kd_barang, i.name as nama_barang');
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);
        $this->db->join('items i','i.id=a.item_id','LEFT');
        return $this->db;
    }

    public function get_by_id($id) {
        $this->db->where('a.id',$id);
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function get_list($id=''){
        if(!empty($id)){
            $this->db->where('a.id',$id);
        }

        return $this->data();
    }

    public function get_data_kitting_item($id=0){
        $this->db->select('id as id_kitting_product, item_id as id_barang, qty');
        $result = $this->db->get_where($this->table2,array('kitting_id'=>$id));
        return $result;
    }

    public function export($condition=[]){
        $this->db
            ->select('itm1.code as serial_number, itm1.name as kitting_name')
            ->select(["CONCAT(itm2.code, ' - ', itm2.name) as bom"])
            ->from($this->table.' a')
            ->join($this->table2.' b','b.kitting_id=a.id')
            ->join('items itm1','itm1.id=a.item_id','left')
            ->join('items itm2','itm2.id=b.item_id','left');

        return $this->db->get();
    }


    public function create($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function insert_kitting_item($data=array()){
        $result = $this->db->insert($this->table2, $data);
        return $result;
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function update_kitting_item($data=array(),$id=0){
        $result = $this->db->update($this->table2, $data,array('id'=>$id));
        return $result;
    }

    public function delete($id) {
        
        $result = array();

        $this->db
            ->select('count(*) as total')
            ->from('outbound_item outbitm')
            ->join('kittings kit','kit.item_id=outbitm.item_id')
            ->where('kit.id',$id);

        $row= $this->db->get()->row_array();

        if($row['total'] > 0){

            return false;

        }else{

            $this->db->trans_start();

            $this->db->delete($this->table, ['id' => $id]);
            $this->db->delete($this->table2, ['kitting_id'=>$id]);

            $this->db->trans_complete();

            return true;

        }
    }

    public function delete_kitting_item($id_kitting_product=0){
        $result = $this->db->delete($this->table2, array('id' => $id_kitting_product));
        return $result;
    }

    public function options($default = '-- Pilih Kitting --', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_kitting] = $row->nama_barang ;
        }
        return $options;
    }

}

?>

<?php

class Api_putaway_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    function get($serial_number){

        $this->db->select("ird.location_id as loc_id, ir.receiving_id as id_receiving, st_receiving, ird.id, ird.putaway_time");
        $this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir",'ir.id=ird.item_receiving_id','left');
        $this->db->join("receivings r", "r.id = ir.receiving_id", "left");
        $this->db->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id');
        $this->db->join("inbound inb", "inb.id_inbound = hpi.inbound_id", "left");
        // $this->db->where('r.st_receiving',1);
        $this->db->where("ird.unique_code", $serial_number);
        $this->db->where("ird.putaway_time", NULL);
        $this->db->where("ird.picking_id", NULL);
        return $this->db->get();

    }

    function get_lp($serial_number){
        $this->db->select("ird.location_id as loc_id, ir.receiving_id as id_receiving, st_receiving, ird.id, ird.putaway_time");
        $this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir",'ir.id=ird.item_receiving_id','left');
        $this->db->join("receivings r", "r.id = ir.receiving_id", "left");
        $this->db->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id','left');
        $this->db->join("inbound inb", "inb.id_inbound = hpi.inbound_id", "left");
        $this->db->where('r.st_receiving',1);
        $this->db->where("ird.parent_code", $serial_number);
        $this->db->where("ird.putaway_time", NULL);
        $this->db->where("ird.picking_id", NULL);
        return $this->db->get();
    }

    function get_sku($sku){
        $this->db->from("items");
        // $this->db->where('status','Active');
        $this->db->where('code',$sku);
        $this->db->where('is_active',1);
        return $this->db->get();
    }

    function get_sku_serial($sku, $qr){
        $this->db->select('*');
        $this->db->from('items a');
        $this->db->join('item_receiving_details b', 'b.item_id=a.id');
        $this->db->where('a.code',$sku);
        $this->db->where('b.unique_code',$qr);
        return $this->db->get();
    }

    function set($data){
        $this->db->select('id')
            ->from('item_receiving_details')
            ->where("unique_code", $data["kd_unik"])
            ->where('putaway_time',NULL)
            ->where('picking_id',NULL)
            ->where('unique_code',$data['kd_unik'])
            ->order_by('id','ASC');

        $serial =$this->db->get()->row_array();
        if($serial){
            $this->db->set("location_id", 103);
            $this->db->where('id',$serial['id']);
            $this->db->update("item_receiving_details");
        }
    }

    function set_lp($data){
        $this->db->set("location_id", 103);
        $this->db->where("parent_code", $data["kd_unik"]);
        $this->db->where('putaway_time',NULL);
        $this->db->where('picking_id',NULL);
        $this->db->update("item_receiving_details");
    }

    function start($data){
        $this->db->set("start_putaway", "COALESCE(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("id", $data["id_receiving"]);
        $this->db->update("receivings");
    }

    function start_lp($data){
        $this->db->set("start_putaway", "COALESCE(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("id", $data["id_receiving"]);
        $this->db->update("receivings");
    }

    function send_staging($kd_receiving=''){
        $this->load_model('api_staging_model');
        $sql = "
            SELECT
                kd_barang,
                rb.id_barang,
                sum(first_qty)AS qty,
                r.id_receiving,
                i.id_staging,
                i.kd_inbound,
                kd_receiving,
                delivery_doc,
                max(tgl_in)AS rcv_date,
                FinaCode,
                max(id_receiving_barang) as max_id
            FROM
                receiving_barang rb
            LEFT JOIN barang b ON b.id_barang = rb.id_barang
            LEFT JOIN receiving r ON r.id_receiving = rb.id_receiving
            LEFT JOIN inbound i ON i.id_inbound = r.id_po
            LEFT JOIN(
                SELECT
                    id_barang,
                    id_inbound,
                    FinaCode
                FROM
                    inbound_barang
                GROUP BY
                    id_inbound,
                    id_barang
            )t_ib ON t_ib.id_barang = b.id_barang
            AND t_ib.id_inbound = i.id_inbound
            WHERE
                1=1
            AND i.id_staging > 0
            AND rb.st_staging = 0
            AND rb.id_qc = 1
            GROUP BY
                id_receiving,
                kd_barang,
                i.id_staging,
                i.kd_inbound";
        $query = $this->db->query($sql);
        $Line = 0;
        foreach ($query->result() as $row) {
            $Line++;
            //================= start update page code 6542========
            //== L2 no longer update, will continue insert row

            $NoLine = $Line;
            $sql = "select max(LineNum) as maxline
                    from staging_inbound_l2
                    where RcvDoc = '".$row->delivery_doc."'";
            $query = $this->db->query($sql);
            $hasil = $query->row();
            if(!empty($hasil->maxline)){
                $NoLine = $hasil->maxline + 100;
            }else{
                $NoLine = 100;
            }
            $data = array(
                'DocEntry' =>$row->id_staging,
                'LineNum' =>$NoLine,
                'SKU'=>$row->kd_barang,
                'ReceiveQty'=>$row->qty,
                'SyncStatus'=>'N',
                'CreateDate'=>date('Y-m-d H:i:s'),
                'RcvDoc'=>$row->delivery_doc,
                'RcvDate'=>$row->rcv_date,
                'InbNo'=> $row->kd_inbound,
                'FinaCode' => $row->FinaCode
                );

            $result = $this->api_staging_model->_curl_process('insert','f_t_inbound_l2',array('data'=>$data));
            if(!empty($result)){
                if($result->status == '001'){
                    $this->db->insert('staging_inbound_l2',$data);
                    $data_update = array(
                        'st_staging' => 1);
                    $data_where = array(
                        'id_receiving_barang <= '.$row->max_id => NULL,
                        'st_staging' => 0,
                        'id_barang' => $row->id_barang,
                        'id_receiving' => $row->id_receiving);
                    $this->db->where($data_where);
                    $this->db->update('receiving_barang',$data_update);
                    //echo '1212';
                }
            }

        }

        $this->load_model('api_sync_model');
        $this->api_sync_model->sync_qty();

    }

    function finish($receiving){

        $this->db->set("start_putaway", "COALESCE(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->set("finish_putaway", date("Y-m-d H:i:s"));
        $this->db->set("finish_tally", date("Y-m-d H:i:s"));
        $this->db->set("st_receiving", 0);
        $this->db->set("remark", "OK");
        $this->db->where("code", $receiving);
        $this->db->update("receivings");

        $this->db
            ->select('r.id as receiving_id')
            ->from('receivings r')
            ->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id')
            ->join("inbound inb", "inb.id_inbound = hpi.inbound_id", "left")
            // ->join('inbound inb','inb.id_inbound=r.po_id')
            // ->join('documents doc','doc.document_id=inb.document_id')
            ->where('r.code',$receiving);

        $data = $this->db->get()->row_array();
        if(!empty($data)){
            // $this->sendToStaging($data['receiving_id']);
            // $this->load_model('api_integration_model');
            // $this->api_integration_model->post_new_goods_receipt($receiving);
        }

    }

    function count_putaway($data){

        $this->db->start_cache();

        $this->db
            ->select('r.id as id_receiving')
            ->from("item_receiving_details ird")
            ->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
            ->join("receivings r", "r.id = ir.receiving_id", "left")
            ->where('r.finish_putaway',NULL)
            ->order_by('ird.id','ASC');

        $this->db->stop_cache();

        $this->db->where('unique_code', $data['kd_unik']);

        $id_receiving   = $this->db->get();

        if($id_receiving->num_rows()>0){

            $id_receiving = $id_receiving->row_array();

        }else{

            $this->db->where('parent_code', $data['kd_unik']);
            $id_receiving = $this->db->get()->row_array();

        }

        $this->db->flush_cache();

        if($id_receiving){

            $id_barang = $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();


            $this->db->start_cache();

            $this->db->from("item_receiving_details ird");
            $this->db->join('item_receiving ir','ir.id=ird.item_receiving_id','left');
            $this->db->join("receivings r", "r.id = ir.receiving_id", "left");
            $this->db->where("r.id", $id_receiving["id_receiving"]);

            $this->db->stop_cache();

            $this->db->select('count(unique_code)');
            $qty = $this->db->get_compiled_select();

            $this->db->select('count(unique_code)');
            $this->db->where('user_id_putaway IS NOT NULL', NULL);
            $putted = $this->db->get_compiled_select();

            $this->db->select("r.code as kd_receiving, ($qty) AS qty, ($putted) AS putted");
            $res = $this->db->get();

            $this->db->flush_cache();
            return $res;
        }else{
            return false;
        }

    }

    function get_last_location($item_code){

        $limit = 1;

		$this->db
			->select(["COALESCE(loc.name, '-') as location_name"])
			->from('item_receiving_details ird')
			->join('items itm','itm.id=ird.item_id')
			->join('locations loc','loc.id=ird.location_id')
			->where('UPPER(loc.status)','ACTIVE')
			->where('itm.code', $item_code)
			->where_not_in('loc.id',[100,101,102,103,104,105,106,107])
			->where_in('loc.location_type_id',[2,3])
			->group_by('loc.name, ird.id')
            ->order_by('ird.id', 'DESC')
			->limit($limit);

        $locations = $this->db->get()->result_array();
		if(count($locations) > 0) {
			return $locations;
		} else {
			$locations[0] = array(
							"location_name"    => '-'
							);
			return $locations;
		}
    }


    function retrieve($serial_number){
        $this->db->select("itm.code as kd_barang, ird.unique_code as kd_unik");
        $this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir",'ir.id=ird.item_receiving_id','left');
        $this->db->join("items itm", "itm.id=ir.item_id", "left");
        $this->db->where("ird.unique_code", $serial_number);
        return $this->db->get();
    }

    function retrieve_lp($serial_number){
        $this->db->select("itm.code as kd_barang, ird.parent_code as kd_unik");
        $this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir",'ir.id=ird.item_receiving_id','left');
        $this->db->join("items itm", "itm.id=ir.item_id", "left");
        $this->db->where("ird.parent_code", $serial_number);
        $this->db->limit(1);
        return $this->db->get();
    }

    function post($data){
        ini_set ( 'max_execution_time', -1);
        $id_loc         = $this->get_id("id as loc_id", "name", $data["loc_name"], "locations")->row_array();
        $id_user        = $this->get_id("id as user_id", "user_name", $data["user_name"], "users")->row_array();
        $id_user_pick   = $this->get_id("id as user_id", "user_name", $data["user_pick"], "users")->row_array();

        if($id_loc && $id_user && $id_user_pick){

            $this->db->select('ird.id,item_receiving_id, ir.item_id as item_id, ird.qc_id, ird.unique_code, ird.parent_code, r.code as rcv_code');
            $this->db->from('item_receiving_details ird');
            $this->db->join('item_receiving ir', 'ird.item_receiving_id=ir.id', 'LEFT');
            $this->db->join('receivings r', 'ir.receiving_id=r.id', 'LEFT');
            $this->db->where("( unique_code='".$data['kd_unik']."' OR parent_code='".$data['kd_unik']."')",NULL);
            $this->db->where('putaway_time',NULL);
            $this->db->where('picking_id',NULL);
            $this->db->order_by('ird.id','ASC');
            $item_receiving = $this->db->get()->row_array();

            if($item_receiving){

                // Check Category Location
                // $this->db
                //     ->from('locations loc')
                //     ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
                //     ->join('warehouses wh','wh.id=loc_area.warehouse_id')
                //     ->where('loc.name',$data['loc_name']);

                $location = $this->db->get_where('locations',['name'=>$data['loc_name'],'UPPER(status)'=>'ACTIVE'])->row_array();

                if($item_receiving['qc_id'] == 2 && $location['location_type_id'] != 4){
                        $result['status']   = false;
                        $result['message']  = "This Items Must Be Put on QC Location.";
                }else{
                // if($location['location_type_id'] == 4){
                //     $this->db->set('qc_id',2);
                //     $this->db->set('parent_code',NULL);
                // }

                $this->db->set('qc_id',3);
                $this->db->set('parent_code',NULL);
                // if($location['location_type_id'] == 5){
                // }

                $this->db->set("location_id", $id_loc["loc_id"]);
                $this->db->set("putaway_time", $data["putaway_time"]);
                $this->db->set("user_id_putaway", $id_user["user_id"]);

                if($item_receiving['unique_code'] === $data['kd_unik']){
                   $this->db->where("unique_code", $data["kd_unik"]);
                   $this->db->where("item_receiving_id", $item_receiving['item_receiving_id']);
                }else{
                   $this->db->where("parent_code", $data["kd_unik"]);
                }

                $this->db->where('putaway_time',NULL);
                $this->db->where('picking_id',NULL);
                $this->db->update("item_receiving_details");

                $data_insert_details    = array(
                                            "loc_id_old"    => 100,
                                            "loc_id_new"    => $id_loc["loc_id"],
                                            "unique_code"   => $data["kd_unik"],
                                            "user_id_pick"  => $id_user_pick["user_id"],
                                            "pick_time"     => $data["pick_time"],
                                            "put_time"      => $data["putaway_time"],
                                            "user_id_put"   => $id_user["user_id"],
                                            "process_name"  => "PUTAWAY"
                                            );

                $this->db->insert("transfers", $data_insert_details);

                $result['status'] = true;
                $result['item_receiving_id'] = $item_receiving['id'];

                $this->load_model('api_integration_model');
                $this->api_integration_model->post_new_goods_receipt_line($item_receiving['rcv_code'],$item_receiving['item_id']);

				// $this->load_model('api_integration_model');
        //         $this->api_integration_model->post_receipt($item_receiving['rcv_code'], $data["kd_unik"]);
                }
            }else{

                $result['status']   = false;
                $result['message']  = "Serial Number or Outer Label Not Available.";

            }
        }else{

            $result['status']   = false;
            $result['message']  = "";

            if(empty($id_loc)){
                $result['message']  = "Location doesn't exist.";
            }else if(empty($id_user)){
                $result['message']  = "User not available.";
            }else{
                $result['message']  = "User Pick not available.";
            }

        }

        // $this->load_model('api_integration_model');
        // $this->api_integration_model->post_new_goods_issue($item_receiving['rcv_code']);

        return $result;
    }

    function post_cancel($data){
        $id_barang = $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();

        $this->db->set("location_id", 100);
        $this->db->where("( unique_code='".$data["kd_unik"]."' OR parent_code='".$data['kd_unik']."')",NULL);
        $this->db->update("item_receiving_details");

        $this->db->where("unique_code", $data["kd_unik"]);
        $this->db->where("process_name", "PUTAWAY");
        $this->db->delete("transfers");
    }

    function get_location($location){
        $avail_location = array("1", "3", "4","5"); // Putaway, put n pick, QC, NG
        $this->db->from("locations");
		$this->db->where('UPPER(status)','ACTIVE');
        $this->db->where("name", $location);
        $this->db->where_in("location_type_id", $avail_location);
        return $this->db->get();
    }

    function get_location_serial($location, $serial_number){
        $type   = $this->get_id("location_type_id as loc_type_id", "name", $location, "locations")->row_array();
        $id_qc  = $this->get_id("qc_id as id_qc", "unique_code", $serial_number, "item_receiving_details");
        $id_qc_lp  = $this->get_id("qc_id as id_qc", "parent_code", $serial_number, "item_receiving_details");

        if($id_qc->num_rows() > 0){
           $id_qc = $id_qc->row_array();
        }else{
           $id_qc = $id_qc_lp->row_array();
        }
        if(($type["loc_type_id"] == "1" || $type["loc_type_id"] == "3") && $id_qc["id_qc"] == 1){ // item good
            return true;
        } elseif ($type["loc_type_id"] == "4" && $id_qc["id_qc"] == 2) { // item QC
            return true;
        } elseif ($type["loc_type_id"] == "5" && $id_qc["id_qc"] == 3) { // item NG
            return true;
        } else{
            return false;
        }

    }

    function cek_kategori($location, $serial_number){

        $this->db->select('category_id as id_kategori');
        $this->db->from('items itm');
        $this->db->join('item_receiving ir','ir.item_id=itm.id','left');
        $this->db->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left');
        $this->db->where('putaway_time',NULL);
        $this->db->where('picking_id',NULL);
        $this->db->where('ird.unique_code',$serial_number);

        $id_kategori = $this->db->get();
        if($id_kategori->num_rows()>0){
            $id_kategori = $id_kategori -> row_array();
        }else{
            $this->db->select('category_id as id_kategori');
            $this->db->from('items itm');
            $this->db->join('item_receiving ir','ir.item_id=itm.id','left');
            $this->db->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left');
            $this->db->where('putaway_time',NULL);
            $this->db->where('picking_id',NULL);
            $this->db->where('ird.parent_code',$serial_number);
            $id_kategori = $this->db->get()->row_array();
        }

        if($id_kategori){

            $id_loc_kategori = $this->get_id("location_category_id as loc_category_id", "name", $location, "locations")->row_array();

            if($id_kategori['id_kategori'] == $id_loc_kategori['loc_category_id']){
                return TRUE;
            }else if(strtolower($location) == 'temporary bin'){
              return TRUE;
            }else{
                return FALSE;
            }

        }else{

            return false;

        }

    }

    function getSerialLocation($location,$serialNumber){

        $this->db->select('inb.warehouse_id');
        $sn = $this->get($serialNumber)->row_array();
        if(!$sn){
            $this->db->select('inb.warehouse_id');
            $sn = $this->get_lp($serialNumber)->row_array();
        }

        $this->db
            ->select("loc.*")
            ->from('item_receiving_details ird')
            ->join('locations loc','loc.id=ird.location_id')
            ->join('location_areas loc_area','loc_area.id=loc.location_area_id')
			->where('UPPER(loc.status)','ACTIVE')
            ->where('putaway_time IS NOT NULL',NULL)
            ->where('loc.loc_type',NULL)
            // ->where_not_in('loc.location_type_id',[4,5])
            ->where_not_in('loc.location_type_id',[5])
            ->where("(unique_code='".$serialNumber."' OR parent_code='".$serialNumber."')",NULL)
            ->where('loc_area.warehouse_id',$sn['warehouse_id']);

        $serial = $this->db->get()->row_array();

        if($serial){
            if(strtolower($serial['name']) == strtolower($location)){

                return false;

            }else{

                return $serial;

            }
        }else{
            return false;
        }

    }

    public function putaway_serial($serial_number){

        $this->db
            ->select(array('itm.code as item_code',"COALESCE(NULL,'') as image","(CASE WHEN r.start_tally = NULL AND r.finish_tally = NULL THEN FALSE ELSE TRUE END) as status"))
            ->from('item_receiving_details ird')
            ->join('item_receiving ir','ir.id=ird.item_receiving_id')
            ->join('receivings r','r.id=ir.receiving_id')
            ->join('items itm','itm.id=ird.item_id')
            ->join('units unt','unt.id=itm.unit_id','left')
            ->where('ird.unique_code',$serial_number)
            ->or_where('ird.parent_code',$serial_number);

        $res = $this->db->get();

        return $res->row_array();

    }

    function getOuterLabelItem($outerLabel){

        $this->db->select(["DISTINCT(ird.unique_code) as unique_code","itm.code as kd_barang"]);
        $this->db->from("item_receiving_details ird");
        $this->db->join("item_receiving ir",'ir.id=ird.item_receiving_id');
        $this->db->join("receivings r", "r.id = ir.receiving_id");
        $this->db->join('hp_receiving_inbound hpi','hpi.receiving_id=r.id');
        $this->db->join("inbound inb", "inb.id_inbound = hpi.inbound_id");
        // $this->db->join('inbound inb','inb.id_inbound=r.po_id');
        $this->db->join('users_warehouses uwh','uwh.warehouse_id=inb.warehouse_id');
        $this->db->join('users usr','usr.id=uwh.user_id');
        $this->db->join('items itm','itm.id=ird.item_id');
        $this->db->where('usr.user_name',$this->input->get_request_header('User', true));
        $this->db->where('r.st_receiving',1);
        $this->db->where("ird.parent_code", $outerLabel);
        $this->db->where("ird.putaway_time", NULL);
        $this->db->group_by('ird.unique_code,itm.code');

        $data = $this->db->get()->result_array();

        return $data;
    }
/*
    function sendToStaging($receivingId){

        $user  = $this->session->userdata('username');

        $this->load_model('api_staging_model');

        $this->db
            ->select([
                "inb.id_inbound as inbound_id","inb.code as inbound_name","inb.date as inbound_date","inb.document_id as inbound_document","doc.name as document_name","doc.movement_type",
                "wh.name as warehouse_name","itm.code as item_code","itm.name as item_name","itm.sku as item_sku","spl.code as supplier_code",
                "unt.code as unit_code","doc.movement_type","rcv.dock as driver_name","COALESCE(NULL,'') as cost_center_code","COALESCE(SUM(ird.first_qty),0) as qty","wg.plant_sap","wg.storage_location_sap","ird.qc_id","inbitm.po_item","rcv.code as receiving_code","inb.remark as inbound_remark","rcv.remark as receiving_remark","whinbg.storage_location_sap as warehouse_storage_loc","itm.sap_reject_code"
            ])
            ->select(["inb.po_number","inbitm.po_item"])
            ->from('receivings rcv')
            ->join('inbound inb','inb.id_inbound=rcv.po_id')
            ->join("documents doc",'doc.document_id=inb.document_id')
            ->join('warehouses wh','wh.id=inb.warehouse_id')
            ->join('warehouses_warehouse_groups wwg','wwg.warehouse_id=wh.id')
            ->join('warehouse_groups wg','wg.id=wwg.warehouse_group_id')
            ->join('warehouses whinb','whinb.id=inb.supplier_id','left outer')
            ->join('warehouses_warehouse_groups whinbwg','whinbwg.warehouse_id=whinb.id and inb.document_id IN (3,14)','left')
            ->join('warehouse_groups whinbg','whinbg.id=whinbwg.warehouse_group_id','left')
            ->join('cost_center cost','cost.id=inb.cost_center_id','left')
            ->join('item_receiving ir','ir.receiving_id=rcv.id','left')
            ->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left')
            ->join("items itm",'itm.id=ir.item_id','left')
            ->join("units unt",'unt.id=itm.unit_id','left')
            ->join("suppliers spl","spl.id=inb.supplier_id and inb.document_id NOT IN (3,14)",'left')
            ->join('inbound_item inbitm','inbitm.inbound_id=inb.id_inbound and inbitm.item_id=itm.id','left')
            ->where('ird.qc_id <>',1)
            ->where('rcv.id',$receivingId)
            ->group_by('inb.id_inbound, inb.code, inb.date, doc.movement_type, inb.document_id, wh.name, itm.code, itm.name, itm.sku, spl.code,  unt.code, rcv.dock, wg.plant_sap, wg.storage_location_sap, ird.qc_id, inb.po_number, inbitm.po_item, rcv.code, inb.remark, doc.name, rcv.remark, whinbg.storage_location_sap, itm.sap_reject_code')
            ->having("SUM(ird.first_qty) > 0");

        $data = $this->db->get()->result_array();

        if($data){

            date_default_timezone_set('UTC');

            $pstngDate = strtotime("now");
            $pstngDate = $pstngDate*1000;
            $pstngDate = number_format($pstngDate, 0, '.', '');

            $docDate = strtotime($data[0]['inbound_date']);
            $docDate = $docDate*1000;
            $docDate = number_format($docDate, 0, '.', '');

            $post = array(
                'Extid'     => $data[0]['receiving_code']."#".$user."##",
                'PstngDate' => "\/Date($pstngDate)\/",
                'DocDate'   => "\/Date($docDate)\/",
            );

            $i=0;
            foreach ($data as $d) {

                $post['HeaderTxt'] = "Tfr ps.Mat.to Mat";

                $post['NV_TPITEM'][$i]['Extid']     = $d['receiving_code'];
                $post['NV_TPITEM'][$i]['Material']  = $d['item_sku'];
                $post['NV_TPITEM'][$i]['MoveMat']   = $d['item_sku'];
                $post['NV_TPITEM'][$i]['Plant']     = $d['plant_sap'];
                $post['NV_TPITEM'][$i]['StgeLoc']   = $d['storage_location_sap'];
                $post['NV_TPITEM'][$i]['MoveType']  = '309';
                $post['NV_TPITEM'][$i]['ValType']   = 'NORMAL';
                $post['NV_TPITEM'][$i]['MoveValType']   = 'REJECT';
                $post['NV_TPITEM'][$i]['EntryQnt']  = $d['qty'];
                $post['NV_TPITEM'][$i]['EntryUom']  = $d['unit_code'];
                $post['NV_TPITEM'][$i]['ItemText']  = $d['receiving_code']." - ".$d['item_sku'];
                $post['NV_TPITEM'][$i]['ItemText']  = "Tfr ps.Mat.to Mat#".$d['item_sku'];

                $i++;
            }

            $api = "stock_mvt/TPHEADERSet";

            $ref = array(
                'refCode'   => $data[0]['receiving_code']."-".$data[0]['movement_type'],
                'refId'     => $receivingId
            );

            $stagingId = $this->api_staging_model->sendToStaging(json_encode($post),$api,$ref);

            $this->api_staging_model->sync($stagingId);

        }

    }
*/

    function sendToStaging(){
        return true;
    }

    function putawayByReceiving($params=array()){

        $user = $this->db->get_where('users',['user_name'=>'will'])->row_array();
        $location = $this->db->get_where('locations',['name'=>$params['location'],'UPPER(status)'=>'ACTIVE'])->row_array();

        if($location){

            $locId = $location['id'];
            $receiving = $params['receiving'];
            $userId = $user['id'];

            $sql = "
                UPDATE
                    item_receiving_details
                SET
                    location_id=$locId,
                    putaway_time=now(),
                    user_id_putaway=$userId
                WHERE
                    item_receiving_id IN (SELECT ir.id FROM item_receiving ir JOIN receivings r ON r.id=ir.receiving_id where r.code='$receiving')
                AND
                    (putaway_time IS NULL OR (putaway_time IS NOT NULL AND location_id IS NULL))
            ";

            $update  = $this->db->query($sql);

            if($update){

                $data['status'] = true;

                $this->db->set("start_putaway", "COALESCE(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);
                $this->db->set("finish_putaway", date("Y-m-d H:i:s"));
                $this->db->where("code", $receiving);
                $this->db->update("receivings");

            }else{

                $data['status'] = false;
                $data['message'] = "Failed update data.";

            }

        }else{

            $data['status'] = false;
            $data['message'] = "Location doesn't exist.";

        }

        return $data;

    }

    function check_routing($serialNumber){
        $item_id = $this->db->select('item_id')->from('item_receiving_details')->where('unique_code', $serialNumber)->get()->row_array()['item_id'];

        $item_routing = $this->db->select('item_id, routing_id')->from('items_routing')->where('item_id', $item_id)->get()->result_array();

        if($item_routing == 0){
            return TRUE;
        }else{
            foreach ($item_routing as $key => $value) {
              if($value['routing_id'] == 1){
                $item_receiving_routing = $this->db->select('status_routing_quality')->from('items_receiving_routing')->where('unique_code', $serialNumber)->where('item_id', $item_id)->get()->row_array();
                $check_status_routing_quality = $item_receiving_routing['status_routing_quality'];
                if($check_status_routing_quality == 0){
                  return FALSE;
                  break;
                }
              }

              if($value['routing_id'] == 2){
                $item_receiving_routing = $this->db->select('status_routing_label')->from('items_receiving_routing')->where('unique_code', $serialNumber)->where('item_id', $item_id)->get()->row_array();
                if(isset($item_receiving_routing['status_routing_label'])){
                    $check_status_routing_label = $item_receiving_routing['status_routing_label'];
                }else{
                    $check_status_routing_label = 0;
                }
                if($check_status_routing_label == 0){
                  return FALSE;
                  break;
                }
              }

              if($value['routing_id'] == 3){
                $item_receiving_routing = $this->db->select('status_routing_repacking')->from('items_receiving_routing')->where('unique_code', $serialNumber)->where('item_id', $item_id)->get()->row_array();
                $check_status_routing_repacking = $item_receiving_routing['status_routing_repacking'];
                if($check_status_routing_repacking == 0){
                  return FALSE;
                  break;
                }
              }
            }
            return TRUE;
        }

    }

    function isFinish($data){

        $this->db->start_cache();

        $this->db
            ->select('r.id as id_receiving')
            ->from("item_receiving_details ird")
            ->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
            ->join("receivings r", "r.id = ir.receiving_id", "left")
            ->where('r.finish_putaway',NULL)
            ->order_by('ird.id','ASC');

        $this->db->stop_cache();

        $this->db->where('unique_code', $data['kd_unik']);

        $id_receiving   = $this->db->get();

        if($id_receiving->num_rows()>0){

            $id_receiving = $id_receiving->row_array();

        }else{

            $this->db->where('parent_code', $data['kd_unik']);
            $id_receiving = $this->db->get()->row_array();

        }

        $this->db->flush_cache();

        if($id_receiving){

            // $id_barang = $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();

            $this->db->start_cache();

            $this->db->from("item_receiving_details ird");
            $this->db->join('item_receiving ir','ir.id=ird.item_receiving_id','left');
            $this->db->join("receivings r", "r.id = ir.receiving_id", "left");
            $this->db->where("r.id", $id_receiving["id_receiving"]);

            $this->db->stop_cache();

            $this->db->select('SUM(ir.qty)');
            $qty = $this->db->get_compiled_select();
            // $this->db->group_by('ir.qty, unique_code');

            $this->db->select('SUM(ird.first_qty)');
            $this->db->where('(putaway_time IS NOT NULL or picking_id IS NOT NULL)', NULL);
            // $this->db->group_by('ird.first_qty, unique_code');
            $putted = $this->db->get_compiled_select();

            $this->db->select("r.code as kd_receiving, ($qty) AS qty, ($putted) AS putted");
            $this->db->group_by('ir.qty,ird.first_qty, unique_code, r.code');
            $res = $this->db->get();

            $this->db->flush_cache();
            $sql = "select r2.code as kd_receiving, sum(qty) as qty, sum((
                select sum(ird2.first_qty) from item_receiving_details ird2 where ird2.item_receiving_id = ir2.id 
                        )) as putted
                from item_receiving ir2
                left join receivings r2 on r2.id = ir2.receiving_id 
                where r2.id = ".$id_receiving["id_receiving"]."
                group by r2.code;
                ";
            $resData = $this->db->query($sql)->row_array();
            // if($resData['qty'] == $resData['putted']){
            //     return array('kd_receiving'=>$resData['kd_receiving']);
            // }
                // dd($resData);
            if($resData['qty'] == $resData['putted']){
                // dd('masuk sini lah km');
                return array(
                    'kd_receiving'  =>$resData['kd_receiving'],
                    'status'        =>true
                );
            }
        }

        return false;
    }

    function isFinish_backup($data){

        $this->db->start_cache();

        $this->db
            ->select('r.id as id_receiving')
            ->from("item_receiving_details ird")
            ->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
            ->join("receivings r", "r.id = ir.receiving_id", "left")
            ->where('r.finish_putaway',NULL)
            ->order_by('ird.id','ASC');

        $this->db->stop_cache();

        $this->db->where('unique_code', $data['kd_unik']);

        $id_receiving   = $this->db->get();

        if($id_receiving->num_rows()>0){

            $id_receiving = $id_receiving->row_array();

        }else{

            $this->db->where('parent_code', $data['kd_unik']);
            $id_receiving = $this->db->get()->row_array();

        }

        $this->db->flush_cache();

        if($id_receiving){

            // $id_barang = $this->get_id("id as id_barang", "code", $data["kd_barang"], "items")->row_array();

            $this->db->start_cache();

            $this->db->from("item_receiving_details ird");
            $this->db->join('item_receiving ir','ir.id=ird.item_receiving_id','left');
            $this->db->join("receivings r", "r.id = ir.receiving_id", "left");
            $this->db->where("r.id", $id_receiving["id_receiving"]);

            $this->db->stop_cache();

            $this->db->select('count(unique_code)');
            $qty = $this->db->get_compiled_select();

            $this->db->select('count(unique_code)');
            $this->db->where('(putaway_time IS NOT NULL or picking_id IS NOT NULL)', NULL);
            $putted = $this->db->get_compiled_select();

            $this->db->select("r.code as kd_receiving, ($qty) AS qty, ($putted) AS putted");
            $res = $this->db->get();

            $this->db->flush_cache();

            $resData = $res->row_array();
            if($resData['qty'] == $resData['putted']){
                return array('kd_receiving'=>$resData['kd_receiving']);
            }
        }

        return false;
    }
}

<?php

class Api_receiving_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_items(){

        $this->db->select("id, code, name");
        $this->db->from('items');
        $this->db->order_by('code','asc');

        $rec  = $this->db->get();
        return $rec->result_array();

    }
}

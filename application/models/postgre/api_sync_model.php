<?php

class Api_sync_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    public function old_sync_qty(){
        $sql = "SELECT
                    b.id_barang,
                    kd_barang,
                    count(kd_unik)as qty
                FROM
                    receiving_barang rb
                LEFT JOIN barang b ON b.id_barang = rb.id_barang
                WHERE
                    putaway_time > 0
                    AND st_shipping = 0
                GROUP BY
                    b.id_barang";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row) {
            $data_where = array('SKU'=>$row->kd_barang,'WhsCode'=>'KRG');
            $cek = $this->api_staging_model->_curl_process('get','f_v_stok_h',array('where'=>$data_where));

            if($cek->status == '001'){
                foreach ($cek->data as $dataStaging) {
                    if($dataStaging->Qty != $row->qty){
                        $data_update = array('Qty' => $row->qty,'SyncStatus' => 'N','WhsCode' => 'KRG');
                        $this->api_staging_model->_curl_process('update','f_v_stok_h',array('where'=>$data_where,'data'=>$data_update));
                    }
                }
                
            }else{
                $data_insert = array(
                    'SKU' => $row->kd_barang,
                    'Qty' => $row->qty,
                    'SyncStatus' => 'N',
                    'WhsCode' => 'KRG'
                    );
                //$this->fina->insert('f_v_stok_h',$data_insert);
                $this->api_staging_model->_curl_process('insert','f_v_stok_h',array('data'=>$data_insert));
            }
            # code...
        }
        
    }

    public function first_sync(){
        $this->load_model('api_staging_model');
        $data_where = array();
        $cek = $this->api_staging_model->_curl_process('get','f_v_stok_h',array('where'=>$data_where));
        foreach ($cek->data as $dataStaging) {
            $data_where = array(
                'WhsCode' => 'KRG',
                'SKU' => $dataStaging->SKU);
            $this->db->from('staging_stok_h');
            $this->db->where($data_where);
            $hasil = $this->db->get();
            if($hasil->num_rows()>0){
                $row = $hasil->row();
                if($dataStaging->Qty != $row->Qty){
                    $data_update = array(
                        'Qty' => $dataStaging->Qty,
                        'SyncStatus' => 'N');
                    $this->db->where($data_where);
                    $this->db->update('staging_stok_h',$data_update);
                }
            }else{
                $data_insert = array(
                    'WhsCode' => 'KRG',
                    'SKU' => $dataStaging->SKU,
                    'Qty' => $dataStaging->Qty,
                    'SyncStatus' => 'N');
                $this->db->insert('staging_stok_h',$data_insert);
            }
            # code...
        }
    }

    public function sync_qty(){
/*        $sql = "SELECT
                    b.id_barang,
                    kd_barang,
                    sum(first_qty) as qty
                FROM
                    receiving_barang rb
                LEFT JOIN barang b ON b.id_barang = rb.id_barang
                WHERE
                    putaway_time > 0
                    AND st_shipping = 0
                GROUP BY
                    b.id_barang";*/
        $this->load_model('api_staging_model');
        $sql = "SELECT
                    wms_s.kd_barang,
                    wms_s.qty
                FROM
                    (
                        SELECT
                            kd_barang,
                            COALESCE(qty,0)AS qty
                        FROM
                            barang b
                        LEFT JOIN
                            (
                                SELECT id_barang, sum(last_qty) as qty FROM receiving_barang
                                WHERE
                                    putaway_time > 0
                                AND (st_shipping = 0 or last_qty >1) 
                                AND loc_id NOT IN (101)
                                GROUP BY id_barang
                             ) rb ON rb.id_barang=b.id_barang
                    )wms_s
                LEFT JOIN staging_stok_h s ON s.SKU = wms_s.kd_barang
                UNION ALL
                    SELECT
                        s.SKU,
                        IFNULL(wms_s.qty,0) as qty
                    FROM
                        (
                            SELECT
                                kd_barang,
                                COALESCE(qty,0)AS qty
                            FROM
                                barang b
                            LEFT JOIN
                                (
                                    SELECT id_barang, sum(first_qty) as qty FROM receiving_barang
                                    WHERE
                                        putaway_time > 0
                                    AND (st_shipping = 0 or last_qty >1) 
                                    AND loc_id NOT IN (101)
                                    GROUP BY id_barang
                                 ) rb ON rb.id_barang=b.id_barang
                        )wms_s
                    RIGHT JOIN staging_stok_h s ON s.SKU = wms_s.kd_barang
                    WHERE
                        wms_s.kd_barang IS NULL";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row) {
            //$data_where = array('SKU'=>$row->kd_barang,'WhsCode'=>'KRG');

            //$cek = $this->api_staging_model->_curl_process('get','f_v_stok_h',array('where'=>$data_where));
            
            $SyncDate=date('Y-m-d H:i:s');

            $data_where = array(
                'WhsCode' => 'KRG',
                'SKU' => $row->kd_barang);
            $this->db->from('staging_stok_h');
            $this->db->where($data_where);
            $hasil = $this->db->get();
            if($hasil->num_rows()>0){
                $LocStg = $hasil->row();
                if($LocStg->Qty != $row->qty){

                    $data_update = array('Qty' => $row->qty,'SyncStatus' => 'N','WhsCode' => 'KRG', 'SyncDate' => $SyncDate);
                    $this->db->where($data_where);
                    $this->db->update('staging_stok_h',$data_update);

                    $this->api_staging_model->_curl_process('update','f_v_stok_h',array('where'=>$data_where,'data'=>$data_update));
                }
            }else{
                $data_insert = array(
                    'SKU' => $row->kd_barang,
                    'Qty' => $row->qty,
                    'SyncDate' => $SyncDate,
                    'SyncStatus' => 'N',
                    'WhsCode' => 'KRG'
                    );
                $this->db->insert('staging_stok_h',$data_insert);
                $this->api_staging_model->_curl_process('insert','f_v_stok_h',array('data'=>$data_insert));
            }
            # code...
        }
        
    }

    public function first_sync_inbound(){
        $this->load_model('api_staging_model');
        $data_where = array();
        $cek = $this->api_staging_model->_curl_process('get','f_t_inbound_l2',array('where'=>$data_where));
        foreach ($cek->data as $dataStaging) {
            $data_where = array(
                'DocEntry' => $dataStaging->DocEntry,
                'LineNum' => $dataStaging->LineNum);
            $this->db->from('staging_inbound_l2');
            $this->db->where($data_where);
            $hasil = $this->db->get();
            if($hasil->num_rows()>0){
                $data_update = $dataStaging;
                $this->db->where($data_where);
                $this->db->update('staging_inbound_l2',$data_update);
            }else{
                $data_insert = $dataStaging;
                $this->db->insert('staging_inbound_l2',$data_insert);
            }
            # code...
        }
    }

    function sync_staging_inb_l2(){
        $this->load_model('api_staging_model');
        $sql = "
            SELECT
                *
            FROM
                (
                    SELECT
                        rb.id_barang,
                        b.kd_barang,
                        rb.id_receiving,
                        r.delivery_doc,
                        sum(first_qty)AS qty,
                        tr.stqty,
                        i.id_staging,
                        i.kd_inbound,
                        FinaCode,
                        r.tanggal_receiving as rcv_date,
                        i.tanggal_inbound
                    FROM
                        receiving_barang rb
                    LEFT JOIN receiving r ON r.id_receiving = rb.id_receiving
                    LEFT JOIN barang b ON b.id_barang = rb.id_barang
                    LEFT JOIN(
                        SELECT
                            SKU,
                            sum(ReceiveQty)AS stqty,
                            RcvDoc
                        FROM
                            staging_inbound_l2
                        GROUP BY
                            SKU,
                            RcvDoc
                    )AS tr ON tr.RcvDoc = r.delivery_doc
                    AND tr.SKU = b.kd_barang
                    LEFT JOIN inbound i ON i.id_inbound = r.id_po
                    LEFT JOIN(
                        SELECT
                            id_barang,
                            id_inbound,
                            FinaCode
                        FROM
                            inbound_barang
                        GROUP BY
                            id_inbound,
                            id_barang
                    )t_ib ON t_ib.id_barang = b.id_barang
                    AND t_ib.id_inbound = i.id_inbound
                    WHERE
                        i.id_staging IS NOT NULL
                    GROUP BY
                        rb.id_barang,
                        rb.id_receiving
                    ORDER BY
                        rb.id_receiving
                )t2
            WHERE
                (stqty < qty
            OR stqty IS NULL )  ";
        $query = $this->db->query($sql);
        $Line = 200;
        foreach ($query->result() as $row) {
            $Line++;
            //================= start update page code 6542========
            //== L2 no longer update, will continue insert row
            if(!empty($row->stqty)){
                $qty = $row->qty - $row->stqty;
            }else{
                $qty = $row->qty;
            }
            $NoLine = $Line;
            $data = array(
                'DocEntry' =>$row->id_staging,
                'LineNum' =>$NoLine,
                'SKU'=>$row->kd_barang,
                'ReceiveQty'=>$qty,
                'SyncStatus'=>'N',
                'CreateDate'=>date('Y-m-d H:i:s'),
                'RcvDoc'=>$row->delivery_doc,
                'RcvDate'=>$row->rcv_date,
                'InbNo'=> $row->kd_inbound,
                'FinaCode' => $row->FinaCode
                );
            
            $result = $this->api_staging_model->_curl_process('insert','f_t_inbound_l2',array('data'=>$data));
            if(!empty($result)){
                if($result->status == '001'){
                    $this->db->insert('staging_inbound_l2',$data);
                    $data_update = array(
                        'st_staging' => 1);
                    $data_where = array(
                        'st_staging' => 0,
                        'id_barang' => $row->id_barang,
                        'id_receiving' => $row->id_receiving);
                    $this->db->where($data_where);
                    $this->db->update('receiving_barang',$data_update);
                }else{
                    var_dump($result);
                    echo '<br/>';
                }
            }else{
                echo $Line.' No Result';
                echo '<br/>';
            }

           
        }
    }

    function cek_inb_l2(){
        $this->load_model('api_staging_model');
        $sql = "
            SELECT
                *
            FROM
                (
                    SELECT
                        rb.id_barang,
                        b.kd_barang,
                        rb.id_receiving,
                        r.delivery_doc,
                        sum(first_qty)AS qty,
                        tr.stqty,
                        i.id_staging,
                        i.kd_inbound,
                        FinaCode,
                        r.tanggal_receiving as rcv_date,
                        i.tanggal_inbound
                    FROM
                        receiving_barang rb
                    LEFT JOIN receiving r ON r.id_receiving = rb.id_receiving
                    LEFT JOIN barang b ON b.id_barang = rb.id_barang
                    LEFT JOIN(
                        SELECT
                            SKU,
                            sum(ReceiveQty)AS stqty,
                            RcvDoc
                        FROM
                            staging_inbound_l2
                        GROUP BY
                            SKU,
                            RcvDoc
                    )AS tr ON tr.RcvDoc = r.delivery_doc
                    AND tr.SKU = b.kd_barang
                    LEFT JOIN inbound i ON i.id_inbound = r.id_po
                    LEFT JOIN(
                        SELECT
                            id_barang,
                            id_inbound,
                            FinaCode
                        FROM
                            inbound_barang
                        GROUP BY
                            id_inbound,
                            id_barang
                    )t_ib ON t_ib.id_barang = b.id_barang
                    AND t_ib.id_inbound = i.id_inbound
                    WHERE
                        i.id_staging IS NOT NULL
                    GROUP BY
                        rb.id_barang,
                        rb.id_receiving
                    ORDER BY
                        rb.id_receiving
                )t2
            WHERE
                (stqty < qty
            OR stqty IS NULL )  ";
        $query = $this->db->query($sql);
        $Line = 0;
        foreach ($query->result() as $row) {
            $Line++;
            echo $Line;
            echo $row->kd_barang.' - '.$row->delivery_doc.' - '.$row->qty;
            echo '<br/>';
            if(!empty($row->stqty)){
                echo $row->stqty;
            }else{
                echo 'qty staging kosong';
            }
            echo '<br/>';
            # code...
        };
    }
   

}
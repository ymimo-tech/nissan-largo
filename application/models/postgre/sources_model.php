<?php
class sources_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'sources';

    public function data($condition = array()) {

        $this->db->from($this->table  . ' a');
        $this->db->join('source_categories sc','sc.source_category_id=a.source_category_id');
        $this->db->where_condition($condition);

        return $this->db;

    }

    public function get_by_id($id) {
        $this->db->where('a.source_id',$id);
        $this->data();
        return $this->db->get();
    }

    public function get_data($condition = array()) {

        if(!empty($condition['id'])){
            $condition['source_id'] = $condition['id'];
            unset($condition['id']);
        }
        $this->data($condition);

        return $this->db->get();

    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('source_id' => $id));
    }

    public function delete($id) {

		$result = array();

        $t1 = $this->db->get_where('inbound',['supplier_id'=>$id])->row_array();

		if($t1){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('source_id' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}

        return $result;

    }

    public function importFromExcel($data){

        $this->db->trans_start();

        $dataLen = count($data);

        for ($i=0; $i < $dataLen ; $i++) {

            $this->db
                ->from('source_categories')
                ->where('lower(source_category_code)',strtolower($data[$i]['source_type']))
                ->or_where('lower(source_category_name)',strtolower($data[$i]['source_type']));

            $getSourceType = $this->db->get()->row_array();

            if($getSourceType){

                $insert= array(
                    'source_code'          => $data[$i]['source_code'],
                    'source_name'          => $data[$i]['source_name'],
                    'source_address'       => $data[$i]['source_address'],
                    'source_phone'         => $data[$i]['source_phone'],
                    'source_contact_person' => $data[$i]['source_contact_person'],
                    'source_email'          => $data[$i]['source_email'],
                    'source_city'          => $data[$i]['source_city'],
                    'source_category_id'          => $getSourceType['source_category_id']
                );

                $this->db
                    ->from($this->table)
                    ->where('LOWER(source_code)', strtolower($data[$i]['source_code']))
                    ->or_where('lower(source_name)', strtolower($data[$i]['source_name']));

                $check = $this->db->get()->row_array();

                if($check){

                    $id = $check['source_id'];
                    $this->db->where('source_id',$id);
                    $this->db->update($this->table,$insert);

                }else{

                    $this->db->insert($this->table,$insert);
                    $id = $this->db->insert_id();

                }

            }else{

                return array(
                    'status'    => false,
                    'message'   => "Source Type \"".$data[$i]['source_type']."\" doesn't exist"
                );

            }

        }

        $this->db->trans_complete();

        return array(
            'status'    => true
        );
    }

}

?>

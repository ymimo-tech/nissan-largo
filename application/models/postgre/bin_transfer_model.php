<?php
class bin_transfer_model extends CI_Model {

	public function getStatus(){

		$result = array();

		$this->db
			->select('status_id as id_status_picking, code as kd_status, name as nama_status')
			->from('status')
			->like('type', 'PICKING');

		$result = $this->db->get()->result_array();

		return $result;

	}

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'bin_transfer_id',
			1 => 'bin_transfer_code',
			2 => 'remark',
			3 => 'bin_transfer_start',
			4 => 'bin_transfer_finish',
			5 => 'status_id'
		);


		$this->db->start_cache();

		if(!empty($post['bin_transfer_doc'])){
			$this->db->where('bin_transfer_code',$post['bin_transfer_doc']);
		}

		if(!empty($post['status'])){
			$this->db->where('status_id', $post['status']);
		}

		$this->db
			->select([
				"*","st.name as status_name","COALESCE(CAST(bin_transfer_start as VARCHAR),'-') as bin_transfer_start","COALESCE(CAST(bin_transfer_finish as VARCHAR),'-') as bin_transfer_finish"
			])
			->from('bin_transfer bt')
			->join('status st','st.status_id=bt.status_id');

		$this->db->stop_cache();

		$row = $this->db->get();
		$totalData = $row->num_rows();

		$this->db
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('edit')) {

				$action .= '<li>';
				$action .= '<a class="data-table-edit" data-id="'.$row[$i]['bin_transfer_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';

            }

            if ($this->access_right->otoritas('delete')) {

                $action .= '<li>';
				$action .= '<a class="data-table-delete" data-id="'.$row[$i]['bin_transfer_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';

			}

			$action .= '</ul>';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'bin_transfer/detail/'.$row[$i]['bin_transfer_id'].'">'.$row[$i]['bin_transfer_code'].'</a>';
			$nested[] = $row[$i]['bin_transfer_remark'];
			$nested[] = $row[$i]['bin_transfer_start'];
			$nested[] = $row[$i]['bin_transfer_finish'];
			$nested[] = $row[$i]['status_name'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	function create($params=array(),$param2=array()){

		$this->db->trans_start();

		$this->db->insert('bin_transfer',$params);
		// $binTransferId = $this->db->insert_id();
		$binTransferId = $this->db->select('bin_transfer_id')->from('bin_transfer')->order_by('bin_transfer_id', 'DESC')->get()->row_array()['bin_transfer_id'];

		$this->updateBinTransferItem($param2,$binTransferId);

		$this->updateIncBinTransferDoc();

		$this->db->trans_complete();

		if($this->db->trans_status()){
			return true;
		}else{
			return false;
		}

	}

	function update($params=array(),$param2=array(),$binTransferId=""){

		$this->db->trans_start();

		$this->db
			->where('bin_transfer_id',$binTransferId)
			->update('bin_transfer',$params);

		$this->updateBinTransferItem($param2,$binTransferId);

		$this->db->trans_complete();

		if($this->db->trans_status()){
			return true;
		}else{
			return false;
		}

	}

	function updateBinTransferItem($params=array(),$binTransferId=""){

		if(!empty($binTransferId)){

			$data = array();

			for($i = 0; $i < count($params['items']); $i++){
				$data[] = array(
					'bin_transfer_id'	=> $binTransferId,
					'item_id'			=> $params['items'][$i],
					'quantity'				=> $params['qty'][$i],
					'unit_id'			=> $params['units'][$i]
				);
			}

			$this->db->delete('bin_transfer_item',['bin_transfer_id'=>$binTransferId]);
			$this->db->insert_batch('bin_transfer_item',$data);

		}

	}

	function updateIncBinTransferDoc(){

		$sql = "UPDATE m_increment SET bin_transfer_inc=bin_transfer_inc+1";
		$this->db->query($sql);

	}

	function getBinTransferItem($binTransferId=""){

		$data = array();

		if($binTransferId){

			$this->db
				->select('*, unt.id as unit_id, unt.code as unit_code')
				->from('bin_transfer_item bti')
				->join('units unt','unt.id=bti.unit_id')
				->where('bti.bin_transfer_id',$binTransferId);

			$data = $this->db->get()->result_array();
		}

		return $data;

	}

	function delete($id=""){

		if($id){

			$scannedItem = $this->db->get_where('bin_transfer_scanned',['bin_transfer_id'=>$id])->result_array();
			if(!$scannedItem){

				$this->db->trans_start();

				$this->db->where('bin_transfer_id',$id)->delete('bin_transfer_item');
				$this->db->where('bin_transfer_id',$id)->delete('bin_transfer');


				$this->db->trans_complete();

				return true;

			}

		}

		return false;

	}

	function getDetail($id=""){

		$data = array();

		if($id){

			$this->db
				->select('bt.*, wh.name as warehouse_name, wh.address as warehouse_address, st.name as status_name, usr.user_name as user_name')
				->from('bin_transfer bt')
				->join('warehouses wh','wh.id=bt.warehouse_id')
				->join('status st','st.status_id=bt.status_id')
				->join('users usr','usr.id=bt.user_id')
				->where('bt.bin_transfer_id',$id);

			$data = $this->db->get()->row_array();

		}

		return $data;

	}

	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'itm.id',
			1 => '',
			2 => 'item_code',
			3 => 'item_name',
			4 => 'quantity',
			5 => 'quantity'
		);

		$where = '';

		$this->db->start_cache();

		$this->db
			->from('bin_transfer bt')
			->join('bin_transfer_item bti','bti.bin_transfer_id=bt.bin_transfer_id')
			->join('items itm','itm.id=bti.item_id')
			->join('units unt','unt.id=bti.unit_id')
			->where('bt.bin_transfer_id',$post['bin_transfer_id']);

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->select([
				"itm.code as item_code","itm.name as item_name","bti.quantity","unt.code as unit_code","COALESCE(SUM(bts.quantity),0) as quantity_picked"
			])
			->join('bin_transfer_scanned bts','bts.bin_transfer_id=bt.bin_transfer_id and bts.item_id=itm.id','left')
			->group_by('itm.id, itm.code, itm.name, bti.quantity, unt.code')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['item_code'];
			$nested[] = $row[$i]['item_name'];
			$nested[] = $row[$i]['quantity']." ".$row[$i]['unit_code'];
			$nested[] = $row[$i]['quantity_picked']." ".$row[$i]['unit_code'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getScannedList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'unique_code',
			1 => 'unique_code',
			2 => 'parent_code',
			3 => 'quantity',
			4 => 'from_location',
			5 => 'to_location',
			6 => 'time',
			7 => 'pick_by'
		);

		$where = '';

		$this->db->start_cache();

		$this->db
			->from('bin_transfer bt')
			->join('bin_transfer_scanned bts','bts.bin_transfer_id=bt.bin_transfer_id')
			->join('item_receiving_details ird','ird.id=bts.item_receiving_detail_id')
			->join('items itm','itm.id=bts.item_id')
			->join('units unt','unt.id=bts.unit_id')
			->join('locations from_loc','from_loc.id=bts.from_location_id')
			->join('locations to_loc','to_loc.id=bts.to_location_id')
			->join('users usr','usr.id=bt.user_id')
			->where('bt.bin_transfer_id',$post['bin_transfer_id'])
			->where('itm.code',$post['item_code']);

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->select([
				"ird.unique_code","COALESCE(ird.parent_code,'-') as parent_code","COALESCE(bts.quantity,0) as quantity","unt.code as unit_code",
				"from_loc.name as from_location","to_loc.name as to_location","usr.user_name as pick_by","bts.created_at as time"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['unique_code'];
			$nested[] = $row[$i]['parent_code'];
			$nested[] = $row[$i]['quantity']." ".$row[$i]['unit_code'];
			$nested[] = $row[$i]['from_location'];
			$nested[] = $row[$i]['to_location'];
			$nested[] = $row[$i]['time'];
			$nested[] = $row[$i]['pick_by'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

}

?>

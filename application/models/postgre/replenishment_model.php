<?php
class replenishment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

	public function getListRaw($post = array()){
		$result = array();

		$where = '';

        if(!empty($post['id_barang'])){
            if($post['id_barang'] != "null"){
                $id_barang = explode(',', $post['id_barang']);
                $this->db->where_in('itm.id', $id_barang);
            }
        }

        $tput = "SELECT
                    ir.item_id,
                    lt.name as loc_type_name,
                    lt.id as loc_type_id,
                    COALESCE(sum(ird.last_qty),0)AS qty
                FROM
                    item_receiving_details ird
                LEFT JOIN item_receiving ir ON ir.id=ird.item_receiving_id
                LEFT JOIN locations loc ON loc.id = ird.location_id
                LEFT JOIN location_types lt ON lt.id = loc.location_type_id
                WHERE
                    (st_shipping = 0 OR last_qty > 1)
                AND loc.location_type_id = 1
                GROUP BY
                    ir.item_id,
                    location_type_id,
                    lt.id,
                    lt.name
                ORDER BY
                    loc.location_type_id,
                    ir.item_id";

        $tpick= "SELECT
                    ir.item_id,
                    lt.name as loc_type_name,
                    lt.id as loc_type_id,
                    COALESCE(sum(ird.last_qty),0)AS qty
                FROM
                    item_receiving_details ird
                LEFT JOIN item_receiving ir ON ir.id=ird.item_receiving_id
                LEFT JOIN locations loc ON loc.id = ird.location_id
                LEFT JOIN location_types lt ON lt.id = loc.location_type_id
                WHERE
                    (st_shipping = 0 OR last_qty > 1)
                AND(
                    loc.location_type_id = 2
                    OR loc.location_type_id = 3
                )
                GROUP BY
                    ir.item_id,
                    loc.location_type_id,
                    lt.id,
                    lt.name
                ORDER BY
                    loc.location_type_id,
                    ir.item_id";

        $this->db
            ->select([
                "itm.code as kd_barang", "itm.name as nama_barang", "minimum_stock", "COALESCE(tpick.qty,0) AS qty_picking", "COALESCE(tput.qty,0) AS qty_putaway"
            ])
            ->from('items itm')
            ->join("($tput) as tput",'tput.item_id=itm.id','left')
            ->join("($tpick) as tpick",'tpick.item_id=itm.id','left')
            ->where('COALESCE(tpick.qty,0) < itm.minimum_stock',NULL);

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'minimum_stock',
			4 => 'qty_picking',
            5 => 'qty_putaway',
            6 => '(qty_picking + qty_putaway)' //total????
		);

		$where = '';

        $this->db->start_cache();

		if(!empty($post['id_barang'])){
            $this->db->where_in('itm.id', $post['id_barang']);
        }

        if(!empty($post['warehouse'])){
            $this->db->where_in('wh.id', $post['warehouse']);
        }

        $userId= $this->session->userdata('user_id');

        $tput = "SELECT
                    ird.item_id,
                    lt.name as loc_type_name,
                    lt.id as loc_type_id,
                    COALESCE(sum(ird.last_qty),0)AS qty
                FROM
                    item_receiving_details ird
                JOIN locations loc ON loc.id = ird.location_id
                JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
                JOIN warehouses wh ON wh.id=loc_area.warehouse_id
                JOIN users_warehouses usrwh ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
                LEFT JOIN location_types lt ON lt.id = loc.location_type_id
                WHERE
                    (st_shipping = 0 OR last_qty > 1)
                AND loc.location_type_id = 1
                GROUP BY
                    ird.item_id,
                    loc.location_type_id,
                    lt.id,
                    lt.name
                ORDER BY
                    loc.location_type_id,
                    ird.item_id";

        $tpick= "SELECT
                    ird.item_id,
                    lt.name as loc_type_name,
                    lt.id as loc_type_id,
                    COALESCE(sum(ird.last_qty),0)AS qty
                FROM
                    item_receiving_details ird
                JOIN locations loc ON loc.id = ird.location_id
                JOIN location_areas loc_area ON loc_area.id=loc.location_area_id
                JOIN warehouses wh ON wh.id=loc_area.warehouse_id
                JOIN users_warehouses usrwh ON usrwh.warehouse_id=wh.id AND usrwh.user_id=$userId
                LEFT JOIN location_types lt ON lt.id = loc.location_type_id
                WHERE
                    (st_shipping = 0 OR last_qty > 1)
                AND(
                    loc.location_type_id = 2
                    OR loc.location_type_id = 3
                )
                GROUP BY
                    ird.item_id,
                    loc.location_type_id,
                    lt.id,
                    lt.name
                ORDER BY
                    loc.location_type_id,
                    ird.item_id";

        $this->db
            ->from('items itm')
            ->join('items_warehouses itmwh','itmwh.item_id=itm.id')
            ->join('warehouses wh','wh.id=itmwh.warehouse_id')
            ->join('users_warehouses uw','uw.warehouse_id=wh.id AND uw.user_id='.$this->session->userdata('user_id'))
            ->join("($tput) as tput",'tput.item_id=itm.id','left')
            ->join("($tpick) as tpick",'tpick.item_id=itm.id','left')
            ->where('COALESCE(tpick.qty,0) < itm.minimum_stock',NULL);

        $this->db->stop_cache();

        $this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

        $this->db
            ->select([
                "itm.code as kd_barang", "itm.name as nama_barang", "minimum_stock", "COALESCE(tpick.qty,0) AS qty_picking", "COALESCE(tput.qty,0) AS qty_putaway"
            ])
            ->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
            ->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['minimum_stock'];
			$nested[] = $row[$i]['qty_picking'];
			$nested[] = $row[$i]['qty_putaway'];
			$nested[] = $row[$i]['qty_picking'] + $row[$i]['qty_putaway']; //total????

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}
}

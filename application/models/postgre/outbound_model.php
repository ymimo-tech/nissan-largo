<?php
class outbound_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'outbound';
    private $table2 = "m_outbound_document";
    private $table3 = "outbound_item";
    private $table4 = "m_customer";

    public function __get_new_outbound(){
        $result = $this->fina->get_where('f_t_outbound_h',array('SyncStatus'=>'N'));
        return $result;
    }

    public function get_outbound_document($outbound_document_name=''){
        $result = $this->db->get_where('m_outbound_document',array('outbound_document_name'=>$outbound_document_name));
        return $result;
    }

    public function create_outbound($data=array()){
        $result = $this->db->insert('outbound',$data);
        return $this->db->insert_id();
    }

    public function get_outbound_item($DocEntry=0){
        $result = $this->fina->get_where('f_t_outbound_l1',array('DocEntry'=>$DocEntry));
        return $result;
    }

    public function get_barang($kd_barang=''){
        $result = $this->db->get_where('barang',array('kd_barang'=>$kd_barang));
        return $result;
    }

    public function create_outbound_barang($data=array()){
        $result = $this->db->insert('outbound_barang',$data);
        return $this->db->insert_id();
    }

    public function update_outbound_barang($data=array(),$id=0){
        $result = $this->db->update('outbound_barang',$data,array('id'=>$id));
        return $result;
    }

    public function update_outbound($data=array(),$id=0){
        $result = $this->db->update('outbound',$data,array('id_outbound'=>$id));
        return $result;
    }

    public function set_outbound_status($DocEntry=0,$SyncDate=''){
        $result = $this->fina->update('f_t_outbound_h',array('SyncStatus'=>'Y','SyncDate'=>$SyncDate),array('DocEntry'=>$DocEntry));
        return $result;
    }

    public function check_outbound($kd_outbound=''){
        $result = $this->db->get_where('outbound',array('kd_outbound'=>$kd_outbound));
        return $result;
    }

    public function check_outbound_barang($where=array()){
        $result = $this->db->get_where('outbound_barang',$where);
        return $result;
    }

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', "a.id_outbound_document=b.id_outbound_document", "left");
        $this->db->join($this->table4 . ' c', "a.id_customer=c.id_customer", "left");
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_outbound'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

	public function getOutboundPeminjaman(){
		$result = array();

		$sql = 'SELECT
					id_outbound, kd_outbound
				FROM
					outbound
				WHERE
					id_outbound_document=\'6\'';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getBatch($post = array()){
		$result = array();

		$this->db
			->select('has_batch')
			->from('items')
			->where('id', $post['id_barang']);

		$row = $this->db->get()->row_array();

		if(isset($row['has_batch'])){

			if($row['has_batch'] == 1){

				$this->db
					->select('kd_batch')
					->from('item_receiving_details ird')
					->join('item_receiving ir','ir.id=ird.item_receiving_id')
					->where('ir.item_id', $post['id_barang'])
					->group_by('kd_batch');

				$row = $this->db->get()->result_array();

				$result['is_batch'] = 'YES';
				$result['data'] = $row;

			}else{
				$result['is_batch'] = 'NO';
				$result['data'] = array();
			}

		}else{

			$result['is_batch'] = 'NO';
			$result['data'] = array();

		}

		return $result;
	}

	public function closeOutbound($post = array()){
		$result = array();

		$sql = 'UPDATE
					outbound
				SET
					status_id=4
				WHERE
					id='.$post['id_outbound'];

		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Close Outbound Document Success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Close Outbound Document Failed';
		}

		return $result;
	}

	public function export_so($params = array()){
		$where = '';

		if($params['outbound'] !== 'null'){
			$where .= ' AND o.id='.$params['outbound'];
		}

		if($params['destination']!== 'null'){
			$where .= ' AND d.destination_id='.$params['destination'];
		}
		
		if($params['doc_type'] !== 'null' && $params['doc_type'] !== ''){
			$where .= ' AND mp.m_priority_id='.$params['doc_type'];
		}

		if($params['to'] !== 'null' && $params['from'] !== 'null'){
			$where .= ' AND (o.date between\''.$params['from'].'\' and \''.$params['to'].'\')';
		}

		if($params['status'] !== 'null'){
			$where .= ' AND o.status_id='.$params['status'];
		}
		
		$sql = "select o.code, o.destination_name, o.date, mp.m_priority_name, itm.sku, itm.name, oi.qty from outbound_item oi 
		left join outbound o on oi.id_outbound = o.id
		left join m_priority mp on mp.m_priority_id = o.m_priority_id
		left join items itm on itm.id = oi.item_id
		left join destinations d on d.destination_id = o.destination_id
		where 1=1 ".$where;

		// dd($sql);

		$res1 = $this->db->query($sql)->result_array();

		return $res1;
	}

	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
			4 => 'jumlah_barang',
			5 => 'batch',
			6 => 'picked'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['id_outbound'])){
			$this->db->where('outb.id', $post['id_outbound']);
		}

		$this->db
			->from('outbound outb')
			->join('outbound_item outbitm','outbitm.id_outbound=outb.id')
			->join('items itm','itm.id=outbitm.item_id')
			->join('units unt','unt.id=itm.unit_id','left')
			->join('categories cat','cat.id=itm.category_id','left');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select([
				'outbitm.id', 'itm.code as kd_barang',
				"(CASE itm.preorder WHEN 1 THEN CONCAT(itm.name, ' - PREORDER') ELSE itm.name END) as nama_barang",
				"COALESCE(outbitm.batch, '-') as batch",
				"COALESCE(CONCAT(outbitm.qty, '', unt.name), CAST(outbitm.qty as CHAR)) as jumlah_barang",
				"CONCAT((
					SELECT
						sum(qty)
					FROM picking_recomendation pr
					JOIN picking_list_outbound plo
						ON plo.pl_id=pr.picking_id
					WHERE
						plo.id_outbound=outb.id
					AND pr.order_item_id = itm.id
				), '', unt.name) as picked"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_barang'];
			$nested[] = $row[$i]['batch'];
			$nested[] = $row[$i]['picked'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function checkStatusPicking($id){
		$result = array();

		$sql = 'SELECT
					COALESCE((
						SELECT
							SUM(qty)
						FROM
							outbound_item
						WHERE
							id_outbound=\''.$id.'\'
					), 0) AS total_document,
					(
						SELECT
							COUNT(*)
						FROM
							picking_list_outbound plo
						JOIN picking_qty pq
							ON pq.picking_id=plo.pl_id
						JOIN item_receiving_details ird
							ON ird.picking_id=pq.picking_id
						JOIN item_receiving ir
							ON ir.id=ird.item_receiving_id
								AND ir.item_id=pq.item_id
						WHERE
							plo.id_outbound=\''.$id.'\'
					) AS total_scanned';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$this->db
			->select([
				'outb.id as id_outbound','outb.code as kd_outbound',"TO_CHAR(outb.date, 'DD/MM/YYYY') as tanggal_outbound",'outb.document_id as id_outbound_document','doc.name as outbound_document_name',
/*				'spl.code as kd_supplier',
				'spl.name as nama_supplier',
				'spl.address as alamat_supplier',
				'spl.phone as telepon_supplier',
				'spl.contact_person as cp_supplier',
				'cust.name as customer_name',
				'cust.address',
*/
				'd.destination_code as kd_supplier',
				'd.destination_name as nama_supplier',
				'd.destination_address as alamat_supplier',
				'd.destination_phone as telepon_supplier',
				'd.destination_contact_person as cp_supplier',
				'outb.phone','st.name as nama_status','usr.user_name','outb.status_id as id_status_outbound','outb.destination_name as "DestinationName"','address_1 as "DestinationAddressL1"','address_2 as "DestinationAddressL2"','address_3 as "DestinationAddressL3"',"wh.name as warehouse_from","COALESCE(wh.address,'-') as warehouse_address","outb.address as outbound_address"
			])
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
/*
			->join('customers cust','cust.id=outb.customer_id AND outb.document_id NOT IN (2,3)','left outer')
			->join('suppliers spl','spl.id=outb.customer_id AND outb.document_id IN (2)','left outer')
			->join('warehouses whs','whs.id=outb.customer_id AND outb.document_id IN (3)','left outer')
*/
			->join('destinations d','d.destination_id=outb.destination_id','left')
			->join('status st','st.status_id=outb.status_id','left')
			->join('documents doc','doc.document_id=outb.document_id','left')
			->join('users usr','usr.id=outb.user_id','left')
			->where('outb.id',$post['id_outbound']);

		$result = $this->db->get()->row_array();

		return $result;
	}

    public function get_data($id) {

		$this->db
			->select([
				"outb.id as id_outbound","TO_CHAR(outb.date, 'DD/MM/YYYY') as tanggal_outbound","outb.code as kd_outbound",'destination_name as "DestinationName"',
				'address_1 as "DestinationAddressL1"','address_2 as "DestinationAddressL2"','address_3 as "DestinationAddressL3"','phone as "Phone"','note as "ShippingNote"',
				'destination_id as id_destination','outb.document_id as id_outbound_document','outb.warehouse_id','cost_center_id','outb.shipping_group','outb.address',"TO_CHAR(outb.delivery_date, 'DD/MM/YYYY') as delivery_date","m_priority_id"
			])
			->from('outbound outb')
			->join('documents doc','doc.document_id=outb.document_id','left')
			->where('outb.id', $id);

        return $this->db->get();
    }

    public function add_do_code(){
        $this->db->set('inc_do',"inc_do+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

	public function getPickingList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'priority',
			1 => 'pl_id',
			2 => 'pl_name',
			3 => 'kd_outbound',
			4 => 'sort_date',
			5 => 'destination_name',
			6 => 'remark',
			7 => 'pl_start_sort',
			8 => 'pl_finish_sort',
			9 => 'priority',
			10 => 'nama_status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['pl_id'])){
			$this->db->where('p.pl_id',$post['pl_id']);
		}

		if(!empty($post['id_outbound'])){
			$this->db->where('outb.id', $post['id_outbound']);
		}

		if(!empty($post['id_customer'])){
			$this->db->where('outb.destination_id', $post['id_customer']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('p.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). ' 00:00:00');
			$this->db->where('p.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). ' 23:59:59');
		}

		if(!empty($post['status'])){
			$this->db->where_in('p.status_id', $post['status']);
		}

		if(!empty($post['id_gate'])){
			$this->db->where('gt.id',$post['id_gate']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id', $post['warehouse']);
		}

		$this->db
			->select('DISTINCT(p.pl_id) as pl_id')
			->from('pickings p')
			->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('outbound outb','outb.id=plo.id_outbound')
			->join('destinations d','d.destination_id = outb.destination_id','left')
			->join('gate gt','gt.id = d.gate','left')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('status st','st.status_id=p.status_id','left')
			->join('m_priority prior','prior.m_priority_id=outb.m_priority_id','left')
			->group_by('p.pl_id');

		$this->db->stop_cache();

		$row = $this->db->get();
		$totalData = $row->num_rows();

		$sub_sql = "left((select itm.item_area from outbound_item oi left join items itm on itm.id = oi.item_id where oi.id_outbound = outb.id limit 1),1) as item_area";
		if(!empty($post['area'])){
			$sub_sql ="left((select itm.item_area from outbound_item oi left join items itm on itm.id = oi.item_id where oi.id_outbound = outb.id and itm.item_area like '".$post['area']."%' limit 1),1) as item_area";
		}

		$this->db
			->select([
				"COALESCE(COUNT(outb.id),0) as total",
				'p.name as pl_name',
				'COALESCE(p.printed,0) as printed',
				"string_agg(DISTINCT CAST(outb.id as VARCHAR), ',') as id_outbound",
				"string_agg(DISTINCT outb.code, ',') as kd_outbound",
				"p.date as sort_date","TO_CHAR(p.date, 'DD/MM/YYYY') as pl_date",
				"string_agg(DISTINCT outb.destination_name, ',') as destination_name",
				"COALESCE(gt.gate_name, '-') as remark",
				"p.start_time as pl_start_sort","p.end_time as pl_finish_sort",
				"COALESCE(TO_CHAR(p.start_time, 'HH24:MI<br><span class=\"small-date \">DD/MM/YYYY</span>'), '-') AS pl_start",
				"COALESCE(TO_CHAR(p.end_time, 'HH24:MI<br><span class=\"small-date\">DD/MM/YYYY<span>'), '-') AS pl_finish",
				"p.status_id as id_status_picking",
				"
				(CASE
					WHEN p.status_id = 6 THEN 1
					WHEN p.status_id = 7 THEN 2
					WHEN p.status_id = 8 THEN 3
					WHEN p.status_id = 9 THEN 4
					WHEN p.status_id = 10 THEN 5
					ELSE 6
				END) AS sequence
				",
				"st.name as nama_status",
				//"(CASE WHEN p.priority=1 THEN 'Normal' ELSE 'High' END) as priority"
				"prior.m_priority_name as priority",
				"outb.id", 
				$sub_sql
			])
			->group_by('st.name, p.name, p.printed,
				p.date, p.remark, p.start_time, p.end_time, p.status_id, prior.m_priority_name, gt.gate_name, outb.id')
			->order_by('sequence','asc')
			->order_by('priority','asc')
			->order_by('p.date','desc')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';
			if($row[$i]['pl_start'] != '-'){
				$class = 'disabled-link';
			}

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-print"></i> Print Picking List</a>';
				$action .= '</li>';

				if($class == 'disabled-link'){
					$class = '';
				}else{
					$class = 'disabled-link';
				}

				$action .= '<li>';
				$action .= '<a class="data-table-tray '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-print"></i> Print Tray List</a>';
				$action .= '</li>';
            }

			$class = '';
			/*if($row[$i]['pl_start'] != '-'){
				$class = 'disabled-link';
			}*/

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

			if($row[$i]['pl_start'] != '-'){

				$classCancel = "";

				if($row[$i]['pl_finish'] != '-'){
					$classCancel = "disabled-link";
				}

	            if ($this->access_right->otoritas('delete')) {
	                $action .= '<li>';
					$action .= '<a class="data-table-cancel '.$classCancel.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-trash"></i> Cancel</a>';
					$action .= '</li>';
	            }

			}else{

	            if ($this->access_right->otoritas('delete')) {
	                $action .= '<li>';
					$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-trash"></i> Delete</a>';
					$action .= '</li>';
	            }

	        }

	        if($this->session->userdata('username') == "largo"){

                $action .= '<li>';
				$action .= '<a class="data-table-reset" id="reset_picking" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-undo"></i> Reset Picking</a>';
				$action .= '</li>';

	        }

			if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {

				$cdisable = "";
				if($row[$i]['pl_finish'] != "-"){
					$cdisable = "disabled-link";
				}

                $action .= '<li>';
				$action .= '<a class="data-table-set-complete $cdisable" id="set-complete " data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-undo"></i> Complete Picking </a>';
				$action .= '</li>';

			}

	        if($row[$i]['kd_outbound']){

	        	$outboundCode = explode(",", $row[$i]['kd_outbound']);
	        	$outboundId = explode(",", $row[$i]['id_outbound']);

	        	if($outboundCode){
	        		$kd_outbound = "";
	        		$r = 0;
	        		foreach ($outboundCode as $oc) {
	        			$kd_outbound .= "<a href=".base_url().'outbound/detail/'.$outboundId[$r].">".$oc."</a><br/>";
	        			$r++;
	        		}
	        	}

	        	if($row[$i]['total'] > 1){
	        		$destination = 'Consolidate';
	        	}else{
	        		$destination = $row[$i]['destination_name'];
	        	}

	        }

			$printed = ($row[$i]['printed'] == 1) ? ' (printed)' : '';

			$action .= '</ul>';
			$nested = array();
			// if(isset($row[$i]['item_area'])){
				$nested[] = $row[$i]['pl_id'];
				$nested[] = '<input type="checkbox" name="id[]" value="'.$row[$i]['pl_id'].'">';
				$nested[] = $_REQUEST['start'] + ($i + 1);
				$nested[] = '<a href="'.base_url().'picking_list/detail/'.$row[$i]['pl_id'].'">'.$row[$i]['pl_name'].$printed.'</a>';
				$nested[] = $kd_outbound;
				$nested[] = $row[$i]['pl_date'];
				$nested[] = $destination;
				$nested[] = $row[$i]['remark'];
				$nested[] = $row[$i]['pl_start'];
				$nested[] = $row[$i]['pl_finish'];
				$nested[] = $row[$i]['priority'];
				$nested[] = $row[$i]['nama_status'];
	
				if($post['params'] != 'no_action')
					$nested[] = $action;

				$data[] = $nested;
			// }

		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}
	
	public function getPackingList($post) {
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'packing_number',
			1 => 'packing_number',
			2 => 'packing_number',
			3 => 'inc',
			4 => 'user_packing',
			5 => 'time',
			6 => 'outbound_doc',
			7 => 'loading_doc',
			8 => 'weight',
			9 => 'volume'
		);

		$where = '';
		$where_o = '';

		if(!empty($post['id_shipping'])){
			$where .= " AND m.manifest_number = '" . $post['id_shipping'] . "' ";
		}
		
		if(!empty($post['id_outbound'])){
			$where_o .= " AND id_outbound = ".$post['id_outbound']." ";
		}

		if(!empty($post['pccode'])){
			/*$pc_code = substr($post['pccode'], 0, 15);
			$inc = substr($post['pccode'], 15, 17);
			$where .= " AND (packing_number = '" . $pc_code . "' AND inc = " . $inc .") ";*/
			$where .= " AND ho.packing_number IN ('". implode("','",$post['pccode'])."')";
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= " AND packing_time >= '" . DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). " 00:00:00'";
			$where .= " AND packing_time <= '" . DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). " 23:59:59'";
		}

		if(!empty($post['warehouse'])){
			$this->db->join('outbound','outbound_item_receiving_details.id_outbound = outbound.id', 'left');
			$this->db->where_in('outbound.warehouse_id', $post['warehouse']);
		}

		$this->db->select([
							"packing_number"
						])
					->from("header_oird")
					->group_by("packing_number");
					
		$totalData = $this->db->get()->num_rows();
		ini_set('memory_limit','6143M'); // boost the memory limit if it's low ;)
        ini_set('max_execution_time', 4000);
		// $query = "SELECT packing_number, inc, STRING_AGG(user_packing, ', ') AS user_packing, packing_time, outbound_id, outbound_doc, loading_id, loading_doc, weight, volume, total_item_packed FROM (
		$query = "select * from (select ho.packing_number, m.id as loading_id, coalesce(m.manifest_number,'-') as loading_doc, coalesce(ho.weight,0) as weight , coalesce(ho.volume,0) as volume , usr.nama as user_packing,
		(select count(*) from outbound_item_receiving_details oird where oird.packing_number = ho.id) as total_item_packed
		,to_char(ho.time, 'DD-MM-YYYY HH24:MI:SS') as time,
		oird.id_outbound
		from header_oird ho
		left join users usr on ho.user_id = usr.id
		left join manifests m on m.id = ho.shipping_id
		join outbound_item_receiving_details oird on oird.packing_number = ho.id
		where 1=1 ".$where."
		ORDER BY
			".$columns[$requestData['order'][0]['column']]."
			".$requestData['order'][0]['dir'].",
			ho.packing_number DESC) as rb where 1=1".$where_o."
		group by packing_number, loading_id, loading_doc, weight, volume, user_packing, total_item_packed, time, id_outbound
		LIMIT ".$requestData['length']." OFFSET ".$requestData['start'];
		
		// var_dump($query);exit;
		
		$row = $this->db->query($query)->result_array();

		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if($row[$i]['loading_doc'] == '-') {
				$action .= '<li>';
				$action .= '<a class="data-table-add '.$class.'" href="'.base_url().'packing/edit/'.$row[$i]['packing_number'].'" target="_blank"><i class="fa fa-pencil"></i> Add More Items</a>';
				$action .= '</li>';
				
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" href="javascript:void(0);" onclick="" data-id='.$row[$i]['packing_number'].'><i class="fa fa-pencil"></i> Edit Dimension</a>';
				$action .= '</li>';
			}

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				// $action .= '<a class="data-table-print '.$class.'" data-packing-number="'.$row[$i]['packing_number'].'" data-outbound="'.$row[$i]['outbound_id'].'"><i class="fa fa-print"></i> Print Packing Label</a>';
				$action .= '<a class="data-table-print '.$class.'" data-packing-number="'.$row[$i]['packing_number'].'"><i class="fa fa-print"></i> Print Packing Label</a>';
				$action .= '</li>';
            }

            /*if ($this->access_right->otoritas('delete')) {

				$disabledClass = "";
				if($row[$i]['loading_doc'] != "-")
				{
					$disabledClass = "disabled-link";
				}

                $action .= '<li>';
				$action .= '<a class="data-table-cancel '.$disabledClass.'" data-id="'.$row[$i]['packing_number'].'"><i class="fa fa-trash"></i> Cancel</a>';
				$action .= '</li>';

            }*/

			$action .= '</ul>';

			$nested = array();

			// $packing_time = $row[$i]['packing_time'];

			// if($packing_time != '-'){
			// 	$packing_time = explode(' ', $packing_time);
			// 	$packing_time = substr($packing_time[1],0,8) . '<br>' . '<span class="small-date">' . $packing_time[0] . '</span>';
			// }
			
			$user = $row[$i]['user_packing'];
			$users = explode(',',$row[$i]['user_packing']);
			if(count($users) > 1) {
				$user = '';
				for($x=0;$x<count($users);$x++){
                	if($x==0){
                    	$user = $users[0].', ';
                    }
                    
					if(strpos($user, $users[$x]) === false){
                    	$user = $user . $users[$x].', ';
                    }
				}
				$user = substr($user,0,strlen($user)-2);
			}

			$nested[] = $row[$i]['packing_number'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a class="detail_packing">'.$row[$i]['packing_number'].'</a>';
			$nested[] = '<a href="'.base_url().'loading/detail/'.$row[$i]['loading_id'].'" target="_blank">'.$row[$i]['loading_doc'].'</a>';
			// $nested[] = '<a href="'.base_url().'outbound/detail/'.$row[$i]['outbound_id'].'" target="_blank">'.$row[$i]['outbound_doc'].'</a>';
			$nested[] = $row[$i]['weight'] . ' ' . WEIGHT_UOM;
			$nested[] = $row[$i]['volume'] . ' ' . VOLUME_UOM;
			// $nested[] = $row[$i]['user_packing'];
			$nested[] = $user;
			$nested[] = $row[$i]['total_item_packed'];
			$nested[] = $row[$i]['time'];
			// $nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

  public function getShippingList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'shipping_id',
			1 => 'shipping_id',
			2 => 'shipping_code',
			3 => 'picking_code',
			4 => 'sort_date',
			5 => 'customer_name',
			6 => 'driver_name',
			7 => 'license_plate',
			8 => 'shipping_start_sort',
			9 => 'shipping_finish_sort',
			10 => 'status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['id_outbound'])){
			// $this->db->where('outb.id', $post['id_outbound']);
			$where .= ' AND o.id = '. $post['id_outbound'];
		}
		
		if(!empty($post['id_shipping'])){
			// $this->db->where('shipp.shipping_id', $post['id_shipping']);
			$where .= ' AND m.id = '. $post['id_shipping'];
		}

		if(!empty($post['pl_id'])){
			// $this->db->where('p.pl_id', $post['pl_id']);
			$where .= ' AND pr.picking_id = '. $post['pl_id'];

		}

		if(!empty($post['from']) && !empty($post['to'])){
			// $this->db->where('p.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). ' 00:00:00');
			// $this->db->where('p.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). ' 23:59:59');
			$where .= ' AND m.create_at between \''. DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). ' 00:00:00\' and \''.DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). ' 23:59:59\'';
		}

		if($post['status'] != '' && $post['status'] != null){
			// $this->db->where('shipp.status_id', $post['status']);
			$where .= ' AND m.status_manifest ='.$post['status'];
		}

		if(!empty($post['warehouse'])){
			// $this->db->where_in('wh.id', $post['warehouse']);
			$where .= ' AND wh.id = '.$post['warehouse']; 

		}
		
		if(!empty($post['gi_status'])){
			// $this->db->where_in('wh.id', $post['warehouse']);
			if($post['gi_status']!=='4'){
				$where .= ' AND oird.gi_status = '.$post['gi_status']; 
			}else{
				$where .= ' AND oird.gi_status is null';
			}

		}


		// $this->db
		// ->select('DISTINCT ON (manf.manifest_number) manifest_number', FALSE)
		// 	// ->select('DISTINCT ON(shipp.shipping_id) shipping_id')
		// 	->from('shippings shipp')
		// 	->join('manifests manf', 'manf.shipping_id = shipp.shipping_id', 'left')
		// 	// ->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
		// 	// ->join('pickings p','p.pl_id=sp.pl_id')
		// 	// ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
		// 	->join('outbound outb','outb.id=shipp.outbound_id')
		// 	->join('outbound_item_receiving_details oird', 'outb.id = oird.id_outbound', 'LEFT')
		// 	->join('warehouses wh','wh.id=outb.warehouse_id')
		// 	->join('documents doc','doc.document_id=outb.document_id')
		// 	->join('users_warehouses uw','uw.warehouse_id=wh.id')
		// 	->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
		// 	// ->join('customers cust','cust.id=outb.customer_id AND outb.document_id NOT IN (2,3)','left outer')
		// 	// ->join('suppliers spl','spl.id=outb.customer_id AND outb.document_id IN (2)','left outer')
		// 	// ->join('warehouses whs','whs.id=outb.customer_id AND outb.document_id IN (3)','left outer')
 		// 	->join('destinations d','d.destination_id=outb.destination_id and doc.to_warehouse=0','left')
 		// 	->join('warehouses whs','whs.id=outb.destination_id and doc.to_warehouse=1','left')
		// 	->join('status st','st.status_id=shipp.status_id','left')
		// 	->join('shipping_delivery_order spo','spo.shipping_id=shipp.shipping_id','left')
		// 	->group_by('manf.manifest_number');


		$sql = "select m.manifest_number, string_agg(coalesce(o.code,'-'),' ,'), string_agg(coalesce(d2.destination_name,'-'),' ,'), sum(ho.weight), sum(ho.volume), count(ho.id), m.start, m.driver, m.license_plate, m.start, m.finish, m.status_manifest 
				from manifests m 
				left join manifest_oird mo on mo.manifests_id = m.id
				left join header_oird ho on ho.id = mo.packing_id and ho.shipping_id is not null
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id 
				left join picking_recomendation pr on pr.id = oird.pr_id 
				left join outbound o on o.id = pr.outbound_id
				left join destinations d2 on d2.destination_id = o.destination_id 
				group by m.manifest_number, m.start, m.driver, m.license_plate, m.finish, m.status_manifest 
				order by m.manifest_number desc";

		$this->db->stop_cache();


		$totalData = $this->db->query($sql)->num_rows();

		// $this->db
		// 	->select([
		// 		// "shipp.shipping_id","shipp.code as shipping_code","p.pl_id as picking_id","p.name as picking_code","shipp.date as sort_date"
		// 		"shipp.shipping_id","shipp.code as shipping_code","shipp.date as sort_date"
		// 		,"shipping_start as shipping_start_sort", "shipping_finish as shipping_finish_sort", "TO_CHAR(shipp.date, 'DD/MM/YYYY') AS shipping_date",
		// 		"coalesce(string_agg(d.destination_name,','),whs.name) AS customer_name", "shipp.driver as driver_name", "license_plate",
		// 		"COALESCE(TO_CHAR(shipping_start, 'DD/MM/YYYY HH24:MI'), '-') AS shipping_start",
		// 		"(SELECT oird.weight FROM outbound outb JOIN manifests man ON man.outbound_id = outb.id JOIN outbound_item_receiving_details oird ON oird.id_outbound = outb.id WHERE man.manifest_number = manf.manifest_number and oird.shipping_id is not null limit 1) as weight",
		// 		"(SELECT oird.volume FROM outbound outb JOIN manifests man ON man.outbound_id = outb.id JOIN outbound_item_receiving_details oird ON oird.id_outbound = outb.id WHERE man.manifest_number = manf.manifest_number and oird.shipping_id is not null limit 1) as volume",
		// 		"COALESCE(TO_CHAR(shipping_finish, 'DD/MM/YYYY HH24:MI'), '-') AS shipping_finish",

		// 		"shipp.st_shipping", "st.name AS status", "do_number", "manf.total_colly AS colly", "(SELECT string_agg(outb.code,'\n') FROM outbound outb JOIN manifests man ON man.outbound_id = outb.id WHERE man.manifest_number = manf.manifest_number)AS code_outbound", "(SELECT string_agg(d.destination_name,'\n') FROM outbound outb JOIN manifests man ON man.outbound_id = outb.id JOIN destinations d ON d.destination_id = outb.destination_id WHERE man.manifest_number = manf.manifest_number) AS destination_name", "outb.id as outbound_id"
		// 	])
		// 	// ->group_by('manf.manifest_number,shipp.shipping_id, p.pl_id, p.name, st.name, spo.do_number, whs.name, outb.id, manf.total_colly')
		// 	->group_by('manf.manifest_number,shipp.shipping_id, st.name, spo.do_number, whs.name, outb.id, outb.code, manf.total_colly, oird.weight, oird.volume')
		// 	// ->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
		// 	->order_by('manf.manifest_number', 'DESC')
		// 	->limit($requestData['length'],$requestData['start']);

		// 	// dd($this->db->get_compiled_select());


		$sql = "select m.id as shipping_id, m.manifest_number as manifest_number, string_agg(DISTINCT(coalesce(o.code,'-')),' ,') as code_outbound, string_agg(DISTINCT(coalesce(d2.destination_name,'-')),' ,') destination_name, (select sum(weight) from header_oird ho2 where shipping_id = m.id) as weight, (select sum(volume) from header_oird ho2 where shipping_id = m.id) as volume, count(distinct(ho.id)) colly, m.start as shipping_date, m.driver as driver_name, m.license_plate license_plate, m.start, to_char(m.create_at,'DD-MM-YYYY') as create_at, m.finish, 
				CASE
					when (m.status_manifest = 0) then 'OPEN' 
					when (m.status_manifest = 1) then 'In Progress'
					when (m.status_manifest = 2) then 'Complete'
					when (m.status_manifest = 3) then 'Cancel'
				END as status_manifest
				from manifests m 
				left join manifest_oird mo on mo.manifests_id = m.id
				left join header_oird ho on ho.id = mo.packing_id and ho.shipping_id is not null
				left join outbound_item_receiving_details oird on oird.packing_number = ho.id 
				left join picking_recomendation pr on pr.id = oird.pr_id 
				left join outbound o on o.id = pr.outbound_id
				left join warehouses w2 on o.warehouse_id = w2.id
				left join destinations d2 on d2.destination_id = o.destination_id 
				where 1=1 ".$where."
				group by m.id, m.manifest_number, m.start, m.driver, m.license_plate, m.finish, m.status_manifest, m.create_at 
				order by m.manifest_number desc
				LIMIT ".$requestData['length']." OFFSET ".$requestData['start'];
		$row = $this->db->query($sql)->result_array();
		// dd($row);
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';
			$class1 = '';
			$class2 = '';
			$class3 = 'disabled-link';
      $class_sj = '';

			if($row[$i]['status_manifest'] == '1' || $row[$i]['status_manifest'] == "Cancel"){
				$class = 'disabled-link';
			}

			if($row[$i]['status_manifest'] == '1' || $row[$i]['status_manifest'] == "In Progress" || $row[$i]['status_manifest'] == "Complete"){
				$class1 = 'disabled-link';
				$class3 = 'disabled-link';
			}

			if($row[$i]['status_manifest'] == '1' || $row[$i]['status_manifest'] == "Complete"){
				$class2 = 'disabled-link';
				$class3 = '';
			}

			if($row[$i]['finish'] == '-' || $row[$i]['status_manifest'] == "Cancel"){
				$class1 = 'disabled-link';
			}

      if($row[$i]['colly'] == 0 || $row[$i]['status_manifest'] == "Close"){
        $class_sj = 'disabled-link';
      }

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$class3.'" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['manifest_number'].'"><i class="fa fa-print"></i> Print Manifest</a>';
				$action .= '</li>';

/*				$action .= '<li>';
				$action .= '<a class="data-table-print-ol '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-print"></i> Print Outer Label</a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-print-do '.$class1.'" data-id="'.$row[$i]['shipping_id'].'" data-do="'.$row[$i]['do_number'].'"><i class="fa fa-print"></i> Print Delivery Order</a>';
				$action .= '</li>';
*/
				// $action .= '<li>';
				// $action .= '<a class="data-table-print-surat-jalan '.$class1.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-print"></i> Print Surat Jalan</a>';
				// $action .= '</li>';

				$action .= '<li>';
        $action .= '<a class="data-table-print-surat-jalan '.$class3.'" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['manifest_number'].'"><i class="fa fa-print"></i> Print Surat Jalan</a>';
				// $action .= '<a class="data-table-print-surat-jalan '.$class_sj.'" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['manifest_number'].'"><i class="fa fa-print"></i> Print Surat Jalan</a>';
				$action .= '</li>';

				// Non-Aktifkan button complete
        		$action .= '<li>';
				$action .= '<a class="data-table-complete-loading '.$class2.'" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['manifest_number'].'"><i class="fa fa-check"></i> Complete Manifest</a>';
				$action .= '</li>';

				// Aktifkan Button complete
				// $action .= '<li>';
				// $action .= '<a class="data-table-complete-loading" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['manifest_number'].'"><i class="fa fa-check"></i> Complete Manifest</a>';
				// $action .= '</li>';
/*
				$action .= '<li>';
				$action .= '<a class="data-table-print-packing-document '.$class1.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-print"></i> Print Packing Document</a>';
				$action .= '</li>';
*/
				// $action .= '<li>';
				// $action .= '<a class="data-table-print-plate '.$class.'" data-id="'.$row[$i]['shipping_id'].'" data-plate="'.$row[$i]['license_plate'].'"><i class="fa fa-print"></i> Print License Plate</a>';
				// $action .= '</li>';
            }

			if ($this->access_right->otoritas('edit')) {

				$class = 'disabled-link';
				if($row[$i]['start'] != '-' && $row[$i]['status_manifest'] != '1')
					$class = '';

				// $action .= '<li>';
				// $action .= '<a class="data-table-close '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-check"></i> Finish Loading</a>';
				// $action .= '</li>';

				$class = '';
				if($row[$i]['status_manifest'] == '1')
					$class = 'disabled-link';

				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class1.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {

				$disabledClass = "";
				if($row[$i]['status_manifest'] == "Cancel" || $row[$i]['status_manifest'] == '1')
				{
					$disabledClass = "disabled-link";
				}

                /*$action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';*/

                $action .= '<li>';
				$action .= '<a class="data-table-cancel '.$class2.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';

            }

			$action .= '<li>';
			$action .= '<a class="data-table-repost-gi" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['manifest_number'].'"><i class="fa fa-check"></i> Repost GI</a>';
			$action .= '</li>';

			$action .= '</ul>';

			$nested = array();

			$start = $row[$i]['start'];
			$finish = $row[$i]['finish'];
			// dd($start);

			if($start != '-'){
				// $start = explode(' ', $start);
				$start = $start . '<br>' . '<span class="small-date">' . $start . '</span>';
			}

			if($finish != '-'){
				// $finish = explode(' ', $finish);
				$finish = $finish . '<br>' . '<span class="small-date">' . $finish . '</span>';
			}

			$nested[] = $row[$i]['shipping_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			// $nested[] = '<a href="'.base_url().'loading/detail/'.$row[$i]['shipping_id'].'">'.$row[$i]['manifest_number'].'</a>';
			$nested[] = '<a class="detail_manifest">'.$row[$i]['manifest_number'].'</a>';
			//   $nested[] = $row[$i]['code_outbound'];
			$nested[] = $row[$i]['destination_name'];
			$nested[] = $row[$i]['weight'] . ' '.WEIGHT_UOM;
			$nested[] = $row[$i]['volume'] . ' '.VOLUME_UOM;
			$nested[] = isset($row[$i]['colly']) ? $row[$i]['colly'] : '-';
			$nested[] = $row[$i]['create_at'];
			$nested[] = $row[$i]['driver_name'];
			$nested[] = $row[$i]['license_plate'];
			$nested[] = $row[$i]['start'];
			$nested[] = $row[$i]['finish'];
			$nested[] = $row[$i]['status_manifest'];

			if($post['params'] != 'no_action')
				$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getShippingList_byDN($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'shipping_id',
			1 => 'shipping_id',
			2 => 'shipping_code',
			3 => 'picking_code',
			4 => 'sort_date',
			5 => 'customer_name',
			6 => 'driver_name',
			7 => 'license_plate',
			8 => 'shipping_start_sort',
			9 => 'shipping_finish_sort',
			10 => 'status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['id_outbound'])){
			$this->db->where('outb.id', $post['id_outbound']);
		}

		if(!empty($post['id_shipping'])){
			$this->db->where('shipp.shipping_id', $post['id_shipping']);
		}

		if(!empty($post['pl_id'])){
			$this->db->where('p.pl_id', $post['pl_id']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('p.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'). ' 00:00:00');
			$this->db->where('p.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'). ' 23:59:59');
		}

		if($post['status'] != '' && $post['status'] != null){
			$this->db->where('shipp.status_id', $post['status']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id', $post['warehouse']);

		}


		$this->db
		->select('DISTINCT ON (manf.manifest_number) manifest_number', FALSE)
			// ->select('DISTINCT ON(shipp.shipping_id) shipping_id')
			->from('shippings shipp')
			->join('manifests manf', 'manf.shipping_id = shipp.shipping_id', 'left')
			// ->join('shipping_picking sp','sp.shipping_id=shipp.shipping_id')
			// ->join('pickings p','p.pl_id=sp.pl_id')
			// ->join('picking_list_outbound plo','plo.pl_id=p.pl_id')
			->join('delivery_notes dn','dn.id=shipp.outbound_id', 'left')
			// ->join('outbound_item_receiving_details oird', 'outb.id = oird.id_outbound', 'LEFT')
      ->join('delivery_pl dpl', 'dpl.dn_id = dn.id', 'left')
      ->join('picking_recomendation pr', 'pr.id = dpl.pl_id', 'left')
      ->join('outbound outb', 'outb.id = pr.outbound_id', 'left')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('documents doc','doc.document_id=outb.document_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			// ->join('customers cust','cust.id=outb.customer_id AND outb.document_id NOT IN (2,3)','left outer')
			// ->join('suppliers spl','spl.id=outb.customer_id AND outb.document_id IN (2)','left outer')
			// ->join('warehouses whs','whs.id=outb.customer_id AND outb.document_id IN (3)','left outer')

 			->join('destinations d','d.destination_id=outb.destination_id and doc.to_warehouse=0','left')
 			->join('warehouses whs','whs.id=outb.destination_id and doc.to_warehouse=1','left')
			->join('status st','st.status_id=shipp.status_id','left')
			->join('shipping_delivery_order spo','spo.shipping_id=shipp.shipping_id','left')
			->group_by('manf.manifest_number');



		$this->db->stop_cache();


		$totalData = $this->db->get()->num_rows();

		$this->db
			->select([
				// "shipp.shipping_id","shipp.code as shipping_code","p.pl_id as picking_id","p.name as picking_code","shipp.date as sort_date"
        "shipp.shipping_id","shipp.code as shipping_code","shipp.date as sort_date"
				,"shipping_start as shipping_start_sort", "shipping_finish as shipping_finish_sort", "TO_CHAR(shipp.date, 'DD/MM/YYYY') AS shipping_date",
				"coalesce(string_agg(d.destination_name,','),whs.name) AS customer_name", "shipp.driver as driver_name", "license_plate",
				"COALESCE(TO_CHAR(shipping_start, 'DD/MM/YYYY HH24:MI'), '-') AS shipping_start",
				// "(
				// 	SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
				// 		SELECT COALESCE((SUM(qty)* CAST(itm.weight as float8)),0) as weight FROM outbound_item_picked outbitmpic JOIN items itm ON itm.id=outbitmpic.item_id WHERE outbound_id = outb.id group by itm.weight
				// 	) as tbl
				// ) as weight",
				// "(
				// 	SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
				// 		SELECT COALESCE((SUM(qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM outbound_item_picked outbitmpic JOIN items itm ON itm.id=outbitmpic.item_id WHERE outbound_id=outb.id group by itm.height, itm.width, itm.length) as tbl
				// ) as volume",
				"COALESCE(TO_CHAR(shipping_finish, 'DD/MM/YYYY HH24:MI'), '-') AS shipping_finish",

				"shipp.st_shipping", "st.name AS status", "do_number", "manf.total_colly AS colly", "string_agg(dn.name,',') AS code_outbound", "string_agg(d.destination_name,',') AS destination_name", "dn.id as delivery_notes_id"
			])
			// ->group_by('manf.manifest_number,shipp.shipping_id, p.pl_id, p.name, st.name, spo.do_number, whs.name, outb.id, manf.total_colly')
      ->group_by('manf.manifest_number,shipp.shipping_id, st.name, spo.do_number, whs.name, dn.id, dn.name, manf.total_colly')
			// ->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->order_by('manf.manifest_number', 'DESC')
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();


		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';
			$class1 = '';
			$class2 = '';
      $class_sj = '';

			if($row[$i]['st_shipping'] == '1' || $row[$i]['status'] == "Cancel"){
				$class = 'disabled-link';
			}

			if($row[$i]['shipping_finish'] == '-' || $row[$i]['status'] == "Cancel"){
				$class1 = 'disabled-link';
			}

      if($row[$i]['colly'] == 0){
        $class_sj = 'disabled-link';
      }

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$class.'" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['shipping_code'].'"><i class="fa fa-print"></i> Print Manifest</a>';
				$action .= '</li>';

/*				$action .= '<li>';
				$action .= '<a class="data-table-print-ol '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-print"></i> Print Outer Label</a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-print-do '.$class1.'" data-id="'.$row[$i]['shipping_id'].'" data-do="'.$row[$i]['do_number'].'"><i class="fa fa-print"></i> Print Delivery Order</a>';
				$action .= '</li>';
*/
				// $action .= '<li>';
				// $action .= '<a class="data-table-print-surat-jalan '.$class1.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-print"></i> Print Surat Jalan</a>';
				// $action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-print-surat-jalan '.$class_sj.'" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['shipping_code'].'"><i class="fa fa-print"></i> Print Surat Jalan</a>';
				$action .= '</li>';

        $action .= '<li>';
				$action .= '<a class="data-table-complete-loading '.$class_sj.'" data-id="'.$row[$i]['shipping_id'].'" data-name="'.$row[$i]['shipping_code'].'"><i class="fa fa-check"></i> Complete Manifest</a>';
				$action .= '</li>';
/*
				$action .= '<li>';
				$action .= '<a class="data-table-print-packing-document '.$class1.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-print"></i> Print Packing Document</a>';
				$action .= '</li>';
*/
				$action .= '<li>';
				$action .= '<a class="data-table-print-plate '.$class.'" data-id="'.$row[$i]['shipping_id'].'" data-plate="'.$row[$i]['license_plate'].'"><i class="fa fa-print"></i> Print License Plate</a>';
				$action .= '</li>';
            }

			if ($this->access_right->otoritas('edit')) {

				$class = 'disabled-link';
				if($row[$i]['shipping_start'] != '-' && $row[$i]['st_shipping'] != '1')
					$class = '';

				// $action .= '<li>';
				// $action .= '<a class="data-table-close '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-check"></i> Finish Loading</a>';
				// $action .= '</li>';

				$class = '';
				if($row[$i]['st_shipping'] == '1')
					$class = 'disabled-link';

				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {

				$disabledClass = "";
				if($row[$i]['status'] == "Cancel" || $row[$i]['st_shipping'] == '1')
				{
					$disabledClass = "disabled-link";
				}

                /*$action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';*/

                $action .= '<li>';
				$action .= '<a class="data-table-cancel '.$disabledClass.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-trash"></i> Cancel</a>';
				$action .= '</li>';

            }

			$action .= '</ul>';

			$nested = array();

			$start = $row[$i]['shipping_start'];
			$finish = $row[$i]['shipping_finish'];

			if($start != '-'){
				$start = explode(' ', $start);
				$start = $start[1] . '<br>' . '<span class="small-date">' . $start[0] . '</span>';
			}

			if($finish != '-'){
				$finish = explode(' ', $finish);
				$finish = $finish[1] . '<br>' . '<span class="small-date">' . $finish[0] . '</span>';
			}

			$nested[] = $row[$i]['shipping_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'loading/detail/'.$row[$i]['shipping_id'].'">'.$row[$i]['manifest_number'].'</a>';
      $nested[] = $row[$i]['code_outbound'];
      $nested[] = $row[$i]['destination_name'];
			$nested[] = 0 . ' '.WEIGHT_UOM;
			$nested[] = 0 . ' '.VOLUME_UOM;
			$nested[] = isset($row[$i]['colly']) ? $row[$i]['colly'] : '-';
			$nested[] = $row[$i]['shipping_date'];
			$nested[] = $row[$i]['driver_name'];
			$nested[] = $row[$i]['license_plate'];
			$nested[] = $start;
			$nested[] = $finish;
			$nested[] = $row[$i]['status'];

			if($post['params'] != 'no_action')
				$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function checkExistDoNumber($post = array()){
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					outbound
				WHERE
					kd_outbound=\''.$post['kd_outbound'].'\'';

		$this->db
			->select('count(*) as total')
			->from('outbound')
			->where('code', $post['kd_outbound']);

		if(!empty($post['id_outbound'])){
			$this->db->where('id <>', $post['id_outbound']);
		}

		$row = $this->db->get()->row_array();

		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Doc. number already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}

		return $result;
	}

	public function get_do_code($post = array()){
		$result = '';

		$sql = 'SELECT
					CONCAT(pre_code_do, \'\', inc_do) AS doc_number
				FROM
					m_increment
				WHERE
					id_inc = \'1\'';

		$row = $this->db->query($sql)->row_array();
		$result = $row['doc_number'];

		return $result;
	}

	public function cek_not_usage_do($id){
		// $sql = 'SELECT
					// id_outbound
				// FROM
					// picking_list_outbound plo
				// JOIN picking_list pl
					// ON pl.pl_id=plo.pl_id
				// WHERE
					// id_outbound=\''.$id.'\'';

		// $q = $this->db->query($sql);

        // if($q->num_rows() > 0){
            // return FALSE;
        // }else{
            // return TRUE;
        // }


		$r = $this->checkStatusPicking($id);
		if($r['total_scanned'] > 0)
			return FALSE;
		else
			return TRUE;
    }

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_outbound',
			3 => 'kd_outbound',
			4 => 'outb.date',
			5 => 'outb.delivery_date',
			6 => 'outbound_document_name',
			7 => 'outb.destination_name',
			8 => 'weight',
			9 => 'volume',
			10 => 'shipping_group',
			12 => 'nama_status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['do_number'])){
			$this->db->where('outb.id', $post['do_number']);
		}

		if(!empty($post['id_customer'])){
			$this->db->where('outb.destination_id', $post['id_customer']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('outb.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'));
			$this->db->where('outb.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'));
		}

		if(!empty($post['doc_type'])){
			$this->db->where('outb.document_id', $post['doc_type']);
		}

		if(!empty($post['priority'])){
			$this->db->where('outb.m_priority_id', $post['priority']);
		}

		if(!empty($post['doc_status'])){
			$this->db->where_in('outb.status_id', $post['doc_status']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id', $post['warehouse']);
		}

		$this->db
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('documents doc','doc.document_id=outb.document_id')
 			->join('destinations d','d.destination_id=outb.destination_id','left')
			->join('status st','st.status_id=outb.status_id','left')
			->join('m_priority mp','mp.m_priority_id=outb.m_priority_id','left');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select([
				"outb.id as id_outbound","outb.code as kd_outbound","TO_CHAR(outb.date, 'DD/MM/YYYY') as tanggal_outbound",
				"doc.name as outbound_document_name",'COALESCE(outb.destination_name, \'-\') as "DestinationName"',"outb.status_id as status_outbound",
				"(CASE
					WHEN outb.status_id = 3 THEN 1
					WHEN outb.status_id = 5 THEN 2
					WHEN outb.status_id = 4 THEN 3
					ELSE 4
				END) AS sequence",
				"(SELECT count(*) from outbound_item WHERE id_outbound=outb.id) as item",
				"COALESCE(d.destination_name,'-') as customer_name",
				"st.name as nama_status","COALESCE(outb.shipping_group,'-') as shipping_group","TO_CHAR(outb.delivery_date, 'DD/MM/YYYY') as delivery_date",
				"(
					SELECT
						COUNT(*)
					FROM
						shippings shipp
					JOIN shipping_picking sp
						ON sp.shipping_id=sp.shipping_id
					JOIN pickings pl
						ON pl.pl_id=sp.pl_id
					JOIN picking_list_outbound plo
						ON plo.pl_id=pl.pl_id
					JOIN outbound soutb
						ON soutb.id=plo.id_outbound
					WHERE
						soutb.id=outb.id
					AND
						soutb.status_id <> 4
					AND
						shipp.shipping_finish IS NOT NULL
				) as finish_loading",
				"(
					SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
						SELECT COALESCE((SUM(qty)* CAST(itm.weight as float8)),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=outb.id group by itm.weight
					) as tbl
				) as weight",
				"(
					SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
						SELECT COALESCE((SUM(qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=outb.id group by itm.height, itm.width, itm.length
					) as tbl
				) as volume",
				"m_priority_name as priority","outb.note as shipping_note"
			])
			->order_by('m_priority_name')
			->order_by('sequence')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			$class = 'disabled-link';
			$class1 = 'disabled-link';
			if($row[$i]['finish_loading'] > 0){
				$class = '';
			}

			if($row[$i]['nama_status'] == 'In Progress' || $row[$i]['nama_status'] == 'Open'){
				$class1 = '';
			}

			if($row[$i]['status_outbound'] == 16){

				if ($this->access_right->otoritas('approve')) {

					$action .= '<li>';
					$action .= '<a class="data-table-approve" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-check-square"></i> Approve</a>';
					$action .= '</li>';

					$action .= '<li>';
					$action .= '<a class="data-table-disapprove" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-close"></i> Disapprove</a>';
					$action .= '</li>';

	            }


			}else{

				$action .= '<li>';
				$action .= '<a class="data-table-close '.$class1.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-check"></i> Close Outbound</a>';
				$action .= '</li>';

				$sts = $this->checkStatusPicking($row[$i]['id_outbound']);

				$class = '';
				if($sts['total_scanned'] > 0){
					$class = 'disabled-link';
				}

				if ($this->access_right->otoritas('edit')) {
					//$action .= '<li>';
					//$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-edit"></i> Edit</a>';
					//$action .= '</li>';
	            }

	            if ($this->access_right->otoritas('delete')) {
	                $action .= '<li>';
					$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-trash"></i> Delete</a>';
					$action .= '</li>';
	            }

	        }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['id_outbound'];

			if($row[$i]['status_outbound'] == 3){
    	        $nested[] = '<input type="checkbox" name="id[]" data-volume="'.$row[$i]['volume'].'" data-weight="'.$row[$i]['weight'].'" value="'.$row[$i]['id_outbound'].'" data-status="'.$row[$i]['nama_status'].'" data-kode="'.$row[$i]['kd_outbound'].'" data-count="'.($row[$i]['item']>1?'multi':'single').'">';
	        }else{
    	        $nested[] = '<input type="checkbox" name="id[]" data-volume="'.$row[$i]['volume'].'" data-weight="'.$row[$i]['weight'].'" value="'.$row[$i]['id_outbound'].'" data-status="'.$row[$i]['nama_status'].'" data-kode="'.$row[$i]['kd_outbound'].'" data-count="'.($row[$i]['item']>1?'multi':'single').'" disabled>';
	        }

			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().$post['className'].'/detail/'.$row[$i]['id_outbound'].'">'.$row[$i]['kd_outbound'].'</a>';
			$nested[] = $row[$i]['tanggal_outbound'];
			$nested[] = $row[$i]['delivery_date'];
			$nested[] = $row[$i]['outbound_document_name'];
			$nested[] = $row[$i]['DestinationName'];
			$nested[] = $row[$i]['weight'] . ' '.WEIGHT_UOM;
			$nested[] = $row[$i]['volume'] . ' '.VOLUME_UOM;
			$nested[] = $row[$i]['priority'];
			$nested[] = $row[$i]['shipping_group'];
			$nested[] = $row[$i]['nama_status'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getList_BAK($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_outbound',
			3 => 'kd_outbound',
			4 => 'outb.date',
			5 => 'outb.delivery_date',
			6 => 'outbound_document_name',
			7 => 'destination_name',
			8 => 'weight',
			9 => 'volume',
			10 => 'shipping_group',
			11 => 'nama_status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['do_number'])){
			$this->db->where('outb.id', $post['do_number']);
		}

		if(!empty($post['id_customer'])){
			$this->db->where('outb.customer_id', $post['id_customer']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('outb.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'));
			$this->db->where('outb.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'));
		}

		if(!empty($post['doc_type'])){
			$this->db->where('outb.document_id', $post['doc_type']);
		}

		if(!empty($post['doc_status'])){
			$this->db->where_in('outb.status_id', $post['doc_status']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('wh.id', $post['warehouse']);
		}

		$this->db
			->from('outbound outb')
			->join('warehouses wh','wh.id=outb.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('documents doc','doc.document_id=outb.document_id')
 			->join('destinations d','d.destination_id=outb.destination_id','left')
			->join('status st','st.status_id=outb.status_id','left')
			->join('m_priority mp','mp.m_priority_id=outb.m_priority_id','left');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select([
				"outb.id as id_outbound","outb.code as kd_outbound","TO_CHAR(outb.date, 'DD/MM/YYYY') as tanggal_outbound",
				"doc.name as outbound_document_name",'COALESCE(outb.destination_name, \'-\') as "DestinationName"',"outb.status_id as status_outbound",
				"(SELECT count(*) from outbound_item WHERE id_outbound=outb.id) as item",
				"COALESCE(d.destination_name,'-') as customer_name",
				"st.name as nama_status","COALESCE(outb.shipping_group,'-') as shipping_group","TO_CHAR(outb.delivery_date, 'DD/MM/YYYY') as delivery_date",
				"(
					SELECT
						COUNT(*)
					FROM
						shippings shipp
					JOIN shipping_picking sp
						ON sp.shipping_id=sp.shipping_id
					JOIN pickings pl
						ON pl.pl_id=sp.pl_id
					JOIN picking_list_outbound plo
						ON plo.pl_id=pl.pl_id
					JOIN outbound soutb
						ON soutb.id=plo.id_outbound
					WHERE
						soutb.id=outb.id
					AND
						soutb.status_id <> 4
					AND
						shipp.shipping_finish IS NOT NULL
				) as finish_loading",
				"(
					SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
						SELECT COALESCE((SUM(qty)* CAST(itm.weight as float8)),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=outb.id group by itm.weight
					) as tbl
				) as weight",
				"(
					SELECT COALESCE(SUM(CAST(weight as float8)),0) as weight FROM (
						SELECT COALESCE((SUM(qty)* (CAST(itm.height as float8)* CAST(itm.width as float8)* CAST(itm.length as float8)) ),0) as weight FROM outbound_item outbitm JOIN items itm ON itm.id=outbitm.item_id WHERE id_outbound=outb.id group by itm.height, itm.width, itm.length
					) as tbl
				) as volume",
				"m_priority_name as priority"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			$class = 'disabled-link';
			if($row[$i]['finish_loading'] > 0){
				$class = '';
			}

			if($row[$i]['status_outbound'] == 16){

				if ($this->access_right->otoritas('approve')) {

					$action .= '<li>';
					$action .= '<a class="data-table-approve" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-check-square"></i> Approve</a>';
					$action .= '</li>';

					$action .= '<li>';
					$action .= '<a class="data-table-disapprove" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-close"></i> Disapprove</a>';
					$action .= '</li>';

	            }


			}else{

				$action .= '<li>';
				$action .= '<a class="data-table-close '.$class.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-check"></i> Close Outbound</a>';
				$action .= '</li>';

				$sts = $this->checkStatusPicking($row[$i]['id_outbound']);

				$class = '';
				if($sts['total_scanned'] > 0){
					$class = 'disabled-link';
				}

				if ($this->access_right->otoritas('edit')) {
					//$action .= '<li>';
					//$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-edit"></i> Edit</a>';
					//$action .= '</li>';
	            }

	            if ($this->access_right->otoritas('delete')) {
	                $action .= '<li>';
					$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-trash"></i> Delete</a>';
					$action .= '</li>';
	            }

	        }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['id_outbound'];

			if($row[$i]['status_outbound'] != 16){
    	        $nested[] = '<input type="checkbox" name="id[]" data-volume="'.$row[$i]['volume'].'" data-weight="'.$row[$i]['weight'].'" value="'.$row[$i]['id_outbound'].'" data-status="'.$row[$i]['nama_status'].'" data-kode="'.$row[$i]['kd_outbound'].'" data-count="'.($row[$i]['item']>1?'multi':'single').'">';
	        }else{
	        	$nested[] = "";
	        }
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().$post['className'].'/detail/'.$row[$i]['id_outbound'].'">'.$row[$i]['kd_outbound'].'</a>';
			$nested[] = $row[$i]['tanggal_outbound'];
			$nested[] = $row[$i]['delivery_date'];
			$nested[] = $row[$i]['outbound_document_name'];
			$nested[] = $row[$i]['DestinationName'];
			$nested[] = $row[$i]['weight'] . ' '.WEIGHT_UOM;
			$nested[] = $row[$i]['volume'] . ' '.VOLUME_UOM;
			$nested[] = $row[$i]['priority'];
			$nested[] = $row[$i]['shipping_group'];
			$nested[] = $row[$i]['nama_status'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getOutboundCode($post = array()){
		$result = array();

		$this->db
			->select('id as id_outbound, code as kd_outbound')
			->from('outbound')
			->like('code', $post['query']);

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getCustomerBySearch($post = array()){
		$result = array();

		$this->db
			->select('destination_id as id_customer, destination_name as customer_name')
			->from('destinations')
			->like('lower(destination_name)', strtolower($post['query']));

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getGateBySearch($post = array()){
		$result = array();

		$this->db
			->select(['id as id_gate', "concat(gate_code,' - ',gate_name) as gate_name"])
			->from('gate')
			->like('lower(gate_code)', strtolower($post['query']));

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getDestination($post = array()){

		$result = array();
/*
		$this->db
			->from('documents d')
			->join('m_source_type mst','mst.m_source_type_id=d.m_source_type_id')
			->where('document_id',$post['document_id']);

		$data = $this->db->get()->row_array();

		$row=array();

		if($data){

			switch ($data['m_source_type_id']) {
				case 1:
					// Get supplier
					$this->db
						->select(['id',"CONCAT(code, ' - ', name) as text"])
						->from('suppliers');

					break;

				case 2:
					// Get customer
					$this->db
						->select(['id',"CONCAT(code, ' - ', name, ' ( ', city ,' )') as text"])
						->from('customers');

					if($post['destination_id']){
						$this->db->select(['name','phone','address']);
						$this->db->where('id',$post['destination_id']);
					}

					break;

				case 3:
					// Get customer
					$this->db
						->select('id,name as text')
						->from('warehouses');

					break;

				case 4:
					// Get customer
					$this->db
						->select('destination_id as id,destination_code as text')
						->from('warehouses');

					break;

				default:
					break;
			}

			if(!$post['destination_id']){
				$row = $this->db->get()->result_array();
			}else{
				$row = $this->db->get()->row_array();
			}

		}
*/
		$this->db
			->select(["destination_id as id","CONCAT(destination_code,' - ', destination_name) as text"])
			->from('destinations d')
			->join('documents doc','doc.source_category_id=d.source_category_id')
			->where('doc.document_id',$post['document_id']);

		if($post['destination_id']){

			$this->db->select(['destination_name as name','destination_phone as phone','destination_address as address']);

			$this->db->where('destination_id',$post['destination_id']);

			$row = $this->db->get()->row_array();

		}else{

			$row = $this->db->get()->result_array();

		}

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

    public function get_outbound_barang($id_outbound) {

		$this->db
            ->select('outbitm.item_id as id_barang, outbitm.qty as jumlah_barang, unt.name as nama_satuan, outbitm.unit_id, unt.code as unit_code')
			->from('outbound_item outbitm')
			->join('items itm','itm.id=outbitm.item_id')
            ->join('units unt','unt.id=outbitm.unit_id','left')
			->where('outbitm.id_outbound', $id_outbound)
			->order_by('outbitm.id','ASC');

		return $this->db->get();

    }

    public function get_outbound_barang2($id_outbound=array()) {

		$this->db
			->select('outbitm.item_id as id_barang, outbitm.qty as jumlah_barang, unt.name as nama_satuan')
			->from('outbound_item outbitm')
			->join('items itm','itm.id=outbitm.item_id')
			->join('units unt','unt.id=itm.unit_id','left')
			->where_in('outbitm.id_outbound', $id_outbound)
			->order_by('outbitm.id','ASC');

		return $this->db->get();

    }

    function data_insert($data){

        $data = array(
        	'code'					=> $data['kd_outbound'],
        	'date'					=> $data['tanggal_outbound'],
        	'document_id'			=> $data['id_outbound_document'],
        	'user_id'				=> $data['user_id'],
        	'destination_name'		=> $data['DestinationName'],
        	'address_1'				=> $data['DestinationAddressL1'],
        	'address_2'				=> $data['DestinationAddressL2'],
        	'address_3'				=> $data['DestinationAddressL3'],
			'phone'                 => $data['Phone'],
			'note'			    	=> $data['ShippingNote'],
			'destination_id'		=> $data['id_destination'],
			'warehouse_id'			=> $data['warehouse'],
			'cost_center_id'		=> $data['cost_center_id'],
			'shipping_group'		=> $data['shipping_group'],
			'delivery_date'			=> $data['delivery_date']
     	);

        return $data;
    }

    function data2_insert($data2){

    	$arr = array();

    	foreach ($data2 as $d) {

	    	$arr[] = array(
	    		'item_id'	=> $d['id_barang'],
	    		'qty'		=> $d['jumlah_barang'],
	    		'batch'		=> $d['batch'],
                'unit_id'   => $d['unit_id']
	    	);

    	}

    	return $arr;
    }

    public function create($data, $data2) {
        $this->db->trans_start();

        $data2 = $this->data2_insert($data2);

        if($data['document_id'] == 3 || $data['document_id'] == 14){
        	$data['status_id'] = 16;
        }else{
    	    $data['status_id'] = 3;
	    }

        $this->db->insert($this->table, $data);
		$this->db->trans_complete();

        // $id_outbound = $this->db->insert_id();
		$this->db->order_by('id','desc');
		$this->db->limit(1);
		$id_outbound = $this->db->get($this->table)->row_array()['id'];

        if(!empty($data2)) {
            foreach ($data2 as &$outbound_barang) {
                $outbound_barang['id_outbound'] = $id_outbound;
            }

            $this->db->insert_batch($this->table3, $data2);
        }

        $this->db->trans_complete();
		return $this->db->trans_status();
    }

    public function create_customer($data) {
        $this->db->insert($this->table4, $data);
    }

    public function update($data, $data2, $id) {

		$this->db->trans_start();

		if(isset($data['delete'])){

			$this->db
				->select('pl_id')
				->from('picking_list_outbound')
				->where('id_outbound', $id);

			$row = $this->db->get()->result_array();
			$len = count($row);


			for($i = 0; $i < $len; $i++){

				$plId = $row[$i]['pl_id'];

				$this->db->where('pl_id',$plId);
				$this->db->delete('pickings');

				$this->db->where('picking_id',$plId);
				$this->db->delete('picking_qty');

				$this->db->where('pl_id',$plId);
				$this->db->delete('picking_list_outbound');

				$this->db->where('picking_id',$plId);
				$this->db->delete('picking_recomendation');

			}

			unset($data['delete']);

			$data['status_id'] = 3;

			$this->db->update($this->table, $data, array('id' => $id));

			//remove all outbound_barang which the id_outbound is $id
			$this->db->where("id_outbound", $id);
			$this->db->delete($this->table3);

			$data2 = $this->data2_insert($data2);

			//add all new outbound_barang
			if(!empty($data2)) {
				foreach ($data2 as &$outbound_barang) {
					$outbound_barang['id_outbound'] = $id;
				}
				$this->db->insert_batch($this->table3, $data2);
			}

		}else{

			$sql = 'SELECT
						COUNT(*) total
					FROM
						picking_list_outbound
					WHERE
						id_outbound=\''.$id.'\'';
			$this->db
				->select('count(*) as total')
				->from('picking_list_outbound')
				->where('id_outbound', $id);

			$row = $this->db->get()->row_array();

			if($row['total'] > 0){

				$result['status'] = 'ERR';
				$result['message'] = 'This outbound document already have picking document. All picking document for this outbound will
										be remove. Are you sure want to do this action ?';

				return $result;

			}else{

				$this->db->update($this->table, $data, array('id' => $id));

				//remove all outbound_barang which the id_outbound is $id
				$this->db->where("id_outbound", $id);
				$this->db->delete($this->table3);

				$data2 = $this->data2_insert($data2);

				//add all new outbound_barang
				if(!empty($data2)) {
					foreach ($data2 as &$outbound_barang) {
						$outbound_barang['id_outbound'] = $id;
					}
					$this->db->insert_batch($this->table3, $data2);
				}
			}
		}

		$this->db->trans_complete();
		return $this->db->trans_status();

    }

	public function delete($id){
		$result = array();

		$this->db
			->select('count(*) as total')
			->from('outbound outb')
			->join('picking_list_outbound plo','plo.id_outbound=outb.id')
			->join('pickings p','p.pl_id=plo.pl_id')
			->where('outb.id', $id);

		$row = $this->db->get()->row_array();
		if($row['total'] > 0 ){

			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';

		}else{

			$this->db->trans_start();

			$outbound = $this->db->get_where('outbound',array('id'=>$id))->row_array();
			if($outbound['document_id'] == 3){

				$inbound = $this->db->get_where('inbound',array('code'=>$outbound['code']))->row_array();

				$this->db->delete('inbound_item', array('inbound_id' => $inbound['id_inbound']));
				$this->db->delete('inbound', array('id_inbound' => $inbound['id_inbound']));

			}

			$this->db->delete($this->table3, array('id_outbound' => $id));
			$this->db->delete($this->table, array('id' => $id));

			$this->db->trans_complete();

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';

		}

        return $result;

	}

    public function delete1($id) {
        return $this->db->delete($this->table, array('id_outbound' => $id));
    }

    public function options($default = '--Pilih Kode Barang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->nama_barang ;
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if($table_name == 'm_outbound_document'){

				$r = $this->setting_model->getData();

				if(strtolower($dt['outbound_document_name']) == 'peminjaman' && $r['peminjaman'] == 'N')
					continue;
			}

            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    function get_barang_SKU($id_barang=0){
        $result = $this->db->get_where('barang',array('id_barang'=>$id_barang));
        return $result->num_rows()>0?$result->row()->sku:'';
    }

    // public function getOutDocument($id){
    public function getOutDocument($id, $id_document=''){

      if(empty($id_document)){

        	if(!empty($id)){

    	    	$doc = $this->db->get_where('documents',array('document_id'=>$id))->row();

    	    	$docName = "";

    	    	if($doc){

    	    		$docName .= $doc->code;
    	    		$docName .= $doc->separator;

    				switch ($doc->year_type) {
    				    case 1:
    				    	$docName .= date("y");
    				        break;
    				    case 2:
    				    	$docName .= date("Y");
    				        break;
    				    default:
    				    	$docName .= '';
    				}

    				if($doc->month_type){
    					$docName .= date("m");
    				}

    				$seqlen = $doc->seq_length - strlen($doc->seq_current);

    				for($i=0;$i<$seqlen;$i++){
    					$docName .= '0';
    				}

    				$docName .= $doc->seq_current;

    	    	}

    	    	if(empty($docName)){
    	    		return false;
    	    	}

    	    	return $docName;
    	    }
      }else {
        $docName = $this->db
            						->select('code')
            						->from('outbound')
            						->where('id', $id_document)
            						->get()
            						->row();
			  return $docName->code;
      }
  }

    public function addOutDocument($data=array()){

    	$sql = "UPDATE documents SET seq_current = seq_current + 1 WHERE document_id = ".$data['document_id'];
    	$this->db->query($sql);

    }

    public function statusOptions($default = '--Pilih Status--', $key = '') {

    	$this->db->like('type','OUTBOUND');
        $data = $this->db->get('status');
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->status_id] = $row->name ;
        }
        return $options;
    }

    public function getKittingItems($idBarang){

    	$result = false;

    	$data = $this->db->get_where('kittings',['item_id'=>$idBarang]);

    	if($data->num_rows() > 0){

    		$this->db
    			->select('ki.item_id, ki.qty')
    			->from('kittings k')
    			->join('kitting_items ki','ki.kitting_id=k.id')
    			->where('k.item_id',$idBarang);

    		$result = $this->db->get()->result();

    	}

    	return $result;

    }


    public function importFromExcel($data){

    	$outbounds = array();
    	$outbound_item = array();
    	$items = array();
    	$documents = array();
    	$destinations = array();

    	$len = count($data);

    	$this->db->trans_start();

    	for ($i=0; $i < $len ; $i++) {

    		if(!array_key_exists($data[$i]['document_name'], $outbounds)){

    			$data_outbound = $data[$i];

        		if(!array_key_exists($data[$i]['document_id'], $documents)){
            		$documents[$data[$i]['document_id']] = $this->db->get_where('documents',['code'=>$data[$i]['document_id'],'type'=>'OUTBOUND'])->row()->document_id;
        		}

        		$data_outbound['document_id'] = $documents[$data[$i]['document_id']];
        		$data_outbound['status_id']	 = 3;
        		$data_outbound['user_id']	 = $this->session->userdata('user_id');

    			unset($data_outbound['item_id']);
    			unset($data_outbound['qty']);

        		$data_outbound = [
        			'code'				=> $data[$i]['document_name'],
        			'date'				=> $data[$i]['date'],
        			'destination_name'	=> $data[$i]['destination_name'],
        			'address_1'			=> $data[$i]['address_1'],
        			'phone'				=> $data[$i]['phone'],
        			'note'				=> $data[$i]['note'],
        			'document_id' 		=> $documents[$data[$i]['document_id']],
        			'status_id'			=> 3,
        			'user_id'			=> $this->session->userdata('user_id')
        		];

        		$outbounds[$data[$i]['document_name']] = $this->outbound_model->create_outbound($data_outbound);

    		}

    		if(!array_key_exists($data[$i]['item_id'], $items)){
        		$items[$data[$i]['item_id']] = $this->db->get_where('items',['code'=>$data[$i]['item_id']])->row()->id;
    		}

    		$outbound_item[] = [
    			'id_outbound'=> $outbounds[$data[$i]['document_name']],
    			'item_id'	=> $items[$data[$i]['item_id']],
    			'qty'		=> $data[$i]['qty']
    		];

    	}

    	$this->db->insert_batch('outbound_item', $outbound_item);

    	$this->db->trans_complete();

    	return $this->db->trans_status();

    }

    public function approval($data){

    	$this->db->trans_start();

    	if($data['status'] === 'approve'){

    		$this->db->set('status_id',3);

    	}else{

    		$this->db->set('status_id',17);

    	}

    	$this->db
    		->where('id',$data['id_outbound'])
    		->update('outbound');

    	$this->db->trans_complete();

    	return $this->db->trans_status();

    }

    public function getOutboundItem($outboundIds=array()){

        $this->db
            ->select('outbitm.item_id as id_barang, outbitm.qty as jumlah_barang, unt.id as unit_id, unt.name as nama_satuan')
            ->from('outbound_item outbitm')
            ->join('units unt','unt.id=outbitm.unit_id','left')
            ->where_in('outbitm.id_outbound', $outboundIds)
            ->order_by('outbitm.id','ASC');

        return $this->db->get();

    }

    public function getDestinationByOutbound($outboundId=""){

    	$result = array();

    	if($outboundId){

    		$this->db
    			->from('outbound outb')
    			->join('destinations d','d.destination_id=outb.destination_id')
    			->where('outb.id',$outboundId);

    		$result = $this->db->get()->row_array();

    	}

    	return $result;

    }

}

?>

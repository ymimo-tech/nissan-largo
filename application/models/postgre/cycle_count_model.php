<?php
class cycle_count_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'cycle_count_h1';

    private $table2 = 'pl_do';
    private $table4 = 'do_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $do = 'do';

    public function get_cc_code(){
        $this->db->select('pre_code_cc,inc_cc');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_cc.$result->inc_cc;
    }

    public function add_cc_code(){
        $this->db->set('inc_cc',"inc_cc+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

	public function finishCc($post = array()){

		$result = array();
		$rectime = date('Y-m-d H:i:s');

		$this->db
			->select('finish as cc_finish')
			->from('cycle_counts')
			->where('cc_id', $post['cc_id']);

		$r = $this->db->get()->row_array();

		if(!empty($r['cc_finish'])){

			$result['status'] = 'OK';
			$result['message'] = 'Cycle count already finished';

		}else{

			$this->db
				->set('finish', $rectime)
				->where('cc_id', $post['cc_id']);

			$q = $this->db->update('cycle_counts');

			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Finish cycle count success';
			}else{
				$result['status'] = 'OK';
				$result['message'] = 'Finish cycle count failed';
			}

		}

		return $result;
	}

	public function checkStatus($id){
		$result = 0;

		$sql = 'SELECT
					status
				FROM
					cycle_count
				WHERE
					cc_id=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		if($row)
			$result = $row['status'];
		else
			redirect(base_url() . 'cycle_count');

		return $result;
	}

	public function setItemLog($post = array()){
		$result = array();

			$rectime = date('Y-m-d H:i:s');

			$this->db
				->select('item_id as id_barang, location_id as loc_id')
				->from('item_receiving_details')
				->where('unique_code', $post['serial_number']);

			$r = $this->db->get()->row_array();

			$this->db
				->set('unique_code', $post['serial_number'])
				->set('loc_id_old', $r['loc_id'])
				->set('user_id_pick',$this->session->userdata('user_id'))
				->set('pick_time',$rectime)
				->set('loc_id_new',104)
				->set('user_id_put',$this->session->userdata('user_id'))
				->set('put_time',$rectime)
				->set('process_name',$this->config->item('cycle_count_name'))
				->insert('transfers');

		return $result;
	}

	public function adjustment($post = array()){

		$result = array();

		$data = json_decode($post['data'], true);

		$len = count($data);
		$ccId = 0;

		$snTemp = array();

		for($i = 0; $i < $len; $i++){

			$ccId = $data[$i]['cc_id'];
			$idBarang = $data[$i]['id_barang'];
			$status = $data[$i]['status'];
/*
			$this->db->start_cache();

			$this->db
				->select('cc.code as cc_code, itm.code as item_code, cc.warehouse_id, ird.unique_code as kd_unik, ird.last_qty, ird.location_id as loc_id, ird.tgl_exp, ird.tgl_in, ccs.location_id as loc_id_cc, ccs.qc_id as id_qc_cc, ccs.exp_date as tgl_exp_cc, ccs.in_date as tgl_in_cc, ccs.unique_code as kd_unik_cc, ccs.parent_code')
				->select(array('COALESCE(SUM(ccs.qty),0) as cc_qty','COALESCE(cctq.qc_id, ird.qc_id) as id_qc'))
				->from('cycle_counts cc')
				->join('cycle_count_details ccd','ccd.cycle_count_id=cc.cc_id')
				->join('cycle_count_scanned ccs','ccs.cycle_count_id=cc.cc_id','left')
				->join('item_receiving_details ird', 'ird.unique_code=ccs.unique_code and ird.cycle_count_id=cc.cc_id','left outer')
				->join('cycle_count_temp_qc cctq','cctq.unique_code=ccs.unique_code AND cctq.cycle_count_id=cc.cc_id','left')
				->join('items itm','itm.id=ccs.item_id','left')
				// ->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
				->where('cc.cc_id', $ccId)
				->where('ccs.item_id', $idBarang)
				->group_by('cc.code, ird.unique_code, ird.last_qty, ird.location_id, ird.tgl_exp, ird.tgl_in, ccs.location_id, ccs.qc_id, ccs.exp_date, ccs.in_date, ccs.unique_code, cctq.qc_id, ird.qc_id, itm.code, cc.warehouse_id, ccs.parent_code,');

			$this->db->stop_cache();
*/

			$this->db
				->select('cc.code as cc_code, itm.code as item_code, cc.warehouse_id, ird.unique_code as kd_unik, ird.last_qty, ird.location_id as loc_id, ird.tgl_exp, ird.tgl_in, ccs.location_id as loc_id_cc, ccs.qc_id as id_qc_cc, ccs.exp_date as tgl_exp_cc, ccs.in_date as tgl_in_cc, ccs.unique_code as kd_unik_cc, ccs.parent_code')
				->select(array('COALESCE(SUM(ccs.qty),0) as cc_qty','COALESCE(cctq.qc_id, ird.qc_id) as id_qc'))
				->from('cycle_counts cc')
				->join('cycle_count_details ccd','ccd.cycle_count_id=cc.cc_id')
				->join('cycle_count_scanned ccs','ccs.cycle_count_id=cc.cc_id','left')
				->join('item_receiving_details ird', 'ird.unique_code=ccs.unique_code and ird.cycle_count_id=cc.cc_id','left')
				->join('cycle_count_temp_qc cctq','cctq.unique_code=ccs.unique_code AND cctq.cycle_count_id=cc.cc_id','left')
				->join('items itm','itm.id=ccs.item_id','left')
				// ->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
				->where('cc.cc_id', $ccId)
				->where('ccs.item_id', $idBarang)
				->group_by('cc.code, ird.unique_code, ird.last_qty, ird.location_id, ird.tgl_exp, ird.tgl_in, ccs.location_id, ccs.qc_id, ccs.exp_date, ccs.in_date, ccs.unique_code, cctq.qc_id, ird.qc_id, itm.code, cc.warehouse_id, ccs.parent_code,');

			$sql1 = $this->db->get_compiled_select();

			$this->db
				->select('cc.code as cc_code, itm.code as item_code, cc.warehouse_id, ird.unique_code as kd_unik, ird.last_qty, ird.location_id as loc_id, ird.tgl_exp, ird.tgl_in, ccs.location_id as loc_id_cc, ccs.qc_id as id_qc_cc, ccs.exp_date as tgl_exp_cc, ccs.in_date as tgl_in_cc, ccs.unique_code as kd_unik_cc, ccs.parent_code')
				->select(array('COALESCE(SUM(ccs.qty),0) as cc_qty','COALESCE(cctq.qc_id, ird.qc_id) as id_qc'))
				->from('cycle_counts cc')
				->join('cycle_count_details ccd','ccd.cycle_count_id=cc.cc_id')
				->join('item_receiving_details ird', 'ird.cycle_count_id=cc.cc_id','left')
				->join('cycle_count_scanned ccs','ird.unique_code=ccs.unique_code and ccs.cycle_count_id=cc.cc_id','left')
				->join('cycle_count_temp_qc cctq','cctq.unique_code=ird.unique_code AND cctq.cycle_count_id=cc.cc_id','left')
				->join('items itm','itm.id=ird.item_id','left')
				// ->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
				->where('cc.cc_id', $ccId)
				->where('ird.item_id', $idBarang)
				->group_by('cc.code, ird.unique_code, ird.last_qty, ird.location_id, ird.tgl_exp, ird.tgl_in, ccs.location_id, ccs.qc_id, ccs.exp_date, ccs.in_date, ccs.unique_code, cctq.qc_id, ird.qc_id, itm.code, cc.warehouse_id, ccs.parent_code,');

			$sql2 = $this->db->get_compiled_select();

			$sql = "SELECT * FROM (($sql1)UNION($sql2)) as tbl";

			$itm = $this->db->query($sql);
			$items = $itm->result_array();

			$inbData = $this->db->query($sql." WHERE kd_unik IS NULL")->result_array();

			$fisikNotFound = $this->db->query($sql." WHERE kd_unik_cc IS NULL")->result_array();

			$this->db->flush_cache();

			if($status != 'ADJUST TO SYSTEM QTY'){

				if($fisikNotFound){

					$warehouse = $this->db->get_where('warehouses',['id'=>$fisikNotFound[0]['warehouse_id']])->row_array();

					$this->db
						->select('loc.id as id')
						->from('locations loc')
						->join('location_areas loc_area','loc_area.id=loc.location_area_id')
						->join('warehouses wh','wh.id=loc_area.warehouse_id')
						->where('wh.id',$warehouse['id'])
						->like('loc.name','MIA');

					$miaLocation = $this->db->get()->row_array();

					if($miaLocation){

						$miaLocationId = $miaLocation['id'];

					}else{

						$this->db
							->select('loc_area.id as id')
							->from('location_areas loc_area')
							->join('warehouses wh','wh.id=loc_area.warehouse_id')
							->where('wh.id',$warehouse['id'])
							->like('loc_area.name','MIA');

						$miaLocationArea = $this->db->get()->row_array();

						if($miaLocationArea){

							$miaLocationAreaId = $miaLocationArea['id'];

						}else{

							$this->db
								->set('name',$warehouse['name'].' - MIA')
								->set('warehouse_id',$warehouse['id'])
								->insert('location_areas');

							$miaLocationAreaId = $this->db->insert_id();

						}

						$this->db
							->set('name',$warehouse['name'].' - MIA')
							->set('location_area_id',$miaLocationAreaId)
							->set('location_type_id',1)
							->set('location_category_id',4)
							->insert('locations');

						$miaLocationId = $this->db->insert_id();

					}

				}

				if($inbData){

					$checkInb = $this->db->get_where('inbound',['code'=>$inbData[0]['cc_code']])->row_array();
					if(!$checkInb){

		                $dataInbound = array(
		                    'code' 				=> $inbData[0]['cc_code'],
		                    'date'				=> date('Y-m-d'),
							'user_id'			=> $this->session->userdata('user_id'),
							'warehouse_id'		=> $inbData[0]['warehouse_id'],
							'status_id'			=> 4
		             	);

		             	$this->db->insert('inbound',$dataInbound);

		             	$inbId['id_inbound'] = $this->db->insert_id();

		             	$dataReceiving = array(
						                    'code' 			=> $inbData[0]['cc_code'],
						                    'po_id' 		=> $inbId['id_inbound'],
											'user_id'		=> $this->session->userdata('user_id'),
											'start_tally'	=> date('Y-m-d h:i:s'),
											'finish_tally'	=> date('Y-m-d h:i:s'),
											'start_putaway'	=> date('Y-m-d h:i:s'),
											'finish_putaway'=> date('Y-m-d h:i:s')
						                );

		             	$this->db->insert('receivings',$dataReceiving);

		             	$rcvId['id'] = $this->db->insert_id();

					}else{

						$inbId = $this->db->get_where('inbound',['code'=>$inbData[0]['cc_code']])->row_array();;
						$rcvId = $this->db->get_where('receivings',['code'=>$inbData[0]['cc_code']])->row_array();;

					}


					$inbItem = $this->db->get_where('inbound_item',['inbound_id'=>$inbId['id_inbound'],'item_id'=>$idBarang])->row_array();
					if(!$inbItem){
	
						$inbItem = array(
							'inbound_id'	=> $inbId['id_inbound'],
							'item_id'		=> $idBarang,
							'qty'			=> count($inbData)
						);

						$this->db->insert('inbound_item',$inbItem);

					}

					$rcvItem = $this->db->get_where('item_receiving',['receiving_id'=>$rcvId['id'],'item_id'=>$idBarang])->row_array();
					if(!$rcvItem){
						$rcvItem = array(
							'receiving_id'	=> $rcvId['id'],
							'item_id'		=> $idBarang,
							'qty'			=> count($inbData)
						);

						$this->db->insert('item_receiving',$rcvItem);
					}

				}

			}

			$this->db->trans_start();

			if($itm->num_rows() > 0){

				$dItem = array();

				$this->db
					->select('ir.id')
					->from('item_receiving ir')
					->join('receivings r','r.id=ir.receiving_id')
					->where('r.code',$items[0]['cc_code'])
					->where('ir.item_id',$idBarang);

				$itemReceivingId = $this->db->get()->row_array();

				$date = date('Y-m-d');
				$dateTime = date('Y-m-d h:i:s');

				foreach ($items as $item) {

					$loc_id = ($item['loc_id_cc'] == NULL) ? $item['loc_id'] : $item['loc_id_cc'];
					$id_qc = ($item['id_qc_cc'] == NULL) ? $item['id_qc'] : $item['id_qc_cc'];
					$kd_unik = ($item['kd_unik_cc'] == NULL) ? $item['kd_unik'] : $item['kd_unik_cc'];
					$tgl_in = ($item['tgl_in_cc'] == NULL) ? $item['tgl_in'] : $item['tgl_in_cc'];
					$tgl_exp = ($item['tgl_exp_cc'] == NULL) ? $item['tgl_exp'] : $item['tgl_exp_cc'];
 					$last_qty = $item['last_qty'];
					$cc_qty = $item['cc_qty'];

					$dItem[] = array(
						'cycle_count_id'		=> $ccId,
						'item_id' => $idBarang,
						'location_id'	=> $loc_id,
						'qc_id'		=> $id_qc,
						'unique_code'	=> $kd_unik,
						'in_date'	=> $tgl_in,
						'exp_date'	=> $tgl_exp,
						'status'	=> $status,
						'system_qty'  => $last_qty,
						'scanned_qty' => $cc_qty,
						'user_id'	=> $this->session->userdata('user_id')
					);

					if($status != 'ADJUST TO SYSTEM QTY'){

						if(empty($item['kd_unik'])){

							$insData	= array(
								'parent_code'		=> $item['parent_code'],
								'item_receiving_id'	=> $itemReceivingId['id'],
								'item_id'			=> $idBarang,
								'location_id'		=> $item['loc_id_cc'],
								'tgl_exp'			=> NULL,
								'tgl_in'			=> $date,
								'st_receive'		=> 1,
								'user_id_receiving'	=> $this->session->userdata('user_id'),
								'first_qty'			=> 1,
								'last_qty'			=> 1,
								'qc_id'				=> 1,
								'unique_code'		=> $kd_unik,
								'putaway_time'		=> date('Y-m-d h:i:s')
							);

					        $this->db->insert("item_receiving_details", $insData);
        
					        $dataTransfer =   array(
	                            "loc_id_new"    => $item['loc_id_cc'],
	                            "loc_id_old"    => $item['loc_id_cc'],
	                            "pick_time"     => $dateTime,
	                            "put_time"      => $dateTime,
	                            "user_id_pick"  => $this->session->userdata('user_id'),
	                            "user_id_put"   => $this->session->userdata('user_id'),
	                            "process_name"  => "RECEIVING"
	                        );

					        $this->db->insert("transfers", $dataTransfer);

						}

					}

					if($status == 'ADJUST TO SYSTEM QTY'){

						$this->db->set('last_qty', $item['last_qty']);
						$this->db->set('location_id', $item['loc_id']);

					}else{

						if($item['cc_qty'] > 0){

/*
							if($item['loc_id'] == 101 || $item['loc_id'] == 104){

								$this->db->set('location_id', 100);

							}else{

								$this->db->set('location_id', $item['loc_id_cc']);

							}
*/

							$this->db->set('last_qty', $item['cc_qty']);
							$this->db->set('location_id', $item['loc_id_cc']);
							$this->db->set('putaway_time', "COALESCE(putaway_time,'".$dateTime."')", FALSE);

						}else{

/*							$this->db->set('last_qty', 0);
							$this->db->set('location_id', 104);
*/
							$this->db->set('location_id', $miaLocationId);

						}

					}


					$this->db->set('qc_id',$item['id_qc']);
					$this->db->where("cycle_count_id=$ccId and unique_code='".$item['kd_unik']."'",NULL);
	
					if($itemReceivingId){
						$this->db->or_where("item_receiving_id=".$itemReceivingId['id']." and unique_code='".$item['kd_unik']."'");
					}

					$this->db->update('item_receiving_details');

					// $p = array(
					// 	'params'	=> $status,
					// 	'serial_number'	=> $kd_unik
					// );

					// $this->setItemLog($p);

				}

				$this->db->insert_batch('cycle_count_results',$dItem);

			}

		}

		$this->db
			->set('status_id', 4)
			->where('cc_id', $ccId)
			->update('cycle_counts');

		$this->db->trans_complete();
		if($this->db->trans_status()){

			$result['status'] = 'OK';
			$result['message'] = 'Adjustment proccess success';
			$result['sn'] = $snTemp;

	        // $this->load_model('api_sync_model');
	        // $this->api_sync_model->sync_qty();

		}else{

			$result['status'] = 'ERR';
			$result['message'] = 'Adjustment proccess error';
			$result['sn'] = $snTemp;

		}

		return $result;

	}
	
	public function checkIsFinish($id){
		$result = array();

		$sql = 'SELECT
					cc_finish
				FROM
					cycle_count
				WHERE
					cc_id=\''.$id.'\'';

		$this->db
			->select('finish')
			->from('cycle_counts')
			->where('cc_id', $id);

		$row = $this->db->get()->row_array();

		if(empty($row['finish'])){
			$result = 'false';
		}else{
			$result = 'true';
		}

		return $result;
	}

	public function checkIsUsed($id){
		$result = array();

		$this->db
			->select('count(*) as total')
			->from('cycle_count_scanned')
			->where('cycle_count_id', $id);

		$row = $this->db->get()->row_array();

		if($row['total'] > 0){
			$result['is_used'] = true;
		}else{
			$result['is_used'] = false;
		}

		return $result;
	}

	public function getDetailCheck($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'cc_id',
			2 => 'loc_id',
			3 => 'id_barang',
			4 => 'id_qc',
			5 => 'tgl_in',
			6 => 'tgl_exp',
			7 => '',
			8 => 'loc_name',
			9 => 'nama_barang',
			10 => 'kd_unik',
			11 => 'status',
			12 => 'system_qty',
			13 => 'scanned_qty'
		);

		if($post['cc_type'] == 'SN'){

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
					(
						SELECT
							COUNT(*)
						FROM
						(
							(
								SELECT
									cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
									cctq.id_qc, rcvbrg.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
									rcvbrg.tgl_in, rcvbrg.tgl_exp
								FROM
									cycle_count cc1
								JOIN cycle_count_h2 cc2
									ON cc1.cc_id=cc2.cc_id_h1
								JOIN receiving_barang rcvbrg
									ON rcvbrg.id_barang=cc2.item_id
								JOIN barang brg
									ON brg.id_barang=cc2.item_id
								JOIN m_loc loc
									ON loc.loc_id=rcvbrg.loc_id
								LEFT JOIN cycle_count_temp_qc cctq
									ON cctq.kd_unik=rcvbrg.kd_unik
										AND cctq.cc_id=cc1.cc_id
								LEFT JOIN cycle_count_detail ccd
									ON ccd.kd_unik=rcvbrg.kd_unik
										AND ccd.cc_id=cc1.cc_id
								LEFT JOIN satuan sat
									ON sat.id_satuan=brg.id_satuan
								WHERE
									cc1.cc_id=\''.$post['cc_id'].'\'
								AND
									loc.loc_type NOT IN('.$this->config->item('cc_location').')
							)
							UNION
							(
								SELECT
									cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
									ccd.id_qc, ccd.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
									ccd.tgl_in, ccd.tgl_exp
								FROM
									cycle_count cc1
								JOIN cycle_count_h2 cc2
									ON cc1.cc_id=cc2.cc_id_h1
								JOIN cycle_count_detail ccd
									ON ccd.cc_id=cc1.cc_id
								LEFT JOIN receiving_barang rcvbrg
									ON rcvbrg.kd_unik=ccd.kd_unik
										AND rcvbrg.id_barang=ccd.id_barang
								LEFT JOIN barang brg
									ON brg.id_barang=ccd.id_barang
								LEFT JOIN m_loc loc
									ON loc.loc_id=ccd.loc_id
								LEFT JOIN satuan sat
									ON sat.id_satuan=brg.id_satuan
								WHERE
									cc1.cc_id=\''.$post['cc_id'].'\'
								AND
									loc.loc_type NOT IN('.$this->config->item('cc_location').')
							)
						) AS tbl
						GROUP BY
							kd_unik
					) AS tbl2';

			$row = $this->db->query($sql)->row_array();
			$totalData = $row['total'];

			$sql = 'SELECT
						cc_id, cc_finish, loc_id, id_barang, loc_name, kd_barang, nama_barang, id_qc, kd_unik, status, cc_qty,
						(
							CASE
								WHEN
									id_receiving_barang IS NULL
								AND
									cc_detail_id IS NOT NULL
								THEN
									\'NO\'
								ELSE
								\'YES\'
							END
						) AS system_qty,
						(
							CASE
								WHEN
									id_receiving_barang IS NOT NULL
								AND
									cc_detail_id IS NULL
								THEN
									\'NO\'
								ELSE
									\'YES\'
							END
						) AS scanned_qty,
						tgl_in, tgl_exp
					FROM
					(
						(
							SELECT
								cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
								cctq.id_qc, rcvbrg.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
								rcvbrg.tgl_in, rcvbrg.tgl_exp, ccd.cc_qty
							FROM
								cycle_count cc1
							JOIN cycle_count_h2 cc2
								ON cc1.cc_id=cc2.cc_id_h1
							JOIN receiving_barang rcvbrg
								ON rcvbrg.id_barang=cc2.item_id
							JOIN barang brg
								ON brg.id_barang=cc2.item_id
							JOIN m_loc loc
								ON loc.loc_id=rcvbrg.loc_id
							LEFT JOIN cycle_count_temp_qc cctq
								ON cctq.kd_unik=rcvbrg.kd_unik
									AND cctq.cc_id=cc1.cc_id
							LEFT JOIN cycle_count_detail ccd
								ON ccd.kd_unik=rcvbrg.kd_unik
									AND ccd.cc_id=cc1.cc_id
							LEFT JOIN satuan sat
								ON sat.id_satuan=brg.id_satuan
							WHERE
								cc1.cc_id=\''.$post['cc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						)
						UNION
						(
							SELECT
								cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
								ccd.id_qc, ccd.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
								ccd.tgl_in, ccd.tgl_exp, ccd.cc_qty
							FROM
								cycle_count cc1
							JOIN cycle_count_h2 cc2
								ON cc1.cc_id=cc2.cc_id_h1
							JOIN cycle_count_detail ccd
								ON ccd.cc_id=cc1.cc_id
							LEFT JOIN receiving_barang rcvbrg
								ON rcvbrg.kd_unik=ccd.kd_unik
									AND rcvbrg.id_barang=ccd.id_barang
							LEFT JOIN barang brg
								ON brg.id_barang=ccd.id_barang
							LEFT JOIN m_loc loc
								ON loc.loc_id=ccd.loc_id
							LEFT JOIN satuan sat
								ON sat.id_satuan=brg.id_satuan
							WHERE
								cc1.cc_id=\''.$post['cc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						)
					) AS tbl
					GROUP BY
						kd_unik';

		}else{

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
					(
						SELECT
							COUNT(*) AS total
						FROM
						(
							(
								SELECT
									cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
									cctq.id_qc, rcvbrg.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
									rcvbrg.tgl_in, rcvbrg.tgl_exp
								FROM
									cycle_count cc1
								JOIN cycle_count_h2 cc2
									ON cc1.cc_id=cc2.cc_id_h1
								JOIN receiving_barang rcvbrg
									ON rcvbrg.loc_id=cc2.loc_id
								JOIN barang brg
									ON brg.id_barang=rcvbrg.id_barang
								JOIN m_loc loc
									ON loc.loc_id=rcvbrg.loc_id
								LEFT JOIN cycle_count_detail ccd
									ON ccd.kd_unik=rcvbrg.kd_unik
										AND ccd.cc_id=cc1.cc_id
								LEFT JOIN cycle_count_temp_qc cctq
									ON cctq.kd_unik=rcvbrg.kd_unik
										AND cctq.cc_id=cc1.cc_id
								LEFT JOIN satuan sat
									ON sat.id_satuan=brg.id_satuan
								WHERE
									cc1.cc_id=\''.$post['cc_id'].'\'
								AND
									loc.loc_type NOT IN('.$this->config->item('cc_location').')
							)
							UNION
							(
								SELECT
									cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
									ccd.id_qc, ccd.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
									ccd.tgl_in, ccd.tgl_exp
								FROM
									cycle_count cc1
								JOIN cycle_count_h2 cc2
									ON cc1.cc_id=cc2.cc_id_h1
								JOIN cycle_count_detail ccd
									ON ccd.cc_id=cc1.cc_id
								LEFT JOIN receiving_barang rcvbrg
									ON rcvbrg.kd_unik=ccd.kd_unik
								LEFT JOIN barang brg
									ON brg.id_barang=ccd.id_barang
								LEFT JOIN m_loc loc
									ON loc.loc_id=ccd.loc_id
								LEFT JOIN satuan sat
									ON sat.id_satuan=brg.id_satuan
								WHERE
									cc1.cc_id=\''.$post['cc_id'].'\'
								AND
									loc.loc_type NOT IN('.$this->config->item('cc_location').')
							)
						) AS tbl
						GROUP BY
							kd_unik
					) AS tbl2';

			$row = $this->db->query($sql)->row_array();
			$totalData = $row['total'];

			$sql = 'SELECT
						cc_id, cc_finish, loc_id, id_barang, loc_name, kd_barang, nama_barang, id_qc, kd_unik, status, cc_qty,
						(
							CASE
								WHEN
									id_receiving_barang IS NULL
								AND
									cc_detail_id IS NOT NULL
								THEN
									\'NO\'
								ELSE
								\'YES\'
							END
						) AS system_qty,
						(
							CASE
								WHEN
									id_receiving_barang IS NOT NULL
								AND
									cc_detail_id IS NULL
								THEN
									\'NO\'
								ELSE
									\'YES\'
							END
						) AS scanned_qty,
						tgl_in, tgl_exp
					FROM
					(
						(
							SELECT
								cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
								cctq.id_qc, rcvbrg.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
								rcvbrg.tgl_in, rcvbrg.tgl_exp, ccd.cc_qty
							FROM
								cycle_count cc1
							JOIN cycle_count_h2 cc2
								ON cc1.cc_id=cc2.cc_id_h1
							JOIN receiving_barang rcvbrg
								ON rcvbrg.loc_id=cc2.loc_id
							JOIN barang brg
								ON brg.id_barang=rcvbrg.id_barang
							JOIN m_loc loc
								ON loc.loc_id=rcvbrg.loc_id
							LEFT JOIN cycle_count_detail ccd
								ON ccd.kd_unik=rcvbrg.kd_unik
									AND ccd.cc_id=cc1.cc_id
							LEFT JOIN cycle_count_temp_qc cctq
								ON cctq.kd_unik=rcvbrg.kd_unik
									AND cctq.cc_id=cc1.cc_id
							LEFT JOIN satuan sat
								ON sat.id_satuan=brg.id_satuan
							WHERE
								cc1.cc_id=\''.$post['cc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						)
						UNION
						(
							SELECT
								cc1.cc_id, cc1.cc_finish AS cc_finish, loc.loc_id, brg.id_barang, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
								ccd.id_qc, ccd.kd_unik, \'PENDING REVIEW\' AS status, rcvbrg.id_receiving_barang, ccd.cc_detail_id,
								ccd.tgl_in, ccd.tgl_exp, ccd.cc_qty
							FROM
								cycle_count cc1
							JOIN cycle_count_h2 cc2
								ON cc1.cc_id=cc2.cc_id_h1
							JOIN cycle_count_detail ccd
								ON ccd.cc_id=cc1.cc_id
							LEFT JOIN receiving_barang rcvbrg
								ON rcvbrg.kd_unik=ccd.kd_unik
							LEFT JOIN barang brg
								ON brg.id_barang=ccd.id_barang
							LEFT JOIN m_loc loc
								ON loc.loc_id=ccd.loc_id
							LEFT JOIN satuan sat
								ON sat.id_satuan=brg.id_satuan
							WHERE
								cc1.cc_id=\''.$post['cc_id'].'\'
							AND
								loc.loc_type NOT IN('.$this->config->item('cc_location').')
						)
					) AS tbl
					GROUP BY
						kd_unik';

		}

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];

		//$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';
			/*
			$disabled = '';
			if($row[$i]['total_scanned'] > 0)
				$disabled = '';
			else
				$disabled = 'disabled-link';

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$disabled.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Close CC</a>';
			$action .= '</li>';
			*/

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-adjust" data-id="'.$row[$i]['cc_id'].'" data-name="ADJUSTMENT"><i class="fa fa-edit"></i> Adjustment</a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-cancel" data-id="'.$row[$i]['cc_id'].'" data-name="CANCEL"><i class="fa fa-close"></i> Cancel</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			if(empty($row[$i]['cc_finish']))
				$action = '';

			$nested = array();
			$nested[] = '<input type="checkbox" name="sn[]" value="">';
			$nested[] = $row[$i]['cc_id'];
			$nested[] = $row[$i]['loc_id'];
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $row[$i]['id_qc'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['kd_unik'];

			$status = $row[$i]['status'];

			if($row[$i]['system_qty'] == 'YES' && $row[$i]['scanned_qty'] == 'YES'){
				$action = '';
				$status = 'CANCEL';
			}

			$nested[] = $status;
			$nested[] = $row[$i]['system_qty'];
			$nested[] = $row[$i]['scanned_qty'];
			$nested[] = $row[$i]['cc_qty'];

			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItem($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'loc_name',
			2 => 'nama_barang',
			3 => 'qty',
			4 => '( qty_number - check_result_number )'
		);

		if($post['cc_type'] == 'SN'){

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
					(
						SELECT
							cc1.cc_id
						FROM
							cycle_count cc1
						JOIN cycle_count_h2 cc2
							ON cc1.cc_id=cc2.cc_id_h1
						LEFT JOIN receiving_barang rcvbrg
							ON rcvbrg.id_barang=cc2.item_id
						LEFT JOIN barang brg
							ON brg.id_barang=cc2.item_id
						LEFT JOIN m_loc loc
							ON loc.loc_id=rcvbrg.loc_id
						LEFT JOIN satuan sat
							ON sat.id_satuan=brg.id_satuan
						WHERE
							cc1.cc_id=\''.$post['cc_id'].'\'
						AND
							loc.loc_type NOT IN('.$this->config->item('cc_location').')
						GROUP BY
							rcvbrg.loc_id, rcvbrg.id_barang
					) AS tbl';

			$row = $this->db->query($sql)->row_array();
			$totalData = $row['total'];

			$sql = 'SELECT
						cc1.cc_id, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
						CONCAT(COUNT(rcvbrg.id_barang), \' \', IFNULL(nama_satuan, \'\')) AS qty,
						COUNT(rcvbrg.id_barang) AS qty_number,
						CONCAT(IFNULL((
							SELECT
								SUM(cc_qty)
							FROM
								cycle_count_detail cc_detail
							WHERE
								cc_id=\''.$post['cc_id'].'\'
						), \'0\'), \' \', IFNULL(nama_satuan, \'\')) AS check_result,
						IFNULL((
							SELECT
								SUM(cc_qty)
							FROM
								cycle_count_detail cc_detail
							WHERE
								cc_id=\''.$post['cc_id'].'\'
						), \'0\') AS check_result_number,
						IFNULL(nama_satuan, \'\') AS nama_satuan
					FROM
						cycle_count cc1
					JOIN cycle_count_h2 cc2
						ON cc1.cc_id=cc2.cc_id_h1
					JOIN receiving_barang rcvbrg
						ON rcvbrg.id_barang=cc2.item_id
					JOIN barang brg
						ON brg.id_barang=cc2.item_id
					JOIN m_loc loc
						ON loc.loc_id=rcvbrg.loc_id
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						cc1.cc_id=\''.$post['cc_id'].'\'
					AND
						loc.loc_type NOT IN('.$this->config->item('cc_location').')
					GROUP BY
						rcvbrg.loc_id, rcvbrg.id_barang';

		}else{

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
					(
						SELECT
							cc1.cc_id
						FROM
							cycle_count cc1
						JOIN cycle_count_h2 cc2
							ON cc1.cc_id=cc2.cc_id_h1
						LEFT JOIN receiving_barang rcvbrg
							ON rcvbrg.loc_id=cc2.loc_id
						LEFT JOIN barang brg
							ON brg.id_barang=cc2.item_id
						LEFT JOIN m_loc loc
							ON loc.loc_id=rcvbrg.loc_id
						LEFT JOIN satuan sat
							ON sat.id_satuan=brg.id_satuan
						WHERE
							cc1.cc_id=\''.$post['cc_id'].'\'
						AND
							loc.loc_type NOT IN('.$this->config->item('cc_location').')
						GROUP BY
							rcvbrg.loc_id, rcvbrg.id_barang
					) AS tbl';

			$row = $this->db->query($sql)->row_array();
			$totalData = $row['total'];

			$sql = 'SELECT
						cc1.cc_id, loc_name, kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
						CONCAT(COUNT(rcvbrg.id_barang), \' \', IFNULL(nama_satuan, \'\')) AS qty,
						COUNT(rcvbrg.id_barang) AS qty_number,
						CONCAT(IFNULL((
							SELECT
								SUM(cc_qty)
							FROM
								cycle_count_detail cc_detail
							WHERE
								cc_id=\''.$post['cc_id'].'\'
						), \'0\'), \' \', IFNULL(nama_satuan, \'\')) AS check_result,
						IFNULL((
							SELECT
								SUM(cc_qty)
							FROM
								cycle_count_detail cc_detail
							WHERE
								cc_id=\''.$post['cc_id'].'\'
						), \'0\') AS check_result_number,
						IFNULL(nama_satuan, \'\') AS nama_satuan
					FROM
						cycle_count cc1
					JOIN cycle_count_h2 cc2
						ON cc1.cc_id=cc2.cc_id_h1
					JOIN receiving_barang rcvbrg
						ON rcvbrg.loc_id=cc2.loc_id
					JOIN barang brg
						ON brg.id_barang=rcvbrg.id_barang
					JOIN m_loc loc
						ON loc.loc_id=rcvbrg.loc_id
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						cc1.cc_id=\''.$post['cc_id'].'\'
					AND
						loc.loc_type NOT IN('.$this->config->item('cc_location').')
					GROUP BY
						rcvbrg.loc_id, rcvbrg.id_barang';

		}

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';
			/*
			$disabled = '';
			if($row[$i]['total_scanned'] > 0)
				$disabled = '';
			else
				$disabled = 'disabled-link';

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$disabled.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Close CC</a>';
			$action .= '</li>';
			*/

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['qty'];
			$nested[] = $row[$i]['check_result'];
			$nested[] = $row[$i]['qty_number'] - $row[$i]['check_result_number'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailDataTable($post = array()){

		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'name'
		);

		$this->db->start_cache();

		$this->db
			->select('cc.cc_id')
			->from('cycle_counts cc')
			->join('cycle_count_details ccd','cc.cc_id=ccd.cycle_count_id')
			->join('locations loc',"loc.id=ccd.location_id AND cc.type='LOC'",'left outer')
			->join('location_areas loc_area',"loc_area.id=ccd.area_id AND cc.type='LOC'",'left outer')
			->join('items itm',"itm.id=ccd.item_id AND cc.type='SN'",'left outer')
			->where('cc.cc_id', $post['cc_id'])
			->group_by('cc.cc_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select([
				"(CASE WHEN cc.type='LOC' AND ccd.location_id IS NULL THEN CONCAT(' ALL AREA ', loc_area.name) ELSE COALESCE(loc.name, CONCAT(itm.code , ' - ', itm.name)) END) AS name"
			])
			->group_by('cc.type, ccd.location_id, loc_area.name, loc.name, itm.code, itm.name')
			->group_by("(CASE WHEN loc.name IS NOT NULL THEN loc.name ELSE CAST(ccd.item_id as CHAR) END)")
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['name'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function closeCc($post = array()){
		$result = array();

		$sql = 'UPDATE cycle_count SET remark=\''.$post['remark'].'\', status=\'1\' WHERE cc_id=\''.$post['cc_id'].'\'';
		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Close cycle count success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Close cycle count failed';
		}

		return $result;
	}

    public function getScans($post = array()){
    	if(!empty($post['cc_id'])){
    		$this->db->where('cycle_count_id', $post['cc_id']);
    	}

        $this->db
        	->select('count(unique_code) as qty, usr.nama, ccr.item_id as id_barang') 
        	->from('cycle_count_results ccr')
        	->join('users usr','usr.id=ccr.user_id','left')
        	->group_by('ccr.item_id, ccr.user_id, usr.nama');

        	$result = $this->db->get();

        	return $result->result_array();
    }

	public function getItems($post = array()){

		$result = array();

		if($post['cc_type'] == 'SN'){

			$sql = "SELECT
						COALESCE(loc.name, '-') AS loc_name,
						COALESCE(itm.code, '') AS kd_barang,
						CONCAT(itm.code, ' - ', itm.name) AS nama_barang,
						itm.name AS nama_barang_ori,
                        itm.id as id_barang,
						(
							SELECT
								COUNT(*)
							FROM
								item_receiving_details irds
							WHERE
								location_id=ird.location_id
							AND
								item_id=ir.item_id
						) AS qty,
						unt.name as nama_satuan
					FROM
						cycle_counts cc
					JOIN cycle_count_details ccd
						ON cc.cc_id=ccd.cycle_count_id
					JOIN items itm
						ON itm.id=ccd.item_id
					LEFT JOIN item_receiving ir
						ON ir.item_id=ccd.item_id
					LEFT JOIN item_receiving_details ird
						ON ir.id=ird.item_receiving_id
					LEFT JOIN locations loc
						ON loc.id=ird.location_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					WHERE
						cc.cc_id=".$post['cc_id']."
					AND
						(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
					GROUP BY
						ird.location_id, ir.item_id, loc.name, itm.code, itm.name, itm.id, unt.name
					ORDER BY
						itm.code ASC";

		}else{

			$sql = "SELECT
						COALESCE(loc.name, '-') AS loc_name,
						COALESCE(itm.code, '') AS kd_barang,
						CONCAT(itm.code, ' - ', itm.name) AS nama_barang,
						itm.name AS nama_barang_ori,
                        itm.id as id_barang,
						(
							SELECT
								COUNT(*)
							FROM
								item_receiving_details irds
							JOIN
								item_receiving irs
								ON irs.id=irds.item_receiving_id
							WHERE
								location_id=irds.location_id
							AND
							irds.item_id=ir.item_id
						) AS qty,
						unt.name as nama_satuan
					FROM
						cycle_counts cc
					JOIN cycle_count_details ccd
						ON cc.cc_id=ccd.cycle_count_id
					JOIN items itm
						ON itm.id=ccd.item_id
					LEFT JOIN item_receiving ir
						ON ir.item_id=ccd.item_id
					LEFT JOIN item_receiving_details ird
						ON ir.id=ird.item_receiving_id
					LEFT JOIN locations loc
						ON loc.id=ccd.location_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					WHERE
						cc.cc_id=".$post['cc_id']."
					AND
						(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
					GROUP BY
						ird.location_id, ir.item_id, loc.name, itm.code, itm.name, itm.id, unt.name
					ORDER BY
						itm.code ASC";
		}

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function delete($post = array()){
		$result = array();

		$this->db
			->select('count(*) as total')
			->from('cycle_count_scanned')
			->where('cycle_count_id', $post['cc_id']);

		$row = $this->db->get()->row_array();

		if($row['total'] > 0){

			$result['status'] = 'ERR';
			$result['message'] = 'Delete failed. Because data has been used';

		}else{


			$this->db->trans_start();

			$this->db
				->from('cycle_count_temp_qc')
				->where('cycle_count_id', $post['cc_id']);

			$row = $this->db->get()->result_array();

			$len = count($row);

			for($i = 0; $i < $len; $i++){

				$this->db
					->set('qc_id', $row[$i]['qc_id'])
					->where('unique_code', $row[$i]['unique_code'])
					->update('item_receiving_details');

			}

			$this->db
				->set('status_id', 100)
				->where('cc_id', $post['cc_id'])
				->update('cycle_counts');

			$this->db->trans_complete();

			$result['status'] = 'OK';
			$result['message'] = 'Delete success';

		}

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$this->db
			->select('location_id as loc_id, item_id,  area_id')
			->from('cycle_count_details')
			->where('cycle_count_id', $post['cc_id'])
			->group_by('location_id, item_id, area_id');

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getEdit($post = array()){
		$result = array();

		$this->db
			->select([
				"cc.cc_id","cc.code as cc_code","TO_CHAR(cc.time, 'DD/MM/YYYY') as cc_time","cc.type as cc_type","cc.status_id as status","cc.no_person",
				"(CASE cc.type WHEN 'SN' THEN 'By Item' WHEN 'LOC' THEN 'By Location' END) as cc_type_alias", "user_name","st.name as nama_status","cc.warehouse_id"
			])
			->from('cycle_counts cc')
			->join('users usr','usr.id=cc.user_id')
			->join('status st','st.status_id=cc.status_id')
			->where('cc.cc_id', $post['cc_id']);

		$result = $this->db->get()->row_array();

		return $result;
	}

	public function getCcCode($post = array()){
		$result = array();

		$this->db
			->select('cc_id, code as cc_code')
			->from('cycle_counts')
			->like('code', $post['query']);

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getCcStatus(){
		$result = array();

		$this->db
			->select('status_id as id_status_cc, code as kd_status, name as nama_status')
			->from('status')
			->like('type','CC');

		$result = $this->db->get()->result_array();

		return $result;
	}

	function data_insert($data){

		$data = array(
			'code' => $data['cc_code'],
			'no_person' => $data['no_person'],
			'time'		=> $data['cc_time'],
			'type'		=> $data['cc_type'],
			'user_id'	=> $data['user_id'],
			'warehouse_id' => $data['warehouse_id']
		);

		return $data;
	}

	public function create($data, $detail, $user,$preorder=0) {
        $this->db->trans_start();

        $data = $this->data_insert($data);
        $data['code']		= $this->get_cc_code();
        if($preorder){
        	$data['code']	.= " - PREORDER";
        }

        $data['status_id'] 	= 3;
        $this->db->insert('cycle_counts', $data);
        $ccId = $this->db->insert_id();

        if(!empty($detail)) {
            foreach ($detail as &$row) {
                $row['cycle_count_id'] = $ccId;

                date_default_timezone_set('Asia/Jakarta');

				if($data['type'] == 'SN'){

					$this->db
						->select('unique_code, qc_id, last_qty')
						->from('item_receiving_details ird')
						->join('locations loc','loc.id=ird.location_id')
						->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
						->where('ir.item_id', $row['item_id'])
						->where("(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))", NULL);

					$rows = $this->db->get()->result_array();
					$len = count($rows);

					for($i = 0; $i < $len; $i++){

						$this->db
							->set('cycle_count_id', $ccId)
							->set('unique_code', $rows[$i]['unique_code'])
							->set('qc_id', $rows[$i]['qc_id'])
							->set('qty', $rows[$i]['last_qty'])
							->insert('cycle_count_temp_qc');

						$uniqueCode[] = $rows[$i]['unique_code'];

					}


				}else{

					$uniqueCode = [];

					$sql = "
						INSERT INTO cycle_count_temp_qc(cycle_count_id,unique_code,qc_id,qty) 
						SELECT 
							CAST('$ccId' as INTEGER) as cycle_count_id, unique_code, qc_id, last_qty as qty
						FROM
							item_receiving_details ird
						JOIN locations loc
							ON loc.id=ird.location_id
						LEFT JOIN item_receiving ir
							ON ir.id=ird.item_receiving_id
						WHERE 
							ird.location_id=".$row['loc_id']."
						AND
							(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
					";

					$this->db->query($sql);

					$sql1= "

						UPDATE
							item_receiving_details
						SET
							cycle_count_id=$ccId,
							st_cc=0,
							qc_id=4
						WHERE
							id IN (
								SELECT 
									ird.id
								FROM
									item_receiving_details ird
								JOIN locations loc
									ON loc.id=ird.location_id
								LEFT JOIN item_receiving ir
									ON ir.id=ird.item_receiving_id
								WHERE 
									ird.location_id=".$row['loc_id']."
								AND
									(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							)

					";

					$this->db->query($sql1);

					// $this->db
					// 	->select('unique_code, qc_id, last_qty')
					// 	->from('item_receiving_details ird')
					// 	->join('locations loc','loc.id=ird.location_id')
					// 	->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
					// 	->where('ird.location_id', $row['loc_id'])
					// 	->where("(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))", NULL);

					// $rows = $this->db->get()->result_array();
					// $len = count($rows);

					// $uniqueCode = [];

					// for($i = 0; $i < $len; $i++){

					// 	$this->db
					// 		->set('cycle_count_id', $ccId)
					// 		->set('unique_code', $rows[$i]['unique_code'])
					// 		->set('qc_id', $rows[$i]['qc_id'])
					// 		->set('qty', $rows[$i]['last_qty'])
					// 		->insert('cycle_count_temp_qc');

					// 	$uniqueCode[] = $rows[$i]['unique_code'];

					// }

					$row['location_id'] = $row['loc_id'];
					unset($row['loc_id']);

				}

				if(!empty($uniqueCode)){
					$this->db
						->set('cycle_count_id', $ccId)
						->set('st_cc',0)
						->set('qc_id',4)
						->where_in('unique_code', $uniqueCode)
						->update('item_receiving_details');
				}


            }

            $this->db->insert_batch('cycle_count_details', $detail);

            $this->add_cc_user($ccId, $user);
        }

        $this->db->trans_complete();
		return $this->db->trans_status();
    }

    public function getLocInArea($AreaId){
    	$this->db->select('id as loc_id');
    	$this->db->from('locations');
    	$this->db->where('location_area_id',$AreaId);
    	return $this->db->get();
    }

    public function getLocInWarehouse($warehouseId){
    	$this->db
    		->select('loc.id as loc_id, loc_area.id as loc_area_id')
    		->from('location_areas loc_area')
    		->join('locations loc','loc.location_area_id=loc_area.id')
    		->where('loc_area.warehouse_id',$warehouseId);

    	return $this->db->get();
    }

    public function update($data, $detail, $id,$user) {

        $this->db->trans_start();

        $dataInsert = $this->data_insert($data);
        $this->db->where('cc_id',$id);
        $this->db->update('cycle_counts', $dataInsert);

        $this->db->where("cycle_count_id", $id);
        $this->db->delete('cycle_count_details');

        $this->db->where("cycle_count_id", $id);
        $this->db->delete('cycle_count_scanned');

        $this->db->set('cycle_count_id',NULL);
        $this->db->where('cycle_count_id',$id);
        $this->db->update('item_receiving_details');

        if(!empty($detail)) {
            foreach ($detail as &$row) {
                $row['cycle_count_id'] = $id;

                date_default_timezone_set('Asia/Jakarta');

				if($data['cc_type'] == 'SN'){

					$this->db
						->select('unique_code, qc_id, last_qty')
						->from('item_receiving_details ird')
						->join('locations loc','loc.id=ird.location_id')
						->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
						->where('ir.item_id', $row['item_id'])
						->where("(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))", NULL);

					$rows = $this->db->get()->result_array();
					$len = count($rows);

					for($i = 0; $i < $len; $i++){

						$this->db
							->set('cycle_count_id', $id)
							->set('unique_code', $rows[$i]['unique_code'])
							->set('qc_id', $rows[$i]['qc_id'])
							->set('qty', $rows[$i]['last_qty'])
							->insert('cycle_count_temp_qc');

						$uniqueCode[] = $rows[$i]['unique_code'];

					}

				}else{

					$this->db
						->select('unique_code, qc_id, last_qty')
						->from('item_receiving_details ird')
						->join('locations loc','loc.id=ird.location_id')
						->join('item_receiving ir','ir.id=ird.item_receiving_id','left')
						->where('ird.location_id', $row['loc_id'])
						->where("(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))", NULL);

					$rows = $this->db->get()->result_array();
					$len = count($rows);

					$uniqueCode = [];

					for($i = 0; $i < $len; $i++){

						$this->db
							->set('cycle_count_id', $id)
							->set('unique_code', $rows[$i]['unique_code'])
							->set('qc_id', $rows[$i]['qc_id'])
							->set('qty', $rows[$i]['last_qty'])
							->insert('cycle_count_temp_qc');

						$uniqueCode[] = $rows[$i]['unique_code'];

					}

					$row['location_id'] = $row['loc_id'];
					unset($row['loc_id']);

				}

				if(!empty($uniqueCode)){
					$this->db
						->set('cycle_count_id', $id)
						->set('st_cc',0)
						->set('qc_id',4)
						->where_in('unique_code', $uniqueCode)
						->update('item_receiving_details');
				}
            }

            $this->db->insert_batch('cycle_count_details', $detail);

            $this->add_cc_user($id, $user);
        }

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

	public function getChooseItem($post = array()){
		$result = array();

		$where = '';
		if(isset($post['cc_id'])){
			$this->db->where('cc.cc_id <>', $post['cc_id']);
		}

		$this->db
			->select('ccd.item_id')
			->from('cycle_count_details ccd')
			->join('cycle_counts cc','cc.cc_id=ccd.cycle_count_id')
			->where('cc.type','SN')
			->where_not_in('cc.status_id', [4,100]);

		$item_id = $this->db->get_compiled_select();

		$this->db
			->select(["id as id_barang","code as kd_barang","CONCAT(code, ' - ', name) as nama_barang"])
			->from('items')
			->where("id NOT IN ($item_id)", NULL);

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getLocation($post = array()){
		$result = array();

		$where = '';
		if(isset($post['cc_id'])){
			$where .= ' AND cc.cc_id != \''.$post['cc_id'].'\' ';
		}

		$sql = 'SELECT
					*
				FROM
					m_loc
				WHERE
					loc_id NOT IN('.$this->config->item('cc_location_id').')
				AND
					loc_name IS NOT NULL
				AND
					loc_id NOT IN(
						SELECT
							loc_id
						FROM
							cycle_count_h2 cc2
						JOIN cycle_count cc
							ON cc.cc_id=cc2.cc_id_h1
						WHERE
							cc.status != \'1\'
						AND
							cc.cc_type = \'LOC\''.$where.'
					)
				ORDER BY loc_name ASC';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    public function getLocation2($post = array()){
		$result = array();

        $where = '';

        if(!empty($post['loc_area_id'])){
        	$this->db->where('location_area_id', $post['loc_area_id']);
        }

        if(!empty($post['warehouse_id'])){
        	$this->db->where('loc_area.warehouse_id', $post['warehouse_id']);
        }

        $this->db
        	->select('loc.id as loc_id, loc.name as loc_name')
        	->from('locations loc')
        	->join('location_areas loc_area','loc_area.id=loc.location_area_id')
        	->where('loc.id NOT IN('.$this->config->item('cc_location_id').')', NULL)
        	->where('loc.name IS NOT NULL', NULL)
        	->order_by('loc.name','ASC');

		$result = $this->db->get()->result_array();

		return $result;
	}

    public function getLocationArea($post = array()){
		$result = array();

		if(!empty($post['warehouse_id'])){
			$this->db->where('warehouse_id',$post['warehouse_id']);
		}

		$this->db
			->select('id as loc_area_id, name as loc_area_name')
			->from('location_areas')
			->where('name IS NOT NULL', NULL)
			->order_by('name','ASC');

		$result = $this->db->get()->result_array();

		$addValue[] = array(
			'loc_area_id' => 0,
			'loc_area_name' => '-- ALL AREA --'
		);

		$result = array_merge($addValue, $result);

		return $result;
	}

	public function getList($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => 'cc_id',
			1 => '',
			2 => 'cc_code',
			3 => 'cc_time_sort',
			4 => 'cc_type',
			5 => 'remark',
			6 => 'cc_start_sort',
			7 => 'cc_finish_sort',
			8 => 'status'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['cc_id'])){
			$this->db->where('cc.cc_id', $post['cc_id']);
		}

		if($post['status'] != ''){
			$this->db->where('cc.status_id',$post['status']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('cc.warehouse_id',$post['warehouse']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('cc.time >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'));
			$this->db->where('cc.time <=', DateTime::createFromFormat('d/m/Y', $post['to'])->modify('+1 day')->format('Y-m-d'));
		}

		$this->db
			->from('cycle_counts cc')
			->join('warehouses wh','wh.id=cc.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('status st','st.status_id=cc.status_id')
			->where('cc.status_id <>', 100);

		$this->db->stop_cache();

		$totalData = $this->db->get()->num_rows();

		$this->db
			->select(["time as cc_time_sort","cc.start as cc_start_sort","cc.finish as cc_finish_sort",
				"TO_CHAR(time, 'DD/MM/YYYY') AS cc_time","COALESCE(remark, '-') AS remark", "cc.status_id as status", "st.name as nama_status", "cc_id", "cc.code as cc_code",
				"(
					CASE cc.type
						WHEN 'SN'
							THEN 'By Item'
						ELSE
							'By Location'
					END
				) AS cc_type",
				"(
					SELECT
						COUNT(*)
					FROM
						cycle_count_scanned
					WHERE
						cycle_count_id=cc.cc_id
				) AS total_scanned",
				"(
					CASE
						WHEN cc.start IS NULL
							THEN '-'
						ELSE TO_CHAR(cc.start, 'HH24:MI <br> DD/MM/YYYY')
					END
				) AS cc_start",
				"(
					CASE
						WHEN cc.finish IS NULL
							THEN '-'
						ELSE TO_CHAR(cc.finish, 'HH24:MI <br> DD/MM/YYYY')
					END
				) AS cc_finish"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			$dis = '';
			$disabled = '';
			$disabled2 = '';
			$disabled3 = '';

			if($row[$i]['total_scanned'] > 0){
				if($row[$i]['status'] == '1')
					//$disabled = 'disabled-link';
					$disabled = '';
				else
					$disabled = '';
			}else{
				$disabled = 'disabled-link';
			}

			if($row[$i]['total_scanned'] > 0){
				$disabled2 = 'disabled-link';
			}else{
				if($row[$i]['status'] == '1')
					$disabled2 = 'disabled-link';
				else
					$disabled2 = '';
			}

			if($row[$i]['status'] == '1'){
				$disabled3 = 'disabled-link';
			}else{
				$disabled3 = '';
			}

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$disabled3.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-print"></i> Print CC Document</a>';
				$action .= '</li>';
                $action .= '<li>';
				$action .= '<a class="data-table-print2 '.$disabled3.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-print"></i> Print CC Document 2</a>';
				$action .= '</li>';
            }

			if($row[$i]['status'] == '1')
				$dis = 'disabled-link';
			else{
				if($row[$i]['total_scanned'] > 0){

					if(!empty($row[$i]['cc_finish']) && $row[$i]['cc_finish'] != '-'){
						$dis = 'disabled-link';
					}else{
						$dis = '';
					}
				}else{
					$dis = 'disabled-link';
				}

				/*
				if($row[$i]['cc_start'] == '-')
					$dis = 'disabled-link';
				else
					$dis = '';
				*/
			}

			$action .= '<li>';
			$action .= '<a class="data-table-finish '.$dis.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Finish Count</a>';
			$action .= '</li>';

			$action .= '<li>';
			$action .= '<a class="data-table-check '.$disabled.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Check Result</a>';
			$action .= '</li>';

			/*
			$action .= '<li>';
			$action .= '<a class="data-table-close '.$disabled.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-check"></i> Close CC</a>';
			$action .= '</li>';
			*/

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$disabled2.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$disabled2.'" data-id="'.$row[$i]['cc_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();

			$s = $row[$i]['cc_start'];
			$f = $row[$i]['cc_finish'];

			if($s != '-'){
				$s = explode('<br>', $s);
				$s1 = '<br><span class="small-date">'.$s[1].'</span>';

				$s = $s[0] . $s1;
			}

			if($f != '-'){
				$f = explode('<br>', $f);
				$f1 = '<br><span class="small-date">'.$f[1].'</span>';

				$f = $f[0] . $f1;
			}

			$nested[] = $row[$i]['cc_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'cycle_count/detail/'.$row[$i]['cc_id'].'">' . $row[$i]['cc_code'] . '</a>';
			$nested[] = $row[$i]['cc_time'];
			$nested[] = $row[$i]['cc_type'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $s;
			$nested[] = $f;
			$nested[] = $row[$i]['nama_status'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    private function data($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $cc_code= $this->input->post("cc_code");
        $tanggal_awal = $this->input->post('tanggal_awal_pl');
        $tanggal_akhir = $this->input->post('tanggal_akhir_pl');

        if(!empty($cc_code)){
            $condition["cc.cc_code like '%$cc_code%'"]=null;
        }
        if(!empty($nama_customer)){
            $condition["a.nama_customer like '%$nama_customer%'"]=null;
        }

        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["cc.cc_time >= '$tanggal_awal'"] = null ;
            $condition["cc.cc_time <= '$tanggal_akhir'"] = null;
        }


        $this->db->from('cycle_count_h1 cc');
        $this->db->order_by('cc.cc_id_h1 DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_list_do($condition = array()) {
        // ===========Filtering=================
        $condition["cc_id_h1"]=null;


        $this->db->from($this->do  . ' a');
        $this->db->order_by('a.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $cc_code= $this->input->post("cc_code");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $cc_id_h1 = $this->input->post('cc_id_h1');
        if(!empty($cc_code)){
            $condition["a.cc_code like '%$cc_code%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($cc_id_h1)){
            $condition["h2.cc_id_h1"]=$cc_id_h1;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

        $this->db->select('*');
        $this->db->from('cycle_count_h2 h2');
        $this->db->join('cycle_count_h1 h1','h1.cc_id_h1 = h2.cc_id_h1','left');
        $this->db->join('m_loc l','l.loc_id = h2.loc_id','left');
        $this->db->join('barang b','b.id_barang = h2.item_id','left');
        $this->db->order_by('cc_id_h2');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_hasil($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $cc_code= $this->input->post("cc_code");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $cc_id_h1 = $this->input->post('cc_id_h1');
        if(!empty($cc_code)){
            $condition["a.cc_code like '%$cc_code%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($cc_id_h1)){
            $condition["h2.cc_id_h1"]=$cc_id_h1;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

        $this->db->from("cycle_count_h2 h2");
        $this->db->join('cycle_count_detail d','d.cc_id_h2 = h2.cc_id_h2','left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_databarcode_by_id($cc_id_h1) {
        $condition['h1.cc_id_h1'] = $cc_id_h1;
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");

        $this->db->select('cc_code, loc_name,d.id_barang, count(d.id_barang) as qty,cc_time, kd_barang, nama_barang, nama_satuan, cc_type');
        $this->db->from('cycle_count_h1 h1');
        $this->db->join('cycle_count_h2 h2','h1.cc_id_h1 = h2.cc_id_h1','left');
        $this->db->join('cycle_count_detail d','d.cc_id_h2 = h2.cc_id_h2','left');
        $this->db->join('barang b','b.id_barang = d.id_barang','left');
        $this->db->join('satuan sat','sat.id_satuan = b.id_satuan','left');
        $this->db->join('m_loc l','l.loc_id = d.loc_id','left');
        $this->db->group_by('d.loc_id');
        $this->db->group_by('d.id_barang');
        $this->db->where_condition($condition);

        return $this->db->get();
    }

    public function get_by_id($id) {
        $condition['cc.cc_id_h1'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['h2.cc_id_h2'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
    //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["cc.cc_time >= '$tahun_aktif_awal'"] = null ;
        $condition["cc.cc_time <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->cc_id_h1;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">';

            $action .= '<li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>View Cycle Count', array('id' => 'drildown_key_p_Pl_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_cycle_count', 'data-source' => base_url('cycle_count/get_detail_hasil_cycle_count/' . $id))) . ' ';
            $action .= '</li>';


            if($value->status == 0){
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print CC Doc', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('cycle_count/print_doc/' . $id)));
                $action .= '</li>';
                if ($this->access_right->otoritas('edit')) {
                    $action .= '<li>';
                    $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('cycle_count/edit/'. $id))) . ' ';
                    $action .= '</li>';
                }

                if ($this->access_right->otoritas('delete')) {
                    $action .= '<li>';
                    $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('cycle_count/delete/'. $id)));
                    $action .= '</li>';
                }
            }
            $action .= '</ul></div>';

            $status = $value->status == 0 ? "Open" : "Close";
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->cc_id_h1,
                    'cc_time' => hgenerator::switch_tanggal($value->cc_time),
                    'cc_code' => anchor(null, $value->cc_code, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_cycle_count', 'data-source' => base_url('cycle_count/get_detail_cycle_count/' . $id))) ,
                    'cc_type' => $value->cc_type,
                    'remark' => $value->remark,
                    'status' => $status,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->cc_id_h1,
                    'cc_time' => hgenerator::switch_tanggal($value->cc_time),
                    'cc_code' => $value->cc_code,
                    'cc_type' => $value->cc_type,
                    'remark' => $value->remark,
                    'status' => $status,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail() {
        $cc_id_h1 = $this->input->post('cc_id_h1');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->cc_id_h2;
            $action = '';
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('cycle_count/edit_detail/' .$cc_id_h1.'/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('cycle_count/delete_detail/' .$cc_id_h1.'/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            if (!empty($value->loc_name)) {
                $rows[] = array(
                    'primary_key' => $value->cc_id_h2,
                    'loc_name' => $value->loc_name,
                    'aksi' => $action

                );
            }else{
                $rows[] = array(
                    'primary_key' => $value->cc_id_h2,
                    'kd_barang' => $value->kd_barang,
                    'item_name' => $value->nama_barang,
                    'aksi' => $action

                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function init_clean($cc_id_h1){
        $this->db->from('cycle_count_h2 h2');
        $this->db->join('cycle_count_h1 h1','h1.cc_id_h1 = h2.cc_id_h1','left');
        $this->db->where('h2.cc_id_h1',$cc_id_h1);
        $hasil = $this->db->get();
        foreach ($hasil->result() as $row) {
            if($row->status==0){
                $this->db->where('cc_id_h2',$row->cc_id_h2);
                $this->db->delete('cycle_count_detail');
                # code...
                $st_clean = TRUE;
            }else{
                $st_clean = FALSE;
            }
        }

        return $st_clean;
    }

    public function sync_cc_detail($cc_id_h1){
        $st_clean = $this->init_clean($cc_id_h1);
        if($st_clean){
            $this->db->select('h2.cc_id_h2,rb.kd_unik as sn_by_item,rb2.kd_unik as sn_by_loc,rb.id_barang as id_barang_sn, rb2.id_barang as id_barang_loc, rb.loc_id as loc_id_sn , rb2.loc_id as loc_id_loc');
            $this->db->from('cycle_count_h1 h1');
            $this->db->join('cycle_count_h2 h2','h1.cc_id_h1 = h2.cc_id_h1','left');
            $this->db->join('receiving_barang rb','rb.id_barang = h2.item_id and rb.shipping_id IS NULL','left');
            $this->db->join('receiving_barang rb2','rb2.loc_id = h2.loc_id and rb2.shipping_id IS NULL','left');
            $this->db->where('h1.cc_id_h1',$cc_id_h1);
            $hasil = $this->db->get();
            foreach ($hasil->result() as $row) {
                if(!empty($row->sn_by_item)){
                    $data = array('cc_id_h2'=>$row->cc_id_h2,'kd_unik'=>$row->sn_by_item,'id_barang' => $row->id_barang_sn,'loc_id' => $row->loc_id_sn);
                    $this->db->insert('cycle_count_detail',$data);
                }else{
                    $data = array('cc_id_h2'=>$row->cc_id_h2,'kd_unik'=>$row->sn_by_loc,'id_barang' => $row->id_barang_loc,'loc_id' => $row->loc_id_loc);
                    $this->db->insert('cycle_count_detail',$data);
                }
                # code...
            }
        }

    }

    public function data_table_detail_hasil() {
        $cc_id_h1 = $this->input->post('cc_id_h1');
        $this->sync_cc_detail($cc_id_h1);

        // Total Record
        $total = $this->data_detail_hasil()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_hasil = $this->data_detail_hasil()->get();
        $rows = array();

        foreach ($data_detail_hasil->result() as $value) {

            $status = $value->cc_status == 0 ? 'Uncheked':'Cheked';
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_unik'=>$value->kd_unik,
                    'cc_status' => $status,
                );
            }else{
                $rows[] = array(
                    'kd_unik'=>$value->kd_unik,
                    'cc_status' => $status,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.cc_time >= '$tahun_aktif_awal'"] = null ;
        $condition["a.cc_time <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->cc_id_h1;
            $action = '';
                $rows[] = array(
                    'cc_time' => hgenerator::switch_tanggal($value->cc_time),
                    'cc_code' => $value->cc_code,
                    'proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                );
        }
        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->cc_id_h1_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create1($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update1($data, $id) {
        return $this->db->update($this->table, $data, array('cc_id_h1' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert('cycle_count_h2', $data);
    }

    public function update_detail($data, $id) {
        $this->db->from('do');
        $this->db->join('do_barang db','do.id_do = db.id_do','left');
        $this->db->where('do.id_do',$id);
        $hasil = $this->db->get();

        foreach ($hasil->result() as $row) {

            if($row->jenis_do == 'FIFO' || $row->jenis_do==''){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_in','ASC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'FEFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_exp','ASC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'LIFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_in','DESC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'LEFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_exp','DESC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }
            # code...
        }
        return $this->db->update($this->do, $data, array('id_do' => $id));
    }

    public function delete1($id) {
        $this->db->delete($this->table4, array('cc_id_h1' => $id));
        return $this->db->delete($this->table, array('cc_id_h1' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id_do' => $id));
    }


    public function options($default = '--Pilih Kode bbk--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->cc_id_h1] = $row->cc_code ;
        }
        return $options;
    }

    public function options_do($default = '--Choose DO--', $key = '') {
        $data = $this->data_list_do()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_do] = $row->kd_do ;
        }
        return $options;
    }

	public function get_user($condition=array()) {
		$this->db->select("a.id as user_id, *");
		$this->db->from("users a");
		$this->db->join("hr_grup b", "a.grup_id = b.grup_id");

		if(!empty($condition)){
			$this->db->where($condition);
		}

		$users = $this->db->get();
		return $users->result_array();
	}

	public function get_cc_users($data=array()){

		$this->db
			->select('user_id')
			->from('cycle_counts_users')
			->where('cycle_count_id',$data['cc_id']);

		$users = $this->db->get()->result_array();

		$res= array();

		foreach($users as $user){
			$res[] = $user['user_id'];
		}

		return $res;

	}

	public function add_cc_user($id='', $users=array()){

		if($users){

	        $this->db->trans_start();

				$data = array();

				foreach($users as $u){
					$data[] = array(
						"cycle_count_id"=> $id,
						"user_id" => $u
					);
				}

		       	$this->db->delete('cycle_counts_users', array('cycle_count_id'=>$id));
				$this->db->insert_batch('cycle_counts_users', $data);

	        $this->db->trans_complete();

	    }

		return $this->db->trans_status();

	}


	public function getCheckResultItem($post=array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'cc_id',
			2 => 'id_barang',
			3 => '',
			4 => 'nama_barang',
			5 => 'system_qty',
			6 => 'scanned_qty'
		);

		$id = $post['cc_id'];
		$type = $post['cc_type'];

		$ignoreLocation = $this->config->item('cc_location');

		$this->db
			->select('status_id as status')
			->from('cycle_counts')
			->where('cc_id',$id);
		$cc = $this->db->get()->row_array();

		if($cc['status'] != 4){

			if($post['cc_type'] == 'SN'){

				$sql = "
					SELECT 
						cc_id, id_barang, nama_barang, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty, cc_finish, cc_status
					FROM 
					(
						(

							SELECT
								cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								COALESCE(sum(ird.last_qty), 0) AS system_qty, '0' AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON cc.cc_id=ccd.cycle_count_id
							JOIN item_receiving ir
								ON ir.item_id=ccd.item_id
							JOIN item_receiving_details ird
								ON ird.item_receiving_id=ir.id
							LEFT JOIN items itm
								ON itm.id=ccd.item_id
							LEFT JOIN locations loc
								ON loc.id=ird.location_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccd.item_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccd.item_id, cc.cc_id, itm.id, ccr.status, cc.finish, itm.code, cc.status_id, cc.finish

						)UNION(

							SELECT
								cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								'0' AS system_qty, COALESCE(sum(ccs.qty), 0) AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON ccd.cycle_count_id=cc.cc_id
							JOIN cycle_count_scanned ccs
								ON cc.cc_id=ccs.cycle_count_id AND ccs.item_id=ccd.item_id
							LEFT JOIN items itm
								ON itm.id=ccd.item_id
							LEFT JOIN locations loc
								ON loc.id=ccs.location_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccd.item_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccd.item_id, cc.cc_id, itm.id, ccr.status, itm.code, cc.status_id, cc.finish

						)
					)as tbl
					GROUP BY id_barang, cc_id, nama_barang, status, cc_finish, cc_status
				";

			}else{

				$sql = "
					SELECT
						cc_id, id_barang, nama_barang, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty, cc_finish, cc_status
					FROM 
					(
						(
							SELECT
								cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								COALESCE(sum(ird.last_qty),0) AS system_qty,
								'0' AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON cc.cc_id=ccd.cycle_count_id
							JOIN item_receiving_details ird
								ON ird.location_id=ccd.location_id
							JOIN items itm
								ON itm.id=ird.item_id
							JOIN locations loc
								ON loc.id=ird.location_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ird.item_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								cc.cc_id, itm.id, ccr.status,cc.finish, cc.status_id, itm.code, itm.name

						)UNION(
							SELECT
								cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								'0' AS system_qty,
								COALESCE(sum(ccs.qty),0) AS scanned_qty, cc.finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							-- JOIN cycle_count_details ccd
							-- 	ON ccd.cycle_count_id=cc.cc_id
							JOIN cycle_count_scanned ccs
								ON ccs.cycle_count_id=cc.cc_id
							LEFT JOIN items itm
								ON itm.id=ccs.item_id
							LEFT JOIN locations loc
								ON loc.id=ccs.location_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccs.item_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccs.item_id, cc.cc_id, itm.id, ccr.status,cc.finish, cc.status_id, itm.code, itm.name
						)
					) as tbl
					GROUP BY id_barang, cc_id, nama_barang, status, cc_finish, cc_status
				";

			}
		}else{

			$sql = "
					SELECT
						cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
						COALESCE(sum(ccr.system_qty),0) as system_qty , COALESCE(sum(ccr.scanned_qty),0) as scanned_qty, cc.finish, cc.status_id as cc_status
					FROM
						cycle_counts cc
					LEFT JOIN cycle_count_results ccr
						ON ccr.cycle_count_id=cc.cc_id
					LEFT JOIN items itm
						ON itm.id=ccr.item_id
					LEFT JOIN locations loc
						ON loc.id=ccr.location_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					WHERE
						cc.cc_id=$id
					GROUP BY
						ccr.item_id, cc.cc_id, itm.id, ccr.status, cc.finish, cc.status_id
			";

		}

		$totalData = $this->db->query($sql)->num_rows();

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class='';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('edit')) {
				if(empty($row[$i]['cc_finish']) || $row[$i]['cc_status'] == 1)
					$class='disabled-link';

				$action .= '<li>';
				$action .= '<a class="data-table-detail" data-id="'.$row[$i]['cc_id'].'" href="javascript:;"><i class="fa fa-list"></i> Show Detail </a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-adjust '.$class.'" data-id="'.$row[$i]['cc_id'].'" data-name="ADJUST TO SYSTEM QTY"><i class="fa icon-check"></i><small> ADJUST TO SYSTEM QTY</small></a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-cancel '.$class.'" data-id="'.$row[$i]['cc_id'].'" data-name="ADJUST TO SCANNED QTY"><i class="fa icon-close"></i><small>ADJUST TO SCANNED QTY</small></a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = '<input type="checkbox" name="sn[]" value="">';
			$nested[] = $row[$i]['cc_id'];
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['nama_barang'];

			$nested[] = $row[$i]['system_qty'];
			$nested[] = $row[$i]['scanned_qty'];
			$nested[] = ($row[$i]['system_qty'] - $row[$i]['scanned_qty']);

			$status = $row[$i]['status'];

			$nested[] = $status;

			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;		
	}

	public function getCheckResultItemDetails($post = array()){
		$result = array();
		$where="";

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_unik',
			2 => 'parent_code',
			3 => 'loc_name',
			4 => 'system_qty',
			5 => 'scanned_qty',
		);

		if(!empty($post['id_barang'])){
			$where = " AND itm.id =". $post['id_barang'];
		}

		$id = $post['cc_id'];
		$ignoreLocation = $this->config->item('cc_location');

		$this->db
			->select('status_id as status')
			->from('cycle_counts')
			->where('cc_id',$id);
		$cc = $this->db->get()->row_array();

		if($cc['status'] != 4){

			if($post['cc_type'] == 'SN'){

			$sql = "
				SELECT
					cc_id, kd_unik, id_barang, item_code, item_name, loc_name, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty,parent_code
				FROM 
				(
					(
						SELECT
							cc.cc_id, itm.id as id_barang, ird.unique_code as kd_unik, loc.name as loc_name, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
							COALESCE(sum(ird.last_qty), 0) AS system_qty, '0' AS scanned_qty, COALESCE(ccs.parent_code, ird.parent_code) as parent_code
						FROM
							cycle_counts cc
						JOIN cycle_count_details ccd
							ON ccd.cycle_count_id=cc.cc_id
						JOIN item_receiving_details ird
							ON ird.location_id=ccd.location_id
						JOIN items itm
							ON itm.id=ird.item_id
						JOIN locations loc
							ON loc.id=ird.location_id
						LEFT JOIN units unt
							ON unt.id=itm.unit_id
						LEFT JOIN cycle_count_results ccr
							ON ccr.cycle_count_id=cc.cc_id
								AND ccr.item_id=ird.item_id
						LEFT JOIN cycle_count_scanned ccs
							ON ccs.cycle_count_id=cc.cc_id and ccs.unique_code=ird.unique_code
						WHERE
							cc.cc_id=$id
						AND
							(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
						$where
						GROUP BY
							ird.unique_code, cc.cc_id, itm.id, loc.name, ccr.status,ccs.parent_code, ird.parent_code, itm.code, itm.name

					)UNION(
						SELECT
							cc.cc_id, itm.id as id_barang, ccs.unique_code as kd_unik, loc.name as loc_name, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
							'0' AS system_qty, COALESCE(sum(ccs.qty), 0) AS scanned_qty, COALESCE(ccs.parent_code, ird.parent_code) as parent_code
						FROM
							cycle_counts cc
						-- JOIN cycle_count_details ccd
						-- 	ON ccd.cycle_count_id=cc.cc_id
						JOIN cycle_count_scanned ccs
							ON ccs.cycle_count_id=cc.cc_id
						JOIN items itm
							ON itm.id=ccs.item_id
						JOIN locations loc
							ON loc.id=ccs.location_id
						LEFT JOIN units unt
							ON unt.id=itm.unit_id
						LEFT JOIN cycle_count_results ccr
							ON ccr.cycle_count_id=cc.cc_id
								AND ccr.item_id=ccs.item_id
						LEFT JOIN item_receiving_details ird
							ON ird.unique_code=ccs.unique_code and ird.cycle_count_id=cc.cc_id
						WHERE
							cc.cc_id=$id
						AND
							(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
						$where
						GROUP BY
							ccs.unique_code, cc.cc_id, itm.id, loc.name, ccr.status, ccs.parent_code, ird.parent_code, itm.code, itm.name
					)
				) as tbl
				GROUP BY kd_unik, cc_id, id_barang, item_code, item_name, loc_name, status, parent_code";

			}else{

				$sql = "
					SELECT
						cc_id, kd_unik, id_barang, nama_barang, loc_name, status, COALESCE(SUM(system_qty),0) as system_qty, COALESCE(SUM(scanned_qty),0) as scanned_qty, COALESCE(parent_code,'-') as parent_code
					FROM 
					(
						(
							SELECT
								cc.cc_id, itm.id as id_barang, ird.unique_code as kd_unik, loc.name as loc_name, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								ird.last_qty AS system_qty, '0' AS scanned_qty, ird.parent_code
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON ccd.cycle_count_id=cc.cc_id
							JOIN item_receiving_details ird
								ON ird.location_id=ccd.location_id
							JOIN items itm
								ON itm.id=ird.item_id
							JOIN locations loc
								ON loc.id=ird.location_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ird.item_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							$where

						)UNION(
							SELECT
								cc.cc_id, itm.id as id_barang, ccs.unique_code as kd_unik, loc.name as loc_name, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								'0' AS system_qty, ccs.qty AS scanned_qty, ccs.parent_code
							FROM
								cycle_counts cc
							-- JOIN cycle_count_details ccd
							-- 	ON ccd.cycle_count_id=cc.cc_id
							JOIN cycle_count_scanned ccs
								ON ccs.cycle_count_id=cc.cc_id
							JOIN items itm
								ON itm.id=ccs.item_id
							JOIN locations loc
								ON loc.id=ccs.location_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccs.item_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							$where
						)
					) as tbl
					GROUP BY
						cc_id, kd_unik, id_barang, nama_barang, loc_name, status, parent_code
				";


			}

		}else{

			$sql = "
					SELECT
						cc.cc_id, itm.id as id_barang, ccr.unique_code as kd_unik, loc.name as loc_name, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
						'0' AS system_qty, ccs.qty AS scanned_qty, COALESCE(ccs.parent_code, ird.parent_code) as parent_code
					FROM
						cycle_counts cc
					LEFT JOIN cycle_count_results ccr
						ON ccr.cycle_count_id=cc.cc_id
					LEFT JOIN cycle_count_scanned ccs
						ON ccs.cycle_count_id=cc.cc_id and ccs.unique_code=ccr.unique_code
					LEFT JOIN item_receiving_details ird
						ON ird.cycle_count_id=cc.cc_id and ird.unique_code=ccr.unique_code
					LEFT JOIN items itm
						ON itm.id=ccr.item_id
					LEFT JOIN locations loc
						ON loc.id=ccr.location_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					WHERE
						cc.cc_id=$id
			";

		}

		$totalData = $this->db->query($sql)->num_rows();

		// $sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['length']." OFFSET ".$requestData['start']."   ";

		$row = $this->db->query($sql)->result_array();

		// print_r($row);
		// exit;

		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['loc_name'];

			$nested[] = $row[$i]['system_qty'];
			$nested[] = $row[$i]['scanned_qty'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function checkIsClose($id){
		$result = array();

		$sql = 'SELECT
					COALESCE((count(*)),0) as total
				FROM
					cycle_count
				WHERE
					cc_id=\''.$id.'\'
				AND
					status = 1
				';

		$this->db
			->select(["COALESCE((COUNT(*)),0) as total"])
			->from('cycle_counts')
			->where('cc_id', $id)
			->where('status_id', 4);

		$row = $this->db->get()->row_array();

		if(empty($row['total'])){
			$result = 'false';
		}else{
			$result = 'true';
		}

		return $result;
	}

	public function print_pdf($post=array()){

		$id = $post['cc_id'];
		$ignoreLocation = $this->config->item('cc_location');

		$this->db
			->select('status_id as status')
			->from('cycle_counts')
			->where('cc_id',$id);
		$cc = $this->db->get()->row_array();

		if($cc['status'] != 1){

			if($post['cc_type'] == 'SN'){

				$sql = "
					SELECT 
						cc_id, id_barang, nama_barang, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty, cc_finish, cc_status
					FROM 
					(
						(

							SELECT
								cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								COALESCE(sum(ird.last_qty), 0) AS system_qty, '0' AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON cc.cc_id=ccd.cycle_count_id
							JOIN item_receiving ir
								ON ir.item_id=ccd.item_id
							JOIN item_receiving_details ird
								ON ird.item_receiving_id=ir.id
							LEFT JOIN items itm
								ON itm.id=ccd.item_id
							LEFT JOIN locations loc
								ON loc.id=ird.location_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccd.item_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccd.item_id, cc.cc_id, itm.id, ccr.status, cc.finish

						)UNION(

							SELECT
								cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								'0' AS system_qty, COALESCE(sum(ccs.qty), 0) AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON ccd.cycle_count_id=cc.cc_id
							JOIN cycle_count_scanned ccs
								ON cc.cc_id=ccs.cycle_count_id AND ccs.item_id=ccd.item_id
							LEFT JOIN items itm
								ON itm.id=ccd.item_id
							LEFT JOIN locations loc
								ON loc.id=ccs.location_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccd.item_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccd.item_id, cc.cc_id, itm.id, ccr.status

						)
					)as tbl
					GROUP BY id_barang, cc_id, nama_barang, status, cc_finish, cc_status
				";

			}else{

				$sql = "
					SELECT
						cc_id, id_barang, nama_barang, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty, cc_finish, cc_status
					FROM 
					(
						(
							SELECT
								cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								COALESCE(sum(ird.last_qty),0) AS system_qty,
								'0' AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON cc.cc_id=ccd.cycle_count_id
							JOIN item_receiving_details ird
								ON ird.location_id=ccd.location_id
							JOIN items itm
								ON itm.id=ird.item_id
							JOIN locations loc
								ON loc.id=ird.location_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ird.item_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								cc.cc_id, itm.id, ccr.status,cc.finish, cc.status_id

						)UNION(
							SELECT
								cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								'0' AS system_qty,
								COALESCE(sum(ccs.qty),0) AS scanned_qty, cc.finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON ccd.cycle_count_id=cc.cc_id
							JOIN cycle_count_scanned ccs
								ON ccs.cycle_count_id=cc.cc_id AND ccs.location_id=ccd.location_id
							JOIN items itm
								ON itm.id=ccs.item_id
							JOIN locations loc
								ON loc.id=ccs.location_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccs.item_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccs.item_id, cc.cc_id, itm.id, ccr.status,cc.finish, cc.status_id
						)
					) as tbl
					GROUP BY id_barang, cc_id, nama_barang, status, cc_finish, cc_status
				";

			}
		}else{
			$sql = "
					SELECT
						cc.cc_id, itm.id as id_barang, CONCAT(itm.code, ' - ', itm.name) AS nama_barang, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
						sum(ccr.system_qty) as system_qty , sum(ccr.scanned_qty) as scanned_qty, cc.finish, cc.status_id as cc_status
					FROM
						cycle_counts cc
					LEFT JOIN cycle_count_results ccr
						ON ccr.cycle_count_id=cc.cc_id
					LEFT JOIN items itm
						ON itm.id=ccr.item_id
					LEFT JOIN locations loc
						ON loc.id=ccr.location_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					WHERE
						cc.cc_id=$id
					GROUP BY
						ccr.item_id, cc.cc_id, itm.id, ccr.status, cc.finish, cc.status_id
			";
		}

		$data = $this->db->query($sql);


		return $data->result_array();

	}

	public function getCcActor($post=array()){

		$this->db->select('user_name')
			->from('cycle_counts cc')
			->join('cycle_counts_users ccu','ccu.cycle_count_id=cc.cc_id')
			->join('users usr','usr.id=ccu.user_id')
			->where('cc.cc_id',$post['cc_id']);

		$data = $this->db->get();

		return $data->result_array();

	}

	public function getResultSummary($post=array()){

		$id = $post['cc_id'];
		$ignoreLocation = $this->config->item('cc_location');

		$this->db
			->select('status_id as status')
			->from('cycle_counts')
			->where('cc_id',$id);
		$cc = $this->db->get()->row_array();

		if($cc['status'] != 1){

			if($post['cc_type'] == 'SN'){

				$sql = "
					SELECT 
						cc_id, id_barang, item_code, item_name, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty, cc_finish, cc_status
					FROM 
					(
						(

							SELECT
								cc.cc_id, itm.id as id_barang, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								COALESCE(sum(ird.last_qty), 0) AS system_qty, '0' AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON cc.cc_id=ccd.cycle_count_id
							JOIN item_receiving ir
								ON ir.item_id=ccd.item_id
							JOIN item_receiving_details ird
								ON ird.item_receiving_id=ir.id
							LEFT JOIN items itm
								ON itm.id=ccd.item_id
							LEFT JOIN locations loc
								ON loc.id=ird.location_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccd.item_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccd.item_id, cc.cc_id, itm.id, ccr.status, cc.finish, itm.code

						)UNION(

							SELECT
								cc.cc_id, itm.id as id_barang, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								'0' AS system_qty, COALESCE(sum(ccs.qty), 0) AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON ccd.cycle_count_id=cc.cc_id
							JOIN cycle_count_scanned ccs
								ON cc.cc_id=ccs.cycle_count_id AND ccs.item_id=ccd.item_id
							LEFT JOIN items itm
								ON itm.id=ccd.item_id
							LEFT JOIN locations loc
								ON loc.id=ccs.location_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccd.item_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccd.item_id, cc.cc_id, itm.id, ccr.status,itm.code

						)
					)as tbl
					GROUP BY id_barang, cc_id, item_code, item_name, status, cc_finish, cc_status
				";

			}else{

				$sql = "
					SELECT
						cc_id, id_barang, item_code, item_name, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty, cc_finish, cc_status
					FROM 
					(
						(
							SELECT
								cc.cc_id, itm.id as id_barang, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								COALESCE(sum(ird.last_qty),0) AS system_qty,
								'0' AS scanned_qty, cc.finish as cc_finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON cc.cc_id=ccd.cycle_count_id
							JOIN item_receiving_details ird
								ON ird.location_id=ccd.location_id
							JOIN items itm
								ON itm.id=ird.item_id
							JOIN locations loc
								ON loc.id=ird.location_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ird.item_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								cc.cc_id, itm.id, ccr.status,cc.finish, cc.status_id, itm.code, itm.name

						)UNION(
							SELECT
								cc.cc_id, itm.id as id_barang, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
								'0' AS system_qty,
								COALESCE(sum(ccs.qty),0) AS scanned_qty, cc.finish, cc.status_id as cc_status
							FROM
								cycle_counts cc
							JOIN cycle_count_details ccd
								ON ccd.cycle_count_id=cc.cc_id
							JOIN cycle_count_scanned ccs
								ON ccs.cycle_count_id=cc.cc_id AND ccs.location_id=ccd.location_id
							JOIN items itm
								ON itm.id=ccs.item_id
							JOIN locations loc
								ON loc.id=ccs.location_id
							LEFT JOIN units unt
								ON unt.id=itm.unit_id
							LEFT JOIN cycle_count_results ccr
								ON ccr.cycle_count_id=cc.cc_id
									AND ccr.item_id=ccs.item_id
							WHERE
								cc.cc_id=$id
							AND
								(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
							GROUP BY
								ccs.item_id, cc.cc_id, itm.id, ccr.status,cc.finish, cc.status_id, itm.code, itm.name
						)
					) as tbl
					GROUP BY id_barang, cc_id, item_code, item_name, status, cc_finish, cc_status
				";

			}
		}else{
			$sql = "
					SELECT
						cc.cc_id, itm.id as id_barang, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
						sum(ccr.system_qty) as system_qty , sum(ccr.scanned_qty) as scanned_qty, cc.finish, cc.status_id as cc_status
					FROM
						cycle_counts cc
					LEFT JOIN cycle_count_results ccr
						ON ccr.cycle_count_id=cc.cc_id
					LEFT JOIN items itm
						ON itm.id=ccr.item_id
					LEFT JOIN locations loc
						ON loc.id=ccr.location_id
					LEFT JOIN units unt
						ON unt.id=itm.unit_id
					WHERE
						cc.cc_id=$id
					GROUP BY
						ccr.item_id, cc.cc_id, itm.id, ccr.status, cc.finish, cc.status_id
			";
		}

		$data = $this->db->query($sql);


		return $data->result_array();

	}

	public function getResultDetails($post=array()){

		$id = $post['cc_id'];
		$ignoreLocation = $this->config->item('cc_location');

		$this->db
			->select('status_id as status')
			->from('cycle_counts')
			->where('cc_id',$id);

		$cc = $this->db->get()->row_array();

		if($post['cc_type'] == 'SN'){

			$sql = "
				SELECT 
					cc_id, kd_unik, id_barang, item_code, item_name, loc_name, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty, parent_code
				FROM 
				(
					(

						SELECT
							cc.cc_id, itm.id as id_barang, ird.unique_code as kd_unik, loc.name as loc_name, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
							COALESCE(sum(ird.last_qty), 0) AS system_qty, '0' AS scanned_qty, COALESCE(ccs.parent_code, ird.parent_code) as parent_code
						FROM
							cycle_counts cc
						JOIN cycle_count_details ccd
							ON ccd.cycle_count_id=cc.cc_id
						JOIN cycle_count_temp_qc cctq ON cctq.cycle_count_id=cc.cc_id
						LEFT JOIN item_receiving_details ird ON ird.unique_code=cctq.unique_code and ird.location_id=ccd.location_id
						-- JOIN item_receiving ir
						-- 	ON ir.item_id=ccd.item_id
						-- JOIN item_receiving_details ird
						-- 	ON ird.item_receiving_id=ir.id
						LEFT JOIN items itm
							ON itm.id=ccd.item_id
						LEFT JOIN locations loc
							ON loc.id=ird.location_id
						LEFT JOIN cycle_count_results ccr
							ON ccr.cycle_count_id=cc.cc_id
								AND ccr.item_id=ccd.item_id
						LEFT JOIN cycle_count_scanned ccs
							ON ccs.cycle_count_id=cc.cc_id and ccs.unique_code=ird.unique_code
						LEFT JOIN units unt
							ON unt.id=itm.unit_id
						WHERE
							cc.cc_id=$id
						AND
							(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
						$where
						GROUP BY
							ird.unique_code, cc.cc_id, itm.id, loc.name, ccr.status,ccs.parent_code, ird.parent_code, itm.code

					)UNION(

						SELECT
							cc.cc_id, itm.id as id_barang, ccs.unique_code as kd_unik, loc.name as loc_name, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
							'0' AS system_qty, COALESCE(sum(ccs.qty), 0) AS scanned_qty, COALESCE(ccs.parent_code, ird.parent_code) as parent_code
						FROM
							cycle_counts cc
						JOIN cycle_count_details ccd
							ON ccd.cycle_count_id=cc.cc_id
						JOIN cycle_count_scanned ccs
							ON ccs.cycle_count_id=cc.cc_id AND ccs.item_id=ccd.item_id
						LEFT JOIN items itm
							ON itm.id=ccd.item_id
						LEFT JOIN locations loc
							ON loc.id=ccs.location_id
						LEFT JOIN cycle_count_results ccr
							ON ccr.cycle_count_id=cc.cc_id
								AND ccr.item_id=ccs.item_id
						LEFT JOIN units unt
							ON unt.id=itm.unit_id
						WHERE
							cc.cc_id=$id
						AND
							(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
						$where
						GROUP BY
							ccs.unique_code, cc.cc_id, itm.id, loc.name, ccr.status,ccs.parent_code, ird.parent_code, itm.code

					)
				)as tbl
				GROUP BY kd_unik, cc_id, id_barang, nama_barang, loc_name, status
			";

		}else{

			$sql = "
				SELECT
					cc_id, kd_unik, id_barang, item_code, item_name, loc_name, status, sum(system_qty) as system_qty, sum(scanned_qty) as scanned_qty,parent_code
				FROM 
				(
					(
						SELECT
							cc.cc_id, itm.id as id_barang, ird.unique_code as kd_unik, loc.name as loc_name, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
							COALESCE(sum(ird.last_qty), 0) AS system_qty, '0' AS scanned_qty, COALESCE(ccs.parent_code, ird.parent_code) as parent_code
						FROM
							cycle_counts cc
						JOIN cycle_count_details ccd
							ON ccd.cycle_count_id=cc.cc_id
						JOIN item_receiving_details ird
							ON ird.location_id=ccd.location_id
						JOIN items itm
							ON itm.id=ird.item_id
						JOIN locations loc
							ON loc.id=ird.location_id
						LEFT JOIN units unt
							ON unt.id=itm.unit_id
						LEFT JOIN cycle_count_results ccr
							ON ccr.cycle_count_id=cc.cc_id
								AND ccr.item_id=ird.item_id
						LEFT JOIN cycle_count_scanned ccs
							ON ccs.cycle_count_id=cc.cc_id and ccs.unique_code=ird.unique_code
						WHERE
							cc.cc_id=$id
						AND
							(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
						$where
						GROUP BY
							ird.unique_code, cc.cc_id, itm.id, loc.name, ccr.status,ccs.parent_code, ird.parent_code, itm.code, itm.name

					)UNION(
						SELECT
							cc.cc_id, itm.id as id_barang, ccs.unique_code as kd_unik, loc.name as loc_name, itm.code as item_code, itm.name as item_name, COALESCE(ccr.status, 'PENDING REVIEW') AS status, 
							'0' AS system_qty, COALESCE(sum(ccs.qty), 0) AS scanned_qty, COALESCE(ccs.parent_code, ird.parent_code) as parent_code
						FROM
							cycle_counts cc
						-- JOIN cycle_count_details ccd
						-- 	ON ccd.cycle_count_id=cc.cc_id
						JOIN cycle_count_scanned ccs
							ON ccs.cycle_count_id=cc.cc_id
						JOIN items itm
							ON itm.id=ccs.item_id
						JOIN locations loc
							ON loc.id=ccs.location_id
						LEFT JOIN units unt
							ON unt.id=itm.unit_id
						LEFT JOIN cycle_count_results ccr
							ON ccr.cycle_count_id=cc.cc_id
								AND ccr.item_id=ccs.item_id
						LEFT JOIN item_receiving_details ird
							ON ird.unique_code=ccs.unique_code and ird.cycle_count_id=cc.cc_id
						WHERE
							cc.cc_id=$id
						AND
							(loc.loc_type IS NULL OR loc.loc_type IN ('QC','NG'))
						$where
						GROUP BY
							ccs.unique_code, cc.cc_id, itm.id, loc.name, ccr.status, ccs.parent_code, ird.parent_code, itm.code, itm.name
					)
				) as tbl
				GROUP BY kd_unik, cc_id, id_barang, item_code, item_name, loc_name, status, parent_code
			";


		}

		$data = $this->db->query($sql);

		return $data->result_array();

	}

}

?>

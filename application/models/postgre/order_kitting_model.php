<?php
class order_kitting_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'repack_orders';
    private $table2 = "repacks_repack_orders";
    private $table3 = 'repack_items';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_order_kitting'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

	public function getOrderKittingPeminjaman(){
		$result = array();

		$sql = 'SELECT
					id_order_kitting, kd_order_kitting
				FROM
					order_kitting
				WHERE
					id_order_kitting_document=\'6\'';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	// public function getBatch($post = array()){
	// 	$result = array();
    //
	// 	$sql = 'SELECT
	// 				has_batch
	// 			FROM
	// 				barang
	// 			WHERE
	// 				id_barang=\''.$post['id_barang'].'\'';
    //
	// 	$row = $this->db->query($sql)->row_array();
    //
	// 	if(isset($row['has_batch'])){
    //
	// 		if($row['has_batch'] == 1){
    //
	// 			$sql = 'SELECT
	// 						kd_batch
	// 					FROM
	// 						receiving_barang
	// 					WHERE
	// 						id_barang=\''.$post['id_barang'].'\'
	// 					GROUP BY
	// 						kd_batch';
    //
	// 			$row = $this->db->query($sql)->result_array();
    //
	// 			$result['is_batch'] = 'YES';
	// 			$result['data'] = $row;
    //
	// 		}else{
	// 			$result['is_batch'] = 'NO';
	// 			$result['data'] = array();
	// 		}
    //
	// 	}else{
    //
	// 		$result['is_batch'] = 'NO';
	// 		$result['data'] = array();
    //
	// 	}
    //
	// 	return $result;
	// }

	public function closeOrderKitting($post = array()){
		$result = array();

		$sql = 'UPDATE
					order_kitting
				SET
					id_status_order_kitting=\'2\'
				WHERE
					id_order_kitting=\''.$post['id_order_kitting'].'\'';

		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Close OrderKitting Document Success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Close OrderKitting Document Failed';
		}

		return $result;
	}

	public function finishTallyOrderKitting($post = array()){
		$result = array();

		$this->db->select('status_id, st_repack');
		$this->db->where('id', $post['id_order_kitting']);

		$OrderKitting = $this->db->get('repack_orders')->row_array();

		if(count($OrderKitting) > 0 && $OrderKitting['status_id'] == 5 && $OrderKitting['st_repack'] == 0){

			$this->db
				->set('st_repack',1)
				->where('id', $post['id_order_kitting']);

			$q = $this->db->update('repack_orders');

			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Finish Tally Order Kitting Document Success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Finish Tally Order Kitting Document Failed';
			}
		}else{

			$result['status'] = 'ERR';
			$result['message'] = 'Finish Tally Order Kitting Document Failed';

		}

		return $result;
	}

	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
			4 => 'jumlah_kitting'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['id_order_kitting'])){
			$this->db->where('ro.id', $post['id_order_kitting']);
		}

		$this->db
			->from('repack_orders ro')
			->join('repacks_repack_orders rro','rro.repack_order_id=ro.id')
			->join('repacks r','r.id=rro.repack_id')
			->join('items itm','itm.id=r.item_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select('rro.id, itm.code as kd_barang, itm.name as nama_barang, rro.qty as jumlah_kitting')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_kitting'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItemPicked($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
			4 => 'jumlah_kitting'
		);

		$where = '';


		$this->db->start_cache();

		if(!empty($post['id_order_kitting'])){
			$this->db->where('ro.id', $post['id_order_kitting']);
		}

		$this->db
			->from('repack_orders ro')
			->join('repacks_repack_orders rro','ro.id=rro.repack_order_id')
			->join('repacks r','r.id=rro.repack_id')
			->join('repack_items ri','ri.repack_id=r.id')
			->join('items itm','itm.id=ri.item_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select('rro.id, itm.code as kd_barang, itm.name as nama_barang, sum((ri.qty * rro.qty)) as jumlah_kitting')
			->group_by('itm.id, rro.id')
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_kitting'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItemPickedDetails($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'rop.id',
			1 => '',
			2 => 'kd_barang',
			3 => 'kd_unik',
			4 => 'qty'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['id_order_kitting'])){
			$this->db->where('ro.id', $post['id_order_kitting']);
		}

		$this->db
			->from('repack_orders ro')
			->join('repack_order_picking rop','rop.repack_order_id=ro.id')
			->join('items itm','itm.id=rop.item_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select(["rop.id as id_order_kitting_sn","CONCAT(itm.code, ' - ', itm.name) as nama_barang","unique_code as kd_unik","rop.qty"])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id_order_kitting_sn'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['qty'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getTallyDetails($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_receiving_barang',
			1 => '',
			2 => 'kd_barang',
			3 => 'kd_unik',
			4 => 'first_qty',
			5 => 'loc_name'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['id_order_kitting'])){
			$this->db->where('ro.id', $post['id_order_kitting']);
		}

		$this->db
			->from('repack_orders ro')
			->join('item_receiving_details ird','ird.repack_order_id=ro.id')
			->join('items itm','itm.id=ird.item_id')
			->join('locations loc','loc.id=ird.location_id');

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select(["ird.id as id_receiving_barang","CONCAT(itm.code, ' - ', itm.name) as nama_barang","unique_code as kd_unik","first_qty","loc.name as loc_name"])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id_receiving_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['first_qty'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function checkStatusPicking($id){
		$result = array();

		$sql = 'SELECT
					IFNULL((
						SELECT
							SUM(jumlah_kitting)
						FROM
							order_kitting_barang
						WHERE
							id_order_kitting=\''.$id.'\'
					), 0) AS total_document,
					(
						SELECT
							COUNT(*)
						FROM
							picking_list_order_kitting plo
						JOIN picking_qty pq
							ON pq.pl_id=plo.pl_id
						JOIN receiving_barang rcvbrg
							ON rcvbrg.id_barang=pq.id_barang
								AND rcvbrg.pl_id=pq.pl_id
						WHERE
							plo.id_order_kitting=\''.$id.'\'
					) AS total_scanned';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$this->db
			->select([
				"ro.id as id_order_kitting", "ro.code as kd_order_kitting", "TO_CHAR(ro.date, 'DD/MM/YYYY') AS tanggal_order_kitting", "st.name as status_order_kitting", "usr.nama", "st_repack"
			])
			->from('repack_orders ro')
			->join('users usr','usr.id=ro.user_id','left')
			->join('status st','st.status_id=ro.status_id','left')
			->where('ro.id', $post['id_order_kitting']);

		$result = $this->db->get()->row_array();

		return $result;
	}

    public function get_data($id) {

		$this->db
			->select('ro.id as id_order_kitting, ro.code as kd_order_kitting, ro.date as tanggal_order_kitting, ro.type as order_kitting_type_id, ro.warehouse_id')
			->from($this->table." ro")
			->where('id', $id);

        return $this->db->get();
    }

    public function add_ok_code(){
        $this->db->set('inc_ok',"inc_ok+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

	public function getPickingList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'pl_id',
			1 => '',
			2 => 'pl_name',
			3 => 'kd_order_kitting',
			4 => 'pl_date',
			5 => 'customer_name',
			6 => 'remark',
			7 => 'pl_start',
			8 => 'pl_finish',
			9 => 'nama_status'
		);

		$where = '';

		if(!empty($post['pl_id']))
			$where .= sprintf(' AND pick.pl_id = \'%s\' ', $post['pl_id']);

		if(isset($post['id_order_kitting'])){
			if(!empty($post['id_order_kitting']))
				$where .= sprintf(' AND outb.id_order_kitting = \'%s\' ', $post['id_order_kitting']);
		}

		if(!empty($post['id_customer']))
			$where .= sprintf(' AND pick.id_customer = \'%s\' ', $post['id_customer']);

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(pick.pl_date) BETWEEN \'%s\' AND \'%s\' ',
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}

		if(!empty($post['status']))
			$where .= sprintf(' AND pick.id_status_picking = \'%s\' ', $post['status']);

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						pick.pl_id
					FROM
						picking_list pick
					JOIN picking_list_order_kitting plo
						ON plo.pl_id=pick.pl_id
					JOIN order_kitting outb
						ON outb.id_order_kitting=plo.id_order_kitting
					LEFT OUTER JOIN supplier spl
						ON spl.id_supplier=outb.id_customer
							AND outb.id_order_kitting_document = \'2\'
					LEFT OUTER JOIN m_customer cust
						ON cust.id_customer=outb.id_customer
							AND outb.id_order_kitting_document != \'2\'
					LEFT JOIN m_status_picking mstat
						ON mstat.id_status_picking=pick.id_status_picking
					WHERE 1=1'.$where.'
					GROUP BY
						pick.pl_id
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					pick.pl_id, pl_name, outb.id_order_kitting, kd_order_kitting, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS pl_date,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					IFNULL(remark, \'-\') AS remark,
					IFNULL(DATE_FORMAT(pl_start, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS pl_start,
					IFNULL(DATE_FORMAT(pl_finish, \'%H:%i<br><span class="small-date">%d\/%m\/%Y<span>\'), \'-\') AS pl_finish,
					IFNULL(DATE_FORMAT(shipping_start, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS shipping_start,
					IFNULL(DATE_FORMAT(shipping_finish, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS shipping_finish,
					pick.id_status_picking,
					(
						SELECT
							nama_status
						FROM
							m_status_picking
						WHERE
							id_status_picking = (
								CASE
									WHEN pl_start IS NULL
										THEN \'1\'
									WHEN pl_start IS NOT NULL AND pl_finish IS NULL
										THEN \'2\'
									WHEN pl_start IS NOT NULL AND pl_finish IS NOT NULL
										THEN \'3\'
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NULL
										THEN \'4\'
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NOT NULL
										THEN \'5\'
								END
							)
					) AS nama_status,
					(
						SELECT
							COUNT(*)
						FROM
							receiving_barang rcvbrg
						JOIN picking_list pl
							ON pl.pl_id=rcvbrg.pl_id
						JOIN picking_list_order_kitting plo
							ON plo.pl_id=pl.pl_id
						WHERE
							plo.id_order_kitting=outb.id_order_kitting
					) AS total_picked
				FROM
					picking_list pick
				JOIN picking_list_order_kitting plo
					ON plo.pl_id=pick.pl_id
				JOIN order_kitting outb
					ON outb.id_order_kitting=plo.id_order_kitting
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_order_kitting_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_order_kitting_document != \'2\'
				LEFT JOIN m_status_picking mstat
					ON mstat.id_status_picking=pick.id_status_picking
				LEFT JOIN shipping_picking shippick
					ON shippick.pl_id=pick.pl_id
				LEFT JOIN shipping shipp
					ON shipp.shipping_id=shippick.shipping_id
				WHERE 1=1'.$where.'
				GROUP BY
					pick.pl_id';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';
			if($row[$i]['pl_start'] != '-'){
				$class = 'disabled-link';
			}

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-print"></i> Print Picking List</a>';
				$action .= '</li>';
            }
			/*
			$class = 'disabled-link';
			if($row[$i]['id_status_picking'] == '2'){

				if($row[$i]['total_picked'] > 0){
					$class = '';
				}

			}

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-check"></i> Finish Picking</a>';
			$action .= '</li>';
			*/
			$class = '';
			if($row[$i]['pl_start'] != '-'){
				$class = 'disabled-link';
			}

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'picking_list/detail/'.$row[$i]['pl_id'].'">'.$row[$i]['pl_name'].'</a>';
			$nested[] = '<a href="'.base_url().'order_kitting/detail/'.$row[$i]['id_order_kitting'].'">'.$row[$i]['kd_order_kitting'].'</a>';
			$nested[] = $row[$i]['pl_date'];
			$nested[] = $row[$i]['customer_name'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['pl_start'];
			$nested[] = $row[$i]['pl_finish'];
			$nested[] = $row[$i]['nama_status'];

			if($post['params'] != 'no_action')
				$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getShippingList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'shipping_id',
			1 => '',
			2 => 'shipping_code',
			3 => 'shipping_date',
			4 => 'driver_name',
			5 => 'license_plate',
			6 => 'shipping_start',
			7 => 'shipping_finish',
			8 => 'st_shipping_name'
		);

		$where = '';

		if(isset($post['id_order_kitting'])){
			if(!empty($post['id_order_kitting']))
				$where .= sprintf(' AND outb.id_order_kitting = \'%s\' ', $post['id_order_kitting']);
		}

		if(!empty($post['id_shipping']))
			$where .= sprintf(' AND shipp.shipping_id = \'%s\' ', $post['id_shipping']);

		if(!empty($post['pl_id']))
			$where .= sprintf(' AND pick.pl_id = \'%s\' ', $post['pl_id']);

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND shipp.shipping_date BETWEEN \'%s\' AND \'%s\' ',
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}

		if($post['status'] != '' && $post['status'] != null)
			$where .= sprintf(' AND shipp.st_shipping = \'%s\' ', $post['status']);

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						shipp.shipping_id
					FROM
						shipping shipp
					JOIN shipping_picking shipppick
						ON shipppick.shipping_id=shipp.shipping_id
					JOIN picking_list pick
						ON pick.pl_id=shipppick.pl_id
					JOIN picking_list_order_kitting plo
						ON plo.pl_id=pick.pl_id
					JOIN order_kitting outb
						ON outb.id_order_kitting=plo.id_order_kitting
					LEFT OUTER JOIN supplier spl
						ON spl.id_supplier=outb.id_customer
							AND outb.id_order_kitting_document = \'2\'
					LEFT OUTER JOIN m_customer cust
						ON cust.id_customer=outb.id_customer
							AND outb.id_order_kitting_document != \'2\'
					LEFT JOIN m_shipping sstat
						ON sstat.st_shipping=shipp.st_shipping
					LEFT JOIN shipping_delivery_order sdo
						ON sdo.shipping_id=shipp.shipping_id
					WHERE 1=1'.$where.'
					GROUP BY
						shipp.shipping_id
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					shipp.shipping_id, shipping_code, DATE_FORMAT(shipping_date, \'%d\/%m\/%Y\') AS shipping_date,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					driver_name, license_plate,
					IFNULL(DATE_FORMAT(shipping_start, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_start,
					IFNULL(DATE_FORMAT(shipping_finish, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_finish,
					shipp.st_shipping, st_shipping_name AS status, do_number
				FROM
					shipping shipp
				JOIN shipping_picking shipppick
					ON shipppick.shipping_id=shipp.shipping_id
				JOIN picking_list pick
					ON pick.pl_id=shipppick.pl_id
				JOIN picking_list_order_kitting plo
					ON plo.pl_id=pick.pl_id
				JOIN order_kitting outb
					ON outb.id_order_kitting=plo.id_order_kitting
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_order_kitting_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_order_kitting_document != \'2\'
				LEFT JOIN m_shipping sstat
					ON sstat.st_shipping=shipp.st_shipping
				LEFT JOIN shipping_delivery_order sdo
					ON sdo.shipping_id=shipp.shipping_id
				WHERE 1=1'.$where.'
				GROUP BY
					shipp.shipping_id';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';
			$class1 = '';

			if($row[$i]['st_shipping'] == '1')
				$class = 'disabled-link';

			if($row[$i]['shipping_finish'] == '-')
				$class1 = 'disabled-link';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-print"></i> Print Manifest</a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-print-do '.$class1.'" data-id="'.$row[$i]['shipping_id'].'" data-do="'.$row[$i]['do_number'].'"><i class="fa fa-print"></i> Print Delivery Order</a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-print-plate" data-id="'.$row[$i]['shipping_id'].'" data-plate="'.$row[$i]['license_plate'].'"><i class="fa fa-print"></i> Print License Plate</a>';
				$action .= '</li>';
            }

			if ($this->access_right->otoritas('edit')) {

				$class = 'disabled-link';
				if($row[$i]['shipping_start'] != '-' && $row[$i]['st_shipping'] != '1')
					$class = '';

				// $action .= '<li>';
				// $action .= '<a class="data-table-close '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-check"></i> Finish Loading</a>';
				// $action .= '</li>';

				$class = '';
				if($row[$i]['st_shipping'] == '1')
					$class = 'disabled-link';

				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();

			$start = $row[$i]['shipping_start'];
			$finish = $row[$i]['shipping_finish'];

			if($start != '-'){
				$start = explode(' ', $start);
				$start = $start[1] . '<br>' . '<span class="small-date">' . $start[0] . '</span>';
			}

			if($finish != '-'){
				$finish = explode(' ', $finish);
				$finish = $finish[1] . '<br>' . '<span class="small-date">' . $finish[0] . '</span>';
			}

			$nested[] = $row[$i]['shipping_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'loading/detail/'.$row[$i]['shipping_id'].'">'.$row[$i]['shipping_code'].'</a>';
			$nested[] = $row[$i]['shipping_date'];
			$nested[] = $row[$i]['customer_name'];
			$nested[] = $row[$i]['driver_name'];
			$nested[] = $row[$i]['license_plate'];
			$nested[] = $start;
			$nested[] = $finish;
			$nested[] = $row[$i]['status'];

			if($post['params'] != 'no_action')
				$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function checkExistDoNumber($post = array()){
		$result = array();

		$this->db
			->select('count(*) as total')
			->from('repack_orders')
			->where('code', $post['kd_order_kitting']);

		if(!empty($post['id_order_kitting'])){
			$this->db->where('id <>', $post['id_order_kitting']);
		}

		$row = $this->db->get()->row_array();

		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Doc. number already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}

		return $result;
	}

	public function get_do_code($post = array()){
		$result = '';

		$sql = 'SELECT
					CONCAT(pre_code_do, \'\', inc_do) AS doc_number
				FROM
					m_increment
				WHERE
					id_inc = \'1\'';

		$row = $this->db->query($sql)->row_array();
		$result = $row['doc_number'];

		return $result;
	}

	public function cek_not_usage_do($id){
		// $sql = 'SELECT
					// id_order_kitting
				// FROM
					// picking_list_order_kitting plo
				// JOIN picking_list pl
					// ON pl.pl_id=plo.pl_id
				// WHERE
					// id_order_kitting=\''.$id.'\'';

		// $q = $this->db->query($sql);

        // if($q->num_rows() > 0){
            // return FALSE;
        // }else{
            // return TRUE;
        // }


		$r = $this->checkStatusPicking($id);
		if($r['total_scanned'] > 0)
			return FALSE;
		else
			return TRUE;
    }

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_order_kitting',
			1 => '',
			2 => 'kd_order_kitting',
			3 => 'tanggal_order_kitting',
			4 => 'status_order_kitting'
		);

		$where = '';

		$this->db->start_cache();

		if(!empty($post['do_number'])){
			$this->db->where('ro.id', $post['do_number']);
		}

		if(!empty($post['from']) && !empty($post['to'])){
			$this->db->where('ro.date >=', DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'));
			$this->db->where('ro.date <=', DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'));
		}

		if(!empty($post['doc_status'])){
			$this->db->where('ro.status_id', $post['doc_status']);
		}

		if(!empty($post['warehouse'])){
			$this->db->where_in('ro.warehouse_id', $post['warehouse']);
		}

		$this->db
			->from('repack_orders ro')
			->join('warehouses wh','wh.id=ro.warehouse_id')
			->join('users_warehouses uw','uw.warehouse_id=wh.id')
			->join('users usr','usr.id=uw.user_id AND usr.id='.$this->session->userdata('user_id'))
			->join('status st','st.status_id=ro.status_id','left')
			->where('ro.status_id <>', 100);

		$this->db->stop_cache();

		$this->db->select('count(*) as total');

		$row = $this->db->get()->row_array();
		$totalData = $row['total'];

		$this->db
			->select([
				"ro.id as id_order_kitting", "ro.code as kd_order_kitting", "TO_CHAR(ro.date, 'DD/MM/YYYY') as tanggal_order_kitting", "st.name as status_order_kitting", "st_repack", "ro.status_id as status"
			])
			->order_by($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'])
			->limit($requestData['length'],$requestData['start']);

		$row = $this->db->get()->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			$class = '';
			$class2 = '';

			if($row[$i]['status'] > 3 ){
				$class = 'disabled-link';
			}

			if($row[$i]['status'] == 4 || $row[$i]['st_repack'] != 0){
				$class2 = 'disabled-link';
			}

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-finish '.$class2.'" data-id="'.$row[$i]['id_order_kitting'].'"><i class="fa fa-check"></i> Finish Repack Tally </a>';
				$action .= '</li>';

			}

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['id_order_kitting'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['id_order_kitting'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['id_order_kitting'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().$post['className'].'/detail/'.$row[$i]['id_order_kitting'].'">'.$row[$i]['kd_order_kitting'].'</a>';
			$nested[] = $row[$i]['tanggal_order_kitting'];
			$nested[] = ucwords($row[$i]['status_order_kitting']);
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getOrderKittingCode($post = array()){
		$result = array();

		$this->db
			->select('id as id_order_kitting, code as kd_order_kitting')
			->from('repack_orders')
			->like('code', $post['query'])
			->where('status_id <>', 100);

		$row = $this->db->get()->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getCustomerBySearch($post = array()){
		$result = array();

		$sql = 'SELECT
					id_customer, customer_name
				FROM
					m_customer
				WHERE
					customer_name LIKE \'%'.$post['query'].'%\'';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

    public function get_order_kitting_barang($id_order_kitting) {
		$sql = 'SELECT
					*
				FROM
					order_kitting_barang outb
				JOIN m_kitting kit
					ON kit.id_kitting=outb.id_kitting
				WHERE
					id_order_kitting=\''.$id_order_kitting.'\'
				ORDER BY
					outb.id ASC';

		$this->db
			->select('r.id as id_kitting, rro.qty as jumlah_kitting')
			->from('repacks_repack_orders rro')
			->join('repacks r','r.id=rro.repack_id')
			->where('rro.repack_order_id',$id_order_kitting)
			->order_by('rro.id','ASC');

		return $this->db->get();
    }

    public function data_insert($data){

    	$data = array(
    		'code'	=> $data['kd_order_kitting'],
    		'date'	=> $data['tanggal_order_kitting'],
    		'type'	=> $data['order_kitting_type_id'],
    		'user_id' => $data['user_id'],
    		'warehouse_id' => $data['warehouse_id']
    	);

    	return $data;
    }

    public function data2_insert($data2){

    	$newData = array();

    	foreach ($data2 as $d) {

    		$newData[] = array(
    			'repack_id' => $d['id_kitting'],
    			'qty'		=> $d['jumlah_kitting']
    		);

	    }

    	return $newData;
    }

    public function create($data, $data2) {

    	$this->db->trans_start();

    	$data = $this->data_insert($data);
    	$data['status_id'] = 3;
        $this->db->insert($this->table, $data);
        $id_order_kitting = $this->db->insert_id();

        if(!empty($data2)) {

        	$data2 = $this->data2_insert($data2);
            foreach ($data2 as &$order_kitting_barang) {
                $order_kitting_barang['repack_order_id'] = $id_order_kitting;
            }

            $this->db->insert_batch($this->table2, $data2);

            foreach($data2 as $d){
            	$idKitting[] = $d['repack_id'];
            }

            $idKit = implode(',', $idKitting);

            $sql = "
            	SELECT * FROM (
            		(
            			SELECT id as id_kitting, item_id, '0' as qty FROM repacks WHERE id IN ($idKit)
            		)UNION(
            			SELECT repack_id as id_kitting, item_id, qty FROM repack_items WHERE repack_id IN ($idKit)
            		)
            	)as tbl
            	ORDER BY id_kitting, item_id
            ";

            $mkp = $this->db->query($sql)->result_array();

            foreach($mkp as &$m){

	            $kit = array_search($m['id_kitting'], $this->arrayColumn($data2, 'repack_id'));

            	$m['repack_order_id'] = $id_order_kitting;

            	if($data['type'] == 1){

	            	if($m['qty'] > 0){
		            	$m['picking_doc_qty'] = ($m['qty'] * $data2[$kit]['qty']);
	            	}else{
		            	$m['tally_doc_qty'] = $data2[$kit]['qty'];
	            	}

	            }else{

	            	if($m['qty'] > 0){
		            	$m['tally_doc_qty'] = ($m['qty'] * $data2[$kit]['qty']);
	            	}else{
		            	$m['picking_doc_qty'] = $data2[$kit]['qty'];
	            	}

	            }

            	unset($m['id_kitting']);
            	unset($m['qty']);

	            $this->db->insert('repack_order_details', $m);

            }

        }

        $this->db->trans_complete();
    }

	private function arrayColumn($input, $column_key, $index_key = null)
	{
	    if (func_num_args() < 2) {
	        trigger_error('array_column() expects at least 2 parameters, '.func_num_args().' given', E_USER_WARNING);
	        return null;
	    } elseif (!is_array($input)) {
	        trigger_error('array_column() expects parameter 1 to be array, '.gettype($input).' given', E_USER_WARNING);
	        return null;
	    }
	    if ($index_key) {
	        return array_combine(
	            arrayColumn($input, $index_key),
	            arrayColumn($input, $column_key)
	        );
	    }
	    // If an array of objects is provided, then public properties can be directly pulled.
	    // In order for protected or private properties to be pulled, the class must implement both the
	    // __get() and __isset() magic methods.
	    foreach ($input as $key => $val) {
	        if (is_object($val) && !in_array($column_key, get_object_vars($val)) &&
	            (!method_exists($val, '__get') || !method_exists($val, '__isset'))) {
	            unset($input[$key]);
	        }
	    }
	    
	    $input_count = count($input);
	    return array_map(
	        function ($element, $column_name) {
	            if (is_object($element)) {
	                return $element->{$column_name};
	            }
	            return $element[$column_name];
	        },
	        $input,
	        $input_count ? array_fill(0, $input_count, $column_key) : array()
	    );
	}

    public function create_customer($data) {
        $this->db->insert($this->table4, $data);
    }

    public function update($data, $data2, $id) {

    	$this->db->trans_start();

    	$data = $this->data_insert($data);
		$this->db->update($this->table, $data, array('id' => $id));

		//remove all order_kitting_barang which the id_order_kitting is $id
		$this->db->where("repack_order_id", $id);
		$this->db->delete($this->table2);

		//add all new order_kitting_barang
		if(!empty($data2)) {

			$data2 = $this->data2_insert($data2);
			foreach ($data2 as &$order_kitting_barang) {
				$order_kitting_barang['repack_order_id'] = $id;
			}

			$this->db->insert_batch($this->table2, $data2);

            foreach($data2 as $d){
            	$idKitting[] = $d['repack_id'];
            }

            $idKit = implode(',', $idKitting);

            $sql = "
            	SELECT * FROM (
            		(
            			SELECT id as id_kitting, item_id, '0' as qty FROM repacks WHERE id IN ($idKit)
            		)UNION(
            			SELECT repack_id as id_kitting, item_id, qty FROM repack_items WHERE repack_id IN ($idKit)
            		)
            	)as tbl
            	ORDER BY id_kitting, item_id
            ";

            $mkp = $this->db->query($sql)->result_array();

            $this->db->delete('repack_order_details', array('repack_order_id'=>$id));

            foreach($mkp as &$m){

	            $kit = array_search($m['id_kitting'], $this->arrayColumn($data2, 'repack_id'));

            	$m['repack_order_id'] = $id;

            	if($data['type'] == 1){

	            	if($m['qty'] > 0){
		            	$m['picking_doc_qty'] = ($m['qty'] * $data2[$kit]['qty']);
	            	}else{
		            	$m['tally_doc_qty'] = $data2[$kit]['qty'];
	            	}

	            }else{

	            	if($m['qty'] > 0){
		            	$m['tally_doc_qty'] = ($m['qty'] * $data2[$kit]['qty']);
	            	}else{
		            	$m['picking_doc_qty'] = $data2[$kit]['qty'];
	            	}

	            }

            	unset($m['id_kitting']);
            	unset($m['qty']);

	            $this->db->insert('repack_order_details', $m);

            }

		}

		$this->db->trans_complete();
    }

	public function delete($id){
		$result = array();

		$this->db->select('status_id, st_repack');
		$this->db->where('id', $id);

		$OrderKitting = $this->db->get('repack_orders')->row_array();

		if(count($OrderKitting) > 0 && ( $OrderKitting['status_id'] == 3 || $OrderKitting['st_repack'] == 0) ){

			$this->db->trans_start();

			$this->db->delete($this->table, array('id' => $id));
			$this->db->delete($this->table2, array('repack_order_id' => $id));
			$this->db->delete('repack_order_details', array('repack_order_id' => $id));

			$this->db->trans_complete();

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';

		}else{

			$result['status'] = 'ERR';
			$result['message'] = 'Delete data Failed';

		}

        return $result;

	}

    public function delete1($id) {
        return $this->db->delete($this->table, array('id_order_kitting' => $id));
    }

    public function options($default = '--Pilih Kode Barang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->nama_barang ;
        }
        return $options;
    }

    public function create_option_kitting($default = '-- Choose Item --', $key = ''){
    	$this->db
    		->select('r.id, itm.name, itm.code')
    		->from('repacks r')
    		->join('items itm','r.item_id=itm.id','left');

        $data = $this->db->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id] = $row->code .' - '. $row->name;
        }

        return $options;
    }

    public function getDetailItemKitting($post = array()){

		$result = array();
		$sql = "select *,total as qty_doc, total as qty_received from v_pick_kitting where id_order_kitting = '".$post['id_order_kitting']."'";

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['message'] = '';

		return $result;
	}

	public function getProduct($post = array()){
		$result = array();
		

		$sql = "SELECT
					b.kd_barang,s.nama_satuan,jumlah_kitting,nama_barang
				FROM
					order_kitting ok
				LEFT JOIN order_kitting_barang okb ON ok.id_order_kitting = okb.id_order_kitting
				LEFT JOIN m_kitting k ON k.id_kitting = okb.id_kitting
				LEFT JOIN barang b ON b.id_barang = k.id_barang
				LEFT JOIN satuan s ON s.id_satuan = b.id_satuan
				where ok.id_order_kitting = '".$post['id_order_kitting']."'";

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['message'] = '';
		return $result;
	}

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if(!empty($empty_value_text)) {
            $options[''] = $empty_value_text;
        }

        foreach ($data as $dt) {

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }

    public function validation_items_qty($data=array(),$data2=array()){

    	$result = array(
    		'status' => true
    	);

    	if(!empty($data2)){

            foreach($data2 as $d){
            	$idKitting[] = $d['id_kitting'];
            }

        	if($data['order_kitting_type_id'] == 1){

        		$this->db
        			->select(array('ri.repack_id as id_kitting','itm.id as id_barang',"CONCAT(itm.code, ' - ', itm.name) as nama_barang",'qty','unt.name as nama_satuan'))
        			->from('repack_items ri')
        			->join('items itm','itm.id=ri.item_id','left')
        			->join('units unt','unt.id=itm.unit_id','left')
        			->where_in('ri.repack_id',$idKitting)
        			->group_by('itm.id, ri.repack_id, ri.qty, unt.name, itm.code, itm.name');

        	}else{

        		$this->db
        			->select(array('r.id as id_kitting',"itm.id as id_barang","CONCAT(itm.code, ' - ', itm.name) as nama_barang","COALESCE(NULL,'1') as qty","unt.name as nama_satuan"))
        			->from('repacks r')
        			->join('items itm','itm.id=r.item_id','left')
        			->join('units unt','unt.id=itm.unit_id','left')
        			->where_in('r.id',$idKitting)
        			->group_by('itm.id, r.id, unt.name, itm.code');

        	}

        	$items = $this->db->get()->result_array();

        	$itm=array();

            foreach($items as $i){
            	$itm[] = $i['id_barang'];
            }

	    	$this->db
	    		->select(array('itm.id as id_barang','coalesce(sum(last_qty),0) as qty'))
	    		->from('items itm')
		    	->join('item_receiving ir','ir.item_id=itm.id','left')
		    	->join('item_receiving_details ird','ird.item_receiving_id=ir.id','left')
		    	->where_in('itm.id', $itm)
		    	->where_not_in('ird.location_id',array('100','101','102','103','104','105','106'))
		    	->where('ird.last_qty >', 0)
		    	->group_by('itm.id');

        	$itemsQty = $this->db->get()->result_array();

			$idBarang = $this->arrayColumn($itemsQty, 'id_barang');

			foreach($items as $i){
	            $key = array_search($i['id_barang'], $idBarang);

	            if($key !== FALSE){

		            $keyKitting = array_search($i['id_kitting'], $this->arrayColumn($data2, 'id_kitting'));
		            $need_qty = ($data2[$keyKitting]['jumlah_kitting'] * $i['qty']);
		            $avail_qty = $itemsQty[$key]['qty'];

	            	if($avail_qty >= $need_qty){

	            		continue;

	            	}else{

				    	$result = array(
				    		'status' => false,
				    		'message' => "Stock ".$i['nama_barang']." Kurang ".( $need_qty - $avail_qty )." ".$i['nama_satuan']
				    	);

				    	return $result;

	            	}

	            }else{

			    	$result = array(
			    		'status' => false,
			    		'message' => "Stock ".$i['nama_barang']." Kosong"
			    	);

			    	return $result;
	            }

			}

			return $result;
		}
    }

    public function typeOptions($default = '-- Choose Type --', $key = ''){
        $data = $this->db->get('repack_order_types');
        $options = array();

        foreach ($data->result() as $row) {
            $options[$row->id] = $row->name ;
        }
        return $options;
    }

}

?>

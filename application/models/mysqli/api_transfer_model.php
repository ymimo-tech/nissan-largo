<?php

class Api_transfer_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($serial_number){
        $ignore_location = array(100, 101, 102, 103, 104, 107, 108, 109);
        $ignore_location = array(100, 101, 102, 104, 107, 108, 109);
        $this->db->select("loc_id");
        $this->db->from("receiving_barang");
        $this->db->where("kd_unik", $serial_number);
        $this->db->where_not_in("loc_id", $ignore_location);
        $this->db->where_not_in("id_qc", 4);
        return $this->db->get();
    }

    function get_pl($serial_number){
        $ignore_location = array(100, 101, 102, 103, 104, 107, 108, 109);
        $ignore_location = array(100, 101, 102, 104, 107, 108, 109);
        $this->db->select("loc_id");
        $this->db->from("receiving_barang");
        $this->db->where("kd_parent", $serial_number);
        $this->db->where_not_in("loc_id", $ignore_location);
        $this->db->where_not_in("id_qc", 4);
        return $this->db->get();
    }

    function set($data){
        $id_user = $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
        $data_insert = array(   'kd_unik'       => $data["kd_unik"], 
                                'loc_id_old'    => $data["loc_id_old"],
                                'user_id_pick'  => $id_user["user_id"],
                                'pick_time'     => $data["time"],
								'process_name'	=> 'TRANSFER'
                                );
        $this->db->insert("int_transfer_detail", $data_insert);

        $ignore_location = array(100, 101, 102, 103, 104, 107, 108, 109);
        $ignore_location = array(100, 101, 102, 104, 107, 108, 109);
        $this->db->set("loc_id", 103);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->where_not_in("loc_id", $ignore_location);
        $this->db->where_not_in("id_qc", 4);
        $this->db->update("receiving_barang");
    }

    function set_pl($data){
        $id_user = $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
        $data_insert = array(   'kd_unik'       => $data["kd_unik"], 
                                'loc_id_old'    => $data["loc_id_old"],
                                'user_id_pick'  => $id_user["user_id"],
                                'pick_time'     => $data["time"],
                                'process_name'  => 'TRANSFER'
                                );
        $this->db->insert("int_transfer_detail", $data_insert);

        $ignore_location = array(100, 101, 102, 103, 104, 107, 108, 109);
        $ignore_location = array(100, 101, 102, 104, 107, 108, 109);
        $this->db->set("loc_id", 103);
        $this->db->where("kd_parent", $data["kd_unik"]);
        $this->db->where_not_in("loc_id", $ignore_location);
        $this->db->where_not_in("id_qc", 4);
        $this->db->update("receiving_barang");
    }

    function retrieve($serial_number){
    	$this->db->select("kd_unik, loc_name");
    	$this->db->from("receiving_barang rcv");
    	$this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
    	$this->db->where("kd_unik", $serial_number);
    	return $this->db->get();
    }

    function retrieve_pl($serial_number){
        $this->db->select("kd_parent as kd_unik, loc_name");
        $this->db->from("receiving_barang rcv");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->group_by('kd_parent');
        $this->db->where("kd_parent", $serial_number);
        return $this->db->get();
    }

    function post($data){
    	$id_loc_old 	= $this->get_id("loc_id", "loc_name", $data["loc_name_old"], "m_loc")->row_array();
    	$id_user_old 	= $this->get_id("user_id", "user_name", $data["user_pick"], "hr_user")->row_array();
    	$id_loc_new		= $this->get_id("loc_id", "loc_name", $data["loc_name_new"], "m_loc")->row_array();
    	$id_user_new 	= $this->get_id("user_id", "user_name", $data["user_put"], "hr_user")->row_array();
    	
        $id_receiving   = $this->get_id("id_receiving", "kd_unik", $data["kd_unik"], "receiving_barang");
        if($id_receiving->num_rows()>0){
            $st_single = TRUE;
        }else{
            $st_single = FALSE;
        }
        $this->db->set("loc_id", $id_loc_new["loc_id"]);
        if($st_single){
    	   $this->db->where("kd_unik", $data["kd_unik"]);
        }else{
           $this->db->where("kd_parent", $data["kd_unik"]);
        }
    	$this->db->update("receiving_barang");

        $this->db->set("loc_id_new", $id_loc_new["loc_id"]);
        $this->db->set("put_time", $data["put_time"]);
        $this->db->set("user_id_put", $id_user_new["user_id"]);
		$this->db->set("process_name", "TRANSFER");
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->where("loc_id_old", $id_loc_old["loc_id"]);
        $this->db->where("user_id_pick", $id_user_old["user_id"]);
        $this->db->update("int_transfer_detail");
    }

    function cancel($data){
        $id_loc         = $this->get_id("loc_id", "loc_name", $data["loc_name_old"], "m_loc")->row_array();
        $id_user        = $this->get_id("user_id", "user_name", $data["user_pick"], "hr_user")->row_array();
        
        $this->db->set("loc_id", $id_loc["loc_id"]);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->update("receiving_barang");

        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->where("user_id_pick", $id_user["user_id"]);
        $this->db->where("loc_id_old", $id_loc["loc_id"]);
        $this->db->where("loc_id_new IS NULL");
        $this->db->where("user_id_put IS NULL");
        $this->db->delete("int_transfer_detail");
    }

    function get_location($location){
        $this->db->select("*");
        $this->db->from("m_loc");
        $this->db->where("loc_name", $location);
        $this->db->where_in("loc_type_id", array(1,2,3));
        return $this->db->get();
    }

}
<?php

class notifikasi_model extends CI_Model {

    function notifikasi_model() {
        parent::__construct();
    }
    private $table = 'hr_pegawai';
    private $table2 = 'hr_ref_jabatan';
    private $table3 = 'hr_ref_unit_kerja';
    private $table4 = 'hr_ref_cabang';
    private $table5 = 'hr_pegawai_detail';

    var $app = 'm_aplikasi';

    function notf_kenaikan_pangkat() {
        $apr = 'F';
        $unit_kerja = "";
        $grup_id = "";
        $approve = "";
        $apr_w = "";
        $nik_s = $this->session->userdata('nik_session');
        $kd_jabatan_s = $this->session->userdata('kd_jabatan');
        $kd_unit_kerja_s = $this->session->userdata('unit_kerja_ses');
        $kd_cabang = $this->session->userdata('cabang_user');
        $grup_id = $this->session->userdata('kd_roles');
        $q_gtu = $this->db->query("select kd_unit_kerja from hr_pegawai where nik='$nik_s'");
        foreach ($q_gtu->result_array() as $row) {
            $kd_unit_kerja_ss = $row['kd_unit_kerja'];
        }
        $apr_w = $grup_id;
        $approve = " AND  hr_pangkat.apr_w='$grup_id' ";
        $hasil = array();
        $query = $this->db->query("SELECT count(1) as jumlah
                FROM hr_pangkat
                LEFT JOIN  hr_pegawai ON hr_pangkat.nik = hr_pegawai.nik
                LEFT JOIN hr_ref_unit_kerja ON hr_pangkat.kd_unit_kerja = hr_ref_unit_kerja.kd_unit_kerja
                LEFT JOIN hr_ref_cabang ON hr_pangkat.kd_cabang = hr_ref_cabang.kd_cabang
                WHERE hr_pegawai.status_aktif = 'Aktif' AND length(hr_pegawai.kd_level) < 3  $approve
                ");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }

    function notf_mutasi() {
        $apr = 'F';
        $grup_id = "";
        $approve = "";
        $nik_s = $this->session->userdata('nik_session');
        $kd_jabatan_s = $this->session->userdata('kd_jabatan');
        $kd_unit_kerja_s = $this->session->userdata('unit_kerja_ses');
        $kd_cabang = $this->session->userdata('cabang_user');
        $grup_id = $this->session->userdata('kd_roles');
        $approve = "";
        $apr_w = "";
        $apr_w = $grup_id;
        $approve = " AND  hr_jabatan.apr_w='$grup_id' ";
        $hasil = array();
        $query = $this->db->query("SELECT count(1) as jumlah
                FROM hr_jabatan
                LEFT JOIN hr_pegawai ON hr_jabatan.nik = hr_pegawai.nik
                LEFT JOIN hr_ref_unit_kerja ON hr_jabatan.kd_unit_kerja = hr_ref_unit_kerja.kd_unit_kerja
                LEFT JOIN hr_ref_cabang ON hr_jabatan.kd_cabang = hr_ref_cabang.kd_cabang
                LEFT JOIN hr_ref_jabatan ON hr_jabatan.kd_jabatan = hr_ref_jabatan.kd_jabatan
                WHERE hr_pegawai.status_aktif = 'Aktif' AND length(hr_pegawai.kd_level) < 3   $approve
                ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }

    function count_prediksi_level() {
        $jumlah = 0;
        $this->load->model('model_tools/tools');
        $this->load->model('model_tools/utility');
        $this->tools->getMenu('master_pegawai_controller');
        $jum_rows = 0;
        if ($this->utility->cekAdd($this->session->userdata('user_id'), $this->session->userdata('id_menu'))) {
            $date = date('Y-m-t');
            $waktu_naik = 0;
            $query = $this->db->query("SELECT kenaikan_level FROM hr_ref_parameter")->result();
            foreach ($query as $data) {
                $waktu_naik = $data->kenaikan_level;
            }
            $waktu_naik = $waktu_naik * 365;
            $hasil = array();
            $query = $this->db->query("SELECT count(1) as jumlah										 
                                   FROM hr_pegawai 
                                   LEFT JOIN hr_ref_unit_kerja ON hr_pegawai.kd_unit_kerja = hr_ref_unit_kerja.kd_unit_kerja 
                                   LEFT JOIN hr_ref_cabang ON hr_pegawai.kd_cabang = hr_ref_cabang.kd_cabang  
                                   INNER JOIN (SELECT nik, tanggal_mulai_berlaku  
                                   FROM hr_pangkat 
                                   WHERE  '$date' - tanggal_mulai_berlaku > $waktu_naik
                                   INTERSECT (select nik, max(tanggal_mulai_berlaku) as tanggal_mulai_berlaku from hr_pangkat where (status = 'T' or status = 'F') GROUP BY nik) 
                                   order by nik)as q1 on hr_pegawai.nik = q1.nik
                                   WHERE hr_pegawai.status_aktif = 'Aktif' AND length(hr_pegawai.kd_level) < 3 AND hr_pegawai.kd_level != '0' AND hr_pegawai.kd_level != 'ho'
                                   AND hr_pegawai.nik NOT IN (SELECT nik from hr_hukuman where tgl_akhir_perhitungan >= '$date' AND status != 'R' )
                                   AND hr_pegawai.nik NOT IN (SELECT nik from hr_pangkat where status = 'F')");
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $data) {
                    $jumlah = $data->jumlah;
                }
            }
        }
        return $jumlah;
    }

    function count_prediksi_pensiun() {
        $jumlah = 0;
        $this->load->model('model_tools/tools');
        $this->load->model('model_tools/utility');
        $this->tools->getMenu('master_pegawai_controller');
        $jum_rows = 0;
        if ($this->utility->cekAdd($this->session->userdata('user_id'), $this->session->userdata('id_menu'))) {
            $waktu_naik = 0;
            $query = $this->db->query("SELECT usia_mpp FROM hr_ref_parameter")->result();
            foreach ($query as $data) {
                $waktu_naik = $data->usia_mpp;
            }
            $waktu_naik = $waktu_naik * 365;
            $date = date('Y-m-t');
            $hasil = array();
            $query = $this->db->query("SELECT COUNT(1) as jumlah
                FROM hr_pegawai
                LEFT JOIN hr_ref_unit_kerja ON hr_pegawai.kd_unit_kerja = hr_ref_unit_kerja.kd_unit_kerja
                LEFT JOIN hr_pegawai_detail ON hr_pegawai.nik = hr_pegawai_detail.nik
                LEFT JOIN hr_ref_cabang ON hr_pegawai.kd_cabang = hr_ref_cabang.kd_cabang
                LEFT JOIN hr_ref_jabatan ON hr_pegawai.kd_jabatan = hr_ref_jabatan.kd_jabatan
                WHERE  length(hr_pegawai.kd_level) < 3
                AND hr_pegawai.status_aktif = 'Aktif'
                AND hr_pegawai.nik NOT IN (SELECT distinct(hr_phk.nik) from hr_phk where status != 'R')
                AND ('$date' - hr_pegawai_detail.tgl_lahir) >= $waktu_naik");
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $data) {
                    $jumlah = $data->jumlah;
                }
            }
        }
        return $jumlah;
    }

    function notf_profil_pegawai() {
        $this->load->model('tools');
        $this->load->model('utility');
        $this->tools->getMenu('master_pegawai_controller');
        $jum_rows = 0;
        if ($this->utility->cekApprove($this->session->userdata('user_id'), $this->session->userdata('id_menu'))) {
            $kd_jabatan_1 = "";
            $kd_level_1 = "";
            $kd_cabang_1 = "";
            $kd_unit_kerja_1 = "";
            $nik = $this->session->userdata("nik_session");
            $qu_1 = $this->db->query("select kd_level,kd_jabatan,kd_cabang,kd_unit_kerja from hr_pegawai where nik='$nik'");
            foreach ($qu_1->result_array()as $row) {
                $kd_jabatan_1 = $row['kd_jabatan'];
                $kd_level_1 = $row['kd_level'];
                $kd_cabang_1 = $row['kd_cabang'];
                $kd_unit_kerja_1 = $row['kd_unit_kerja'];
            }
            $query = $this->db->query("select hr_pegawai.nik from hr_pegawai where hr_pegawai.apr = 'F' ");
            $jum_rows = $query->num_rows();
        }
        return $jum_rows;
    }

    function notf_penghargaan_pegawai() {
        $apr_w = "";
        $grup_id = "";
        $approve = "";
        $apr = 'F';
        $nik_s = $this->session->userdata('nik_session');
        $kd_jabatan_s = $this->session->userdata('kd_jabatan');
        $kd_unit_kerja_s = $this->session->userdata('unit_kerja_ses');
        $kd_cabang = $this->session->userdata('cabang_user');
        $grup_id = $this->session->userdata('kd_roles');
        $q_gtu = $this->db->query("select kd_unit_kerja from hr_pegawai where nik='$nik_s'");
        foreach ($q_gtu->result_array() as $row) {
            $kd_unit_kerja_ss = $row['kd_unit_kerja'];
        }
        $apr_w = "$grup_id";
        if (($grup_id == '1')) {
            $this->db->where('hr_penghargaan.nik', $nik_s);
        }
        if (($grup_id == '2')) {
            $this->db->where('hr_penghargaan.kd_unit_kerja', $kd_unit_kerja_ss);
        }
        if (($grup_id == '3')) {
            $this->db->where('hr_penghargaan.kd_cabang', $kd_cabang);
        }
        $hasil = array();
        $this->db->select('count(1) as jumlah');
        $this->db->from('hr_penghargaan');
        $this->db->join('hr_pegawai', 'hr_penghargaan.nik = hr_pegawai.nik', 'LEFT');
        $this->db->join('hr_ref_jabatan', 'hr_penghargaan.kd_jabatan = hr_ref_jabatan.kd_jabatan', 'LEFT');
        $this->db->join('hr_ref_unit_kerja', 'hr_penghargaan.kd_unit_kerja = hr_ref_unit_kerja.kd_unit_kerja', 'LEFT');
        $this->db->join('hr_ref_penghargaan', 'hr_penghargaan.kd_penghargaan = hr_ref_penghargaan.kd_penghargaan', 'LEFT');
        $this->db->join('hr_ref_cabang', 'hr_penghargaan.kd_cabang = hr_ref_cabang.kd_cabang', 'LEFT');
        //$this->db->where('hr_penghargaan.apr_w', $apr_w);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }

    function notf_hukuman_pegawai() {
        $nik = $this->input->post('select_nik');
        $apr_w = "";
        $kd_jabatan = "";
        $grup_id = "";
        $kd_jabatan = "";
        $login = $this->session->userdata('login');
        if ($login != 'TRUE') {
            redirect(base_url() . 'login_controller/login_controller/index');
        }
        $apr = 'F';
        $nik_s = $this->session->userdata('nik_session');
        $kd_jabatan_s = $this->session->userdata('kd_jabatan');
        $kd_unit_kerja_s = $this->session->userdata('unit_kerja_ses');
        $kd_cabang = $this->session->userdata('cabang_user');
        $grup_id = $this->session->userdata('kd_roles');
        $apr_w = "$grup_id";
        $q_gtu = $this->db->query("select kd_unit_kerja from hr_pegawai where nik='$nik_s'");
        foreach ($q_gtu->result_array() as $row) {
            $kd_unit_kerja_ss = $row['kd_unit_kerja'];
        }
        if (($grup_id == '1')) {
            $this->db->where('hr_hukuman.nik', $nik_s);
        }
        if (($grup_id == '2')) {
            $this->db->where('hr_pegawai.kd_unit_kerja', $kd_unit_kerja_ss);
        }
        if (($grup_id == '3')) {
            $this->db->where('hr_hukuman.kd_cabang', $kd_cabang);
        }
        $hasil = array();
        $this->db->select('count(1) as jumlah');
        $this->db->from('hr_hukuman');
        $this->db->join('hr_pegawai', 'hr_pegawai.nik = hr_hukuman.nik', 'LEFT');
        $this->db->join('hr_ref_jabatan', 'hr_pegawai.kd_jabatan = hr_ref_jabatan.kd_jabatan', 'LEFT');
        $this->db->join('hr_ref_unit_kerja', 'hr_pegawai.kd_unit_kerja = hr_ref_unit_kerja.kd_unit_kerja', 'LEFT');
        $this->db->join('hr_ref_hukuman', 'hr_hukuman.kd_hukuman = hr_ref_hukuman.kd_hukuman', 'LEFT');
        $this->db->join('hr_ref_cabang', 'hr_pegawai.kd_cabang = hr_ref_cabang.kd_cabang', 'LEFT');
        //=============Batas Edit
        //if (($apr == 'F')) {
        //    $this->db->where('hr_hukuman.nik', $grup_id);
        //}
        //=======================
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }

    function notf_phk() {
        $nik = "";
        $apr_w = "";
        $apr = "";
        $approve = "";
        $nik_s = $this->session->userdata('nik_session');
        $kd_jabatan_s = $this->session->userdata('kd_jabatan');
        $kd_unit_kerja_s = $this->session->userdata('unit_kerja_ses');
        $kd_cabang = $this->session->userdata('cabang_user');
        $grup_id = $this->session->userdata('kd_roles');
        $q_gtu = $this->db->query("select kd_unit_kerja from hr_pegawai where nik='$nik_s'");
        foreach ($q_gtu->result_array() as $row) {
            $kd_unit_kerja_ss = $row['kd_unit_kerja'];
        }
        $apr_w = $grup_id;
        $apr = "F";
        $approve = " AND  hr_phk.apr_w='$grup_id' ";
        $hasil = array();
        $query = $this->db->query("SELECT count(1) as jumlah
                FROM hr_phk
                LEFT JOIN hr_pegawai ON hr_phk.nik = hr_pegawai.nik
                LEFT JOIN hr_ref_unit_kerja ON hr_phk.kd_unit_kerja = hr_ref_unit_kerja.kd_unit_kerja
                LEFT JOIN hr_ref_cabang ON hr_phk.kd_cabang = hr_ref_cabang.kd_cabang
                LEFT JOIN hr_ref_jabatan ON hr_phk.kd_jabatan = hr_ref_jabatan.kd_jabatan
                LEFT JOIN hr_ref_phk ON hr_phk.kd_phk = hr_ref_phk.kd_phk
                WHERE  length(hr_pegawai.kd_level) < 3  $approve
                ");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }

    function notf_sppd() {
        $query = $this->db->query("select * from hr_apr_sppd where status='ENABLED' and step='sdm'");
        $jum_rows = $query->num_rows();
        return $jum_rows;
    }

    function notf_cuti() {
        $query = $this->db->query("select * from hr_apr_cuti where status='ENABLED' and step='sdm'");
        $jum_rows = $query->num_rows();
        return $jum_rows;
    }

    function notf_penempatan() {
        $grup_id = $this->session->userdata('kd_roles');
        $query = $this->db->query("select 
                count(1) as jumlah
                from hr_penempatan
                where hr_penempatan.status ='$grup_id'
                ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }

    function notf_tunjangan_kesehatan(){
        $this->db->select("count(1) as jumlah");
        $this->db->from("hr_tunjangan_kesehatan");
        $this->db->where("apr","O");

        $hasil = 0;
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }

    function notf_permintaan() {
        $nik = $this->session->userdata('nik_session');
        $query = $this->db->query("select 
                count(1) as jumlah
                from hr_apr_permintaan
                where hr_apr_permintaan.nik ='$nik' and status='ENABLED'
                ");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }

    function count_ulang_tahun() {
        $query = $this->db->query("select tgl_lahir from hr_pegawai_detail where date_part('day', tgl_lahir) = date_part('day', CURRENT_DATE) And date_part('MONTH', tgl_lahir) = date_part('MONTH', CURRENT_DATE)");
        $jum_rows = $query->num_rows();
        return $jum_rows;
    }

    function get_ulang_tahun() {
        $this->db->select('h.nama,h.nik');
        $this->db->join('hr_pegawai h', 'b.nik=h.nik');
        $this->db->where("(date_part('day', tgl_lahir) = date_part('day', CURRENT_DATE))");
        $this->db->where("(date_part('MONTH', tgl_lahir) = date_part('MONTH', CURRENT_DATE))");
        $query = $this->db->get('hr_pegawai_detail b');
        return $query->result_array();
    }

    function get_count_ulang_tahun() {
        $this->db->select('count(1) as jumlah');
        $this->db->join('hr_pegawai h', 'b.nik=h.nik');
        $this->db->where("(date_part('day', tgl_lahir) = date_part('day', CURRENT_DATE))");
        $this->db->where("(date_part('MONTH', tgl_lahir) = date_part('MONTH', CURRENT_DATE))");
        $query = $this->db->get('hr_pegawai_detail b');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;

        return $query->result_array();
    }
    
    //=====================
    public function data($condition = array()) {

        $this->db->select("a.nik, a.nama, a.kd_jabatan, a.kd_unit_kerja, a.kd_cabang,  
                     a.tgl_masuk, to_char(a.tgl_masuk, 'yyyy') as tahun_masuk, 
                     e.tgl_lahir, to_char(e.tgl_lahir, 'yyyy') as tahun_lahir, b.nama_jabatan, c.nama_unit_kerja, d.nama_cabang", FALSE);

        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.kd_jabatan = b.kd_jabatan', 'left');
        $this->db->join($this->table3 . ' c', 'a.kd_unit_kerja = c.kd_unit_kerja', 'left');
        $this->db->join($this->table4 . ' d', 'a.kd_cabang = d.kd_cabang', 'left');
        $this->db->join($this->table5 . ' e', 'a.nik = e.nik', 'left');
        $this->db->where("(date_part('day', e.tgl_lahir) = date_part('day', CURRENT_DATE))");
        $this->db->where("(date_part('MONTH', e.tgl_lahir) = date_part('MONTH', CURRENT_DATE))");
        $this->db->where_condition($condition);
        return $this->db;
    }
    
    public function get_data_ultah($condition = array()) {
        $this->data($condition);
        $query = $this->db->get();
        return $query->result_array();
    }
    //=====================

    function get_duka() {
        $this->db->select('berita');
        $this->db->where("(kategori='Duka')");
        $this->db->where("(publish<=current_date)");
        $this->db->where("(berakhir>=current_date)");
        $query = $this->db->get('hr_berita');
        return $query->result_array();
    }

    function get_news() {
        $this->db->select('*');
        $this->db->join('hr_pegawai h', 'b.add_by=h.nik');
        $this->db->where("(kategori='Perusahaan')");
        $this->db->where("(publish<=current_date)");
        $this->db->where("(berakhir>=current_date)");
        $this->db->order_by("(publish)");
        $this->db->order_by("(beritaid)");
        $query = $this->db->get('hr_berita b');
        return $query->result_array();
    }

    function get_app() {
        $app = '';
        $this->db->from($this->app);
        $this->db->order_by('app_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $a = explode('/', current_url());
            $active = $a[3];
            $app .= '
                <div class="product-info mail">
                    <p>Hubungkan dengan aplikasi lain : </p>
                        <ul class="features">';
            foreach ($query->result() as $row) {
                if ($active == $row->url)
                    $sty = 'style="font-weight:bold;background:none repeat scroll 0 0 #F5F5F5;border:1px solid #E5E5E5"';
                else
                    $sty = '';

                $app .= '
                            <li ' . $sty . '>
                                <img src="' . base_url() . $row->img_src . '" alt="">
                                <a href="' . base_url() . '../' . $row->url . '">
                                    <p class="title">' . $row->app_name . '</p>
                                </a>
                                <p>' . $row->description . '</p>
                            </li>
                            ';
            }
            $app .= '
                </ul>
            </div>';
        }
        return $app;
    }

    function notf_pelatihan() {
        
    }
	/* EDITED BY AGNI */	
	function get_kesalahan_pass(){
		$max_login=0;
		$q = $this->db->query("select max_login FROM hr_ref_parameter");
		  if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
				$max_login= $data->max_login;
			}
		}			
		$query = $this->db->query("select username from hr_salah_password where jumlah >='$max_login'");
        $jum_rows = $query->num_rows();
        return $jum_rows;
	}
	/*-----------------*/
	
	function notf_tunj_kematian() {
        //$nik = $this->session->userdata('nik_session');
        $query = $this->db->query("select 
                count(1) as jumlah
                from hr_tunjangan_kematian
                where hr_tunjangan_kematian.apr ='O'
                ");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
        }
        return $hasil;
    }
}

?>
<?php
class barang_son_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'barang';
    private $table2 = 'kategori';
    private $table3 = 'satuan';
    private $table4 = 'bbm';
    private $table5 = 'bbm_barang';
    private $table6 = 'bbk';
    private $table7 = 'bbk_barang';
    private $table8 = 'purchase_order';
    private $table9 = 'supplier';
    private $table10 = 'proyek';
    private $table11 = 'barang_son';

    function __rev1(){
        $this->db->from('temp_barang');
        $sql = $this->db->get();
        foreach ($sql->result() as $row){
            $data = array('jumlah_awal'=>$row->stok);
            $where = array('id_barang'=>$row->id);
            $this->db->update('barang',$data,$where);
        }
    }
    public function data($condition = array(),$where_date = array()) {
        if(empty($where_date['son'])){
            $where_date['son'] = '';
        }
        if(empty($where_date['bbm'])){
            $where_date['bbm'] = '';
        }
        if(empty($where_date['bbk'])){
            $where_date['bbk'] = '';
        }
        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', 'a.id_kategori = b.id_kategori','left');
        $this->db->join($this->table3 . ' c', 'c.id_satuan = a.id_satuan','left');
        $this->db->join("(select id_barang, sum(surplus) as surplus_son, sum(minus) as minus_son from barang_son where 1=1 ".$where_date['son']." group by id_barang ) as np",'np.id_barang = a.id_barang','left');
        $this->db->join('(select id_barang, sum(jumlah_barang) as jumlah_keluar from bbk_barang , bbk  where 1=1 and bbk.id_bbk = bbk_barang.id_bbk '.$where_date['bbk'].' group by id_barang) as nj','nj.id_barang = a.id_barang','left');
        $this->db->join('(select id_barang, sum(jumlah_barang) as jumlah_masuk from bbm_barang , bbm  where 1=1 and bbm.id_bbm = bbm_barang.id_bbm '.$where_date['bbm'].' group by id_barang) as nm','nm.id_barang = a.id_barang','left');
        $this->db->join("(select id_barang, sum(surplus) as jumlah_total_surplus, sum(minus) as jumlah_total_minus from barang_son group by id_barang ) as nst",'nst.id_barang = a.id_barang','left');
        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail($condition = array()) {
        //$this->db->select('*');
        $this->db->from($this->table11  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_jumlah_awal($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_barang'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['a.id_barang_son'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function jumlah_awal($id_barang) {
        // Filtering
        $condition['id_barang = '.$id_barang] = null;


        // List Data
        $data_jumlah_awal = $this->data_jumlah_awal($condition)->get();
        $rows = array();

        foreach ($data_jumlah_awal->result() as $value) {
            $id = $value->id_barang;
            $jumlah = $value->jumlah_awal;
        }
        return $jumlah;
    }    

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["a.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["a.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"] = null;
        }

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';

        $where_date['son'] = " and tanggal_son >= '$tahun_aktif_awal' and tanggal_son <= '$tahun_aktif_akhir'";
        $where_date['bbm'] = " and tanggal_bbm <= '$tahun_aktif_akhir'";
        $where_date['bbk'] = " and tanggal_bbk <= '$tahun_aktif_akhir'";
        //$condition["np.tanggal_son >= '$tahun_aktif_awal'"] = null ;
        //$condition["np.tanggal_son <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------

        // Total Record
        $total = $this->data($condition,$where_date)->count_all_results();

        // List Data
        
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition,$where_date)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;
            $action = '';
            $action .= anchor(null, '<i class="icon-cog"></i>', array('id' => 'drildown_key_t_barang_' . $id, 'class' => 'btn', 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_barang', 'data-original-title' => 'Detil Proses',  'data-source' => base_url('barang_son/get_detail_barang/' . $id.'/'.$tahun_aktif))) . ' ';

            $jumlah = $value->surplus_son  - $value->minus_son;
            $jumlah_total = $value->jumlah_total_surplus  - $value->jumlah_total_minus + $value->jumlah_awal + $value->jumlah_masuk - $value->jumlah_keluar;
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                
                $rows[] = array(
                    'primary_key' =>$value->id_barang,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'jumlah'=>$jumlah,
                    'jumlah_total' =>$jumlah_total,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_barang,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'jumlah'=>$jumlah,
                    'jumlah_total' =>$jumlah_total,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    


    public function data_table_detail(){
        $id_barang = $this->input->post('id_barang');
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        //$where_son = "tanggal_son >= '$tahun_aktif_awal' and tanggal_son <= '$tahun_aktif_akhir'";
        $condition["a.tanggal_son >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_son <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------


        // Total Record
        $total = $this->data_detail($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.tanggal_son');
        $this->db->limit($this->limit, $this->offset);
        $data_detail_son = $this->data_detail($condition)->get();
        $rows = array();


        foreach ($data_detail_son->result() as $value) {
            $id = $value->id_barang_son;
            $id_barang = $value->id_barang;
            $action = '';
            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('barang_son/edit_detail/' .$id_barang.'/'. $id.'/'.$tahun_aktif))) . ' ';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('barang_son/delete_detail/' .$id_barang.'/'. $id.'/'.$tahun_aktif)));
            }
                $rows[] = array(
                    'tanggal_son' => $value->tanggal_son,
                    'surplus' => $value->surplus,
                    'minus' => $value->minus,
                    'keterangan' => $value->keterangan,
                    'action' => $action
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_bbk(){
        $id_barang = $this->input->post('id_barang');
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbk');
        $this->db->limit($this->limit, $this->offset);
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'primary_key' =>$value->id_barang,
                    'tanggal_bbm' => $value->tanggal_bbk,
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["a.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["a.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"] = null;
        }

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;
            $action = '';
            $jumlah = $value->jumlah_awal + $value->jumlah_masuk - $value->jumlah_keluar;
                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'jumlah'=>$jumlah,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function data_table_detail_bbm_excel(){
        $id_barang = $this->input->post('id_barang');
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;

        // Total Record
        $total = $this->data_detail_bbm($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbm');
        $data_detail_bbm = $this->data_detail_bbm($condition)->get();
        $rows = array();

        foreach ($data_detail_bbm->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => $value->tanggal_bbm,
                    'kd_bbm' => $value->kd_bbm,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_bbk_excel(){
        $id_barang = $this->input->post('id_barang');
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbk');
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => $value->tanggal_bbk,
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_barang' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_barang' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table11, array('id_barang_son' => $id));
    }
    
    public function options_filter($default = '--Pilih Jenis Perawatan--', $key = '') {
        $this->db->order_by('a.id_barang');
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->jenis_perawatan;
        }
        return $options;
    }


    public function options($default = '--Pilih Barang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->kd_barang.' / '.$row->nama_barang ;
        }
        return $options;
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table11, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table11, $data, array('id_barang_son' => $id));
    }

    public function jumlah_son($id_barang) {
        // Filtering
        $condition['id_barang = '.$id_barang] = null;


        // List Data
        $data_jumlah_awal = $this->data_jumlah_son($condition)->get();
        $rows = array();
        $jumlah = 0;
        foreach ($data_jumlah_awal->result() as $value) {
            $id = $value->id_barang;
            $jumlah = $value->surplus_son - $value->minus_son;
        }
        return $jumlah;
    }  

    private function data_jumlah_son($condition = array()) {
        $this->db->select('sum(minus) as minus_son, sum(surplus) as surplus_son,id_barang');
        $this->db->from($this->table11  . ' a');
        $this->db->where_condition($condition);
        $this->db->group_by('id_barang');
        return $this->db;
    }
    
}

?>
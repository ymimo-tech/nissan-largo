<?php

class Api_packing_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($packing_code){
    	$this->db->select("*");
    	$this->db->from("m_packing");
    	$this->db->where("packing_code", $packing_code);
    	return $this->db->get();
    }

    function add($packing_code){
        $query  = $this->db->query("SELECT packing_code FROM m_packing WHERE packing_code LIKE '" . substr($packing_code, 0, -1) . "%' ORDER BY packing_id DESC LIMIT 1");
        $row    = $query->row_array();
        if(isset($row)){
        	$last = intval(substr($row['packing_code'], -1)) + 1;
            return substr($packing_code, 0, -1).$last;
        } else{
            return null;
        }
    }

    function generate($data){
    	$id_user		= $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
    	$data_insert 	= array(	'packing_code'		=> $data["packing_code"], 
                        	        'packing_date'    	=> $data["packing_date"],
                            	    'packing_user_id'	=> $id_user["user_id"]
                                	);
        $this->db->insert("m_packing", $data_insert);
    }

    function get_serial($serial_number){
    	$this->db->select("*");
    	$this->db->from("receiving_barang");
    	$this->db->where("packing_id IS NULL");
    	$this->db->where("pl_id IS NOT NULL");
    	$this->db->where("id_barang IS NOT NULL");
    	$this->db->where("kd_unik", $serial_number);
    	return $this->db->get();
    }

}
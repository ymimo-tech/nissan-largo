<?php
class inbound_document_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'm_inbound_document';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_inbound_document'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function create($data) { 
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_inbound_document' => $id));
    }

    public function delete($id) {
        //return $this->db->delete($this->table, array('id_inbound_document' => $id));
		
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS t 
				FROM 
					inbound 
				WHERE 
					id_inbound_document=\''.$id.'\'';
					
		$row = $this->db->query($sql)->row_array();
		if($row['t'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('id_inbound_document' => $id));
			
			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}
			
        return $result;
    }
    
    public function options($default = '--Pilih Dokumen Inbound--', $key = '') {
        $this->db->where('is_active',1);
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_inbound_document] = $row->inbound_document_desc ;
        }
        return $options;
    }
    
    function create_options($table_name, $value_column, $caption_column) {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    public function get_inbound_num($id){

        $this->db->where('id_inbound_document',$id);

        $data = $this->db->get('inbound')->num_rows();

        return $data;
    }
}

?>
<?php
class item_stok_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'barang';
    private $table2 = 'kategori';
    private $table3 = 'satuan';
    private $table4 = 'receiving';
    private $table5 = 'receiving_barang';
    private $table6 = 'do';
    private $table7 = 'do_barang';
    private $table8 = 'purchase_order';
    private $table9 = 'supplier';
    private $table10 = 'proyek';
    private $table11 = 'barang_son';
    private $table12 = 'purchase_order_barang';
    private $loc     = 'm_loc';

    function __rev1(){
        $this->db->from('temp_barang');
        $sql = $this->db->get();
        foreach ($sql->result() as $row){
            $data = array('jumlah_awal'=>$row->stok);
            $where = array('id_barang'=>$row->id);
            $this->db->update('barang',$data,$where);
        }
    }
	
	public function getStockCardTotal($id){
		$result = array();
		
		$sql = 'SELECT COUNT(*) AS total  
				FROM
					(
						(
							SELECT 
								DATE_FORMAT(finish_putaway, \'%d\/%m\/%Y\') AS date,
								finish_putaway AS date_sort,
								(
									SELECT 
										COUNT(*) 
									FROM 
										receiving_barang r 
									JOIN barang b 
										ON b.id_barang=r.id_barang
									WHERE 
										id_receiving=rcv.id_receiving 
									AND 
										r.id_barang=brg.id_barang
								) AS qty, \'IN\' AS type, user_name, kd_receiving AS doc, 
								rcv.id_receiving AS id_doc
							FROM 
								receiving rcv 
							JOIN receiving_barang rcvbrg 
								ON rcvbrg.id_receiving=rcv.id_receiving 
							JOIN barang brg 
								ON brg.id_barang=rcvbrg.id_barang 
							LEFT JOIN hr_user hu 
								ON hu.user_id=rcv.user_id
							WHERE 
								rcvbrg.id_barang=\''.$id.'\'
						)
						UNION
						(
							SELECT 
								DATE_FORMAT(shipping_finish, \'%d\/%m\/%Y\') AS date,
								shipping_finish AS date_sort,
								(
									SELECT 
										COUNT(*) 
									FROM 
										receiving_barang r 
									JOIN barang b 
										ON b.id_barang=r.id_barang
									WHERE 
										shipping_id=shipp.shipping_id
									AND 
										r.id_barang=brg.id_barang
								) AS qty, \'OUT\' AS type, user_name, shipping_code AS doc,
								shipp.shipping_id AS id_doc
							FROM 
								shipping shipp
							JOIN receiving_barang rcvbrg 
								ON rcvbrg.shipping_id=shipp.shipping_id 
							JOIN barang brg 
								ON brg.id_barang=rcvbrg.id_barang 
							LEFT JOIN hr_user hu 
								ON hu.user_id=shipp.user_id
							WHERE 
								rcvbrg.id_barang=\''.$id.'\'
						)
					) AS tbl';
						
		$row = $this->db->query($sql)->row_array();
		$result = $row;
		
		return $result;
	}
	
	public function getStockCard($post = array()){
		$result = array();
		
		$offset = 10;
		$limit = ' 0, ' . $offset;
		
		if(isset($post['start'])){
			if(!empty($post['start']))
				$limit = ' ' . $post['start'].', '.$offset;
		}
		

		$sql = 'SELECT * 
				FROM
					(
						(
							SELECT 
								DATE_FORMAT(IFNULL(finish_putaway,start_putaway), \'%d\/%m\/%Y\') AS date,
								DATE_FORMAT(IFNULL(finish_putaway, start_putaway), \'%Y\/%m\/%d\') AS date_sort,
								IFNULL(sum(first_qty),0) AS qty, 
								\'IN\' AS type, IFNULL(user_name, \'-\') AS user_name, kd_receiving AS doc, 
								rcv.id_receiving AS id_doc, \'\' AS balance, \'RCV\' as type_doc,
								IFNULL(NULL,0) as system_qty, IFNULL(NULL,0) as scanned_qty
							FROM 
								receiving_barang rcvbrg 
							JOIN barang brg 
								ON brg.id_barang=rcvbrg.id_barang 
							LEFT JOIN receiving rcv
								ON rcv.id_receiving=rcvbrg.id_receiving 
							LEFT JOIN cycle_count cc 
								ON cc.cc_id=rcvbrg.cc_id 
							LEFT JOIN hr_user hu 
								ON hu.user_id=rcv.user_id
							WHERE 
								rcvbrg.id_barang=\''.$post['id_barang'].'\'
							group by
								rcv.id_receiving
						)
						UNION
						(
							SELECT 
								DATE_FORMAT(IFNULL(pl_finish, pl_date), \'%d\/%m\/%Y\') AS date,
								DATE_FORMAT(pl_finish, \'%Y\/%m\/%d\') AS date_sort,
								sum(qty) AS qty, \'OUT\' AS type, IFNULL(user_name, \'-\') AS user_name, pl_name AS doc,
								pl.pl_id AS id_doc, \'\' AS balance, \'PICK\' as type_doc,
								IFNULL(NULL,0) as system_qty, IFNULL(NULL,0) as scanned_qty
							FROM 
								picking_list pl
							JOIN picking_recomendation pr 
								ON pr.id_picking=pl.pl_id
							JOIN receiving_barang rcvbrg 
								ON rcvbrg.kd_unik=pr.kd_unik
							JOIN barang brg 
								ON brg.id_barang=rcvbrg.id_barang 
							LEFT JOIN hr_user hu 
								ON hu.user_id=pl.user_id
							WHERE 
								rcvbrg.id_barang=\''.$post['id_barang'].'\'
							group by
								pl.pl_id
						)
						UNION
						(
							SELECT 
								DATE_FORMAT(tanggal_order_kitting, \'%d\/%m\/%Y\') AS date,
								DATE_FORMAT(tanggal_order_kitting, \'%Y\/%m\/%d\') AS date_sort,
								sum(qty) AS qty, \'OUT\' AS type, IFNULL(user_name, \'-\') AS user_name, kd_order_kitting AS doc,
								ok.id_order_kitting AS id_doc, \'\' AS balance, \'KIT\' as type_doc,
								IFNULL(NULL,0) as system_qty, IFNULL(NULL,0) as scanned_qty
							FROM 
								order_kitting ok
							JOIN order_kitting_picking okp 
								ON okp.id_order_kitting=ok.id_order_kitting
							JOIN receiving_barang rcvbrg 
								ON rcvbrg.kd_unik=okp.kd_unik
							JOIN barang brg 
								ON brg.id_barang=rcvbrg.id_barang 
							LEFT JOIN hr_user hu 
								ON hu.user_id=ok.user_id
							WHERE 
								rcvbrg.id_barang=\''.$post['id_barang'].'\'
							group by
								ok.id_order_kitting
						)UNION(
							SELECT
								DATE_FORMAT(cc_time, \'%d\/%m\/%Y\') AS date,
								DATE_FORMAT(cc_time, \'%Y\/%m\/%d\') AS date_sort,
								(sum(scanned_qty)-sum(system_qty)) AS qty, \'IN\' AS type, IFNULL(user_name, \'-\') AS user_name, cc_code AS doc,
								cc.cc_id AS id_doc, \'\' AS balance, \'CC\' as type_doc,
								IFNULL(sum(system_qty),0) as system_qty, IFNULL(sum(scanned_qty),0) as scanned_qty
							FROM
								cycle_count cc
							JOIN cycle_count_result ccr
								ON ccr.cc_id=cc.cc_id
							JOIN barang brg
								ON brg.id_barang=ccr.id_barang
							JOIN hr_user hu
								ON hu.user_id=cc.user_id
							Where
								ccr.id_barang=\''.$post['id_barang'].'\'
							AND
								ccr.status=\'ADJUST TO SCANNED QTY\'
							GROUP BY
								cc.cc_id
							HAVING
								(system_qty < scanned_qty)
						)UNION(
							SELECT
								DATE_FORMAT(cc_time, \'%d\/%m\/%Y\') AS date,
								DATE_FORMAT(cc_time, \'%Y\/%m\/%d\') AS date_sort,
								(sum(system_qty)-sum(scanned_qty)) AS qty, \'OUT\' AS type, IFNULL(user_name, \'-\') AS user_name, cc_code AS doc,
								cc.cc_id AS id_doc, \'\' AS balance, \'CC\' as type_doc,
								IFNULL(sum(system_qty),0) as system_qty, IFNULL(sum(scanned_qty),0) as scanned_qty
							FROM
								cycle_count cc
							JOIN cycle_count_result ccr
								ON ccr.cc_id=cc.cc_id
							JOIN barang brg
								ON brg.id_barang=ccr.id_barang
							JOIN hr_user hu
								ON hu.user_id=cc.user_id
							Where
								ccr.id_barang=\''.$post['id_barang'].'\'
							AND
								ccr.status=\'ADJUST TO SCANNED QTY\'
							GROUP BY
								cc.cc_id
							HAVING
								(system_qty > scanned_qty)
						)
					) AS tbl
					ORDER BY 
						date_sort DESC';
						
		$row = $this->db->query($sql)->result_array();
		
		/*
		$len = count($row);
		
		for($i = 0; $i < $len; $i++){
			$row[$i]['id_barang'] = $post['id_barang'];
			$row[$i]['limit'] = $limit;
			
			if($row[$i]['type'] == 'IN'){
				//var_dump($this->countBalance($row[$i]));
				$row[$i]['balance'] = ($row[$i]['qty'] + $this->countBalance($row[$i]));
			}
			
			if($row[$i]['type'] == 'OUT')
				$row[$i]['balance'] = ($this->countBalance($row[$i]) - $row[$i]['qty']);
		}
		*/
		

		$this->db->select(array("nama_barang","kd_barang"));
		$this->db->from('barang');
		$this->db->where('id_barang', $post['id_barang']);

		$item = $this->db->get()->row_array();

		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['item_name'] = $item['nama_barang'];
		$result['item_code'] = $item['kd_barang'];

		return $result;
	}
	
	public function countBalance($post = array()){
		$result = 0;
		
		$sql = 'SELECT * 
				FROM
				(
					(
						SELECT 
							finish_putaway AS date_sort,
							(
								SELECT 
									COUNT(*) 
								FROM 
									receiving_barang r 
								JOIN barang b 
									ON b.id_barang=r.id_barang
								WHERE 
									id_receiving=rcv.id_receiving 
								AND 
									r.id_barang=brg.id_barang
							) AS qty, \'IN\' AS type
						FROM 
							receiving rcv 
						JOIN receiving_barang rcvbrg 
							ON rcvbrg.id_receiving=rcv.id_receiving 
						JOIN barang brg 
							ON brg.id_barang=rcvbrg.id_barang 
						LEFT JOIN hr_user hu 
							ON hu.user_id=rcv.user_id
						WHERE 
							rcvbrg.id_barang=\''.$post['id_barang'].'\' 
						AND 
							IFNULL(finish_putaway, tanggal_receiving) < \''.$post['date_sort'].'\'
					)
					UNION
					(
						SELECT 
							shipping_finish AS date_sort,
							(
								SELECT 
									COUNT(*) 
								FROM 
									receiving_barang r 
								JOIN barang b 
									ON b.id_barang=r.id_barang
								WHERE 
									shipping_id=shipp.shipping_id
								AND 
									r.id_barang=brg.id_barang
							) AS qty, \'OUT\' AS type
						FROM 
							shipping shipp
						JOIN receiving_barang rcvbrg 
							ON rcvbrg.shipping_id=shipp.shipping_id 
						JOIN barang brg 
							ON brg.id_barang=rcvbrg.id_barang 
						LEFT JOIN hr_user hu 
							ON hu.user_id=shipp.user_id
						WHERE 
							rcvbrg.id_barang=\''.$post['id_barang'].'\' 
						AND 
							IFNULL(shipping_finish, shipping_date) < \''.$post['date_sort'].'\'
					)
				) AS tbl 
				ORDER BY 
					date_sort ASC';
					
				//LIMIT'.$post['limit'];
				
		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		
		for($i = 0; $i < $len; $i++){
			if($row[$i]['type'] == 'IN'){
				$result = $result + $row[$i]['qty'];
			}
			
			if($row[$i]['type'] == 'OUT'){
				$result = $row[$i]['qty'] - $result;
			}
		}
		
		//var_dump($result);
		
		return $result;
	}
	
	public function getLowQty(){
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS total
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id 
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					IFNULL(tbl.qty, 0) <= minimum_stock';
					
		$row = $this->db->query($sql)->row_array();
		
		return $row['total'];
	}

	public function update_lok_outbound(){
		$where = array(
					'loc_id' => 101,
					'last_qty > 1' => NULL);
		$this->db->select('kd_unik');
		$this->db->from('receiving_barang');
		$this->db->where($where);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			$data_where = array('kd_unik' => $row->kd_unik);
			$data_update = array('loc_id' => 103);
			$this->db->where($data_where);
			$this->db->update('receiving_barang',$data_update);
			# code...
		}
	}
	
	public function getListQty($post = array()){
		$result = array();
		
		// $where = '';
		// $where1 = '';
		// $wheres = '';
		$where = '';
		if(!empty($post['id_barang'])){
			// $where1 .= ' AND tbl.id_barang = \''.$post['id_barang'].'\' ';
			// $wheres .= ' AND r.id_barang = \''.$post['id_barang'].'\' ';
			// $where .=  " AND rcvbrg.id_barang = '".$post['id_barang']."'";
			$where .=  " AND rb.id_barang = '".$post['id_barang']."'";
		}		
		
/*		$sql = 'SELECT 
					IFNULL(rec.rec_qty, 0) AS rec_qty, IFNULL(onhand.onhand_qty, 0) AS onhand_qty, 
					"0" AS allocated_qty, IFNULL(suspend.suspend_qty, 0) AS suspend_qty,
					IFNULL((IFNULL(onhand.onhand_qty, 0) - 0) - IFNULL(suspend.suspend_qty, 0) - IFNULL(damage.damage_qty, 0), 0) AS available_qty, 
					IFNULL(damage.damage_qty, 0) AS damage_qty 
				FROM 
					receiving_barang rcvbrg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS rec_qty 
					FROM 
						m_loc loc 
					JOIN receiving_barang r 
						ON r.loc_id=loc.loc_id 
					JOIN receiving rcv 
						ON rcv.id_receiving=r.id_receiving
					WHERE 
						loc.loc_type IN('.$this->config->item('rec_location_qty').') 
					AND 
						r.pl_id IS NULL'.$wheres.' 
				) AS rec 
					ON rec.id_barang=rcvbrg.id_barang 
			
				LEFT JOIN(
					SELECT
						id_barang,
						sum(last_qty)AS onhand_qty
					FROM
						receiving_barang r
					WHERE
						1 = 1
					AND (r.last_qty >1 or r.pl_id IS NULL)
					'.$wheres.' 	
				)AS onhand ON onhand.id_barang = rcvbrg.id_barang
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS suspend_qty
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					WHERE 
						r.id_qc IN(\'2\', \'4\')'.$wheres.' 
				) AS suspend 
					ON suspend.id_barang=rcvbrg.id_barang 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS damage_qty
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					WHERE 
						r.id_qc IN(\'3\')'.$wheres.' 
				) AS damage 
					ON damage.id_barang=rcvbrg.id_barang
				WHERE 1=1'.$where.' 
				GROUP BY 
					rcvbrg.id_barang';
*/
		$sql = "SELECT 
				COALESCE((
					SELECT 
						sum(first_qty) 
					FROM 
						receiving_barang rb
					WHERE 
						1=1 
					AND 
						loc_id IN (100)
					$where
				),0) AS rec_qty,
				COALESCE((
					SELECT 
						sum(last_qty)AS onhand_qty
					FROM 
						receiving_barang rb
					JOIN
						barang brg
						ON brg.id_barang = rb.id_barang
					WHERE 
						1 = 1
					AND
						loc_id NOT IN (100,101,104)
					AND 
						(CASE WHEN brg.has_qty=1 THEN rb.last_qty >=1 ELSE rb.pl_id IS NULL END)
					$where
				),0	) AS onhand_qty,
				COALESCE( (
					SELECT
						sum(qty)
					FROM
						receiving_barang rb
					LEFT JOIN picking_recomendation pr 
						ON pr.kd_unik=rb.kd_unik
					WHERE
						1=1
					AND
						st_packing <> 1
					$where
				),0) AS allocated_qty,
				COALESCE((
					SELECT 
						COUNT(*)
					FROM 
						receiving_barang rb
					JOIN m_qc qc 
						ON rb.id_qc=qc.id_qc 
					WHERE 
						rb.id_qc IN(2,4)
					$where
				),0) AS suspend_qty,
				COALESCE((
					SELECT 
						COUNT(*) AS damage_qty
					FROM 
						receiving_barang rb
					JOIN m_qc qc
						ON rb.id_qc=qc.id_qc 
					WHERE 
						rb.id_qc IN (3)
					$where
				),0) AS damage_qty";
		
		$row = $this->db->query($sql)->row_array();
		$len = count($row);
		
		if($len > 0){
		
			$recQty = $row['rec_qty'];
			$onhandQty = $row['onhand_qty'];
			$allocQty = $row['allocated_qty'];
			$spnQty = $row['suspend_qty'];
			$dmgQty = $row['damage_qty'];
			// $availQty = $row['available_qty'];
			$availQty = $onhandQty - ($allocQty + $spnQty + $dmgQty);
			
		}else{
			
			$recQty = 0;
			$onhandQty = 0;
			$allocQty = 0;
			$spnQty = 0;
			$dmgQty = 0;
			$availQty = 0;
			
		}
		
		if($recQty > 0)
			$recQty = '<a href="javascript:;" data-params="receiving" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$recQty.'</a>';
		
		if($onhandQty > 0)
			$onhandQty = '<a href="javascript:;" data-params="onhand" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$onhandQty.'</a>';
		
		if($allocQty > 0)
			$allocQty = '<a href="javascript:;" data-params="allocated" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$allocQty.'</a>';
		
		if($spnQty > 0)
			$spnQty = '<a href="javascript:;" data-params="suspend" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$spnQty.'</a>';
		
		if($availQty > 0)
			$availQty = '<a href="javascript:;" data-params="available" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$availQty.'</a>';
		
		if($dmgQty > 0)
			$dmgQty = '<a href="javascript:;" data-params="damage" data-toggle="tooltip" data-placement="bottom" title="Click here to see detail">'.$dmgQty.'</a>';
		
		$result['rec_qty'] = $recQty;
		$result['onhand_qty'] = $onhandQty;
		$result['allocated_qty'] = $allocQty;
		$result['suspend_qty'] = $spnQty;
		$result['available_qty'] = $availQty;
		$result['damage_qty'] = $dmgQty;
		
		return $result;
	}
	
	public function getListQtyDataTable($post = array()){
		
		//THIS METHOD NOT UPDATED
		
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'rec_qty',
			2 => 'onhand_qty',
			3 => 'allocated_qty',
			4 => 'suspend_qty',
			5 => '(onhand.onhand_qty - pick.allocated_qty - suspend.suspend_qty)'
		);
		
		$where = '';
		$where1 = '';
		$wheres = '';
		if(!empty($post['id_barang'])){
			$where1 .= ' AND tbl.id_barang = \''.$post['id_barang'].'\' ';
			$wheres .= ' AND r.id_barang = \''.$post['id_barang'].'\' ';
			$where .= ' AND rcvbrg.id_barang = \''.$post['id_barang'].'\' ';
		}
		
		$sql = 'SELECT 
					COUNT(*) AS total
				FROM 
				(
					SELECT 
						COUNT(*)
					FROM 
					(
						SELECT 
							rcvbrg.id_barang
						FROM 
							receiving_barang rcvbrg 
						LEFT JOIN 
						(
							SELECT 
								id_barang, COUNT(*) AS rec_qty
							FROM 
								m_loc loc
							JOIN receiving_barang r 
								ON r.loc_id=loc.loc_id 
							WHERE 
								loc.loc_type IN('.$this->config->item('rec_location_qty').')'.$wheres.' 
						) AS rec 
							ON rec.id_barang=rcvbrg.id_barang 
						LEFT JOIN 
						(
							SELECT 
								id_barang, COUNT(*) AS onhand_qty
							FROM 
								m_loc loc
							JOIN receiving_barang r 
								ON r.loc_id=loc.loc_id 
							JOIN receiving rcv 
								ON rcv.id_receiving=r.id_receiving
							WHERE 
								loc.loc_type IN('.$this->config->item('onhand_location_qty').')'.$wheres.' 	
						) AS onhand 
							ON onhand.id_barang=rcvbrg.id_barang 
						LEFT JOIN 
						(
							SELECT 
								id_barang, COUNT(*) AS allocated_qty
							FROM 
								picking_list pl 
							JOIN receiving_barang r 
								ON r.pl_id=pl.pl_id 
							WHERE r.loc_id != \'104\' AND r.loc_id != \'101\''.$wheres.' 
						) AS pick  
							ON pick.id_barang=rcvbrg.id_barang 
						LEFT JOIN 
						(
							SELECT 
								id_barang, COUNT(*) AS suspend_qty
							FROM 
								m_qc qc
							JOIN receiving_barang r 
								ON r.id_qc=qc.id_qc 
							WHERE 
								r.id_qc = \'4\''.$wheres.' 
						) AS suspend 
							ON suspend.id_barang=rcvbrg.id_barang 
					) AS tbl 
					WHERE 1=1'.$where1.'
					GROUP BY 
						tbl.id_barang
				) AS tbl2';
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					IFNULL(rec.rec_qty, 0) AS rec_qty, IFNULL(onhand.onhand_qty, 0) AS onhand_qty, 
					IFNULL(pick.allocated_qty, 0) AS allocated_qty, IFNULL(suspend.suspend_qty, 0) AS suspend_qty,
					IFNULL((onhand.onhand_qty - pick.allocated_qty - suspend.suspend_qty), 0) AS available_qty
				FROM 
					receiving_barang rcvbrg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS rec_qty
					FROM 
						m_loc loc
					JOIN receiving_barang r 
						ON r.loc_id=loc.loc_id 
					JOIN receiving rcv 
						ON rcv.id_receiving=r.id_receiving
					WHERE 
						loc.loc_type IN('.$this->config->item('rec_location_qty').')'.$wheres.' 
				) AS rec 
					ON rec.id_barang=rcvbrg.id_barang 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS onhand_qty
					FROM 
						m_loc loc
					JOIN receiving_barang r 
						ON r.loc_id=loc.loc_id 
					WHERE 
						loc.loc_type IN('.$this->config->item('onhand_location_qty').') 
					AND 
						r.id_qc != \'3\''.$wheres.' 	
				) AS onhand 
					ON onhand.id_barang=rcvbrg.id_barang 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS allocated_qty
					FROM 
						picking_list pl 
					JOIN receiving_barang r 
						ON r.pl_id=pl.pl_id 
					WHERE r.loc_id != \'104\' AND r.loc_id != \'101\''.$wheres.' 
				) AS pick  
					ON pick.id_barang=rcvbrg.id_barang 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS suspend_qty
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					WHERE 
						r.id_qc = \'4\''.$wheres.' 
				) AS suspend 
					ON suspend.id_barang=rcvbrg.id_barang
				WHERE 1=1'.$where.' 
				GROUP BY 
					rcvbrg.id_barang';
		
		//$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$recQty = $row[$i]['rec_qty'];
			$onhandQty = $row[$i]['onhand_qty'];
			$allocQty = $row[$i]['allocated_qty'];
			$spnQty = $row[$i]['suspend_qty'];
			$availQty = $row[$i]['available_qty'];
			
			if($recQty > 0)
				$recQty = '<a href="javascript:;" data-params="receiving" alt="Click to see detail">'.$recQty.'</a>';
			
			if($onhandQty > 0)
				$onhandQty = '<a href="javascript:;" data-params="onhand" alt="Click to see detail">'.$onhandQty.'</a>';
			
			if($allocQty > 0)
				$allocQty = '<a href="javascript:;" data-params="allocated" alt="Click to see detail">'.$allocQty.'</a>';
			
			if($spnQty > 0)
				$spnQty = '<a href="javascript:;" data-params="suspend" alt="Click to see detail">'.$spnQty.'</a>';
			
			if($availQty > 0)
				$availQty = '<a href="javascript:;" data-params="available" alt="Click to see detail">'.$availQty.'</a>';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $recQty;
			$nested[] = $onhandQty;
			$nested[] = $allocQty;
			$nested[] = $spnQty;
			$nested[] = $availQty;
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}

	public function getStatus(){
		$result = array();
		
		$sql = 'SELECT 
					id_qc, kd_qc 
				FROM 
					m_qc 
				ORDER BY 
					kd_qc ASC';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getCategory(){
		$result = array();
		
		$sql = 'SELECT 
					id_kategori, kd_kategori, CONCAT(kd_kategori, \' - \', nama_kategori) AS nama_kategori  
				FROM 
					kategori';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getLocation(){
		$result = array();
		
/*		$sql = 'SELECT 
					loc_id, 
					(
						CASE  
							WHEN loc_id > 109 
								THEN CONCAT(loc_name, \' ( \', CONCAT(loc_type, \' )\', \'\')) 
							ELSE 
								loc_name 
						END
					) AS loc_name 
				FROM 
					m_loc 
				WHERE 
					loc_name IS NOT NULL 
				AND 
					loc_type != \'SENTOUT\' 
				ORDER BY 
					loc_id ASC';
*/					

		$sql = 'SELECT 
					loc_id, loc_name 
				FROM 
					m_loc 
				WHERE 
					loc_name IS NOT NULL 
				AND 
					loc_type != \'SENTOUT\' 
				ORDER BY 
					loc_id ASC';

		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getLocationType(){
		$result = array();
		
		$result = array(
			'INBOUND'	=> 'INBOUND',
			'TRANSIT'	=> 'TRANSIT',
			'BINLOC'	=> 'BINLOC',
			'OUTBOUND'	=> 'OUTBOUND',
			'QC'		=> 'QC',
			'NG'		=> 'NG',
		);
		
		return $result;
	}
	
	public function getUom(){
		$result = array();
		
		$sql = 'SELECT 
					id_satuan, nama_satuan 
				FROM 
					satuan';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getItem(){
		$result = array();
		
		$sql = 'SELECT 
					id_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang 
				FROM 
					barang';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
    public function get_receiving_by_id_barang($id_barang){
		$condition['a.loc_id != \'104\''] = null;
		$condition['a.loc_id != \'101\''] = null;
        $condition['a.id_barang'] = $id_barang;
        return $this->data_detail($condition);
    }

    public function data($condition = array(),$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $tahun_faktur = $tahun_aktif;
        //$jumlah = 0;

        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', 'a.id_kategori = b.id_kategori','left');
		$this->db->join('kategori_2' . ' d', 'd.id_kategori_2 = a.id_kategori_2','left'); //ADJI
		$this->db->join('owner' . ' e', 'e.id_owner = a.id_owner','left'); //ADJI
        $this->db->join($this->table3 . ' c', 'c.id_satuan = a.id_satuan','left');
        //$this->db->join('receiving_barang rb','rb.id_barang = a.id_barang and rb.id_do is null','left');
        //$this->db->join('(select id_barang, count(kd_unik) as qty from receiving_barang where pl_status = 0 and loc_id != \'100\' and loc_id != \'101\' and loc_id != \'104\' and id_qc = 1 GROUP BY id_barang) as rb ','a.id_barang = rb.id_barang','left');
		$this->db->join('(select id_barang, count(kd_unik) as qty from receiving_barang where pl_status = 0 and loc_id != \'104\' and loc_id != \'101\'  GROUP BY id_barang) as rb ','a.id_barang = rb.id_barang','left');
        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);

        return $this->db;
    }
	
	public function getDetailList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		//NOTE :
		//CHANGE JOIN TO LEFT RECEIVING TO GET STOCK FROM CYCLE COUNT NOT FROM RECEIVING PROCESS
		
		$columns = array(
			0 => '',
			1 => '',
			2 => 'kd_unik',
			3 => 'kd_parent',
			4 => 'last_qty',
			5 => '',
			6 => 'tgl_exp',
			7 => 'tgl_in',
			8 => 'loc_name',
			9 => 'loc_type',
			10 => 'kd_qc'
		);
		
		$where = '';
		if(!empty($post['id_barang'])){
			$where .= ' AND id_barang = \''.$post['id_barang'].'\' ';
		}
		
		$params = '';
		if(!empty($post['params']))
			$params = $post['params'];
		
		if($params == 'receiving')
			$where .= ' AND loc.loc_type IN ('.$this->config->item('rec_location_qty').') AND rcvbrg.pl_id IS NULL';
		
		if($params == 'onhand')
			$where .= ' AND loc.loc_type IN ('.$this->config->item('onhand_location_qty').') ';
		
		if($params == 'allocated')
			$where .= ' AND rcvbrg.pl_id IS NOT NULL ';
		
		if($params == 'suspend')
			$where .= ' AND rcvbrg.id_qc IN(\'2\', \'4\') ';
		
		if($params == 'damage')
			$where .= ' AND rcvbrg.id_qc IN(\'3\') ';
		
		if($params == 'available')
			$where .= ' AND rcvbrg.id_qc = \'1\' AND loc.loc_type = '.$this->config->item('available_location').' ';
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					receiving_barang rcvbrg
				LEFT JOIN m_loc loc 
					ON loc.loc_id = rcvbrg.loc_id 
				LEFT JOIN m_qc qc 
					ON qc.id_qc = rcvbrg.id_qc 
				LEFT JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving 
				LEFT JOIN inbound inb 
					ON inb.id_inbound=rcv.id_po 
				LEFT JOIN picking_list pl 
					ON pl.pl_id=rcvbrg.pl_id 
				LEFT JOIN picking_list_outbound plo 
					ON plo.pl_id=pl.pl_id 
				LEFT JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				LEFT JOIN cycle_count cc 
					ON cc.cc_id=rcvbrg.cc_id
				LEFT JOIN order_kitting ok
					ON rcvbrg.id_order_kitting=ok.id_order_kitting
				WHERE 
					rcvbrg.loc_id != \'104\' AND rcvbrg.loc_id != \'101\''.$where.'';
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					* 
				FROM 
					receiving_barang rcvbrg
				LEFT JOIN m_loc loc 
					ON loc.loc_id = rcvbrg.loc_id 
				LEFT JOIN m_qc qc 
					ON qc.id_qc = rcvbrg.id_qc 
				LEFT JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving 
				LEFT JOIN inbound inb 
					ON inb.id_inbound=rcv.id_po 
				LEFT JOIN picking_list pl 
					ON pl.pl_id=rcvbrg.pl_id 
				LEFT JOIN picking_list_outbound plo 
					ON plo.pl_id=pl.pl_id 
				LEFT JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				LEFT JOIN cycle_count cc 
					ON cc.cc_id=rcvbrg.cc_id
				LEFT JOIN order_kitting ok
					ON rcvbrg.id_order_kitting=ok.id_order_kitting
				WHERE 
					rcvbrg.loc_id != \'104\' AND rcvbrg.loc_id != \'101\''.$where.'';
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$docRef = $row[$i]['kd_inbound'];
			$idRef = $row[$i]['id_inbound'];
			$ctrlRef = 'inbound/detail/';
			$locType = $row[$i]['loc_type'];
			
			if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('inbound_name'))){
				
				$docRef = $row[$i]['kd_inbound']; 
				$idRef = $row[$i]['id_inbound'];
				$ctrlRef = 'inbound/detail/';

				if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

					$docRef = $row[$i]['kd_order_kitting']; 
					$idRef = $row[$i]['id_order_kitting'];
					$ctrlRef = 'order_kitting/detail/';

				}

			}else if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){
				
				$docRef = $row[$i]['kd_order_kitting']; 
				$idRef = $row[$i]['id_order_kitting'];
				$ctrlRef = 'order_kitting/detail/';
				
			}else if(!empty($row[$i]['cc_id']) && empty($row[$i]['id_receiving'])){
				
				$docRef = $row[$i]['cc_code']; 
				$idRef = $row[$i]['cc_id'];
				$ctrlRef = 'cycle_count/detail/';
				
			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('outbound_name'))){
				
				$docRef = $row[$i]['kd_outbound']; 
				$idRef = $row[$i]['id_outbound'];
				$ctrlRef = 'outbound/detail/';
				
			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('transit_location'))){
				
				$sql = 'SELECT 
							id_receiving, cc_id, pl_id, shipping_id, id_qc, loc_id  
						FROM 
							receiving_barang 
						WHERE 
							kd_unik = \''.$row[$i]['kd_unik'].'\'';
							
				$r = $this->db->query($sql)->row_array();
				
				if(empty($r['pl_id'])){
					
					$docRef = $row[$i]['kd_inbound'];
					$idRef = $row[$i]['id_inbound'];
					$ctrlRef = 'inbound/detail/';
					
					$locType = $locType;
					
				}else if(!empty($r['cc_id']) && $r['id_qc'] == '4'){
					
					$docRef = $row[$i]['cc_code'];
					$idRef = $row[$i]['cc_id'];
					$ctrlRef = 'cycle_count/detail/';
					
				}else if(!empty($r['pl_id']) && empty($r['shipping_id'])){
					
					$docRef = $row[$i]['kd_outbound'];
					$idRef = $row[$i]['id_outbound'];
					$ctrlRef = 'outbound/detail/';
					
					$locType = $locType;
					
				}
				
			}
			
			$exp = '-';
			$dIn = '-';
			
			if(!empty($row[$i]['tgl_exp']) && $row[$i]['tgl_exp'] != '0000-00-00')
				$exp = date('d/m/Y', strtotime($row[$i]['tgl_exp']));
			
			if(!empty($row[$i]['tgl_in']) && $row[$i]['tgl_in'] != '0000-00-00 00:00:00')
				$dIn = date('d/m/Y', strtotime($row[$i]['tgl_in']));
			$kd_parent = !empty($row[$i]['kd_parent']) ? $row[$i]['kd_parent'] : '-';
			$nested = array();
			$nested[] = '<input type="checkbox" name="sn[]" value="'.$row[$i]['kd_unik'].'">';
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $kd_parent;
			$nested[] = $row[$i]['last_qty'];
			$nested[] = '<a href="'.base_url().$ctrlRef.$idRef.'">'.$docRef.'</a>';
			$nested[] = $exp;
			$nested[] = $dIn;
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $locType;
			$nested[] = $row[$i]['kd_qc'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}

	public function getDetailListExport($post=array()){

		$where = '';
		if(!empty($post['id_barang'])){
			$where .= ' AND rcvbrg.id_barang = \''.$post['id_barang'].'\' ';
		}
		
		$params = '';
		if(!empty($post['params']))
			$params = $post['params'];
		
		if($params == 'receiving')
			$where .= ' AND loc.loc_type IN ('.$this->config->item('rec_location_qty').') AND rcvbrg.pl_id IS NULL';
		
		if($params == 'onhand')
			$where .= ' AND loc.loc_type IN ('.$this->config->item('onhand_location_qty').') ';
		
		if($params == 'allocated')
			$where .= ' AND rcvbrg.pl_id IS NOT NULL ';
		
		if($params == 'suspend')
			$where .= ' AND rcvbrg.id_qc IN(\'2\', \'4\') ';
		
		if($params == 'damage')
			$where .= ' AND rcvbrg.id_qc IN(\'3\') ';
		
		if($params == 'available')
			$where .= ' AND rcvbrg.id_qc = \'1\' AND loc.loc_type = '.$this->config->item('available_location').' ';

		$sql = 'SELECT 
					* 
				FROM 
					receiving_barang rcvbrg
				LEFT JOIN m_loc loc 
					ON loc.loc_id = rcvbrg.loc_id 
				LEFT JOIN m_qc qc 
					ON qc.id_qc = rcvbrg.id_qc 
				LEFT JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving 
				LEFT JOIN inbound inb 
					ON inb.id_inbound=rcv.id_po 
				LEFT JOIN picking_list pl 
					ON pl.pl_id=rcvbrg.pl_id 
				LEFT JOIN picking_list_outbound plo 
					ON plo.pl_id=pl.pl_id 
				LEFT JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				LEFT JOIN cycle_count cc 
					ON cc.cc_id=rcvbrg.cc_id
				LEFT JOIN order_kitting ok
					ON rcvbrg.id_order_kitting=ok.id_order_kitting
				LEFT JOIN barang brg
					ON brg.id_barang=rcvbrg.id_barang
				WHERE 
					rcvbrg.loc_id != \'104\' AND rcvbrg.loc_id != \'101\''.$where.'';

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$docRef = $row[$i]['kd_inbound'];
			$locType = $row[$i]['loc_type'];
			
			if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('inbound_name'))){
				
				$docRef = $row[$i]['kd_inbound']; 

				if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){

					$docRef = $row[$i]['kd_order_kitting']; 

				}

			}else if(!empty($row[$i]['id_order_kitting']) && empty($row[$i]['id_receiving'])){
				
				$docRef = $row[$i]['kd_order_kitting']; 
				
			}else if(!empty($row[$i]['cc_id']) && empty($row[$i]['id_receiving'])){
				
				$docRef = $row[$i]['cc_code']; 
				
			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('outbound_name'))){
				
				$docRef = $row[$i]['kd_outbound']; 
				
			}else if($row[$i]['loc_type'] == str_replace("'", '', $this->config->item('transit_location'))){
				
				$sql = 'SELECT 
							id_receiving, cc_id, pl_id, shipping_id, id_qc, loc_id  
						FROM 
							receiving_barang 
						WHERE 
							kd_unik = \''.$row[$i]['kd_unik'].'\'';
							
				$r = $this->db->query($sql)->row_array();
				
				if(empty($r['pl_id'])){
					
					$docRef = $row[$i]['kd_inbound'];
					
					$locType = $locType;
					
				}else if(!empty($r['cc_id']) && $r['id_qc'] == '4'){
					
					$docRef = $row[$i]['cc_code'];
					
				}else if(!empty($r['pl_id']) && empty($r['shipping_id'])){
					
					$docRef = $row[$i]['kd_outbound'];
					
					$locType = $locType;
					
				}
				
			}
			
			$exp = '-';
			$dIn = '-';
			
			if(!empty($row[$i]['tgl_exp']) && $row[$i]['tgl_exp'] != '0000-00-00'){
				$exp = date('d/m/Y', strtotime($row[$i]['tgl_exp']));
			}
			
			if(!empty($row[$i]['tgl_in']) && $row[$i]['tgl_in'] != '0000-00-00 00:00:00'){
				$dIn = date('d/m/Y', strtotime($row[$i]['tgl_in']));
			}

			$kd_parent = !empty($row[$i]['kd_parent']) ? $row[$i]['kd_parent'] : '-';

			$nested = array();

			$nested[] = $i + 1;
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $kd_parent;
			$nested[] = $row[$i]['last_qty'];
			$nested[] = $docRef;
			$nested[] = $exp;
			$nested[] = $dIn;
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $locType;
			$nested[] = $row[$i]['kd_qc'];
			
			$data[] = $nested;
		}

		return $data;

	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		/*
			ADJ REMOVE THIS PARAMETER 
			WHERE 
				pl_status = 0 
			AND 
			
			MONITORING
		*/
		
		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'nama_kategori',
			4 => 'nama_satuan',
			5 => 'loc_name',
			6 => 'loc_type',
			7 => 'rb.qty',
			8 => 'minimum_stock'
		);
		
		$where = '';
		$wh = '';

		if(!empty($post['id_barang']))
			$where .= ' AND a.id_barang IN ('.implode(',', $post['id_barang']).') ';
		
		if(!empty($post['id_satuan']))
			$where .= ' AND a.id_satuan IN ('.implode(',', $post['id_satuan']).') ';
		
		if(!empty($post['id_qc']))
			$where .= ' AND rb.id_qc IN ('.implode(',', $post['id_qc']).') ';
		
		if(!empty($post['id_kategori']))
			$where .= ' AND a.id_kategori IN ('.implode(',', $post['id_kategori']).') ';
		
		if(!empty($post['loc_id'])){
			//$where .= ' AND CONCAT(\',\', rb.loc_id_all, \',\') LIKE \'%,'.implode(',', $post['loc_id']).',%\' ';
			//$where .= ' AND CONCAT(\',\', rb.loc_id_all, \',\') REGEXP \','.implode(',', $post['loc_id']).',\' ';
		
			//$where .= ' AND rb.loc_id_all REGEXP \''.implode(',', $post['loc_id']).'\' ';
			$locId = $post['loc_id'];
			$len = count($locId);
			
			for($i = 0; $i < $len; $i++){
				$where .= ' AND FIND_IN_SET(\''.$locId[$i].'\', rb.loc_id_all) > 0 ';
			}
		}
		
		if(!empty($post['loc_type'])){
			
			$locType = $post['loc_type'];
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				$where .= ' AND FIND_IN_SET(\''.$locType[$i].'\', rb.loc_type_all) > 0 ';
			}
			
			//$where .= ' AND FIND_IN_SET(\''.$post['loc_type'].'\', rb.loc_type_all) > 0 ';
		}
		
		$params = '';
		if(!empty($post['params']))
			$params = $post['params'];
		
		if($params == 'receiving'){
			
			$locTypes = $this->config->item('rec_location_qty');
			$locType = explode(',', $locTypes);
			
			$len = count($locType);
			
			$where .=' AND (';
			for($i = 0; $i < $len; $i++){
				if($i == 0){
					$where .= ' FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				}
				else
				{
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				}
			}
			$where .=')';

			
			$where .= ' AND rb.pl_id = 0 ';
			$wh .= ' AND loc_type IN ('.$locTypes.') ';
		}
		
		if($params == 'onhand'){

			$where .= ' AND rb.loc_id <> 100';
			$wh .= ' AND r.loc_id <> 100';

/*			$locType = $this->config->item('onhand_location_qty');
			$locType = explode(',', $locType);
			
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				if($i == 0)
					$where .= ' AND FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				else
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
			}			
*/
		}
		
		if($params == 'allocated'){
			$where .= ' AND rb.pl_id > 0 ';
			$wh .= ' AND pl_id > 0';
		}
		
		if($params == 'suspend'){
			$where .= ' AND FIND_IN_SET(\'2\', rb.id_qc_all) > 0 ';
			$where .= ' OR FIND_IN_SET(\'4\', rb.id_qc_all) > 0 ';
			$wh .= ' AND id_qc IN (2,4)';
		}

		if($params == 'damage'){
			$where .= ' AND FIND_IN_SET(\'3\', rb.id_qc_all) > 0 ';
			$wh .= ' AND id_qc IN (3)';
		}
		
		if($params == 'available'){
			$where .= ' AND FIND_IN_SET(\'1\', rb.id_qc_all) > 0 ';
			$where .= ' AND tbl.qty > 0';

			$where .= ' AND rb.loc_id <> 100';
			$wh .= ' AND r.loc_id <> 100';

/*			$locType = $this->config->item('available_location');
			$locType = explode(',', $locType);
			
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				if($i == 0)
					$where .= ' AND FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				else
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
			}
*/
		}
		
		if($params == 'low'){
			$where .= ' AND IFNULL(tbl.qty, 0) <= minimum_stock ';
		}
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					barang a 
				LEFT JOIN kategori b 
					ON a.id_kategori = b.id_kategori 
				LEFT JOIN kategori_2 d 
					ON d.id_kategori_2 = a.id_kategori_2 
				LEFT JOIN owner e 
					ON e.id_owner = a.id_owner 
				LEFT JOIN satuan c 
					ON c.id_satuan = a.id_satuan 
				LEFT JOIN 
				(
					SELECT 
						id_barang, r.loc_id, r.id_qc, 
						SUM(IFNULL(pl.pl_id, 0)) AS pl_id, 
						GROUP_CONCAT(DISTINCT id_qc ORDER BY r.id_qc ASC) AS id_qc_all,
						GROUP_CONCAT(DISTINCT r.loc_id ORDER BY r.loc_id ASC) AS loc_id_all,
						GROUP_CONCAT(DISTINCT loc_type ORDER BY r.loc_id ASC) AS loc_type_all,
						GROUP_CONCAT(DISTINCT loc_name ORDER BY r.loc_id ASC) AS loc_name,
						COUNT(kd_unik) AS qty 
					FROM 
						receiving_barang r 
					LEFT JOIN m_loc loc 
						ON loc.loc_id=r.loc_id 
					LEFT JOIN picking_list pl 
						ON pl.pl_id=r.pl_id
					WHERE 
						r.loc_id != \'104\' AND r.loc_id != \'101\' 
					'.$wh.'
					GROUP BY 
						id_barang
				) AS rb 
					ON a.id_barang = rb.id_barang 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id 
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=a.id_barang
				WHERE 1=1'.$where.'';
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT *, 
					a.id_barang AS id_barang, loc_name, loc_type_all, IFNULL(rb.qty, \'0\') AS qty    
				FROM 
					barang a 
				LEFT JOIN kategori b 
					ON a.id_kategori = b.id_kategori 
				LEFT JOIN kategori_2 d 
					ON d.id_kategori_2 = a.id_kategori_2 
				LEFT JOIN owner e 
					ON e.id_owner = a.id_owner 
				LEFT JOIN satuan c 
					ON c.id_satuan = a.id_satuan 
				LEFT JOIN 
				(
					SELECT 
						b.id_barang, r.loc_id, r.id_qc, 
						SUM(IFNULL(r.pl_id, 0)) AS pl_id, 
						GROUP_CONCAT(DISTINCT id_qc ORDER BY r.id_qc ASC) AS id_qc_all,
						GROUP_CONCAT(DISTINCT r.loc_id ORDER BY r.loc_id ASC) AS loc_id_all,
						GROUP_CONCAT(DISTINCT loc_type ORDER BY r.loc_id ASC) AS loc_type_all,
						GROUP_CONCAT(DISTINCT loc_name ORDER BY r.loc_id ASC SEPARATOR ", ") AS loc_name,
						IF(b.has_qty = 0,COUNT(kd_unik),sum(last_qty)) as QTY
					FROM 
						receiving_barang r 
					LEFT JOIN m_loc loc 
						ON loc.loc_id=r.loc_id
					LEFT JOIN barang b
						ON b.id_barang = r.id_barang
					WHERE
						r.loc_id != \'104\' and r.loc_id != \'101\'
					'.$wh.'
					AND
						(CASE WHEN b.has_qty=1 THEN r.last_qty >1 ELSE r.pl_id IS NULL END)
					GROUP BY 
						b.id_barang
				) AS rb 
					ON a.id_barang = rb.id_barang 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id 
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=a.id_barang
				WHERE 1=1'.$where.'';
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		//echo $this->db->last_query();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'item_stok/detail/'.$row[$i]['id_barang'].'">' . $row[$i]['kd_barang'] . '</a>';
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['nama_kategori'];
			$nested[] = $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['loc_type_all'];
			$nested[] = $row[$i]['qty'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['minimum_stock'] . ' ' . $row[$i]['nama_satuan'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function getListRaw($post = array()){
		$result = array();
		
		/*
			ADJ REMOVE THIS PARAMETER 
			WHERE 
				pl_status = 0 
			AND 
			
			MONITORING
		*/
		
		$where = '';
		if(!empty($post['id_barang']))
			$where .= ' AND a.id_barang IN ('.$post['id_barang'].') ';
		
		if(!empty($post['id_satuan']))
			$where .= ' AND a.id_satuan IN ('.$post['id_satuan'].') ';
		
		if(!empty($post['id_qc']))
			$where .= ' AND rb.id_qc IN ('.$post['id_qc'].') ';
		
		if(!empty($post['id_kategori']))
			$where .= ' AND a.id_kategori IN ('.$post['id_kategori'].') ';
		
		if(!empty($post['loc_id'])){
			
			$locId = $post['loc_id'];
			$locId = explode(',', $locId);
			$len = count($locId);
			
			for($i = 0; $i < $len; $i++){
				$where .= ' AND FIND_IN_SET(\''.$locId[$i].'\', rb.loc_id_all) > 0 ';
			}
		}
		
		if(!empty($post['loc_type'])){
			
			$locType = $post['loc_type'];
			$locType = explode(',', $locType);
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				$where .= ' AND FIND_IN_SET(\''.$locType[$i].'\', rb.loc_type_all) > 0 ';
			}
			
		}
		
		$params = '';
		if(!empty($post['params']))
			$params = $post['params'];
		
		if($params == 'receiving'){
			
			$locType = $this->config->item('rec_location_qty');
			$locType = explode(',', $locType);
			
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				if($i == 0)
					$where .= ' AND FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				else
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
			}
			
			$where .= ' AND rb.pl_id = 0 ';
		}
		
		if($params == 'onhand'){
			$locType = $this->config->item('onhand_location_qty');
			$locType = explode(',', $locType);
			
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				if($i == 0)
					$where .= ' AND FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				else
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
			}			
		}
		
		if($params == 'allocated')
			$where .= ' AND rb.pl_id > 0 ';
		
		if($params == 'suspend'){
			$where .= ' AND FIND_IN_SET(\'2\', rb.id_qc_all) > 0 ';
			$where .= ' OR FIND_IN_SET(\'4\', rb.id_qc_all) > 0 ';
		}

		if($params == 'damage')
			$where .= ' AND FIND_IN_SET(\'3\', rb.id_qc_all) > 0 ';
		
		if($params == 'available'){
			$where .= ' AND FIND_IN_SET(\'1\', rb.id_qc_all) > 0 ';
			
			$locType = $this->config->item('available_location');
			$locType = explode(',', $locType);
			
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				if($i == 0)
					$where .= ' AND FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				else
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
			}
		}
		
		if($params == 'low'){
			$where .= ' AND IFNULL(tbl.qty, 0) <= minimum_stock ';
		}
		
		$sql = 'SELECT *, 
					a.id_barang AS id_barang, loc_name, loc_type_all, IFNULL(rb.qty, \'0\') AS qty    
				FROM 
					barang a 
				LEFT JOIN kategori b 
					ON a.id_kategori = b.id_kategori 
				LEFT JOIN kategori_2 d 
					ON d.id_kategori_2 = a.id_kategori_2 
				LEFT JOIN owner e 
					ON e.id_owner = a.id_owner 
				LEFT JOIN satuan c 
					ON c.id_satuan = a.id_satuan 
				LEFT JOIN 
				(
					SELECT 
						b.id_barang, r.loc_id, r.id_qc, 
						SUM(IFNULL(r.pl_id, 0)) AS pl_id, 
						GROUP_CONCAT(DISTINCT id_qc ORDER BY r.id_qc ASC) AS id_qc_all,
						GROUP_CONCAT(DISTINCT r.loc_id ORDER BY r.loc_id ASC) AS loc_id_all,
						GROUP_CONCAT(DISTINCT loc_type ORDER BY r.loc_id ASC) AS loc_type_all,
						GROUP_CONCAT(DISTINCT loc_name ORDER BY r.loc_id ASC) AS loc_name,
						IF(b.has_qty = 0, COUNT(kd_unik),SUM(last_qty)) AS qty 
					FROM 
						receiving_barang r 
					LEFT JOIN m_loc loc 
						ON loc.loc_id=r.loc_id
					LEFT JOIN barang b
						ON b.id_barang = r.id_barang
					WHERE
						r.loc_id != \'104\' AND r.loc_id != \'101\' 
					GROUP BY 
						b.id_barang
				) AS rb 
					ON a.id_barang = rb.id_barang 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id 
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=a.id_barang
				WHERE 1=1'.$where.' 
				ORDER BY 
					a.kd_barang ASC';
		
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}

	public function getListRawFormat2($post = array()){
		$result = array();
		
		/*
			ADJ REMOVE THIS PARAMETER 
			WHERE 
				pl_status = 0 
			AND 
			
			MONITORING
		*/
		
		$where = '';
		if(!empty($post['id_barang']))
			$where .= ' AND a.id_barang IN ('.$post['id_barang'].') ';
		
		if(!empty($post['id_satuan']))
			$where .= ' AND a.id_satuan IN ('.$post['id_satuan'].') ';
		
		if(!empty($post['id_qc']))
			$where .= ' AND rb.id_qc IN ('.$post['id_qc'].') ';
		
		if(!empty($post['id_kategori']))
			$where .= ' AND a.id_kategori IN ('.$post['id_kategori'].') ';
		
		if(!empty($post['loc_id'])){
			
			$locId = $post['loc_id'];
			$locId = explode(',', $locId);
			$len = count($locId);
			
			for($i = 0; $i < $len; $i++){
				$where .= ' AND FIND_IN_SET(\''.$locId[$i].'\', rb.loc_id_all) > 0 ';
			}
		}
		
		if(!empty($post['loc_type'])){
			
			$locType = $post['loc_type'];
			$locType = explode(',', $locType);
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				$where .= ' AND FIND_IN_SET(\''.$locType[$i].'\', rb.loc_type_all) > 0 ';
			}
			
		}
		
		$params = '';
		if(!empty($post['params']))
			$params = $post['params'];
		
		if($params == 'receiving'){
			
			$locType = $this->config->item('rec_location_qty');
			$locType = explode(',', $locType);
			
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				if($i == 0)
					$where .= ' AND FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				else
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
			}
			
			$where .= ' AND rb.pl_id = 0 ';
		}
		
		if($params == 'onhand'){
			$locType = $this->config->item('onhand_location_qty');
			$locType = explode(',', $locType);
			
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				if($i == 0)
					$where .= ' AND FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				else
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
			}			
		}
		
		if($params == 'allocated')
			$where .= ' AND rb.pl_id > 0 ';
		
		if($params == 'suspend'){
			$where .= ' AND FIND_IN_SET(\'2\', rb.id_qc_all) > 0 ';
			$where .= ' OR FIND_IN_SET(\'4\', rb.id_qc_all) > 0 ';
		}

		if($params == 'damage')
			$where .= ' AND FIND_IN_SET(\'3\', rb.id_qc_all) > 0 ';
		
		if($params == 'available'){
			$where .= ' AND FIND_IN_SET(\'1\', rb.id_qc_all) > 0 ';
			
			$locType = $this->config->item('available_location');
			$locType = explode(',', $locType);
			
			$len = count($locType);
			
			for($i = 0; $i < $len; $i++){
				if($i == 0)
					$where .= ' AND FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
				else
					$where .= ' OR FIND_IN_SET('.$locType[$i].', rb.loc_type_all) > 0 ';
			}
		}
		
		if($params == 'low'){
			$where .= ' AND IFNULL(tbl.qty, 0) <= minimum_stock ';
		}
		
		$sql = 'SELECT *, 
					a.id_barang AS id_barang, loc_name, loc_type_all, IFNULL(rb.qty, \'0\') AS qty    
				FROM 
					barang a 
				LEFT JOIN kategori b 
					ON a.id_kategori = b.id_kategori 
				LEFT JOIN kategori_2 d 
					ON d.id_kategori_2 = a.id_kategori_2 
				LEFT JOIN owner e 
					ON e.id_owner = a.id_owner 
				LEFT JOIN satuan c 
					ON c.id_satuan = a.id_satuan 
				LEFT JOIN 
				(
					SELECT 
						b.id_barang, r.loc_id, r.id_qc, 
						SUM(IFNULL(r.pl_id, 0)) AS pl_id, 
						GROUP_CONCAT(DISTINCT id_qc ORDER BY r.id_qc ASC) AS id_qc_all,
						GROUP_CONCAT(DISTINCT r.loc_id ORDER BY r.loc_id ASC) AS loc_id_all,
						GROUP_CONCAT(DISTINCT loc_type ORDER BY r.loc_id ASC) AS loc_type_all,
						GROUP_CONCAT(DISTINCT loc_name ORDER BY r.loc_id ASC) AS loc_name,
						IF(b.has_qty = 0, COUNT(kd_unik),SUM(last_qty)) AS qty 
					FROM 
						receiving_barang r 
					LEFT JOIN m_loc loc 
						ON loc.loc_id=r.loc_id
					LEFT JOIN barang b
						ON b.id_barang = r.id_barang
					WHERE
						r.loc_id != \'104\' AND r.loc_id != \'101\' 
					GROUP BY 
						b.id_barang,loc.loc_id
				) AS rb 
					ON a.id_barang = rb.id_barang 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS qty,loc.loc_id
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id 
					GROUP BY 
						id_barang, loc.loc_id
				) AS tbl 
					ON tbl.id_barang=a.id_barang and tbl.loc_id = rb.loc_id
				WHERE 1=1'.$where.' 
				ORDER BY 
					a.kd_barang ASC';
		
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        //-----------end filtering-------------------

        $this->db->select('*');
        $this->db->from('receiving_barang a');//receiving_barang
        $this->db->join($this->loc . ' b',' a.loc_id = b.loc_id','left');//receiving_barang
        $this->db->join("receiving c", "a.id_receiving = c.id_receiving");
		$this->db->join('m_qc d', 'd.id_qc = a.id_qc', 'left'); //adji
        $this->db->order_by('a.kd_unik ASC');
        $this->db->where_condition($condition);
		
        return $this->db;
    }
    private function data_list($condition = array(),$tahun_aktif='') {
        $this->db->select('*');
        $this->db->from($this->table  . ' a');
       
        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_jumlah_awal($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_jumlah_son($condition = array()) {
        $this->db->select('sum(minus) as minus_son, sum(surplus) as surplus_son,id_barang');
        $this->db->from($this->table11  . ' a');
        $this->db->where_condition($condition);
        $this->db->group_by('id_barang');
        return $this->db;
    }

    public function get_by_id($id,$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $condition['a.id_barang'] = $id;
        $this->data($condition,$tahun_aktif);
        return $this->db->get();
    }

    public function get_data($condition = array(),$tahun_aktif) {
        $this->data($condition,$tahun_aktif);
        return $this->db->get();
    }

    
    public function data_table() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["a.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["a.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"] = null;
        }

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        //-----------end tahunaktif-------------------

        // Total Record
        $total = $this->data($condition,$tahun_aktif)->count_all_results();

        // List Data
        
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition,$tahun_aktif)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">';
                         
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('item_stok/edit/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('item_stok/delete/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';
            //$jumlah = $value->jumlah_awal + $value->jumlah_masuk - $value->jumlah_keluar + $value->surplus_son - $value->minus_son;
             switch ($value->shipment_type) {
                case 'FIFON':
                    $shipment_type = 'FIFO WITHOUT HAS EXP DATE';
                    break;
                case 'FIFOY':
                    $shipment_type = 'FIFO AND HAS EXP DATE';
                    break;
                case 'FEFOY':
                    $shipment_type = 'FEFO AND HAS EXPIRE DATE';
                    break;
                default:
                    $shipment_type = '';
                    # code...
                    break;
            };/*(
                'FIFON'=>'FIFO WITHOUT HAS EXP DATE',
                'FIFOY'=>'FIFO AND HAS EXP DATE',
                'FEFOY'=>'FEFO AND HAS EXP DATE'
                );*/
            $rows[] = array(
                    'primary_key' =>$value->id_barang,
                    'kd_barang' => anchor(null, $value->kd_barang, array('id' => 'drildown_key_t_item_stok_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_item_stok', 'data-source' => base_url('item_stok/get_detail_item_stok/' . $id))) ,
                    'nama_barang' => anchor(null, $value->nama_barang, array('id' => 'drildown_key_t_item_stok_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_item_stok', 'data-source' => base_url('item_stok/get_detail_item_stok/' . $id))),
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'qty' => $value->qty
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    
    public function data_table_excel() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["a.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["a.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"] = null;
        }

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        //-----------end tahunaktif-------------------

        // Total Record
        $total = $this->data($condition,$tahun_aktif)->count_all_results();

        // List Data
        
        $data = $this->data($condition,$tahun_aktif)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;
            $action = '';
            $jumlah = $value->jumlah_awal + $value->jumlah_masuk - $value->jumlah_keluar + $value->surplus_son - $value->minus_son;
                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'jumlah'=>$jumlah,
                    'jumlah_po'=>$value->jumlah_po
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

     public function data_table_detail() {
        $id_barang = $this->input->post('id_barang');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();


        foreach ($data_detail->result() as $value) {
            $id = $value->id_receiving_barang;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">';
            if ($this->access_right->otoritas('print')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('item_stok/print_single_label/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';
            
            $status = $value->pl_status==1 ? 'alocated':'';
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_unik' => $value->kd_unik,
                    'loc_name' => $value->loc_name,
                    'exp_date' => $value->tgl_exp,
                    'status' => $status,
                    'action' => $action
                );

        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_bbm_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbm <= '$tahun_aktif-12-31' and e.tanggal_bbm >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbm($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbm');
        $data_detail_bbm = $this->data_detail_bbm($condition)->get();
        $rows = array();

        foreach ($data_detail_bbm->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbm),
                    'kd_bbm' => $value->kd_bbm,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_bbk_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbk <= '$tahun_aktif-12-31' and e.tanggal_bbk >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbk');
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbk),
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_po_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbk <= '$tahun_aktif-12-31' and e.tanggal_bbk >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbk');
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbk),
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_barang' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_barang' => $id));
    }
    
    public function options_filter($default = '--Pilih Jenis Perawatan--', $key = '') {
        $this->db->order_by('a.id_barang');
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->jenis_perawatan;
        }
        return $options;
    }


    public function options($default = '--Choose Item--', $key = '') {
        $data = $this->data_list()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->kd_barang.' / '.$row->nama_barang ;
        }
        return $options;
    }

    public function get_location_list($post=array()){
    
    	$this->db->select('loc_id as id, loc_name as name')
    		->from('m_loc');

    	if(!empty($post['keyword'])){
	   		$this->db->like('loc_name', $post['keyword']);
    	}

    	$this->db->where_not_in('loc_type',array('SENTOUT','OUTBOUND'));

    	$data= $this->db->get();

    	return $data->result_array();
    }
    
}

?>
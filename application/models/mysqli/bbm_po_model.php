<?php
class bbm_po_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'bbm';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'bbm_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $table8 = 'purchase_order_barang';

    private function data() {
        // ==========Filtering==============
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');


        if(!empty($id_purchase_order)){
            $condition["c.kd_po like '%$id_purchase_order%'"]=null;
        }
        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["c.tanggal_po >= '$tanggal_awal'"] = null ;
            $condition["c.tanggal_po <= '$tanggal_akhir'"] = null;
        }

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["c.tanggal_po >= '$tahun_aktif_awal'"] = null ;
        $condition["c.tanggal_po <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        $this->db->from($this->table3 . ' c');
        $this->db->join($this->table2 . ' d','c.id_supplier = d.id_supplier');
        $this->db->order_by('c.tanggal_po DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail($condition = array()) {
        $this->db->select('*,b.id_po_barang as id_bbm_barang,sum(b.jumlah_barang) as jumlah_barang');
        $this->db->from($this->table3  . ' a');
        $this->db->join($this->table8 . ' b',' a.id_po = b.id_po');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->join($this->table2 . ' f',' a.id_supplier = f.id_supplier','left');
        //$this->db->join($this->table3 . ' g',' a.id_po = g.id_po','left');
        $this->db->group_by(' c.id_barang ');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_po_bbm($condition = array()) {
        $this->db->select('*,b.id as id_bbm_barang,jumlah_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_bbm = b.id_bbm');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->join($this->table2 . ' f',' a.id_supplier = f.id_supplier','left');
        $this->db->join($this->table3 . ' g',' a.id_po = g.id_po','left');
        //$this->db->group_by(' c.id_barang ');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['c.id_po'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_po;
            $action = '';
            $action .= anchor(null, '<i class="icon-cog"></i>', array('id' => 'drildown_key_t_bbm_' . $id, 'class' => 'btn', 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-original-title' => 'Detil Proses',  'data-source' => base_url('bbm_po/get_detail_po/' . $id))) . ' ';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_po,
                    'tanggal_po' => hgenerator::switch_tanggal($value->tanggal_po),
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_po,
                    'tanggal_po' => hgenerator::switch_tanggal($value->tanggal_po),
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function data_table_detail() {
        // Filtering
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_po = $this->input->post('id_po');

        $id_bbm = $this->input->post('id_bbm');

        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }


        
        // Total Record
        $data_total = $this->data_detail($condition)->get();
        $total = $data_total->num_rows();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail($condition)->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';


            $total = $value->harga_barang * $value->jumlah_barang;
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'harga' => hgenerator::rupiah($value->harga_barang),
                    'total' => hgenerator::rupiah($total)
                );
            }else{
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'harga' => hgenerator::rupiah($value->harga_barang),
                    'total' => hgenerator::rupiah($total)
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    } 

    public function data_table_detail_po_bbm() {
        // Filtering
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_po = $this->input->post('id_po');

        $id_bbm = $this->input->post('id_bbm');

        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }


        
        // Total Record
        $data_total = $this->data_detail_po_bbm($condition)->get();
        $total = $data_total->num_rows();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail_po_bbm($condition)->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';



            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $id,
                    'tanggal_bbm' => $value->tanggal_bbm,
                    'kd_bbm' => $value->kd_bbm,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,                   
                    'jumlah' => $value->jumlah_barang,
                    
                );
            }else{
                $rows[] = array(
                    'primary_key' => $id,
                    'tanggal_bbm' => $value->tanggal_bbm,
                    'kd_bbm' => $value->kd_bbm,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,                    
                    'jumlah' => $value->jumlah_barang,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    } 

    public function data_table_excel() {
        // Filtering
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');


        if(!empty($id_purchase_order)){
            $condition["c.id_po"]=$id_purchase_order;
        }
        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["c.tanggal_po >= '$tanggal_awal'"] = null ;
            $condition["c.tanggal_po <= '$tanggal_akhir'"] = null;
        }
        
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["c.tanggal_po >= '$tahun_aktif_awal'"] = null ;
        $condition["c.tanggal_po <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_po;
            $action = '';
                $rows[] = array(
                    'primary_key' =>$value->id_po,
                    'tanggal_po' => hgenerator::switch_tanggal($value->tanggal_po),
                    'kd_po' => $value->kd_po,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function data_table_detail_excel() {
        // Filtering
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_po = $this->input->post('id_po');

        $id_bbm = $this->input->post('id_bbm');

        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }

        // Total Record
        $data_total = $this->data_detail($condition)->get();
        $total = $data_total->num_rows();

        // List Data
        $data_detail = $this->data_detail($condition)->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }   

    
    


    
}

?>
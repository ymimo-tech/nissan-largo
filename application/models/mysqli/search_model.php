<?php
class search_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getLoadingBySn($id){
		$result = array();
		
		$sql = 'SELECT 
					sp.shipping_id, shipping_code, DATE_FORMAT(shipping_date, \'%d\/%m\/%Y\') AS shipping_date, 
					driver_name, sp.license_plate, st_shipping_name AS status,
					DATE_FORMAT(shipping_start, \'%d\/%m\/%Y %H:%i\') AS shipping_start,
					DATE_FORMAT(shipping_finish, \'%d\/%m\/%Y %H:%i\') AS shipping_finish,
					user_name 
				FROM 
					receiving_barang rcvbrg
				JOIN shipping sp 
					ON sp.shipping_id=rcvbrg.shipping_id 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				JOIN m_shipping msp 
					ON msp.st_shipping=sp.st_shipping
				LEFT JOIN hr_user hu 
					ON hu.user_id=sp.user_id
				WHERE 
					rcvbrg.kd_unik=\''.$id.'\' 
				ORDER BY 
					sp.shipping_id DESC 
				LIMIT 1';
		
		$result = $this->db->query($sql)->row_array();
		return $result;
	}
	
	public function getPickingBySn($id){
		$result = array();
		
		$sql = 'SELECT 
					pl.pl_id, pl_name, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS pl_date,
					DATE_FORMAT(pl_start, \'%d\/%m\/%Y %H:%i\') AS pl_start,
					DATE_FORMAT(pl_finish, \'%d\/%m\/%Y %H:%i\') AS pl_finish,
					user_name,
					(
						SELECT 
							nama_status 
						FROM 
							m_status_picking 
						WHERE 
							id_status_picking = (
								CASE
									WHEN pl_start IS NULL 
										THEN \'1\' 
									WHEN pl_start IS NOT NULL AND pl_finish IS NULL 
										THEN \'2\' 
									WHEN pl_start IS NOT NULL AND pl_finish IS NOT NULL 
										THEN \'3\' 
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NULL 
										THEN \'4\' 
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NOT NULL 
										THEN \'5\' 
								END
							)
					) AS status  					
				FROM 
					receiving_barang rcvbrg
				JOIN picking_list pl 
					ON pl.pl_id=rcvbrg.pl_id 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				JOIN m_status_picking msp 
					ON msp.id_status_picking=pl.id_status_picking 
				LEFT JOIN shipping_picking shippick 
					ON shippick.pl_id=pl.pl_id 
				LEFT JOIN shipping shipp 
					ON shipp.shipping_id=shippick.shipping_id
				LEFT JOIN hr_user hu 
					ON hu.user_id=pl.user_id 
				WHERE 
					rcvbrg.kd_unik=\''.$id.'\'
				ORDER BY 
					pl.pl_id DESC 
				LIMIT 1';
		
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function getOutboundBySn($id){
		$result = array();
		
		$sql = 'SELECT 
					outb.id_outbound, kd_outbound, DATE_FORMAT(tanggal_outbound, \'%d/%m/%Y\') AS tanggal_outbound,
					outb.id_outbound_document, outbound_document_name, 
					kd_supplier, nama_supplier, alamat_supplier, telepon_supplier, cp_supplier,
					customer_name, address, phone, nama_status, jumlah_barang, user_name 
				FROM 
					outbound outb 
				JOIN outbound_barang outbbrg 
					ON outbbrg.id_outbound=outb.id_outbound 
				JOIN barang brg 
					ON brg.id_barang=outbbrg.id_barang 
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.id_barang=brg.id_barang 
				JOIN shipping shipp 
					ON shipp.shipping_id=rcvbrg.shipping_id
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_status_outbound msoutb 
					ON msoutb.id_status_outbound=outb.id_status_outbound 
				LEFT JOIN m_outbound_document moutbd 
					ON moutbd.id_outbound_document=outb.id_outbound_document 
				LEFT JOIN hr_user hu 
					ON hu.user_id=outb.user_id
				WHERE 
					rcvbrg.kd_unik=\''.$id.'\' 
				ORDER BY 
					outb.id_outbound DESC 
				LIMIT 1';
		
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function getReceivedBySn($id){
		$result = array();
		
		$sql = 'SELECT 
					rcv.id_receiving, inb.id_inbound, kd_inbound, kd_receiving, 
					IFNULL(vehicle_plate,\'-\') AS vehicle_plate,
					DATE_FORMAT(tanggal_receiving, \'%d\/%m\/%Y\') AS tanggal_receiving,
					DATE_FORMAT(tanggal_receiving, \'%H:%i\') AS time_receiving,
					dock, delivery_doc,
					DATE_FORMAT(tanggal_inbound, \'%d\/%m\/%Y\') AS tanggal_inbound,
					hui.user_name, 
					(
						CASE  
							WHEN start_tally IS NULL
								THEN \'-\' 
							WHEN start_tally IS NOT NULL AND finish_tally IS NULL 
								THEN CONCAT(\'Started\',\' at \',
									(
										CASE 
											WHEN DATE(start_tally) != DATE(tanggal_receiving) 
												THEN DATE_FORMAT(start_tally, \'%d-%m-%Y %H:%i\') 
											ELSE
												DATE_FORMAT(start_tally, \'%H:%i\')
										END
									)
								)
							WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL 
								THEN CONCAT(\'Finished\',\' at \',
									(
										CASE 
											WHEN DATE(finish_tally) != DATE(tanggal_receiving) 
												THEN DATE_FORMAT(finish_tally, \'%d-%m-%Y %H:%i\') 
											ELSE
												DATE_FORMAT(finish_tally, \'%H:%i\')
										END
									)
								)
						END
					) AS tally,
					(
						CASE  
							WHEN start_putaway IS NULL
								THEN \'-\' 
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL 
								THEN CONCAT(\'Started\',\' at \',
									(
										CASE 
											WHEN DATE(start_putaway) != DATE(tanggal_receiving) 
												THEN DATE_FORMAT(start_putaway, \'%d-%m-%Y %H:%i\') 
											ELSE
												DATE_FORMAT(start_putaway, \'%H:%i\')
										END
									)
								)
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL 
								THEN CONCAT(\'Finished\',\' at \',
									(
										CASE 
											WHEN DATE(finish_putaway) != DATE(tanggal_receiving) 
												THEN DATE_FORMAT(finish_putaway, \'%d-%m-%Y %H:%i\') 
											ELSE
												DATE_FORMAT(finish_putaway, \'%H:%i\')
										END
									)
								)
						END
					) AS putaway,
					(
						SELECT 
							nama_status 
						FROM 
							m_status_receiving 
						WHERE 
							id_status = (
								CASE
									WHEN start_tally IS NULL 
										THEN \'1\' 
									WHEN start_tally IS NOT NULL AND finish_tally IS NULL 
										THEN \'2\' 
									WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
										THEN \'3\' 
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL 
										THEN \'4\'
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL 
										THEN \'5\' 
								END
							)
					) AS status
				FROM 
					receiving_barang rcvbrg 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				JOIN receiving_qty rcvqty 
					ON brg.id_barang=rcvqty.id_barang 
				JOIN receiving rcv
					ON rcv.id_receiving=rcvbrg.id_receiving
				LEFT JOIN inbound inb 
					ON inb.id_inbound=rcv.id_po 
				LEFT JOIN hr_user hui 
					ON hui.user_id=inb.user_id
				WHERE 
					rcvbrg.kd_unik=\''.$id.'\' 
				ORDER BY 
					rcv.id_receiving DESC 
				LIMIT 1';
		
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function getInboundBySn($id){
		$result = array();
		/*
		$sql = 'SELECT 
					inb.id_inbound, kd_inbound, DATE_FORMAT(tanggal_inbound, \'%d\/%m\/%Y\') AS tanggal_inbound, 
					kd_supplier, nama_supplier, alamat_supplier, telepon_supplier, cp_supplier, 
					customer_name, address, phone, 
					inb.id_inbound_document, 
					inbound_document_name, nama_status, jumlah_barang, user_name
				FROM 
					receiving_barang rcvbrg 
				LEFT JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving
				LEFT JOIN inbound inb 
					ON inb.id_inbound=rcv.id_po
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=inb.id_supplier 
						AND inb.id_inbound_document != \'2\'
						OR inb.id_inbound_document != \'7\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=inb.id_supplier
						AND inb.id_inbound_document = \'2\'
						OR inb.id_inbound_document = \'7\'
				LEFT JOIN m_inbound_document mid 
					ON mid.id_inbound_document=inb.id_inbound_document 
				LEFT JOIN m_status_inbound msi 
					ON msi.id_status_inbound=inb.id_status_inbound 
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound 
				LEFT JOIN barang brg 
					ON brg.id_barang=inbbrg.id_barang 
				LEFT JOIN hr_user hr
					ON hr.user_id=inb.user_id
				WHERE 
					rcvbrg.kd_unik=\''.$id.'\' 
				ORDER BY 
					inb.id_inbound DESC 
				LIMIT 1';
		*/
		
		$sql = 'SELECT 
					inb.id_inbound, kd_inbound, DATE_FORMAT(tanggal_inbound, \'%d\/%m\/%Y\') AS tanggal_inbound, 
					kd_supplier, nama_supplier, alamat_supplier, telepon_supplier, cp_supplier, 
					customer_name, address, phone, 
					inb.id_inbound_document, 
					inbound_document_name, nama_status, jumlah_barang, user_name
				FROM 
					receiving_barang rcvbrg 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				JOIN inbound_barang inbbrg
					ON inbbrg.id_barang=brg.id_barang 
				JOIN inbound inb 
					ON inb.id_inbound=inbbrg.id_inbound 
				JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=inb.id_supplier 
						AND inb.id_inbound_document NOT IN(\'2\', \'7\')
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=inb.id_supplier
						AND inb.id_inbound_document IN(\'2\', \'7\')
				LEFT JOIN m_inbound_document mid 
					ON mid.id_inbound_document=inb.id_inbound_document 
				LEFT JOIN m_status_inbound msi 
					ON msi.id_status_inbound=inb.id_status_inbound 
				LEFT JOIN hr_user hr
					ON hr.user_id=inb.user_id
				WHERE 
					rcvbrg.kd_unik=\''.$id.'\' 
				ORDER BY 
					inb.id_inbound DESC 
				LIMIT 1';
					
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function getItemBySn($id){
		$result = array();
		
		$sql = 'SELECT 
					brg.id_barang, kd_barang, nama_barang,
					shipment_type, fifo_period,
					nama_satuan, DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\') AS tgl_in, 
					DATE_FORMAT(tgl_exp, \'%d\/%m\/%Y\') AS tgl_exp,
					kd_kategori, nama_kategori, kd_kategori_2, nama_kategori_2, owner_name, kd_qc,
					loc_name, loc_type
				FROM 
					receiving_barang rcvbrg 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				JOIN m_loc loc 
					ON loc.loc_id=rcvbrg.loc_id
				LEFT JOIN kategori kat 
					ON kat.id_kategori=brg.id_kategori
				LEFT JOIN kategori_2 kat2 
					ON kat2.id_kategori_2=brg.id_kategori_2
				LEFT JOIN owner own 
					ON own.id_owner=brg.id_owner
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan
				LEFT JOIN m_qc qc
					ON qc.id_qc=rcvbrg.id_qc
				WHERE 
					rcvbrg.kd_unik=\''.$id.'\'';
					
		$result = $this->db->query($sql)->row_array();		
		if($result['tgl_exp'] == '00/00/0000')
			$result['tgl_exp'] = '-';
		
		return $result;
	}
	
	public function getReceivedList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'kd_unik',
			2 => 'tgl_exp',
			3 => 'tgl_in',
			4 => 'loc_name',
			5 => 'status',
			6 => 'user_name'
		);
		
		$where = '';
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					receiving_barang rcvbrg 
				LEFT JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving 
				LEFT JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN m_loc mc
					ON mc.loc_id=rcvbrg.loc_id 
				LEFT JOIN m_qc qc 
					ON qc.id_qc=rcvbrg.id_qc 
				LEFT JOIN hr_user hu 
					ON hu.user_id=rcvbrg.user_id_putaway
				WHERE 
					rcvbrg.id_receiving=\''.$post['id_receiving'].'\' 
				AND 	
					rcvbrg.id_barang=\''.$post['id_barang'].'\'';
				
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					kd_unik, IFNULL(DATE_FORMAT(tgl_exp, \'%d\/%m\/%Y\'), \'-\') AS tgl_exp,
					DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\') AS tgl_in,
					loc_name, kd_qc, IFNULL(hu.user_name, hu1.user_name) AS user_name 
				FROM 
					receiving_barang rcvbrg 
				LEFT JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving 
				LEFT JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN m_loc mc
					ON mc.loc_id=rcvbrg.loc_id 
				LEFT JOIN m_qc qc 
					ON qc.id_qc=rcvbrg.id_qc
				LEFT JOIN hr_user hu 
					ON hu.user_id=rcvbrg.user_id_putaway
				LEFT JOIN hr_user hu1 
					ON hu1.user_id=rcvbrg.user_id_receiving
				WHERE 
					rcvbrg.id_receiving=\''.$post['id_receiving'].'\' 
				AND 	
					rcvbrg.id_barang=\''.$post['id_barang'].'\'';
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		$action = '';
		
		for($i=0;$i<$totalFiltered;$i++){
			
			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['kd_qc'];
			$nested[] = $row[$i]['user_name'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}

	public function getReceivedListExcel($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$sql = 'SELECT 
					kd_unik, IFNULL(DATE_FORMAT(tgl_exp, \'%d\/%m\/%Y\'), \'-\') AS tgl_exp,
					DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\') AS tgl_in,
					loc_name, kd_qc, IFNULL(hu.user_name, hu1.user_name) AS user_name 
				FROM 
					receiving_barang rcvbrg 
				LEFT JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving 
				LEFT JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN m_loc mc
					ON mc.loc_id=rcvbrg.loc_id 
				LEFT JOIN m_qc qc 
					ON qc.id_qc=rcvbrg.id_qc
				LEFT JOIN hr_user hu 
					ON hu.user_id=rcvbrg.user_id_putaway
				LEFT JOIN hr_user hu1 
					ON hu1.user_id=rcvbrg.user_id_receiving
				WHERE 
					rcvbrg.id_receiving=\''.$post['id_receiving'].'\' 
				AND 	
					rcvbrg.id_barang=\''.$post['id_barang'].'\'';
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		$action = '';
		
		for($i=0;$i<$totalFiltered;$i++){
			
			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['kd_qc'];
			$nested[] = $row[$i]['user_name'];
			
			$data[] = $nested;
		}
		
		return $data;
	}
	
	public function getLastOutbound($id){
		$result = array();
		
		$sql = 'SELECT 
					outb.id_outbound, kd_outbound, DATE_FORMAT(tanggal_outbound, \'%d/%m/%Y\') AS tanggal_outbound,
					outb.id_outbound_document, outbound_document_name, 
					kd_supplier, nama_supplier, alamat_supplier, telepon_supplier, cp_supplier,
					customer_name, address, phone, nama_status, jumlah_barang, user_name 
				FROM 
					outbound outb 
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_status_outbound msoutb 
					ON msoutb.id_status_outbound=outb.id_status_outbound 
				LEFT JOIN m_outbound_document moutbd 
					ON moutbd.id_outbound_document=outb.id_outbound_document 
				LEFT JOIN outbound_barang outbbrg 
					ON outbbrg.id_outbound=outb.id_outbound 
				LEFT JOIN barang brg 
					ON brg.id_barang=outbbrg.id_barang
				LEFT JOIN hr_user hu 
					ON hu.user_id=outb.user_id
				WHERE 
					brg.id_barang=\''.$id.'\' 
				ORDER BY 
					outb.id_outbound DESC 
				LIMIT 1';
		
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function getLastReceived($id){
		$result = array();
		
		$sql = 'SELECT 
					rcv.id_receiving, inb.id_inbound, kd_inbound, kd_receiving, 
					IFNULL(vehicle_plate,\'-\') AS vehicle_plate,
					DATE_FORMAT(tanggal_receiving, \'%d\/%m\/%Y\') AS tanggal_receiving,
					DATE_FORMAT(tanggal_receiving, \'%H:%i\') AS time_receiving,
					dock, delivery_doc,
					DATE_FORMAT(tanggal_inbound, \'%d\/%m\/%Y\') AS tanggal_inbound,
					hui.user_name, 
					(
						CASE  
							WHEN start_tally IS NULL
								THEN \'-\' 
							WHEN start_tally IS NOT NULL AND finish_tally IS NULL 
								THEN CONCAT(\'Started\',\' at \',
									(
										CASE 
											WHEN DATE(start_tally) != DATE(tanggal_receiving) 
												THEN DATE_FORMAT(start_tally, \'%d-%m-%Y %H:%i\') 
											ELSE
												DATE_FORMAT(start_tally, \'%H:%i\')
										END
									)
								)
							WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL 
								THEN CONCAT(\'Finished\',\' at \',
									(
										CASE 
											WHEN DATE(finish_tally) != DATE(tanggal_receiving) 
												THEN DATE_FORMAT(finish_tally, \'%d-%m-%Y %H:%i\') 
											ELSE
												DATE_FORMAT(finish_tally, \'%H:%i\')
										END
									)
								)
						END
					) AS tally,
					(
						CASE  
							WHEN start_putaway IS NULL
								THEN \'-\' 
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL 
								THEN CONCAT(\'Started\',\' at \',
									(
										CASE 
											WHEN DATE(start_putaway) != DATE(tanggal_receiving) 
												THEN DATE_FORMAT(start_putaway, \'%d-%m-%Y %H:%i\') 
											ELSE
												DATE_FORMAT(start_putaway, \'%H:%i\')
										END
									)
								)
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL 
								THEN CONCAT(\'Finished\',\' at \',
									(
										CASE 
											WHEN DATE(finish_putaway) != DATE(tanggal_receiving) 
												THEN DATE_FORMAT(finish_putaway, \'%d-%m-%Y %H:%i\') 
											ELSE
												DATE_FORMAT(finish_putaway, \'%H:%i\')
										END
									)
								)
						END
					) AS putaway,
					(
						SELECT 
							nama_status 
						FROM 
							m_status_receiving 
						WHERE 
							id_status = (
								CASE
									WHEN start_tally IS NULL 
										THEN \'1\' 
									WHEN start_tally IS NOT NULL AND finish_tally IS NULL 
										THEN \'2\' 
									WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
										THEN \'3\' 
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL 
										THEN \'4\'
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL 
										THEN \'5\' 
								END
							)
					) AS status
				FROM 
					barang brg 
				JOIN receiving_qty rcvqty 
					ON brg.id_barang=rcvqty.id_barang 
				LEFT JOIN receiving rcv
					ON rcv.id_receiving=rcvqty.id_receiving
				LEFT JOIN inbound inb 
					ON inb.id_inbound=rcv.id_po 
				LEFT JOIN hr_user hui 
					ON hui.user_id=inb.user_id
				WHERE 
					brg.id_barang=\''.$id.'\'
				ORDER BY 
					id_receiving DESC 
				LIMIT 1';
		
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function getLastInbound($id){
		$result = array();
		
		$sql = 'SELECT 
					kd_inbound, DATE_FORMAT(tanggal_inbound, \'%d\/%m\/%Y\') AS tanggal_inbound, 
					kd_supplier, nama_supplier, alamat_supplier, telepon_supplier, cp_supplier, 
					customer_name, address, phone, 
					inb.id_inbound_document, 
					inbound_document_name, nama_status, jumlah_barang, user_name
				FROM 
					inbound inb 
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=inb.id_supplier 
						AND inb.id_inbound_document NOT IN(\'2\', \'7\')
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=inb.id_supplier
						AND inb.id_inbound_document IN(\'2\', \'7\')
				LEFT JOIN m_inbound_document mid 
					ON mid.id_inbound_document=inb.id_inbound_document 
				LEFT JOIN m_status_inbound msi 
					ON msi.id_status_inbound=inb.id_status_inbound 
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound 
				LEFT JOIN barang brg 
					ON brg.id_barang=inbbrg.id_barang
				LEFT JOIN hr_user hr
					ON hr.user_id=inb.user_id
				WHERE 
					brg.id_barang=\''.$id.'\' 
				ORDER BY 
					inb.id_inbound DESC 
				LIMIT 1';
					
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function getResult($post = array()){
		$result = array();
		$rows = array();
		$data = array();
		
		/*
		UNION
		(
			SELECT 
				brg.id_barang AS id, 
				CONCAT(IFNULL(kd_unik, \'\'), \' \', CONCAT(\'(\', kd_barang, CONCAT(\') \', \' \', nama_barang))) AS code, 
				\'ITEM\' as type 
			FROM 
				barang brg 
			LEFT JOIN receiving_barang rcvbrg
				ON rcvbrg.id_barang=brg.id_barang 
			LEFT JOIN receiving rcv 
				ON rcv.id_receiving=rcvbrg.id_receiving
			WHERE 
				LOWER(kd_unik) LIKE \'%'.$post['keyword'].'%\' OR LOWER(kd_barang) LIKE \'%'.$post['keyword'].'%\'
				OR LOWER(nama_barang) LIKE \'%'.$post['keyword'].'%\'
		)
		*/
		
		$sql = '(
					SELECT 
						id_inbound AS id, kd_inbound AS code, 
						\'INBOUND\' as type 
					FROM 
						inbound inb 
					LEFT JOIN m_inbound_document inbdoc 
						ON inbdoc.id_inbound_document=inb.id_inbound_document 
					LEFT JOIN m_status_inbound inbstatus 
						ON inbstatus.id_status_inbound=inb.id_status_inbound
					WHERE 
						LOWER(kd_inbound) LIKE \'%'.$post['keyword'].'%\'
				) 
				UNION
				(
					SELECT 
						id_receiving AS id, kd_receiving AS code, 
						\'RECEIVING\' as type 
					FROM 
						receiving
					WHERE 
						LOWER(kd_receiving) LIKE \'%'.$post['keyword'].'%\'
				) 
				UNION
				(
					SELECT 
						id_outbound AS id, kd_outbound AS code, 
						\'OUTBOUND\' as type 
					FROM 
						outbound outb 
					LEFT JOIN m_outbound_document outbdoc 
						ON outbdoc.id_outbound_document=outb.id_outbound_document
					WHERE 
						LOWER(kd_outbound) LIKE \'%'.$post['keyword'].'%\'
				)
				UNION
				(
					SELECT 
						pl_id AS id, pl_name AS code, 
						\'PICKING\' as type 
					FROM 
						picking_list
					WHERE 
						LOWER(pl_name) LIKE \'%'.$post['keyword'].'%\'
				)
				UNION
				(
					SELECT 
						shipping_id AS id, shipping_code AS code, 
						\'LOADING\' as type 
					FROM 
						shipping
					WHERE 
						LOWER(shipping_code) LIKE \'%'.$post['keyword'].'%\'
				)
				UNION
				(
					SELECT 
						brg.id_barang AS id, 
						CONCAT(\'(\', kd_barang, CONCAT(\') \', \' \', nama_barang)) AS code, 
						\'ITEM\' as type 
					FROM 
						barang brg 
					WHERE 
						LOWER(kd_barang) LIKE \'%'.$post['keyword'].'%\'
						OR LOWER(nama_barang) LIKE \'%'.$post['keyword'].'%\'
				)
				UNION
				(
					SELECT 
						kd_unik AS id, 
						CONCAT(IFNULL(kd_unik, \'\'), \' \', CONCAT(\'(\', kd_barang, CONCAT(\') \', \' \', nama_barang))) AS code, 
						\'SN\' as type 
					FROM 
						receiving_barang rcvbrg 
					JOIN barang brg 
						ON brg.id_barang=rcvbrg.id_barang
					WHERE 
						LOWER(kd_unik) LIKE \'%'.$post['keyword'].'%\' OR LOWER(kd_barang) LIKE \'%'.$post['keyword'].'%\'
						OR LOWER(nama_barang) LIKE \'%'.$post['keyword'].'%\'
				)
				UNION
				(
					SELECT 
						id_supplier AS id, CONCAT(kd_supplier, \' - \', nama_supplier) AS code, 
						\'SUPPLIER\' as type 
					FROM 
						supplier
					WHERE 
						LOWER(kd_supplier) LIKE \'%'.$post['keyword'].'%\' OR LOWER(nama_supplier) LIKE \'%'.$post['keyword'].'%\' 
				)';
				
		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		
		for($i = 0; $i < $len; $i++){
			if($row[$i]['type'] == 'INBOUND'){
				
				$data['INBOUND'][] = array(
					'id'	=> $row[$i]['id'],
					'text'	=> $row[$i]['code'],
					'type'	=> 'INBOUND'
				);
				
			}
			
			if($row[$i]['type'] == 'RECEIVING'){
				
				$data['RECEIVING'][] = array(
					'id'	=> $row[$i]['id'],
					'text'	=> $row[$i]['code'],
					'type'	=> 'RECEIVING'
				);
			}
			
			if($row[$i]['type'] == 'OUTBOUND'){
				
				$data['OUTBOUND'][] = array(
					'id'	=> $row[$i]['id'],
					'text'	=> $row[$i]['code'],
					'type'	=> 'OUTBOUND'
				);
			}
			
			if($row[$i]['type'] == 'PICKING'){
				
				$data['PICKING'][] = array(
					'id'	=> $row[$i]['id'],
					'text'	=> $row[$i]['code'],
					'type'	=> 'PICKING'
				);
			}
			
			if($row[$i]['type'] == 'LOADING'){
				
				$data['LOADING'][] = array(
					'id'	=> $row[$i]['id'],
					'text'	=> $row[$i]['code'],
					'type'	=> 'LOADING'
				);
			}
			
			if($row[$i]['type'] == 'ITEM'){
				
				$data['ITEM'][] = array(
					'id'	=> $row[$i]['id'],
					'text'	=> $row[$i]['code'],
					'type'	=> 'ITEM'
				);
			}
			
			if($row[$i]['type'] == 'SN'){
				
				$data['SN'][] = array(
					'id'	=> $row[$i]['id'],
					'text'	=> $row[$i]['code'],
					'type'	=> 'SN'
				);
			}
			
			if($row[$i]['type'] == 'SUPPLIER'){
				
				$data['SUPPLIER'][] = array(
					'id'	=> $row[$i]['id'],
					'text'	=> $row[$i]['code'],
					'type'	=> 'SUPPLIER'
				);
			}
		}
		
		foreach($data as $k => $v){
			if($k == 'INBOUND'){
				$rows[] = array(
					'text'		=> 'INBOUND',
					'children'	=> $data[$k]
				);
			}
			
			if($k == 'RECEIVING'){
				$rows[] = array(
					'text'		=> 'RECEIVING',
					'children'	=> $data[$k]
				);
			}
			
			if($k == 'OUTBOUND'){
				$rows[] = array(
					'text'		=> 'OUTBOUND',
					'children'	=> $data[$k]
				);
			}
			
			if($k == 'PICKING'){
				$rows[] = array(
					'text'		=> 'PICKING',
					'children'	=> $data[$k]
				);
			}
			
			if($k == 'LOADING'){
				$rows[] = array(
					'text'		=> 'LOADING',
					'children'	=> $data[$k]
				);
			}
			
			if($k == 'ITEM'){
				$rows[] = array(
					'text'		=> 'ITEM',
					'children'	=> $data[$k]
				);
			}
			
			if($k == 'SN'){
				$rows[] = array(
					'text'		=> 'SN',
					'children'	=> $data[$k]
				);
			}
			
			if($k == 'SUPPLIER'){
				$rows[] = array(
					'text'		=> 'SUPPLIER',
					'children'	=> $data[$k]
				);
			}
		}
		
		$result['status'] = 'OK';
		$result['result'] = $rows;
		
		return $result;
	}
	
	public function getSearchResult($post = array()){
		$result = array();
		
		$sql = '(
					SELECT 
						id_inbound AS id, kd_inbound AS code, 
						DATE_FORMAT(tanggal_inbound, \'%d\/%m\/%Y\') AS date, 
						CONCAT_WS(\',\', inbound_document_name, nama_status) AS description, 
						\'INBOUND\' as type 
					FROM 
						inbound inb 
					LEFT JOIN m_inbound_document inbdoc 
						ON inbdoc.id_inbound_document=inb.id_inbound_document 
					LEFT JOIN m_status_inbound inbstatus 
						ON inbstatus.id_status_inbound=inb.id_status_inbound
					WHERE 
						kd_inbound LIKE \'%'.$post['keyword'].'%\'
				) 
				UNION
				(
					SELECT 
						id_receiving AS id, kd_receiving AS code, 
						DATE_FORMAT(tanggal_receiving, \'%d\/%m\/%Y\') AS date, 
						\'\' AS description,
						\'RECEIVING\' as type 
					FROM 
						receiving
					WHERE 
						kd_receiving LIKE \'%'.$post['keyword'].'%\'
				) 
				UNION
				(
					SELECT 
						id_outbound AS id, kd_outbound AS code, 
						DATE_FORMAT(tanggal_outbound, \'%d\/%m\/%Y\') AS date, 
						outbound_document_name AS description,
						\'OUTBOUND\' as type 
					FROM 
						outbound outb 
					LEFT JOIN m_outbound_document outbdoc 
						ON outbdoc.id_outbound_document=outb.id_outbound_document
					WHERE 
						kd_outbound LIKE \'%'.$post['keyword'].'%\'
				)
				UNION
				(
					SELECT 
						brg.id_barang AS id, kd_unik AS code, 
						DATE_FORMAT(tgl_exp, \'%d\/%m\/%Y\') AS date,
						CONCAT_WS(\',\', rcv.id_receiving, kd_receiving, kd_barang, nama_barang, DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\'), loc_name) AS description,						
						\'ITEM\' as type 
					FROM 
						receiving rcv
					JOIN receiving_barang rcvbrg
						ON rcvbrg.id_receiving=rcv.id_receiving 
					JOIN barang brg 
						ON brg.id_barang=rcvbrg.id_barang 
					LEFT JOIN m_loc loc 
						ON loc.loc_id=rcvbrg.loc_id
					WHERE 
						kd_unik LIKE \'%'.$post['keyword'].'%\' OR kd_barang LIKE \'%'.$post['keyword'].'%\' OR nama_barang LIKE \'%'.$post['keyword'].'%\'
				)
				UNION
				(
					SELECT 
						id_supplier AS id, kd_supplier AS code, 
						\'\' AS date,
						CONCAT_WS(\',\', nama_supplier, alamat_supplier, telepon_supplier, cp_supplier) AS description,						
						\'SUPPLIER\' as type 
					FROM 
						supplier
					WHERE 
						kd_supplier LIKE \'%'.$post['keyword'].'%\' OR nama_supplier LIKE \'%'.$post['keyword'].'%\' OR alamat_supplier LIKE \'%'.$post['keyword'].'%\'
				)';
				
		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		
		for($i = 0; $i < $len; $i++){
			if($row[$i]['type'] == 'INBOUND'){
				
				$desc = $row[$i]['description'];
				$desc = explode(',', $desc);
				
				$str = 'Inbound Date : ' . $row[$i]['date'] . '<br>';
				$str .= 'Doc. Type : ' . $desc[0] . '<br>';
				$str .= 'Status : ' . $desc[1] . '<br>';
				
				$row[$i]['description'] = $str;
				
				$result['INBOUND'][] = $row[$i];
			}
			
			if($row[$i]['type'] == 'RECEIVING'){
				
				$desc = $row[$i]['description'];
				
				$str = 'Receiving Date : ' . $row[$i]['date'] . '<br>';
				$row[$i]['description'] = $str;
				
				$result['RECEIVING'][] = $row[$i];
			}
			
			if($row[$i]['type'] == 'OUTBOUND'){
				
				$desc = $row[$i]['description'];
				
				$str = 'Outbound Date : ' . $row[$i]['date'] . '<br>';
				$str .= 'Doc. Type : ' . $desc . '<br>';
				
				$row[$i]['description'] = $str;
				
				$result['OUTBOUND'][] = $row[$i];
			}
			
			if($row[$i]['type'] == 'ITEM'){
				
				$desc = $row[$i]['description'];
				$desc = explode(',', $desc);
				
				$str = 'Expired Date : ' . $row[$i]['date'] . '<br>';
				$str .= 'Date In : ' . $desc[4] . '<br>';
				$str .= 'Rcv. Number : ' . $desc[1] . '<br>';
				$str .= 'Item Code : ' . $desc[2] . '<br>';
				$str .= 'Item Name : ' . $desc[3] . '<br>';
				$str .= 'Location : ' . $desc[5] . '<br>';
				
				$row[$i]['description'] = $str;
				
				$result['ITEM'][] = $row[$i];
			}
			
			if($row[$i]['type'] == 'SUPPLIER'){
				
				$desc = $row[$i]['description'];
				$desc = explode(',', $desc);
				
				$str = 'Supplier Name : ' . $desc[0] . '<br>';
				$str .= 'Address : ' . $desc[1] . '<br>';
				$str .= 'Phone : ' . $desc[2] . '<br>';
				$str .= 'Contac Person : ' . $desc[3] . '<br>';
				
				$row[$i]['description'] = $str;
				
				$result['SUPPLIER'][] = $row[$i];
			}
		}
		
		return $result;
	}

    private function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b','a.id_supplier = b.id_supplier','left');
        $this->db->join($this->kurs . ' c','a.id_kurs = c.id_kurs','left');
        $this->db->join($this->status_po . ' st','st.status_po_id = a.status_po_id','left');
        $this->db->where_condition($condition);
        $this->db->order_by('a.id_po DESC,a.kd_po DESC');
        return $this->db;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_receiving= $this->input->post("kd_receiving");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_po = $this->input->post('id_po');
        if(!empty($kd_receiving)){
            $condition["a.kd_receiving like '%$kd_receiving%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id_po_barang as id_po_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_po = b.id_po');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_po($condition = array()) {
        $this->db->select('*,id_po_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');       
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail_supplier($condition = array()) {
        $this->db->select('*');
        $this->db->from($this->table  . ' a');
        //$this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');       
        $this->db->join($this->table2 . ' f',' a.id_supplier = f.id_supplier','left');
        $this->db->join($this->table8 . ' g',' g.id_supplier = f.id_supplier','left');
        $this->db->join($this->table5 . ' c',' c.id_barang = g.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function total_harga_po($id_po) {
        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }else{
            $condition["a.id_po"]=0;
        }
        $this->db->select('sum(harga_barang*jumlah_barang)as harga_total, id_po_barang,a.id_po as id_po_1');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');       
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->where_condition($condition);
        $this->db->group_by('id_po_1');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row) {
                return $row->harga_total;
            }
        }
        else{
            return 0;
        }
    }

    public function get_by_id($id) {
        $condition['a.id_po'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id_po_barang'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function get_kurs_simbol($id_po){
        $condition['a.id_po'] = $id_po;
        $hasil = '';
        $this->data($condition);
        $data = $this->db->get();

        foreach ($data->result() as $value) {
            $hasil = $value->lambang_kurs;
        }
        return $hasil;
    }

    public function data_table() {
        // Filtering
        $condition = array();
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_po >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_po <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        $kd_po= $this->input->post("search_key");
        $nama_supplier= $this->input->post("kd_supplier");
        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        if(!empty($kd_po)){
            $condition["a.kd_po like '%$kd_po%'"]=null;
        }
        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $condition["a.tanggal_po >= '$tanggal_awal'"]=null;
            $condition["a.tanggal_po <= '$tanggal_akhir'"]=null;
        }
       
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_po;

            if($value->nama_supplier != '' || $value->kd_supplier!='')
            {
                $nama_supplier = $value->kd_supplier.' / '.$value->nama_supplier;
            }else{
                $nama_supplier = '';
            }

            $rows[] = array(
                'primary_key' =>$value->id_po,
                'tanggal_po' => hgenerator::switch_tanggal($value->tanggal_po),
                'kd_po' => anchor(null, $value->kd_po, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_purchase_order', 'data-source' => base_url('referensi_purchase_order/get_detail_purchase_order/' . $id))),
                'remark' => 'Purchase Order, Supplier: '.$nama_supplier .'('.$value->nama_status.')', 
            );
        }

        return array('rows' => $rows, 'total' => $total);
    } 

    public function checkExist($id_po,$id_barang){
        $this->db->from($this->table4);
        $where = array('id_po'=>$id_po,
            'id_barang' => $id_barang);
        $this->db->where($where);
        return $this->db->get()->num_rows();
    }

    public function data_table_detail() {
        $id_po = $this->input->post('id_po');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_po_barang;
            $action = '';
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';
                                
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_purchase_order/edit_detail/' .$id_po.'/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_purchase_order/delete_detail/' .$id_po.'/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }   

  

    public function edit_data_table_detail() {

        // Filtering
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_po = $this->input->post('id_po');

        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }


        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));

        
        // Total Record
        $total = $this->data_detail_po($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_po = $this->data_detail_po($condition)->get();
        $rows = array();

        $i_po = 1;
        foreach ($data_detail_po->result() as $value) {
            $hasil_po['id_barang'][$i_po] = $value->id_barang;
            $hasil_po['kd_barang'][$i_po] = $value->kd_barang;
            $hasil_po['nama_barang'][$i_po] = $value->nama_barang;
            $hasil_po['tipe_barang'][$i_po] = $value->tipe_barang;
            $hasil_po['detail_barang'][$i_po] = $value->detail_barang;
            $hasil_po['jumlah_barang'][$i_po] = $value->jumlah_barang;
            $hasil_po['harga_barang'][$i_po] = $value->harga_barang;
            $hasil_po['id_po_barang'][$i_po] = $value->id_po_barang;
            $i_po++;
        }

        // Total Record
        $total_su = $this->data_detail_supplier($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_supplier = $this->data_detail_supplier($condition)->get();
        $rows = array();

        $i_su = 1;
        foreach ($data_detail_supplier->result() as $value) {
            $hasil_su['id_barang'][$i_su] = $value->id_barang;
            $hasil_su['kd_barang'][$i_su] = $value->kd_barang;
            $hasil_su['nama_barang'][$i_su] = $value->nama_barang;
            $hasil_su['tipe_barang'][$i_su] = $value->tipe_barang;
            $hasil_su['detail_barang'][$i_su] = $value->detail_barang;
            $hasil_su['status_po'][$i_su] = FALSE;
            $i_su++;
        }

        for($i=1;$i<$i_po;$i++){
            for($j=1;$j<$i_su;$j++){
                if($hasil_po['kd_barang'][$i] == $hasil_su['kd_barang'][$j]){
                    $hasil_su['status_po'][$j] = TRUE;
                }
            }

            $id = $hasil_po['id_po_barang'][$i];
            $action = '';
            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_supplier/edit_detail/' .$id.'/'. $id))) . ' ';
                }

            if ($this->access_right->otoritas('delete')) {
                $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_supplier/delete_detail/' .$id.'/'. $id)));
            }
            # code...
            $rows[] = array(
                    'kd_barang'=>$hasil_po['kd_barang'][$i],
                    'nama_barang' =>$hasil_po['nama_barang'][$i],
                    'tipe_barang' => $hasil_po['tipe_barang'][$i],
                    'detail_barang' => $hasil_po['detail_barang'][$i],
                    'jumlah_barang' => form_input("jumlah_barang_".$id_po."_".$hasil_po['id_barang'][$i],$hasil_po['jumlah_barang'][$i],'id = "jumlah_barang_'.$id_po."_".$i.'"class="span12 jumlah_barang" data-a-dec="," data-a-sep="."'),
                    'harga_barang' => form_input("harga_barang_".$id_po."_".$hasil_po['id_barang'][$i],$hasil_po['harga_barang'][$i],'id="harga_barang_'.$id_po."_".$i.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'),
                    'harga_total' => form_input("harga_total_".$id_po."_".$hasil_po['id_barang'][$i],$hasil_po['harga_barang'][$i] * $hasil_po['jumlah_barang'][$i],'id = "harga_total_'.$id_po."_".$i.'" class="span12 " data-a-dec="," data-a-sep="."'). form_hidden('id_barang[]',$hasil_po['id_barang'][$i]).'
                    <script type="text/javascript">
                    $(function() {
                        $(".numeric").numeric();
                        $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_barang_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_total_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_barang_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_satuan = parseInt($("#harga_barang_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_total = harga_satuan * jumlah;
                            $("#harga_total_'.$id_po."_".$i.'").autoNumeric("set",harga_total);                  
                        });
                        $("#jumlah_barang_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_satuan = parseInt($("#harga_barang_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_total = harga_satuan * jumlah;
                            $("#harga_total_'.$id_po."_".$i.'").autoNumeric("set",harga_total);                  
                        });
                        $("#harga_total_'.$id_po."_".$i.'").change(function() {
                            var jumlah =  $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_total = parseInt($("#harga_total_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_satuan = harga_total / jumlah;
                            $("#harga_barang_'.$id_po."_".$i.'").autoNumeric("set",harga_satuan);
                        });
                    });
                    </script>'
                );
        }
        for($i=1;$i<$i_su;$i++){
            if($hasil_su['status_po'][$i] != TRUE){
                $rows[] = array(
                    'kd_barang'=>$hasil_su['kd_barang'][$i],
                    'nama_barang' =>$hasil_su['nama_barang'][$i],
                    'tipe_barang' => $hasil_su['tipe_barang'][$i],
                    'detail_barang' => $hasil_su['detail_barang'][$i],
                    'jumlah_barang' => form_input("jumlah_barang_".$id_po."_".$hasil_su['id_barang'][$i],'','id = "jumlah_barang_'.$id_po."_".$i.'"class="span12 jumlah_barang" data-a-dec="," data-a-sep="."'),
                    'harga_barang' => form_input("harga_barang_".$id_po."_".$hasil_su['id_barang'][$i],'','id="harga_barang_'.$id_po."_".$i.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'),
                    'harga_total' => form_input("harga_total_".$id_po."_".$hasil_su['id_barang'][$i],'','id = "harga_total_'.$id_po."_".$i.'" class="span12 " data-a-dec="," data-a-sep="."'),
                    'action' => form_hidden('id_barang[]',$hasil_su['id_barang'][$i]).'
                    <script type="text/javascript">
                    $(function() {
                        $(".numeric").numeric();
                        $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_barang_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_total_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_barang_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_satuan = parseInt($("#harga_barang_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_total = harga_satuan * jumlah;
                            $("#harga_total_'.$id_po."_".$i.'").autoNumeric("set",harga_total);                  
                        });
                        $("#jumlah_barang_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_satuan = parseInt($("#harga_barang_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_total = harga_satuan * jumlah;
                            $("#harga_total_'.$id_po."_".$i.'").autoNumeric("set",harga_total);                  
                        });
                        $("#harga_total_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_total = parseInt($("#harga_total_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_satuan = harga_total / jumlah;
                            $("#harga_barang_'.$id_po."_".$i.'").autoNumeric("set",harga_satuan);
                        });
                    });
                    </script>'
                );
            }
        }

        return array('rows' => $rows, 'total' => $total_su);
    }    

    public function data_table_detail_excel() {

        // Filtering
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_po = $this->input->post('id_po');

        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }


        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));

        
        // Total Record
        $total = $this->data_detail_po($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_po = $this->data_detail_po($condition)->get();
        $rows = array();

        $i_po = 1;
        foreach ($data_detail_po->result() as $value) {
            $hasil_po['kd_barang'][$i_po] = $value->kd_barang;
            $hasil_po['nama_barang'][$i_po] = $value->nama_barang;
            $hasil_po['tipe_barang'][$i_po] = $value->tipe_barang;
            $hasil_po['detail_barang'][$i_po] = $value->detail_barang;
            $hasil_po['jumlah_barang'][$i_po] = $value->jumlah_barang;
            $hasil_po['harga_barang'][$i_po] = $value->harga_barang;
            $hasil_po['id_po_barang'][$i_po] = $value->id_po_barang;
            $hasil_po['nama_satuan'][$i_po] = $value->nama_satuan;
            $i_po++;
        }

        // Total Record
        $total_su = $this->data_detail_supplier($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_supplier = $this->data_detail_supplier($condition)->get();
        $rows = array();

        $i_su = 1;
        foreach ($data_detail_supplier->result() as $value) {
            $hasil_su['id_barang'][$i_su] = $value->id_barang;
            $hasil_su['kd_barang'][$i_su] = $value->kd_barang;
            $hasil_su['nama_barang'][$i_su] = $value->nama_barang;
            $hasil_su['tipe_barang'][$i_su] = $value->tipe_barang;
            $hasil_su['detail_barang'][$i_su] = $value->detail_barang;
            $hasil_su['status_po'][$i_su] = FALSE;
            $i_su++;
        }
        //echo $i_su;
        

        for($i=1;$i<$i_po;$i++){
            for($j=1;$j<$i_su;$j++){
                if($hasil_po['kd_barang'][$i] == $hasil_su['kd_barang'][$j]){
                    $hasil_su['status_po'][$j] = TRUE;
                }
            }
            $id = $hasil_po['id_po_barang'][$i];
            if($id!=''){
                $action = '';
                if ($this->access_right->otoritas('edit')) {
                    $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_supplier/edit_detail/' .$id.'/'. $id))) . ' ';
                    }

                if ($this->access_right->otoritas('delete')) {
                    $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_supplier/delete_detail/' .$id.'/'. $id)));
                }
                # code...
                if($hasil_po['tipe_barang'][$i]!=''){
                    $tipe_barang_pr = ', '.$hasil_po['tipe_barang'][$i];
                }else{
                    $tipe_barang_pr = '';
                }
                if($hasil_po['detail_barang'][$i]!=''){
                    $detail_barang_pr = ', '.$hasil_po['detail_barang'][$i];
                }else{
                    $detail_barang_pr = '';
                }
                $nama_barang_pr = $hasil_po['nama_barang'][$i].$tipe_barang_pr.$detail_barang_pr;
                $rows[] = array(
                        'kd_barang'=>$hasil_po['kd_barang'][$i],
                        'nama_barang' =>$hasil_po['nama_barang'][$i],
                        'jumlah_barang' => $hasil_po['jumlah_barang'][$i],
                        'satuan' => $hasil_po['nama_satuan'][$i],
                        'harga_barang' => hgenerator::numberexcel($hasil_po['harga_barang'][$i]),
                        'harga_total' => hgenerator::rupiah($hasil_po['harga_barang'][$i] * $hasil_po['jumlah_barang'][$i])
                    );
            }
        }

        return array('rows' => $rows, 'total' => $total_su);
    }   

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_po' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_po' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id_po_barang' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id_po_barang' => $id));
    }
    
    public function options($default = '--Choose Purchase Order--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_po] = $row->kd_po.' / '.$row->nama_po ;
        }
        return $options;
    }

    public function options_open($default = '--Choose Purchase Order--', $key = '') {
        $data = $this->data(array('a.status_po_id'=>1))->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_po] = $row->kd_po.' / '.$row->nama_po ;
        }
        return $options;
    }

    
}

?>
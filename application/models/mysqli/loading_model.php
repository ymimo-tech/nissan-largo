<?php
class loading_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getDoNumber($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					do_number 
				FROM 
					shipping_delivery_order 
				WHERE 
					shipping_id=\''.$post['shipping_id'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		if($row){
			$result = $row;
		}else{
			$result['do_number'] = '';
		}
		
		return $result;
	}
	
	public function getStatus(){
		$result = array();
		
		$sql = 'SELECT 
					* 
				FROM 
					m_shipping';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getScannedList($post = array()){
		$result = array();
					
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'kd_unik',
			2 => 'tgl_exp',
			3 => 'tgl_in',
			4 => 'user_name'
		);
		
		$where = '';
				
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					shipping shipp 
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.shipping_id=shipp.shipping_id 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				LEFT JOIN hr_user hu 
					ON hu.user_id=rcvbrg.user_id_loading
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\' 
				AND 
					rcvbrg.id_barang=\''.$post['id_barang'].'\'
				AND 
					rcvbrg.st_loading=\'1\'';
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];		
		/*
		$sql = 'SELECT 
					kd_unik, DATE_FORMAT(tgl_exp, \'%d\/%m\/%Y\') AS tgl_exp,
					DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\') AS tgl_in			
				FROM 
					shipping shipp 
				JOIN shipping_picking shipppick 
					ON shipppick.shipping_id=shipp.shipping_id
				JOIN picking_list pick 
					ON pick.pl_id=shipppick.pl_id  
				JOIN picking_list_outbound plo 
					ON plo.pl_id=pick.pl_id 
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				JOIN picking_qty pqty 
					ON pqty.pl_id=pick.pl_id
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.pl_id=pqty.pl_id 
						AND rcvbrg.id_barang=pqty.id_barang
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\'';
		*/

		$sql = 'SELECT 
					kd_unik, IFNULL(DATE_FORMAT(tgl_exp, \'%d\/%m\/%Y\'), \'-\') AS tgl_exp,
					IFNULL(DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\'), \'\') AS tgl_in,
					IFNULL(user_name, \'-\') AS user_name
				FROM 
					shipping shipp 
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.shipping_id=shipp.shipping_id 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				LEFT JOIN hr_user hu 
					ON hu.user_id=rcvbrg.user_id_loading
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\' 
				AND 
					rcvbrg.id_barang=\''.$post['id_barang'].'\'
				AND 
					rcvbrg.st_loading=\'1\'';
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['user_name'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function closeLoading($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					st_shipping 
				FROM 
					shipping 
				WHERE
					shipping_id=\''.$post['shipping_id'].'\''; 
		
		$row = $this->db->query($sql)->row_array();
		if($row['st_shipping'] == 1){
			
			$result['status'] = 'ERR';
			$result['message'] = 'Loading already finish.';
			
		}else{
			$rectime = date('Y-m-d H:i:s');
		
			$sql = 'UPDATE 
						shipping 
					SET 
						st_shipping=\'1\'
						shipping_finish=\''.$rectime.'\'
					WHERE 
						shipping_id=\''.$post['shipping_id'].'\'';
						
			$q = $this->db->query($sql);
			
			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Finish loading success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Finish loading failed';
			}
		
		}
		
		return $result;
	}
	
	public function checkDoNumber($post = array()){
		$result = array();
		
		$sql = 'SELECT COUNT(*) AS total 
				FROM shipping_delivery_order 
				WHERE 
					do_number=\''.$post['do_number'].'\' 
				AND 
					shipping_id!=\''.$post['shipping_id'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		
		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Delivery Order number already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}
		
		return $result;
	}
	
	public function saveDo($post = array()){
		$result = array();
		
		$sql = 'SELECT COUNT(*) AS total 
				FROM shipping_delivery_order 
				WHERE shipping_id=\''.$post['shipping_id'].'\'';
				
		$row = $this->db->query($sql)->row_array();
		
		if($row['total'] > 0){
			$rectime = date('Y-m-d H:i:s');
			$sql = 'UPDATE 
						shipping_delivery_order 
					SET 
						do_number=\''.$post['do_number'].'\',
						update_by=\''.$this->session->userdata('user_id').'\',
						modified_date=\''.$rectime.'\' 
					WHERE 
						shipping_id=\''.$post['shipping_id'].'\'';
						
			$this->db->query($sql);
			
		}else{
			
			$rectime = date('Y-m-d H:i:s');
			$sql = 'INSERT INTO shipping_delivery_order(
						do_number,
						shipping_id,
						created_by,
						created_date,
						update_by,
						modified_date
					)VALUES(
						\''.$post['do_number'].'\',
						\''.$post['shipping_id'].'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\'
					)';
					
			$this->db->query($sql);
		}
		
		return $result;
	}
	
	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 =>  'pl_id_qty',
			1 => 'shipping_id',
			2 => 'id_barang',
			3 => '',
			4 => 'kd_barang',
			5 => 'nama_barang',
			6 => 'picked'
		);
		
		$where = '';
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM(
					SELECT 
						shipp.shipping_id
					FROM 
						shipping shipp 
					JOIN shipping_picking shipppick 
						ON shipppick.shipping_id=shipp.shipping_id
					JOIN picking_list pick 
						ON pick.pl_id=shipppick.pl_id  
					JOIN picking_list_outbound plo 
						ON plo.pl_id=pick.pl_id 
					JOIN outbound outb 
						ON outb.id_outbound=plo.id_outbound 
					JOIN picking_qty pqty 
						ON pqty.pl_id=pick.pl_id
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.pl_id=pqty.pl_id 
							AND rcvbrg.id_barang=pqty.id_barang
					LEFT JOIN barang brg 
						ON brg.id_barang=rcvbrg.id_barang 
					LEFT JOIN satuan sat 
						ON sat.id_satuan=brg.id_satuan 
					WHERE 
						shipp.shipping_id=\''.$post['shipping_id'].'\' 
					GROUP BY 
						rcvbrg.id_barang
				) AS tbl';
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					pqty.pl_id_qty, shipp.shipping_id, brg.id_barang, COUNT(rcvbrg.id_barang) AS picked, kd_barang, CONCAT(nama_barang, \' - \', kd_barang) AS nama_barang,
					IFNULL(nama_satuan, \'\') AS nama_satuan 
				FROM 
					shipping shipp 
				JOIN shipping_picking shipppick 
					ON shipppick.shipping_id=shipp.shipping_id
				JOIN picking_list pick 
					ON pick.pl_id=shipppick.pl_id  
				JOIN picking_list_outbound plo 
					ON plo.pl_id=pick.pl_id 
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				JOIN picking_qty pqty 
					ON pqty.pl_id=pick.pl_id
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.pl_id=pqty.pl_id 
						AND rcvbrg.id_barang=pqty.id_barang
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\' 
				GROUP BY 
					rcvbrg.id_barang';
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $row[$i]['pl_id_qty'];
			$nested[] = $row[$i]['shipping_id'];
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['picked'] . ' ' . $row[$i]['nama_satuan'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function getDetailPicking($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => 'pl_id',
			1 => '',
			2 => 'pl_name',
			3 => 'pl_date',
			4 => 'remark',
			5 => 'pl_start',
			6 => 'pl_finish',
			7 => 'nama_status'
		);
		
		$where = '';
		
		if(!empty($post['shipping_id']))
			$where .= sprintf(' AND shipp.shipping_id = \'%s\' ', $post['shipping_id']); 
		
		$sql = 'SELECT 
					COUNT(*) AS total
				FROM (
					SELECT 
						pick.pl_id
					FROM 
						picking_list pick 
					JOIN picking_list_outbound plo 
						ON plo.pl_id=pick.pl_id
					JOIN outbound outb 
						ON outb.id_outbound=plo.id_outbound 
					JOIN shipping_picking shippick 
						ON shippick.pl_id=pick.pl_id 
					JOIN shipping shipp 
						ON shipp.shipping_id=shippick.shipping_id
					LEFT OUTER JOIN supplier spl 
						ON spl.id_supplier=outb.id_customer 
							AND outb.id_outbound_document = \'2\'
					LEFT OUTER JOIN m_customer cust 
						ON cust.id_customer=outb.id_customer 
							AND outb.id_outbound_document != \'2\' 
					LEFT JOIN m_status_picking mstat
						ON mstat.id_status_picking=pick.id_status_picking
					WHERE 1=1'.$where.' 
					GROUP BY 
						pick.pl_id
				) AS tbl';
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					pick.pl_id, pl_name, kd_outbound, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS pl_date,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					IFNULL(remark, \'-\') AS remark, IFNULL(DATE_FORMAT(pl_start, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS pl_start,
					IFNULL(DATE_FORMAT(pl_finish, \'%H:%i<br><span class="small-date">%d\/%m\/%Y<span>\'), \'-\') AS pl_finish,
					IFNULL(DATE_FORMAT(shipping_start, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS shipping_start,
					IFNULL(DATE_FORMAT(shipping_finish, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS shipping_finish,
					pick.id_status_picking, 
					(
						SELECT 
							nama_status 
						FROM 
							m_status_picking 
						WHERE 
							id_status_picking = (
								CASE
									WHEN pl_start IS NULL 
										THEN \'1\' 
									WHEN pl_start IS NOT NULL AND pl_finish IS NULL 
										THEN \'2\' 
									WHEN pl_start IS NOT NULL AND pl_finish IS NOT NULL 
										THEN \'3\' 
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NULL 
										THEN \'4\'
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NOT NULL 
										THEN \'5\' 
								END
							)
					) AS nama_status				
				FROM 
					picking_list pick 
				JOIN picking_list_outbound plo 
					ON plo.pl_id=pick.pl_id
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				JOIN shipping_picking shippick 
					ON shippick.pl_id=pick.pl_id 
				JOIN shipping shipp 
					ON shipp.shipping_id=shippick.shipping_id
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\' 
				LEFT JOIN m_status_picking mstat
					ON mstat.id_status_picking=pick.id_status_picking
				WHERE 1=1'.$where.' 
				GROUP BY 
					pick.pl_id';
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'picking_list/detail/'.$row[$i]['pl_id'].'">'.$row[$i]['pl_name'].'</a>';
			$nested[] = $row[$i]['pl_date'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['pl_start'];
			$nested[] = $row[$i]['pl_finish'];
			$nested[] = $row[$i]['nama_status'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function getDetail($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					shipp.shipping_id, shipping_code, kd_outbound, id_outbound_document, 
					DATE_FORMAT(shipping_date, \'%d\/%m\/%Y\') AS shipping_date, kd_supplier, 
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					alamat_supplier, telepon_supplier, cp_supplier, address, phone, 
					IFNULL(DATE_FORMAT(shipping_start, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_start,
					IFNULL(DATE_FORMAT(shipping_finish, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_finish,
					user_name, st_shipping_name AS nama_status, driver_name, license_plate  
				FROM 
					shipping shipp
				JOIN shipping_picking shippick 
					ON shippick.shipping_id=shipp.shipping_id 
				JOIN picking_list pl 
					ON pl.pl_id=shippick.pl_id 
				JOIN picking_list_outbound plo 
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\' 
				LEFT JOIN hr_user hu 
					ON hu.user_id=shipp.user_id 
				LEFT JOIN m_shipping ms 
					ON ms.st_shipping=shipp.st_shipping
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\' 
				GROUP BY 
					shipp.shipping_id';
					
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function getOutboundCompletePicking(){
		$result = array();
		
		$sql = 'SELECT 
					outb.id_outbound, kd_outbound 
				FROM 
					outbound outb 
				JOIN picking_list_outbound plo 
					ON plo.id_outbound=outb.id_outbound 
				WHERE 
					outb.id_status_outbound != \'2\'
				ORDER BY 
					id_outbound DESC';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getPickingDocument($post = array()){
		$result = array();
		
		$where = '';
		if(isset($post['shipping_id']))
			$where .= ' AND pl.id_status_picking = \'3\' ';
		else{
			$where .= ' AND pl.pl_id NOT IN (
							SELECT 
								pl_id 
							FROM 
								shipping_picking
						) AND pl.id_status_picking = \'3\' ';
		}
		
		if(isset($post['id_outbound']))
			$where .= ' AND outb.id_outbound = \''.$post['id_outbound'].'\' ';
		
		$sql = 'SELECT 
					pl.pl_id, pl_name,
					DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS pl_date, 
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name 
				FROM 
					picking_list pl 
				JOIN picking_list_outbound plo 
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				LEFT JOIN outbound_barang outbrg 
					ON outbrg.id_outbound=outb.id_outbound
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\' 
				WHERE 1=1
					'.$where.'
				GROUP BY 
					pl.pl_id';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getItems($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					COUNT(rcvbrg.id_barang) AS qty, kd_barang, CONCAT(nama_barang, \' - \', kd_barang) AS nama_barang,
					IFNULL(nama_satuan, \'\') AS nama_satuan 
				FROM 
					shipping shipp 
				JOIN shipping_picking shipppick 
					ON shipppick.shipping_id=shipp.shipping_id
				JOIN picking_list pick 
					ON pick.pl_id=shipppick.pl_id  
				JOIN picking_list_outbound plo 
					ON plo.pl_id=pick.pl_id 
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				JOIN picking_qty pqty 
					ON pqty.pl_id=pick.pl_id 
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.pl_id=pqty.pl_id 
						AND rcvbrg.id_barang=pqty.id_barang
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\' 
				GROUP BY 
					rcvbrg.id_barang 
				ORDER BY 
					pqty.pl_id_qty';
					
		$row = $this->db->query($sql)->result_array();
		$result['data'] = $row;
		
		return $result;
	}
	
	public function delete($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					shipping 
				WHERE 
					shipping_id = \''.$post['shipping_id'].'\' 
				AND 
					shipping_start IS NOT NULL';
					
		$r = $this->db->query($sql)->row_array();
		if($r['total'] > 0){
			
			$result['status'] = 'ERR';
			$result['message'] = 'Delete data failed, because loading process already started';
			
		}else{
			$sql = 'DELETE FROM shipping WHERE shipping_id = \''.$post['shipping_id'].'\'';
			$this->db->query($sql);
			
			/*
			$sql = 'SELECT 
						pl_id 
					FROM 
						shipping_picking 
					WHERE 
						shipping_id=\''.$post['shipping_id'].'\'';
						
			$row = $this->db->query($sql)->row_array();
			*/
			
			$sql = 'DELETE FROM shipping_picking WHERE shipping_id = \''.$post['shipping_id'].'\'';
			$q = $this->db->query($sql);
			
			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Delete success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Delete failed';
			}
		}
		
		return $result;
	}
	
	public function getOutboundDocument($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					pl.pl_id, pl_name, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS  pl_date, 
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					IFNULL(alamat_supplier, address) AS address 
				FROM 
					shipping shipp 
				JOIN shipping_picking shipppick 
					ON shipppick.shipping_id=shipppick.shipping_id 
				JOIN picking_list pl
					ON pl.pl_id=shipppick.pl_id 
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id 
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound
				LEFT JOIN m_status_outbound mso 
					ON mso.id_status_outbound=outb.id_status_outbound 
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\' 
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\'';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getEdit($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					shipp.shipping_id, shipping_code, plo.id_outbound, kd_outbound, 
					DATE_FORMAT(shipping_date, \'%d\/%m\/%Y\') AS shipping_date, 
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					IFNULL(alamat_supplier, address) AS address, 
					driver_name, license_plate, shipppick.pl_id 
				FROM 
					shipping shipp 
				JOIN shipping_picking shipppick 
					ON shipppick.shipping_id=shipp.shipping_id 
				JOIN picking_list pl 
					ON pl.pl_id=shipppick.pl_id 
				JOIN picking_list_outbound plo 
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound 
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\' 
				WHERE 
					shipp.shipping_id=\''.$post['shipping_id'].'\'';
					
		$result = $this->db->query($sql)->row_array();
		
		return $result;
	}
	
	public function updateOutbound1($idOutbound, $data = array(), $id){
		
		$sql = 'DELETE FROM shipping_picking WHERE shipping_id=\''.$id.'\'';
		$this->db->query($sql);
		
		$len = count($data);
		for($i = 0; $i < $len; $i++){
			
			$sql = 'INSERT INTO shipping_picking(
						shipping_id,
						id_outbound,
						pl_id
					)VALUES(
						\''.$id.'\',
						\''.$idOutbound.'\',
						\''.$data[$i].'\'
					)';
						
			$this->db->query($sql);
			
			//update_shipping_id_in_receiving_barang ?
		}
	}
	
	public function updateShippingPicking($idOutbound, $idPicking, $id){
		
		$sql = 'DELETE FROM shipping_picking WHERE shipping_id=\''.$id.'\'';
		$this->db->query($sql);
		
		//$len = count($data);
		//for($i = 0; $i < $len; $i++){
			
			$sql = 'INSERT INTO shipping_picking(
						shipping_id,
						id_outbound,
						pl_id
					)VALUES(
						\''.$id.'\',
						\''.$idOutbound.'\',
						\''.$idPicking.'\'
					)';
						
			$this->db->query($sql);
			
			//update_shipping_id_in_receiving_barang ?
		//}
	}
	
	public function updateShippingIdInReceivingBarang($shippingId){
		$result = array();
		
		$sql = 'SELECT 
					id_barang, pl.pl_id 
				FROM 
					shipping ship 
				JOIN shipping_picking sp 
					ON sp.shipping_id=ship.shipping_id 
				JOIN picking_list pl 
					ON pl.pl_id=sp.pl_id 
				JOIN picking_qty pq 
					ON pq.pl_id=pl.pl_id 
				WHERE 
					ship.shipping_id=\''.$shippingId.'\'';
					
		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		
		for($i = 0; $i < $len; $i++){
			$idBarang = $row[$i]['id_barang'];
			$plId = $row[$i]['pl_id'];
			
			$sql = 'UPDATE 
						receiving_barang 
					SET 
						shipping_id=\''.$shippingId.'\' 
					WHERE 
						id_barang=\''.$idBarang.'\' 
					AND 
						pl_id=\''.$plId.'\'';
						
			$this->db->query($sql);
		}
		
		return $result;
	}
	
	public function updatePicking($id){
		$sql = 'UPDATE picking_list SET id_status_picking = \'4\' WHERE pl_id = \''.$id.'\'';
		$this->db->query($sql);
	}	
	
	public function setShipmentDoc($pickingId, $shippingId){
		
		$sql = 'SELECT 
					shipment_doc  
				FROM 
					picking_list_outbound plo 
				JOIN shipping_doc sd 
					ON sd.outbound_id=plo.id_outbound
				WHERE 
					pl_id=\''.$pickingId.'\'';
					
		$r = $this->db->query($sql)->row_array();
		if($r){
			
			$rectime = date('Y-m-d H:i:s');
			
			$sql = 'INSERT INTO shipping_delivery_order(
						shipping_id, 
						do_number,
						created_by,
						created_date,
						update_by,
						modified_date
					)VALUES(
						\''.$shippingId.'\',
						\''.$r['shipment_doc'].'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\',
						\''.$this->session->userdata('user_id').'\',
						\''.$rectime.'\'
					)';
			
			$this->db->query($sql);
			
		}
	}
	
	public function create($data, $idOutbound, $picking) {	
        $this->db->insert('shipping', $data);
		$shippingId = $this->db->insert_id();
		
		//$this->updatePicking($picking);
		$this->updateShippingPicking($idOutbound, $picking, $shippingId);
		$this->updateShippingIdInReceivingBarang($shippingId);
		
		//INTEGRATION
		$this->setShipmentDoc($picking, $shippingId);
		
		return true;
    }

    public function update($data, $idOutbound, $picking, $id) {
        $this->db->update('shipping', $data, array('shipping_id' => $id));
		
		$this->updateShippingPicking($idOutbound, $picking, $id);
		
		//INTEGRATION
		$this->setShipmentDoc($picking, $id);
    }
	
	public function add_shipping_code(){
        $this->db->set('inc_shipping',"inc_shipping+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }
	
	public function getOutboundNotInShipping(){
		$result = array();
		
		$sql = 'SELECT 
					outb.id_outbound, kd_outbound, DATE_FORMAT(tanggal_outbound, \'%d\/%m\/%Y\') AS  tanggal_outbound, 
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name					
				FROM 
					outbound outb 
				JOIN picking_list pick 
					ON pick.id_outbound=pick.id_outbound 
				LEFT JOIN outbound_barang outbrg 
					ON outbrg.id_outbound=outb.id_outbound 
				LEFT JOIN receiving_barang rcvbrg 
					ON rcvbrg.id_barang=outbrg.id_barang
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document != \'2\' 
				WHERE 
					rcvbrg.shipping_id IS NULL
				GROUP BY 
					outb.id_outbound';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getLoadingCodeIncrement(){
		
		$result = '';
		
		$sql = 'SELECT 
					CONCAT(pre_code_shipping, \'\', inc_shipping) AS loading_code 
				FROM 	
					m_increment 
				WHERE 
					id_inc = \'1\'';
					
		$row = $this->db->query($sql)->row_array();
		$result = $row['loading_code'];
		
		return $result;

	}
	
	public function getLoadingCode($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					shipping_id, shipping_code
				FROM 
					shipping 
				WHERE 
					shipping_code LIKE \'%'.$post['query'].'%\'';
					
		$row = $this->db->query($sql)->result_array();
		
		$result['status'] = 'OK';
		$result['result'] = $row;
		
		return $result;
	}
	
}
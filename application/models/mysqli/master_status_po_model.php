<?php
class master_status_po_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $class_name = 'master_status_po';
    private $table  = 'm_status_po';

    private function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.status_po_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_status= $this->input->post("kd_status");
        $nama_status= $this->input->post("nama_status");

        if(!empty($kd_status)){
            $condition["a.kd_status like '%$kd_status%'"]=null;
        }


        if(!empty($nama_status)){
            $condition["a.nama_status like '%$nama_status%'"]=null;
        }

        
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.status_po_id');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->status_po_id;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name.'/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name.'/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_status' => $value->kd_status,
                    'nama_status' => $value->nama_status,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'kd_status' => $value->kd_status,
                    'nama_status' => $value->nama_status,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('status_po_id' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('status_po_id' => $id));
    }
    
    public function options($default = '--Pilih Status PO--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
         //   $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->status_po_id] = $row->nama_status ;
        }
        return $options;
    }

    
}

?>
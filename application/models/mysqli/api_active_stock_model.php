<?php

class Api_active_stock_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($serial_number){
    	// $this->db->select("nama_barang, kd_barang, loc_name, (SELECT GROUP_CONCAT(DISTINCT loc_name ORDER BY loc_name ASC) FROM receiving_barang rcv_ LEFT JOIN barang brg_ ON rcv_.id_barang = brg_.id_barang LEFT JOIN m_loc loc_ ON rcv_.loc_id = loc_.loc_id WHERE rcv_.id_barang = rcv.id_barang) AS location, kd_batch, tgl_exp, has_qty, def_qty, last_qty");
        $this->db->select("nama_barang, kd_barang, loc_name AS location, kd_batch, tgl_exp, has_qty, def_qty, last_qty, kd_qc");
    	$this->db->from("receiving_barang rcv");
    	$this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
    	$this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->join("m_qc mqc", "rcv.id_qc = mqc.id_qc", "left");
    	$this->db->where("kd_unik", $serial_number);
    	$this->db->where("rcv.id_barang IS NOT NULL");
    	return $this->db->get();
    }

    function get_pl($serial_number){
        $this->db->select("nama_barang, kd_barang, loc_name AS location, kd_batch, tgl_exp, has_qty, def_qty, last_qty, kd_qc");
        $this->db->from("receiving_barang rcv");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->join("m_qc mqc", "rcv.id_qc = mqc.id_qc", "left");
        $this->db->where("kd_parent", $serial_number);
        $this->db->where("rcv.id_barang IS NOT NULL");
        return $this->db->get();
    }

    function get_item($item_code){
        $this->db->select("*");
        $this->db->from("barang");
        $this->db->where("kd_barang", $item_code);
        return $this->db->get();
    }

    function retrieve_item($item_code){
        $this->db->select("loc_name, SUM(last_qty) AS qty");
        $this->db->from("receiving_barang rcv");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->where("kd_barang", $item_code);
		$this->db->where_not_in("loc.loc_type", "SENTOUT");
        $this->db->group_by("loc_name");
        return $this->db->get();
    }

    function receiving($serial_number){
        $this->db->select("kd_receiving, tgl_in AS tanggal_receiving, user_name");
        $this->db->from("receiving_barang rcvb");
        $this->db->join("receiving rcv", "rcvb.id_receiving = rcv.id_receiving", "left");
        $this->db->join("hr_user usr", "rcvb.user_id_receiving = usr.user_id", "left");
        $this->db->where("kd_unik", $serial_number);
        return $this->db->get();
    }

    function putaway($serial_number){
        $this->db->select("putaway_time, loc_name, user_name");
        $this->db->from("receiving_barang rcv");
        $this->db->join("int_transfer_detail trf", "rcv.kd_unik = trf.kd_unik", "left");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->join("hr_user usr", "rcv.user_id_putaway = usr.user_id", "left");
        $this->db->where("rcv.kd_unik", $serial_number);
        $this->db->limit(1);
        return $this->db->get();
    }

    function transfer($serial_number){
        $this->db->select("(SELECT loc_name FROM m_loc WHERE loc_id = trf.loc_id_old) AS loc_name_old, (SELECT user_name FROM hr_user WHERE user_id = trf.user_id_pick) AS user_name_old, pick_time, (SELECT loc_name FROM m_loc WHERE loc_id = trf.loc_id_new) loc_name_new, (SELECT user_name FROM hr_user WHERE user_id = trf.user_id_put) AS user_name_new, put_time");
        $this->db->from("int_transfer_detail trf");
        $this->db->where("kd_unik", $serial_number);
        return $this->db->get();
    }

    function picking($serial_number){
        $this->db->select("pl_name, picking_time, user_name");
        $this->db->from("receiving_barang rcv");
        $this->db->join("picking_list pls", "rcv.pl_id = pls.pl_id", "left");
        $this->db->join("hr_user usr", "rcv.user_id_picking = usr.user_id", "left");
        $this->db->where("kd_unik", $serial_number);
        return $this->db->get();
    }
	
	function history($serial_number){
		$this->db->select("(SELECT loc_name FROM m_loc WHERE loc_id = trf.loc_id_old) AS loc_name_old, (SELECT user_name FROM hr_user WHERE user_id = trf.user_id_pick) AS user_name_old, pick_time, (SELECT loc_name FROM m_loc WHERE loc_id = trf.loc_id_new) loc_name_new, (SELECT user_name FROM hr_user WHERE user_id = trf.user_id_put) AS user_name_new, put_time, process_name");
		$this->db->from("int_transfer_detail trf");
		$this->db->where("kd_unik", $serial_number);
        return $this->db->get();
	}

}
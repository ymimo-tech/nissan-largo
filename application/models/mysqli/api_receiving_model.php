<?php

class Api_receiving_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function load(){
        $this->db->select("kd_receiving, kd_barang, has_batch, has_expdate, has_qty, def_qty, qty");
        $this->db->from("receiving_qty rcq");
        $this->db->join("receiving rcv", "rcq.id_receiving = rcv.id_receiving", "left");
        $this->db->join("barang brg", "rcq.id_barang = brg.id_barang", "left");
        $this->db->where("rcv.st_receiving", 0);
        $this->db->where("rcq.qty > 0",NULL);
        return $this->db->get();
    }

    function __load(){
        $this->db->select("kd_receiving, st_receiving as kd_barang, st_receiving as has_batch,  st_receiving as has_expdate, st_receiving as has_qty,st_receiving as def_qty,sum(rq.qty) as qty");
        $this->db->from('receiving r');
        $this->db->join('receiving_qty rq','rq.id_receiving = r.id_receiving','left');
        //$this->db->from("receiving_qty rcq");
        //$this->db->join("receiving rcv", "rcq.id_receiving = rcv.id_receiving", "left");
        //$this->db->join("barang brg", "rcq.id_barang = brg.id_barang", "left");
        $this->db->where("st_receiving", 0);
        $this->db->group_by('r.id_receiving');
        return $this->db->get();
    }

    function get($receiving_code){
    	$this->db->select("*");
    	$this->db->from("receiving");
    	$this->db->where("kd_receiving", $receiving_code);
    	return $this->db->get();
    }

    function start($receiving_code){
        $this->db->set("start_tally", date("Y-m-d H:i:s"));
        $this->db->where("kd_receiving", $receiving_code);
        $this->db->update("receiving");
    }

    function start_receiving($receiving_code){
        $this->db->set("start_tally", "IFNULL(start_tally, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("kd_receiving", $receiving_code);
        $this->db->update("receiving");
    }

    function retrieve($receiving_code){
    	$this->db->select("b.kd_barang, b.st_batch, b.has_expdate, a.qty");
    	$this->db->from("receiving_qty a");
    	$this->db->join("barang b", "b.id_barang = a.id_barang", "left");
    	$this->db->join("receiving c", "c.id_receiving = a.id_receiving", "left");
    	$this->db->where("c.kd_receiving", $receiving_code);
    	return $this->db->get();
    }

    function post($data){
        $id_receiving   = $this->get_id("id_receiving", "kd_receiving", $data["kd_receiving"], "receiving")->row_array();
    	$id_barang 		= $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
        $id_qc          = $this->get_id("id_qc", "kd_qc", $data["kd_qc"], "m_qc")->row_array();
    	$id_user 	 	= $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
        $this->db->set("kd_parent", $data['kd_parent']);
        $this->db->set("id_receiving", $id_receiving["id_receiving"]);
        $this->db->set("id_barang", $id_barang["id_barang"]);
        $this->db->set("kd_batch", $data["kd_batch"]);
    	$this->db->set("loc_id", 100);
    	$this->db->set("tgl_exp", $data["tgl_exp"]);
    	$this->db->set("tgl_in", $data["tgl_in"]);
    	$this->db->set("st_receive", 1);
    	$this->db->set("user_id_receiving", $id_user["user_id"]);
        $this->db->set("first_qty", $data["qty"]);
        $this->db->set("last_qty", $data["qty"]);
        $this->db->set("id_qc", $id_qc["id_qc"]);
    	$this->db->where("kd_unik", $data['kd_unik']);
    	$this->db->update("receiving_barang");
		
		$data_insert	= array(
									"loc_id_new"	=> 100,
									"loc_id_old"	=> 100,
									"pick_time"		=> $data["tgl_in"],
									"put_time"		=> $data["tgl_in"],
									"user_id_pick"	=> $id_user["user_id"],
									"user_id_put"	=> $id_user["user_id"],
									"process_name"	=> "RECEIVING"
									);
        $this->db->insert("int_transfer_detail", $data_insert);
    }

    function update($data){
        $id_receiving   = $this->get_id("id_receiving", "kd_receiving", $data["kd_receiving"], "receiving")->row_array();
        $id_barang      = $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
        $id_qc          = $this->get_id("id_qc", "kd_qc", $data["kd_qc"], "m_qc")->row_array();
        $id_user        = $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
        $this->db->set("kd_parent", $data['kd_parent']);
        $this->db->set("id_receiving", $id_receiving["id_receiving"]);
        $this->db->set("id_barang", $id_barang["id_barang"]);
        $this->db->set("kd_batch", $data["kd_batch"]);
        $this->db->set("loc_id", 100);
        $this->db->set("tgl_exp", $data["tgl_exp"]);
        $this->db->set("tgl_in", $data["tgl_in"]);
        $this->db->set("st_receive", 1);
        $this->db->set("user_id_receiving", $id_user["user_id"]);
        $this->db->set("first_qty", $data["qty"]);
        $this->db->set("last_qty", $data["qty"]);
        $this->db->set("id_qc", $id_qc["id_qc"]);
        $this->db->where("kd_unik", $data['kd_unik']);
        $this->db->update("receiving_barang");
        
        $data_insert    = array(
                                    "loc_id_new"    => 100,
                                    "loc_id_old"    => 100,
                                    "pick_time"     => $data["tgl_in"],
                                    "put_time"      => $data["tgl_in"],
                                    "user_id_pick"  => $id_user["user_id"],
                                    "user_id_put"   => $id_user["user_id"],
                                    "process_name"  => "RECEIVING"
                                    );
        $this->db->insert("int_transfer_detail", $data_insert);
    }

    function insert($data){
        $id_receiving   = $this->get_id("id_receiving", "kd_receiving", $data["kd_receiving"], "receiving")->row_array();
        $id_barang      = $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
        $id_qc          = $this->get_id("id_qc", "kd_qc", $data["kd_qc"], "m_qc")->row_array();
        $id_user        = $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
        $data_insert    = array(
                                    "kd_parent" => $data["kd_parent"],
                                    "id_barang" => $id_barang["id_barang"], 
                                    "id_receiving" => $id_receiving["id_receiving"], 
                                    "kd_batch" => $data["kd_batch"], 
                                    "loc_id" => 100, 
                                    "tgl_exp" => $data["tgl_exp"], 
                                    "tgl_in" => $data["tgl_in"], 
                                    "st_receive" => 1,
                                    "user_id_receiving" => $id_user["user_id"],
                                    "first_qty" => $data["qty"],
                                    "last_qty" => $data["qty"],
                                    "id_qc" => $id_qc["id_qc"],
                                    "kd_unik" => $data['kd_unik'] 
                                    );
        $this->db->insert("receiving_barang", $data_insert);
		
		$data_masukan	= array(
									"kd_unik"		=> $data["kd_unik"],
									"loc_id_new"	=> 100,
									"loc_id_old"	=> 100,
									"pick_time"		=> $data["tgl_in"],
									"put_time"		=> $data["tgl_in"],
									"user_id_pick"	=> $id_user["user_id"],
									"user_id_put"	=> $id_user["user_id"],
									"process_name"	=> "RECEIVING"
									);
        $this->db->insert("int_transfer_detail", $data_masukan);
    }

    function get_item($receiving_code, $item_code){
        $id_receiving   = $this->get_id("id_receiving", "kd_receiving", $receiving_code, "receiving")->row_array();
        $id_barang      = $this->get_id("id_barang", "kd_barang", $item_code, "barang")->row_array();
        $this->db->select("st_receive");
        $this->db->from("receiving_qty");
        $this->db->where("kd_receiving", $id_receiving["id_receiving"]);
        $this->db->where("kd_barang", $id_barang["id_barang"]);
        return $this->db->get();
    }

    function lock_item($receiving_code, $item_code){
        $id_receiving   = $this->get_id("id_receiving", "kd_receiving", $receiving_code, "receiving")->row_array();
        $id_barang      = $this->get_id("id_barang", "kd_barang", $item_code, "barang")->row_array();
        $this->db->set("st_receive", 1);
        $this->db->where("id_receiving", $id_receiving["id_receiving"]);
        $this->db->where("id_barang", $id_barang["id_barang"]);
        $this->db->update("receiving_qty");
    }

    function get_serial($serial_number){
        $this->db->select("id_barang");
        $this->db->from("receiving_barang");
        $this->db->where("kd_unik", $serial_number);
        return $this->db->get();
    }

    function get_serial_lp($serial_number){
        $this->db->select("id_barang");
        $this->db->from("receiving_barang");
        //$this->db->where("kd_unik", $serial_number);
        $this->db->where("kd_unik = '".$serial_number."' or kd_parent = '".$serial_number."'", NULL);
        return $this->db->get();
    }

    function get_qc(){
        $this->db->select("kd_qc");
        $this->db->from("m_qc");
        return $this->db->get();
    }

    function validQty($data=array()){
        $this->db
            ->select('rq.qty as doc_qty, IF(has_qty=1, sum(last_qty), count(kd_unik)) as act_qty')
            ->from('receiving r')
            ->join('receiving_qty rq','rq.id_receiving=r.id_receiving')
            ->join('receiving_barang rb','rb.id_receiving=r.id_receiving','left')
            ->join('barang b','b.id_barang=rq.id_barang','left')
            ->where('r.kd_receiving',$data['kd_receiving'])
            ->where('b.kd_barang',$data['kd_barang'])
            ->group_by('b.id_barang');

        $row = $this->db->get()->row_array();

        if($row['doc_qty'] >= ($row['act_qty'] + $data['qty'])){
            return true;
        }

        return false;

    }

    public function get_document(){

        $this->db->select(array("kd_receiving","CONCAT(count(*), ' Items') as qty","IFNULL(spl.nama_supplier,cust.customer_name) as source"));
        $this->db->from('receiving r');
        $this->db->join('receiving_qty rq','rq.id_receiving=r.id_receiving');
        $this->db->join('inbound inb','inb.id_inbound=r.id_po','left');
        $this->db->join('supplier spl','spl.id_supplier=inb.id_supplier and inb.id_inbound_document NOT IN (2,7)','left outer');
        $this->db->join('m_customer cust','cust.id_customer=inb.id_supplier and inb.id_inbound_document IN (2,7)','left outer');
        $this->db->where("r.st_receiving", 0);
        $this->db->group_by('r.id_receiving');

        $rec  = $this->db->get();
        return $rec->result_array();

    }

    public function get_document_item($receivingCode){

        $this->db->select(array('kd_receiving as receiving_code','kd_barang as item_code','nama_barang as item_name','sum(qty) as qty','sat.nama_satuan','IFNULL(supl.nama_supplier,cust.customer_name) as source','r.st_receiving as status','image',"COALESCE(def_qty,'1') as default_qty"));
        $this->db->from('receiving r');
        $this->db->join('receiving_qty rq','rq.id_receiving=r.id_receiving');
        $this->db->join('inbound i','i.id_inbound=r.id_po','left');
        $this->db->join('supplier supl','supl.id_supplier=i.id_supplier and i.id_inbound_document NOT IN (2)','left');
        $this->db->join('m_customer cust','cust.id_customer=i.id_supplier and i.id_inbound_document IN (2)','left');
        $this->db->join('barang b','b.id_barang=rq.id_barang','left');
        $this->db->join('satuan sat','sat.id_satuan=b.id_satuan','left');
        $this->db->where('r.kd_receiving',$receivingCode);
        $this->db->group_by('b.id_barang');

        $rec  = $this->db->get()->result_array();

        return $rec;

    }

    public function putaway_serial($serial_number){

        /*
        */

        $this->db
            ->select(array('b.kd_barang as item_code','image',"(CASE WHEN r.start_tally = NULL AND r.finish_tally = NULL THEN FALSE ELSE TRUE END) as status"))
            ->from('receiving_barang rb')
            ->join('receiving r','r.id_receiving=rb.id_receiving')
            ->join('barang b','b.id_barang=rb.id_barang')
            ->join('satuan sat','sat.id_satuan=b.id_satuan','left')
            ->where('rb.kd_unik',$serial_number);

        $res = $this->db->get();

        return $res->row_array();

    }

}
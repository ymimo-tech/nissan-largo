<?php

class Api_staging_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function _curl_process($type='',$table='',$data=array()){
        $curl_handle = curl_init ();

        //supported format ['json','csv','jsonp','serialized','xml']
        //change url suffix ex : .json to .xml for change response format
        $clientid='test';
        $local_ip='::1';
        $staging_ip='localhost';
        $data2 = array (
                'table' => $table,
                'clientid' => $clientid,
                'hash' => sha1 ( $table . $clientid . $local_ip ),
                'xkey' => '0b5581b7ec815ad66a916b4fc5716d2ec84cd45e',
        );

        $data = array_merge($data,$data2);

        curl_setopt ( $curl_handle, CURLOPT_URL, 'http://'.$staging_ip.'/salubritas-staging/api/'.$type.'.json' );
        curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $curl_handle, CURLOPT_POST, 1 );
        curl_setopt ( $curl_handle, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
        curl_setopt ( $curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST );
        curl_setopt ( $curl_handle, CURLOPT_USERPWD, 'test:123456' );

        $buffer = curl_exec ( $curl_handle );
        curl_close ( $curl_handle );

        $data = json_decode($buffer);
        return $data;
    }

}

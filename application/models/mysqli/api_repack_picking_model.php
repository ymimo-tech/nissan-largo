<?php

class Api_repack_picking_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function loadDocument(){
    	$q = 	"SELECT 
					a.*,
					(SELECT 
						SUM(jumlah_kitting)
					FROM
						order_kitting_barang b
					WHERE
						b.id_order_kitting = a.id_order_kitting
					GROUP BY 
						b.id_order_kitting) AS total
				FROM 
					order_kitting a
				WHERE 
					a.st_repack = 0
						AND
					(a.status_order_kitting IS NOT NULL
						OR
					a.status_order_kitting <> 3)
				";
		$r = $this->db->query($q, false)->result_array();
		
		return $r;
    }
	
	function loadDetail($data = array()){
		$wh1 = "";
		$wh2 = "";
		for($i = 0; $i < count($data); $i++){
			if($data[$i]['order_kitting_type_id'] == 1){
				$wh1 .= "'". $data[$i]['id_order_kitting'] ."',";
			}else{
				$wh2 .= "'". $data[$i]['id_order_kitting'] ."',";
			}
		}
		$wh1 = ($wh1 == "") ? "" : rtrim($wh1, ", ");
		$wh2 = ($wh2 == "") ? "" : rtrim($wh2, ", ");

		/*
    	$q = 	"SELECT
					a.id,
					a.id_order_kitting,
					a.id_kitting,
					a.jumlah_kitting,
					c.id_barang,
					c.kd_barang,
					c.nama_barang
				FROM
					order_kitting_barang a
				LEFT JOIN
					m_kitting b 
						ON
					a.id_kitting = b.id_kitting
				LEFT JOIN
					barang c
						ON
					c.id_barang = b.id_barang
				WHERE 
					a.id_order_kitting in (". $wh .")
				";
		*/
		$r = array();

		if($wh2 != ''){
			$q =	"SELECT
						outbg.id,
						outbg.id_order_kitting,
						outbg.id_kitting,
						mk.id_barang,
						kd_barang,
						nama_barang,
						outbg.jumlah_kitting AS jumlah_kitting,
						COALESCE((
							SELECT 
								COALESCE(sum(qty),0)
							FROM
								order_kitting_picking
							WHERE
								id_order_kitting=outb.id_order_kitting
							AND
								(CASE WHEN id_barang=0 THEN kd_barang=brg.kd_barang ELSE id_barang=brg.id_barang END)
							GROUP BY
								id_barang
						),0) as jumlah_picking,
						rb.kd_all_location
					FROM
						order_kitting outb
					JOIN order_kitting_barang outbg ON outbg.id_order_kitting = outb.id_order_kitting
					JOIN m_kitting mk ON mk.id_kitting = outbg.id_kitting
					JOIN barang brg ON brg.id_barang = mk.id_barang
					LEFT JOIN (
						SELECT
							id_barang,
							GROUP_CONCAT(DISTINCT loc_name ORDER BY loc_name) as kd_all_location
						FROM
							receiving_barang rb
						LEFT JOIN m_loc loc
							ON loc.loc_id=rb.loc_id
						WHERE
							rb.loc_id not in (100,101,102,103,104,105,106)
						GROUP BY id_barang
					) as rb ON rb.id_barang=mk.id_barang
					WHERE 
						outbg.id_order_kitting in (". $wh2 .")
					GROUP BY brg.id_barang, id_order_kitting
					ORDER BY id_order_kitting, id_barang
					";

			$r = array_merge($r, $this->db->query($q, false)->result_array());
		}

		if($wh1 != ''){

			$q =	"SELECT
						outbg.id,
						outbg.id_order_kitting,
						outbg.id_kitting,
						mkp.id_barang,
						kd_barang,
						nama_barang,
						sum((mkp.qty * outbg.jumlah_kitting)) AS jumlah_kitting,
						COALESCE((
							SELECT 
								COALESCE(sum(qty),0)
							FROM
								order_kitting_picking
							WHERE
								id_order_kitting=outb.id_order_kitting
							AND
								(CASE WHEN id_barang=0 THEN kd_barang=brg.kd_barang ELSE id_barang=brg.id_barang END)
							GROUP BY
								id_barang
						),0) as jumlah_picking,
						rb.kd_all_location
					FROM
						order_kitting outb
					JOIN order_kitting_barang outbg ON outbg.id_order_kitting = outb.id_order_kitting
					JOIN m_kitting mk ON mk.id_kitting = outbg.id_kitting
					JOIN m_kitting_product mkp ON mkp.id_kitting = mk.id_kitting
					JOIN barang brg ON brg.id_barang = mkp.id_barang
					LEFT JOIN (
						SELECT
							id_barang,
							GROUP_CONCAT(DISTINCT loc_name ORDER BY loc_name) as kd_all_location
						FROM
							receiving_barang r
						LEFT JOIN m_loc loc
							ON loc.loc_id=r.loc_id
						WHERE
							r.loc_id not in (100,101,102,103,104,105,106)
						GROUP BY id_barang
					) as rb ON rb.id_barang=mkp.id_barang
					WHERE 
						outbg.id_order_kitting in (". $wh1 .")
					GROUP BY brg.id_barang, id_order_kitting
					ORDER BY id_order_kitting, id_barang
					";

			$r = array_merge($r, $this->db->query($q, false)->result_array());
		}

		return $r;
    }
	
	function validSN($data = array()){

    	$q = 	"SELECT
					a.*,
					NOW() as server_time
				FROM
					receiving_barang a
				JOIN
					barang c ON c.id_barang=a.id_barang
				WHERE
					a.kd_unik = '". $data['kd_unik'] ."'
						AND
					a.last_qty != 0
						AND
					c.kd_barang = '". $data['kd_barang'] ."'
				";
		$r = $this->db->query($q, false)->result_array();
		
		return $r;
    }
	
	function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
		return $this->db->get();
	}
	
	function postSN($data = array()){
		$kd_barang 	= $this->get_id("*", "id_barang", $data["id_barang"], "barang")->row_array();
		
    	$q = 	"INSERT INTO
					order_kitting_picking
					(id_order_kitting,kd_barang,kd_unik,qty,id_barang)
				VALUES
					('". $data['id_order_kitting'] ."','". $kd_barang['kd_barang'] ."','". $data['kd_unik'] ."','". $data['qty'] ."','". $data['id_barang'] ."')
				";
		$this->db->query($q, false);

		// $getLocId = $this->get_id('loc_id', "kd_unik",$data['kd_unik'], 'receiving_barang')->row_array();
		// $this->db
		// 	->set('kd_unik', $data['kd_unik'])
		// 	->set('loc_id_old', $getLocId['loc_id'])
		// 	->set('user_id_pick', $this->input->get_request_header('User', true))
		// 	->set('pick_time', 'NOW()')
		// 	->set('loc_id_new', 103)
		// 	->set('user_id_put', $this->input->get_request_header('User', true))
		// 	->set('put_time', 'NOW()')
		// 	->set('process_name','REPACK PICKING');

		// $this->db->insert('int_transfer_detail');

		// $u =	"UPDATE
		// 			receiving_barang
		// 		SET
		// 			last_qty = last_qty - ". $data['qty'] .",
		// 			loc_id=103
		// 		WHERE
		// 			kd_unik = '". $data['kd_unik'] ."'
		// 		";
		// $this->db->query($u, false);

		$this->db->set('status_order_kitting', 2)
			->where('status_order_kitting', 1)
			->where('id_order_kitting', $data['id_order_kitting']);

		$this->db->update('order_kitting');

		$serialize 	= $this->get_id("last_qty", "kd_unik", $data["kd_unik"], "receiving_barang")->row_array();

		$qty = $serialize['last_qty'] - $data['qty'];

		if($qty == 0){
			$this->db->set('loc_id',101);
		}

		$this->db->set("last_qty",$qty);
		$this->db->where('kd_unik',$data['kd_unik']);
		$this->db->update('receiving_barang');

    }

    function addTallyQty($post=array()){

		$this->db->select('id_order_kitting, b.id_barang, sum(qty) as qty');
		$this->db->from('order_kitting_picking okp');
		$this->db->join('barang b','b.kd_barang=okp.kd_barang','left');
		$this->db->where('id_order_kitting',$post['id_order_kitting']);
		$this->db->where('b.id_barang',$post['id_barang']);
		$this->db->group_by('b.id_barang');

		$row = $this->db->get()->row_array();

		$this->db->set('tally_doc_qty', $row['qty']);
		$this->db->where('id_barang', $row['id_barang']);
		$this->db->where('id_order_kitting', $row['id_order_kitting']);
		$this->db->update('order_kitting_barang_details');

    }

	function deleteSN($data = array()){
		$kd_barang 	= $this->get_id("kd_barang", "id_barang", $data["id_barang"], "barang")->row_array();
		
    	$q = 	"DELETE FROM
					order_kitting_picking
				WHERE
					id_order_kitting = '". $data['id_order_kitting'] ."'
						AND
					kd_barang = '". $kd_barang['kd_barang'] ."'
						AND
					kd_unik = '". $data['kd_unik'] ."'
						AND
					qty = '". $data['qty'] ."'
				";
		$this->db->query($q, false);
		
		$u =	"UPDATE
					receiving_barang
				SET
					last_qty = last_qty + ". $data['qty'] ."
				WHERE
					kd_unik = '". $data['kd_unik'] ."'
				";
		$this->db->query($u, false);
    }


	function itemPicked($data = array()){
    	$q = 	"SELECT
    				kd_unik, qty
				FROM
					order_kitting_picking
				WHERE
					id_order_kitting = '".$data['id_order_kitting']."'
						AND
					kd_barang = '".$data['kd_barang']."'
				";
		$r = $this->db->query($q, false)->result_array();
		return $r;
    }

    function getDocQty($data=array()){

		// $kd_barang 	= $this->get_id("kd_barang", "id_barang", $data["id_barang"], "barang")->row_array();

	   	$this->db
			->select(array('COALESCE(IF(ok.order_kitting_type_id=1, sum((qty * jumlah_kitting)), jumlah_kitting),0) as doc_qty'))
			->from('order_kitting ok')
			->join('order_kitting_barang okb','okb.id_order_kitting=ok.id_order_kitting','left')
			->join('m_kitting mk','mk.id_kitting=okb.id_kitting','left')
			->join('m_kitting_product mkp','mkp.id_kitting=mk.id_kitting','left')
			->where('ok.id_order_kitting', $data['id_order_kitting'])
			->where('( mkp.id_barang='.$data['id_barang'].' OR mk.id_barang='.$data['id_barang'].')',NULL);

		$res = $this->db->get()->row();
		return $res->doc_qty;
    }

    function getPickQty($data=array()){

		$kd_barang 	= $this->get_id("kd_barang", "id_barang", $data["id_barang"], "barang")->row_array();

		$this->db
			->select('sum(qty) as actual_bom_picked')
			->from('order_kitting_picking')
			->where('id_order_kitting', $data['id_order_kitting'])
			->where('kd_barang', $kd_barang['kd_barang']);

		$res = $this->db->get()->row();

		return $res->actual_bom_picked;
	}

    function checkQty($data = array()){

		$kd_barang 	= $this->get_id("kd_barang", "id_barang", $data["id_barang"], "barang")->row_array();

	   	$this->db
			->select('sum((qty * jumlah_kitting)) as bom_qty_doc')
			->from('order_kitting_barang okb','okb.id_order_kittin=ok.id_order_kitting')
			->join('m_kitting mk','mk.id_kitting=okb.id_kitting')
			->join('m_kitting_product mkp','mkp.id_kitting=mk.id_kitting')
			->where('id_order_kitting', $data['id_order_kitting'])
			->where('mkp.id_barang', $data['id_barang']);

		$bom_qty_doc = $this->db->get_compiled_select();

		$this->db
			->select('sum(qty) as actual_bom_picked')
			->from('order_kitting_picking')
			->where('id_order_kitting', $data['id_order_kitting'])
			->where('kd_barang', $kd_barang['kd_barang']);

		$actual_bom_picked = $this->db->get_compiled_select();

		$this->db->select("($bom_qty_doc) as bom_qty_doc, ($actual_bom_picked) as actual_bom_picked");
		$results = $this->db->get()->row_array();

		if($results['bom_qty_doc'] < $results['actual_bom_picked']){	
			return false;
		}

		return true;
    }

}
<?php
class referensi_supplier_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'supplier';

    public function get_new_supplier(){
        $result = $this->fina->get_where('m_t_supplier',array('SyncStatus'=>'N'));
        return $result;
    }

    public function create_supplier($data=array()){
        $result = $this->db->insert('supplier',$data);
        return $this->db->insert_id();
    }

    public function set_supplier_status($SupplierCode=0,$SyncDate=''){
        $result = $this->fina->update('m_t_supplier',array('SyncStatus'=>'Y','SyncDate'=>$SyncDate),array('SupplierCode'=>$SupplierCode));
        return $result;
    }

    public function check_supplier($kd_supplier=''){
        $result = $this->db->get_where('supplier',array('kd_supplier'=>$kd_supplier));
        return $result;
    }

    public function update_supplier($data=array(),$id=0){
        $result = $this->db->update('supplier',$data,array('id_supplier'=>$id));
        return $result;
    }

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_supplier'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

	public function getSupplier($post = array()){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					supplier
				ORDER BY
					CAST(SUBSTRING_INDEX(kd_supplier, \'-\', -1) AS UNSIGNED) ASC';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_supplier= $this->input->post("kd_supplier");
        $nama_supplier= $this->input->post("nama_supplier");
        $alamat_supplier= $this->input->post("alamat_supplier");
        $telepon_supplier= $this->input->post("telepon_supplier");
        $cp_supplier= $this->input->post("cp_supplier");
        $email_supplier= $this->input->post("email_supplier");

        if(!empty($kd_supplier)){
            $condition["a.kd_supplier like '%$kd_supplier%'"]=null;
        }


        if(!empty($nama_supplier)){
            $condition["a.nama_supplier like '%$nama_supplier%'"]=null;
        }

        if(!empty($alamat_supplier)){
            $condition["a.alamat_supplier like '%$alamat_supplier%'"]=null;
        }

        if(!empty($telepon_supplier)){
            $condition["a.telepon_supplier like '%$telepon_supplier%'"]=null;
        }

        if(!empty($cp_supplier)){
            $condition["a.cp_supplier like '%$cp_supplier%'"]=null;
        }

        if(!empty($email_supplier)){
            $condition["a.email_supplier like '%$email_supplier%'"]=null;
        }


        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.id_supplier');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_supplier;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
            $action .= '</li>';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_supplier' => $value->kd_supplier,
                    'nama_supplier' => $value->nama_supplier,
                    'alamat_supplier' => $value->alamat_supplier,
                    'telepon_supplier' => $value->telepon_supplier,
                    'cp_supplier' => $value->cp_supplier,
                    'email_supplier' => $value->email_supplier,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'kd_supplier' => $value->kd_supplier,
                    'nama_supplier' => $value->nama_supplier,
                    'alamat_supplier' => $value->alamat_supplier,
                    'telepon_supplier' => $value->telepon_supplier,
                    'cp_supplier' => $value->cp_supplier,
                    'email_supplier' => $value->email_supplier,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_supplier' => $id));
    }

    public function delete($id) {
        //return $this->db->delete($this->table, array('id_supplier' => $id));

		$result = array();

		$sql = 'SELECT
					(SELECT COUNT(*) FROM inbound WHERE id_supplier = \''.$id.'\' AND id_inbound_document != \'2\') AS t1,
					(SELECT COUNT(*) FROM outbound WHERE id_customer = \''.$id.'\' AND id_outbound_document = \'2\') AS t2';

		$row = $this->db->query($sql)->row_array();
		if(($row['t1'] + $row['t2']) > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('id_supplier' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}

        return $result;
    }

	public function options2($default = '--Choose Supplier--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_supplier] = $row->nama_supplier.' - '.$row->nama_supplier;
        }
        return $options;
    }

    public function options($default = '--Pilih Kode Supplier--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_supplier] = $row->nama_supplier ;
        }
        return $options;
    }

}

?>

<?php
class referensi_lokasi_area_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'm_loc_area';

    public function data($condition = array()) {

        $this->db->from($this->table  . ' a');
        $this->db->where('a.loc_area_name IS NOT NULL', NULL);
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function getLocationById($post = array()){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_loc_area
				WHERE
					loc_area_id IN('.implode(',', $post['loc_area_id']).')';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getLocation(){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_loc_area
				WHERE
					loc_area_name IS NOT NULL';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getQtyLocation(){
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					m_loc_area';

		$row = $this->db->query($sql)->row_array();
		$result['qty'] = $row['total'];

		return $result;
	}

    public function get_by_id($id) {
        $condition['a.loc_area_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    // public function data_table() {
    //     // Filtering
    //     $condition = array();
    //     $loc_area_name = $this->input->post("loc_area_name");
    //     $loc_area_desc = $this->input->post("loc_area_desc");
    //
    //     if(!empty($loc_area_name)){
    //         $condition["a.loc_area_name like '%$loc_area_name%'"]=null;
    //     }
    //
    //     if(!empty($loc_area_desc)){
    //         $condition["a.loc_area_desc like '%$loc_area_desc%'"]=null;
    //     }
    //
    //     // Total Record
    //     $total = $this->data($condition)->count_all_results();
    //
    //     // List Data
    //     $this->db->order_by('a.loc_area_id');
    //     $this->db->limit($this->limit, $this->offset);
    //     $data = $this->data($condition)->get();
    //     $rows = array();
    //
    //     foreach ($data->result() as $value) {
    //         $id = $value->loc_area_id;
    //
    //         $action = '<div class="btn-group">
    //                     <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
    //                     <i class="glyphicon glyphicon-flash"></i>
    //                     Action <i class="fa fa-angle-down"></i>
    //                     </button>';
    //
    //         $action .= '<ul class="dropdown-menu" role="menu">
    //                         <li>';
    //         $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
    //         $action .= '</li>';
    //
    //         if ($this->access_right->otoritas('edit')) {
    //             $action .= '<li>';
    //             $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
    //             $action .= '</li>';
    //         }
    //
    //         if ($this->access_right->otoritas('delete')) {
    //             $action .= '<li>';
    //             $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
    //             $action .= '</li>';
    //
    //         }
    //         $action .= '</ul>
    //                 </div>';
    //
    //         if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
    //             $rows[] = array(
    //                 'loc_area_name' => $value->loc_area_name,
    //                 'loc_area_desc' => $value->loc_area_desc,
    //                 'aksi' => $action
    //             );
    //         }else{
    //             $rows[] = array(
    //                 'loc_area_name' => $value->loc_area_name,
    //                 'loc_area_desc' => $value->loc_area_desc,
    //             );
    //         }
    //     }
    //
    //     return array('rows' => $rows, 'total' => $total);
    // }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('loc_area_id' => $id));
    }

    public function delete($id) {

		$result = array();

		$in = explode(',', $this->config->item('hardcode_location_id'));

		if(in_array($id, $in)){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete master location';
		}else{

			// $sql = 'SELECT
			// 			COUNT(*) AS total
			// 		FROM
			// 			receiving_barang
			// 		WHERE
			// 			loc_area_id=\''.$id.'\'';
            //
			// $row = $this->db->query($sql)->row_array();
            //
			// if($row['total'] > 0){
			// 	$result['status'] = 'ERR';
			// 	$result['message'] = 'Cannot delete location, because data have relation to another table';
			// }else{
				$this->db->delete($this->table, array('loc_area_id' => $id));

				$result['status'] = 'OK';
				$result['message'] = 'Delete data success';
			// }
		}

		return $result;

    }

    // public function options($default = '--Pilih Nama Lokasi--', $key = '') {
    //     $data = $this->data()->get();
    //     $options = array();
    //
    //     if (!empty($default))
    //         $options[$key] = $default;
    //
    //     foreach ($data->result() as $row) {
    //         $options[$row->loc_area_id] = $row->loc_area_name ;
    //     }
    //     return $options;
    // }

}

?>

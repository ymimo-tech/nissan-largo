<?php

class Api_picking_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function load(){
        $this->db->select("pl_name, kd_barang, qty");
        $this->db->from("picking_qty pqty");
        $this->db->join("picking_list plist", "pqty.pl_id = plist.pl_id", "left");
        $this->db->join("barang brg", "pqty.id_barang = brg.id_barang", "left");
        $this->db->where("plist.pl_status", 0);
        return $this->db->get();
    }

    function get($picking_code){
    	$this->db->select("pl_status");
    	$this->db->from("picking_list");
    	$this->db->where("pl_name", $picking_code);
    	return $this->db->get();
    }

    function get_count($data){
        $serialize  = $this->get_serial($data["kd_unik"])->row_array();
        if($serialize["first_qty"] == 0){
            $this->db->select("qty, (SELECT COUNT(*) FROM receiving_barang rcv_ LEFT JOIN picking_list pls_ ON rcv_.pl_id = pls_.pl_id WHERE pls_.pl_name = '" . $data["pl_name"] . "' AND rcv_.id_barang = pqt.id_barang) AS picked", false);
            $this->db->from("picking_qty pqt");
            $this->db->join("picking_list pls" , "pqt.pl_id = pls.pl_id", "left");
            $this->db->join("barang brg" , "pqt.id_barang = brg.id_barang", "left");
            $this->db->join("receiving_barang rcv" , "brg.id_barang = rcv.id_barang", "left");
            $this->db->join("m_loc loc" , "rcv.loc_id = loc.loc_id", "left");
            $this->db->where("pls.pl_name", $data["pl_name"]);
            $this->db->where("pqt.id_barang", $serialize["id_barang"]);
            return $this->db->get();
        } else{
            /*$this->db->select("qtya, (SELECT IFNULL(SUM(qty_pick), 0) FROM kitting_history kit_ LEFT JOIN picking_list pls_ ON kit_.pl_id = pls_.pl_id LEFT JOIN receiving_barang rcv_ ON kit_.kd_unik = rcv_.kd_unik LEFT JOIN barang brg_ ON rcv_.id_barang = brg_.id_barang WHERE pls_.pl_name = '" . $data["pl_name"] . "' AND rcv_.id_barang = pqt.id_barang) AS picked", false);
            $this->db->from("picking_qty pqt");
            $this->db->join("picking_list pls" , "pqt.pl_id = pls.pl_id", "left");
            $this->db->join("barang brg" , "pqt.id_barang = brg.id_barang", "left");
            $this->db->join("receiving_barang rcv" , "brg.id_barang = rcv.id_barang", "left");
            $this->db->join("m_loc loc" , "rcv.loc_id = loc.loc_id", "left");
            $this->db->where("pls.pl_name", $data["pl_name"]);
            $this->db->where("pqt.id_barang", $serialize["id_barang"]);*/
            $query = "
            SELECT
                qty,
                IFNULL(tpk.pick,0) AS picked
            FROM
                (`picking_qty` pqt)
            LEFT JOIN `picking_list` pls ON `pqt`.`pl_id` = `pls`.`pl_id`
            LEFT JOIN `barang` brg ON `pqt`.`id_barang` = `brg`.`id_barang`
            LEFT JOIN `receiving_barang` rcv ON `brg`.`id_barang` = `rcv`.`id_barang`
            LEFT JOIN `m_loc` loc ON `rcv`.`loc_id` = `loc`.`loc_id`
            LEFT JOIN(
                SELECT
                    IFNULL(SUM(qty_pick), 0)AS pick,
                    pls_.pl_name,
                    rcv_.id_barang
                FROM
                    kitting_history kit_
                LEFT JOIN picking_list pls_ ON kit_.pl_id = pls_.pl_id
                LEFT JOIN receiving_barang rcv_ ON kit_.kd_unik = rcv_.kd_unik
                LEFT JOIN barang brg_ ON rcv_.id_barang = brg_.id_barang
                WHERE
                    pls_.pl_name = '".$data["pl_name"]."'
                AND rcv_.id_barang = '".$serialize["id_barang"]."'
            )tpk ON tpk.pl_name = pls.pl_name
            AND tpk.id_barang = pqt.id_barang
            WHERE
                `pls`.`pl_name` = '".$data["pl_name"]."'
            AND `pqt`.`id_barang` = '".$serialize["id_barang"]."'";
            return $this->db->query($query);
        }
        
    }

    function get_picked2($picking_code){
        $ignore_location = array(100, 101, 102, 103, 104, 105, 106, 107, 108, 109);
        $avail_location    = array('BINLOC', 'QC');
		$this->db->_protect_identifiers = false;
        $this->db->select("kd_barang, GROUP_CONCAT(DISTINCT loc_name ORDER BY loc_name ASC) AS location, qty, (SELECT COUNT(*) FROM receiving_barang rcv_ LEFT JOIN picking_list pls_ ON rcv_.pl_id = pls_.pl_id WHERE pls_.pl_name = '" . $picking_code . "' AND rcv_.id_barang = pqt.id_barang) AS single_picked, (SELECT IFNULL(SUM(qty_pick), 0) FROM kitting_history kit_ LEFT JOIN picking_list pls_ ON kit_.pl_id = pls_.pl_id LEFT JOIN receiving_barang rcv_ ON kit_.kd_unik = rcv_.kd_unik LEFT JOIN barang brg_ ON rcv_.id_barang = brg_.id_barang WHERE pls_.pl_name = '" . $picking_code . "' AND rcv_.id_barang = pqt.id_barang) AS multiple_picked");
        $this->db->from("picking_qty pqt");
        $this->db->join("picking_list pls" , "pqt.pl_id = pls.pl_id", "left");
        $this->db->join("barang brg" , "pqt.id_barang = brg.id_barang", "left");
        $this->db->join("receiving_barang rcv" , "brg.id_barang = rcv.id_barang", "left");
        $this->db->join("m_loc loc" , "rcv.loc_id = loc.loc_id", "left");
        $this->db->where("pls.pl_name", $picking_code);
        $this->db->where("(rcv.pl_id IS NULL or rcv.last_qty >1)");
        // $this->db->where_not_in("rcv.loc_id", $ignore_location);
        $this->db->where_in("loc_type", $avail_location);
        $this->db->group_by("pqt.id_barang");
        return $this->db->get();
    }

    function get_picked($picking_code){
        /*
        $ignore_location = array(100, 101, 102, 103, 104, 105, 106, 107, 108, 109);
        $avail_location    = array('2', '3');
        $this->db->_protect_identifiers = false;
        $this->db->select("kd_barang, GROUP_CONCAT(DISTINCT loc_name ORDER BY loc_name ASC) AS location, qty, (SELECT COUNT(*) FROM receiving_barang rcv_ LEFT JOIN picking_list pls_ ON rcv_.pl_id = pls_.pl_id WHERE pls_.pl_name = '" . $picking_code . "' AND rcv_.id_barang = pqt.id_barang) AS single_picked, (SELECT IFNULL(SUM(qty_pick), 0) FROM kitting_history kit_ LEFT JOIN picking_list pls_ ON kit_.pl_id = pls_.pl_id LEFT JOIN receiving_barang rcv_ ON kit_.kd_unik = rcv_.kd_unik LEFT JOIN barang brg_ ON rcv_.id_barang = brg_.id_barang WHERE pls_.pl_name = '" . $picking_code . "' AND rcv_.id_barang = pqt.id_barang) AS multiple_picked");
        $this->db->from("picking_qty pqt");
        $this->db->join("picking_list pls" , "pqt.pl_id = pls.pl_id", "left");
        $this->db->join("barang brg" , "pqt.id_barang = brg.id_barang", "left");
        $this->db->join("receiving_barang rcv" , "brg.id_barang = rcv.id_barang", "left");
        $this->db->join("m_loc loc" , "rcv.loc_id = loc.loc_id", "left");
        $this->db->where("pls.pl_name", $picking_code);
        $this->db->where("rcv.pl_id IS NULL");
        $this->db->where_not_in("rcv.loc_id", $ignore_location);
        // $this->db->where_in("loc_type_id", $avail_location);
        $this->db->group_by("pqt.id_barang");
        return $this->db->get();
        */

        $q =    "SELECT
                    kd_barang,
                    IFNULL(GROUP_CONCAT(
                        DISTINCT loc_name
                        ORDER BY
                            loc_name ASC
                    ),'--No Stok--')AS location,
                    qty,
                    IFNULL(sp2.q_picked,0) AS single_picked,
                    IFNULL(sp.q_picked,0) AS multiple_picked
                FROM
                    (picking_qty pqt)
                LEFT JOIN(
                    SELECT
                        id_barang,
                        sum(qty)q_picked
                    FROM
                        picking_picked pr
                    LEFT JOIN picking_list pl ON pr.id_picking = pl.pl_id
                    WHERE
                        pl.pl_name = '" . $picking_code . "'
                    GROUP BY
                        id_barang
                )AS sp ON sp.id_barang = pqt.id_barang
                left join(
                    SELECT
                        id_barang,
                        sum(last_qty)q_picked
                    FROM
                        receiving_barang rb
                    LEFT JOIN picking_list pl ON rb.pl_id = pl.pl_id
                    WHERE
                        pl.pl_name = '" . $picking_code . "'
                    GROUP BY
                        id_barang) as sp2 on sp2.id_barang = pqt.id_barang
                LEFT JOIN picking_list pls ON pqt.pl_id = pls.pl_id
                LEFT JOIN barang brg ON pqt.id_barang = brg.id_barang
                LEFT JOIN receiving_barang rcv ON brg.id_barang = rcv.id_barang 
                    AND rcv.loc_id NOT IN(
                    100,
                    101,
                    102,
                    103,
                    104,
                    105,
                    106,
                    107,
                    108,
                    109
                )
                LEFT JOIN m_loc loc ON rcv.loc_id = loc.loc_id
                WHERE
                    `pls`.`pl_name` = '" . $picking_code . "'
                AND (rcv.pl_id IS NULL or rcv.last_qty >1)
                 
                GROUP BY
                    pqt.id_barang";

        $r = $this->db->query($q, false);

        return $r;
    }

    function retrieve($picking_code){
    	$this->db->select("pl_name, kd_barang, qty");
    	$this->db->from("picking_qty a");
    	$this->db->join("picking_list b", "a.pl_id = b.pl_id", "left");
    	$this->db->join("barang brg", "a.id_barang = brg.id_barang", "left");
    	$this->db->where("b.pl_name", $picking_code);
    	return $this->db->get();
    }

    function retrieve_location($picking_code){
        $q  =   "select
                  pl_name,
                  kd_barang,
                  IFNULL(GROUP_CONCAT(
                      DISTINCT e.loc_name
                      ORDER BY
                          e.loc_name ASC
                  ),'--No Stok--')AS loc_name,
                  has_qty, 
                  COUNT(d.kd_unik) AS qty, 
                  IFNULL(SUM(d.last_qty),0) AS multi_qty
                from
                  picking_qty a
                left join
                  picking_list b
                    on
                  a.pl_id = b.pl_id
                left join
                  barang c
                    on
                  a.id_barang = c.id_barang
                left join
                  receiving_barang d
                    on
                  a.id_barang = d.id_barang
                    and
                  d.loc_id not in (100, 101, 102, 103, 104, 105, 106, 107, 108, 109)
                left join
                  m_loc e
                    on
                  d.loc_id = e.loc_id
                where
                  b.pl_name = '". $picking_code ."' 
                group by
                  a.id_barang";
        $r = $this->db->query($q,false);

        return $r;
    }

    function get_batch($item_code, $batch_code){
    	$this->db->select("*");
    	$this->db->from("receiving_barang rcv");
    	$this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
    	$this->db->where("kd_barang", $item_code);
    	$this->db->where("kd_batch", $batch_code);
    	return $this->db->get();
    }

    function get_serial($serial_number){
        $ignore_location = array(100, 101, 102, 103, 104, 105, 106, 107, 108, 109);
		//$avail_location    = array('2', '3');
        $avail_location = '2 , 3';
    	$this->db->select("*");
    	$this->db->from("receiving_barang");
		$this->db->join("m_loc", "receiving_barang.loc_id = m_loc.loc_id", "left");
    	$this->db->where("kd_unik", $serial_number);
        $this->db->where("((pl_id IS NULL and loc_type_id in (".$avail_location.")) or last_qty >1)");
        // $this->db->where_not_in("loc_id", $ignore_location);
		//$this->db->where_in("loc_type_id", $avail_location);
    	return $this->db->get();
    }

    function retrieve_serial($serial_number){
        $this->db->select("kd_barang, has_qty, last_qty");
        $this->db->from("receiving_barang rcv");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->where("kd_unik", $serial_number);
        $this->db->where("(pl_id IS NULL or last_qty >1)");
        return $this->db->get();
    }

    function post_serial($data){
        $id_user    = $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
        $id_picking = $this->get_id("pl_id", "pl_name", $data["pl_name"], "picking_list")->row_array();
        //$id_barang  = $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
        $serialize  = $this->get_serial($data["kd_unik"])->row_array();
        switch ($data["code"]) {
            case 'single':
                $data_insert = array(   'kd_unik'       => $data["kd_unik"], 
                                        'loc_id_old'    => $serialize["loc_id"],
                                        'user_id_pick'  => $id_user["user_id"],
                                        'pick_time'     => $data["pick_time"],
										'process_name'	=> 'PICKING',
                                        'id_picking' => $id_picking["pl_id"]
                                        );
                $this->db->insert("int_transfer_detail", $data_insert);

                // $this->db->set("picked", "picked + 1", false);
                // $this->db->where("pl_id", $id_picking["pl_id"]);
                // $this->db->where("id_barang", $serialize["id_barang"]);
                // $this->db->update("picking_qty");
                $qty_after = $serialize["last_qty"] - $data["qty"];

                $this->db->set("last_qty", $qty_after);
                $this->db->where("kd_unik", $data["kd_unik"]);
                $this->db->update("receiving_barang");


                $this->db->set("pl_id", $id_picking["pl_id"]);
                $this->db->set("loc_id", 103);
                $this->db->where("kd_unik", $data["kd_unik"]);
                $this->db->update("receiving_barang");
                break;
            case 'multiple':
                $data_insert = array(   'pl_id'     => $id_picking["pl_id"],
                                        'kd_unik'   => $data["kd_unik"], 
                                        'first_qty' => $serialize["first_qty"],
                                        'qty_bef'   => $serialize["last_qty"],
                                        'qty_pick'  => $data["qty"],
                                        'qty_after' => $serialize["last_qty"] - $data["qty"],
                                        'user_kit'  => $id_user["user_id"],
                                        'kit_time'  => $data["kit_time"]
                                        );
                $this->db->insert("kitting_history", $data_insert);

                // $this->db->set("picked", "picked + " . $data["qty"], false);
                // $this->db->where("pl_id", $id_picking["pl_id"]);
                // $this->db->where("id_barang", $serialize["id_barang"]);
                // $this->db->update("picking_qty");

                $this->db->set("last_qty", $data_insert["qty_after"]);
                $this->db->where("kd_unik", $data["kd_unik"]);
                $this->db->update("receiving_barang");
                break;
            default:
                # nothing to do.
                break;
        }

        if($data['qty']==0 || $data['qty'] == NULL){
            $qty_insert = 1;
        }else{
            $qty_insert = $data['qty'];
        }

        $q =    "INSERT INTO
                    picking_picked
                    (id_picking,kd_unik,id_barang,loc_id,qty)
                VALUES
                    ('". $id_picking["pl_id"] ."','". $data['kd_unik'] ."','". $serialize["id_barang"] ."','101','". $qty_insert ."')
                ";
                
        $this->db->query($q, false);

        $q =    "INSERT INTO
                    picking_recomendation
                    (id_picking,kd_unik,id_barang,loc_id,qty)
                VALUES
                    ('". $id_picking["pl_id"] ."','". $data['kd_unik'] ."','". $serialize["id_barang"] ."','101','". $qty_insert ."')
                ";
                
        $this->db->query($q, false);
    }

    // function get_count($data){
    //     $serialize  = $this->get_serial($data["kd_unik"])->row_array();
    //     $this->db->select("qty, picked");
    //     $this->db->from("picking_qty pqt");
    //     $this->db->join("picking_list pls", "pqt.pl_id = pls.pl_id", "left");
    //     $this->db->where("pls.pl_name", $data["pl_name"]);
    //     $this->db->where("id_barang", $serialize["id_barang"]);
    //     return $this->db->get();
    // }

    function get_item($item_code){
        $this->db->select("*");
        $this->db->where("kd_barang", $item_code);
        $this->db->from("barang");
        return $this->db->get();
    }

    function post($data){
        $id_picking = $this->get_id("pl_id", "pl_name", $data["picking_code"], "picking_list")->row_array();
    	$id_barang 	= $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
    	$id_picker 	= $this->get_id("user_id", "user_name", $data["uname_pick"], "hr_user")->row_array();
        $id_putter  = $this->get_id("user_id", "user_name", $data["uname_put"], "hr_user")->row_array();
        $loc_old    = $this->get_id("loc_id", "loc_name", $data["old_loc"], "m_loc")->row_array();
        $loc_new    = $this->get_id("loc_id", "loc_name", $data["new_loc"], "m_loc")->row_array();
        $itemize    = $this->get_item($data["kd_barang"])->row_array();

    	if ($itemize["has_qty"] == 1) {
            $this->db->set("pl_id", $id_picking["pl_id"]);
	    	$this->db->set("pl_status", 1);
	    	$this->db->set("picking_time", $data["pick_time"]);
	    	$this->db->set("user_id_picking", $id_picker["user_id"]);
	    	$this->db->where("id_barang", $id_barang["id_barang"]);
	    	$this->db->where("kd_unik", $data["kd_unik"]);
	    	$this->db->update("receiving_barang");
    	} else{
            $this->db->set("loc_id", $loc_new["loc_id"]);
            $this->db->set("pl_id", $id_picking["pl_id"]);
            $this->db->set("pl_status", 1);
            $this->db->set("picking_time", $data["pick_time"]);
            $this->db->set("user_id_picking", $id_picker["user_id"]);
            $this->db->where("id_barang", $id_barang["id_barang"]);
            $this->db->where("kd_unik", $data["kd_unik"]);
            $this->db->update("receiving_barang");

            $this->db->set("loc_id_new", $loc_new["loc_id"]);
            $this->db->set("user_id_put", $id_putter["user_id"]);
            $this->db->set("put_time", $data["put_time"]);
			$this->db->set("process_name", "PICKING");
            $this->db->where("kd_unik", $data["kd_unik"]);
            $this->db->where("loc_id_old", $loc_old["loc_id"]);
            $this->db->where("user_id_pick", $id_picker["user_id"]);
            $this->db->where("loc_id_new IS NULL");
            $this->db->where("user_id_put IS NULL");
            $this->db->update("int_transfer_detail");
    	}
    }

    function update_location($picking_code,$data){
        $id_picking = $this->get_id("pl_id", "pl_name", $picking_code, "picking_list")->row_array();
        $id_picker  = $this->get_id("user_id", "user_name", $data["uname_pick"][0], "hr_user")->row_array();
        $loc_new    = $this->get_id("loc_id", "loc_name", $data["new_loc"][0], "m_loc")->row_array();
        $id_putter  = $this->get_id("user_id", "user_name", $data["uname_put"][0], "hr_user")->row_array();

        $this->db->set("pl_status", 1);
        $this->db->set("picking_time", $data["pick_time"][0]);
        $this->db->set("user_id_picking", $id_picker["user_id"]);

        $this->db->set("loc_id", 101);
        $this->db->where("pl_id", $id_picking["pl_id"]);
        $this->db->where("first_qty",1);
        $this->db->update("receiving_barang");

        $this->db->set("loc_id_new", 101);
        $this->db->set("user_id_put", $id_putter["user_id"]);
        $this->db->set("put_time", $data["put_time"][0]);
        $this->db->set("process_name", "PICKING");
        $this->db->where("id_picking",$id_picking["pl_id"]);
        $this->db->update("int_transfer_detail");        
    }
	
	function submit_recomendation($data){
		$id_picking = $this->get_id("pl_id", "pl_name", $data["picking_code"], "picking_list")->row_array();
		$id_barang 	= $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
		$loc_new    = $this->get_id("loc_id", "loc_name", $data["new_loc"], "m_loc")->row_array();
		
		$q = 	"INSERT INTO
					picking_recomendation
					(id_picking,kd_unik,id_barang,loc_id,qty)
				VALUES
					('". $id_picking["pl_id"] ."','". $data['kd_unik'] ."','". $id_barang["id_barang"] ."','". $loc_new["loc_id"] ."','". $data['qty'] ."')
				";
				
		$this->db->query($q, false);
	}

    function get_location($location){
        $this->db->select("*");
        $this->db->from("m_loc");
        $this->db->where("loc_name", $location);
        $this->db->where("loc_type", "OUTBOUND");
        return $this->db->get();
    }

    // function loc_name($serial_number){
    //     $this->db->select("loc_name");
    //     $this->db->from("receiving_barang rcv");
    //     $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
    //     $this->db->where("kd_unik", $serial_number);
    //     return $this->db->get();
    // }

    function loc_name($serial_number){
        $query  = $this->db->query("SELECT loc_name FROM receiving_barang rcv LEFT JOIN m_loc loc ON rcv.loc_id = loc.loc_id WHERE kd_unik = '" . $serial_number . "';");
        $row    = $query->row_array();
        if(isset($row)){
            return $row["loc_name"];
        } else{
            return null;
        }
    }

    function start($picking_code){
        $this->db->set("pl_start", "IFNULL(pl_start, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("pl_name", $picking_code);
        $this->db->update("picking_list");
    }

    function finish($picking_code){
        $this->db->set("pl_status", 1);
        $this->db->set("id_status_picking", 3);
        $this->db->set("pl_finish", date("Y-m-d H:i:s"));
        $this->db->where("pl_name", $picking_code);
        $this->db->update("picking_list");
    }

    function get_document(){

        $this->db->select("pl_name, kd_barang, sum(qty) as qty");
        $this->db->from("picking_list pl");
        $this->db->join("picking_qty pq","pq.pl_id = pl.pl_id");
        $this->db->join("barang brg", "pq.id_barang = brg.id_barang", "left");
        $this->db->where("pl.pl_status", 0);
        $this->db->group_by('pl.pl_id');

        return $this->db->get()->result_array();

    }

}
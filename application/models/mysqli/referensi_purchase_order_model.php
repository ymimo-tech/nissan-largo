<?php
class referensi_purchase_order_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'purchase_order';
    private $table2 = 'supplier';
    private $table4 = 'purchase_order_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $table8 = 'supplier_barang';
    private $kurs   = 'kurs';
    private $status_po = 'm_status_po';

    public function get_po_code(){
        $this->db->select('pre_code_po,inc_po');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_po.$result->inc_po;
    }

    public function add_po_code(){
        $this->db->set('inc_po',"inc_po+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

    private function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b','a.id_supplier = b.id_supplier','left');
        $this->db->join($this->kurs . ' c','a.id_kurs = c.id_kurs','left');
        $this->db->join($this->status_po . ' st','st.status_po_id = a.status_po_id','left');
        $this->db->where_condition($condition);
        $this->db->order_by('a.id_po DESC,a.kd_po DESC');
        return $this->db;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_receiving= $this->input->post("kd_receiving");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_po = $this->input->post('id_po');
        if(!empty($kd_receiving)){
            $condition["a.kd_receiving like '%$kd_receiving%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id_po_barang as id_po_barang');
        $this->db->from($this->table  . ' a'); //po
        $this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left'); //pob
        $this->db->join('v_receiving_per_po vrp','vrp.id_po = a.id_po and vrp.id_barang = b.id_barang','left');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_po($condition = array()) {
        $this->db->select('*,id_po_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');       
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail_supplier($condition = array()) {
        $this->db->select('*');
        $this->db->from($this->table  . ' a');
        //$this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');       
        $this->db->join($this->table2 . ' f',' a.id_supplier = f.id_supplier','left');
        $this->db->join($this->table8 . ' g',' g.id_supplier = f.id_supplier','left');
        $this->db->join($this->table5 . ' c',' c.id_barang = g.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function total_harga_po($id_po) {
        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }else{
            $condition["a.id_po"]=0;
        }
        $this->db->select('sum(harga_barang*jumlah_barang)as harga_total, id_po_barang,a.id_po as id_po_1');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');       
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->where_condition($condition);
        $this->db->group_by('id_po_1');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            foreach ($query->result() as $row) {
                return $row->harga_total;
            }
        }
        else{
            return 0;
        }
    }

    public function get_by_id($id) {
        $condition['a.id_po'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id_po_barang'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function get_kurs_simbol($id_po){
        $condition['a.id_po'] = $id_po;
        $hasil = '';
        $this->data($condition);
        $data = $this->db->get();

        foreach ($data->result() as $value) {
            $hasil = $value->lambang_kurs;
        }
        return $hasil;
    }

    public function data_table() {
        // Filtering
        $condition = array();
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_po >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_po <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        $kd_po= $this->input->post("kd_po");
        $nama_supplier= $this->input->post("kd_supplier");
        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        $terms = $this->input->post('terms');
        $delivery = $this->input->post('delivery');

        if(!empty($kd_po)){
            $condition["a.kd_po like '%$kd_po%'"]=null;
        }


        if(!empty($nama_supplier)){
            $condition[" (b.nama_supplier like '%$nama_supplier%' or b.kd_supplier like '%$nama_supplier%') "]=null;
        }
        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $condition["a.tanggal_po >= '$tanggal_awal'"]=null;
            $condition["a.tanggal_po <= '$tanggal_akhir'"]=null;
        }
        if(!empty($terms)){
            $condition["a.terms like '%$terms%'"]=null;
        }
        if(!empty($delivery)){
            $condition["a.delivery like '%$delivery%'"]=null;
        }

        
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_po;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu"> ';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_purchase_order/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_purchase_order/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if($value->nama_supplier != '' || $value->kd_supplier!='')
            {
                $nama_supplier = $value->kd_supplier.' / '.$value->nama_supplier;
            }else{
                $nama_supplier = '';
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_po,
                    'tanggal_po' => hgenerator::switch_tanggal($value->tanggal_po),
                    'kd_po' => anchor(null, $value->kd_po, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_purchase_order', 'data-source' => base_url('referensi_purchase_order/get_detail_purchase_order/' . $id))),
                    'nama_supplier' => $nama_supplier, 
                    'kd_receiving' => '',
                    'status'=>$value->nama_status, 
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_po,
                    'tanggal_po' => hgenerator::switch_tanggal($value->tanggal_po),
                    'kd_po' => $value->kd_po,
                    'nama_supplier' => $nama_supplier, 
                    'kurs' => '',
                    'status'=>$value->nama_status,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    } 

    public function checkExist($id_po,$id_barang){
        $this->db->from($this->table4);
        $where = array('id_po'=>$id_po,
            'id_barang' => $id_barang);
        $this->db->where($where);
        return $this->db->get()->num_rows();
    }

    public function data_table_detail() {
        $id_po = $this->input->post('id_po');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_po_barang;
            $action = '';
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';
                                
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_purchase_order/edit_detail/' .$id_po.'/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_purchase_order/delete_detail/' .$id_po.'/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang.' '.$value->nama_satuan,
                    'jumlah_rcv' => $value->qty_rec_po.' '.$value->nama_satuan,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'jumlah_rcv' => $value->qty_rec_po.' '.$value->nama_satuan,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }   

  

    public function edit_data_table_detail() {

        // Filtering
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_po = $this->input->post('id_po');

        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }


        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));

        
        // Total Record
        $total = $this->data_detail_po($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_po = $this->data_detail_po($condition)->get();
        $rows = array();

        $i_po = 1;
        foreach ($data_detail_po->result() as $value) {
            $hasil_po['id_barang'][$i_po] = $value->id_barang;
            $hasil_po['kd_barang'][$i_po] = $value->kd_barang;
            $hasil_po['nama_barang'][$i_po] = $value->nama_barang;
            $hasil_po['tipe_barang'][$i_po] = $value->tipe_barang;
            $hasil_po['detail_barang'][$i_po] = $value->detail_barang;
            $hasil_po['jumlah_barang'][$i_po] = $value->jumlah_barang;
            $hasil_po['harga_barang'][$i_po] = $value->harga_barang;
            $hasil_po['id_po_barang'][$i_po] = $value->id_po_barang;
            $i_po++;
        }

        // Total Record
        $total_su = $this->data_detail_supplier($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_supplier = $this->data_detail_supplier($condition)->get();
        $rows = array();

        $i_su = 1;
        foreach ($data_detail_supplier->result() as $value) {
            $hasil_su['id_barang'][$i_su] = $value->id_barang;
            $hasil_su['kd_barang'][$i_su] = $value->kd_barang;
            $hasil_su['nama_barang'][$i_su] = $value->nama_barang;
            $hasil_su['tipe_barang'][$i_su] = $value->tipe_barang;
            $hasil_su['detail_barang'][$i_su] = $value->detail_barang;
            $hasil_su['status_po'][$i_su] = FALSE;
            $i_su++;
        }

        for($i=1;$i<$i_po;$i++){
            for($j=1;$j<$i_su;$j++){
                if($hasil_po['kd_barang'][$i] == $hasil_su['kd_barang'][$j]){
                    $hasil_su['status_po'][$j] = TRUE;
                }
            }

            $id = $hasil_po['id_po_barang'][$i];
            $action = '';
            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_supplier/edit_detail/' .$id.'/'. $id))) . ' ';
                }

            if ($this->access_right->otoritas('delete')) {
                $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_supplier/delete_detail/' .$id.'/'. $id)));
            }
            # code...
            $rows[] = array(
                    'kd_barang'=>$hasil_po['kd_barang'][$i],
                    'nama_barang' =>$hasil_po['nama_barang'][$i],
                    'tipe_barang' => $hasil_po['tipe_barang'][$i],
                    'detail_barang' => $hasil_po['detail_barang'][$i],
                    'jumlah_barang' => form_input("jumlah_barang_".$id_po."_".$hasil_po['id_barang'][$i],$hasil_po['jumlah_barang'][$i],'id = "jumlah_barang_'.$id_po."_".$i.'"class="span12 jumlah_barang" data-a-dec="," data-a-sep="."'),
                    'harga_barang' => form_input("harga_barang_".$id_po."_".$hasil_po['id_barang'][$i],$hasil_po['harga_barang'][$i],'id="harga_barang_'.$id_po."_".$i.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'),
                    'harga_total' => form_input("harga_total_".$id_po."_".$hasil_po['id_barang'][$i],$hasil_po['harga_barang'][$i] * $hasil_po['jumlah_barang'][$i],'id = "harga_total_'.$id_po."_".$i.'" class="span12 " data-a-dec="," data-a-sep="."'). form_hidden('id_barang[]',$hasil_po['id_barang'][$i]).'
                    <script type="text/javascript">
                    $(function() {
                        $(".numeric").numeric();
                        $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_barang_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_total_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_barang_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_satuan = parseInt($("#harga_barang_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_total = harga_satuan * jumlah;
                            $("#harga_total_'.$id_po."_".$i.'").autoNumeric("set",harga_total);                  
                        });
                        $("#jumlah_barang_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_satuan = parseInt($("#harga_barang_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_total = harga_satuan * jumlah;
                            $("#harga_total_'.$id_po."_".$i.'").autoNumeric("set",harga_total);                  
                        });
                        $("#harga_total_'.$id_po."_".$i.'").change(function() {
                            var jumlah =  $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_total = parseInt($("#harga_total_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_satuan = harga_total / jumlah;
                            $("#harga_barang_'.$id_po."_".$i.'").autoNumeric("set",harga_satuan);
                        });
                    });
                    </script>'
                );
        }
        for($i=1;$i<$i_su;$i++){
            if($hasil_su['status_po'][$i] != TRUE){
                $rows[] = array(
                    'kd_barang'=>$hasil_su['kd_barang'][$i],
                    'nama_barang' =>$hasil_su['nama_barang'][$i],
                    'tipe_barang' => $hasil_su['tipe_barang'][$i],
                    'detail_barang' => $hasil_su['detail_barang'][$i],
                    'jumlah_barang' => form_input("jumlah_barang_".$id_po."_".$hasil_su['id_barang'][$i],'','id = "jumlah_barang_'.$id_po."_".$i.'"class="span12 jumlah_barang" data-a-dec="," data-a-sep="."'),
                    'harga_barang' => form_input("harga_barang_".$id_po."_".$hasil_su['id_barang'][$i],'','id="harga_barang_'.$id_po."_".$i.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'),
                    'harga_total' => form_input("harga_total_".$id_po."_".$hasil_su['id_barang'][$i],'','id = "harga_total_'.$id_po."_".$i.'" class="span12 " data-a-dec="," data-a-sep="."'),
                    'action' => form_hidden('id_barang[]',$hasil_su['id_barang'][$i]).'
                    <script type="text/javascript">
                    $(function() {
                        $(".numeric").numeric();
                        $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_barang_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_total_'.$id_po."_".$i.'").autoNumeric("init");
                        $("#harga_barang_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_satuan = parseInt($("#harga_barang_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_total = harga_satuan * jumlah;
                            $("#harga_total_'.$id_po."_".$i.'").autoNumeric("set",harga_total);                  
                        });
                        $("#jumlah_barang_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_satuan = parseInt($("#harga_barang_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_total = harga_satuan * jumlah;
                            $("#harga_total_'.$id_po."_".$i.'").autoNumeric("set",harga_total);                  
                        });
                        $("#harga_total_'.$id_po."_".$i.'").change(function() {
                            var jumlah = $("#jumlah_barang_'.$id_po."_".$i.'").autoNumeric("get");
                            var harga_total = parseInt($("#harga_total_'.$id_po."_".$i.'").autoNumeric("get"));
                            var harga_satuan = harga_total / jumlah;
                            $("#harga_barang_'.$id_po."_".$i.'").autoNumeric("set",harga_satuan);
                        });
                    });
                    </script>'
                );
            }
        }

        return array('rows' => $rows, 'total' => $total_su);
    }    

    public function data_table_detail_excel() {

        // Filtering
        $condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_po = $this->input->post('id_po');

        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_po)){
            $condition["a.id_po"]=$id_po;
        }


        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));

        
        // Total Record
        $total = $this->data_detail_po($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_po = $this->data_detail_po($condition)->get();
        $rows = array();

        $i_po = 1;
        foreach ($data_detail_po->result() as $value) {
            $hasil_po['kd_barang'][$i_po] = $value->kd_barang;
            $hasil_po['nama_barang'][$i_po] = $value->nama_barang;
            $hasil_po['tipe_barang'][$i_po] = $value->tipe_barang;
            $hasil_po['detail_barang'][$i_po] = $value->detail_barang;
            $hasil_po['jumlah_barang'][$i_po] = $value->jumlah_barang;
            $hasil_po['harga_barang'][$i_po] = $value->harga_barang;
            $hasil_po['id_po_barang'][$i_po] = $value->id_po_barang;
            $hasil_po['nama_satuan'][$i_po] = $value->nama_satuan;
            $i_po++;
        }

        // Total Record
        $total_su = $this->data_detail_supplier($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail_supplier = $this->data_detail_supplier($condition)->get();
        $rows = array();

        $i_su = 1;
        foreach ($data_detail_supplier->result() as $value) {
            $hasil_su['id_barang'][$i_su] = $value->id_barang;
            $hasil_su['kd_barang'][$i_su] = $value->kd_barang;
            $hasil_su['nama_barang'][$i_su] = $value->nama_barang;
            $hasil_su['tipe_barang'][$i_su] = $value->tipe_barang;
            $hasil_su['detail_barang'][$i_su] = $value->detail_barang;
            $hasil_su['status_po'][$i_su] = FALSE;
            $i_su++;
        }
        //echo $i_su;
        

        for($i=1;$i<$i_po;$i++){
            for($j=1;$j<$i_su;$j++){
                if($hasil_po['kd_barang'][$i] == $hasil_su['kd_barang'][$j]){
                    $hasil_su['status_po'][$j] = TRUE;
                }
            }
            $id = $hasil_po['id_po_barang'][$i];
            if($id!=''){
                $action = '';
                if ($this->access_right->otoritas('edit')) {
                    $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_supplier/edit_detail/' .$id.'/'. $id))) . ' ';
                    }

                if ($this->access_right->otoritas('delete')) {
                    $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_supplier/delete_detail/' .$id.'/'. $id)));
                }
                # code...
                if($hasil_po['tipe_barang'][$i]!=''){
                    $tipe_barang_pr = ', '.$hasil_po['tipe_barang'][$i];
                }else{
                    $tipe_barang_pr = '';
                }
                if($hasil_po['detail_barang'][$i]!=''){
                    $detail_barang_pr = ', '.$hasil_po['detail_barang'][$i];
                }else{
                    $detail_barang_pr = '';
                }
                $nama_barang_pr = $hasil_po['nama_barang'][$i].$tipe_barang_pr.$detail_barang_pr;
                $rows[] = array(
                        'kd_barang'=>$hasil_po['kd_barang'][$i],
                        'nama_barang' =>$hasil_po['nama_barang'][$i],
                        'jumlah_barang' => $hasil_po['jumlah_barang'][$i],
                        'satuan' => $hasil_po['nama_satuan'][$i],
                        'harga_barang' => hgenerator::numberexcel($hasil_po['harga_barang'][$i]),
                        'harga_total' => hgenerator::rupiah($hasil_po['harga_barang'][$i] * $hasil_po['jumlah_barang'][$i])
                    );
            }
        }

        return array('rows' => $rows, 'total' => $total_su);
    }   

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_po' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_po' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id_po_barang' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id_po_barang' => $id));
    }
    
    public function options($default = '--Choose Purchase Order--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_po] = $row->kd_po.' / '.$row->nama_po ;
        }
        return $options;
    }

    public function options_open($default = '--Choose Purchase Order--', $key = '') {
        $data = $this->data(array('a.status_po_id'=>1))->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_po] = $row->kd_po.' / '.$row->nama_po ;
        }
        return $options;
    }

    
}

?>
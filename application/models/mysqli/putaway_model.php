<?php
class putaway_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    // private $table  = 'm_loc';
    // private $table2 = 'receiving_barang';
    // private $table3 = 'barang';
    // private $table4 = 'satuan';

    private $table  = 'receiving';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'receiving_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $table8 = 'supplier_barang';
    private $po_barang = 'purchase_order_barang';
    private $kurs   = 'kurs';
    private $loc  = 'm_loc';

    private function data($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");
        /*$tanggal_awal = $this->input->post('tanggal_awal_bbk');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbk');*/

        // if(!empty($loc_name)){
        //     $condition["a.loc_name like '%$loc_name%'"]=null;
        // }
        // if(!empty($loc_desc)){
        //     $condition["b.loc_desc like '%$loc_desc%'"]=null;
        // }

       /* if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.tanggal_do >= '$tanggal_awal'"] = null ;
            $condition["a.tanggal_do <= '$tanggal_akhir'"] = null;
        }*/


        $this->db->select('*,b.id_supplier as id_supplier');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table3 . ' c',' a.id_po = c.id_po','left');
        $this->db->join($this->table2 . ' b',' c.id_supplier = b.id_supplier','left');
        $this->db->order_by('a.tanggal_receiving DESC,a.kd_receiving DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_print_barcode($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");

        $this->db->select('kd_barang,nama_barang, jumlah_barang as qty, kd_receiving, tanggal_receiving, kd_po, kd_supplier, nama_supplier,nama_kategori, nama_satuan,r.*,po.*');
        $this->db->from('receiving r');
        $this->db->join('purchase_order po',' r.id_po = po.id_po','left');
        $this->db->join('purchase_order_barang pob','po.id_po = pob.id_po','left');
        $this->db->join('supplier s','po.id_supplier = s.id_supplier','left');
        $this->db->join('barang b','b.id_barang = pob.id_barang','left');
        $this->db->join('kategori k','k.id_kategori = b.id_kategori','left');
        $this->db->join('satuan sat','sat.id_satuan = b.id_satuan','left');
        $this->db->order_by('b.id_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_unik($condition = array()) {
        $condition['loc_id'] = NULL;
        $this->db->from($this->table2  . ' a');
        $this->db->order_by('a.kd_unik ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $id_receiving = $this->input->post("id_receiving");

        if(!empty($id_receiving)){
            $condition["a.id_receiving"]=$id_receiving;
        }

        //-----------end filtering-------------------

        $this->db->select('*');
        $this->db->from($this->table4  . ' a');//receiving_barang
        $this->db->join($this->loc . ' b',' a.loc_id = b.loc_id','left');//receiving_barang
        $this->db->join($this->table5 . ' br',' br.id_barang = a.id_barang','left');//barang
        //$this->db->join($this->table4 . ' c',' c.id_satuan = b.id_satuan','left');//barang
        $this->db->order_by('a.kd_unik ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['r.id_receiving'] = $id;
        $this->data_print_barcode($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['a.id_receiving_barang'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        $condition = array();
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_receiving;
            $action = '';

            $rows[] = array(
                'primary_key' =>$value->id_receiving,
                'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                'kd_receiving' => anchor(null, $value->kd_receiving, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_putaway', 'data-source' => base_url('putaway/get_detail_putaway/' . $id))) ,
                'kd_po' => $value->kd_po,
                'supplier' => $value->nama_supplier,
                'aksi' => ''
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail() {
        $id_receiving = $this->input->post('id_receiving');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_receiving_barang;
            $action = '';
            if(empty($value->loc_name)){
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';


            if ($this->access_right->otoritas('edit')) {
                
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('putaway/edit_detail/'.$id.'/' .$value->kd_unik))) . ' ';
                $action .= '</li>';
                
            }

            /*if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('putaway/delete_detail/'.$id_receiving.'/'. $id)));
                $action .= '</li>';

            }*/
            $action .= '</ul>
                    </div>';
            }
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_unik' => $value->kd_unik,
                    'item_code' => $value->kd_barang,
                    'item_name' => $value->nama_barang,
                    'loc_name' => $value->loc_name,
                );
            }else{
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_unik' => $value->kd_unik,
                    'item_code' => $value->kd_barang,
                    'item_name' => $value->nama_barang,
                    'loc_name' => $value->loc_name,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_do >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_do <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->loc_id;
            $action = '';
                $rows[] = array(
                    'tanggal_do' => hgenerator::switch_tanggal($value->tanggal_do),
                    'kd_do' => $value->kd_do,
                    'proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                );
        }
        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->loc_id_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('loc_id' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id_receiving_barang' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table4, array('loc_id' => $id));
        return $this->db->delete($this->table, array('loc_id' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }


    public function options($default = '--Pilih Kode bbk--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->loc_id] = $row->kd_do ;
        }
        return $options;
    }

    public function options_unik_kode($default = '--Pilih Unik Kode--', $key = '') {
        $data = $this->data_unik()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_receiving_barang] = $row->kd_unik ;
        }
        return $options;
    }




}

?>

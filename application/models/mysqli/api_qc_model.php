<?php

class Api_qc_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($serial_number){
        $this->db->select("loc_id");
        $this->db->from("receiving_barang");
        $this->db->where("kd_unik", $serial_number);
        $this->db->where("id_barang IS NOT NULL");
        $this->db->where_not_in("id_qc", 4);
        return $this->db->get();
    }

    function get_out($serial_number){
        $this->db->select("loc_id");
        $this->db->from("receiving_barang");
        $this->db->where("kd_unik", $serial_number);
        $this->db->where("id_barang IS NOT NULL");
        $this->db->where("id_qc", 2);
        return $this->db->get();
    }

    function retrieve($serial_number){
    	$this->db->select("loc_name, kd_qc");
    	$this->db->from("receiving_barang rcv");
    	$this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
    	$this->db->join("m_qc qc", "rcv.id_qc = qc.id_qc", "left");
    	$this->db->where("kd_unik", $serial_number);
    	return $this->db->get();
    }

    function intransit($serial_number){
    	$this->db->set("loc_id", 103);
        //$this->db->set("id_qc", 2);
    	$this->db->where("kd_unik", $serial_number);
    	$this->db->update("receiving_barang");
    }

    function get_location($location){
        $this->db->select("loc_name");
        $this->db->from("m_loc");
        $this->db->where("loc_name", $location);
        $this->db->where("loc_type", "QC");
        return $this->db->get();
    }

    function get_location_out($location){
        $available = array('1','2','3','5');
        $this->db->select("loc_name, loc_type");
        $this->db->from("m_loc");
        $this->db->where("loc_name", $location);
        $this->db->where_in("loc_type_id", $available);
        return $this->db->get();
    }

    function cancel($data){
    	$id_loc 	 	= $this->get_id("loc_id", "loc_name", $data["loc_name"], "m_loc")->row_array();
    	$id_user		= $this->get_id("user_id", "user_name", $data["user"], "hr_user")->row_array();
    	$id_qc 			= $this->get_id("id_qc", "kd_qc", $data["kd_qc"], "m_qc")->row_array();
    	
        $this->db->set("loc_id", $id_loc["loc_id"]);
        $this->db->set("id_qc", $id_qc["id_qc"]);
    	$this->db->where("kd_unik", $data["kd_unik"]);
    	$this->db->update("receiving_barang");

        $data_insert = array(   'kd_unik'       => $data["kd_unik"], 
                                'loc_id_old'    => $id_loc["loc_id"],
                                'user_id_pick'  => $id_user["user_id"],
                                'pick_time'     => $data["pick_time"],
                                'loc_id_new'    => $id_loc["loc_id"],
                                'user_id_put'  	=> $id_user["user_id"],
                                'put_time' 		=> date("Y-m-d H:i:s"),
								'process_name'	=> 'QC CANCEL'
                                );
        $this->db->insert("int_transfer_detail", $data_insert);
    }

    function post($data){
    	$id_loc_old 	= $this->get_id("loc_id", "loc_name", $data["loc_name_old"], "m_loc")->row_array();
    	$id_user_old 	= $this->get_id("user_id", "user_name", $data["user_pick"], "hr_user")->row_array();
    	$id_loc_new		= $this->get_id("loc_id", "loc_name", $data["loc_name_new"], "m_loc")->row_array();
    	$id_user_new 	= $this->get_id("user_id", "user_name", $data["user_put"], "hr_user")->row_array();
        if(empty($data['kd_qc']))
            $data['kd_qc'] = 'GOOD';
        if($data['kd_qc'] == '')
            $data['kd_qc'] = 'GOOD';
    	$id_qc 			= $this->get_id("id_qc", "kd_qc", $data["kd_qc"], "m_qc")->row_array();
    	
        $this->db->set("loc_id", $id_loc_new["loc_id"]);
        $this->db->set("id_qc", $id_qc["id_qc"]);
    	$this->db->where("kd_unik", $data["kd_unik"]);
    	$this->db->update("receiving_barang");

        $data_insert = array(   'kd_unik'       => $data["kd_unik"], 
                                'loc_id_old'    => $id_loc_old["loc_id"],
                                'user_id_pick'  => $id_user_old["user_id"],
                                'pick_time'     => $data["pick_time"],
                                'loc_id_new'    => $id_loc_new["loc_id"],
                                'user_id_put'  	=> $id_user_new["user_id"],
                                'put_time' 		=> $data["put_time"],
								'process_name'	=> 'QC'
                                );
        $this->db->insert("int_transfer_detail", $data_insert);
    }

}
<?php
class referensi_barang_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'barang';

    public function get_new_barang(){
        $result = $this->fina->get_where('m_t_item',array('SyncStatus'=>'N'));
        return $result;
    }

    public function create_barang($data=array()){
        $result = $this->db->insert('barang',$data);
        return $this->db->insert_id();
    }

    public function set_barang_status($SKU=0,$SyncDate=''){
        $result = $this->fina->update('m_t_item',array('SyncStatus'=>'Y','SyncDate'=>$SyncDate),array('SKU'=>$SKU));
        return $result;
    }

    public function get_barang_category($category=''){
        $result = $this->db->get_where('kategori',array('kd_kategori'=>$category));
        return ($result->num_rows()?$result->row()->id_kategori:0);
    }

    public function get_barang_satuan($satuan=''){
        $result = $this->db->get_where('satuan',array('kd_satuan'=>$satuan));
        return ($result->num_rows()?$result->row()->id_satuan:0);
    }

    public function check_barang($kd_barang=''){
        $result = $this->db->get_where('barang',array('kd_barang'=>$kd_barang));
        return $result;
    }

    public function update_barang($data=array(),$id=0){
        $result = $this->db->update('barang',$data,array('id_barang'=>$id));
        return $result;
    }

    public function update_kategori(){
        $this->db->from('barang');
        $hasil = $this->db->get();
        foreach ($hasil->result() as $row) {
            $id_kategori = $row->id_kategori;
            while($id_kategori > 4 ){
                $id_kategori = $id_kategori - 4;
                $this->db->where('id_barang',$row->id_barang);
                $this->db->update('barang',array('id_kategori'=>$id_kategori));
            }
            # code...
        }
        return 'sukses';
    }

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->join('kategori b', 'a.id_kategori = b.id_kategori', 'left');
		//$this->db->join('kategori_2 g', 'a.id_kategori_2 = g.id_kategori_2', 'left');
        $this->db->join('satuan c', 'a.id_satuan = c.id_satuan', 'left');
        $this->db->join('owner d', 'a.id_owner = d.id_owner', 'left');
		$this->db->join('m_shipment_type e', 'a.shipment_type = e.shipment_type_code', 'left');
		$this->db->join('m_shipment_periode f', 'a.fifo_period = f.periode_code', 'left');
        $this->db->where_condition($condition);
        $this->db->order_by('a.id_barang');
        return $this->db;
    }

    public function get_all_categories() {
        return $this->db->get("kategori");
    }

    public function get_all_satuan() {
        return $this->db->get("satuan");
    }

    public function get_all_suppliers() {
        return $this->db->get("supplier");
    }

    public function get_by_id($id) {
        $condition['a.id_barang'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);

        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_barang = $this->input->post('kd_barang');
        $nama_barang = $this->input->post('nama_barang');
        $kd_kategori = $this->input->post('kd_kategori');
        $kd_satuan = $this->input->post('kd_satuan');
        $tipe_barang = $this->input->post('tipe_barang');
        $shipment_type = $this->input->post('shipment_type');
        $fifo_period = $this->input->post('fifo_period');
        $owner_name = $this->input->post('owner_name');

        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"]=null;
        }

        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"]=null;
        }

        if(!empty($kd_kategori)){
            $condition["left join kategori on a.id_kategori = kategori.id_kategori"];
            $condition["a.kd_kategori like '%$kd_kategori%'"]=null;
        }

        if(!empty($kd_satuan)){
            $condition["left join satuan on a.id_satuan = satuan.id_satuan"];
            $condition["a.kd_satuan like '%$kd_satuan%'"]=null;
        }

        if(!empty($tipe_barang)){
            $condition["a.tipe_barang like '%$tipe_barang%'"]=null;
        }

        if(!empty($shipment_type)){
            $condition["a.shipment_type like '%$shipment_type%'"]=null;
        }

        if(!empty($fifo_period)){
            $condition["a.fifo_period like '%$fifo_period%'"]=null;
        }

        if(!empty($owner_name)){
            $condition["left join owner on a.id_owner = owner.id_owner"];
            $condition["a.owner_name like '%$owner_name%'"]=null;
        }


        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.id_barang');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
            $action .= '</li>';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'alamat_barang' => $value->alamat_barang,
                    'telepon_barang' => $value->telepon_barang,
                    'cp_barang' => $value->cp_barang,
                    'email_barang' => $value->email_barang,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'alamat_barang' => $value->alamat_barang,
                    'telepon_barang' => $value->telepon_barang,
                    'cp_barang' => $value->cp_barang,
                    'email_barang' => $value->email_barang,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_barang' => $id));
    }

	public function getPickingStrategy(){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_shipment_type';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getPickingPeriod(){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_shipment_periode';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    public function delete($id) {
		$result = array();

		$sql = 'SELECT
					(SELECT COUNT(*) FROM inbound_barang WHERE id_barang = \''.$id.'\') AS t1,
					(SELECT COUNT(*) FROM receiving_barang WHERE id_barang = \''.$id.'\') AS t2';

		$row = $this->db->query($sql)->row_array();
		if(($row['t1'] + $row['t2']) > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('id_barang' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}


        return $result;
    }

    public function options($default = '--Pilih Kode Barang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->nama_barang ;
        }
        return $options;
    }

	function create_options_code($table_name, $value_column, $caption_column) {
		if($table_name == 'm_shipment_type')
			$this->db->order_by('id_shipment_type', "ASC");
		else
			$this->db->order_by('id_periode', "ASC");

        $data = $this->db->get($table_name)->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column) {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    function shipmentTypeOptions() {
        $data = $this->db->get('m_shipment_type')->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt['shipment_type_code']] = $dt['shipment_type_code'];
        }
        return $options;
    }

    function shipmentPeriodOptions() {
        $data = $this->db->get('m_shipment_periode')->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt['periode_code']] = $dt['periode_code'];
        }
        return $options;
    }

    public function is_unique($itemCode){

        $this->db->where('a.kd_barang',$itemCode);

        $dataLength = $this->data()->get()->num_rows();;
        if($dataLength){
            return 0;
        }else{
            return 1;
        }
    }

}

?>

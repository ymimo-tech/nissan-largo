<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_model extends CI_Model {

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}

	function get_all() {
		return $this->data()->get();
	}

	private function data($condition = array()) {
		$this->db->select("*");
		$this->db->from("hr_user a");
		$this->db->join("hr_grup b", "a.grup_id = b.grup_id");
		$this->db->where_condition($condition);

		return $this->db;
	}

	public function get_user_with_id($id) {
		return $this->data(array('user_id' => $id))->get();
	}
	
	public function login($username='',$password=''){
		/*$sql = "SELECT res_users.id,username,level_id,res_users.name,avatar,pers_no,gender,cost_center.name as cost_center,cost_center_id,res_level.code,manager_id,code_area FROM res_users LEFT JOIN cost_center on res_users.cost_center_id=cost_center.id LEFT JOIN res_level on res_users.level_id=res_level.id where active=1 and username=".$this->db->escape($username)." and password=SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1(" . $this->db->escape($password) . "))))) LIMIT 0,1";
		$res = $this->db->query($sql);*/
		$this->db->from('hr_user');
		$this->db->where('user_name',$username);
		$this->db->where('user_password',md5($password));
		$res = $this->db->get();
		return $res->row();
	}
	
	public function get_user_data($data=array()){
		$this->db->select('id,username,name,email,avatar,address,gender,birthdate');
		$res = $this->db->get_where('res_users',$data);
		return $res->row();
	}
	
	public function update_user_data($data=array()){
		$res=$this->db->update('res_users',$data,array('id'=>$data['id']));
		return $res;
	}
	
	public function check_pass($data=array()){
		$sql = "SELECT id FROM res_users where id=".$this->db->escape($data['id'])." and password=SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1(" . $this->db->escape($data['password']) . "))))) LIMIT 0,1";
		$res = $this->db->query($sql);
		return $res->num_rows();
	}
	
	public function update_pass($data=array()){
		$sql = "UPDATE res_users set salt = " . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . ", password = " . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . " where id=".$this->db->escape($data['id']);
		$res = $this->db->query($sql);
		return $res;
	}
	
	public function reset_pass($data=array()){
		$sql = "UPDATE res_users set resfp = 1, salt = " . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . ", password = " . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . " where id=".$this->db->escape($data['id']);
		$res = $this->db->query($sql);
		return $res;
	}
	
	public function update_avatar($data=array()){
		$res=$this->update_user_data($data);
		return $res;
	}
	
	public function get_avatar($data=array()){
		$this->db->select('avatar');
		$res = $this->db->get_where('res_users',$data);
		return $res->row()->avatar;
	}

	public function create($data) {
		return $this->db->insert("hr_user", $data);
	}

	public function update($data, $id) {
		$this->db->where("user_id", $id);
		return $this->db->update("hr_user", $data);
	}

	public function delete($id) {
		return $this->db->delete("hr_user", array('user_id' => $id));
	}
	
	public function check_lm($data=array()){
		$this->db->select('id');
		$res=$this->db->get_where('res_users',$data);
		return $res;
	}

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }
	
	public function checkOldPassword($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					hr_user 
				WHERE 
					user_id=\''.$post['user_id'].'\' 
				AND 
					user_password=\''.md5($post['old']).'\'';
					
		$row = $this->db->query($sql)->row_array();
		if($row['total'] > 0){
			$result['status'] = 'OK';
			$result['message'] = '';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Invalid old password';
		}
		
		return $result;
	}
	
	public function saveNewPassword($post = array()){
		$result = array();
		
		$sql = 'UPDATE 
					hr_user 
				SET 
					user_password=\''.md5($post['new_password']).'\' 
				WHERE 
					user_id=\''.$post['user_id'].'\'';
					
		$q = $this->db->query($sql);
		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Change password success. Use new password in next login';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Change password failed';
		}
		
		return $result;
	}
	
}
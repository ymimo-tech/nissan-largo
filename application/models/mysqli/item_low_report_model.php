<?php
class item_low_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getListRaw($post = array()){
		$result = array();
		
		$where = '';
		
		if(!empty($post['id_barang']) && $post['id_barang'] != 'null')
			$where .= ' AND brg.id_barang IN('.$post['id_barang'].') '; 
		
		$sql = 'SELECT 
					kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					IFNULL(tbl.qty, 0) AS stock, min_reorder AS min, nama_satuan  
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, sum(last_qty)AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					where
						putaway_time > 0
                        AND (st_shipping = 0 or last_qty >1)
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					IFNULL(tbl.qty, 0) <= min_reorder'.$where.' 
				ORDER BY 
					nama_barang ASC';
			
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'nama_barang',
			2 => 'stock',
			3 => 'min'
		);
		
		$where = '';
		
		if(!empty($post['id_barang']))
			$where .= ' AND brg.id_barang IN('.implode(',', $post['id_barang']).') '; 
		
		$sql = 'SELECT 
					COUNT(*) AS total
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, sum(last_qty)AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					where
						putaway_time > 0
                        AND (st_shipping = 0 or last_qty >1)
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					IFNULL(tbl.qty, 0) <= min_reorder'.$where;
				
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					IFNULL(tbl.qty, 0) AS stock, min_reorder AS min, nama_satuan  
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, sum(last_qty)AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					where
						putaway_time > 0
                        AND (st_shipping = 0 or last_qty >1)
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					IFNULL(tbl.qty, 0) <= min_reorder'.$where;
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['stock'];
			$nested[] = $row[$i]['min'];
			$nested[] = $row[$i]['nama_satuan'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
}
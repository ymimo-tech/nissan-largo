<?php
class referensi_kitting_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'm_kitting';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);
        $this->db->join('barang','barang.id_barang=a.id_barang','LEFT');
        return $this->db;
    }

    public function get_all_categories() {
        return $this->db->get("kategori");
    }

    public function get_all_satuan() {
        return $this->db->get("satuan");
    }

    public function get_all_suppliers() {
        return $this->db->get("supplier");
    }

    public function get_by_id($id) {
        $condition['a.id_kitting'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_kitting = $this->input->post('kd_kitting');
        $nama_kitting = $this->input->post('nama_kitting');
        $kd_kategori = $this->input->post('kd_kategori');
        $kd_satuan = $this->input->post('kd_satuan');
        $tipe_kitting = $this->input->post('tipe_kitting');
        $shipment_type = $this->input->post('shipment_type');
        $fifo_period = $this->input->post('fifo_period');
        $owner_name = $this->input->post('owner_name');

        if(!empty($kd_kitting)){
            $condition["a.kd_kitting like '%$kd_kitting%'"]=null;
        }

        if(!empty($nama_kitting)){
            $condition["a.nama_kitting like '%$nama_kitting%'"]=null;
        }

        if(!empty($kd_kategori)){
            $condition["left join kategori on a.id_kategori = kategori.id_kategori"];
            $condition["a.kd_kategori like '%$kd_kategori%'"]=null;
        }

        if(!empty($kd_satuan)){
            $condition["left join satuan on a.id_satuan = satuan.id_satuan"];
            $condition["a.kd_satuan like '%$kd_satuan%'"]=null;
        }

        if(!empty($tipe_kitting)){
            $condition["a.tipe_kitting like '%$tipe_kitting%'"]=null;
        }

        if(!empty($shipment_type)){
            $condition["a.shipment_type like '%$shipment_type%'"]=null;
        }

        if(!empty($fifo_period)){
            $condition["a.fifo_period like '%$fifo_period%'"]=null;
        }

        if(!empty($owner_name)){
            $condition["left join owner on a.id_owner = owner.id_owner"];
            $condition["a.owner_name like '%$owner_name%'"]=null;
        }


        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.id_kitting');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_kitting;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
            $action .= '</li>';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_kitting' => $value->kd_kitting,
                    'nama_kitting' => $value->nama_kitting,
                    'alamat_kitting' => $value->alamat_kitting,
                    'telepon_kitting' => $value->telepon_kitting,
                    'cp_kitting' => $value->cp_kitting,
                    'email_kitting' => $value->email_kitting,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'kd_kitting' => $value->kd_kitting,
                    'nama_kitting' => $value->nama_kitting,
                    'alamat_kitting' => $value->alamat_kitting,
                    'telepon_kitting' => $value->telepon_kitting,
                    'cp_kitting' => $value->cp_kitting,
                    'email_kitting' => $value->email_kitting,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function create_barang($data) {
        $this->db->insert('barang', $data);
        return $this->db->insert_id();
    }

    public function insert_kitting_item($data=array()){
        $result = $this->db->insert('m_kitting_product', $data);
        return $result;
    }

    public function update_kitting_item($data=array(),$id=0){
        $result = $this->db->update('m_kitting_product', $data,array('id_kitting_product'=>$id));
        return $result;
    }

    public function delete_kitting_item($id_kitting_product=0){
        $result = $this->db->delete('m_kitting_product', array('id_kitting_product' => $id_kitting_product));
        return $result;
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_kitting' => $id));
    }

    public function update_barang($data, $id) {
        return $this->db->update('barang', $data, array('id_kitting' => $id));
    }

    public function get_data_kitting_product($id=0){
        $result = $this->db->get_where('m_kitting_product',array('id_kitting'=>$id));
        return $result;
    }

	public function getPickingStrategy(){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_shipment_type';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getPickingPeriod(){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_shipment_periode';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    public function delete($id) {
		$result = array();

		$sql = 'SELECT COUNT(*) as t1 FROM order_kitting_barang WHERE id_kitting = '.$id;

		$row = $this->db->query($sql)->row_array();
		if(($row['t1']) > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			$this->db->delete($this->table, array('id_kitting' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}


        return $result;
    }

    public function options($default = '--Pilih Kode Kitting--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_kitting] = $row->nama_kitting ;
        }
        return $options;
    }

	function create_options_code($table_name, $value_column, $caption_column) {
		if($table_name == 'm_shipment_type')
			$this->db->order_by('id_shipment_type', "ASC");
		else
			$this->db->order_by('id_periode', "ASC");

        $data = $this->db->get($table_name)->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column) {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    function create_options2($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }

    function create_options3($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->order_by($value_column, "ASC");
        $this->db->join('barang','barang.id_barang=m_kitting.id_barang','LEFT');
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }
}

?>

<?php
class packing_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

	public function getList(){
        $result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'pl_id',
			1 => '',
			2 => 'pl_name',
			3 => 'pl_date',
            4 => 'remark',
			5 => 'nama_status'
		);

		$where = '';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						pick.pl_id
					FROM
						picking_list pick
					LEFT JOIN m_status_picking mstat
						ON mstat.id_status_picking=pick.id_status_picking
					WHERE (pick.id_status_picking=3)'.$where.'
					GROUP BY
						pick.pl_id
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					pick.pl_id, pl_name, DATE_FORMAT(pl_date, "%d/%m/%Y") AS pl_date,
					IFNULL(remark, "-") AS remark,
					pick.id_status_picking,nama_status
				FROM
					picking_list pick
				LEFT JOIN m_status_picking mstat
					ON mstat.id_status_picking=pick.id_status_picking
				WHERE (pick.id_status_picking=3 or pick.id_status_picking=5)'.$where.'
				GROUP BY
					pick.pl_id';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<span class="pl_name">'.$row[$i]['pl_name'].'</span>';
			$nested[] = $row[$i]['pl_date'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['nama_status'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    public function getOutboundList($param=array()){
        $result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_outbound',
			1 => '',
			2 => 'kd_outbound',
			3 => 'DestinationName',
            4 => 'DestinationAddressL1',
		);

		$where = ' AND plo.pl_id='.$param['pl_id'];

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						otb.id_outbound
					FROM
						outbound otb
					JOIN picking_list_outbound plo
						ON otb.id_outbound=plo.id_outbound
					WHERE 1=1'.$where.'
					GROUP BY
						otb.id_outbound
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					otb.id_outbound, kd_outbound, DestinationName, DestinationAddressL1
				FROM
					outbound otb
                    JOIN picking_list_outbound plo
                        ON otb.id_outbound=plo.id_outbound
				WHERE 1=1'.$where.'
				GROUP BY
					otb.id_outbound';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id_outbound'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<span class="kd_outbound" data-id="'.$row[$i]['id_outbound'].'">'.$row[$i]['kd_outbound'].'</span>';
			$nested[] = '<span class="DestinationName">'.$row[$i]['DestinationName'].'</span>';
			$nested[] = '<span class="DestinationAddressL1">'.$row[$i]['DestinationAddressL1'].'</span>';

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    public function getRecList($param=array()){
        $result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'outbbrg.id',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
            4 => 'DestinationAddressL1',
		);

        $sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						outbbrg.id_barang
					FROM
						picking_list pl
					JOIN picking_list_outbound plo
						ON plo.pl_id=pl.pl_id
					JOIN outbound outb
						ON outb.id_outbound=plo.id_outbound
					LEFT JOIN outbound_barang outbbrg
						ON outbbrg.id_outbound=outb.id_outbound
        			LEFT JOIN barang brg
        				ON brg.id_barang=outbbrg.id_barang
					WHERE
						pl.pl_id='.$param['pl_id'].'
                        AND
                        outb.id_outbound='.$param['id_outbound'].'
					GROUP BY
						outbbrg.id_barang
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

        $sql = '
			SELECT
				outbbrg.id, pl.pl_id, outbbrg.id_barang, kd_barang, nama_barang, sum(jumlah_barang) as jumlah_barang,
				(
					SELECT
						COUNT(*)
					FROM
						receiving_barang
                    LEFT JOIN
                        outbound_receiving_barang
                        on receiving_barang.id_receiving_barang=outbound_receiving_barang.id_receiving_barang
					WHERE
						id_barang=plq.id_barang
					AND
						pl_id=\''.$param['pl_id'].'\'
                    AND
                        outbound_receiving_barang.id_outbound=\''.$param['id_outbound'].'\'
                    AND
						st_shipping=1
				) AS actual_picked_qty
			FROM
				picking_list pl
			JOIN picking_list_outbound plo
				ON plo.pl_id=pl.pl_id
			JOIN outbound outb
				ON outb.id_outbound=plo.id_outbound
			LEFT JOIN outbound_barang outbbrg
				ON outbbrg.id_outbound=outb.id_outbound
			LEFT JOIN picking_qty plq
				ON plq.id_barang=outbbrg.id_barang
					AND plq.pl_id=pl.pl_id
			LEFT JOIN barang brg
				ON brg.id_barang=outbbrg.id_barang
			WHERE
				pl.pl_id=\''.$param['pl_id'].'\'
                AND
                outb.id_outbound='.$param['id_outbound'].'
				AND plq.qty != 0
                AND plq.qty IS NOT NULL
			GROUP BY
				brg.id_barang';
		$sql = 'SELECT
					outbbrg.id,
					pl.pl_id,
					outbbrg.id_barang,
					kd_barang,
					nama_barang,
					sum(jumlah_barang)AS jumlah_barang,
					IFNULL(sum(t_obp.qty),0)AS actual_picked_qty
				FROM
					picking_list pl
				JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
				JOIN outbound outb ON outb.id_outbound = plo.id_outbound
				LEFT JOIN outbound_barang outbbrg ON outbbrg.id_outbound = outb.id_outbound
				LEFT JOIN picking_qty plq ON plq.id_barang = outbbrg.id_barang
				AND plq.pl_id = pl.pl_id
				LEFT JOIN barang brg ON brg.id_barang = outbbrg.id_barang
				LEFT JOIN(
					SELECT
						sum(qty) as qty,
						id_barang
					FROM
						outbound_barang_picked
					WHERE
						id_outbound = '.$param['id_outbound'].'
					GROUP BY
						id_barang
				) as t_obp on t_obp.id_barang = plq.id_barang
				WHERE
					pl.pl_id = \''.$param['pl_id'].'\'
				AND outb.id_outbound = '.$param['id_outbound'].'
				AND plq.qty != 0
				AND plq.qty IS NOT NULL
				GROUP BY
					brg.id_barang
				';
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
        $scanned = $totalFiltered>0?1:0;
        $packing_numbers='';
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_barang'];
			$nested[] = $row[$i]['actual_picked_qty'];
			$nested[] = $row[$i]['actual_picked_qty']>=$row[$i]['jumlah_barang']?'Scanned':'In Progress';

            if(!$row[$i]['actual_picked_qty']>=$row[$i]['jumlah_barang']){
                $scanned=0;
            }

			$data[] = $nested;
		}

        $this->load->library('enc');

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data,
            "scanned"         => $scanned,
            "enc"             => $this->enc->encode($param['id_outbound'])
		);

		return $result;
	}

    public function getOutboundPack($param=array()){
        $sql = 'SELECT
            otb.id_outbound,
            kd_outbound,
            DestinationName,
            DestinationAddressL1,
            DestinationAddressL2,
            DestinationAddressL3,
            nama_barang,
            kd_barang,
            packing_number,
            inc,
           	qty
        FROM outbound otb
            LEFT JOIN outbound_receiving_barang orb ON otb.id_outbound = orb.id_outbound
            LEFT JOIN receiving_barang rb ON orb.id_receiving_barang = rb.id_receiving_barang
            LEFT JOIN barang b ON rb.id_barang = b.id_barang
            LEFT JOIN picking_list_outbound plo ON plo.id_outbound=otb.id_outbound
			LEFT JOIN picking_qty pq ON pq.pl_id=plo.pl_id AND pq.id_barang=b.id_barang
            WHERE otb.id_outbound='.$param['id_outbound'];
        $result = $this->db->query($sql);
        return $result;
    }

    public function get_packing_number($id_outbound=0){
        //$sql = 'SELECT group_concat(concat(packing_number,"-",inc,"/x") separator ", ") as pc_string,packing_number,count(inc) as inc from outbound_receiving_barang where id_outbound='.$id_outbound.' group by packing_number';
        $sql = 'SELECT packing_number,max(inc) as inc from outbound_receiving_barang where id_outbound='.$id_outbound.' group by packing_number';
        $result = $this->db->query($sql);
        return $result;
    }

    public function get_pc_code(){
        $this->db->select('pre_code_pc,inc_pc');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_pc.date('ymd').str_pad($result->inc_pc,4,0,STR_PAD_LEFT);
    }

    public function add_pc_code(){
        $this->db->set('inc_pc',"inc_pc+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

    public function get_picking_id($pl_name=''){
        $result = $this->db->get_where('picking_list',array('pl_name'=>$pl_name));
        return $result;
    }

    public function check_kd_unik($kd_unik=''){
        $result = $this->db->get_where('picking_recomendation',array('kd_unik'=>$kd_unik));
        return $result->num_rows();
    }

    public function checkQty($id_outbound='', $id_picking='', $id_barang=''){
        $sql = 'SELECT
                    IFNULL(sum(jumlah_barang), 0) AS doc_qty,
                    IFNULL(sum(t_obp.qty),0) AS act_qty
                FROM
                    picking_list pl
                JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
                JOIN outbound outb ON outb.id_outbound = plo.id_outbound
                LEFT JOIN outbound_barang outbbrg ON outbbrg.id_outbound = outb.id_outbound
                LEFT JOIN picking_qty plq ON plq.id_barang = outbbrg.id_barang
                AND plq.pl_id = pl.pl_id
                LEFT JOIN barang brg ON brg.id_barang = outbbrg.id_barang
                LEFT JOIN(
                    SELECT
                        sum(qty) as qty,
                        id_barang
                    FROM
                        outbound_barang_picked opb
                    WHERE
                        id_outbound = \''.$id_outbound.'\'
                    AND 
                        id_barang = \''.$id_barang.'\'
                    GROUP BY
                        id_barang
                ) as t_obp on t_obp.id_barang = plq.id_barang
                WHERE
                    pl.pl_id = \''.$id_picking.'\'
                AND outb.id_outbound = \''.$id_outbound.'\'
                AND plq.qty != 0
                AND plq.qty IS NOT NULL
                AND 
                    brg.id_barang = \''.$id_barang.'\'
                GROUP BY
                    brg.id_barang
                ';

        $res = $this->db->query($sql)->row_array();

        if($res['doc_qty'] > $res['act_qty']){
            return true;
        }

        return false;
    }

    public function get_avail_qty($kd_unik='',$id_picking=''){
    	//=========get maks available qty from scaned picking=======
    	$this->db->select('(qty-picked_qty) as avail_qty');
    	$this->db->from('picking_recomendation');
    	$this->db->where('id_picking',$id_picking);
    	$this->db->where('kd_unik',$kd_unik);
    	$hasil = $this->db->get();
    	if($hasil->num_rows()>0){
    		$avail_qty = $hasil->row()->avail_qty;
    		return $avail_qty;
    	}else{
    		return 0;
    	}
    	
    	//-------------------end get maks------------------------------
    }
    public function update_location_outbound($kd_unik){
    	$data_where = array(
    		'kd_unik'=>$kd_unik,
    		'first_qty' => 1);
    	$data_update = array('loc_id'=>'101');
    	$this->db->where($data_where);
    	$this->db->update('receiving_barang',$data_update);
    }

    public function update_picking_recomendation($kd_unik='',$id_outbound='',$id_picking=''){
    	$this->db->select('jumlah_barang,rb.id_barang');
    	$this->db->from('outbound_barang ob');
    	$this->db->join('receiving_barang rb','rb.id_barang = ob.id_barang','left');
    	$this->db->where('id_outbound',$id_outbound);
    	$this->db->where('rb.kd_unik',$kd_unik);
    	$hasil = $this->db->get()->row();
    	$jumlah_barang = $hasil->jumlah_barang;
    	$id_barang = $hasil->id_barang;
    	//=========get maks available qty from scaned picking=======
    	$avail_qty = $this->get_avail_qty($kd_unik,$id_picking);
    	//-------------------end get maks------------------------------
    	if($avail_qty >= $jumlah_barang){
    		$qty_update = $jumlah_barang;
    	}else{
    		$qty_update = $avail_qty;
    	}

    	$sql = "UPDATE `picking_recomendation`
				SET `st_packing` = 1,
				 `picked_qty` = picked_qty + ".$qty_update." 
				WHERE
					`kd_unik` = '".$kd_unik."'
				AND `id_picking` = '".$id_picking."'";
		$result = $this->db->query($sql);
		$insert_data = array(
			'id_outbound' => $id_outbound,
			'id_barang' => $id_barang,
			'kd_unik'=> $kd_unik,
			'qty'=>$qty_update);
		$result = $this->db->insert('outbound_barang_picked',$insert_data);
        //$result = $this->db->update('picking_recomendation',array('st_packinga'=>1,'picked_qty = picked_qty + '.$jumlah_barang =>NULL),array('kd_unik'=>$kd_unik,'id_picking'=>$id_picking));
        return $result;
    }

    public function update_receiving_barang($kd_unik=''){
        $result = $this->db->update('receiving_barang',array('st_shipping'=>1),array('kd_unik'=>$kd_unik));
        return $result;
    }

    public function get_receiving_barang($kd_unik=''){
        $result = $this->db->get_where('receiving_barang',array('kd_unik'=>$kd_unik));
        return ($result->num_rows()>0?$result->row()->id_receiving_barang:0);
    }

    public function get_picking($id_outbound){
    	$result = $this->db->get_where('picking_list_outbound',array('id_outbound' =>$id_outbound));
    	return $result->row()->pl_id;
    }

    public function insert_outbound_receiving($data=array()){
        $result = $this->db->insert('outbound_receiving_barang',$data);
        return $result;
    }

    public function get_qty_print($data){
    	$sql = "select IFNULL(max(inc),0) as qty_print from outbound_receiving_barang where id_outbound = ".$data['id_outbound'];
    	$query = $this->db->query($sql);
    	return $query->row()->qty_print;
    }

    public function get_data_label($data){
    	$sql = "SELECT
					o.*, orb_temp.packing_number
				FROM
					outbound o
				LEFT JOIN(
					SELECT
						packing_number,
						id_outbound
					FROM
						outbound_receiving_barang orb
					WHERE
						id_outbound = ".$data['id_outbound']."
					GROUP BY
						id_outbound
				)orb_temp ON orb_temp.id_outbound = o.id_outbound
				WHERE
					o.id_outbound = ".$data['id_outbound'];
		$query = $this->db->query($sql);
		return $query->row();

    }

    public function update_staging_outbound(){
    	$this->load->model('api_staging_model');
    	$sql = "
    			SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					count(kd_unik)AS qty,
					mx_inc
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						id_outbound
					FROM
						outbound_receiving_barang orb
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				GROUP BY
					kd_outbound,
					kd_barang,
					inc";
		$sql = "
				SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					mx_inc,
					sum(pr.qty) as qty,
					pl_finish
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				left join picking_list_outbound plo on plo.id_outbound = o.id_outbound
				LEFT JOIN picking_list pl ON pl.pl_id=plo.pl_id 
				left join picking_recomendation pr on pr.kd_unik = rb.kd_unik and pr.id_picking = plo.pl_id
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						id_outbound
					FROM
						outbound_receiving_barang orb
					
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				
				GROUP BY
					kd_outbound,
					kd_barang,
					inc,
					o.id_staging";
		$query = $this->db->query($sql);
		$line=0;
		foreach ($query->result() as $row) {
			$line++;
			if($row->inc<10){
                $pad=0;
            }else{
                $pad='';
            }
            if($row->mx_inc<10){
                $pad2=0;
            }else{
                $pad2='';
            }
			$PackingNo = $row->packing_number.$pad.$row->inc.$pad2.$row->mx_inc;

			/*$this->fina->from('f_t_outbound_h');
			$this->fina->where('OutbondNo',$row->kd_outbound);
			$hasil = $this->fina->get();*/

			$data_where = array(
                'OutbondNo' => $row->kd_outbound);
            $hasil = $this->api_staging_model->_curl_process('get','f_t_outbound_h',array('where'=>$data_where));

			
			if($hasil->status=='001'){
				foreach ($hasil->data as $hasil_t) {
					# code...
					$DocEntry = $hasil_t->DocEntry;
				}
				
				
				/*$this->fina->from('f_t_outbound_l2');
				$this->fina->where($data_where);
				$cek_PackingNo = $this->fina->get();*/

				$data_where = array(
					'PackingNo' => $PackingNo,
					'SKU' => $row->kd_barang);
	            $cek_PackingNo = $this->api_staging_model->_curl_process('get','f_t_outbound_l2',array('where'=>$data_where));
				if($cek_PackingNo ->status == '001'){
					//$data_update = $cek_PackingNo->row_array();
					foreach ($cek_PackingNo->data as $data_t2) {
						# code...
					
						if($row->qty <> $data_t2->PackingQty){
							$data_update['PackingQty'] = $row->qty;
							$data_update['SyncStatus'] = 'N';
							/*$this->fina->where('PackingNo',$PackingNo);
							$this->fina->update('f_t_outbound_l2',$data_update);*/
							$data_where = array('PackingNo'=>$PackingNo);
							$this->api_staging_model->_curl_process('update','f_t_outbound_l2',array('where'=>$data_where,'data'=>$data_update));
						}
					}
				}else{
					$data_insert = array(
						'DocEntry' => $DocEntry,
						'LineNum'=>$line,
						'SKU'=>$row->kd_barang,
						'PackingQty' =>$row->qty,
						'PackingNo' => $PackingNo,
						'SyncStatus' => 'N',
						'ShippingDate' => $row->pl_finish
					);
					//$this->fina->insert('f_t_outbound_l2',$data_insert);
					$this->api_staging_model->_curl_process('insert','f_t_outbound_l2',array('data'=>$data_insert));

					$data_where = array(
		                'SKU' => $row->kd_barang);
		            //$cek_val = $this->fina->get_where('f_v_stok_h',$data_where);

	            	$cek_val = $this->api_staging_model->_curl_process('get','f_v_stok_h',array('where'=>$data_where));
		            if($cek_val->status=='001'){
		            	foreach ($cek_val->data as $data_cek) {
		            		# code...
		            	
			                $qty = $data_cek->Qty;
			                $new_qty = $qty-$row->qty;
			                $data_update = array(
			                    'Qty' => $new_qty,
			                    'SyncStatus'=> 'N');
			                /*$this->fina->where($data_where);
			                $this->fina->update('f_v_stok_h',$data_update);*/
			                $this->api_staging_model->_curl_process('update','f_v_stok_h',array('where'=>$data_where,'data'=>$data_update));
			            }
		            }else{
		                $data_insert = array(
		                    'SKU'=>$row->kd_barang,
		                    'Qty'=>$row->qty,
		                    'SyncStatus'=> 'N'
		                    );
		                //$this->fina->insert('f_v_stok_h',$data_insert);
		                $this->api_staging_model->_curl_process('insert','f_v_stok_h',array('data'=>$data_insert));
		            }
				}
			}
			# code...
		}
    }

    public function update_staging_outbound_by_id_outbound($id_outbound){
    	$this->load->model('api_staging_model');
    	/*$sql = "
    			SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					count(kd_unik)AS qty,
					mx_inc,
					o.id_staging
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						id_outbound
					FROM
						outbound_receiving_barang orb
					WHERE 
						orb.id_outbound = ".$id_outbound."
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				WHERE
					orb.id_outbound = ".$id_outbound."
				GROUP BY
					kd_outbound,
					kd_barang,
					inc,
					o.id_staging";*/

		$sql ="
		 		SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					mx_inc,
					o.id_staging,
					sum(pr.qty) as qty,
					pl_finish
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				left join picking_list_outbound plo on plo.id_outbound = o.id_outbound
				LEFT JOIN picking_list pl ON pl.pl_id=plo.pl_id 
				left join picking_recomendation pr on pr.kd_unik = rb.kd_unik and pr.id_picking = plo.pl_id
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						id_outbound
					FROM
						outbound_receiving_barang orb
					WHERE
						orb.id_outbound = ".$id_outbound."
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				WHERE
					orb.id_outbound = ".$id_outbound."
				GROUP BY
					kd_outbound,
					kd_barang,
					inc,
					o.id_staging";
		$query = $this->db->query($sql);
		$line=0;
		foreach ($query->result() as $row) {
			$line++;
			if($row->inc<10){
                $pad=0;
            }else{
                $pad='';
            }
            if($row->mx_inc<10){
                $pad2=0;
            }else{
                $pad2='';
            }
			$PackingNo = $row->packing_number.$pad.$row->inc.$pad2.$row->mx_inc;

			$DocEntry = $row->id_staging;
			
			$data_where = array(
				'PackingNo' => $PackingNo,
				'SKU' => $row->kd_barang);
            $cek_PackingNo = $this->api_staging_model->_curl_process('get','f_t_outbound_l2',array('where'=>$data_where));
			if($cek_PackingNo ->status == '001'){
				foreach ($cek_PackingNo->data as $data_t2) {
					# code...
				
					if($row->qty <> $data_t2->PackingQty){
						$data_update['PackingQty'] = $row->qty;
						$data_update['SyncStatus'] = 'N';
						$data_where = array('PackingNo'=>$PackingNo);
						$this->api_staging_model->_curl_process('update','f_t_outbound_l2',array('where'=>$data_where,'data'=>$data_update));
					}
				}
			}else{
				$data_insert = array(
					'DocEntry' => $DocEntry,
					'LineNum'=>$line,
					'SKU'=>$row->kd_barang,
					'PackingQty' =>$row->qty,
					'PackingNo' => $PackingNo,
					'SyncStatus' => 'N',
					'ShippingDate' => $row->pl_finish
				);
				$this->api_staging_model->_curl_process('insert','f_t_outbound_l2',array('data'=>$data_insert));

				$data_where = array(
	                'SKU' => $row->kd_barang,
	                'WhsCode' => 'KRG');
				$cek_val = $this->db->get_where('staging_stok_h',$data_where);
	            if($cek_val->num_rows > 0){
	            	$data_cek = $cek_val->row();
	                $qty = $data_cek->Qty;
	                $new_qty = $qty-$row->qty;
	                $data_update = array(
	                    'Qty' => $new_qty,
	                    'SyncStatus'=> 'N');
	                $this->db->where($data_where);
	                $this->db->update('staging_stok_h',$data_update);
	                $this->api_staging_model->_curl_process('update','f_v_stok_h',array('where'=>$data_where,'data'=>$data_update));
		            
	            }else{
	                $data_insert = array(
	                    'SKU'=>$row->kd_barang,
	                    'Qty'=>$row->qty,
	                    'SyncStatus'=> 'N',
	                    'WhsCode' => 'KRG'
	                    );
	                $this->db->insert('staging_stok_h',$data_insert);
	                $this->api_staging_model->_curl_process('insert','f_v_stok_h',array('data'=>$data_insert));
	            }
			}
		}
    }

    public function check_pc_code($post=array()){

    	$this->db
    		->select('count(*) as total')
    		->from('outbound_receiving_barang')
    		->where('id_outbound', $post['id_outbound'])
    		->where('packing_number', $post['pc_code'])
    		->where('inc', $post['inc']);

    	$row = $this->db->get()->row_array();

    	return $row;
    }

    public function update_staging_outbound_by_id_outbound_min(){
    	$this->load->model('api_staging_model');
    	

		$sql ="
		 		SELECT
					packing_number,
					inc,
					kd_barang,
					o.kd_outbound,
					mx_inc,
					o.id_staging,
					sum(pr.qty) as qty,
					pl_finish
				FROM
					outbound_receiving_barang orb
				LEFT JOIN receiving_barang rb ON rb.id_receiving_barang = orb.id_receiving_barang
				LEFT JOIN barang b ON b.id_barang = rb.id_barang
				LEFT JOIN outbound o ON o.id_outbound = orb.id_outbound
				left join picking_list_outbound plo on plo.id_outbound = o.id_outbound
				LEFT JOIN picking_list pl ON pl.pl_id=plo.pl_id 
				left join picking_recomendation pr on pr.kd_unik = rb.kd_unik and pr.id_picking = plo.pl_id
				LEFT JOIN(
					SELECT
						max(inc)AS mx_inc,
						o.id_outbound
					FROM
						outbound_receiving_barang orb
					left join outbound o on o.id_outbound = orb.id_outbound
					WHERE
						o.id_staging > 1221
					GROUP BY
						id_outbound
				)AS t_orb ON t_orb.id_outbound = orb.id_outbound
				WHERE
					o.id_staging > 1221
				GROUP BY
					kd_outbound,
					kd_barang,
					inc,
					o.id_staging";
		$query = $this->db->query($sql);
		$line=0;
		foreach ($query->result() as $row) {
			$line++;
			if($row->inc<10){
                $pad=0;
            }else{
                $pad='';
            }
            if($row->mx_inc<10){
                $pad2=0;
            }else{
                $pad2='';
            }
			$PackingNo = $row->packing_number.$pad.$row->inc.$pad2.$row->mx_inc;

			$DocEntry = $row->id_staging;


			
			$data_where = array(
				'PackingNo' => $PackingNo,
				'SKU' => $row->kd_barang);
            $cek_PackingNo = $this->api_staging_model->_curl_process('get','f_t_outbound_l2',array('where'=>$data_where));
			if($cek_PackingNo ->status == '001'){
				foreach ($cek_PackingNo->data as $data_t2) {
					# code...
				
					if($row->qty <> $data_t2->PackingQty){
						$data_update['PackingQty'] = $row->qty;
						$data_update['SyncStatus'] = 'N';
						$data_where = array('PackingNo'=>$PackingNo);
						$this->api_staging_model->_curl_process('update','f_t_outbound_l2',array('where'=>$data_where,'data'=>$data_update));
						echo 'Update - '.$DocEntry.'<br/>';
						echo 'No Packing - '.$PackingNo.'<br/>';
					}
				}
			}else{
				$data_insert = array(
					'DocEntry' => $DocEntry,
					'LineNum'=>$line,
					'SKU'=>$row->kd_barang,
					'PackingQty' =>$row->qty,
					'PackingNo' => $PackingNo,
					'SyncStatus' => 'N',
					'ShippingDate' => $row->pl_finish
				);
				
				$this->api_staging_model->_curl_process('insert','f_t_outbound_l2',array('data'=>$data_insert));
				echo 'insert - '.$DocEntry.'<br/>';
				echo 'No Packing - '.$PackingNo.'<br/>';
				
				$data_where = array(
	                'SKU' => $row->kd_barang,
	                'WhsCode' => 'KRG');
				$cek_val = $this->db->get_where('staging_stok_h',$data_where);
	            if($cek_val->num_rows > 0){
	            	$data_cek = $cek_val->row();
	                $qty = $data_cek->Qty;
	                $new_qty = $qty-$row->qty;
	                $data_update = array(
	                    'Qty' => $new_qty,
	                    'SyncStatus'=> 'N');
	                $this->db->where($data_where);
	                $this->db->update('staging_stok_h',$data_update);
	                $this->api_staging_model->_curl_process('update','f_v_stok_h',array('where'=>$data_where,'data'=>$data_update));
		            
	            }else{
	                $data_insert = array(
	                    'SKU'=>$row->kd_barang,
	                    'Qty'=>$row->qty,
	                    'SyncStatus'=> 'N',
	                    'WhsCode' => 'KRG'
	                    );
	                $this->db->insert('staging_stok_h',$data_insert);
	                $this->api_staging_model->_curl_process('insert','f_v_stok_h',array('data'=>$data_insert));
	            }
			}

			echo $DocEntry.'<br/>';
			echo $PackingNo.'<br/>';
		}
    }
}

?>

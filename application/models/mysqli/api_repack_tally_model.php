<?php

class Api_repack_tally_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

/*    function loadDocument(){
    	$q = 	"SELECT 
					a.*,
					(SELECT 
						SUM(jumlah_kitting)
					FROM
						order_kitting_barang b
					WHERE
						b.id_order_kitting = a.id_order_kitting
					GROUP BY 
						b.id_order_kitting) AS total
				FROM 
					order_kitting a
				WHERE 
					a.status_order_kitting IS NOT NULL
						OR
					a.status_order_kitting = 1
				";
		$r = $this->db->query($q, false)->result_array();
		
		return $r;
    }
*/	
/*	function loadDetail($data = array()){
		$wh = "";
		for($i = 0; $i < count($data); $i++){
			$wh .= "'". $data[$i]['id_order_kitting'] ."',";
		}
		$wh = rtrim($wh, ", ");
				
		$q =	"SELECT
					outbg.id,
					outbg.id_order_kitting,
					outbg.id_kitting,
					mkp.id_barang,
					kd_barang,
					nama_barang,
					outbg.jumlah_kitting AS jumlah_kitting
				FROM
					order_kitting outb
				JOIN order_kitting_barang outbg ON outbg.id_order_kitting = outb.id_order_kitting
				JOIN m_kitting mk ON mk.id_kitting = outbg.id_kitting
				JOIN m_kitting_product mkp ON mkp.id_kitting = mk.id_kitting
				JOIN barang brg ON brg.id_barang = mkp.id_barang
				WHERE 
					outbg.id_order_kitting in (". $wh .")
				";
		$r = $this->db->query($q, false)->result_array();
		
		return $r;
    }
*/	

	function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
		return $this->db->get();
	}

    function loadDocument(){
    	$q = 	"SELECT 
					a.*,
					sum(jumlah_kitting) as total
				FROM 
					order_kitting a
				LEFT JOIN order_kitting_barang b
					ON a.id_order_kitting=b.id_order_kitting
				WHERE 
					a.st_repack = 0
						AND
					(a.status_order_kitting IS NOT NULL
						OR
					a.status_order_kitting <> 3)
				GROUP BY 
					a.id_order_kitting
				";
	
		$r = $this->db->query($q, false)->result_array();
		
		return $r;
    }

	function loadDetail($data=array()){

		$id_order_kitting = array();

		foreach ($data as $d) {
			$id_order_kitting[] = $d['id_order_kitting'];
		}

		$id_order_kitting = implode(',', $id_order_kitting);

		$sql = "

			SELECT * FROM (
				(

					SELECT 
						a.id_order_kitting, b.id_kitting, d.id_barang, d.kd_barang, d.nama_barang, sum(jumlah_kitting) as jumlah_kitting,
						COALESCE(
							(
								SELECT
									sum(first_qty)
								FROM
									receiving_barang
								WHERE
									id_order_kitting=a.id_order_kitting
								AND
									id_barang=d.id_barang
							)
						,0) as jumlah_tally
					FROM 
						order_kitting a
					LEFT JOIN 
						order_kitting_barang b
						ON b.id_order_kitting=a.id_order_kitting
					LEFT JOIN 
						m_kitting c
						ON c.id_kitting=b.id_kitting
					LEFT JOIN
						barang d
						ON d.id_barang=c.id_barang
					WHERE a.id_order_kitting IN ($id_order_kitting)
					GROUP BY id_barang, id_order_kitting
				)
				UNION
				(
					SELECT 
						a.id_order_kitting, b.id_kitting, d.id_barang, d.kd_barang, d.nama_barang, (sum(jumlah_kitting) * qty) as jumlah_kitting,
						COALESCE(
							(
								SELECT
									sum(first_qty)
								FROM
									receiving_barang
								WHERE
									id_order_kitting=a.id_order_kitting
								AND
									id_barang=d.id_barang
							)
						,0) as jumlah_tally
					FROM 
						order_kitting a
					LEFT JOIN 
						order_kitting_barang b
						ON b.id_order_kitting=a.id_order_kitting
					LEFT JOIN 
						m_kitting_product c
						ON c.id_kitting=b.id_kitting
					LEFT JOIN
						barang d
						ON d.id_barang=c.id_barang
					WHERE a.id_order_kitting IN ($id_order_kitting)
					GROUP BY id_barang, id_order_kitting
				)
			) AS tbl
			ORDER BY id_order_kitting
		";

		$r = $this->db->query($sql, false);

		return $r->result_array();

	}

	function validSN($data = array()){
		$r = $this->db
			->where('kd_unik', $data['kd_unik'])
			->or_where('kd_parent', $data['kd_unik'])
			->get('receiving_barang')
			->num_rows();

		return $r;
    }
	
    function validQty($data=array()){
    	return true;
    }

	function postSN($data = array()){

		$barang = $this->get_id("id_barang","kd_barang",$data['kd_barang'],"barang")->row_array();
		$qc = $this->get_id("id_qc","kd_qc",$data['kd_qc'],"m_qc")->row_array();
		$user = $this->get_id("user_id","user_name",$data['uname'],"hr_user")->row_array();

        $data_insert    = array(
                                    "kd_parent" => $data["kd_parent"],
                                    "id_barang" => $barang['id_barang'], 
                                    "kd_batch" => $data["kd_batch"],
                                    "loc_id" => 100,
                                    "tgl_exp" => $data["tgl_exp"],
                                    "tgl_in" => $data["tgl_in"],
                                    "st_receive" => 1,
                                    "first_qty" => $data["qty"],
                                    "last_qty" => $data["qty"],
                                    "id_qc" => $qc["id_qc"],
                                    "kd_unik" => $data['kd_unik'],
                                    "kd_parent" => $data['kd_parent'],
                                    "user_id_kitting" => $user["user_id"],
                                    "id_order_kitting" => $data["id_order_kitting"]
                                    );
        
        $this->db->insert("receiving_barang", $data_insert);
		
		$data_masukan	= array(
									"kd_unik"		=> $data["kd_unik"],
									"loc_id_new"	=> 100,
									"loc_id_old"	=> 100,
									"pick_time"		=> $data["tgl_in"],
									"put_time"		=> $data["tgl_in"],
									"user_id_pick"	=> $user["user_id"],
									"user_id_put"	=> $user["user_id"],
									"process_name"	=> "KITTING"
									);
        $this->db->insert("int_transfer_detail", $data_masukan);

    }

    function get_item($kd_barang=''){
    	$this->db->select("kd_barang,has_batch,has_expdate,has_qty,def_qty, kd_satuan")
    		->from('barang b')
    		->join('satuan s','s.id_satuan=b.id_satuan','left')
    		->where('kd_barang', $kd_barang);

    	return $this->db->get();
    }


}
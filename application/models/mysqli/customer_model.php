<?php 
	
class customer_model extends CI_model{

	private $table = "m_customer";

	public function __construct(){
		parent::__construct();
	}
	
    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_customer'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

	public function getCustomer(){
		$result = array();
		
		$sql = 'SELECT 
					customer_code,customer_name,address,
					city,postal_code,country,phone,fax,contact_person 
				FROM 
					m_customer 
				ORDER BY 
					CAST(SUBSTRING_INDEX(customer_code, \'-\', -1) AS UNSIGNED) ASC';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function delete($id){
		$result = array();
		
		$sql = 'SELECT 	
					COUNT(*) AS total 
				FROM 
					inbound 
				WHERE 
					id_supplier=\''.$id.'\' 
				AND 
					id_inbound_document = \'2\'';
					
		$row = $this->db->query($sql)->row_array();
		
		$sql = 'SELECT 	
					COUNT(*) AS total 
				FROM 
					outbound 
				WHERE 
					id_customer=\''.$id.'\' 
				AND 
					id_outbound_document != \'2\'';
					
		$r = $this->db->query($sql)->row_array();
		
		$total = $row['total'] + $r['total'];
		
		if($total > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete data because this data have relation to another table';
		}else{
			
			$sql = 'DELETE FROM m_customer WHERE id_customer = \''.$id.'\'';			
			
			$this->db->query($sql);
			
			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';
		}
		
		return $result;
	}
	
	public function getEdit($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					id_customer,customer_code,customer_name,address,
					city,postal_code,country,phone,fax,contact_person 
				FROM 
					m_customer 
				WHERE 
					id_customer=\''.$post['customer_id'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		
		if($row){
			$result['status'] = 'OK';
			$result['data'] = $row;
			$result['message'] = '';
		}else{
			$result['status'] = 'ERR';
			$result['data'] = array();
			$result['message'] = 'Get edit data failed';
		}
		
		return $result;
	}
	
	public function save($post = array()){
		$result = array();
		
		if(empty($post['customer_id'])){
			
			$sql = 'INSERT INTO m_customer(
						customer_code,
						customer_name, 
						address,
						city,
						postal_code,
						country,
						phone,
						fax,
						contact_person
					)VALUES(
						\''.$post['customer_code'].'\',
						\''.$post['customer_name'].'\',
						\''.$post['customer_address'].'\',
						\''.$post['customer_city'].'\',
						\''.$post['customer_postcode'].'\',
						\''.$post['customer_country'].'\',
						\''.$post['customer_phone'].'\',
						\''.$post['customer_fax'].'\',
						\''.$post['customer_cp'].'\'
					)';
			
			$q = $this->db->query($sql);
			
		}else{
			
			$sql = 'UPDATE 
						m_customer 
					SET 
						customer_code=\''.$post['customer_code'].'\',
						customer_name=\''.$post['customer_name'].'\', 
						address=\''.$post['customer_address'].'\',
						city=\''.$post['customer_city'].'\',
						postal_code=\''.$post['customer_postcode'].'\',
						country=\''.$post['customer_country'].'\',
						phone=\''.$post['customer_phone'].'\',
						fax=\''.$post['customer_fax'].'\',
						contact_person=\''.$post['customer_cp'].'\' 
					WHERE 
						id_customer=\''.$post['customer_id'].'\'';
						
			$q = $this->db->query($sql);
			
		}
		
		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Save success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Save failed';
		}
		
		return $result;
	}
	
	public function checkCustomerCode($post = array()){
		$result = array();
		
		if(isset($post['customer_id'])){
			if(!empty($post['customer_id'])){
				$sql = 'SELECT 
							COUNT(*) AS total 
						FROM 
							m_customer 
						WHERE 
							id_customer!=\''.$post['customer_id'].'\' 
						AND 
							LOWER(customer_code)=\''.strtolower($post['customer_code']).'\'';
			}else{
				$sql = 'SELECT 
							COUNT(*) AS total 
						FROM 
							m_customer 
						WHERE 
							LOWER(customer_code)=\''.strtolower($post['customer_code']).'\'';
			}
		}else{
			$sql = 'SELECT 
						COUNT(*) AS total 
					FROM 
						m_customer 
					WHERE 
						LOWER(customer_code)=\''.strtolower($post['customer_code']).'\'';
		}
		
		$row = $this->db->query($sql)->row_array();
		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Customer code already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'customer_code',
			2 => 'customer_name',
			3 => 'phone'
		);
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					m_customer';
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					id_customer, customer_code, customer_name, phone  
				FROM 
					m_customer';
		
		$order = $columns[$requestData['order'][0]['column']];
		if($order == 'customer_code')
			$order = ' CAST(SUBSTRING_INDEX(customer_code, \'-\', -1) AS UNSIGNED) ';
		
		$sql .=" ORDER BY ". $order ."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i = 0; $i < $totalFiltered; $i++){
			
			$action = '';
			
			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';
			
			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit" data-id="'.$row[$i]['id_customer'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete" data-id="'.$row[$i]['id_customer'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }
			
			$action .= '</ul>';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['customer_code'];
			$nested[] = $row[$i]['customer_name'];
			$nested[] = $row[$i]['phone'];
			$nested[] = $action;
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
}
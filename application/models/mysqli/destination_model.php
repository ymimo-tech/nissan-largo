<?php
class destination_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'm_gudang';
    private $table2 = 'm_outbound_document_m_gudang';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function getById($id){
        $condition['idGudang'] = $id;
        $this->data($condition);

        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function get_doc_gudang($id){

        $this->db
            ->select('id_m_outbound_document')
            ->from($this->table2)
            ->where('id_m_gudang',$id);

        $data = $this->db->get()->result_array();

        $results = array();
        foreach ($data as $d) {
            $results[]  = $d['id_m_outbound_document'];
        }

        return $results;
    }

    public function create($data,$data2) { 

        $this->db->insert($this->table, $data);

        $id = $this->db->insert_id();

        if(!empty($data2)){
            $data_insert = array();
            foreach($data2 as $idDoc){
                $data_insert[] = array(
                    'id_m_outbound_document' => $idDoc,
                    'id_m_gudang' => $id
                );
            }
        }

        $this->db->insert_batch($this->table2, $data_insert);

    }

    public function update($id, $data, $data2) {

        $this->db->update($this->table, $data, array('idGudang' => $id));

        $this->db->delete($this->table2,array('id_m_gudang'=>$id));

        if(!empty($data2)){
            $data_insert = array();
            foreach($data2 as $idDoc){
                $data_insert[] = array(
                    'id_m_outbound_document' => $idDoc,
                    'id_m_gudang' => $id
                );
            }
            $this->db->insert_batch($this->table2,$data_insert);
        }

    }

    public function delete($id) {
        //return $this->db->delete($this->table, array('id_outbound_document' => $id));
        
        $result = array();
        
        $sql = 'SELECT 
                    COUNT(*) AS t 
                FROM 
                    outbound 
                WHERE 
                    id_destination=\''.$id.'\'';
                    
        $row = $this->db->query($sql)->row_array();

        if($row['t'] > 0){

            $result['status'] = 'ERR';
            $result['message'] = 'Cannot delete data because this data have relation to another table';

        }else{

            $this->db->delete($this->table,array('idGudang'=>$id));
            $this->db->delete($this->table2,array('id_m_gudang'=>$id));
            
            $result['status'] = 'OK';
            $result['message'] = 'Delete data success';
        }
            
        return $result;
    }

    public function getDestination($post=array()){

        $results = array();

        $this->db
            ->select('idGudang, namaGudang')
            ->from($this->table2.' a')
            ->join($this->table.' b',' b.idGudang=a.id_m_gudang')
            ->where('a.id_m_outbound_document', $post['id']);

        $data = $this->db->get()->result_array();

        if($data){
            $results['data'] = $data;
            $results['status'] = 'OK';
        }else{
            $results['status'] = 'ERR';
            $results['message'] = 'Destination Not Available';
        }

        return $results;
    }

}

?>
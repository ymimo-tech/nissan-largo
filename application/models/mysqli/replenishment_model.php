<?php
class replenishment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

	public function getListRaw($post = array()){
		$result = array();

		$where = '';

		if(!empty($post['id_barang']) && $post['id_barang'] != 'null')
			$where .= ' AND b.id_barang IN('.$post['id_barang'].') ';

		$sql = 'SELECT
                	b.kd_barang,
                	b.nama_barang,
                	minimum_stock,
                	tpick.qty AS qty_picking,
                	tput.qty AS qty_putaway
                FROM
                	barang b
                LEFT JOIN(
                	SELECT
                		rb.id_barang,
                		t.loc_type_name,
                		t.loc_type_id,
                		sum(last_qty)AS qty
                	FROM
                		receiving_barang rb
                	LEFT JOIN m_loc l ON l.loc_id = rb.loc_id
                	LEFT JOIN m_loc_type t ON t.loc_type_id = l.loc_type_id
                	WHERE
                		(st_shipping = 0 OR last_qty > 1)
                	AND l.loc_type_id = 1
                	GROUP BY
                		id_barang,
                		loc_type_id
                	ORDER BY
                		l.loc_type_id,
                		rb.id_barang
                )AS tput ON tput.id_barang = b.id_barang
                LEFT JOIN(
                	SELECT
                		rb.id_barang,
                		t.loc_type_name,
                		t.loc_type_id,
                		sum(last_qty)AS qty
                	FROM
                		receiving_barang rb
                	LEFT JOIN m_loc l ON l.loc_id = rb.loc_id
                	LEFT JOIN m_loc_type t ON t.loc_type_id = l.loc_type_id
                	WHERE
                		(st_shipping = 0 OR last_qty > 1)
                	AND(
                		l.loc_type_id = 2
                		OR l.loc_type_id = 3
                	)
                	GROUP BY
                		id_barang,
                		loc_type_id
                	ORDER BY
                		l.loc_type_id,
                		rb.id_barang
                )AS tpick ON tpick.id_barang = b.id_barang
                WHERE
                	tpick.qty < minimum_stock'.$where.'
				ORDER BY
					nama_barang ASC';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'minimum_stock',
			4 => 'qty_picking',
            5 => 'qty_putaway',
            6 => '(qty_picking + qty_putaway)' //total????
		);

		$where = '';

		if(!empty($post['id_barang']))
			$where .= ' AND b.id_barang IN('.implode(',', $post['id_barang']).') ';

		$sql = 'SELECT
                	COUNT(*) AS total
                FROM
                	barang b
                LEFT JOIN(
                	SELECT
                		rb.id_barang,
                		t.loc_type_name,
                		t.loc_type_id,
                		sum(last_qty)AS qty
                	FROM
                		receiving_barang rb
                	LEFT JOIN m_loc l ON l.loc_id = rb.loc_id
                	LEFT JOIN m_loc_type t ON t.loc_type_id = l.loc_type_id
                	WHERE
                		(st_shipping = 0 OR last_qty > 1)
                	AND l.loc_type_id = 1
                	GROUP BY
                		id_barang,
                		loc_type_id
                	ORDER BY
                		l.loc_type_id,
                		rb.id_barang
                )AS tput ON tput.id_barang = b.id_barang
                LEFT JOIN(
                	SELECT
                		rb.id_barang,
                		t.loc_type_name,
                		t.loc_type_id,
                		sum(last_qty)AS qty
                	FROM
                		receiving_barang rb
                	LEFT JOIN m_loc l ON l.loc_id = rb.loc_id
                	LEFT JOIN m_loc_type t ON t.loc_type_id = l.loc_type_id
                	WHERE
                		(st_shipping = 0 OR last_qty > 1)
                	AND(
                		l.loc_type_id = 2
                		OR l.loc_type_id = 3
                	)
                	GROUP BY
                		id_barang,
                		loc_type_id
                	ORDER BY
                		l.loc_type_id,
                		rb.id_barang
                )AS tpick ON tpick.id_barang = b.id_barang
                WHERE
                	tpick.qty < minimum_stock'.$where;

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
                	b.kd_barang,
                	b.nama_barang,
                	minimum_stock,
                	tpick.qty AS qty_picking,
                	tput.qty AS qty_putaway
                FROM
                	barang b
                LEFT JOIN(
                	SELECT
                		rb.id_barang,
                		t.loc_type_name,
                		t.loc_type_id,
                		sum(last_qty)AS qty
                	FROM
                		receiving_barang rb
                	LEFT JOIN m_loc l ON l.loc_id = rb.loc_id
                	LEFT JOIN m_loc_type t ON t.loc_type_id = l.loc_type_id
                	WHERE
                		(st_shipping = 0 OR last_qty > 1)
                	AND l.loc_type_id = 1
                	GROUP BY
                		id_barang,
                		loc_type_id
                	ORDER BY
                		l.loc_type_id,
                		rb.id_barang
                )AS tput ON tput.id_barang = b.id_barang
                LEFT JOIN(
                	SELECT
                		rb.id_barang,
                		t.loc_type_name,
                		t.loc_type_id,
                		sum(last_qty)AS qty
                	FROM
                		receiving_barang rb
                	LEFT JOIN m_loc l ON l.loc_id = rb.loc_id
                	LEFT JOIN m_loc_type t ON t.loc_type_id = l.loc_type_id
                	WHERE
                		(st_shipping = 0 OR last_qty > 1)
                	AND(
                		l.loc_type_id = 2
                		OR l.loc_type_id = 3
                	)
                	GROUP BY
                		id_barang,
                		loc_type_id
                	ORDER BY
                		l.loc_type_id,
                		rb.id_barang
                )AS tpick ON tpick.id_barang = b.id_barang
                WHERE
                	tpick.qty < minimum_stock'.$where;

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['minimum_stock'];
			$nested[] = $row[$i]['qty_picking'];
			$nested[] = $row[$i]['qty_putaway'];
			$nested[] = $row[$i]['qty_picking'] + $row[$i]['qty_putaway']; //total????

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}
}

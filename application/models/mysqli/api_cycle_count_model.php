<?php

class Api_cycle_count_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function load(){
        $this->db->select("cc_code, cc_type");
        $this->db->from("cycle_count cc");
        $this->db->join('cycle_count_users ccu','cc.cc_id=ccu.cc_id');
        $this->db->join('hr_user usr','usr.user_id=ccu.user_id');
        $this->db->where_not_in("status", array(1,4));
		$this->db->where("cc_finish IS NULL");
        $this->db->where('usr.user_name',$this->input->get_request_header('User', true));
        $this->db->group_by('cc.cc_id');
        return $this->db->get();
    }

    function load_type($type){
        if($type == "SN"){
            $this->db->select("loc_name AS type_name");
            $this->db->from("m_loc");
            $this->db->where("loc_name IS NOT NULL");
			$this->db->where("loc_type", "BINLOC");
			$this->db->order_by("loc_name", "ASC");
        }else{
            $this->db->select("kd_barang AS type_name");
            $this->db->from("barang");
            $this->db->where("kd_barang IS NOT NULL");
			$this->db->order_by("kd_barang", "ASC");
        }
        return $this->db->get();
    }

    function check($cycle_code){
    	$this->db->select("status");
    	$this->db->from("cycle_count");
    	$this->db->where("cc_code", $cycle_code);
    	return $this->db->get();
    }
	
	function check_positive_type($cycle_code, $type, $type1){
    	if($type1 == "SN"){
			$this->db->select("*");
			$this->db->from("cycle_count_h2 cc2");
			$this->db->join("cycle_count cc", "cc2.cc_id_h1 = cc.cc_id");
			$this->db->join("barang brg", "cc2.item_id = brg.id_barang");
			$this->db->where("cc.cc_code", $cycle_code);
			$this->db->where("brg.kd_barang", $type);
			return $this->db->get();
		} else{
			$this->db->select("*");
			$this->db->from("cycle_count_h2 cc2");
			$this->db->join("cycle_count cc", "cc2.cc_id_h1 = cc.cc_id");
			$this->db->join("m_loc loc", "cc2.loc_id = loc.loc_id");
			$this->db->where("cc.cc_code", $cycle_code);
			$this->db->where("loc.loc_name", $type);
			return $this->db->get();

		}
    }

    // function get($cycle_code){
    // 	$this->db->select("kd_barang, GROUP_CONCAT(DISTINCT loc_name ORDER BY loc_name ASC) AS location, qty, (SELECT IF(brg.has_qty = 0, COUNT(ccd.kd_unik), IFNULL(SUM(cc_qty), 0)) FROM cycle_count_detail ccd LEFT JOIN receiving_barang rcv_ ON ccd.kd_unik = rcv_.kd_unik WHERE ccd.cc_id = ccq.cc_id AND rcv_.id_barang = ccq.id_barang AND rcv_.pl_id IS NULL) AS cced_qty");
    //     $this->db->from("cycle_count_qty ccq");
    //     $this->db->join("barang brg", "ccq.id_barang = brg.id_barang", "left");
    //     $this->db->join("receiving_barang rcv" , "brg.id_barang = rcv.id_barang", "left");
    //     $this->db->join("m_loc loc" , "rcv.loc_id = loc.loc_id", "left");
    //     $this->db->join("cycle_count cc", "cc.cc_id = ccq.cc_id", "left");
    //     $this->db->where("cc_code", $cycle_code);
    //     $this->db->group_by("ccq.id_barang");
    //     return $this->db->get();
    // }
	
	/*
	function get($cycle_code, $cycle_type){
        if($cycle_type == "SN"){
            $this->db->select("cc_code, brg.kd_barang AS type, (SELECT IFNULL(SUM(cc_qty), 0) FROM cycle_count_detail ccd_ LEFT JOIN cycle_count cc_ ON ccd_.cc_id = cc_.cc_id LEFT JOIN barang brg_ ON ccd_.id_barang = brg_.id_barang WHERE cc_.cc_code = '" . $cycle_code . "' AND brg_.kd_barang = brg.kd_barang) AS cc_qty, (SELECT IFNULL(SUM(last_qty), 0) FROM receiving_barang rcv_ LEFT JOIN barang brg_ ON rcv_.id_barang = brg_.id_barang LEFT JOIN cycle_count cc_ ON rcv_.cc_id = cc_.cc_id WHERE cc_.cc_code = '" . $cycle_code . "' AND brg_.kd_barang = brg.kd_barang) AS last_qty", false);
            $this->db->from("cycle_count cc");
            $this->db->join("receiving_barang rcv", "cc.cc_id = rcv.cc_id", "left");
            $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
            $this->db->group_by("brg.kd_barang");
            $this->db->where("cc_code", $cycle_code);
            return $this->db->get();
        } else{
            $this->db->select("cc_code, loc.loc_name AS type, (SELECT IFNULL(SUM(cc_qty), 0) FROM cycle_count_detail ccd_ LEFT JOIN cycle_count cc_ ON ccd_.cc_id = cc_.cc_id LEFT JOIN m_loc loc_ ON ccd_.loc_id = loc_.loc_id WHERE cc_.cc_code = '" . $cycle_code . "' AND loc_.loc_name = loc.loc_name) AS cc_qty, (SELECT IFNULL(SUM(last_qty), 0) FROM receiving_barang rcv_ LEFT JOIN m_loc loc_ ON rcv_.loc_id = loc_.loc_id LEFT JOIN cycle_count cc_ ON rcv_.cc_id = cc_.cc_id WHERE cc_.cc_code = '" . $cycle_code . "' AND loc_.loc_name = loc.loc_name) AS last_qty", false);
            $this->db->from("cycle_count cc");
            $this->db->join("receiving_barang rcv", "cc.cc_id = rcv.cc_id", "left");
            $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
            $this->db->group_by("loc.loc_name");
            $this->db->where("cc_code", $cycle_code);
            return $this->db->get();
        }
    }
	*/
    // function get($cycle_code, $cycle_type){
    //     $this->db->select(array("cc_id","IFNULL(NULL,'$cycle_type') as cc_type","IFNULL(item_code, location_code) as type","system_qty as avail_qty","scanned_qty as pick_qty"));
    //     $this->db->where('cc_id',$cycle_code);
    //     $data = $this->db->get('cycle_count_temp_result');

    //     return $data;
    // }

    function get($cycle_code, $cycle_type){
        if($cycle_type == "SN"){
            /*$this->db->select("cc_code, (SELECT kd_barang FROM barang brg_ WHERE brg_.id_barang = brg.id_barang) AS type, (SELECT IFNULL(SUM(cc_qty), 0) FROM cycle_count_detail ccd_ LEFT JOIN barang brg_ ON ccd_.id_barang = brg_.id_barang WHERE ccd_.cc_id = cc.cc_id AND ccd_.id_barang = cc2.item_id) AS cc_qty, (SELECT IFNULL(SUM(last_qty), 0) FROM receiving_barang rcv_ WHERE rcv_.cc_id = cc.cc_id AND rcv_.id_barang = cc2.item_id) AS last_qty", false);
            $this->db->from("cycle_count cc");
			$this->db->join("cycle_count_h2 cc2", "cc.cc_id = cc2.cc_id_h1", "left");
            $this->db->join("receiving_barang rcv", "cc.cc_id = rcv.cc_id", "left");
            $this->db->join("barang brg", "cc2.item_id = brg.id_barang", "left");
            $this->db->group_by("brg.kd_barang");
            $this->db->where("cc_code", $cycle_code);*/
           /* $sql =  "SELECT
                        cc_code,
                        (
                            kd_barang
                        )AS type,
                        (
                            IFNULL(SUM(cc_qty), 0)
                        )AS cc_qty,
                        (
                            IFNULL(SUM(last_qty), 0)
                        )AS last_qty
                    FROM
                        (`cycle_count` cc)
                    LEFT JOIN cycle_count_detail ccd on ccd.cc_id = cc.cc_id
                    LEFT JOIN `cycle_count_h2` cc2 ON `cc`.`cc_id` = `cc2`.`cc_id_h1`
                    LEFT JOIN `receiving_barang` rcv ON `cc`.`cc_id` = `rcv`.`cc_id`
                    LEFT JOIN `barang` brg ON `cc2`.`item_id` = `brg`.`id_barang`
                    WHERE
                        `cc_code` = '".$cycle_code."'
                    GROUP BY
                        `brg`.`kd_barang`";*/

            $sql = "SELECT
                        cc_code,
                        (kd_barang)AS type,
                        IFNULL(cc_qty,0) as pick_qty,
                        (IFNULL(SUM(last_qty), 0))AS avail_qty
                    FROM
                        (`cycle_count` cc)
                    LEFT JOIN `receiving_barang` rcv ON `cc`.`cc_id` = `rcv`.`cc_id`
                    LEFT JOIN `barang` brg ON rcv.id_barang = `brg`.`id_barang`
                    LEFT JOIN(
                        SELECT
                            id_barang,
                            sum(cc_qty)AS cc_qty
                        FROM
                            cycle_count cc
                        LEFT JOIN cycle_count_detail ccd ON ccd.cc_id = cc.cc_id
                        WHERE
                            `cc_code` = '".$cycle_code."'
                        GROUP BY
                            id_barang
                    )AS det ON det.id_barang = brg.id_barang
                    WHERE
                        `cc_code` = '".$cycle_code."'
                    GROUP BY
                        `brg`.`kd_barang`";

            
            return $this->db->query($sql);
        } else{
            /*$this->db->select("cc_codea, (SELECT loc_name FROM m_loc loc_ WHERE loc_.loc_id = loc.loc_id) AS type, (SELECT IFNULL(SUM(cc_qty), 0) FROM cycle_count_detail ccd_ LEFT JOIN m_loc loc_ ON ccd_.loc_id = loc_.loc_id WHERE ccd_.cc_id = cc.cc_id AND ccd_.loc_id = cc2.loc_id) AS cc_qty, (SELECT IFNULL(SUM(last_qty), 0) FROM receiving_barang rcv_ LEFT JOIN m_loc loc_ ON rcv_.loc_id = loc_.loc_id LEFT JOIN cycle_count cc_ ON rcv_.cc_id = cc_.cc_id WHERE cc_.cc_code = '" . $cycle_code . "' AND loc_.loc_name = loc.loc_name) AS last_qty", false);
            $this->db->from("cycle_count cc");
			$this->db->join("cycle_count_h2 cc2", "cc.cc_id = cc2.cc_id_h1", "left");
            $this->db->join("receiving_barang rcv", "cc.cc_id = rcv.cc_id", "left");
            $this->db->join("m_loc loc", "cc2.loc_id = loc.loc_id", "left");
            $this->db->group_by("loc.loc_name");
            $this->db->where("cc_code", $cycle_code);*/
           /* $sql =  "SELECT
                        cc_code,
                        (
                            loc_name
                        )AS type,
                        (
                            IFNULL(SUM(cc_qty), 0)
                        )AS cc_qty,
                        (
                            IFNULL(SUM(last_qty), 0)
                        )AS last_qty
                    FROM
                        (`cycle_count` cc)
                    LEFT JOIN cycle_count_detail ccd on ccd.cc_id = cc.cc_id
                    LEFT JOIN `cycle_count_h2` cc2 ON `cc`.`cc_id` = `cc2`.`cc_id_h1`
                    LEFT JOIN `receiving_barang` rcv ON `cc`.`cc_id` = `rcv`.`cc_id`
                    LEFT JOIN `m_loc` loc ON `cc2`.`loc_id` = `loc`.`loc_id`
                    WHERE
                        `cc_code` = '".$cycle_code."'
                    GROUP BY
                        `loc`.`loc_name`";*/
            $sql = "SELECT
                        cc_code,
                        (loc_name)AS type,
                        IFNULL(cc_qty,0) as pick_qty,
                        (IFNULL(SUM(last_qty), 0))AS avail_qty
                    FROM
                        (`cycle_count` cc)
                    LEFT JOIN `receiving_barang` rcv ON `cc`.`cc_id` = `rcv`.`cc_id`
                    LEFT JOIN `m_loc` l ON l.loc_id = rcv.loc_id
                    LEFT JOIN(
                        SELECT
                            loc_id,
                            sum(cc_qty)AS cc_qty
                        FROM
                            cycle_count cc
                        LEFT JOIN cycle_count_detail ccd ON ccd.cc_id = cc.cc_id
                        WHERE
                            `cc_code` = '".$cycle_code."'
                        GROUP BY
                            loc_id
                    )AS det ON det.loc_id = l.loc_id
                    WHERE
                        `cc_code` = '".$cycle_code."'
                    GROUP BY
                        l.loc_name";
            return $this->db->query($sql);
        }
    }
	
	function get_serial_item($serial_number){
		$this->db->select("IFNULL(kd_barang, 'GENERATED') AS kd_barang", false);
		$this->db->from("receiving_barang rcv");
		$this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
		$this->db->where("kd_unik", $serial_number);
		return $this->db->get();
	}

    function get_serial($cycle_code, $item_code, $type, $serial_number){
        if($type == "SN"){
            $this->db->select("has_qty, IF(has_qty = 0, 1, last_qty) AS avail_qty, loc_name AS type1, kd_qc, tgl_exp, tgl_in, putaway_time, kd_barang, first_qty",false);
            $this->db->from("receiving_barang rcv");
			$this->db->join("m_qc qc", "rcv.id_qc = qc.id_qc", "left");
			$this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
            $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
            $this->db->join("cycle_count cc", "rcv.cc_id = cc.cc_id", "left");
            $this->db->where("kd_unik", $serial_number);
            $this->db->where("cc_code", $cycle_code);
            $this->db->where("kd_barang", $item_code);
            $this->db->where("rcv.id_barang IS NOT NULL");
            $this->db->where("rcv.loc_id IS NOT NULL");
            // $this->db->where("rcv.pl_id IS NULL");
            // $this->db->where("rcv.st_cc", 0);
			$this->db->where_not_in("rcv.loc_id", 104);
            return $this->db->get();
        } else{
            $this->db->select("has_qty, IF(has_qty = 0, 1, last_qty) AS avail_qty, kd_barang AS type1, kd_qc, tgl_exp, tgl_in, putaway_time, kd_barang, first_qty",false);
            $this->db->from("receiving_barang rcv");
			$this->db->join("m_qc qc", "rcv.id_qc = qc.id_qc", "left");
            $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
            $this->db->join("cycle_count cc", "rcv.cc_id = cc.cc_id", "left");
            $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
            $this->db->where("kd_unik", $serial_number);
            $this->db->where("cc_code", $cycle_code);
            $this->db->where("loc_name", $item_code);
            $this->db->where("rcv.id_barang IS NOT NULL");
            $this->db->where("rcv.loc_id IS NOT NULL");
            // $this->db->where("rcv.pl_id IS NULL");
            // $this->db->where("rcv.st_cc", 0);
            return $this->db->get();
        }
    }
	
	function get_serial_loc($serial_number){
		$this->db->select("has_qty, IF(has_qty = 0, 1, last_qty) AS avail_qty, loc_name AS type1, kd_qc, tgl_exp, tgl_in, putaway_time, kd_barang", false);
		$this->db->from("receiving_barang rcv");
		$this->db->join("m_qc qc", "rcv.id_qc = qc.id_qc", "left");
		$this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
		$this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
		$this->db->where("kd_unik", $serial_number);
		$this->db->where("rcv.id_barang IS NOT NULL");
		$this->db->where("rcv.loc_id IS NOT NULL");
		$this->db->where("rcv.pl_id IS NULL");
		$this->db->where_not_in("rcv.loc_id", 104);
		return $this->db->get();
    }

    function get_serial_lp($serial_number){
        $this->db->select("*", false);
        $this->db->from("receiving_barang rcv");
        $this->db->join("m_qc qc", "rcv.id_qc = qc.id_qc", "left");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->where("kd_parent", $serial_number);
        $this->db->where("rcv.id_barang IS NOT NULL");
        $this->db->where("rcv.loc_id IS NOT NULL");
        $this->db->where("rcv.pl_id IS NULL");
        $this->db->where_not_in("rcv.loc_id", 104);
        return $this->db->get();
    }

    function get_positive_serial($serial_number){
        $this->db->select("*");
        $this->db->from("receiving_barang");
        $this->db->where("kd_unik", $serial_number);
        return $this->db->get();
    }

    function get_item($item_code){
        $this->db->select("has_batch, has_expdate, has_qty, def_qty");
        $this->db->from("barang");
        $this->db->where("kd_barang", $item_code);
        return $this->db->get();
    }

    function get_cc_to_rcv($cycle_code){
        $this->db->select("*");
        $this->db->from("receiving");
        $this->db->where("kd_receiving", $cycle_code);
        return $this->db->get();
    }

    function set_cc_to_rcv($cycle_code,$user){
        $id_user  = $this->get_id("user_id", "user_name", $user, "hr_user")->row_array();
        $data_insert = array(  'tanggal_receiving'  => date("Y-m-d H:i:s"), 
                               'kd_receiving'       => $cycle_code,
                               'st_receiving'       => 0, 
                               'user_id'            => $id_user["user_id"]
                               );
        $this->db->insert("receiving",$data_insert);
    }

    function is_exist_to_post($data)
    {
        $id_cc = $this->get_id("cc_id", "cc_code", $data["cc_code"], "cycle_count")->row_array();

        $this->db->select("*");
        $this->db->from("cycle_count_detail");
        $this->db->where("cc_id", $id_cc["cc_id"]);
        $this->db->where("kd_unik", $data["kd_unik"]);
        return $this->db->get();
    }

    function post($data){
        $id_cc      = $this->get_id("cc_id", "cc_code", $data["cc_code"], "cycle_count")->row_array();
        $id_user    = $this->get_id("user_id", "user_name", $data["user"], "hr_user")->row_array();
        $id_loc     = $this->get_id("loc_id", "kd_unik", $data["kd_unik"], "receiving_barang")->row_array();
        $id_barang  = $this->get_id("id_barang", "kd_unik", $data["kd_unik"], "receiving_barang")->row_array();
    	$data_insert = array(  'cc_id'      => $id_cc["cc_id"], 
                               'id_barang'  => $id_barang["id_barang"],
                               'loc_id'     => $id_loc["loc_id"], 
                               'kd_unik'    => $data["kd_unik"],
                               'cc_qty'     => $data["cc_qty"],
                               'cc_time'    => $data["cc_time"],
                               'user_id_cc' => $id_user["user_id"]
                               );
        $this->db->insert("cycle_count_detail",$data_insert);

        $this->db->set("st_cc", 1);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->update("receiving_barang");
    }

    function start($cycle_code){
        $this->db->set("status", 2);
		$this->db->set("cc_start", "IFNULL(cc_start, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("cc_code", $cycle_code);
        $this->db->update("cycle_count");
    }

    function finish($cycle_code){
        $this->db->set("status", 0);
        $this->db->where("cc_code", $cycle_code);
        $this->db->update("cycle_count");
    }
	
	function get_type($cycle_code){
		$this->db->select("*");
		$this->db->from("cycle_count");
		$this->db->where("cc_code", $cycle_code);
		return $this->db->get();
	}
	
	function post_loc($data){
        $id_cc      = $this->get_id("cc_id", "cc_code", $data["cc_code"], "cycle_count")->row_array();
        $id_user    = $this->get_id("user_id", "user_name", $data["user"], "hr_user")->row_array();
        $id_loc     = $this->get_id("loc_id", "loc_name", $data["type"], "m_loc")->row_array();
        $id_barang  = $this->get_id("id_barang", "kd_barang", $data["type1"], "barang")->row_array();
		$id_qc      = $this->get_id("id_qc", "kd_qc", $data["kd_qc"], "m_qc")->row_array();
    	$data_insert = array(  'cc_id'      => $id_cc["cc_id"], 
                               'id_barang'  => $id_barang["id_barang"],
                               'loc_id'     => $id_loc["loc_id"], 
                               'kd_unik'    => $data["kd_unik"],
                               'cc_qty'     => $data["cc_qty"],
                               'cc_time'    => $data["cc_time"],
                               'user_id_cc' => $id_user["user_id"],
							   'id_qc' 		=> $id_qc["id_qc"],
							   'tgl_exp' 	=> $data["tgl_exp"],
							   'tgl_in' 	=> $data["tgl_in"]
                               );
        $this->db->insert("cycle_count_detail",$data_insert);

        $this->db->set("st_cc", 1);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->update("receiving_barang");
    }
	
	function post_sn($data){
        $id_cc      = $this->get_id("cc_id", "cc_code", $data["cc_code"], "cycle_count")->row_array();
        $id_user    = $this->get_id("user_id", "user_name", $data["user"], "hr_user")->row_array();
        $id_loc     = $this->get_id("loc_id", "loc_name", $data["type1"], "m_loc")->row_array();
        $id_barang  = $this->get_id("id_barang", "kd_barang", $data["type"], "barang")->row_array();
		$id_qc      = $this->get_id("id_qc", "kd_qc", $data["kd_qc"], "m_qc")->row_array();
    	$data_insert = array(  'cc_id'      => $id_cc["cc_id"], 
                               'id_barang'  => $id_barang["id_barang"],
                               'loc_id'     => $id_loc["loc_id"], 
                               'kd_unik'    => $data["kd_unik"],
                               'cc_qty'     => $data["cc_qty"],
                               'cc_time'    => $data["cc_time"],
                               'user_id_cc' => $id_user["user_id"],
							   'id_qc' 		=> $id_qc["id_qc"],
							   'tgl_exp' 	=> $data["tgl_exp"],
							   'tgl_in' 	=> $data["tgl_in"]
                               );
        $this->db->insert("cycle_count_detail",$data_insert);

        $this->db->set("st_cc", 1);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->update("receiving_barang");
    }

    function get_item_picked($data=array()){

        if($data['cc_type'] == "SN"){

            $this->db
                ->select(array("cc_code","cc_type","loc_name as type","IFNULL( sum(last_qty), '0') as avail_qty","IFNULL(sum(cc_qty),0) as pick_qty"))
                ->from('cycle_count cc')
                ->join('cycle_count_h2 cc2','cc2.cc_id_h1=cc.cc_id')
                ->join('receiving_barang rb','rb.id_barang=cc2.item_id')
                ->join('barang b','b.id_barang=rb.id_barang')
                ->join('m_loc loc','loc.loc_id=rb.loc_id')
                ->join('cycle_count_detail ccd','ccd.cc_id=cc.cc_id and ccd.kd_unik=rb.kd_unik','left')
                ->where('cc.cc_code',$data['cc_code'])
                ->where('b.kd_barang',$data['code'])
                ->where_not_in('rb.loc_id',array(100,101,102,103,104,105,106))
                ->group_by('rb.loc_id');

        }else{

            $this->db
                ->select(array("cc_code","cc_type","kd_barang as type","IFNULL( sum(last_qty), '0') as avail_qty","IFNULL(sum(cc_qty),0) as pick_qty"))
                ->from('cycle_count cc')
                ->join('cycle_count_h2 cc2','cc2.cc_id_h1=cc.cc_id')
                ->join('receiving_barang rb','rb.loc_id=cc2.loc_id')
                ->join('barang b','b.id_barang=rb.id_barang')
                ->join('m_loc loc','loc.loc_id=rb.loc_id')
                ->join('cycle_count_detail ccd','ccd.cc_id=cc.cc_id and ccd.kd_unik=rb.kd_unik','left')
                ->where('cc.cc_code',$data['cc_code'])
                ->where('loc.loc_name',$data['code'])
                ->where_not_in('rb.loc_id',array(100,101,102,103,104,105,106))
                ->group_by('rb.id_barang');

        }

        $data = $this->db->get();

        return $data->result_array();

    }

    public function delPost($post=array(),$cc=array()){
        $this->db->where('cc_id',$cc['cc_id']);
        $this->db->where('kd_unik',$post['kd_unik']);
        $this->db->delete('cycle_count_detail');
    }

}
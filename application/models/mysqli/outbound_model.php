<?php
class outbound_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'outbound';
    private $table2 = "m_outbound_document";
    private $table3 = "outbound_barang";
    private $table4 = "m_customer";

    public function __get_new_outbound(){
        $result = $this->fina->get_where('f_t_outbound_h',array('SyncStatus'=>'N'));
        return $result;
    }

    public function get_outbound_document($outbound_document_name=''){
        $result = $this->db->get_where('m_outbound_document',array('outbound_document_name'=>$outbound_document_name));
        return $result;
    }

    public function create_outbound($data=array()){
        $result = $this->db->insert('outbound',$data);
        return $this->db->insert_id();
    }

    public function get_outbound_item($DocEntry=0){
        $result = $this->fina->get_where('f_t_outbound_l1',array('DocEntry'=>$DocEntry));
        return $result;
    }

    public function get_barang($kd_barang=''){
        $result = $this->db->get_where('barang',array('kd_barang'=>$kd_barang));
        return $result;
    }

    public function create_outbound_barang($data=array()){
        $result = $this->db->insert('outbound_barang',$data);
        return $this->db->insert_id();
    }

    public function update_outbound_barang($data=array(),$id=0){
        $result = $this->db->update('outbound_barang',$data,array('id'=>$id));
        return $result;
    }

    public function update_outbound($data=array(),$id=0){
        $result = $this->db->update('outbound',$data,array('id_outbound'=>$id));
        return $result;
    }

    public function set_outbound_status($DocEntry=0,$SyncDate=''){
        $result = $this->fina->update('f_t_outbound_h',array('SyncStatus'=>'Y','SyncDate'=>$SyncDate),array('DocEntry'=>$DocEntry));
        return $result;
    }

    public function check_outbound($kd_outbound=''){
        $result = $this->db->get_where('outbound',array('kd_outbound'=>$kd_outbound));
        return $result;
    }

    public function check_outbound_barang($where=array()){
        $result = $this->db->get_where('outbound_barang',$where);
        return $result;
    }

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', "a.id_outbound_document=b.id_outbound_document", "left");
        $this->db->join($this->table4 . ' c', "a.id_customer=c.id_customer", "left");
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_outbound'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

	public function getOutboundPeminjaman(){
		$result = array();

		$sql = 'SELECT
					id_outbound, kd_outbound
				FROM
					outbound
				WHERE
					id_outbound_document=\'6\'';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getBatch($post = array()){
		$result = array();

		$sql = 'SELECT
					has_batch
				FROM
					barang
				WHERE
					id_barang=\''.$post['id_barang'].'\'';

		$row = $this->db->query($sql)->row_array();

		if(isset($row['has_batch'])){

			if($row['has_batch'] == 1){

				$sql = 'SELECT
							kd_batch
						FROM
							receiving_barang
						WHERE
							id_barang=\''.$post['id_barang'].'\'
						GROUP BY
							kd_batch';

				$row = $this->db->query($sql)->result_array();

				$result['is_batch'] = 'YES';
				$result['data'] = $row;

			}else{
				$result['is_batch'] = 'NO';
				$result['data'] = array();
			}

		}else{

			$result['is_batch'] = 'NO';
			$result['data'] = array();

		}

		return $result;
	}

	public function closeOutbound($post = array()){
		$result = array();

		$sql = 'UPDATE
					outbound
				SET
					id_status_outbound=\'2\'
				WHERE
					id_outbound=\''.$post['id_outbound'].'\'';

		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Close Outbound Document Success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Close Outbound Document Failed';
		}

		return $result;
	}

	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
			4 => 'jumlah_barang',
			5 => 'batch',
			6 => 'picked'
		);

		$where = '';

		if(!empty($post['id_outbound']))
			$where .= sprintf(' AND outb.id_outbound = \'%s\' ', $post['id_outbound']);

		$sql = 'SELECT
					%s
				FROM
					outbound outb
				JOIN outbound_barang outbg
					ON outbg.id_outbound=outb.id_outbound
				JOIN barang brg
					ON brg.id_barang=outbg.id_barang
				LEFT JOIN satuan st
					ON st.id_satuan=brg.id_satuan
				LEFT JOIN kategori kat
					ON kat.id_kategori=brg.id_kategori
				WHERE 1=1'.$where;

		//$q = sprintf($sql, 'COUNT(*) AS total, COALESCE(CONCAT(SUM(outbg.jumlah_barang), \' \', st.nama_satuan), SUM(outbg.jumlah_barang)) AS total_qty');
		$q = sprintf($sql, 'COUNT(*) AS total');

		$row = $this->db->query($q)->row_array();
		$totalData = $row['total'];

		$q = sprintf($sql, 'outbg.id, kd_barang, nama_barang,
							IFNULL(outbg.batch, \'-\') AS batch,
							COALESCE(CONCAT(outbg.jumlah_barang, \' \', st.nama_satuan), outbg.jumlah_barang) AS jumlah_barang,
							CONCAT((
								SELECT
									sum(qty)
								FROM
									picking_recomendation pr
								JOIN picking_list_outbound plo ON plo.pl_id = pr.id_picking
								WHERE
									plo.id_outbound = outb.id_outbound
								AND pr.id_barang = brg.id_barang
							),\' \', st.nama_satuan) AS picked');

		$q .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($q)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_barang'];
			$nested[] = $row[$i]['batch'];
			$nested[] = $row[$i]['picked'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function checkStatusPicking($id){
		$result = array();

		$sql = 'SELECT
					IFNULL((
						SELECT
							SUM(jumlah_barang)
						FROM
							outbound_barang
						WHERE
							id_outbound=\''.$id.'\'
					), 0) AS total_document,
					(
						SELECT
							COUNT(*)
						FROM
							picking_list_outbound plo
						JOIN picking_qty pq
							ON pq.pl_id=plo.pl_id
						JOIN receiving_barang rcvbrg
							ON rcvbrg.id_barang=pq.id_barang
								AND rcvbrg.pl_id=pq.pl_id
						WHERE
							plo.id_outbound=\''.$id.'\'
					) AS total_scanned';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$sql = 'SELECT
					outb.id_outbound, kd_outbound, DATE_FORMAT(tanggal_outbound, \'%d/%m/%Y\') AS tanggal_outbound,
					outb.id_outbound_document, outbound_document_name,
					kd_supplier, nama_supplier, alamat_supplier, telepon_supplier, cp_supplier,
					customer_name, address, outb.phone, nama_status, user_name,
					outb.id_status_outbound,
                    DestinationName,DestinationAddressL1,DestinationAddressL2,DestinationAddressL3
				FROM
					outbound outb
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_status_outbound msoutb
					ON msoutb.id_status_outbound=outb.id_status_outbound
				LEFT JOIN m_outbound_document moutbd
					ON moutbd.id_outbound_document=outb.id_outbound_document
				LEFT JOIN hr_user hu
					ON hu.user_id=outb.user_id
				WHERE
					outb.id_outbound=\''.$post['id_outbound'].'\'';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

    public function get_data($id) {
        //$this->data($condition);

		$sql = 'SELECT
					*
				FROM
					outbound outb
				LEFT JOIN m_outbound_document outbdoc
					ON outbdoc.id_outbound_document=outb.id_outbound_document
				WHERE
					id_outbound=\''.$id.'\'';

        return $this->db->query($sql);
    }

    public function add_do_code(){
        $this->db->set('inc_do',"inc_do+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

	public function getPickingList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'pl_id',
			1 => '',
			2 => 'pl_name',
			3 => 'kd_outbound',
			4 => 'pl_date',
			5 => 'customer_name',
			6 => 'remark',
			7 => 'pl_start',
			8 => 'pl_finish',
			9 => 'nama_status'
		);

		$where = '';

		if(!empty($post['pl_id']))
			$where .= sprintf(' AND pick.pl_id = \'%s\' ', $post['pl_id']);

		if(isset($post['id_outbound'])){
			if(!empty($post['id_outbound']))
				$where .= sprintf(' AND outb.id_outbound = \'%s\' ', $post['id_outbound']);
		}

		if(!empty($post['id_customer']))
			$where .= sprintf(' AND pick.id_customer = \'%s\' ', $post['id_customer']);

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(pick.pl_date) BETWEEN \'%s\' AND \'%s\' ',
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}

		if(!empty($post['status']))
			$where .= ' AND pick.id_status_picking IN ( '. implode(',', $post['status']) .' )';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						pick.pl_id
					FROM
						picking_list pick
					JOIN picking_list_outbound plo
						ON plo.pl_id=pick.pl_id
					JOIN outbound outb
						ON outb.id_outbound=plo.id_outbound
					LEFT OUTER JOIN supplier spl
						ON spl.id_supplier=outb.id_customer
							AND outb.id_outbound_document = \'2\'
					LEFT OUTER JOIN m_customer cust
						ON cust.id_customer=outb.id_customer
							AND outb.id_outbound_document != \'2\'
					LEFT JOIN m_status_picking mstat
						ON mstat.id_status_picking=pick.id_status_picking
					WHERE 1=1'.$where.'
					GROUP BY
						pick.pl_id
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = "SELECT
					pl.pl_id,
					pl_name,
					sum(jumlah_barang)AS t_jum,
					sum(qty)AS t_qty
				FROM
					picking_list pl
				LEFT JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
				LEFT JOIN outbound_barang ob ON ob.id_outbound = plo.id_outbound
				LEFT JOIN(
					SELECT
						id_picking,
						id_barang,
						sum(qty)AS qty
					FROM
						picking_recomendation
					GROUP BY
						id_picking,
						id_barang
				)tpr ON tpr.id_picking = pl.pl_id
				AND tpr.id_barang = ob.id_barang
				where pl.id_status_picking = 3
				GROUP BY
					pl_id";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			if($row->t_jum == $row->t_qty){
				$data_update = array(
					'id_status_picking' => 5);
				$this->db->where('pl_id',$row->pl_id);
				$this->db->update('picking_list',$data_update);
			}
			# code...
		}

		$sql = 'SELECT
					pick.pl_id, pl_name, outb.id_outbound, kd_outbound, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS pl_date,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
                    DestinationName,DestinationAddressL1,DestinationAddressL2,DestinationAddressL3,
					IFNULL(remark, \'-\') AS remark,
					IFNULL(DATE_FORMAT(pl_start, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS pl_start,
					IFNULL(DATE_FORMAT(pl_finish, \'%H:%i<br><span class="small-date">%d\/%m\/%Y<span>\'), \'-\') AS pl_finish,
					IFNULL(DATE_FORMAT(shipping_start, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS shipping_start,
					IFNULL(DATE_FORMAT(shipping_finish, \'%H:%i<br><span class="small-date">%d\/%m\/%Y</span>\'), \'-\') AS shipping_finish,
					pick.id_status_picking,nama_status
					/*,
					(
						SELECT
							COUNT(*)
						FROM
							receiving_barang rcvbrg
						JOIN picking_list pl
							ON pl.pl_id=rcvbrg.pl_id
						JOIN picking_list_outbound plo
							ON plo.pl_id=pl.pl_id
						WHERE
							plo.id_outbound=outb.id_outbound
					) AS total_picked*/
				FROM
					picking_list pick
				JOIN picking_list_outbound plo
					ON plo.pl_id=pick.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_status_picking mstat
					ON mstat.id_status_picking=pick.id_status_picking
				LEFT JOIN shipping_picking shippick
					ON shippick.pl_id=pick.pl_id
				LEFT JOIN shipping shipp
					ON shipp.shipping_id=shippick.shipping_id
				WHERE 1=1'.$where.'
				GROUP BY
					pick.pl_id';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';
			if($row[$i]['pl_start'] != '-'){
				$class = 'disabled-link';
			}

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-print"></i> Print Picking List</a>';
				$action .= '</li>';
            }
			/*
			$class = 'disabled-link';
			if($row[$i]['id_status_picking'] == '2'){

				if($row[$i]['total_picked'] > 0){
					$class = '';
				}

			}

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-check"></i> Finish Picking</a>';
			$action .= '</li>';
			*/
			$class = '';
			if($row[$i]['pl_start'] != '-'){
				$class = 'disabled-link';
			}

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['pl_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'picking_list/detail/'.$row[$i]['pl_id'].'">'.$row[$i]['pl_name'].'</a>';
			$nested[] = '<a href="'.base_url().'outbound/detail/'.$row[$i]['id_outbound'].'">'.$row[$i]['kd_outbound'].'</a>';
			$nested[] = $row[$i]['pl_date'];
			$nested[] = $row[$i]['DestinationName'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['pl_start'];
			$nested[] = $row[$i]['pl_finish'];
			$nested[] = $row[$i]['nama_status'];

			if($post['params'] != 'no_action')
				$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getShippingList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'shipping_id',
			1 => '',
			2 => 'shipping_code',
			3 => 'shipping_date',
			4 => 'driver_name',
			5 => 'license_plate',
			6 => 'shipping_start',
			7 => 'shipping_finish',
			8 => 'st_shipping_name'
		);

		$where = '';

		if(isset($post['id_outbound'])){
			if(!empty($post['id_outbound']))
				$where .= sprintf(' AND outb.id_outbound = \'%s\' ', $post['id_outbound']);
		}

		if(!empty($post['id_shipping']))
			$where .= sprintf(' AND shipp.shipping_id = \'%s\' ', $post['id_shipping']);

		if(!empty($post['pl_id']))
			$where .= sprintf(' AND pick.pl_id = \'%s\' ', $post['pl_id']);

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND shipp.shipping_date BETWEEN \'%s\' AND \'%s\' ',
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}

		if($post['status'] != '' && $post['status'] != null)
			$where .= sprintf(' AND shipp.st_shipping = \'%s\' ', $post['status']);

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						shipp.shipping_id
					FROM
						shipping shipp
					JOIN shipping_picking shipppick
						ON shipppick.shipping_id=shipp.shipping_id
					JOIN picking_list pick
						ON pick.pl_id=shipppick.pl_id
					JOIN picking_list_outbound plo
						ON plo.pl_id=pick.pl_id
					JOIN outbound outb
						ON outb.id_outbound=plo.id_outbound
					LEFT OUTER JOIN supplier spl
						ON spl.id_supplier=outb.id_customer
							AND outb.id_outbound_document = \'2\'
					LEFT OUTER JOIN m_customer cust
						ON cust.id_customer=outb.id_customer
							AND outb.id_outbound_document != \'2\'
					LEFT JOIN m_shipping sstat
						ON sstat.st_shipping=shipp.st_shipping
					LEFT JOIN shipping_delivery_order sdo
						ON sdo.shipping_id=shipp.shipping_id
					WHERE 1=1'.$where.'
					GROUP BY
						shipp.shipping_id
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					shipp.shipping_id, shipping_code, DATE_FORMAT(shipping_date, \'%d\/%m\/%Y\') AS shipping_date,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					driver_name, license_plate,
					IFNULL(DATE_FORMAT(shipping_start, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_start,
					IFNULL(DATE_FORMAT(shipping_finish, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_finish,
					shipp.st_shipping, st_shipping_name AS status, do_number
				FROM
					shipping shipp
				JOIN shipping_picking shipppick
					ON shipppick.shipping_id=shipp.shipping_id
				JOIN picking_list pick
					ON pick.pl_id=shipppick.pl_id
				JOIN picking_list_outbound plo
					ON plo.pl_id=pick.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_shipping sstat
					ON sstat.st_shipping=shipp.st_shipping
				LEFT JOIN shipping_delivery_order sdo
					ON sdo.shipping_id=shipp.shipping_id
				WHERE 1=1'.$where.'
				GROUP BY
					shipp.shipping_id';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';
			$class = '';
			$class1 = '';

			if($row[$i]['st_shipping'] == '1')
				$class = 'disabled-link';

			if($row[$i]['shipping_finish'] == '-')
				$class1 = 'disabled-link';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			if ($this->access_right->otoritas('print')) {
				$action .= '<li>';
				$action .= '<a class="data-table-print '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-print"></i> Print Manifest</a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-print-do '.$class1.'" data-id="'.$row[$i]['shipping_id'].'" data-do="'.$row[$i]['do_number'].'"><i class="fa fa-print"></i> Print Delivery Order</a>';
				$action .= '</li>';

				$action .= '<li>';
				$action .= '<a class="data-table-print-plate" data-id="'.$row[$i]['shipping_id'].'" data-plate="'.$row[$i]['license_plate'].'"><i class="fa fa-print"></i> Print License Plate</a>';
				$action .= '</li>';
            }

			if ($this->access_right->otoritas('edit')) {

				$class = 'disabled-link';
				if($row[$i]['shipping_start'] != '-' && $row[$i]['st_shipping'] != '1')
					$class = '';

				// $action .= '<li>';
				// $action .= '<a class="data-table-close '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-check"></i> Finish Loading</a>';
				// $action .= '</li>';

				$class = '';
				if($row[$i]['st_shipping'] == '1')
					$class = 'disabled-link';

				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['shipping_id'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();

			$start = $row[$i]['shipping_start'];
			$finish = $row[$i]['shipping_finish'];

			if($start != '-'){
				$start = explode(' ', $start);
				$start = $start[1] . '<br>' . '<span class="small-date">' . $start[0] . '</span>';
			}

			if($finish != '-'){
				$finish = explode(' ', $finish);
				$finish = $finish[1] . '<br>' . '<span class="small-date">' . $finish[0] . '</span>';
			}

			$nested[] = $row[$i]['shipping_id'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().'loading/detail/'.$row[$i]['shipping_id'].'">'.$row[$i]['shipping_code'].'</a>';
			$nested[] = $row[$i]['shipping_date'];
			$nested[] = $row[$i]['customer_name'];
			$nested[] = $row[$i]['driver_name'];
			$nested[] = $row[$i]['license_plate'];
			$nested[] = $start;
			$nested[] = $finish;
			$nested[] = $row[$i]['status'];

			if($post['params'] != 'no_action')
				$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function checkExistDoNumber($post = array()){
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					outbound
				WHERE
					kd_outbound=\''.$post['kd_outbound'].'\'';

		if(!empty($post['id_outbound'])){
			$sql = 'SELECT
						COUNT(*) AS total
					FROM
						outbound
					WHERE
						kd_outbound=\''.$post['kd_outbound'].'\'
					AND
						id_outbound!=\''.$post['id_outbound'].'\'';
		}

		$row = $this->db->query($sql)->row_array();

		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Doc. number already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}

		return $result;
	}

	public function get_do_code($post = array()){
		$result = '';

		$sql = 'SELECT
					CONCAT(pre_code_do, \'\', inc_do) AS doc_number
				FROM
					m_increment
				WHERE
					id_inc = \'1\'';

		$row = $this->db->query($sql)->row_array();
		$result = $row['doc_number'];

		return $result;
	}

	public function cek_not_usage_do($id){
		// $sql = 'SELECT
					// id_outbound
				// FROM
					// picking_list_outbound plo
				// JOIN picking_list pl
					// ON pl.pl_id=plo.pl_id
				// WHERE
					// id_outbound=\''.$id.'\'';

		// $q = $this->db->query($sql);

        // if($q->num_rows() > 0){
            // return FALSE;
        // }else{
            // return TRUE;
        // }


		$r = $this->checkStatusPicking($id);
		if($r['total_scanned'] > 0)
			return FALSE;
		else
			return TRUE;
    }

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_outbound',
			1 => '',
			2 => 'kd_outbound',
			3 => 'tanggal_outbound',
			4 => 'outbound_document_name',
			5 => 'customer_name',
			6 => 'nama_status'
		);

		$where = '';

		if(!empty($post['do_number']))
			$where .= sprintf(' AND outb.id_outbound = \'%s\' ', $post['do_number']);

		if(!empty($post['id_customer']))
			$where .= sprintf(' AND outb.id_customer = \'%s\' ', $post['id_customer']);

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND tanggal_outbound BETWEEN \'%s\' AND \'%s\' ',
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}

		if(!empty($post['doc_type']))
			$where .= sprintf(' AND outb.id_outbound_document = \'%s\' ', $post['doc_type']);

		if(!empty($post['doc_status']))
			$where .= ' AND outb.id_status_outbound IN ('. implode(',', $post['doc_status']) .')';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					outbound outb
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_status_outbound msout
					ON msout.id_status_outbound=outb.id_status_outbound
				LEFT JOIN m_outbound_document mout
					ON mout.id_outbound_document=outb.id_outbound_document
				WHERE 1=1'.$where;

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = "SELECT
					kd_outbound,
					o.id_outbound,
					sum(jumlah_barang)AS t_sum,
					sum(tobp.qty)AS t_qty
				FROM
					outbound o
				LEFT JOIN outbound_barang ob ON o.id_outbound = ob.id_outbound
				LEFT JOIN(
					SELECT
						id_outbound,
						id_barang,
						sum(qty)AS qty
					FROM
						outbound_barang_picked obp
					GROUP BY
						id_outbound,
						id_barang
				)tobp ON tobp.id_outbound = o.id_outbound
				AND ob.id_barang = tobp.id_barang
				where (o.id_status_outbound = 3 OR o.id_status_outbound = 1)
				AND ob.id_barang > 0
				GROUP BY
					o.id_outbound";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			if($row->t_sum == $row->t_qty){
				$data_update = array(
					'id_status_outbound' => 2 );
				$this->db->where('id_outbound',$row->id_outbound);
				$this->db->update('outbound',$data_update);
			}
			# code...
		}

		$sql = 'SELECT
					outb.id_outbound, kd_outbound, DATE_FORMAT(tanggal_outbound, \'%d/%m/%Y\') AS tanggal_outbound,
					outbound_document_name,DestinationName,(select count(id) from outbound_barang where id_outbound=outb.id_outbound) as item,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name, nama_status,
					(
						SELECT
							COUNT(*)
						FROM
							shipping shipp
						JOIN shipping_picking sp
							ON sp.shipping_id=sp.shipping_id
						JOIN picking_list pl
							ON pl.pl_id=sp.pl_id
						JOIN picking_list_outbound plo
							ON plo.pl_id=pl.pl_id
						JOIN outbound soutb
							ON soutb.id_outbound=plo.id_outbound
						WHERE
							soutb.id_outbound=outb.id_outbound
						AND
							soutb.id_status_outbound != \'2\'
						AND
							shipp.shipping_finish IS NOT NULL
					) AS finish_loading
				FROM
					outbound outb
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_status_outbound msout
					ON msout.id_status_outbound=outb.id_status_outbound
				LEFT JOIN m_outbound_document mout
					ON mout.id_outbound_document=outb.id_outbound_document
				WHERE 1=1'.$where;

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';
			/*
			if($row[$i]['total_received'] > 0){
				$action .= '<li>';
				$action .= '<a class="data-table-close" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-check"></i> Close PO</a>';
				$action .= '</li>';
			}
			*/

			$class = 'disabled-link';
			if($row[$i]['finish_loading'] > 0){
				$class = '';
			}

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$class.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-check"></i> Close Outbound</a>';
			$action .= '</li>';

			$sts = $this->checkStatusPicking($row[$i]['id_outbound']);

			$class = '';
			if($sts['total_scanned'] > 0){
				$class = 'disabled-link';
			}

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['id_outbound'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['id_outbound'];
            $nested[] = '<input type="checkbox" name="id[]" value="'.$row[$i]['id_outbound'].'" data-status="'.$row[$i]['nama_status'].'" data-kode="'.$row[$i]['kd_outbound'].'" data-count="'.($row[$i]['item']>1?'multi':'single').'">';
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().$post['className'].'/detail/'.$row[$i]['id_outbound'].'">'.$row[$i]['kd_outbound'].'</a>';
			$nested[] = $row[$i]['tanggal_outbound'];
			$nested[] = $row[$i]['outbound_document_name'];
			$nested[] = $row[$i]['DestinationName'];
			$nested[] = $row[$i]['nama_status'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getOutboundCode($post = array()){
		$result = array();

		$sql = 'SELECT
					id_outbound, kd_outbound
				FROM
					outbound
				WHERE
					kd_outbound LIKE \'%'.$post['query'].'%\'';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getCustomerBySearch($post = array()){
		$result = array();

		$sql = 'SELECT
					id_customer, customer_name
				FROM
					m_customer
				WHERE
					customer_name LIKE \'%'.$post['query'].'%\'';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

    public function get_outbound_barang($id_outbound) {
		$sql = 'SELECT
					*
				FROM
					outbound_barang outb
				JOIN barang brg
					ON brg.id_barang=outb.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					id_outbound=\''.$id_outbound.'\'
				ORDER BY
					outb.id ASC';

		return $this->db->query($sql);

        //return $this->db->get_where("outbound_barang", array('id_outbound' => $id_outbound));
    }

    public function get_outbound_barang2($id_outbound) {
		$sql = 'SELECT
					*
				FROM
					outbound_barang outb
				JOIN barang brg
					ON brg.id_barang=outb.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					id_outbound IN ('.$id_outbound.')
				ORDER BY
					outb.id ASC';

		return $this->db->query($sql);

        //return $this->db->get_where("outbound_barang", array('id_outbound' => $id_outbound));
    }

    public function create($data, $data2) {
        $this->db->trans_start();

        $this->db->insert($this->table, $data);
        $id_outbound = $this->db->insert_id();

        if(!empty($data2)) {
            foreach ($data2 as &$outbound_barang) {
                $outbound_barang['id_outbound'] = $id_outbound;
            }

            $this->db->insert_batch($this->table3, $data2);
        }

        $this->db->trans_complete();
		return $this->db->trans_status();
    }

    public function create_customer($data) {
        $this->db->insert($this->table4, $data);
    }

    public function update($data, $data2, $id) {

		if(isset($data['delete'])){

			$sql = 'SELECT
						pl_id
					FROM
						picking_list_outbound
					WHERE
						id_outbound=\''.$id.'\'';

			$row = $this->db->query($sql)->result_array();
			$len = count($row);

			for($i = 0; $i < $len; $i++){

				$plId = $row[$i]['pl_id'];

				$sql = 'DELETE FROM picking_list WHERE pl_id=\''.$plId.'\'';
				$this->db->query($sql);

				$sql = 'DELETE FROM picking_qty WHERE pl_id=\''.$plId.'\'';
				$this->db->query($sql);

				$sql = 'DELETE FROM picking_list_outbound WHERE pl_id=\''.$plId.'\'';
				$this->db->query($sql);

				$sql = 'DELETE FROM picking_recomendation WHERE id_picking=\''.$plId.'\'';
				$this->db->query($sql);

			}

			unset($data['delete']);

			$data['id_status_outbound'] = '1';

			$this->db->trans_start();
			$this->db->update($this->table, $data, array('id_outbound' => $id));

			//remove all outbound_barang which the id_outbound is $id
			$this->db->where("id_outbound", $id);
			$this->db->delete($this->table3);

			//add all new outbound_barang
			if(!empty($data2)) {
				foreach ($data2 as &$outbound_barang) {
					$outbound_barang['id_outbound'] = $id;
				}
				$this->db->insert_batch($this->table3, $data2);
			}

			$this->db->trans_complete();
			return $this->db->trans_status();

		}else{

			$sql = 'SELECT
						COUNT(*) total
					FROM
						picking_list_outbound
					WHERE
						id_outbound=\''.$id.'\'';

			$row = $this->db->query($sql)->row_array();

			if($row['total'] > 0){

				$result['status'] = 'ERR';
				$result['message'] = 'This outbound document already have picking document. All picking document for this outbound will
										be remove. Are you sure want to do this action ?';

				return $result;

			}else{

				$this->db->trans_start();
				$this->db->update($this->table, $data, array('id_outbound' => $id));

				//remove all outbound_barang which the id_outbound is $id
				$this->db->where("id_outbound", $id);
				$this->db->delete($this->table3);

				//add all new outbound_barang
				if(!empty($data2)) {
					foreach ($data2 as &$outbound_barang) {
						$outbound_barang['id_outbound'] = $id;
					}
					$this->db->insert_batch($this->table3, $data2);
				}

				$this->db->trans_complete();
				return $this->db->trans_status();

			}

		}
    }

	public function delete($id){
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					outbound outb
				JOIN picking_list_outbound plo
					ON plo.id_outbound=outb.id_outbound
				JOIN picking_list pl
					ON pl.pl_id=plo.pl_id
				WHERE
					outb.id_outbound=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		if($row['total'] > 0 ){

			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';

		}else{

			$this->db->delete($this->table, array('id_outbound' => $id));
			$this->db->delete($this->table3, array('id_outbound' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';

		}

        return $result;

	}

    public function delete1($id) {
        return $this->db->delete($this->table, array('id_outbound' => $id));
    }

    public function options($default = '--Pilih Kode Barang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->nama_barang ;
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if($table_name == 'm_outbound_document'){

				$r = $this->setting_model->getData();

				if(strtolower($dt['outbound_document_name']) == 'peminjaman' && $r['peminjaman'] == 'N')
					continue;
			}

            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    function get_barang_SKU($id_barang=0){
        $result = $this->db->get_where('barang',array('id_barang'=>$id_barang));
        return $result->num_rows()>0?$result->row()->sku:'';
    }

    public function getOutDocument($id){
    	
    	$doc = $this->db->get_where('m_outbound_document',array('id_outbound_document'=>$id))->row();

    	$docName = "";

    	if($doc){

    		$docName .= $doc->outbound_document_name;
    		$docName .= $doc->separator;

			switch ($doc->year) {
			    case 1:
			    	$docName .= date("y");
			        break;
			    case 2:
			    	$docName .= date("Y");
			        break;
			    default:
			    	$docName .= ''; 
			}

			if($doc->month){
				$docName .= date("m");
			}

			$seqlen = $doc->seq_length - strlen($doc->sequence);

			for($i=0;$i<$seqlen;$i++){
				$docName .= '0';
			}

			$docName .= $doc->sequence;

    	}

    	if(empty($docName)){
    		return false;
    	}

    	return $docName;
    }

    public function addOutDocument($data=array()){

    	$sql = "UPDATE m_outbound_document SET sequence = sequence + 1 WHERE id_outbound_document = ".$data['id_outbound_document'];
    	$this->db->query($sql);

    }

}

?>

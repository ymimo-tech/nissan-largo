<?php
class referensi_kategori_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'kategori_report';


    private function data($condition = array()) {
        $this->db->from($this->table);
        //$this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {

        $condition['id_kategori_report'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    

    public function data_table() {
        // Filtering
        $condition = array();
        $nama_kategori_report= $this->input->post("nama_kategori_report");
        if(!empty($nama_kategori_report)){
            $condition["nama_kategori_report like '%$nama_kategori_report%'"] = null;
        }

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_kategori_report;
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">';
                                
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_kategori_report/edit/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_kategori_report/delete/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            $rows[] = array(
                    'primary_key' =>$value->id_kategori_report,
                    'owner' => $value->nama_kategori_report,
                    'aksi' => $action
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

   

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_kategori_report' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_kategori_report' => $id));
    }
    
    public function options_filter($default = '--Choose Owner--', $key = '') {
        $this->db->order_by('id_kategori_report');
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_kategori_report] = $row->nama_kategori_report;
        }
        return $options;
    }


    public function options($default = '--Choose Categories 2--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_kategori_report] = $row->nama_kategori_report;
        }
        return $options;
    }
    
}

?>
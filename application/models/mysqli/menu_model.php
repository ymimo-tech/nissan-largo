<?php

/**
 * Description of menu_model
 *
 * @author Warman Suganda
 */
class menu_model extends CI_Model {

    public $limit;
    public $offset;
    private $table = 'hr_menu';
    private $table2 = "hr_grup_menu";

    public function __construct() {
        parent::__construct();
    }

    private function data($condition = array()) {
        $this->db->from($this->table . " a");
        $this->db->join($this->table2 . " b", "a.kd_grup_menu = b.kd_grup_menu");
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_all() {
        return $this->data()->get();
    }

    public function get_by_id($id) {
        $condition['kd_menu'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function parsing_data() {
        // Filtering
        $condition = array();

        // List Data
        $this->db->order_by('menu_urutan');
        $data = $this->data($condition)->get();

        $rows = array();

        foreach ($data->result() as $value) {
            $parent_id = !empty($value->kd_parent) ? $value->kd_parent : 0;
            $rows[$value->kd_grup_menu][$parent_id][$value->kd_menu] = array(
                'kd_menu' => $value->kd_menu,
                'kd_grup_menu' => $value->kd_grup_menu,
                'nama_menu' => $value->nama_menu,
                'deskripsi' => $value->deskripsi,
                'menu_urutan' => $value->menu_urutan,
                'url' => $value->url,
                'kd_parent' => $value->kd_parent
            );
        }
        return $rows;
    }

    public function get_urutan_menu($condition = array()) {
        $this->db->select('MAX(menu_urutan) AS last_menu_urutan', false);
        $this->db->from($this->table);

        $this->db->where_condition($condition);

        $urutan = $this->db->get();
        if ($urutan->num_rows() > 0) {
            return $urutan->row()->last_menu_urutan;
        } else {
            return 1;
        }
    }

    public function create($data) {
        $this->db->trans_begin();

        $urutan = $this->get_urutan_menu(array('kd_grup_menu' => $data['kd_grup_menu'], 'kd_parent' => !empty($data['kd_parent']) ? $data['kd_parent'] : null));
        $data['menu_urutan'] = $urutan+1;

        $this->db->insert($this->table, $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function update($data, $id) {
        $this->db->trans_begin();

        $this->db->update($this->table, $data, array('kd_menu' => $id));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('kd_menu' => $id));
    }

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }

    /* Heubeul */

    function get_all_menu() {
        $grup_c = $this->input->post('grup_menu');
        $filter = "";
        if ($grup_c != '') {
            $filter = "where hr_menu.kd_grup_menu='$grup_c'";
        }
        $query = "select hr_menu.kd_menu, hr_menu.url, hr_menu.kd_grup_menu, hr_menu.nama_menu,
        hr_grup_menu.kd_grup_menu, hr_grup_menu.nama_grup_menu
        from hr_menu
        left join hr_grup_menu on hr_menu.kd_grup_menu= hr_grup_menu.kd_grup_menu
        $filter
        ORDER BY hr_menu.kd_grup_menu, hr_menu.menu_urutan";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function get_detail_menu($noid) {
        $query = "
    select 
    hr_menu.kd_menu, 
    hr_menu.kd_grup_menu, 
    hr_menu.nama_menu, 
    hr_menu.url, 
	hr_menu.menu_urutan, 
	hr_menu.menu_level, 
	hr_grup_menu.kd_grup_menu,
    hr_grup_menu.nama_grup_menu
    from hr_menu
    left join hr_grup_menu
    on 
    hr_menu.kd_grup_menu= hr_grup_menu.kd_grup_menu
    where kd_menu='$noid'
    ";


        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function subMenu($noid = '') {
        $idChild = "";
        $query = $this->db->query("
    select 
    hr_menu.kd_parent, hr_menu.kd_menu from hr_menu 
    where hr_menu.kd_menu='$noid'
    ");
        foreach ($query->result() as $row) {
            $idChild = $row->kd_parent;
        }
        if ($idChild == '') {
            $idChild = 0;
        }

        $query2 = ("select 
    hr_menu.kd_menu, 
    hr_menu.kd_grup_menu, 
    hr_menu.nama_menu, 
    hr_menu.url, 
	hr_menu.menu_urutan, 
	hr_menu.menu_level, 
	hr_grup_menu.kd_grup_menu,
    hr_grup_menu.nama_grup_menu
    from hr_menu
    left join hr_grup_menu
    on 
    hr_menu.kd_grup_menu= hr_grup_menu.kd_grup_menu
    where kd_menu='$idChild' ORDER BY  hr_menu.nama_menu ASC ");

        $q = $this->db->query($query2);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function data_menu_all() {
        $query = "
    select 
    hr_menu.kd_menu, 
    hr_menu.kd_grup_menu, 
    hr_menu.nama_menu, 
    hr_menu.url, 
	hr_menu.menu_urutan, 
	hr_menu.menu_level, 
	hr_grup_menu.kd_grup_menu,
    hr_grup_menu.nama_grup_menu
    from hr_menu
    left join hr_grup_menu
    on 
    hr_menu.kd_grup_menu= hr_grup_menu.kd_grup_menu ORDER BY hr_menu.nama_menu ASC    ";


        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function retrieve_grup_menu() {
        $query = "
    select 
    hr_grup_menu.nama_grup_menu, 
    hr_grup_menu.kd_grup_menu 
    from hr_grup_menu
    ";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function add_data_menu_mdl() {
        $nama = $this->input->post('txtnamamenu');
        $url = $this->input->post('txturl');
        $grup = $this->input->post('grup_menu');
        $lev = $this->input->post('txtlevel');
        $ur = $this->input->post('txturutan');
        $parent = $this->input->post('select_sub_menu');
        $data_menu = array
            (
            'kd_grup_menu' => $grup,
            'nama_menu' => $nama,
            'url' => $url,
            'kd_grup_menu' => $grup,
            'menu_level' => $grup,
            'menu_urutan' => $ur,
            'kd_parent' => $parent
        );
        $this->db->insert('hr_menu', $data_menu);

        $q0 = $this->db->query("select kd_menu from hr_menu where nama_menu='$nama'");
        foreach ($q0->result_array() as $row) {
            $id_menu = $row['kd_menu'];
        }

        $q1 = $this->db->query("select user_id,grup_id from hr_user ");
        foreach ($q1->result_array() as $row) {
            $user_id = $row['user_id'];
            $roles_id = $row['grup_id'];

            $this->db->query(
                    "insert into hr_user_akses (kd_menu,kd_grup,userid,is_view,is_add,is_edit,is_del,is_import,is_print,is_apr) values ('$id_menu','$roles_id','$user_id','d','d','d','d','d','d','d')");
        }

        $q1 = $this->db->query("select hr_roles.grup_id from hr_roles  group by grup_id");
        foreach ($q1->result_array() as $row) {
            $gr = $row['grup_id'];
            $this->db->query("insert into hr_roles (menu_id,grup_id,is_view,is_add,is_edit,is_delete,is_import,is_print,is_approve) values ('$id_menu','$gr','f','f','f','f','f','f','f')");
        }
    }

    function update_menu_mdl() {

        $nama_menu = $this->input->post('txtnamamenu');
        $url = $this->input->post('txturl');
        $id = $this->input->post('id');
        $menu_urutan = $this->input->post('txturutan');
        $grup_menu = $this->input->post('grup_menu');
        $parent = $this->input->post('select_sub_menu');

        $data_menu =
                array(
                    'nama_menu' => $nama_menu,
                    'url' => $url,
                    'menu_urutan' => $menu_urutan,
                    'kd_grup_menu' => $grup_menu,
                    'kd_parent' => $parent,
        );

        $this->db->where('kd_menu', $id);
        $this->db->update('hr_menu', $data_menu);
    }

    function delete_menu_mdl($id) {
        $this->db->query("delete from hr_roles where menu_id='$id'");
        $this->db->query("delete from hr_menu where kd_menu='$id'");
    }

    function get_url_by_name($name) {
        $this->db->select('*');
        $this->db->where("nama_menu = '$name'");
        $query = $this->db->get('hr_menu');

        return $query->result_array();
    }

}

/* End of file menu_model.php */
/* Location: ./application/models/menu_model.php */


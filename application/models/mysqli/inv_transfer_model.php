<?php
class inv_transfer_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    // private $table  = 'm_loc';
    // private $table2 = 'receiving_barang';
    // private $table3 = 'barang';
    // private $table4 = 'satuan';

    private $table  = 'int_transfer';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'receiving_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $table8 = 'supplier_barang';
    private $po_barang = 'purchase_order_barang';
    private $kurs   = 'kurs';
    private $loc  = 'm_loc';

    private function data($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");
        /*$tanggal_awal = $this->input->post('tanggal_awal_bbk');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbk');*/

        if(!empty($loc_name)){
            $condition["a.loc_name like '%$loc_name%'"]=null;
        }
        if(!empty($loc_desc)){
            $condition["b.loc_desc like '%$loc_desc%'"]=null;
        }

       /* if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.tanggal_do >= '$tanggal_awal'"] = null ;
            $condition["a.tanggal_do <= '$tanggal_akhir'"] = null;
        }*/


        $this->db->select('i.transfer_id, transfer_code, b.loc_name as loc_from, c.loc_name as loc_to, transfer_type, loc_id_old, loc_id_new');
        $this->db->from('int_transfer i');
        $this->db->join('m_loc b','i.loc_id_old = b.loc_id','left');
        $this->db->join('m_loc c','i.loc_id_new = c.loc_id','left');
        $this->db->order_by('transfer_id ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }
	
	public function getSerialNumber($post = array()){
		$result = array();
		
		$where = '';
		
		$sql = 'SELECT 
					kd_unik 
				FROM 
					receiving_barang
				WHERE 
					LOWER(kd_unik) LIKE \'%'.$post['query'].'%\''.$where;
					
		$row = $this->db->query($sql)->result_array();
		
		$result['status'] = 'OK';
		$result['result'] = $row;
		
		return $result;
	}
	
	public function getLocNameById($id){
		$result = '';
		
		$sql = 'SELECT 
					loc_name 
				FROM 
					mc_loc 
				WHERE 
					loc_id=\''.$id.'\'';
					
		$row = $this->db->query($sql)->row_array();
		$result = $row['loc_name'];
		
		return $result;
	}
	
	public function getItemNameById($id){
		$result = '';
		
		$sql = 'SELECT 
					nama_barang 
				FROM 
					barang 
				WHERE 
					id_barang=\''.$id.'\'';
					
		$row = $this->db->query($sql)->row_array();
		$result = $row['nama_barang'];
		
		return $result;
	}
	
	public function getListRaw($post = array()){
		$result = array();
		
		$where = '';
		
		if(!empty($post['id_barang']))
			$where .= sprintf(' AND brg.id_barang = \'%s\' ', $post['id_barang']);
		
		if(!empty($post['loc_id']))
			$where .= sprintf(' AND ( itd.loc_id_old = \'%s\' OR itd.loc_id_new = \'%s\') ', $post['loc_id'], $post['loc_id']);
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(pick_time) BETWEEN \'%s\' AND \'%s\' ', 
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}
		
		if(!empty($post['sn']))
			$where .= sprintf(' AND rcvbrg.kd_unik = \'%s\' ', $post['sn']);
		
		$sql = 'SELECT 
					brg.kd_barang, nama_barang, itd.kd_unik, fr.loc_name AS loc_from,
					DATE_FORMAT(pick_time, \'%d\/%m\/%Y %H:%i\') AS pick_time,
					upick.user_name AS pick_by,
					t.loc_name AS loc_to,
					DATE_FORMAT(put_time, \'%d\/%m\/%Y %H:%i\') AS put_time,
					upick.user_name AS put_by
				FROM 
					int_transfer_detail itd
				JOIN (select kd_unik, id_barang from receiving_barang) rcvbrg 
					ON rcvbrg.kd_unik=itd.kd_unik 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				JOIN m_loc fr
					ON fr.loc_id=itd.loc_id_old 
				JOIN hr_user upick 
					ON upick.user_id=itd.user_id_pick
				JOIN m_loc t
					ON t.loc_id=itd.loc_id_new 
				JOIN hr_user uput 
					ON uput.user_id=itd.user_id_put
				WHERE 1=1 
				AND 
					itd.process_name=\''.$this->config->item('inv_transfer_name').'\'
				'.$where.' 
				ORDER BY 
					brg.kd_barang, rcvbrg.kd_unik ASC';
				
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'kd_unik',
			4 => 'loc_from',
			5 => 'pick_by',
			6 => 'pick_time',
			7 => 'loc_to',
			8 => 'put_by',
			9 => 'put_time',
		);
		
		$where = '';
		$join = '';
		if(!empty($post['id_barang'])){
			$where .= sprintf(' AND brg.id_barang = \'%s\' ', $post['id_barang']);
			$join .= "	
						LEFT JOIN barang brg ON brg.id_barang = rcvbrg.id_barang";
		}
		
		if(!empty($post['loc_id'])){
			$where .= sprintf(' AND ( itd.loc_id_old = \'%s\' OR itd.loc_id_new = \'%s\') ', $post['loc_id'], $post['loc_id']);
			$join .= "	Left JOIN m_loc fr
							ON fr.loc_id=itd.loc_id_old 
						
						LEFT JOIN m_loc t
							ON t.loc_id=itd.loc_id_new 
						";
		}
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(pick_time) BETWEEN \'%s\' AND \'%s\' ', 
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}
		
		$sql = "SELECT 
					COUNT(itd.kd_unik) AS total 
				FROM 
					int_transfer_detail itd		
				JOIN (select kd_unik, id_barang from receiving_barang) rcvbrg ON rcvbrg.kd_unik = itd.kd_unik		
				".$join."
				WHERE 1=1
				AND 
					itd.process_name= '".$this->config->item('inv_transfer_name')."' ". $where;
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					brg.kd_barang, nama_barang, itd.kd_unik, fr.loc_name AS loc_from,
					DATE_FORMAT(pick_time, \'%d\/%m\/%Y %H:%i\') AS pick_time,
					upick.user_name AS pick_by,
					t.loc_name AS loc_to,
					DATE_FORMAT(put_time, \'%d\/%m\/%Y %H:%i\') AS put_time,
					upick.user_name AS put_by
				FROM 
					int_transfer_detail itd
				JOIN (select kd_unik, id_barang from receiving_barang) rcvbrg 
					ON rcvbrg.kd_unik=itd.kd_unik 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				JOIN m_loc fr
					ON fr.loc_id=itd.loc_id_old 
				JOIN hr_user upick 
					ON upick.user_id=itd.user_id_pick
				JOIN m_loc t
					ON t.loc_id=itd.loc_id_new 
				JOIN hr_user uput 
					ON uput.user_id=itd.user_id_put
				WHERE 1=1
				AND 
					itd.process_name=\''.$this->config->item('inv_transfer_name').'\''.$where;
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['loc_from'];
			$nested[] = $row[$i]['pick_by'];
			$nested[] = $row[$i]['pick_time'];
			$nested[] = $row[$i]['loc_to'];
			$nested[] = $row[$i]['put_by'];
			$nested[] = $row[$i]['put_time'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function getItem($post = array()){
		$result = array();
		
		$sql = 'SELECT * FROM barang 
				ORDER BY kd_barang ASC';
				
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getLocation($post = array()){
		$result = array();
		
		$sql = 'SELECT * FROM m_loc 
				WHERE 
					loc_name IS NOT NULL 
				ORDER BY loc_id ASC';
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}

    private function data_print_barcode($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");

        $this->db->select('*');
        $this->db->from($this->table4 . ' a');
        $this->db->join($this->table5 . ' b',' a.id_barang = b.id_barang','left');
        $this->db->join($this->table  . ' c',' c.loc_id = a.loc_id');
        $this->db->order_by('a.loc_id_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_unik($condition = array()) {
        $condition['loc_id'] = NULL;
        $this->db->from($this->table2  . ' a');
        $this->db->order_by('a.kd_unik ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $transfer_id = $this->input->post("transfer_id");

        if(!empty($transfer_id)){
            $condition["a.transfer_id"]=$transfer_id;
        }

        //-----------end filtering-------------------

        $this->db->select('*, i.loc_name as loc_from, j.loc_name as loc_to');
        $this->db->from('int_transfer_detail'  . ' a');
        $this->db->join('int_transfer' . ' b',' a.transfer_id = b.transfer_id','left');//receiving_barang
        $this->db->join('receiving_barang' . ' c',' a.kd_unik = c.kd_unik','left');//barang
        $this->db->join('barang' . ' d',' d.id_barang = c.id_barang','left');//barang
        $this->db->join('m_loc i','i.loc_id = a.loc_id_old','left');
        $this->db->join('m_loc j','j.loc_id = a.loc_id_new','left');
        $this->db->order_by('a.kd_unik');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['i.transfer_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['a.loc_id_barang'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        $condition = array();
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->transfer_id;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu"> ';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('inv_transfer/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('inv_transfer/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul></div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->transfer_id,
                    'transfer_code' =>  anchor(null, $value->transfer_code, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_inv_transfer', 'data-source' => base_url('inv_transfer/get_detail_inv_transfer/' . $id)))  ,
                    'transfer_type' => $value->transfer_type,
                    'loc_from' => $value->loc_from,
                    'loc_to' => $value->loc_to,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->transfer_id,
                    'transfer_code' => $value->transfer_code,
                    'transfer_type' => $value->transfer_type,
                    'loc_from' => $value->loc_from,
                    'loc_to' => $value->loc_to,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail() {
        $transfer_id = $this->input->post('transfer_id');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->transfer_detail_id;
            $action = '';
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';


            if ($this->access_right->otoritas('edit')) {
                
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('inv_transfer/edit_detail/'.$id.'/' .$value->kd_unik))) . ' ';
                $action .= '</li>';
                
            }

            $action .= '</ul>
                    </div>';
            
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $id,
                    'nama_barang' => $value->nama_barang,
                    'kd_unik' => $value->kd_unik,
                    'loc_to' => $value->loc_to,
                    'loc_from' => $value->loc_from,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_unik' => $value->kd_unik,
                    'nama_barang' => $value->nama_barang,
                    'aksi' => ''
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_do >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_do <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->loc_id;
            $action = '';
                $rows[] = array(
                    'tanggal_do' => hgenerator::switch_tanggal($value->tanggal_do),
                    'kd_do' => $value->kd_do,
                    'proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                );
        }
        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->loc_id_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('transfer_id' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('transfer_detail_id' => $id));
    }

    public function delete($id) {
        $this->db->delete($this->table4, array('loc_id' => $id));
        return $this->db->delete($this->table, array('loc_id' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }


    public function options($default = '--Pilih Kode bbk--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->loc_id] = $row->kd_do ;
        }
        return $options;
    }

    public function options_unik_kode($default = '--Pilih Unik Kode--', $key = '') {
        $data = $this->data_unik()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->loc_id_barang] = $row->kd_unik ;
        }
        return $options;
    }




}

?>

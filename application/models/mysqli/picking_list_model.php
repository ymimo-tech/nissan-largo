<?php
class picking_list_model extends CI_Model {

	CONST MONTHLY = 'MONTHLY';
	CONST YEARLY = 'YEARLY';

	CONST FIFO = 'FIFO';
	CONST FEFO = 'FEFO';
	CONST FIFOFEFO = 'FIFOFEFO';
	CONST FEFOFIFO = 'FEFOFIFO';

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'picking_list';
    private $table2 = 'pl_do';
    private $table4 = 'do_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $do = 'do';

    public function get_pl_code(){
        $this->db->select('pre_code_pl,inc_pl');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_pl.$result->inc_pl;
    }

    public function add_pl_code(){
        $this->db->set('inc_pl',"inc_pl+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

    private function data($condition = array()) {
        // ===========Filtering=================
        //$condition = array();
        $pl_name= $this->input->post("pl_name");
        $tanggal_awal = $this->input->post('tanggal_awal_pl');
        $tanggal_akhir = $this->input->post('tanggal_akhir_pl');

        if(!empty($pl_name)){
            $condition["a.pl_name like '%$pl_name%'"]=null;
        }
        if(!empty($nama_customer)){
            $condition["a.nama_customer like '%$nama_customer%'"]=null;
        }

        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.pl_date >= '$tanggal_awal'"] = null ;
            $condition["a.pl_date <= '$tanggal_akhir'"] = null;
        }


        $this->db->from($this->table  . ' a');
        $this->db->join('do','do.pl_id = a.pl_id','left');
        $this->db->join('toko t','t.id_toko = do.id_toko','left');
        $this->db->order_by('a.pl_name DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function getStatus(){
		$result = array();

		$sql = 'SELECT
					*
				FROM
					m_status_picking';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

	public function getScannedList($post = array()){
		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_unik',
			2 => 'tgl_exp',
			3 => 'tgl_in',
			4 => 'loc_name',
			5 => 'user_name'
		);

		$where = '';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					receiving_barang rcvbrg
				JOIN barang brg
					ON brg.id_barang=rcvbrg.id_barang
				LEFT JOIN m_loc loc
					ON loc.loc_id=rcvbrg.loc_id
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				LEFT JOIN hr_user hu
					ON hu.user_id=rcvbrg.user_id_picking
				WHERE
					rcvbrg.id_barang=\''.$post['id_barang'].'\'
				AND
					pl_id=\''.$post['pl_id'].'\'';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					kd_unik, IFNULL(DATE_FORMAT(tgl_exp, \'%d\/%m\/%Y\'), \'-\') AS tgl_exp,
					IFNULL(DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\'), \'-\') AS tgl_in, loc_name,
					IFNULL(user_name, \'-\') AS user_name
				FROM
					receiving_barang rcvbrg
				JOIN barang brg
					ON brg.id_barang=rcvbrg.id_barang
				LEFT JOIN m_loc loc
					ON loc.loc_id=rcvbrg.loc_id
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				LEFT JOIN hr_user hu
					ON hu.user_id=rcvbrg.user_id_picking
				WHERE
					rcvbrg.id_barang=\''.$post['id_barang'].'\'
				AND
					pl_id=\''.$post['pl_id'].'\'';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['user_name'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$sql = 'SELECT
					pick.pl_id, pl_name, outb.id_outbound, kd_outbound, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS pl_date,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					DestinationName,DestinationAddressL1,DestinationAddressL2,DestinationAddressL3,
					alamat_supplier, telepon_supplier, cp_supplier, address, outb.phone,
					IFNULL(remark, \'-\') AS remark, IFNULL(DATE_FORMAT(pl_start, \'%d\/%m\/%Y %H:%i\'), \'-\') AS pl_start,
					IFNULL(DATE_FORMAT(pl_finish, \'%d\/%m\/%Y %H:%i\'), \'-\') AS pl_finish,
					IFNULL(DATE_FORMAT(shipping_start, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_start,
					IFNULL(DATE_FORMAT(shipping_finish, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_finish,
					pick.id_status_picking, user_name, outb.id_outbound_document,
					(
						SELECT
							nama_status
						FROM
							m_status_picking
						WHERE
							id_status_picking = (
								CASE
									WHEN pl_start IS NULL
										THEN \'1\'
									WHEN pl_start IS NOT NULL AND pl_finish IS NULL
										THEN \'2\'
									WHEN pl_start IS NOT NULL AND pl_finish IS NOT NULL AND shipping_start IS NULL
										THEN \'3\'
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NULL
										THEN \'4\'
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NOT NULL
										THEN \'5\'
								END
							)
					) AS nama_status
				FROM
					picking_list pick
				JOIN picking_list_outbound plo
					ON plo.pl_id=pick.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_status_picking mstat
					ON mstat.id_status_picking=pick.id_status_picking
				LEFT JOIN shipping_picking shippick
					ON shippick.pl_id=pick.pl_id
				LEFT JOIN shipping shipp
					ON shipp.shipping_id=shippick.shipping_id
				LEFT JOIN hr_user hu
					ON hu.user_id=pick.user_id
				WHERE
					pick.pl_id=\''.$post['pl_id'].'\'
				GROUP BY
					pick.pl_id';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function getDetail2($post = array()){
		$result = array();

		$sql = 'SELECT
					pick.pl_id, pl_name, outb.id_outbound, kd_outbound, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS pl_date,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name,
					DestinationName,DestinationAddressL1,DestinationAddressL2,DestinationAddressL3,
					alamat_supplier, telepon_supplier, cp_supplier, address, outb.phone,
					IFNULL(remark, \'-\') AS remark, IFNULL(DATE_FORMAT(pl_start, \'%d\/%m\/%Y %H:%i\'), \'-\') AS pl_start,
					IFNULL(DATE_FORMAT(pl_finish, \'%d\/%m\/%Y %H:%i\'), \'-\') AS pl_finish,
					IFNULL(DATE_FORMAT(shipping_start, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_start,
					IFNULL(DATE_FORMAT(shipping_finish, \'%d\/%m\/%Y %H:%i\'), \'-\') AS shipping_finish,
					pick.id_status_picking, user_name, outb.id_outbound_document,
					(
						SELECT
							nama_status
						FROM
							m_status_picking
						WHERE
							id_status_picking = (
								CASE
									WHEN pl_start IS NULL
										THEN \'1\'
									WHEN pl_start IS NOT NULL AND pl_finish IS NULL
										THEN \'2\'
									WHEN pl_start IS NOT NULL AND pl_finish IS NOT NULL AND shipping_start IS NULL
										THEN \'3\'
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NULL
										THEN \'4\'
									WHEN shipping_start IS NOT NULL AND shipping_finish IS NOT NULL
										THEN \'5\'
								END
							)
					) AS nama_status
				FROM
					picking_list pick
				JOIN picking_list_outbound plo
					ON plo.pl_id=pick.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				LEFT JOIN m_status_picking mstat
					ON mstat.id_status_picking=pick.id_status_picking
				LEFT JOIN shipping_picking shippick
					ON shippick.pl_id=pick.pl_id
				LEFT JOIN shipping shipp
					ON shipp.shipping_id=shippick.shipping_id
				LEFT JOIN hr_user hu
					ON hu.user_id=pick.user_id
				WHERE
					pick.pl_id=\''.$post['pl_id'].'\'';

		$result = $this->db->query($sql);

		return $result;
	}

	public function getWidget(){
		$result = array();
		$today = date('Y-m-d');

		$sql = 'SELECT
					IFNULL(SUM(total_item), 0) AS total_item, IFNULL(SUM(total_qty), 0) AS total_qty,
					IFNULL(SUM(total_picking), 0) AS total_picking, IFNULL(SUM(total_qty_picking), 0) AS total_qty_picking,
					IFNULL(SUM(total_loading), 0) AS total_loading, IFNULL(SUM(total_qty_loading), 0) AS total_qty_loading,
					-- TIME_FORMAT(
						-- SEC_TO_TIME(
							AVG(
								TIME_TO_SEC(
									TIMEDIFF(
										shipping_finish,
										pl_date
									)
								)
							)
						-- ),
					-- \'%H:%i:%s\') AS average
					AS average
				FROM
					picking_list pl
				LEFT JOIN shipping_picking shippick
					ON shippick.pl_id=pl.pl_id
				LEFT JOIN shipping shipp
					ON shipp.shipping_id=shippick.shipping_id
				LEFT JOIN
				(
					SELECT
						table_1.pl_id, COUNT(total_item) AS total_item, SUM(total_qty) AS total_qty
					FROM
					(
						SELECT
							pqty.pl_id, COUNT(*) AS total_item, SUM(qty) AS total_qty
						FROM
							picking_qty pqty
						JOIN barang brg
							ON brg.id_barang=pqty.id_barang
						GROUP BY
							pqty.id_barang, pl_id
					) AS table_1
					GROUP BY
						pl_id
				) AS rec_qty
					ON rec_qty.pl_id=pl.pl_id
				LEFT JOIN
				(
					SELECT
						table_2.pl_id, COUNT(total_picking) AS total_picking, SUM(total_qty_picking) AS total_qty_picking
					FROM
					(
						SELECT
							pqty.pl_id, COUNT(*) AS total_picking, SUM(qty) AS total_qty_picking
						FROM
							picking_qty pqty
						JOIN barang brg
							ON brg.id_barang=pqty.id_barang
						JOIN picking_list spl
							ON spl.pl_id=pqty.pl_id
						WHERE
							spl.pl_start IS NULL
						AND
							spl.pl_finish IS NULL
						GROUP BY
							pqty.id_barang, pl_id
					) AS table_2
					GROUP BY
						pl_id
				) AS rec_picking
					ON rec_picking.pl_id=pl.pl_id
				LEFT JOIN
				(
					SELECT
						table_3.pl_id, COUNT(total_loading) AS total_loading, SUM(total_qty_loading) AS total_qty_loading
					FROM
					(
						SELECT
							pqty.pl_id, COUNT(*) AS total_loading, SUM(qty) AS total_qty_loading
						FROM
							picking_qty pqty
						JOIN barang brg
							ON brg.id_barang=pqty.id_barang
						JOIN picking_list spl
							ON spl.pl_id=pqty.pl_id
						WHERE
							spl.pl_start IS NOT NULL
						AND
							spl.pl_finish IS NOT NULL
						GROUP BY
							pqty.id_barang, pl_id
					) AS table_3
					GROUP BY
						pl_id
				) AS rec_loading
					ON rec_loading.pl_id=pl.pl_id
				WHERE
					DATE(pl_date) = \''.$today.'\'';

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

	public function insertRelationTable2($post = array()){
		$result = array();

		$sql = 'INSERT INTO picking_list_outbound(
					pl_id,
					id_outbound
				)VALUES(
					\''.$post['pl_id'].'\',
					\''.$post['id_outbound'].'\'
				)';

		$q = $this->db->query($sql);

		return $result;
	}

	public function insertRelationTable($post = array()){
		$result = array();

		$sql = 'DELETE FROM picking_list_outbound WHERE pl_id=\''.$post['pl_id'].'\'';
		$this->db->query($sql);

		$sql = 'INSERT INTO picking_list_outbound(
					pl_id,
					id_outbound
				)VALUES(
					\''.$post['pl_id'].'\',
					\''.$post['id_outbound'].'\'
				)';

		$q = $this->db->query($sql);

		return $result;
	}

	public function closePicking($post = array()){
		$result = array();

		$sql = 'SELECT
					id_status_picking
				FROM
					picking_list
				WHERE
					pl_id=\''.$post['pl_id'].'\'';

		$row = $this->db->query($sql)->row_array();
		if($row['id_status_picking'] > 2){

			$result['status'] = 'ERR';
			$result['message'] = 'Picking already finish.';

		}else{
			$rectime = date('Y-m-d H:i:s');

			$sql = 'UPDATE
						picking_list
					SET
						pl_status=\'1\'
						id_status_picking=\'3\',
						pl_finish=\''.$rectime.'\'
					WHERE
						pl_id=\''.$post['pl_id'].'\'';

			$q = $this->db->query($sql);

			if($q){
				$result['status'] = 'OK';
				$result['message'] = 'Finish picking success';
			}else{
				$result['status'] = 'ERR';
				$result['message'] = 'Finish picking failed';
			}

		}

		return $result;
	}

	public function getPlCode($post = array()){
		$result = array();

		$sql = 'SELECT
					pl_id, pl_name
				FROM
					picking_list
				WHERE
					pl_name LIKE \'%'.$post['query'].'%\'';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

    private function data_list_do($condition = array()) {
        // ===========Filtering=================
        $condition["pl_id"]=null;


        $this->db->from($this->do  . ' a');
        $this->db->order_by('a.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

	public function updateOutboundStatus($id){
		$result = array();

		$sql = 'UPDATE
					outbound
				SET
					id_status_outbound=\'3\'
				WHERE
					id_outbound=\''.$id.'\'';

		$this->db->query($sql);

		return $result;
	}

	public function getEdit($post = array()){
		$result = array();

		$sql = 'SELECT
					pl.pl_id, outb.id_outbound, kd_outbound, pl_name, DATE_FORMAT(pl_date, \'%d\/%m\/%Y\') AS pl_date
				FROM
					picking_list pl
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				WHERE
					pl.pl_id=\''.$post['pl_id'].'\'';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function update_pl_qty($id,$id_barang,$qty,$batch){
        $condition['pl_id'] = $id;
        $condition['id_barang'] = $id_barang;

        $this->db->from('picking_qty pq');
        $this->db->where($condition);
        $cek_eksis = $this->db->get();

        if($cek_eksis->num_rows() > 0){

            $update = array(
				'qty' 		=> $qty,
				'batch'		=> $batch
			);
            $this->db->where($condition);
            $this->db->update('picking_qty', $update);

        }else{

            $insert = array(
				'pl_id' 	=> $id,
                'id_barang' => $id_barang,
                'qty' 		=> $qty,
				'batch'		=> $batch
			);

            $this->db->insert('picking_qty', $insert);

        }
    }

	public function getItems($post = array()){
		$result = array();

		$sql = 'SELECT
					pick.pl_id, pq.id_barang, CONCAT(nama_barang, \' - \', kd_barang) AS nama_barang, kd_barang,
					qty, IFNULL(nama_satuan, \'\') AS nama_satuan
				FROM
					picking_qty pq
				JOIN picking_list pick
					ON pick.pl_id=pq.pl_id
				JOIN barang brg
					ON brg.id_barang=pq.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					pick.pl_id=\''.$post['pl_id'].'\'
				AND
					qty != \'0\'';

		$row = $this->db->query($sql)->result_array();
		$len = count($row);

		for($i = 0; $i < $len; $i++){

			$locations = array();

			$sql = 'SELECT
						loc_name
					FROM
						picking_recomendation pr
					JOIN m_loc loc
						ON loc.loc_id=pr.loc_id
					WHERE
						id_picking=\''.$post['pl_id'].'\'
					AND
						id_barang = \''.$row[$i]['id_barang'].'\'
					GROUP BY
						loc_name
					ORDER BY
						loc_name ASC
					';

			$rows = $this->db->query($sql)->result_array();
			$lens = count($rows);

			for($j = 0; $j < $lens; $j++){
				array_push($locations, $rows[$j]['loc_name']);
			}

			$row[$i]['locations'] = implode(',', $locations);
		}

		$result['data'] = $row;

		return $result;
	}

	public function setRecomendation($post = array()){
		$result = array();

		//EDIT THIS METHOD CAREFULY

		$sql = 'SELECT
					brg.id_barang, CONCAT(nama_barang, \' - \', kd_barang) AS nama_barang, kd_barang, qty,
					has_batch, shipment_type, fifo_period, nama_satuan, batch
				FROM
					barang brg
				JOIN picking_qty pq
					ON pq.id_barang=brg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					pl_id=\''.$post['pl_id'].'\'';

		$row = $this->db->query($sql)->result_array();
		$len = count($row);

		$sql = 'DELETE FROM picking_recomendation WHERE id_picking=\''.$post['pl_id'].'\'';
		$this->db->query($sql);

		for($i = 0; $i < $len; $i++){

			$idBarang = $row[$i]['id_barang'];
			$shipmentType = $row[$i]['shipment_type'];
			$periode = $row[$i]['fifo_period'];
			$hasBatch = $row[$i]['has_batch'];

			if($hasBatch == 1){

				$sql = 'SELECT
							id_barang, kd_unik, rcvbrg.loc_id, loc_name
						FROM
							receiving_barang rcvbrg
						JOIN m_loc loc
							ON loc.loc_id=rcvbrg.loc_id
						WHERE
							id_barang=\''.$idBarang.'\'
						AND
							kd_batch = \''.$row[$i]['batch'].'\'
						AND
							id_qc = \'1\'
						AND
							loc.loc_type NOT IN('.$this->config->item('hardcode_location').')
						ORDER BY
							rcvbrg.tgl_in ASC
						LIMIT ' . $row[$i]['qty'];

				$rows = $this->db->query($sql)->result_array();
				$lens = count($rows);

				for($j = 0; $j < $lens; $j++){

					$sql = 'INSERT INTO picking_recomendation(
								id_picking,
								kd_unik,
								id_barang,
								loc_id
							)VALUES(
								\''.$post['pl_id'].'\',
								\''.$rows[$j]['kd_unik'].'\',
								\''.$rows[$j]['id_barang'].'\',
								\''.$rows[$j]['loc_id'].'\'
							)';

					$this->db->query($sql);

				}

			}else{

				$orderBy = '';
				$groupBy = '';

				if($periode == self::MONTHLY && $shipmentType == self::FIFO)
					$groupBy .= ' GROUP BY YEAR(tgl_in), MONTH(tgl_in), loc_id, kd_unik ';

				if($periode == self::MONTHLY && $shipmentType == self::FEFO)
					$groupBy .= ' GROUP BY YEAR(tgl_exp), MONTH(tgl_exp), loc_id, kd_unik ';

				if($periode == self::YEARLY && $shipmentType == self::FIFO)
					$groupBy .= ' GROUP BY YEAR(tgl_in), loc_id, kd_unik ';

				if($periode == self::YEARLY && $shipmentType == self::FEFO)
					$groupBy .= ' GROUP BY YEAR(tgl_exp), loc_id, kd_unik ';

				if($periode == self::MONTHLY && $shipmentType == self::FIFOFEFO)
					$groupBy .= ' GROUP BY YEAR(tgl_in), MONTH(tgl_in), YEAR(tgl_exp), MONTH(tgl_exp), loc_id, kd_unik ';

				if($periode == self::MONTHLY && $shipmentType == self::FEFOFIFO)
					$groupBy .= ' GROUP BY YEAR(tgl_exp), MONTH(tgl_exp), YEAR(tgl_in), MONTH(tgl_in), loc_id, kd_unik ';

				if($periode == self::YEARLY && $shipmentType == self::FIFOFEFO)
					$groupBy .= ' GROUP BY YEAR(tgl_in), YEAR(tgl_exp), loc_id, kd_unik ';

				if($periode == self::YEARLY && $shipmentType == self::FEFOFIFO)
					$groupBy .= ' GROUP BY YEAR(tgl_exp), YEAR(tgl_in), loc_id, kd_unik ';

				if($periode == self::MONTHLY)
					$groupBy .= '';

				if($shipmentType == self::FIFO)
					$orderBy .= ' ORDER BY tgl_in ASC ';

				if($shipmentType == self::FEFO)
					$orderBy .= ' ORDER BY tgl_exp ASC ';

				if($shipmentType == self::FIFOFEFO)
					$orderBy .= ' ORDER BY tgl_in, tgl_exp ASC ';

				if($shipmentType == self::FEFOFIFO)
					$orderBy .= ' ORDER BY tgl_exp, tgl_in ASC ';

				//TODO id_qc

				$sql = 'SELECT
							id_barang, kd_unik, rcvbrg.loc_id, loc_name, COUNT(*) AS total
						FROM
							receiving_barang rcvbrg
						JOIN m_loc loc
							ON loc.loc_id=rcvbrg.loc_id
						WHERE
							id_barang=\''.$idBarang.'\'
						AND
							id_qc = \'1\'
						AND
							loc.loc_type NOT IN('.$this->config->item('hardcode_location').')'.$groupBy.$orderBy.'  LIMIT ' . $row[$i]['qty'];

				$rows = $this->db->query($sql)->result_array();

				//file_put_contents('debug-query', $this->db->last_query());
				//echo '<pre>', var_dump($this->db->last_query());
				//exit();

				$lens = count($rows);
				//$locations = array();

				$total = 1;
				$brg = 0;

				for($j = 0; $j < $lens; $j++){

					//array_push($locations, $rows[$j]['loc_name']);

					// $sql = 'SELECT
								// COUNT(*) AS total
							// FROM
								// picking_recomendation
							// WHERE
								// kd_unik=\''.$rows[$j]['kd_unik'].'\'
							// AND
								// id_picking=\''.$post['pl_id'].'\'';

					// $q = $this->db->query($sql)->row_array();

					// if($q['total'] > 0){

						// $sql = 'UPDATE
									// picking_recomendation
								// SET
									// kd_unik=\''.$rows[$j]['kd_unik'].'\',
									// id_barang=\''.$rows[$j]['id_barang'].'\',
									// loc_id=\''.$rows[$j]['loc_id'].'\'
								// WHERE
									// id_picking=\''.$post['pl_id'].'\'';

						// $this->db->query($sql);

					// }else{

					$sql = 'INSERT INTO picking_recomendation(
								id_picking,
								kd_unik,
								id_barang,
								loc_id
							)VALUES(
								\''.$post['pl_id'].'\',
								\''.$rows[$j]['kd_unik'].'\',
								\''.$rows[$j]['id_barang'].'\',
								\''.$rows[$j]['loc_id'].'\'
							)';

					$this->db->query($sql);

					// echo 'total<br>';
					// echo $total.'-';
					// echo $row[$i]['qty'].'<br>';

					// echo 'barang<br>';
					// echo $brg.'-';
					// echo $rows[$j]['id_barang'].'<br>';

					// if($total >= $row[$i]['qty'] && $brg == $rows[$j]['id_barang']){
						// //file_put_contents('debug-id', $rows[$j]['loc_id']);
						// continue;
					// }

					// if($brg != $rows[$j]['id_barang'])
						// $total = 0;

					// //$total .= $rows[$j]['total'];
					// $total++;
					// $brg = $rows[$j]['id_barang'];

					//}

				}
			}
			//$row[$i]['locations'] = implode(',', $locations);

		}

		//exit();

		//$result['data'] = $row;

		return $result;
	}

	public function getDetailItemPickedDataTable($post = array()){
		$result = array();

		//TODO id_qc
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_barang',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
			4 => 'jumlah_barang',
			5 => 'available_qty',
			6 => 'previous_pick',
			7 => 'picked_qty',
			8 => 'jumlah_barang - (previous_pick + picked_qty)'
		);

		$where = '';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						outbbrg.id_barang
					FROM
						picking_list pl
					JOIN picking_list_outbound plo
						ON plo.pl_id=pl.pl_id
					JOIN outbound outb
						ON outb.id_outbound=plo.id_outbound
					LEFT JOIN outbound_barang outbbrg
						ON outbbrg.id_outbound=outb.id_outbound
					LEFT JOIN picking_qty plq
						ON plq.id_barang=outbbrg.id_barang
							AND plq.pl_id=pl.pl_id
					LEFT JOIN barang brg
						ON brg.id_barang=outbbrg.id_barang
					LEFT JOIN (
						SELECT
							id_barang, COUNT(*) AS available_qty
						FROM
							receiving_barang r
						JOIN m_loc l
							ON l.loc_id=r.loc_id
						WHERE
							l.loc_type NOT IN('.$this->config->item('hardcode_location').')
						AND
							id_qc = \'1\'
						GROUP BY
							id_barang
					) AS tbl_1
						ON tbl_1.id_barang=outbbrg.id_barang
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						pl.pl_id=\''.$post['pl_id'].'\'
					GROUP BY
						brg.id_barang
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					outbbrg.id_barang, kd_barang, nama_barang, jumlah_barang,
					IFNULL(tbl_1.available_qty, 0) AS available_qty,
					IFNULL((
						SELECT
							SUM(qty)
						FROM
							picking_qty plqs
						JOIN picking_list pls
							ON pls.pl_id=plqs.pl_id
						JOIN picking_list_outbound plos
							ON plos.pl_id=pls.pl_id
						JOIN outbound outbs
							ON outbs.id_outbound=plos.id_outbound
						WHERE
							id_barang = outbbrg.id_barang
						AND
							pls.pl_id < \''.$post['pl_id'].'\'
					), 0) AS previous_pick, IFNULL(plq.qty, 0) AS picked_qty, nama_satuan
				FROM
					picking_list pl
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				LEFT JOIN outbound_barang outbbrg
					ON outbbrg.id_outbound=outb.id_outbound
				LEFT JOIN picking_qty plq
					ON plq.id_barang=outbbrg.id_barang
						AND plq.pl_id=pl.pl_id
				LEFT JOIN barang brg
					ON brg.id_barang=outbbrg.id_barang
				LEFT JOIN (
					SELECT
						id_barang, COUNT(*) AS available_qty
					FROM
						receiving_barang r
					JOIN m_loc l
						ON l.loc_id=r.loc_id
					WHERE
						l.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						id_qc = \'1\'
					GROUP BY
						id_barang
				) AS tbl_1
					ON tbl_1.id_barang=outbbrg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					pl.pl_id=\''.$post['pl_id'].'\'
				GROUP BY
					brg.id_barang';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_barang'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['available_qty'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['previous_pick'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['picked_qty'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = ($row[$i]['jumlah_barang'] - $row[$i]['previous_pick']) . ' ' . $row[$i]['nama_satuan'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItemPicked($post = array()){
		$result = array();

		//TODO id_qc

		$sql = 'SELECT
					outbbrg.id_barang, kd_barang, nama_barang, sum(jumlah_barang) as jumlah_barang,
					IFNULL(tbl_1.available_qty, 0) AS available_qty,
					IFNULL((
						SELECT
							SUM(qty)
						FROM
							picking_qty plqs
						JOIN picking_list pls
							ON pls.pl_id=plqs.pl_id
						JOIN picking_list_outbound plos
							ON plos.pl_id=pls.pl_id
						JOIN outbound outbs
							ON outbs.id_outbound=plos.id_outbound
						WHERE
							id_barang = outbbrg.id_barang
						AND
							pls.pl_id < \''.$post['pl_id'].'\'
						AND
							outbs.id_outbound in ( \''.$post['id_outbound'].'\' )
					), 0) AS previous_pick, IFNULL(plq.qty, 0) AS picked_qty, nama_satuan, plq.batch
				FROM
					picking_list pl
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				LEFT JOIN outbound_barang outbbrg
					ON outbbrg.id_outbound=outb.id_outbound
				LEFT JOIN picking_qty plq
					ON plq.id_barang=outbbrg.id_barang
						AND plq.pl_id=pl.pl_id
				LEFT JOIN barang brg
					ON brg.id_barang=outbbrg.id_barang
				LEFT JOIN (
					SELECT
						id_barang, COUNT(*) AS available_qty
					FROM
						receiving_barang r
					JOIN m_loc l
						ON l.loc_id=r.loc_id
					WHERE
						l.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						id_qc = \'1\'
					GROUP BY
						id_barang
				) AS tbl_1
					ON tbl_1.id_barang=outbbrg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					pl.pl_id=\''.$post['pl_id'].'\'
				GROUP BY
					brg.id_barang
				ORDER BY
					outbbrg.id ASC';

		$row = $this->db->query($sql)->result_array();
		$len = count($row);

		for($i = 0; $i < $len; $i++){
			$params = array(
				'id_barang' => $row[$i]['id_barang']
			);

			$r = $this->outbound_model->getBatch($params);

			$row[$i]['batchs'] = $r['data'];
			$row[$i]['remaining_qty'] = $row[$i]['jumlah_barang'] - ($row[$i]['previous_pick'] + $row[$i]['picked_qty']);
		}

		$result['data'] = $row;

		return $result;
	}

	public function getDetailItemList($post = array()){
		$result = array();

		//TODO id_qc
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id',
			1 => 'pl_id',
			2 => 'id_barang',
			3 =>  '',
			4 => 'kd_barang',
			5 => 'nama_barang',
			6 => 'jumlah_barang',
			7 => 'available_qty',
			8 => 'previous_pick',
			9 => 'picked_qty',
			10 => 'jumlah_barang - previous_pick',
			11 => 'batch',
			12 => 'actual_picked_qty'
		);

		$where = '';

		// $sql = 'SELECT
					// COUNT(*) AS total
				// FROM (
					// SELECT
						// outb.id_outbound
					// FROM
						// outbound outb
					// LEFT JOIN outbound_barang outbbrg
						// ON outbbrg.id_outbound=outb.id_outbound
					// LEFT JOIN barang brg
						// ON brg.id_barang=outbbrg.id_barang
					// LEFT JOIN (
						// SELECT
							// id_barang, COUNT(*) AS available_qty
						// FROM
							// receiving_barang r
						// JOIN m_loc l
							// ON l.loc_id=r.loc_id
						// WHERE
							// l.loc_type NOT IN('.$this->config->item('hardcode_location').')
						// AND
							// id_qc = \'1\'
						// GROUP BY
							// id_barang
					// ) AS tbl_1
						// ON tbl_1.id_barang=outbbrg.id_barang
					// LEFT JOIN picking_list_outbound plo
						// ON plo.id_outbound=outb.id_outbound
					// LEFT JOIN picking_list pl
						// ON pl.pl_id=plo.pl_id
					// LEFT JOIN picking_qty plq
						// ON plq.id_barang=outbbrg.id_barang
					// LEFT JOIN satuan sat
						// ON sat.id_satuan=brg.id_satuan
					// WHERE
						// outb.id_outbound=\''.$post['id_outbound'].'\'
					// GROUP BY
						// brg.id_barang
				// ) AS tbl';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						outbbrg.id_barang
					FROM
						picking_list pl
					JOIN picking_list_outbound plo
						ON plo.pl_id=pl.pl_id
					JOIN outbound outb
						ON outb.id_outbound=plo.id_outbound
					LEFT JOIN outbound_barang outbbrg
						ON outbbrg.id_outbound=outb.id_outbound
					LEFT JOIN picking_qty plq
						ON plq.id_barang=outbbrg.id_barang
							AND plq.pl_id=pl.pl_id
					LEFT JOIN barang brg
						ON brg.id_barang=outbbrg.id_barang
					LEFT JOIN (
						SELECT
							id_barang, COUNT(*) AS available_qty
						FROM
							receiving_barang r
						JOIN m_loc l
							ON l.loc_id=r.loc_id
						WHERE
							l.loc_type NOT IN('.$this->config->item('hardcode_location').')
						AND
							id_qc = \'1\'
						GROUP BY
							id_barang
					) AS tbl_1
						ON tbl_1.id_barang=outbbrg.id_barang
					LEFT JOIN satuan sat
						ON sat.id_satuan=brg.id_satuan
					WHERE
						pl.pl_id=\''.$post['pl_id'].'\'
						AND plq.qty != 0
						AND plq.qty IS NOT NULL
					GROUP BY
						brg.id_barang
				) AS tbl';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		// $sql = 'SELECT
					// outbbrg.id_barang, kd_barang, nama_barang, jumlah_barang,
					// IFNULL(tbl_1.available_qty, 0) AS available_qty,
					// IFNULL((
						// SELECT
							// SUM(qty)
						// FROM
							// picking_qty plqs
						// JOIN picking_list pls
							// ON pls.pl_id=plqs.pl_id
						// JOIN picking_list_outbound plos
							// ON plos.pl_id=pls.pl_id
						// JOIN outbound outbs
							// ON outbs.id_outbound=plos.id_outbound
						// WHERE
							// id_barang = outbbrg.id_barang
						// AND
							// pls.pl_id != \''.$post['pl_id'].'\'
						// AND
							// outbs.id_outbound = \''.$post['id_outbound'].'\'
					// ), 0) AS previous_pick, IFNULL(plq.qty, 0) AS picked_qty, IFNULL(nama_satuan, \'\') AS nama_satuan
				// FROM
					// outbound outb
				// LEFT JOIN outbound_barang outbbrg
					// ON outbbrg.id_outbound=outb.id_outbound
				// LEFT JOIN barang brg
					// ON brg.id_barang=outbbrg.id_barang
				// LEFT JOIN (
					// SELECT
						// id_barang, COUNT(*) AS available_qty
					// FROM
						// receiving_barang
					// WHERE
						// loc_id != \'100\'
					// AND
						// loc_id != \'101\'
					// AND
						// id_qc = \'1\'
					// GROUP BY
						// id_barang
				// ) AS tbl_1
					// ON tbl_1.id_barang=outbbrg.id_barang
				// LEFT JOIN picking_list_outbound plo
					// ON plo.id_outbound=outb.id_outbound
				// LEFT JOIN picking_list pl
					// ON pl.pl_id=plo.pl_id
				// LEFT JOIN picking_qty plq
					// ON plq.id_barang=outbbrg.id_barang
				// LEFT JOIN satuan sat
					// ON sat.id_satuan=brg.id_satuan
				// WHERE
					// outb.id_outbound=\''.$post['id_outbound'].'\'
				// GROUP BY
					// brg.id_barang';

		$sql = '
			SELECT
				outbbrg.id, pl.pl_id, outbbrg.id_barang, kd_barang, nama_barang, sum(jumlah_barang) as jumlah_barang,
				IFNULL(tbl_1.available_qty, 0) AS available_qty,
				IFNULL((
					SELECT
						SUM(qty)
					FROM
						picking_qty plqs
					JOIN picking_list pls
						ON pls.pl_id=plqs.pl_id
					JOIN picking_list_outbound plos
						ON plos.pl_id=pls.pl_id
					JOIN outbound outbs
						ON outbs.id_outbound=plos.id_outbound
					WHERE
						id_barang = outbbrg.id_barang
					AND
						pls.pl_id < \''.$post['pl_id'].'\'
					AND
						outbs.id_outbound = \''.$post['id_outbound'].'\'
				), 0) AS previous_pick, IFNULL(plq.qty, 0) AS picked_qty, nama_satuan,
				IFNULL(plq.batch, \'-\') AS batch,
				(
					SELECT
						sum(qty)
					FROM
						picking_recomendation 
					WHERE
						id_barang=plq.id_barang
					AND
						id_picking=\''.$post['pl_id'].'\'
				) AS actual_picked_qty
			FROM
				picking_list pl
			JOIN picking_list_outbound plo
				ON plo.pl_id=pl.pl_id
			JOIN outbound outb
				ON outb.id_outbound=plo.id_outbound
			LEFT JOIN outbound_barang outbbrg
				ON outbbrg.id_outbound=outb.id_outbound
			LEFT JOIN picking_qty plq
				ON plq.id_barang=outbbrg.id_barang
					AND plq.pl_id=pl.pl_id
			LEFT JOIN barang brg
				ON brg.id_barang=outbbrg.id_barang
			LEFT JOIN (
				SELECT
					id_barang, COUNT(*) AS available_qty
				FROM
					receiving_barang r
				JOIN m_loc l
					ON l.loc_id=r.loc_id
				WHERE
					l.loc_type NOT IN('.$this->config->item('hardcode_location').')
				AND
					id_qc = \'1\'
				GROUP BY
					id_barang
			) AS tbl_1
				ON tbl_1.id_barang=outbbrg.id_barang
			LEFT JOIN satuan sat
				ON sat.id_satuan=brg.id_satuan
			WHERE
				pl.pl_id=\''.$post['pl_id'].'\'
				AND plq.qty != 0
                AND plq.qty IS NOT NULL
			GROUP BY
				brg.id_barang';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id'];
			$nested[] = $row[$i]['pl_id'];
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_barang'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['available_qty'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['previous_pick'] . ' ' . $row[$i]['nama_satuan'];
			//$nested[] = ($row[$i]['jumlah_barang'] - $row[$i]['previous_pick']) . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['jumlah_barang'] - ($row[$i]['previous_pick'] + $row[$i]['picked_qty']) . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['picked_qty'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $row[$i]['batch'];
			$nested[] = $row[$i]['actual_picked_qty'] . ' ' . $row[$i]['nama_satuan'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getBatchRow($post = array()){
		$result = array();

		$sql = 'SELECT
					outbbrg.id_barang, kd_barang, nama_barang, jumlah_barang,
					IFNULL(tbl_1.available_qty, 0) AS available_qty,
					IFNULL((
						SELECT
							SUM(qty)
						FROM
							picking_qty plqs
						JOIN picking_list pls
							ON pls.pl_id=plqs.pl_id
						JOIN picking_list_outbound plos
							ON plos.pl_id=pls.pl_id
						JOIN outbound outbs
							ON outbs.id_outbound=plos.id_outbound
						WHERE
							id_barang = outbbrg.id_barang
						AND
							outbs.id_outbound=\''.$post['id_outbound'].'\'
					), 0) AS previous_pick, IFNULL(plq.qty, 0) AS picked_qty, outbbrg.batch, nama_satuan
				FROM
					outbound outb
				LEFT JOIN outbound_barang outbbrg
					ON outbbrg.id_outbound=outb.id_outbound
				LEFT JOIN barang brg
					ON brg.id_barang=outbbrg.id_barang
				LEFT JOIN (
					SELECT
						id_barang, kd_batch, COUNT(*) AS available_qty
					FROM
						receiving_barang r
					JOIN m_loc l
						ON l.loc_id=r.loc_id
					WHERE
						l.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						id_qc = \'1\'
					AND
						kd_batch=\''.$post['kd_batch'].'\'
					GROUP BY
						id_barang, kd_batch
				) AS tbl_1
					ON tbl_1.id_barang=outbbrg.id_barang
				LEFT JOIN picking_list_outbound plo
					ON plo.id_outbound=outb.id_outbound
				LEFT JOIN picking_list pl
					ON pl.pl_id=plo.pl_id
				LEFT JOIN picking_qty plq
					ON plq.id_barang=outbbrg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					outb.id_outbound=\''.$post['id_outbound'].'\'
				AND
					outbbrg.id_barang=\''.$post['id_barang'].'\'
				GROUP BY
					brg.id_barang';

		$row = $this->db->query($sql)->row_array();
		$row['remaining_qty'] = $row['jumlah_barang'] - $row['previous_pick'];

		$result['data'] = $row;

		return $result;
	}

	public function getDetailItem($post = array()){
		$result = array();

		//TODO id_qc
		//IFNULL(tbl_1.available_qty, 0) AS available_qty,

		$sql = 'SELECT
					outbbrg.id, outbbrg.id_barang, kd_barang, nama_barang, jumlah_barang,
					(
						CASE
							WHEN outbbrg.batch IS NULL
								THEN IFNULL(tbl_1.available_qty, 0)
							ELSE IFNULL(tbl_2.available_qty, 0)
						END
					) AS available_qty,
					IFNULL((
						SELECT
							SUM(qty)
						FROM
							picking_qty plqs
						JOIN picking_list pls
							ON pls.pl_id=plqs.pl_id
						JOIN picking_list_outbound plos
							ON plos.pl_id=pls.pl_id
						JOIN outbound outbs
							ON outbs.id_outbound=plos.id_outbound
						WHERE
							id_barang = outbbrg.id_barang
						AND
							outbs.id_outbound=\''.$post['id_outbound'].'\'
					), 0) AS previous_pick, IFNULL(plq.qty, 0) AS picked_qty, outbbrg.batch, nama_satuan
				FROM
					outbound outb
				LEFT JOIN outbound_barang outbbrg
					ON outbbrg.id_outbound=outb.id_outbound
				LEFT JOIN barang brg
					ON brg.id_barang=outbbrg.id_barang
				LEFT JOIN (
					SELECT
						id_barang, COUNT(*) AS available_qty
					FROM
						receiving_barang r
					JOIN m_loc l
						ON l.loc_id=r.loc_id
					WHERE
						l.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						id_qc = \'1\'
					GROUP BY
						id_barang
				) AS tbl_1
					ON tbl_1.id_barang=outbbrg.id_barang
				LEFT JOIN (
					SELECT
						id_barang, kd_batch, COUNT(*) AS available_qty
					FROM
						receiving_barang r
					JOIN m_loc l
						ON l.loc_id=r.loc_id
					WHERE
						l.loc_type NOT IN('.$this->config->item('hardcode_location').')
					AND
						id_qc = \'1\'
					GROUP BY
						id_barang, kd_batch
				) AS tbl_2
					ON tbl_2.id_barang=outbbrg.id_barang
						AND tbl_2.kd_batch=outbbrg.batch
				LEFT JOIN picking_list_outbound plo
					ON plo.id_outbound=outb.id_outbound
				LEFT JOIN picking_list pl
					ON pl.pl_id=plo.pl_id
				LEFT JOIN picking_qty plq
					ON plq.id_barang=outbbrg.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					outb.id_outbound=\''.$post['id_outbound'].'\'
				GROUP BY
					brg.id_barang
				ORDER BY
					outbbrg.id ASC';

		$row = $this->db->query($sql)->result_array();
		$len = count($row);

		for($i = 0; $i < $len; $i++){

			$params = array(
				'id_barang' => $row[$i]['id_barang']
			);

			$r = $this->outbound_model->getBatch($params);

			$row[$i]['batchs'] = $r['data'];
			$row[$i]['remaining_qty'] = $row[$i]['jumlah_barang'] - $row[$i]['previous_pick'];

		}

		$result['data'] = $row;

		return $result;
	}

	public function getOutboundCodeList(){
		$result = array();

		$sql = 'SELECT
					id_outbound, kd_outbound, DATE_FORMAT(tanggal_outbound, \'%d\/%m\/%Y\') AS  tanggal_outbound,
					IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS customer_name
				FROM
					outbound outb
				LEFT JOIN m_status_outbound mso
					ON mso.id_status_outbound=outb.id_status_outbound
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=outb.id_customer
						AND outb.id_outbound_document = \'2\'
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=outb.id_customer
						AND outb.id_outbound_document != \'2\'
				WHERE
					outb.id_status_outbound != \'2\'
				ORDER BY
					id_outbound DESC';

		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    private function data_detail($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $pl_name= $this->input->post("pl_name");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $pl_id = $this->input->post('pl_id');
        if(!empty($pl_name)){
            $condition["a.pl_name like '%$pl_name%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($pl_id)){
            $condition["b.pl_id"]=$pl_id;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

        $this->db->select('*');
        //$this->db->from($this->table  . ' a');//pl
        //$this->db->from($this->table2 . ' f');//pl_do
        $this->db->from($this->do . ' b');
        $this->db->join('toko t','t.id_toko = b.id_toko','left');
        $this->db->order_by('b.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_hasil($condition = array()) {
        // ================Filtering===================
        //$condition = array();
        $pl_name= $this->input->post("pl_name");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $pl_id = $this->input->post('pl_id');
        if(!empty($pl_name)){
            $condition["a.pl_name like '%$pl_name%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($pl_id)){
            $condition["b.pl_id"]=$pl_id;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //-----------end filtering-------------------

        $this->db->select('*');
        //$this->db->from($this->table  . ' a');//pl
        //$this->db->from($this->table2 . ' f');//pl_do
        $this->db->from($this->do . ' b');
        $this->db->join('receiving_barang rb','rb.id_do = b.id_do','left');
        $this->db->join('m_loc','m_loc.loc_id = rb.loc_id','left');
        $this->db->join('barang ','barang.id_barang = rb.id_barang','left');
        $this->db->join('satuan s','s.id_satuan = barang.id_satuan','left');
        $this->db->order_by('b.kd_do DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_databarcode_by_id($pl_id) {
        $condition['a.pl_id'] = $pl_id;
        // ===========Filtering=================
        //$condition = array();
        $this->load->model('bbk_model');
        return $this->bbk_model->data_detail($condition)->get();

    }

    public function get_by_id($id) {
        $condition['a.pl_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
    //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.pl_date >= '$tahun_aktif_awal'"] = null ;
        $condition["a.pl_date <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->pl_id;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">';

            $action .= '<li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Proses Picking List', array('id' => 'drildown_key_p_Pl_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_picking_list', 'data-source' => base_url('picking_list/get_detail_hasil_picking_list/' . $id))) . ' ';
            $action .= '</li>';
            $action .= '<li>';
            $action .= anchor(null, '<i class="fa fa-print"></i>Print Picking List', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('picking_list/print_barcode/' . $id)));
            $action .= '</li>';


            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('picking_list/edit/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('picking_list/delete/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->pl_id,
                    'pl_date' => hgenerator::switch_tanggal($value->pl_date),
                    'pl_name' => anchor(null, $value->pl_name, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_picking_list', 'data-source' => base_url('picking_list/get_detail_hasil_picking_list/' . $id))) ,
                    'do_code' => $value->kd_do,
                    'store' => $value->nama_toko,
                    'address' => $value->alamat,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->pl_id,
                    'pl_date' => hgenerator::switch_tanggal($value->pl_date),
                    'pl_name' => anchor(null, $value->pl_name, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_picking_list', 'data-source' => base_url('picking_list/get_detail_hasil_picking_list/' . $id))) ,
                    'do_code' => $value->kd_do,
                    'store' => $value->nama_toko,
                    'address' => $value->alamat,
                    'aksi' => $action
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail() {
        $pl_id = $this->input->post('pl_id');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_do;
            $action = '';
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            ';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbk/edit_detail/' .$pl_id.'/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbk/delete_detail/' .$pl_id.'/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $value->id_do,
                    'tanggal_do' => $value->tanggal_do,
                    'kd_do'=>$value->kd_do,
                    'nama_customer' => $value->nama_toko,
                    'alamat' => $value->alamat,
                    'aksi' => $action

                );
            }else{
                $rows[] = array(
                    'primary_key' => $value->id_do,
                    'tanggal_do' => $value->tanggal_do,
                    'kd_do'=>$value->kd_do,
                    'jenis_do' => $value->jenis_do,
                    'nama_customer' => $value->nama_customer,
                    'alamat' => $value->alamat,
                    'aksi' => $action

                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function qty_fefo($id_barang,$tahun=NULL){
        $this->db->select('sum(qty) as jumlah, min(tahun) as tahun');
        $this->db->from('v_pl_loc');
        $this->db->where('id_barang',$id_barang);
        if($tahun){
            $this->db->where('tahun <='.$tahun,NULL);
        }else{
            $this->db->where("tahun = (select min(tahun) from v_pl_loc where id_barang=$id_barang)",NULL);
        }
        $hasil = $this->db->get()->row();
        return $hasil;
    }

    public function list_loc_fefo($id_barang,$tahun){
        $this->db->select('loc_name');
        $this->db->from('v_pl_loc vl');
        $this->db->join('m_loc l','l.loc_id = vl.loc_id','left');
        $this->db->where('id_barang',$id_barang);
        $this->db->where('tahun <='.$tahun,NULL);
        $this->db->where('l.loc_id <> 100',NULL);
        $this->db->where('l.loc_id <> 101',NULL);
        $hasil = $this->db->get();
        $data = '';
        $i=1;
        foreach ($hasil->result() as $row) {
            if($i!=1)
                $data .= ', ';
            $data .= $row->loc_name;
            $i++;
            # code...
        }
        return $data;
    }

    function get_list_loc_fefo($id_barang,$jumlah_barang){
        $cek_qty_fefo = $this->qty_fefo($id_barang);
        $list_loc_fefo = '';
        if($jumlah_barang <= $cek_qty_fefo->jumlah){
            $list_loc_fefo = $this->list_loc_fefo($id_barang,$cek_qty_fefo->tahun);
        }else{
            $tahun = $cek_qty_fefo->tahun;
            $i = 1;
            while($i<=10){
                $cek_qty_fefo = $this->qty_fefo($id_barang,$tahun+$i);
                if($jumlah_barang <= $cek_qty_fefo->jumlah){
                    $list_loc_fefo = $this->list_loc_fefo($id_barang,$cek_qty_fefo->tahun + $i);
                    $i=$i+10;
                }
                $i++;
            }
        }
        return $list_loc_fefo;
    }

    public function data_table_detail_hasil() {
        $pl_id = $this->input->post('pl_id');
        // Total Record
        $this->load->model('bbk_model');
        $where = array('a.pl_id'=>$pl_id);
        $total = $this->bbk_model->data_detail_pl($where)->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);

        $data_detail_hasil = $this->bbk_model->data_detail_pl($where)->get();
        $rows = array();


        foreach ($data_detail_hasil->result() as $value) {
            $list_loc_fefo = $this->get_list_loc_fefo($value->id_barang,$value->jumlah_barang);
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'qty' => $value->jumlah_barang.' '.$value->nama_satuan,
                    'loc' => $list_loc_fefo,

                );
            }else{
                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'qty' => $value->jumlah_barang.' '.$value->nama_satuan,
                    'loc' => $list_loc_fefo,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        //=============Tahun Aktif=================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.pl_date >= '$tahun_aktif_awal'"] = null ;
        $condition["a.pl_date <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->pl_id;
            $action = '';
                $rows[] = array(
                    'pl_date' => hgenerator::switch_tanggal($value->pl_date),
                    'pl_name' => $value->pl_name,
                    'proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                );
        }
        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->pl_id_barang;
            $action = '';

                $rows[] = array(
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
		/*
        $pl_id = $this->db->insert_id();
        $id_do = $this->input->post('id_do');
        $update = array('pl_id' =>$pl_id);
        $this->db->where('id_do',$id_do);
        return $this->db->update('do',$update);
		*/
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('pl_id' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        $this->db->from('do');
        $this->db->join('do_barang db','do.id_do = db.id_do','left');
        $this->db->where('do.id_do',$id);
        $hasil = $this->db->get();

        foreach ($hasil->result() as $row) {

            if($row->jenis_do == 'FIFO' || $row->jenis_do==''){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_in','ASC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'FEFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_exp','ASC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'LIFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_in','DESC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }elseif($row->jenis_do == 'LEFO'){
                $this->db->from('receiving_barang rb');
                $this->db->where('id_barang',$row->id_barang);
                $this->db->where('id_do is NULL',NULL);
                $this->db->order_by('tgl_exp','DESC');
                $this->db->limit($row->jumlah_barang);
                $hasil2 = $this->db->get();
                foreach ($hasil2->result() as $row2) {
                    $this->db->update('receiving_barang',array('id_do'=>$id),array('id_receiving_barang'=>$row2->id_receiving_barang));
                    # code...
                }
            }
            # code...
        }
        return $this->db->update($this->do, $data, array('id_do' => $id));
    }

    public function delete($id) {

		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					picking_list pl
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				JOIN receiving_barang rcvbrg
					ON rcvbrg.pl_id=pl.pl_id
				WHERE
					pl.pl_id=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		if($row['total'] <= 0){

			$sql = 'DELETE FROM picking_list WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'DELETE FROM picking_qty WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'SELECT
						id_outbound
					FROM
						picking_list_outbound
					WHERE
						pl_id=\''.$id.'\'';

			$r = $this->db->query($sql)->row_array();

			$sql = 'DELETE FROM picking_list_outbound WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);
			$sql = 'DELETE FROM picking_recomendation WHERE id_picking=\''.$id.'\'';
			$this->db->query($sql);
			$sql = 'UPDATE 
						receiving_barang
					SET
						pl_id = NULL, pl_status = 0, st_shipping = 0,loc_id = 171
					WHERE
						pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
						picking_list_outbound
					WHERE
						id_outbound=\''.$r['id_outbound'].'\'';

			$rows = $this->db->query($sql)->row_array();
			if($rows['total'] <= 0){
				$sql = 'UPDATE outbound SET id_status_outbound=\'1\' WHERE id_outbound=\''.$r['id_outbound'].'\'';
				$this->db->query($sql);
			}

			$result['status'] = 'OK';
			$result['message'] = 'Delete success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';
		}

		return $result;
		/*
        $this->db->delete($this->table4, array('pl_id' => $id));
        return $this->db->delete($this->table, array('pl_id' => $id));
		*/
    }

    public function force_delete($id) {

		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					picking_list pl
				JOIN picking_list_outbound plo
					ON plo.pl_id=pl.pl_id
				JOIN outbound outb
					ON outb.id_outbound=plo.id_outbound
				JOIN receiving_barang rcvbrg
					ON rcvbrg.pl_id=pl.pl_id
				WHERE
					pl.pl_id=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		//if($row['total'] <= 0){

			$sql = 'DELETE FROM picking_list WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'DELETE FROM picking_qty WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'SELECT
						id_outbound
					FROM
						picking_list_outbound
					WHERE
						pl_id=\''.$id.'\'';

			$r = $this->db->query($sql)->row_array();
			$id_outbound = $r['id_outbound'];

			$sql = 'DELETE FROM outbound_barang_picked WHERE id_outbound=\''.$id_outbound.'\'';
			$this->db->query($sql);

			$sql = 'DELETE FROM picking_list_outbound WHERE pl_id=\''.$id.'\'';
			$this->db->query($sql);
			$sql = 'DELETE FROM picking_recomendation WHERE id_picking=\''.$id.'\'';
			$this->db->query($sql);
			$sql = 'UPDATE 
						receiving_barang
					SET
						pl_id = NULL, pl_status = 0, st_shipping = 0,loc_id = 171
					WHERE
						pl_id=\''.$id.'\'';
			$this->db->query($sql);

			$sql = 'SELECT
						COUNT(*) AS total
					FROM
						picking_list_outbound
					WHERE
						id_outbound=\''.$id_outbound.'\'';

			$rows = $this->db->query($sql)->row_array();
			if($rows['total'] <= 0){
				$sql = 'UPDATE outbound SET id_status_outbound=\'1\' WHERE id_outbound=\''.$id_outbound.'\'';
				$this->db->query($sql);
			}

			$result['status'] = 'OK';
			$result['message'] = 'Delete success';
		/*}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';
		}*/

		return $result;
		/*
        $this->db->delete($this->table4, array('pl_id' => $id));
        return $this->db->delete($this->table, array('pl_id' => $id));
		*/
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id_do' => $id));
    }


    public function options($default = '--Pilih Kode bbk--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->pl_id] = $row->pl_name ;
        }
        return $options;
    }

    public function options_do($default = '--Choose DO--', $key = '') {
        $data = $this->data_list_do()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_do] = $row->kd_do ;
        }
        return $options;
	}
	
	public function check_picking($id_outbound=array()){
		$this->db->where_in('id_outbound',$id_outbound);
		$result = $this->db->get('picking_list_outbound');
		return $result->num_rows();
	}




}

?>

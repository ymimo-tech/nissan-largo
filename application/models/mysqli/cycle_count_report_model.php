<?php
class cycle_count_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getCcCodeById($id){
		$result = '';
		
		$sql = 'SELECT 
					cc_code 
				FROM 
					cycle_count 
				WHERE 
					cc_id=\''.$id.'\''; 
		
		$row = $this->db->query($sql)->row_array();
		$result = $row['cc_code'];
		
		return $result;
	}
	
	public function getCcDoc($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					cc_id, cc_code 
				FROM 
					cycle_count 
				WHERE 
					LOWER(cc_code) LIKE \'%'.strtolower($post['query']).'%\'';
					
		$result['result'] = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getListRaw($post = array()){
		$result = array();
		
		$where = '';
		
		if(!empty($post['cc_id']))
			$where .= sprintf(' AND cc.cc_id = \'%s\' ', $post['cc_id']);
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(cc_time) BETWEEN \'%s\' AND \'%s\' ', 
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}
		
		if(!empty($post['status'])){
			if($post['status'] == 'CANCEL'){
				$where .= ' AND system_qty = \'YES\' AND scanned_qty = \'YES\' ';
			}
			
			if($post['status'] == 'POSITIVE'){
				$where .= ' AND system_qty = \'NO\' AND scanned_qty = \'YES\' ';
			}
			
			if($post['status'] == 'NEGATIVE'){
				$where .= ' AND system_qty = \'YES\' AND scanned_qty = \'NO\' ';
			}
		}
		
		$sql = 'SELECT 
					cc_code, DATE_FORMAT(cc_time, \'%d\/%m\/%Y\') AS cc_time, 
					(
						CASE cc_type 
							WHEN \'SN\' 
								THEN \'By Item\'
							ELSE \'By Location\' 
						END
					) AS cc_type, loc_name, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					kd_unik, ccr.status, system_qty, scanned_qty 
				FROM 
					cycle_count cc
				JOIN cycle_count_result ccr 
					ON ccr.cc_id=cc.cc_id
				JOIN barang brg 
					ON brg.id_barang=ccr.id_barang
				JOIN m_loc loc
					ON loc.loc_id=ccr.loc_id 
				WHERE 1=1'.$where;
				
		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		
		for($i = 0; $i < $len; $i++){
			$status = 'CANCEL';
			if($row[$i]['system_qty'] == 'NO' && $row[$i]['scanned_qty'] == 'YES')
				$status = 'POSITIVE ADJUSTMENT';
			else if($row[$i]['system_qty'] == 'YES' && $row[$i]['scanned_qty'] == 'NO')
				$status = 'NEGATIVE ADJUSTMENT';
			
			$row[$i]['status'] = $status;
		}
		
		$result = $row;
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'cc_code',
			2 => 'cc_time',
			3 => 'cc_type',
			4 => 'loc_name',
			5 => 'nama_barang',
			6 => 'kd_unik',
			7 => 'status',
			8 => 'system_qty',
			9 => 'scanned_qty',
		);
		
		$where = '';
		
		if(!empty($post['cc_id']))
			$where .= sprintf(' AND cc.cc_id = \'%s\' ', $post['cc_id']);
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(cc_time) BETWEEN \'%s\' AND \'%s\' ', 
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['from'])))),
				date('Y-m-d', strtotime(str_replace('/','-',trim($post['to'])))));
		}
		
		if(!empty($post['status'])){
			if($post['status'] == 'CANCEL'){
				$where .= ' AND system_qty = \'YES\' AND scanned_qty = \'YES\' ';
			}
			
			if($post['status'] == 'POSITIVE'){
				$where .= ' AND system_qty = \'NO\' AND scanned_qty = \'YES\' ';
			}
			
			if($post['status'] == 'NEGATIVE'){
				$where .= ' AND system_qty = \'YES\' AND scanned_qty = \'NO\' ';
			}
		}
		
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					cycle_count cc
				JOIN cycle_count_result ccr 
					ON ccr.cc_id=cc.cc_id
				JOIN barang brg 
					ON brg.id_barang=ccr.id_barang
				JOIN m_loc loc
					ON loc.loc_id=ccr.loc_id 
				WHERE 1=1'.$where;
		
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					cc_code, DATE_FORMAT(cc_time, \'%d\/%m\/%Y\') AS cc_time, 
					(
						CASE cc_type 
							WHEN \'SN\' 
								THEN \'By Item\'
							ELSE \'By Location\' 
						END
					) AS cc_type, loc_name, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					kd_unik, ccr.status, system_qty, scanned_qty 
				FROM 
					cycle_count cc
				JOIN cycle_count_result ccr 
					ON ccr.cc_id=cc.cc_id
				JOIN barang brg 
					ON brg.id_barang=ccr.id_barang
				JOIN m_loc loc
					ON loc.loc_id=ccr.loc_id 
				WHERE 1=1'.$where;
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$status = 'CANCEL';
			if($row[$i]['system_qty'] == 'NO' && $row[$i]['scanned_qty'] == 'YES')
				$status = 'POSITIVE ADJUSTMENT';
			else if($row[$i]['system_qty'] == 'YES' && $row[$i]['scanned_qty'] == 'NO')
				$status = 'NEGATIVE ADJUSTMENT';
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['cc_code'];
			$nested[] = $row[$i]['cc_time'];
			$nested[] = $row[$i]['cc_type'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $status;
			$nested[] = $row[$i]['system_qty'];
			$nested[] = $row[$i]['scanned_qty'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
}
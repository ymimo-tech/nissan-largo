<?php
class dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getReplenishment($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => 'nama_barang',
			1 => 'stock',
			2 => 'min'
		);
		
		$where = '';
		
		$sql = 'SELECT 
					COUNT(*) AS total
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					IFNULL(tbl.qty, 0) <= minimum_stock';
				
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					IFNULL(tbl.qty, 0) AS stock, minimum_stock AS min, nama_satuan  
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					IFNULL(tbl.qty, 0) <= minimum_stock';
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['stock'];
			$nested[] = $row[$i]['min'];
			$nested[] = $row[$i]['nama_satuan'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function getReplenishment1($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					tbl.qty AS stock, minimum_stock AS min, nama_satuan  
				FROM 
					barang brg 
				JOIN 
				(
					SELECT 
						id_barang, COUNT(*) AS qty
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					GROUP BY 
						id_barang
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					tbl.qty <= minimum_stock';
		
		$row = $this->db->query($sql)->result_array();
		
		$result['status'] = 'OK';
		$result['data'] = $row;
		
		return $result;
	}
	
	public function getInboundStatus($post = array()){
		$result = array();
		$today = date('Y-m-d');
		
		$sql = 'SELECT 
					(
						SELECT 
							SUM(rq.qty)
						FROM 
							receiving rcv 
						JOIN receiving_qty rq 
							ON rq.id_receiving=rcv.id_receiving 
						WHERE 
							DATE(tanggal_receiving) = \''.$today.'\' 
						AND
							start_tally IS NOT NULL 
						AND 
							finish_tally IS NULL
					) AS tally_in_progress, 
					(
						SELECT 
							SUM(rq.qty)
						FROM 
							receiving rcv 
						JOIN receiving_qty rq 
							ON rq.id_receiving=rcv.id_receiving 
						WHERE 
							DATE(tanggal_receiving) = \''.$today.'\' 
						AND
							start_tally IS NULL
					) AS ready_for_tally,
					(
						SELECT 
							SUM(rq.qty)
						FROM 
							receiving rcv 
						JOIN receiving_qty rq 
							ON rq.id_receiving=rcv.id_receiving 
						WHERE 
							DATE(tanggal_receiving) = \''.$today.'\' 
						AND
							start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
					) AS ready_to_bin,
					(
						SELECT 
							SUM(rq.qty)
						FROM 
							receiving rcv 
						JOIN receiving_qty rq 
							ON rq.id_receiving=rcv.id_receiving 
						WHERE 
							DATE(tanggal_receiving) = \''.$today.'\' 
						AND
							start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
					) AS completed';
					
		$row = $this->db->query($sql)->row_array();			
		
		$data = array();
		
		foreach($row as $k => $v){
			$data[] = array(
				'label'		=> str_replace('_',' ', ucfirst($k)),
				'qty'		=> $row[$k]
			);
		}
		
		$result['status'] = 'OK';
		$result['data'] = $data;
		
		return $result;
	}
	
	public function getOutboundStatus($post = array()){
		$result = array();
		$today = date('Y-m-d');
		
		$sql = 'SELECT 
					(
						SELECT 
							SUM(pq.qty)
						FROM 
							picking_list pl 
						JOIN picking_qty pq 
							ON pq.pl_id=pl.pl_id 
						WHERE 
							DATE(pl_date) = \''.$today.'\' 
						AND
							pl_start IS NOT NULL
						AND 
							pl_finish IS NULL 
					) AS picking_in_progress,
					(
						SELECT 
							SUM(pq.qty)
						FROM 
							picking_list pl 
						JOIN picking_qty pq 
							ON pq.pl_id=pl.pl_id 
						WHERE 
							DATE(pl_date) = \''.$today.'\' 
						AND
							pl_start IS NULL
					) AS ready_for_pick,
					(
						SELECT 
							SUM(pq.qty)
						FROM 
							picking_list pl 
						JOIN picking_qty pq 
							ON pq.pl_id=pl.pl_id 
						JOIN shipping_picking sp 
							ON sp.pl_id=pl.pl_id 
						JOIN shipping s 
							ON s.shipping_id=sp.shipping_id
						WHERE 
							DATE(pl_date) = \''.$today.'\' 
						AND
							pl_start IS NOT NULL AND pl_finish IS NOT NULL AND shipping_start IS NULL
					) AS ready_to_load,
					(
						SELECT 
							SUM(pq.qty)
						FROM 
							picking_list pl 
						JOIN picking_qty pq 
							ON pq.pl_id=pl.pl_id 
						JOIN shipping_picking sp 
							ON sp.pl_id=pl.pl_id 
						JOIN shipping s 
							ON s.shipping_id=sp.shipping_id
						WHERE 
							DATE(pl_date) = \''.$today.'\' 
						AND
							shipping_start IS NOT NULL AND shipping_finish IS NOT NULL
					) AS completed';
					
		$row = $this->db->query($sql)->row_array();			
		
		$data = array();
		
		foreach($row as $k => $v){
			$data[] = array(
				'label'		=> str_replace('_',' ', ucfirst($k)),
				'qty'		=> $row[$k]
			);
		}
		
		$result['status'] = 'OK';
		$result['data'] = $data;
		
		return $result;
	}

	public function getTopReceived($post = array()){
		$result = array();
		
		/*$sql = 'SELECT 
					kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					(
						SELECT 
							COUNT(*) 
						FROM 
							receiving_barang r 
						JOIN receiving rc 
							ON rc.id_receiving=r.id_receiving
						WHERE 
							rc.finish_putaway IS NOT NULL 
						AND
							id_barang=rcvbrg.id_barang 
						AND
							DATE(r.tgl_in) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\'
						GROUP BY
							id_barang
					) AS qty, nama_satuan 
				FROM 
					receiving_barang rcvbrg 
				JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					rcv.finish_putaway IS NOT NULL 
				AND
					DATE(rcvbrg.tgl_in) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
				GROUP BY 
					rcvbrg.id_barang 
				ORDER BY 
					qty DESC 
				LIMIT 5';
*/
		$sql = 'SELECT 
					kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					sum(first_qty)AS qty, nama_satuan 
				FROM 
					receiving_barang rcvbrg 
				JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					rcv.finish_putaway IS NOT NULL 
				AND
					DATE(rcvbrg.tgl_in) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
				GROUP BY 
					rcvbrg.id_barang 
				ORDER BY 
					qty DESC 
				LIMIT 5';
				
		$row = $this->db->query($sql)->result_array();
		
		$result['data'] = $row;
		$result['status'] = 'OK';
		
		return $result;
	}
	
	public function getTopOrdered($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					kd_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang,
					(
						SELECT 
							COUNT(*) 
						FROM 
							receiving_barang r 
						JOIN shipping s 
							ON s.shipping_id=r.shipping_id
						WHERE 
							s.shipping_finish IS NOT NULL 
						AND
							id_barang=rcvbrg.id_barang 
						AND
							s.shipping_date BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
						GROUP BY
							id_barang
					) AS qty, nama_satuan 
				FROM 
					receiving_barang rcvbrg 
				JOIN shipping shipp 
					ON shipp.shipping_id=rcvbrg.shipping_id 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 
					shipp.shipping_finish IS NOT NULL 
				AND
					shipp.shipping_date BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
				GROUP BY 
					rcvbrg.id_barang 
				ORDER BY 
					qty DESC 
				LIMIT 5';
		
		$row = $this->db->query($sql)->result_array();
		
		$result['data'] = $row;
		$result['status'] = 'OK';
		
		return $result;
	}
	
	public function getStatusStock(){
		$result = array();
					
		$sql = 'SELECT 
				(
					SELECT 
						COUNT(*) 
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id
					WHERE 
						r.id_qc=\'1\' 
					AND 
						loc_type!='.$this->config->item('sentout_location').'
				) AS good,
				(
					SELECT 
						COUNT(*) 
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					WHERE 
						r.id_qc=\'2\'
				) AS qc_hold,
				(
					SELECT 
						COUNT(*) 
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					WHERE 
						r.id_qc=\'3\'
				) AS ng,
				(
					SELECT 
						COUNT(*) 
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					WHERE 
						r.id_qc = \'4\'
				) AS cc_hold';
					
		$row = $this->db->query($sql)->row_array();
		
		$data = array();
		$total = 0;
		
		foreach($row as $k => $v){
			$data[] = array(
				'label'		=> str_replace('_',' ', strtoupper($k)),
				'qty'		=> $row[$k]
			);
			
			$total += $row[$k];
		}
		
		$result['status'] = 'OK';
		$result['data'] = $data;
		$result['total'] = $total;
		
		return $result;
	}
	
	public function getActiveStock(){
		$result = array();
					
/*		$sql = 'SELECT 
				(
					SELECT 
						sum(first_qty) 
					FROM 
						m_loc loc
					JOIN receiving_barang r 
						ON r.loc_id=loc.loc_id 
					WHERE 
						loc.loc_type IN('.$this->config->item('rec_location_qty').') 
					AND 
						r.pl_id IS NULL
				) AS receiving_qty,
				(
					SELECT 
						sum(last_qty)AS onhand_qty
					FROM 
						receiving_barang r
					
					WHERE 
						1 = 1
					AND (r.last_qty >1 or r.pl_id IS NULL)
				) AS on_hand_qty,
				"0" AS allocated_qty,
				(
					SELECT 
						COUNT(*) 
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					WHERE 
						r.id_qc IN(\'2\', \'4\')
				) AS hold_qty,
				(
					SELECT 
						COUNT(*) AS damage_qty
					FROM 
						m_qc qc
					JOIN receiving_barang r 
						ON r.id_qc=qc.id_qc 
					WHERE 
						r.id_qc IN(\'3\')
				) AS damage';
*/					

		$sql = "SELECT 
				COALESCE((
					SELECT 
						sum(first_qty) 
					FROM 
						receiving_barang rb
					WHERE 
						1=1 
					AND 
						loc_id IN (100)
				),0) AS rec_qty,
				COALESCE((
					SELECT 
						sum(last_qty)AS onhand_qty
					FROM 
						receiving_barang rb
					LEFT JOIN
						barang brg
						ON brg.id_barang = rb.id_barang
					WHERE 
						1 = 1
					AND
						loc_id NOT IN (100,101,104)
					AND 
						(CASE WHEN brg.has_qty=1 THEN rb.last_qty >=1 ELSE rb.pl_id IS NULL END)
				),0	) AS on_hand_qty,
				COALESCE( (
					SELECT
						sum(qty)
					FROM
						receiving_barang rb
					LEFT JOIN picking_recomendation pr 
						ON pr.kd_unik=rb.kd_unik
					WHERE
						1=1
					AND
						st_packing <> 1
				),0) AS allocated_qty,
				COALESCE((
					SELECT 
						COUNT(*)
					FROM 
						receiving_barang rb
					JOIN m_qc qc 
						ON rb.id_qc=qc.id_qc 
					WHERE 
						rb.id_qc IN(2,4)
				),0) AS hold_qty,
				COALESCE((
					SELECT 
						COUNT(*) AS damage_qty
					FROM 
						receiving_barang rb
					JOIN m_qc qc
						ON rb.id_qc=qc.id_qc 
					WHERE 
						rb.id_qc IN (3)
				),0) AS damage_qty";


		$row = $this->db->query($sql)->row_array();
		$row['available_qty'] = $row['on_hand_qty'] - ($row['allocated_qty'] + $row['hold_qty']);
		
		$data = array();
		$total = 0;
		
		foreach($row as $k => $v){
			$data[] = array(
				'label'		=> str_replace('_',' ', ucfirst($k)),
				'qty'		=> $row[$k]
			);
			
			// if($k == 'receiving_qty' || $k == 'on_hand_qty')
			// 	$total += $row[$k];

			$total += $row[$k];

		}
		
		$result['status'] = 'OK';
		$result['data'] = $data;
		$result['total'] = $total;
		
		return $result;
	}
	
	public function getLoadingChart($post = array()){
		$result = array();

		// $sql = 'SELECT 
					// DATE_FORMAT(shipping_date, \'%m\/%Y\') AS date, SUM(qty_store) AS qty
				// FROM 
					// shipping shipp 
				// JOIN shipping_picking shippick 
					// ON shippick.shipping_id=shipp.shipping_id 
				// JOIN picking_list pl 
					// ON pl.pl_id=shippick.pl_id 
				// JOIN picking_list_outbound plo 
					// ON plo.pl_id=pl.pl_id 
				// JOIN outbound outb 
					// ON outb.id_outbound=plo.id_outbound 
				// JOIN outbound_barang outbbrg 
					// ON outbbrg.id_outbound=outb.id_outbound 
				// JOIN barang brg 
					// ON brg.id_barang=outbbrg.id_barang
				// WHERE 
					// DATE(shipping_date) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
				// GROUP BY 
					// MONTH(shipping_date) 
				// ORDER BY 
					// shipping_date ASC';
		
		$sql = 'SELECT 
					DATE_FORMAT(shipping_date, \'%m\/%Y\') AS date, COUNT(*) AS qty
				FROM 
					shipping shipp 
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.shipping_id=shipp.shipping_id 
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				JOIN m_loc loc 
				    ON loc.loc_id=rcvbrg.loc_id
				WHERE 
					DATE(shipping_date) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
				AND 
				    loc.loc_type=\'SENTOUT\'
				GROUP BY 
					MONTH(shipping_date) 
				ORDER BY 
					shipping_date ASC';
		
		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		for($i = 0; $i < $len; $i++){
			$result[] = array($row[$i]['date'], $row[$i]['qty']);
		}
		
		return $result;
	}
	
	public function getInboundChart($post = array()){
		$result = array();
		/*
		$sql = 'SELECT 
					DATE_FORMAT(tanggal_inbound, \'%m\/%Y\') AS date, SUM(jumlah_barang) AS qty
				FROM 
					inbound inb 
				JOIN inbound_barang inbbrg 
					ON inbbrg.id_inbound=inb.id_inbound
				JOIN barang brg 
					ON brg.id_barang=inbbrg.id_barang
				WHERE 
					tanggal_inbound BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
				GROUP BY 
					MONTH(tanggal_inbound) 
				ORDER BY 
					tanggal_inbound ASC';
		*/

		$sql = 'SELECT 
					DATE_FORMAT(tanggal_receiving, \'%m\/%Y\') AS date, COUNT(*) AS qty
				FROM 
					receiving rcv 
				JOIN receiving_barang rcvbrg 
					ON rcvbrg.id_receiving=rcv.id_receiving
				JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang
				WHERE 
					DATE(tanggal_receiving) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
				GROUP BY 
					MONTH(tanggal_receiving) 
				ORDER BY 
					tanggal_receiving ASC';
		
		$row = $this->db->query($sql)->result_array();
		
		$len = count($row);
		for($i = 0; $i < $len; $i++){
			$result[] = array($row[$i]['date'], $row[$i]['qty']);
		}
		
		return $result;
	}
	
	public function getPicking($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS total_picking,
					(
						SELECT 
							COUNT(*)
						FROM 
							picking_list pl
						JOIN picking_qty pqty 
							ON pqty.pl_id=pl.pl_id
						JOIN barang brg 
							ON brg.id_barang=pqty.id_barang
						WHERE 
							DATE(pl_date) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
					) AS total_item
				FROM 
					picking_list 
				WHERE 
					DATE(pl_date) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		$result = $row;
		
		return $result;
	}
	
	public function getOutbound($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS total_outbound, 
					(
						SELECT 
							COUNT(*)
						FROM 
						(
							SELECT 
								COUNT(*)
							FROM 
								outbound outb 
							JOIN outbound_barang outbbrg 
								ON outbbrg.id_outbound=outb.id_outbound
							JOIN barang brg 
								ON brg.id_barang=outbbrg.id_barang
							WHERE 
								id_status_outbound != \'2\' 
							AND 
								tanggal_outbound BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
							GROUP BY 
								outbbrg.id_barang
						) AS s_table
					) AS total_item
				FROM 
					outbound 
				WHERE 
					id_status_outbound != \'2\' 
				AND 
					tanggal_outbound BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		$result = $row;
		
		return $result;
	}	
	
	public function getInbound($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS total_inbound, 
					(
						SELECT 
							COUNT(*)
						FROM 
						(
							SELECT 
								COUNT(*)
							FROM 
								inbound inb 
							JOIN inbound_barang inbbrg 
								ON inbbrg.id_inbound=inb.id_inbound
							JOIN barang brg 
								ON brg.id_barang=inbbrg.id_barang
							WHERE 
								id_status_inbound != \'2\' 
							AND
								tanggal_inbound BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
							GROUP BY 
								inbbrg.id_barang
						) AS s_table
					) AS total_item
				FROM 
					inbound 
				WHERE 
					id_status_inbound != \'2\' 
				AND 
					tanggal_inbound BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		$result = $row;
		
		return $result;
	}	
	
	public function getReceiving($post = array()){
		$result = array();
		
		$sql = 'SELECT 
					COUNT(*) AS total_receiving,
					(
						SELECT 
							COUNT(*)
						FROM 
							receiving rcv 
						JOIN receiving_qty rcvqty 
							ON rcvqty.id_receiving=rcv.id_receiving
						JOIN barang brg 
							ON brg.id_barang=rcvqty.id_barang
						WHERE 
							DATE(tanggal_receiving) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\' 
					) AS total_item
				FROM 
					receiving 
				WHERE 
					DATE(tanggal_receiving) BETWEEN \''.$post['start'].'\' AND \''.$post['end'].'\'';
					
		$row = $this->db->query($sql)->row_array();
		$result = $row;
		
		return $result;
	}
	
    public function cek_null_loc(){
        $update = array('loc_id'=>100);
        $where = array(
            'id_barang IS NOT NULL'=> NULL,
            'loc_id IS NULL' => NULL);
        
        $this->db->where($where);
        $this->db->update('receiving_barang',$update);
    }
   
    public function get_open_po(){
        $this->db->select('count(status_po_id) as jum');
        $this->db->from('purchase_order');
        $this->db->where('status_po_id',1);
        return $this->db->get()->row()->jum;
    }

    public function get_open_do(){
        $this->db->select('count(id_do) as jum');
        $this->db->from('do');
        $this->db->where('shipping_id IS NULL',NULL);
        return $this->db->get()->row()->jum;
    }

    public function get_total_stok(){
        $condition['pl_id IS NULL'] = NULL;
        $condition['st_cc'] = 0;
        $this->db->select('count(id_barang) as jum');
        $this->db->from('receiving_barang');
        $this->db->where($condition);
        return $this->db->get()->row()->jum;
    }

    public function get_graph_inbound(){
        $this->db->select('bulan,qty');
        $this->db->from('v_graph_inbound');
        return $this->db->get();
    }
    
    public function get_total_inbound(){
        $bulan = date('m');
        $tahun = date('Y');

        $this->db->select('count(id_barang) as qty_inbound');
        $this->db->from('receiving_barang');
        $this->db->where('month(tgl_in)',$bulan);
        $this->db->where('year(tgl_in)',$tahun);
        return $this->db->get()->row()->qty_inbound;

    }

    public function get_total_outbond(){
        $bulan = date('m');
        $tahun = date('Y');

        $this->db->select('count(rb.id_barang) as qty_outbound');
        $this->db->from('receiving_barang rb');
        $this->db->join('shipping s','s.shipping_id = rb.shipping_id','left');
        $this->db->where('month(shipping_date)',$bulan);
        $this->db->where('year(shipping_date)',$tahun);
        return $this->db->get()->row()->qty_outbound;
    }

    public function get_graph_outbound(){
        $this->db->select('bulan,qty');
        $this->db->from('v_graph_outbound');
        return $this->db->get();
    }
}

?>
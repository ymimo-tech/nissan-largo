<?php
class peminjaman_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getCustomer(){
		$result = array();
		
		$sql = 'SELECT 
					id_customer, customer_code, 
					CONCAT(customer_code, \' - \', customer_name) AS customer_name 
				FROM 
					m_customer 
				ORDER BY 
					customer_code ASC';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getList1($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'customer_name',
			2 => 'date',
			3 => 'nama_barang',
			4 => 'out_number',
			5 => 'in_number'
		);
		
		$where = '';
		$where1 = '';
		$where2 = '';
		
		if(!empty($post['id_customer']))
			$where .= ' AND cust.id_customer = \''.$post['id_customer'].'\' ';
		
		if(!empty($post['form']) && !empty($post['to'])){
			$where .= ' AND (date_in BETWEEN \''.date('Y-m-d', strtotime($post['from'])).'\'
						AND \''.date('Y-m-d', strtotime($post['to'])).'\') 
						AND (date_out BETWEEN \''.date('Y-m-d', strtotime($post['from'])).'\'
						AND \''.date('Y-m-d', strtotime($post['to'])).'\')';
		}
		
		$sql = 'SELECT 
					customer_name, 
					IFNULL(date_in, date_out) AS date, 
					IFNULL(nama_barang_in, nama_barang_out) AS nama_barang,
					IFNULL(in_table.total, 0) AS in_number, IFNULL(out_table.total, 0) AS out_number 
				FROM 
					m_customer cust
				'.$where;
		
		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'customer_name',
			2 => 'date',
			3 => 'nama_barang',
			4 => 'out_number',
			5 => 'in_number'
		);
		
		$where = '';
		$where1 = '';
		$where2 = '';
		
		if(!empty($post['id_customer']))
			$where .= ' AND cust.id_customer = \''.$post['id_customer'].'\' ';
		
		if(!empty($post['form']) && !empty($post['to'])){
			$where .= ' AND (date_in BETWEEN \''.date('Y-m-d', strtotime($post['from'])).'\'
						AND \''.date('Y-m-d', strtotime($post['to'])).'\') 
						AND (date_out BETWEEN \''.date('Y-m-d', strtotime($post['from'])).'\'
						AND \''.date('Y-m-d', strtotime($post['to'])).'\')';
		}
		
		/*
		if(!empty($post['form']) && !empty($post['to'])){
			$where1 .= ' AND date_in BETWEEN \''.date('Y-m-d', strtotime($post['from'])).'\'
						AND \''.date('Y-m-d', strtotime($post['to'])).'\'';
						
			$where2 .= ' AND date_out BETWEEN \''.date('Y-m-d', strtotime($post['from'])).'\'
						AND \''.date('Y-m-d', strtotime($post['to'])).'\'';
		}
		*/
			
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					m_customer cust
				LEFT JOIN (
					SELECT 
						id_supplier, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang_out, 
						DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\') AS date_in, COUNT(*) AS total 
					FROM 
						receiving rcv 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.id_receiving=rcv.id_receiving 
					JOIN barang brg 
						ON brg.id_barang=rcvbrg.id_barang
					JOIN inbound inb 
						ON inb.id_inbound=rcv.id_po 
					WHERE 
						id_inbound_document=\'7\''.$where1.'  
					GROUP BY 
						id_supplier, rcvbrg.id_barang, tgl_in 
				) in_table 
					ON in_table.id_supplier=cust.id_customer 
				LEFT JOIN (
					SELECT 
						id_customer, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang_in,
						DATE_FORMAT(shipping_date, \'%d\/%m\/%Y\') AS date_out, COUNT(*) AS total 
					FROM 
						shipping s
					JOIN shipping_picking sp
						ON sp.shipping_id=s.shipping_id 
                    JOIN picking_list pl 
                    	ON pl.pl_id=sp.pl_id
					JOIN picking_list_outbound plo 
						ON plo.pl_id=sp.pl_id 
					JOIN outbound outb 
						ON outb.id_outbound=plo.id_outbound 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.shipping_id=s.shipping_id 
					JOIN barang brg 
						ON brg.id_barang=rcvbrg.id_barang
					WHERE 
						id_outbound_document=\'6\''.$where2.'  
					GROUP BY 
						id_customer, rcvbrg.id_barang, shipping_date 
				) out_table 
					ON out_table.id_customer=cust.id_customer
				WHERE 
					in_table.total IS NOT NULL 
				OR 
					out_table.total IS NOT NULL'.$where;
				
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					customer_name, 
					IFNULL(date_in, date_out) AS date, 
					IFNULL(nama_barang_in, nama_barang_out) AS nama_barang,
					IFNULL(in_table.total, 0) AS in_number, IFNULL(out_table.total, 0) AS out_number 
				FROM 
					m_customer cust
				LEFT JOIN (
					SELECT 
						id_supplier, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang_out, 
						DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\') AS date_in, COUNT(*) AS total 
					FROM 
						receiving rcv 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.id_receiving=rcv.id_receiving 
					JOIN barang brg 
						ON brg.id_barang=rcvbrg.id_barang
					JOIN inbound inb 
						ON inb.id_inbound=rcv.id_po 
					WHERE 
						id_inbound_document=\'7\''.$where1.'  
					GROUP BY 
						id_supplier, rcvbrg.id_barang, tgl_in 
				) in_table 
					ON in_table.id_supplier=cust.id_customer 
				LEFT JOIN (
					SELECT 
						id_customer, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang_in,
						DATE_FORMAT(shipping_date, \'%d\/%m\/%Y\') AS date_out, COUNT(*) AS total 
					FROM 
						shipping s
					JOIN shipping_picking sp
						ON sp.shipping_id=s.shipping_id 
                    JOIN picking_list pl 
                    	ON pl.pl_id=sp.pl_id
					JOIN picking_list_outbound plo 
						ON plo.pl_id=sp.pl_id 
					JOIN outbound outb 
						ON outb.id_outbound=plo.id_outbound 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.shipping_id=s.shipping_id 
					JOIN barang brg 
						ON brg.id_barang=rcvbrg.id_barang
					WHERE 
						id_outbound_document=\'6\''.$where2.'  
					GROUP BY 
						id_customer, rcvbrg.id_barang, shipping_date 
				) out_table 
					ON out_table.id_customer=cust.id_customer
				WHERE 
					in_table.total IS NOT NULL 
				OR 
					out_table.total IS NOT NULL'.$where.' 
				GROUP BY 
					out_table.date_out, in_table.date_in';
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['customer_name'];
			$nested[] = $row[$i]['date'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['out_number'];
			$nested[] = $row[$i]['in_number'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
	
	public function getReportListRaw($post = array()){
		$result = array();
		
		$where = '';
		
		if(!empty($post['id_customer']))
			$where .= ' AND cust.id_customer = \''.$post['id_customer'].'\' '; 
		
		$sql = 'SELECT 
					customer_name, in_table.total AS in_number, out_table.total AS out_number 
				FROM 
					m_customer cust
				LEFT JOIN (
					SELECT 
						id_customer, COUNT(*) AS total 
					FROM 
						shipping s
					JOIN shipping_picking sp
						ON sp.shipping_id=s.shipping_id 
                    JOIN picking_list pl 
                    	ON pl.pl_id=sp.pl_id
					JOIN picking_list_outbound plo 
						ON plo.pl_id=sp.pl_id 
					JOIN outbound outb 
						ON outb.id_outbound=plo.id_outbound 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.shipping_id=s.shipping_id 
					WHERE 
						id_outbound_document=\'6\' 
					AND 
						shipping_date=\''.date('Y-m-d').'\' 
					GROUP BY 
						id_customer
				) in_table 
					ON in_table.id_customer=cust.id_customer 
				LEFT JOIN (
					SELECT 
						id_supplier, COUNT(*) AS total 
					FROM 
						receiving rcv 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.id_receiving=rcv.id_receiving 
					JOIN inbound inb 
						ON inb.id_inbound=rcv.id_po 
					WHERE 
						id_inbound_document=\'7\' 
					AND 
						rcvbrg.tgl_in=\''.date('Y-m-d').'\'
					GROUP BY 
						id_supplier
				) out_table 
					ON out_table.id_supplier=id_supplier
				WHERE 
					1=1'.$where;
				
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getReportList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'customer_name',
			2 => 'in_number',
			3 => 'out_number'
		);
		
		$where = '';
		
		if(!empty($post['id_customer']))
			$where .= ' AND cust.id_customer = \''.$post['id_customer'].'\' ';
			
		$sql = 'SELECT 
					COUNT(*) AS total 
				FROM 
					m_customer cust
				LEFT JOIN (
					SELECT 
						id_customer, COUNT(*) AS total 
					FROM 
						shipping s
					JOIN shipping_picking sp
						ON sp.shipping_id=s.shipping_id 
                    JOIN picking_list pl 
                    	ON pl.pl_id=sp.pl_id
					JOIN picking_list_outbound plo 
						ON plo.pl_id=sp.pl_id 
					JOIN outbound outb 
						ON outb.id_outbound=plo.id_outbound 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.shipping_id=s.shipping_id 
					WHERE 
						id_outbound_document=\'6\' 
					AND 
						shipping_date=\''.date('Y-m-d').'\' 
					GROUP BY 
						id_customer
				) in_table 
					ON in_table.id_customer=cust.id_customer 
				LEFT JOIN (
					SELECT 
						id_supplier, COUNT(*) AS total 
					FROM 
						receiving rcv 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.id_receiving=rcv.id_receiving 
					JOIN inbound inb 
						ON inb.id_inbound=rcv.id_po 
					WHERE 
						id_inbound_document=\'7\' 
					AND 
						rcvbrg.tgl_in=\''.date('Y-m-d').'\'
					GROUP BY 
						id_supplier
				) out_table 
					ON out_table.id_supplier=id_supplier
				WHERE 
					1=1'.$where;
				
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					customer_name, in_table.total AS in_number, out_table.total AS out_number 
				FROM 
					m_customer cust
				LEFT JOIN (
					SELECT 
						id_customer, COUNT(*) AS loan, 
						(
							SELECT 
								id_supplier, COUNT(*) AS total 
							FROM 
								receiving rcv 
							JOIN receiving_barang rcvbrg 
								ON rcvbrg.id_receiving=rcv.id_receiving 
							JOIN inbound inb 
								ON inb.id_inbound=rcv.id_po 
							WHERE 
								id_inbound_document=\'7\' 
							AND 
								rcvbrg.tgl_in=\''.date('Y-m-d').'\'
							GROUP BY 
								id_supplier
						) AS return 
					FROM 
						shipping s
					JOIN shipping_picking sp
						ON sp.shipping_id=s.shipping_id 
                    JOIN picking_list pl 
                    	ON pl.pl_id=sp.pl_id
					JOIN picking_list_outbound plo 
						ON plo.pl_id=sp.pl_id 
					JOIN outbound outb 
						ON outb.id_outbound=plo.id_outbound 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.shipping_id=s.shipping_id 
					WHERE 
						id_outbound_document=\'6\' 
					AND 
						shipping_date=\''.date('Y-m-d').'\' 
					GROUP BY 
						id_customer
				) in_table 
					ON in_table.id_customer=cust.id_customer 
				LEFT JOIN (
					SELECT 
						id_supplier, COUNT(*) AS return, 
						(
						
						)
					FROM 
						receiving rcv 
					JOIN receiving_barang rcvbrg 
						ON rcvbrg.id_receiving=rcv.id_receiving 
					JOIN inbound inb 
						ON inb.id_inbound=rcv.id_po 
					WHERE 
						id_inbound_document=\'7\' 
					AND 
						rcvbrg.tgl_in=\''.date('Y-m-d').'\'
					GROUP BY 
						id_supplier
				) out_table 
					ON out_table.id_supplier=id_supplier
				WHERE 
					1=1'.$where;
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['customer_name'];
			$nested[] = $row[$i]['in_number'];
			$nested[] = $row[$i]['out_number'];
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}
}
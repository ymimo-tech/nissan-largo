<?php
class stok_barang_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'barang';
    private $table2 = 'kategori';
    private $table3 = 'satuan';
    private $table4 = 'receiving';
    private $table5 = 'receiving_barang';
    private $table6 = 'do';
    private $table7 = 'do_barang';
    private $table8 = 'purchase_order';
    private $table9 = 'supplier';
    private $table10 = 'proyek';
    private $table11 = 'barang_son';
    private $table12 = 'purchase_order_barang';

    private function data_detail_po($condition = array()) {
        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table12 . ' b', 'a.id_barang = b.id_barang','left');
        $this->db->join($this->table8 . ' c', 'c.id_po = b.id_po','left');
        $this->db->join($this->table9 . ' d', 'd.id_supplier = c.id_supplier');
        $this->db->order_by('c.tanggal_po DESC, c.kd_po DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    function __rev1(){
        $this->db->from('temp_barang');
        $sql = $this->db->get();
        foreach ($sql->result() as $row){
            $data = array('jumlah_awal'=>$row->stok);
            $where = array('id_barang'=>$row->id);
            $this->db->update('barang',$data,$where);
        }
    }
    private function data($condition = array(),$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $tahun_faktur = $tahun_aktif;
        $where = '';
        $where_bbm = " and tanggal_bbm <= '$tahun_faktur-12-31' ";
        $where_bbk = " and tanggal_bbk <= '$tahun_faktur-12-31' ";
        $where_son = " and tanggal_son <= '$tahun_faktur-12-31' ";
        $where_po = " and po.tanggal_po <= '$tahun_faktur-12-31' and po.tanggal_po >= '$tahun_faktur-01-01'";
        //$jumlah = 0;

        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', 'a.id_kategori = b.id_kategori','left');
        $this->db->join($this->table3 . ' c', 'c.id_satuan = a.id_satuan','left');
        /*$this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_masuk from po_barang,bbm where 1=1 and po_barang.id_bbm = bbm.id_bbm $where_bbm group by id_barang ) as np",'np.id_barang = a.id_barang','left');
        $this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_keluar from bbk_barang,bbk where 1=1 and bbk.id_bbk = bbk_barang.id_bbk $where_bbk group by id_barang) as nj",'nj.id_barang = a.id_barang','left');
        $this->db->join("(select id_barang, sum(surplus) as surplus_son, sum(minus) as minus_son from barang_son where 1=1 $where_son group by id_barang) as ns",'ns.id_barang = a.id_barang','left');
        $this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_po from purchase_order_barang pob,purchase_order po where 1=1 and pob.id_po = po.id_po $where_po group by id_barang ) as npo",'npo.id_barang = a.id_barang','left');*/
        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_list($condition = array(),$tahun_aktif='') {
        $this->db->select('*');
        $this->db->from($this->table  . ' a');
       
        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_bbm($condition = array()) {
        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', 'a.id_kategori = b.id_kategori','left');
        $this->db->join($this->table3 . ' c', 'c.id_satuan = a.id_satuan','left');
        $this->db->join($this->table5 . ' d', 'd.id_barang = a.id_barang');
        $this->db->join($this->table4 . ' e', 'e.id_bbm = d.id_bbm','left');
        $this->db->join($this->table8 . ' f', 'f.id_po = e.id_po','left');
        $this->db->join($this->table9 . ' g', 'g.id_supplier = e.id_supplier','left');
        $this->db->order_by('e.tanggal_bbm DESC, e.kd_bbm DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    private function data_detail_bbk($condition = array()) {
        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', 'a.id_kategori = b.id_kategori','left');
        $this->db->join($this->table3 . ' c', 'c.id_satuan = a.id_satuan','left');
        $this->db->join($this->table7 . ' d', 'd.id_barang = a.id_barang');
        $this->db->join($this->table6 . ' e', 'e.id_bbk = d.id_bbk','left');
        $this->db->join($this->table10 . ' f', 'f.id_proyek = e.id_proyek','left');
        $this->db->order_by('e.tanggal_bbk DESC, e.kd_bbk DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }


    private function data_jumlah_awal($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_jumlah_son($condition = array()) {
        $this->db->select('sum(minus) as minus_son, sum(surplus) as surplus_son,id_barang');
        $this->db->from($this->table11  . ' a');
        $this->db->where_condition($condition);
        $this->db->group_by('id_barang');
        return $this->db;
    }

    public function get_by_id($id,$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $condition['a.id_barang'] = $id;
        $this->data($condition,$tahun_aktif);
        return $this->db->get();
    }

    public function get_data($condition = array(),$tahun_aktif) {
        $this->data($condition,$tahun_aktif);
        return $this->db->get();
    }

    function jumlah_awal($id_barang='',$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $tahun_faktur = $tahun_aktif -1;
        $where = '';
        $where_bbm = " and tanggal_bbm <= '$tahun_faktur-12-31' and tanggal_bbm >= '$tahun_faktur-01-01'";
        $where_bbk = " and tanggal_bbk <= '$tahun_faktur-12-31' and tanggal_bbk >= '$tahun_faktur-01-01'";
        $where_son = " and tanggal_son <= '$tahun_faktur-12-31' ";
        $jumlah = 0;
        $condition = array('a.id_barang'=>$id_barang);
        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_masuk from po_barang,bbm where 1=1 and bbm.id_bbm = po_barang.id_bbm $where_bbm group by id_barang ) as np",'np.id_barang = a.id_barang','left');
        $this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_keluar from bbk_barang,bbk where 1=1 and bbk.id_bbk = bbk_barang.id_bbk $where_bbk group by id_barang) as nj",'nj.id_barang = a.id_barang','left');
        $this->db->join("(select id_barang, sum(surplus) as surplus_son, sum(minus) as minus_son from barang_son where 1=1 $where_son group by id_barang) as ns",'ns.id_barang = a.id_barang','left');
        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);
        $data = $this->db->get();
        foreach ($data->result() as $row) 
        {
            $jumlah = $row->jumlah_awal + $row->jumlah_masuk - $row->jumlah_keluar + $row->surplus_son - $row->minus_son;
            # code...
        }

        return $jumlah;
    }

    function jumlah_bbm($id_barang='',$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $tahun_faktur = $tahun_aktif;
        $where = '';
        $where_bbm = " and tanggal_bbm <= '$tahun_faktur-12-31' and tanggal_bbm >= '$tahun_faktur-01-01'";
        $where_bbk = " and tanggal_bbk <= '$tahun_faktur-12-31' and tanggal_bbk >= '$tahun_faktur-01-01'";
        $where_son = " and tanggal_son <= '$tahun_faktur-12-31' ";
        $jumlah = 0;
        $condition = array('a.id_barang'=>$id_barang);
        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_masuk from po_barang,bbm where 1=1 and bbm.id_bbm = po_barang.id_bbm $where_bbm group by id_barang ) as np",'np.id_barang = a.id_barang','left');
        //$this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_keluar from bbk_barang,bbk where 1=1 and bbk.id_bbk = bbk_barang.id_bbk $where_bbk group by id_barang) as nj",'nj.id_barang = a.id_barang','left');
        //$this->db->join("(select id_barang, sum(surplus) as surplus_son, sum(minus) as minus_son from barang_son where 1=1 $where_son group by id_barang) as ns",'ns.id_barang = a.id_barang','left');
        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);
        $data = $this->db->get();
        foreach ($data->result() as $row) 
        {
            $jumlah = $row->jumlah_masuk;
            # code...
        }

        return $jumlah;
    }

    function jumlah_bbk($id_barang='',$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $tahun_faktur = $tahun_aktif;
        $where = '';
        $where_bbm = " and tanggal_bbm <= '$tahun_faktur-12-31' and tanggal_bbm >= '$tahun_faktur-01-01'";
        $where_bbk = " and tanggal_bbk <= '$tahun_faktur-12-31' and tanggal_bbk >= '$tahun_faktur-01-01'";
        $where_son = " and tanggal_son <= '$tahun_faktur-12-31' ";
        $jumlah = 0;
        $condition = array('a.id_barang'=>$id_barang);
        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_masuk from bbk_barang,bbk where 1=1 and bbk.id_bbk = bbk_barang.id_bbk $where_bbk group by id_barang ) as np",'np.id_barang = a.id_barang','left');
        $this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);
        $data = $this->db->get();
        foreach ($data->result() as $row) 
        {
            $jumlah = $row->jumlah_masuk;
            # code...
        }

        return $jumlah;
    }
    function jumlah_po($id_barang='',$tahun_aktif='') {
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $tahun_faktur = $tahun_aktif;
        $where = '';
        $where_bbm = " and po.tanggal_po <= '$tahun_faktur-12-31' and po.tanggal_po >= '$tahun_faktur-01-01'";
        //$where_bbk = " and tanggal_bbk <= '$tahun_faktur-12-31' and tanggal_bbk >= '$tahun_faktur-01-01'";
        //$where_son = " and tanggal_son <= '$tahun_faktur-12-31' ";
        $jumlah = 0;
        $condition = array('a.id_barang'=>$id_barang);
        $this->db->select('*, a.id_barang as id_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_masuk from purchase_order_barang pob,purchase_order po where 1=1 and pob.id_po = po.id_po $where_bbm group by id_barang ) as np",'np.id_barang = a.id_barang','left');
        //$this->db->select('sum(')
        //$this->db->join("(select id_barang, sum(jumlah_barang) as jumlah_keluar from bbk_barang,bbk where 1=1 and bbk.id_bbk = bbk_barang.id_bbk $where_bbk group by id_barang) as nj",'nj.id_barang = a.id_barang','left');
        //$this->db->join("(select id_barang, sum(surplus) as surplus_son, sum(minus) as minus_son from barang_son where 1=1 $where_son group by id_barang) as ns",'ns.id_barang = a.id_barang','left');
        //$this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);
        $data = $this->db->get();
        foreach ($data->result() as $row) 
        {
            $jumlah = $row->jumlah_masuk;
            # code...
        }

        return $jumlah;
    }

    public function __jumlah_awal($id_barang) {
        // Filtering
        $condition['id_barang = '.$id_barang] = null;


        // List Data
        $data_jumlah_awal = $this->data_jumlah_awal($condition)->get();
        $rows = array();

        foreach ($data_jumlah_awal->result() as $value) {
            $id = $value->id_barang;
            $jumlah = $value->jumlah_awal;
        }
        return $jumlah;
    }   

    public function jumlah_son($id_barang,$tahun_aktif='') {
        // Filtering
        if($tahun_aktif == '')
        {
            $tahun_aktif = date('Y');
        }
        $condition[' id_barang = '.$id_barang] = null;
        $condition[" tanggal_son <= '$tahun_aktif-12-31' and tanggal_son >= '$tahun_aktif-01-01' "] = NULL;

        // List Data
        $data_jumlah_awal = $this->data_jumlah_son($condition)->get();
        $rows = array();
        $jumlah = 0;
        foreach ($data_jumlah_awal->result() as $value) {
            $id = $value->id_barang;
            $jumlah = $value->surplus_son - $value->minus_son;
        }
        return $jumlah;
    }  

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["a.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["a.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"] = null;
        }

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        //-----------end tahunaktif-------------------

        // Total Record
        $total = $this->data($condition,$tahun_aktif)->count_all_results();

        // List Data
        
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition,$tahun_aktif)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;
            $action = '<div class="btn-group">
                        <button class="btn yellow dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';
            $action .= '<ul class="dropdown-menu" role="menu">';
            $action .= '<li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_purchase_order', 'data-source' => base_url('referensi_purchase_order/get_detail_purchase_order/' . $id))) . ' ';
            $action .= '</li>';'


            
                                
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-detail-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('stok_barang/edit/'. $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-detail-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('stok_barang/delete/'. $id)));
                $action .= '</li>';
            }
            $action .= '</ul></div>';
            //$jumlah = $value->jumlah_awal + $value->jumlah_masuk - $value->jumlah_keluar + $value->surplus_son - $value->minus_son;
            $rows[] = array(
                    'primary_key' =>$value->id_barang,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'aksi' => $action
                );
            /*if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                
                $rows[] = array(
                    'primary_key' =>$value->id_barang,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'jumlah'=> hgenerator::number($jumlah),
                    'jumlah_po' => hgenerator::number($value->jumlah_po),
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_barang,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'jumlah'=>hgenerator::number($jumlah),
                    'jumlah_po' => hgenerator::number($value->jumlah_po),
                    'aksi' => $action
                );
            }*/
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function data_table_detail_bbm(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbm <= '$tahun_aktif-12-31' and e.tanggal_bbm >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbm($condition)->count_all_results();

        // List Data
        //$this->db->order_by('e.tanggal_bbm');
        $this->db->limit($this->limit, $this->offset);
        $data_detail_bbm = $this->data_detail_bbm($condition)->get();
        $rows = array();

        foreach ($data_detail_bbm->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbm),
                    'kd_bbm' => $value->kd_bbm,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_po_bbm(){
        $id_po = $this->input->post('id_po');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbm <= '$tahun_aktif-12-31' and e.tanggal_bbm >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbm($condition)->count_all_results();

        // List Data
        //$this->db->order_by('e.tanggal_bbm');
        $this->db->limit($this->limit, $this->offset);
        $data_detail_bbm = $this->data_detail_bbm($condition)->get();
        $rows = array();

        foreach ($data_detail_bbm->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbm),
                    'kd_bbm' => $value->kd_bbm,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }


    public function data_table_detail_bbk(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbk <= '$tahun_aktif-12-31' and e.tanggal_bbk >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        //$this->db->order_by('e.tanggal_bbk');
        $this->db->limit($this->limit, $this->offset);
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbk),
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_po(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" c.tanggal_po <= '$tahun_aktif-12-31' and c.tanggal_po >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_po($condition)->count_all_results();

        // List Data
        //$this->db->order_by('e.tanggal_bbk');
        $this->db->limit($this->limit, $this->offset);
        $data_detail_po = $this->data_detail_po($condition)->get();
        $rows = array();

        foreach ($data_detail_po->result() as $value) {
            $id = $value->id_barang;
                $total = $value->harga_barang * $value->jumlah_barang;
                $rows[] = array(
                    'tanggal_po' => hgenerator::switch_tanggal($value->tanggal_po),
                    'kd_po' => $value->kd_po,
                    'nama_supplier' => '('.$value->kd_supplier.') '.$value->nama_supplier,
                    'jumlah'=>$value->jumlah_barang,
                    'harga' =>number_format($value->harga_barang,0,',','.'),
                    'total'=>number_format($total,0,',','.')
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        // Filtering
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["a.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["a.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["a.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["a.nama_barang like '%$nama_barang%'"] = null;
        }

        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        //-----------end tahunaktif-------------------

        // Total Record
        $total = $this->data($condition,$tahun_aktif)->count_all_results();

        // List Data
        
        $data = $this->data($condition,$tahun_aktif)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_barang;
            $action = '';
            $jumlah = $value->jumlah_awal + $value->jumlah_masuk - $value->jumlah_keluar + $value->surplus_son - $value->minus_son;
                $rows[] = array(
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'nama_satuan' => $value->nama_satuan,
                    'nama_kategori' => $value->nama_kategori,
                    'jumlah'=>$jumlah,
                    'jumlah_po'=>$value->jumlah_po
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function data_table_detail_bbm_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbm <= '$tahun_aktif-12-31' and e.tanggal_bbm >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbm($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbm');
        $data_detail_bbm = $this->data_detail_bbm($condition)->get();
        $rows = array();

        foreach ($data_detail_bbm->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbm),
                    'kd_bbm' => $value->kd_bbm,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_bbk_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbk <= '$tahun_aktif-12-31' and e.tanggal_bbk >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbk');
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbk),
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_detail_po_excel(){
        $id_barang = $this->input->post('id_barang');
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        // Filtering
        $condition = array();
        $condition['a.id_barang'] = $id_barang;
        $condition[" e.tanggal_bbk <= '$tahun_aktif-12-31' and e.tanggal_bbk >= '$tahun_aktif-01-01'"] = NULL;

        // Total Record
        $total = $this->data_detail_bbk($condition)->count_all_results();

        // List Data
        $this->db->order_by('e.tanggal_bbk');
        $data_detail_bbk = $this->data_detail_bbk($condition)->get();
        $rows = array();

        foreach ($data_detail_bbk->result() as $value) {
            $id = $value->id_barang;

                $rows[] = array(
                    'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbk),
                    'kd_bbm' => $value->kd_bbk,
                    'kd_proyek' => '('.$value->kd_proyek.') '.$value->nama_proyek,
                    'jumlah'=>$value->jumlah_barang,
                );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_barang' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_barang' => $id));
    }
    
    public function options_filter($default = '--Pilih Jenis Perawatan--', $key = '') {
        $this->db->order_by('a.id_barang');
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->jenis_perawatan;
        }
        return $options;
    }


    public function options($default = '--Pilih Barang--', $key = '') {
        $data = $this->data_list()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->kd_barang.' / '.$row->nama_barang ;
        }
        return $options;
    }
    
}

?>
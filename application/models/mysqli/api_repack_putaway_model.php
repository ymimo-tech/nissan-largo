<?php

class Api_repack_putaway_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function get($serial_number){
        $this->db->select("loc_id, rb.id_order_kitting");
        $this->db->from("receiving_barang rb");
        $this->db->join("order_kitting ok", "ok.id_order_kitting = rb.id_order_kitting", "left");
        $this->db->where("st_repack", 1);
        $this->db->where("rb.kd_unik", $serial_number);
        return $this->db->get();
    }

    function get_lp($serial_number){
        $this->db->select("loc_id, rb.id_order_kitting");
        $this->db->from("receiving_barang rb");
        $this->db->join("order_kitting ok", "ok.id_order_kitting = rb.id_order_kitting", "left");
        $this->db->where("st_repack", 1);
        $this->db->where("rb.kd_parent", $serial_number);
        return $this->db->get();
    }

    function set($data){
        $id_user = $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
        $this->db->set("loc_id", 103);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->update("receiving_barang");
    }

    function set_lp($data){
        $id_user = $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
        $this->db->set("loc_id", 103);
        $this->db->where("kd_parent", $data["kd_unik"]);
        $this->db->update("receiving_barang");
    }

    function start($data){
        $this->db->set("start_putaway", "IFNULL(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("id_receiving", $data["id_receiving"]);
        $this->db->update("receiving");
    }

    function start_lp($data){
        $this->db->set("start_putaway", "IFNULL(start_putaway, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("id_receiving", $data["id_receiving"]);
        $this->db->update("receiving");
    }

    

    function send_staging($kd_receiving=''){
        $this->load->model('api_staging_model');
        $sql = "
            SELECT
                kd_barang,
                rb.id_barang,
                sum(first_qty)AS qty,
                r.id_receiving,
                i.id_staging,
                i.kd_inbound,
                kd_receiving,
                delivery_doc,
                max(tgl_in)AS rcv_date,
                FinaCode,
                max(id_receiving_barang) as max_id
            FROM
                receiving_barang rb
            LEFT JOIN barang b ON b.id_barang = rb.id_barang
            LEFT JOIN receiving r ON r.id_receiving = rb.id_receiving
            LEFT JOIN inbound i ON i.id_inbound = r.id_po
            LEFT JOIN(
                SELECT
                    id_barang,
                    id_inbound,
                    FinaCode
                FROM
                    inbound_barang
                GROUP BY
                    id_inbound,
                    id_barang
            )t_ib ON t_ib.id_barang = b.id_barang
            AND t_ib.id_inbound = i.id_inbound 
            WHERE
                1=1
            AND i.id_staging > 0 
            AND rb.st_staging = 0 
            AND rb.id_qc = 1
            GROUP BY
                id_receiving,
                kd_barang,
                i.id_staging,
                i.kd_inbound";
        $query = $this->db->query($sql);
        $Line = 0;
        foreach ($query->result() as $row) {
            $Line++;
            //================= start update page code 6542========
            //== L2 no longer update, will continue insert row

            $NoLine = $Line;
            $sql = "select max(LineNum) as maxline 
                    from staging_inbound_l2
                    where RcvDoc = '".$row->delivery_doc."'";
            $query = $this->db->query($sql);
            $hasil = $query->row();
            if(!empty($hasil->maxline)){
                $NoLine = $hasil->maxline + 100;
            }else{
                $NoLine = 100;
            }
            $data = array(
                'DocEntry' =>$row->id_staging,
                'LineNum' =>$NoLine,
                'SKU'=>$row->kd_barang,
                'ReceiveQty'=>$row->qty,
                'SyncStatus'=>'N',
                'CreateDate'=>date('Y-m-d H:i:s'),
                'RcvDoc'=>$row->delivery_doc,
                'RcvDate'=>$row->rcv_date,
                'InbNo'=> $row->kd_inbound,
                'FinaCode' => $row->FinaCode
                );
            
            $result = $this->api_staging_model->_curl_process('insert','f_t_inbound_l2',array('data'=>$data));
            if(!empty($result)){
                if($result->status == '001'){
                    $this->db->insert('staging_inbound_l2',$data);
                    $data_update = array(
                        'st_staging' => 1);
                    $data_where = array(
                        'id_receiving_barang <= '.$row->max_id => NULL,
                        'st_staging' => 0,
                        'id_barang' => $row->id_barang,
                        'id_receiving' => $row->id_receiving);
                    $this->db->where($data_where);
                    $this->db->update('receiving_barang',$data_update);
                    //echo '1212';
                }
            }

            //--------------- end update L2 code 6542 ---------------

            /*if(!empty($row->id_staging)){
                $data = array(
                    'DocEntry' =>$row->id_staging,
                    'LineNum' =>$Line,
                    'SKU'=>$row->kd_barang,
                    'ReceiveQty'=>$row->qty,
                    'SyncStatus'=>'N',
                    'CreateDate'=>date('Y-m-d H:i:s'),
                    'RcvDoc'=>$row->delivery_doc,
                    'RcvDate'=>$row->rcv_date,
                    'InbNo'=> $row->kd_inbound,
                    );
                $data_where = array(
                    'SKU'=>$row->kd_barang,
                    'RcvDoc'=>$row->delivery_doc,
                    'InbNo'=> $row->kd_inbound);
                
                $cek_val = $this->db->get_where('staging_inbound_l2',$data_where);
                if($cek_val->num_rows()>0){
                    $data_st = $cek_val->row();
                    if($data_st->ReceiveQty != $row->qty){
                        $data = array(
                            'DocEntry' =>$row->id_staging,
                            'LineNum' =>$data_st->LineNum,
                            'SKU'=>$row->kd_barang,
                            'ReceiveQty'=>$row->qty,
                            'SyncStatus'=>'N',
                            'CreateDate'=>date('Y-m-d H:i:s'),
                            'RcvDoc'=>$row->delivery_doc,
                            'RcvDate'=>$row->rcv_date,
                            'InbNo'=> $row->kd_inbound,
                            'FinaCode' => $row->FinaCode
                            );*/
                        
                        /*$result = $this->api_staging_model->_curl_process('update','f_t_inbound_l2',array('where'=>$data_where,'data'=>$data));
                        if(!empty($result)){
                            if($result->status == '001'){
                                $this->db->where($data_where);
                                $this->db->update('staging_inbound_l2',$data);
                            }
                        }*/

                   // }                    
                //}else{
                    //$data_where = array('DocEntry'=>$row->id_staging);
                    /*$cek_val = $this->db->get_where('staging_inbound_l2',$data_where);
                    if($cek_val->num_rows()>0){
                        $data_st = $cek_val->row();
                        $query = $data_st->LineNum;
                    }else{
                        $query = 0;
                    }*/
                    
                    
            /*    }
               
            }*/

            # code...
        }

        $this->load->model('api_sync_model');
        $this->api_sync_model->sync_qty();

    }

    function finish($receiving){
        $this->db->set("status_order_kitting", 3);
        $this->db->where("id_order_kitting", $receiving);
        $this->db->update("order_kitting");
    }

    function count_putaway($data){

        $this->db->select('id_order_kitting')
            ->from('receiving_barang')
            ->where('kd_unik',$data['kd_unik']);

        $idOrderKitting = $this->db->get()->row_array();

        $this->db->select('id_order_kitting')
            ->from('receiving_barang')
            ->where('loc_id',100)
            ->where('id_order_kitting',$idOrderKitting['id_order_kitting']);

        $res = $this->db->get()->num_rows();

        if($res < 1){
            return $idOrderKitting;
        }

        return false;
    }

    function count_putaway_po($data){
        $id_receiving   = $this->get_id("id_receiving", "kd_unik", $data["kd_unik"], "receiving_barang");
        if($id_receiving->num_rows()>0){
            $id_receiving = $id_receiving->row_array();
        }else{
            $id_receiving   = $this->get_id("id_receiving", "kd_parent", $data["kd_unik"], "receiving_barang")->row_array();
        }
        $id_barang      = $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();

        $this->db->select("kd_receiving, (SELECT COUNT(kd_unik) FROM receiving_barang WHERE id_receiving = '" . $id_receiving["id_receiving"] . "') AS qty, (SELECT COUNT(kd_unik) FROM receiving_barang WHERE id_receiving = '" . $id_receiving["id_receiving"] . "' AND user_id_putaway NOT IN (0)) AS putted");
        $this->db->from("receiving_barang rcvb");
        $this->db->join("receiving rcv", "rcvb.id_receiving = rcv.id_receiving", "left");
        $this->db->where("rcvb.id_receiving", $id_receiving["id_receiving"]);
        return $this->db->get();
    }

    function retrieve($serial_number){
    	$this->db->select("kd_barang, kd_unik");
    	$this->db->from("receiving_barang rcv");
    	$this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
    	$this->db->where("kd_unik", $serial_number);
    	return $this->db->get();
    }

    function retrieve_lp($serial_number){
        $this->db->select("kd_barang, kd_parent as kd_unik");
        $this->db->from("receiving_barang rcv");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->where("rcv.kd_parent", $serial_number);
        $this->db->limit(1);
        return $this->db->get();
    }

    function post($data){
    	$id_barang 		= $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
    	$id_loc 		= $this->get_id("loc_id", "loc_name", $data["loc_name"], "m_loc")->row_array();
    	$id_user 		= $this->get_id("user_id", "user_name", $data["user_name"], "hr_user")->row_array();
		$id_user_pick 	= $this->get_id("user_id", "user_name", $data["user_pick"], "hr_user")->row_array();

        $id_receiving   = $this->get_id("id_receiving", "kd_unik", $data["kd_unik"], "receiving_barang");
        if($id_receiving->num_rows()>0){
            $st_single = TRUE;
        }else{
            $st_single = FALSE;
        }
    	
        $this->db->set("loc_id", $id_loc["loc_id"]);
        $this->db->set("putaway_time", $data["putaway_time"]);
    	$this->db->set("user_id_putaway", $id_user["user_id"]);
    	$this->db->where("id_barang", $id_barang["id_barang"]);
        if($st_single){
    	   $this->db->where("kd_unik", $data["kd_unik"]);
        }else{
           $this->db->where("kd_parent", $data["kd_unik"]);
        }
    	$this->db->update("receiving_barang");

        $data_insert	= array(
									"kd_unik"		=> $data["kd_unik"],
									"loc_id_old"	=> 100,
									"user_id_pick"	=> $id_user_pick["user_id"],
									"pick_time"		=> $data["pick_time"],
									"loc_id_new"	=> $id_loc["loc_id"],
									"put_time"		=> $data["putaway_time"],
									"user_id_put"	=> $id_user["user_id"],
									"process_name"	=> "PUTAWAY"
									);
        $this->db->insert("int_transfer_detail", $data_insert);
    }

  

    function post_cancel($data){
        $id_barang = $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
        $this->db->set("loc_id", 100);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->where("id_barang", $id_barang["id_barang"]);
        $this->db->update("receiving_barang");
		
		$this->db->where("kd_unik", $data["kd_unik"]);
		$this->db->where("process_name", "PUTAWAY");
		$this->db->delete("int_transfer_detail");
    }

    function get_location($location){
        $avail_location = array("1", "3", "4"); // Putaway, put n pick, QC
        $this->db->select("*");
        $this->db->from("m_loc");
        $this->db->where("loc_name", $location);
        $this->db->where_in("loc_type_id", $avail_location);
        return $this->db->get();
    }

    function get_location_serial($location, $serial_number){
        $type   = $this->get_id("loc_type_id", "loc_name", $location, "m_loc")->row_array();
        $id_qc  = $this->get_id("id_qc", "kd_unik", $serial_number, "receiving_barang");
        $id_qc_lp  = $this->get_id("id_qc", "kd_parent", $serial_number, "receiving_barang");

        if($id_qc->num_rows() > 0){
           $id_qc = $id_qc->row_array();
        }else{
           $id_qc = $id_qc_lp->row_array();
        }
        if(($type["loc_type_id"] == "1" || $type["loc_type_id"] == "3") && $id_qc["id_qc"] == 1){ // item good
            return true;
        } elseif ($type["loc_type_id"] == "4" && $id_qc["id_qc"] == 2) { // item QC
            return true;
        } elseif ($type["loc_type_id"] == "5" && $id_qc["id_qc"] == 3) { // item NG
            return true;
        } else{
            return false;
        }
        
    }

    function cek_kategori($location, $serial_number){
        //$id_kategori             = $this->get_id("id_kategori", "kd_barang", $serial_number, "barang")->row_array();
        $this->db->select('id_kategori');
        $this->db->from('barang b');
        $this->db->join('receiving_barang rb','rb.id_barang = b.id_barang','left');
        $this->db->where('rb.kd_unik',$serial_number);
        $id_kategori = $this->db->get();
        if($id_kategori->num_rows()>0){
            $id_kategori = $id_kategori -> row_array();
        }else{
            $this->db->select('id_kategori');
            $this->db->from('barang b');
            $this->db->join('receiving_barang rb','rb.id_barang = b.id_barang','left');
            $this->db->where('rb.kd_parent',$serial_number);
            $id_kategori = $this->db->get()->row_array();
        }
        $id_loc_kategori         = $this->get_id("loc_category_id", "loc_name", $location, "m_loc")->row_array();
        
        if($id_kategori['id_kategori'] == $id_loc_kategori['loc_category_id']){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
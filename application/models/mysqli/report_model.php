<?php
class report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
	
	public function getMaterialOut($post = array()){
		$result = array();
		
		$where = '';
		$where2 = '';
		$where3 = '';
		$having = '';
		$groupBy = '';
		$groupBy2 = '';
		$groupBy3 = '';
		$orderBy = '';

		if(!empty($post['destination_id'])){
			$where .= ' AND outb.DestinationName = \''.$post['destination_id'].'\' ';
			$having .= ' AND destination =\''.$post['destination_id'].'\' ';
		}
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= ' AND DATE(pl.pl_date) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';

			$where2 .= ' AND DATE(ok.tanggal_order_kitting) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';
			
			$where3 .= ' AND DATE(cc.cc_time) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';
		}
						
		if(!empty($post['group_by'])){
			if($post['group_by'] == 'DATE'){
				$groupBy = ' DATE(pl.pl_date) ';
				$groupBy2 = ' DATE(ok.tanggal_order_kitting) ';
				$groupBy3 = ' DATE(cc.cc_time) ';
				$orderBy = 'date_sort';
			}else{
				$groupBy = ' outb.DestinationName ';
				$groupBy2 = ' destination ';
				$groupBy3 = ' destination ';
				$orderBy = 'destination';
			}
		}

		if(!empty($post['sn'])){
			$where .= " AND rcvbrg.kd_unik = '".$post['sn']."'";
			$where2 .= " AND okp.kd_unik = '".$post['sn']."'";
			$where3 .= " AND ccr.kd_unik = '".$post['sn']."'";
		}

		if(!empty($post['item'])){
			if($post['item'] !== 'null'){
				if(is_array($post['item'])){
					$item = (count($post['item']) > 1) ? implode(',',$post['item']) : $post['item'][0];
				}else{
					$item = $post['item'];
				}

				$where .= " AND brg.id_barang IN ($item)";
				$where2 .= " AND brg.id_barang IN ($item)";
				$where3 .= " AND brg.id_barang IN ($item)";
			}

		}
		
		 
		// --AND 
			// --shipp.st_shipping=\'1\'
		
		/*$sql = 'SELECT 
					DATE_FORMAT(shipp.shipping_datea, \'%d %b %Y\') AS tgl_load, kd_outbound, outb.id_customer,   
					(
						CASE  
							WHEN kd_supplier IS NULL 
								THEN CONCAT(customer_code, \' - \', customer_name)
							ELSE 
								CONCAT(kd_supplier, \' - \', nama_supplier)
						END
					) AS destination,
					pl_name, CONCAT(kd_barang, \' - \', nama_barang) AS item,
					outbrg.jumlah_barang AS doc_qty, 
					(
						SELECT 
							COUNT(id_barang) 
						FROM 
							receiving_barang 
						WHERE 
							id_barang=brg.id_barang 
						AND 
							shipping_id=shipp.shipping_id
					) AS load_qty, 
					IFNULL(pl.remark, \'\') AS remark, IFNULL(nama, \'-\') AS nama, 
					IFNULL(nama_satuan, \'\') AS nama_satuan    
				FROM 
					shipping shipp 
				JOIN shipping_picking sp 
					ON sp.shipping_id=shipp.shipping_id 
				JOIN picking_list pl 
					ON pl.pl_id=sp.pl_id
				JOIN picking_list_outbound plo 
					ON plo.pl_id=sp.pl_id 
				JOIN outbound outb 
					ON outb.id_outbound=plo.id_outbound
				JOIN outbound_barang outbrg 
					ON outbrg.id_outbound=outb.id_outbound 
				JOIN barang brg 
					ON brg.id_barang=outbrg.id_barang
				LEFT JOIN receiving_barang rcvbrg 
					ON rcvbrg.shipping_id=shipp.shipping_id 
						AND rcvbrg.id_barang=brg.id_barang
				LEFT OUTER JOIN supplier spl 
					ON spl.id_supplier=outb.id_customer 
						AND outb.id_outbound_document != \'2\'
				LEFT OUTER JOIN m_customer cust 
					ON cust.id_customer=outb.id_customer 
						AND outb.id_outbound_document = \'2\' 
				LEFT JOIN hr_user hu 
					ON hu.user_id=rcvbrg.user_id_shipping 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE 1=1'.$where.'
				GROUP BY 
					'.$groupBy.', rcvbrg.id_receiving, rcvbrg.id_barang
				ORDER BY 
					'.$groupBy.' DESC'; */

		// $sql = "SELECT
		// 			outb.id_outbound,
		// 			DATE_FORMAT(
		// 				pl.pl_date,
		// 				'%d %b %Y'
		// 			)AS tgl_load,
		// 			kd_outbound,
		// 			outb.id_customer,
		// 			outb.DestinationName as destination,
		// 			pl_name,
		// 			CONCAT(
		// 				kd_barang,
		// 				' - ',
		// 				nama_barang
		// 			)AS item,
		// 			outbrg.jumlah_barang AS doc_qty,
		// 			pr.qty AS load_qty,
		// 			IFNULL(pl.remark, '')AS remark,
		// 			IFNULL(nama, '-')AS nama,
		// 			IFNULL(nama_satuan, '')AS nama_satuan,
		// 			IFNULL(packing_number, '') AS packing_number,
		// 			IFNULL(inc, '') AS inc_packing_number
		// 		FROM
		// 			picking_list pl
		// 		JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
		// 		JOIN outbound outb ON outb.id_outbound = plo.id_outbound
		// 		JOIN outbound_barang outbrg ON outbrg.id_outbound = outb.id_outbound
		// 		left join picking_recomendation pr on pr.id_picking=pl.pl_id and pr.id_barang=outbrg.id_barang
		// 		left JOIN barang brg ON brg.id_barang = outbrg.id_barang
		// 		LEFT JOIN ( select id_outbound, packing_number, max(inc) as inc from outbound_receiving_barang GROUP BY id_outbound ) as orb ON orb.id_outbound = outb.id_outbound
		// 		LEFT JOIN receiving_barang rcvbrg ON rcvbrg.pl_id = pl.pl_id
		// 		AND rcvbrg.id_barang = brg.id_barang
		// 		LEFT OUTER JOIN supplier spl ON spl.id_supplier = outb.id_customer
		// 		AND outb.id_outbound_document != '2'
		// 		LEFT OUTER JOIN m_customer cust ON cust.id_customer = outb.id_customer
		// 		AND outb.id_outbound_document = '2'
		// 		LEFT JOIN hr_user hu ON hu.user_id = rcvbrg.user_id_shipping
		// 		LEFT JOIN satuan sat ON sat.id_satuan = brg.id_satuan
		// 		WHERE 1=1 and kd_barang='FB01000006' ".$where."
		// 		GROUP BY 
		// 			".$groupBy.", outb.id_outbound, outbrg.id_barang
		// 		ORDER BY 
		// 			".$groupBy." DESC";

		$sql1 = "
				SELECT
					outb.id_outbound,
					DATE_FORMAT(
						pl.pl_date,
						'%d %b %Y'
					)AS tgl_load,
					kd_outbound,
					outb.id_customer,
				outb.DestinationName as destination,

					pl_name,
					CONCAT(
						kd_barang,
						' - ',
						nama_barang
					)AS item,
					outbrg.jumlah_barang AS doc_qty,
					(
						SELECT
							IFNULL(sum(pr.qty),0)
						FROM
							picking_recomendation pr
						LEFT JOIN 
							outbound_barang_picked obp
							ON pr.kd_unik=obp.kd_unik
						WHERE
							pr.id_barang = brg.id_barang
						AND id_picking = pl.pl_id
						AND obp.id_outbound = outb.id_outbound
					)AS load_qty,
					IFNULL(pl.remark, '')AS remark,
					IFNULL(nama, '-')AS nama,
					IFNULL(nama_satuan, '-')AS nama_satuan,
					IFNULL(packing_number, '') AS packing_number,
					IFNULL(inc, '') AS inc_packing_number,
					pl.pl_date as date_sort,
					IFNULL(NULL,0) as system_qty,
					IFNULL(NULL,0) as scanned_qty
				FROM
					picking_list pl
				JOIN picking_list_outbound plo ON plo.pl_id = pl.pl_id
				JOIN outbound outb ON outb.id_outbound = plo.id_outbound
				JOIN outbound_barang outbrg ON outbrg.id_outbound = outb.id_outbound
				JOIN barang brg ON brg.id_barang = outbrg.id_barang
				LEFT JOIN ( select id_outbound, packing_number, max(inc) as inc from outbound_receiving_barang GROUP BY id_outbound ) as orb ON orb.id_outbound = outb.id_outbound
				LEFT JOIN receiving_barang rcvbrg ON rcvbrg.pl_id = pl.pl_id
				AND rcvbrg.id_barang = brg.id_barang
				LEFT OUTER JOIN supplier spl ON spl.id_supplier = outb.id_customer
				AND outb.id_outbound_document != '2'
				LEFT OUTER JOIN m_customer cust ON cust.id_customer = outb.id_customer
				AND outb.id_outbound_document = '2'
				LEFT JOIN hr_user hu ON hu.user_id = rcvbrg.user_id_shipping
				LEFT JOIN satuan sat ON sat.id_satuan = brg.id_satuan
				WHERE 1=1 ".$where."
			GROUP BY 
				".$groupBy.", outb.id_outbound, outbrg.id_barang, pl.pl_id
		";

		$sql2 = "
			SELECT
				ok.id_order_kitting as id_outbound,
				DATE_FORMAT(
					ok.tanggal_order_kitting,
					'%d %b %Y'
				)AS tgl_load,
				kd_order_kitting as kd_outbound,
				'-' as id_customer,
				'-' as destination,
				'-' as pl_name,
				CONCAT(
					brg.kd_barang,
					' - ',
					brg.nama_barang
				)AS item,
				okbd.picking_doc_qty AS doc_qty,
				IFNULL(sum(okp.qty),0) AS load_qty,
				'-'AS remark,
				IFNULL(nama, '-')AS nama,
				IFNULL(nama_satuan, '')AS nama_satuan,
				IFNULL(NULL, '') AS packing_number,
				IFNULL(NULL, '') AS inc_packing_number,
				ok.tanggal_order_kitting as date_sort,
				IFNULL(NULL,0) as system_qty,
				IFNULL(NULL,0) as scanned_qty
			FROM
				order_kitting ok
			JOIN order_kitting_picking okp
				ON okp.id_order_kitting = ok.id_order_kitting
			JOIN receiving_barang rb
				ON rb.kd_unik=okp.kd_unik
			JOIN order_kitting_barang_details okbd
				ON okbd.id_order_kitting = ok.id_order_kitting AND okbd.id_barang=rb.id_barang
			LEFT JOIN barang brg
				ON brg.id_barang = rb.id_barang
			LEFT JOIN hr_user hu 
				ON hu.user_id = rb.user_id_kitting
			LEFT JOIN satuan sat 
				ON sat.id_satuan = brg.id_satuan
			WHERE
			1=1
				$where2
			GROUP BY 
				$groupBy2 , ok.id_order_kitting, rb.id_barang
			HAVING
				1=1
				$having
		";

		$sql3 = "
			SELECT
				cc.cc_id as id_outbound,
				DATE_FORMAT(
					cc.cc_time,
					'%d %b %Y'
				)AS tgl_load,
				cc_code as kd_outbound,
				'-' as id_customer,
				'-' as destination,
				'-' as pl_name,
				CONCAT(
					brg.kd_barang,
					' - ',
					brg.nama_barang
				)AS item,
				IFNULL(NULL,0) AS doc_qty,
				IFNULL((sum(system_qty) - sum(scanned_qty)),0) AS load_qty,
				remark AS remark,
				IFNULL(nama, '-')AS nama,
				IFNULL(nama_satuan, '')AS nama_satuan,
				IFNULL(NULL, '') AS packing_number,
				IFNULL(NULL, '') AS inc_packing_number,
				cc.cc_time as date_sort,
				IFNULL(sum(system_qty),0) as system_qty,
				IFNULL(sum(scanned_qty),0) as scanned_qty
			FROM
				cycle_count cc
			JOIN cycle_count_result ccr
				ON ccr.cc_id = cc.cc_id
			JOIN receiving_barang rb
				ON rb.kd_unik=ccr.kd_unik
			LEFT JOIN barang brg
				ON brg.id_barang = rb.id_barang
			LEFT JOIN hr_user hu 
				ON hu.user_id = cc.user_id
			LEFT JOIN satuan sat 
				ON sat.id_satuan = brg.id_satuan
			WHERE
				1=1
			AND
				ccr.status = 'ADJUST TO SCANNED QTY'
			$where3
			GROUP BY 
				$groupBy3 , cc.cc_id, ccr.id_barang
			HAVING
				1=1
			AND
				(system_qty > scanned_qty)
			$having
		";


		$sql = "SELECT * FROM ( ($sql1)UNION($sql2)UNION($sql3) ) as tbl ORDER BY $orderBy DESC";

		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		$dt = '';
		$j = -1;
		$data = array();
		
		//var_dump($row);
		
		if($post['group_by'] == 'DATE'){
		
			for($i = 0; $i < $len; $i++){
				
				if($dt != $row[$i]['tgl_load']){
					$dt = $row[$i]['tgl_load'];
					$j++;
				}
				
				if($dt == $row[$i]['tgl_load']){
					
					$name = $row[$i]['nama'];
					/*
					if(strpos($name, ' ')){
						$expl = explode(' ', $name);
						$name = substr($expl[0], 0, 1) . substr($expl[1], 0, 1);
						$name = strtoupper($name);
					}else{
						$name = strtoupper(substr($name, 0, 1));
					}
					*/
					$pcCode = (!empty($row[$i]['packing_number'])) ? $this->get_pc_code($row[$i]['packing_number'], $row[$i]['inc_packing_number']) : '-';
					$data[$j][$dt][] = array(
						'tgl_outbound'		=> $row[$i]['tgl_load'],
						'kd_outbound'		=> $row[$i]['kd_outbound'],
						'destination'		=> $row[$i]['destination'],
						'pl_name'			=> $row[$i]['pl_name'],
						'packing_number'	=> $pcCode,
						'item'				=> $row[$i]['item'],
						'doc_qty'			=> $row[$i]['doc_qty'],
						'load_qty'			=> $row[$i]['load_qty'],
						//'disc'				=> $row[$i]['disc'],
						'remark'			=> $row[$i]['remark'],
						'nama'				=> $name,
						'nama_satuan'		=> $row[$i]['nama_satuan'],
					);
				}
				
			}
			
		}else{
			
			for($i = 0; $i < $len; $i++){
				
				if($dt != $row[$i]['destination']){
					$dt = $row[$i]['destination'];
					$j++;
				}
				
				if($dt == $row[$i]['destination']){
					
					$name = $row[$i]['nama'];
					/*
					if(strpos($name, ' ')){
						$expl = explode(' ', $name);
						$name = substr($expl[0], 0, 1) . substr($expl[1], 0, 1);
						$name = strtoupper($name);
					}else{
						$name = strtoupper(substr($name, 0, 1));
					}
					*/
					$pcCode = (!empty($row[$i]['packing_number'])) ? $this->get_pc_code($row[$i]['packing_number'], $row[$i]['inc_packing_number']) : '-';
					$data[$j][$row[$i]['destination']][] = array(
						'tgl_outbound'	=> $row[$i]['tgl_load'],
						'kd_outbound'	=> $row[$i]['kd_outbound'],
						'destination'	=> $row[$i]['destination'],
						'pl_name'		=> $row[$i]['pl_name'],
						'packing_number'=> $pcCode,
						'item'			=> $row[$i]['item'],
						'doc_qty'		=> $row[$i]['doc_qty'],
						'load_qty'		=> $row[$i]['load_qty'],
						//'disc'			=> $row[$i]['disc'],
						'remark'		=> $row[$i]['remark'],
						'nama'			=> $name,
						'nama_satuan'	=> $row[$i]['nama_satuan'],
					);
				}
				
			}
			
		}
			
		$result['status'] = 'OK';
		$result['data'] = $data;
						
		return $result;
	}

    public function get_pc_code($pcCode, $inc){
    	$pc_code = '';
    	$pc_string_tmp = '';
    	$pad = ($inc < 10) ? 0 : '';
    	for($i=1;$i<=$inc;$i++){
	    	$pc_string_tmp = ($i == $inc || ($i == 1 && $inc <= 1 ) )? '' : ',';
            $pc_code .= $pcCode.$pad.$i.$pad.$inc. $pc_string_tmp .'</br>';
    	}
    	return $pc_code;
    }
	
	public function getMaterialIn($post = array()){
		$result = array();
		
		$where = '';
		$wheres = '';

		$where2 = '';
		$where3 = '';
		$groupBy = '';
		$groupBy3 = '';
		$orderBy = '';
		$having = '';
		
		if(!empty($post['source_id'])){
			$wheres .= " AND inb.id_supplier = '".$post['source_id']."' ";
			$having .= " AND id_supplier = '".$post['source_id']."' ";
		}

		if(!empty($post['sn'])){
			$wheres .= " AND rcvbrg.kd_unik = '".$post['sn']."' ";
			$where2 .= " AND rb.kd_unik = '".$post['sn']."' ";
			$where3 .= " AND ccr.kd_unik = '".$post['sn']."' ";
		}

		if(!empty($post['item'])){
			if($post['item'] !== 'null'){
				$item = (count($post['item']) > 1) ? implode(',',$post['item']) : $post['item'][0];
				$wheres .= " AND brg.id_barang IN ($item)";
				$where2 .= " AND brg.id_barang IN ($item)";
				$where3 .= " AND brg.id_barang IN ($item)";
			}

		}
		
		if(!empty($post['from']) && !empty($post['to'])){
			$where .= ' AND DATE(rcv.tanggal_receiving) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';
			$tanggal_from = date('Y-m-d',strtotime(str_replace('/','-',$post['from'])));
			$tanggal_to = date('Y-m-d',strtotime(str_replace('/','-',$post['to'])));

			$where2 .= ' AND DATE(ok.tanggal_order_kitting) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';

			$where3 .= ' AND DATE(cc.cc_time) 
						BETWEEN \''.date('Y-m-d', strtotime(str_replace('/','-',$post['from']))).'\' AND \''.date('Y-m-d', strtotime(str_replace('/','-',$post['to']))).'\' ';

		}
						
		if(!empty($post['group_by'])){
			if($post['group_by'] == 'DATE'){
				$groupBy = ' ok.id_order_kitting';
				$groupBy3 = 'cc.cc_id';
				$orderBy = 'date_sort';
			}else{
				$groupBy = 'id_supplier';
				$groupBy3 = 'id_supplier';
				$orderBy = 'id_supplier';
			}
		}

		// $sql = 'SELECT 
		// 			DATE_FORMAT(rcvbrg.tgl_in, \'%d %b %Y\') AS tgl_in, kd_inbound, inb.id_supplier,   
		// 			(
		// 				CASE  
		// 					WHEN kd_supplier IS NULL 
		// 						THEN CONCAT(customer_code, \' - \', customer_name)
		// 					ELSE 
		// 						CONCAT(kd_supplier, \' - \', nama_supplier)
		// 				END
		// 			) AS source,
		// 			kd_receiving, CONCAT(kd_barang, \' - \', nama_barang) AS item,
		// 			rcvqty.qty AS doc_qty, 
		// 			IF(brg.has_qty = 0, COUNT(rcvbrg.id_barang), IFNULL(SUM(rcvbrg.first_qty),0)) AS rec_qty, 
		// 			(rcvqty.qty - IF(brg.has_qty = 0, COUNT(rcvbrg.id_barang), IFNULL(SUM(rcvbrg.first_qty),0))) AS disc, IFNULL(rcv.remark, \'\') AS remark, IFNULL(nama, \'-\') AS nama, 
		// 			IFNULL(nama_satuan, \'\') AS nama_satuan    
		// 		FROM 
		// 			receiving rcv 
		// 		JOIN inbound inb 
		// 			ON inb.id_inbound=rcv.id_po 
		// 		JOIN inbound_barang inbbrg 
		// 			ON inbbrg.id_inbound=inb.id_inbound 
		// 		JOIN barang brg 
		// 			ON brg.id_barang=inbbrg.id_barang
		// 		JOIN receiving_qty rcvqty 
		// 			ON rcvqty.id_receiving=rcv.id_receiving  
		// 				AND rcvqty.id_barang=brg.id_barang
		// 		LEFT JOIN receiving_barang rcvbrg 
		// 			ON rcvbrg.id_receiving=rcv.id_receiving 
		// 				AND rcvbrg.id_barang=brg.id_barang
		// 		LEFT OUTER JOIN supplier spl 
		// 			ON spl.id_supplier=inb.id_supplier 
		// 				AND inb.id_inbound_document NOT IN (\'2\', \'7\')
		// 		LEFT OUTER JOIN m_customer cust 
		// 			ON cust.id_customer=inb.id_supplier 
		// 				AND inb.id_inbound_document IN (\'2\', \'7\')
		// 		LEFT JOIN hr_user hu 
		// 			ON hu.user_id=rcvbrg.user_id_receiving 
		// 		LEFT JOIN satuan sat 
		// 			ON sat.id_satuan=brg.id_satuan 
		// 		WHERE 1=1'.$where. $wheres.'
		// 		GROUP BY 
		// 			'.$groupBy.', rcvbrg.id_receiving, rcvbrg.id_barang, rcvqty.id_barang
		// 		ORDER BY 
		// 			'.$groupBy.' DESC '; 
		if(!empty($post['group_by'])){
			if($post['group_by'] == 'DATE'){
				$groupBy = 'ok.id_order_kitting';
				$orderBy = 'date_sort';

				$sql1 = "
				SELECT
					DATE_FORMAT(
						rcv.tanggal_receiving,
						'%d %b %Y'
					)AS tgl_in,
					kd_inbound,
					inb.id_supplier,
					CONCAT(
						kd_supplier,
						' - ',
						nama_supplier
					)AS source,
					kd_receiving,
					CONCAT(
						kd_barang,
						' - ',
						nama_barang
					)AS item,
					kd_barang as item_code,
					nama_barang as item_name,
					rcvqty.qty AS doc_qty,
					IFNULL(tr.qty,0) AS rec_qty,
					(rcvqty.qty - IFNULL(tr.qty,0))AS disc,
					IFNULL(rcv.remark, '')AS remark,
					IFNULL(nama, '-')AS nama,
					IFNULL(nama_satuan, '')AS nama_satuan,
					DATE(rcv.tanggal_receiving) AS date_sort,
					IFNULL(NULL,0) as system_qty,
					IFNULL(NULL,0) as scanned_qty
				FROM
					receiving rcv
				JOIN inbound inb ON inb.id_inbound = rcv.id_po
				JOIN inbound_barang inbbrg ON inbbrg.id_inbound = inb.id_inbound
				JOIN barang brg ON brg.id_barang = inbbrg.id_barang
				JOIN receiving_qty rcvqty ON rcvqty.id_receiving = rcv.id_receiving
				AND rcvqty.id_barang = brg.id_barang
				LEFT JOIN receiving_barang rcvbrg ON rcvbrg.id_receiving=rcv.id_receiving
				AND rcvbrg.id_barang=rcvqty.id_barang
				LEFT OUTER JOIN supplier spl ON spl.id_supplier = inb.id_supplier
				AND inb.id_inbound_document NOT IN('2', '7')
				
				LEFT JOIN satuan sat ON sat.id_satuan = brg.id_satuan
				LEFT JOIN(
					SELECT
						rb.id_receiving,
						sum(first_qty)AS qty,
						rb.id_barang,
						max(user_id_receiving) as user_id
					FROM
						receiving_barang rb
					LEFT JOIN receiving rcv ON rcv.id_receiving = rb.id_receiving
					WHERE 1=1
						".$where."
					GROUP BY
						rb.id_receiving,
						rb.id_barang
				)tr ON tr.id_receiving = rcv.id_receiving and tr.id_barang = brg.id_barang
				LEFT JOIN hr_user hu ON hu.user_id = tr.user_id
				WHERE
					1 = 1
				".$where." and rcvqty.qty > 0 
				".$wheres."
				ORDER BY
					DATE(rcv.tanggal_receiving)DESC";
			}else{

				$sql1 = "
				SELECT
					DATE_FORMAT(
						rcv.tanggal_receiving,
						'%d %b %Y'
					)AS tgl_in,
					kd_inbound,
					inb.id_supplier,
					CONCAT(
						kd_supplier,
						' - ',
						nama_supplier
					)AS source,
					kd_receiving,
					CONCAT(
						kd_barang,
						' - ',
						nama_barang
					)AS item,
					kd_barang as item_code,
					nama_barang as item_name,
					rcvqty.qty AS doc_qty,
					IFNULL(tr.qty,0) AS rec_qty,
					(rcvqty.qty - IFNULL(tr.qty,0))AS disc,
					IFNULL(rcv.remark, '')AS remark,
					IFNULL(nama, '-')AS nama,
					IFNULL(nama_satuan, '')AS nama_satuan,
					DATE(rcv.tanggal_receiving) AS date_sort,
					IFNULL(NULL,0) as system_qty,
					IFNULL(NULL,0) as scanned_qty
				FROM
					receiving rcv
				JOIN inbound inb ON inb.id_inbound = rcv.id_po
				JOIN inbound_barang inbbrg ON inbbrg.id_inbound = inb.id_inbound
				JOIN barang brg ON brg.id_barang = inbbrg.id_barang
				JOIN receiving_qty rcvqty ON rcvqty.id_receiving = rcv.id_receiving
				AND rcvqty.id_barang = brg.id_barang
				LEFT JOIN receiving_barang rcvbrg ON rcvbrg.id_receiving=rcv.id_receiving
				AND rcvbrg.id_barang=rcvqty.id_barang
				LEFT OUTER JOIN supplier spl ON spl.id_supplier = inb.id_supplier
				AND inb.id_inbound_document NOT IN('2', '7')
				
				LEFT JOIN satuan sat ON sat.id_satuan = brg.id_satuan
				LEFT JOIN(
					SELECT
						inb.id_supplier,
						sum(first_qty)AS qty,
						rb.id_barang,
						max(user_id_receiving) as user_id
					FROM
						receiving_barang rb
					LEFT JOIN receiving rcv ON rcv.id_receiving = rb.id_receiving
					LEFT JOIN inbound inb ON inb.id_inbound=rcv.id_po 
					WHERE 1=1
						".$where."
					GROUP BY
						rb.id_barang,
						inb.id_supplier

				)tr ON tr.id_supplier = inb.id_supplier and tr.id_barang = brg.id_barang
				LEFT JOIN hr_user hu ON hu.user_id = tr.user_id
				WHERE
					1 = 1
				".$where." and rcvqty.qty > 0 
				".$wheres."
				ORDER BY
					inb.id_supplier DESC";
			}
		}

		$sql2 = "
					SELECT
						DATE_FORMAT(
							ok.tanggal_order_kitting,
							'%d %b %Y'
						)AS tgl_in,
						kd_order_kitting as kd_inbound,
						'-' as id_supplier,
						'-' AS source,
						kd_order_kitting as kd_receiving,
						CONCAT(
							brg.kd_barang,
							' - ',
							brg.nama_barang
						)AS item,
						brg.kd_barang as item_code,
						brg.nama_barang as item_name,
						okbd.tally_doc_qty AS doc_qty,
						sum(rb.first_qty) AS rec_qty,
						'0' AS disc,
						IFNULL(NULL, '')AS remark,
						IFNULL(nama, '-')AS nama,
						IFNULL(nama_satuan, '')AS nama_satuan,
						DATE(ok.tanggal_order_kitting) AS date_sort,
						IFNULL(NULL,0) as system_qty,
						IFNULL(NULL,0) as scanned_qty
					FROM
						order_kitting ok
					JOIN receiving_barang rb
						ON rb.id_order_kitting=ok.id_order_kitting
					JOIN order_kitting_barang_details okbd
						ON okbd.id_order_kitting=ok.id_order_kitting AND okbd.id_barang=rb.id_barang
					LEFT JOIN barang brg
						ON brg.id_barang=rb.id_barang
					LEFT JOIN satuan st
						ON st.id_satuan=brg.id_satuan
					LEFT JOIN hr_user usr
						ON usr.user_id=ok.user_id
					WHERE
						rb.loc_id NOT IN (100)
					$where2
					GROUP BY 
						$groupBy , rb.id_barang
					HAVING
						1=1
					$having
					";

		$sql3 = "
					SELECT
						DATE_FORMAT(
							cc.cc_time,
							'%d %b %Y'
						)AS tgl_in,
						cc.cc_code as kd_inbound,
						'-' as id_supplier,
						'-' AS source,
						cc.cc_code as kd_receiving,
						CONCAT(
							brg.kd_barang,
							' - ',
							brg.nama_barang
						)AS item,
						brg.kd_barang as item_code,
						brg.nama_barang as item_name,
						IFNULL(NULL,0) AS doc_qty,
						IFNULL((sum(scanned_qty) - sum(system_qty)),0) AS rec_qty,
						IFNULL((sum(scanned_qty) - sum(system_qty)),0) AS disc,
						IFNULL(NULL, '')AS remark,
						IFNULL(nama, '-')AS nama,
						IFNULL(nama_satuan, '')AS nama_satuan,
						DATE(cc.cc_time) AS date_sort,
						IFNULL(sum(system_qty),0) as system_qty,
						IFNULL(sum(scanned_qty),0) as scanned_qty
					FROM
						cycle_count cc
					JOIN cycle_count_result ccr
						ON ccr.cc_id=cc.cc_id
					JOIN receiving_barang rb
						ON rb.kd_unik=ccr.kd_unik
					LEFT JOIN barang brg
						ON brg.id_barang=rb.id_barang
					LEFT JOIN satuan st
						ON st.id_satuan=brg.id_satuan
					LEFT JOIN hr_user usr
						ON usr.user_id=cc.user_id
					WHERE
						rb.loc_id NOT IN (100)
					AND
						ccr.status = 'ADJUST TO SCANNED QTY'
					$where3
					GROUP BY 
						$groupBy3 , rb.id_barang
					HAVING
						1=1
					AND
						(system_qty < scanned_qty)
					$having
					";

		$sql = "SELECT * FROM ( ($sql1)UNION($sql2)UNION($sql3) ) as tbl ORDER BY $orderBy DESC";

		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		$dt = '';
		$j = -1;
		$data = array();
		
		//var_dump($row);
		
		if($post['group_by'] == 'DATE'){
		
			for($i = 0; $i < $len; $i++){
				
				if($dt != $row[$i]['tgl_in']){
					$dt = $row[$i]['tgl_in'];
					$j++;
				}
				
				if($dt == $row[$i]['tgl_in']){
					
					$name = $row[$i]['nama'];
					/*
					if(strpos($name, ' ')){
						$expl = explode(' ', $name);
						$name = substr($expl[0], 0, 1) . substr($expl[1], 0, 1);
						$name = strtoupper($name);
					}else{
						$name = strtoupper(substr($name, 0, 1));
					}
					*/
					$data[$j][$dt][] = array(
						'tgl_inbound'	=> $row[$i]['tgl_in'],
						'kd_inbound'	=> $row[$i]['kd_inbound'],
						'source'		=> $row[$i]['source'],
						'kd_receiving'	=> $row[$i]['kd_receiving'],
						'item_code'		=> $row[$i]['item_code'],
						'item_name'		=> $row[$i]['item_name'],
						'item'			=> $row[$i]['item'],
						'doc_qty'		=> $row[$i]['doc_qty'],
						'rec_qty'		=> $row[$i]['rec_qty'],
						'disc'			=> ($row[$i]['doc_qty'] - $row[$i]['rec_qty']),
						'remark'		=> $row[$i]['remark'],
						'nama'			=> $name,
						'nama_satuan'	=> $row[$i]['nama_satuan'],
					);
				}
				
			}
			
		}else{
			
			for($i = 0; $i < $len; $i++){
				
				if($dt != $row[$i]['id_supplier']){
					$dt = $row[$i]['id_supplier'];
					$j++;
				}
				
				if($dt == $row[$i]['id_supplier']){
					
					$name = $row[$i]['nama'];
					/*
					if(strpos($name, ' ')){
						$expl = explode(' ', $name);
						$name = substr($expl[0], 0, 1) . substr($expl[1], 0, 1);
						$name = strtoupper($name);
					}else{
						$name = strtoupper(substr($name, 0, 1));
					}
					*/
					$data[$j][$row[$i]['source']][] = array(
						'tgl_inbound'	=> $row[$i]['tgl_in'],
						'kd_inbound'	=> $row[$i]['kd_inbound'],
						'source'		=> $row[$i]['source'],
						'kd_receiving'	=> $row[$i]['kd_receiving'],
						'item_code'		=> $row[$i]['item_code'],
						'item_name'		=> $row[$i]['item_name'],
						'item'			=> $row[$i]['item'],
						'doc_qty'		=> $row[$i]['doc_qty'],
						'rec_qty'		=> $row[$i]['rec_qty'],
						'disc'			=> ($row[$i]['doc_qty'] - $row[$i]['rec_qty']),
						'remark'		=> $row[$i]['remark'],
						'nama'			=> $name,
						'nama_satuan'	=> $row[$i]['nama_satuan'],
					);
				}
				
			}
			
		}
			
		$result['status'] = 'OK';
		$result['data'] = $data;
						
		return $result;
	}
	
	public function getSource($type=""){
		$result = array();

		if($type=="in"){
		$sql = '(
					SELECT 
						id_supplier AS id, kd_supplier AS code, nama_supplier AS name, 
						\'supplier\' AS type
					FROM 
						supplier 
					ORDER BY 
						CAST(kd_supplier AS UNSIGNED) ASC
				) 
				UNION
				(
					SELECT 
						id_customer AS id, customer_code AS code, customer_name AS name, 
						\'customer\' AS type						
					FROM 
						m_customer
					ORDER BY 
						id_customer ASC
				)';

		$row = $this->db->query($sql)->result_array();
		$len = count($row);
		for($i = 0; $i < $len; $i++){
			if($row[$i]['type'] == 'supplier')
				$result['supplier'][] = $row[$i];
			else
				$result['customer'][] = $row[$i];
		}
				
		}else{

			$sql = "
				SELECT DestinationName as destination FROM outbound GROUP BY DestinationName ORDER BY DestinationName ASC;
			";

			$result = $this->db->query($sql)->result_array();

		}
		
		return $result;
	}
	
}
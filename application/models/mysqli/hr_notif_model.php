<?php
class hr_notif_model extends CI_Model {

	var $details;
	public $limit;
	public $offset;
	private $table = 'hr_notif';
    

/*	public function __construct()
	{
		$this->load->database();
	}*/
	
	
	private function data($condition = array()) {
	    $this->db->from($this->table . ' a');
	    $this->db->where_condition($condition);
    
	    return $this->db;
	}
    
	public function get_by_id($id) {
	    $condition['a.id'] = $id;
	    $this->data($condition);
	    return $this->db->get();
	}
    
	public function get_data($condition = array()) {
	    $this->data($condition);
	    return $this->db->get();
	}	

	public function create($data) {
	    return $this->db->insert($this->table, $data);
	}
    
	public function update($data, $id) {
	    return $this->db->update($this->table, $data, array('id' => $id));
	}
    
	public function delete($data) {
	    return $this->db->delete($this->table, $data);
	}

}

?>
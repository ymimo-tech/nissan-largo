<?php 
	
class setting_model extends CI_model{

	public function __construct(){
		parent::__construct();
	}

	public function getData(){
		$result = array();
		
		$sql = 'SELECT 
					* 
				FROM 
					setting';
					
		$r = $this->db->query($sql)->result_array();
		foreach($r as $k => $v){
			$result[$v['module']] = $v['value'];
		}
		
		return $result;
	}
	
	public function save($post = array()){
		$result = array();
		
		$sql = 'UPDATE 
					setting 
				SET 
					value=\''.$post['peminjaman'].'\' 
				WHERE 
					module=\'peminjaman\'';
					
		$q = $this->db->query($sql);
		
		if($q){
			$result['message'] = 'Update setting success';
		}else{
			$result['message'] = 'Update setting failed';
		}
		
		return $result;
	}
	
}
<?php

class Api_setting_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function user_validation($uname, $sessionCode) {
    	return $this->db->get_where("hr_user", array("user_name" => $uname, "handheld_session_code" => $sessionCode));
    }

    function user_authentication(){
        $uname 		= $this->input->get_request_header('User', true);
        $session    = $this->input->get_request_header('Authorization', true);

        $validation = $this->user_validation($uname, $session)->row_array();
        if($validation){
            return true;
        } else{
            return false;
        }
    }

    function user_login($data){
    	return $this->db->get_where("hr_user", array("user_name" => $data["uname"], "user_password" => $data["upass"]));
    }

    function user_session($uid, $session_code) {
    	$data["handheld_session_code"] = $session_code;
    	$this->db->where("user_id", $uid);
    	$this->db->update("hr_user", $data);
    }

    function user_login_detail($uname, $session){
    	$this->db->select("user_id");
		$this->db->select("user_name");
    	$this->db->select("handheld_session_code");
    	$this->db->from("hr_user");
    	$this->db->where("user_name", $uname);
    	$this->db->where("handheld_session_code", $session);
    	return $this->db->get();
    }
	
	function get_update($customer){
		$this->db->select("*");
		$this->db->from("update_handheld");
		$this->db->where("target", $customer);
		$this->db->order_by("id", "DESC");
		$this->db->limit(1);
		return $this->db->get();
	}

}
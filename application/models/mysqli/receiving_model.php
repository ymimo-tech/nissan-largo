<?php
class receiving_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'receiving';
    private $table2 = 'supplier';
    private $table3 = 'inbound';
    private $table4 = 'receiving_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';
    private $table8 = 'supplier_barang';
    private $po_barang = 'purchase_order_barang';
    private $kurs   = 'kurs';
	
	public function getStatus(){
		$result = array();
		
		$sql = 'SELECT 
					* 
				FROM 
					m_status_receiving';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}

	public function updateInboundStatus($idInbound){
		$result = array();

		$sql = 'UPDATE
					inbound
				SET
					id_status_inbound=\'3\'
				WHERE
					id_inbound=\''.$idInbound.'\'';

		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Update status inbound success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Update status inbound failed';
		}

		return $result;
	}

	public function finishTally($post = array()){
		$result = array();
		$rectime = date('Y-m-d H:i:s');

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					receiving_barang rcvbrg
				JOIN receiving rcv
					ON rcv.id_receiving=rcvbrg.id_receiving
				WHERE
					rcvbrg.id_receiving=\''.$post['id_receiving'].'\'';

		$row = $this->db->query($sql)->row_array();

		if($row['total'] <= 0){

			$result['status'] = 'ERR';
			$result['message'] = 'Cannot finish tally, because process tally not started yet.';

		}else{

			$sql = 'SELECT
						start_tally, finish_tally
					FROM
						receiving
					WHERE
						id_receiving=\''.$post['id_receiving'].'\'';

			$row = $this->db->query($sql)->row_array();
			$start = $row['start_tally'];
			$finish = $row['finish_tally'];

			if(empty($start)){

				$result['status'] = 'ERR';
				$result['message'] = 'Cannot finish tally, because process tally not started yet.';

			}else if(!empty($finish)){

				$result['status'] = 'ERR';
				$result['message'] = 'Process tally already finished.';

			}else{

				$set = '';

				if(!empty($post['remark']))
					$set .= ', remark=\''.$post['remark'].'\' ';

				$sql = 'UPDATE receiving SET finish_tally=\''.$rectime.'\', st_receiving=\'1\' '.$set.'
						WHERE id_receiving=\''.$post['id_receiving'].'\'';
				$q = $this->db->query($sql);

				if($q){
					$result['status'] = 'OK';
					$result['message'] = 'Finish tally success';
				}else{
					$result['status'] = 'ERR';
					$result['message'] = 'Finish tally failed';
				}

			}

		}

		return $result;
	}

	public function getPutAwayList($post = array()){

		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_inbound',
			2 => 'kd_receiving',
			3 => 'tanggal_receiving',
			4 => 'kd_unik',
			5 => 'kd_barang',
			6 => 'nama_barang',
			7 => 'loc_name'
		);

		$where = '';

		if(!empty($post['id_receiving']))
			$where .= sprintf(' AND rcv.id_receiving = \'%s\' ', $post['id_receiving']);

		if(!empty($post['id_po']))
			$where .= sprintf(' AND id_po = \'%s\' ', $post['id_po']);

		if(!empty($post['id_supplier']))
			$where .= sprintf(' AND inb.id_supplier = \'%s\' ', $post['id_supplier']);

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(tanggal_receiving) BETWEEN \'%s\' AND \'%s\' ',
				DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'),
				DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'));
		}

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					inbound inb
				JOIN receiving rcv
					ON rcv.id_po=inb.id_inbound
				JOIN supplier spl
					ON spl.id_supplier=inb.id_supplier
				JOIN receiving_barang rcvbrg
					ON rcvbrg.id_receiving=rcv.id_receiving
				JOIN barang brg
					ON brg.id_barang=rcvbrg.id_barang
				JOIN m_loc loc
					ON loc.loc_id=rcvbrg.loc_id
				WHERE 1=1'.$where;

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					kd_inbound, kd_receiving,
					DATE_FORMAT(tanggal_receiving, \'%d\/%m\/%Y\') AS tanggal_receiving,
					kd_unik, kd_barang, nama_barang, loc_name
				FROM
					inbound inb
				JOIN receiving rcv
					ON rcv.id_po=inb.id_inbound
				JOIN supplier spl
					ON spl.id_supplier=inb.id_supplier
				JOIN receiving_barang rcvbrg
					ON rcvbrg.id_receiving=rcv.id_receiving
				JOIN barang brg
					ON brg.id_barang=rcvbrg.id_barang
				JOIN m_loc loc
					ON loc.loc_id=rcvbrg.loc_id
				WHERE 1=1'.$where;

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		$action = '';

		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_inbound'];
			$nested[] = $row[$i]['kd_receiving'];
			$nested[] = $row[$i]['tanggal_receiving'];
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['loc_name'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getWidget(){
		$result = array();
		$today = date('Y-m-d');

		$sql = 'SELECT
					 IFNULL(SUM(total_item), 0) AS total_item, IFNULL(SUM(total_qty), 0) AS total_qty,
					 IFNULL(SUM(total_tally), 0) AS total_tally, IFNULL(SUM(total_qty_tally), 0) AS total_qty_tally,
					 IFNULL(SUM(total_putaway), 0) AS total_putaway, IFNULL(SUM(total_qty_putaway), 0) AS total_qty_putaway,
					 finish_putaway,tanggal_receiving,
					 -- TIME_FORMAT(
						-- SEC_TO_TIME(
							AVG(
								TIME_TO_SEC(
									TIMEDIFF(
										finish_putaway,
										tanggal_receiving
									)
								)
							)
						-- ),
					-- \'%H:%i:%s\') AS average
					AS average
				FROM
					receiving rcv
				LEFT JOIN
				(
					SELECT
						table_1.id_receiving, COUNT(total_item) AS total_item, SUM(total_qty) AS total_qty
					FROM
					(
						SELECT
							rcvqty.id_receiving, rcvqty.id_barang AS total_item, SUM(qty) AS total_qty
						FROM
							receiving_qty rcvqty
						JOIN barang brg
							ON brg.id_barang=rcvqty.id_barang
						GROUP BY
							rcvqty.id_barang, id_receiving
					) AS table_1
					GROUP BY
						id_receiving
				) AS rec_qty
					ON rec_qty.id_receiving=rcv.id_receiving
				LEFT JOIN
				(
					SELECT
						table_2.id_receiving, COUNT(total_tally) AS total_tally, SUM(total_qty_tally) AS total_qty_tally
					FROM
					(
						SELECT
							rcvqty.id_receiving, COUNT(*) AS total_tally, SUM(qty) AS total_qty_tally
						FROM
							receiving_qty rcvqty
						JOIN barang brg
							ON brg.id_barang=rcvqty.id_barang
						JOIN receiving srcv
							ON srcv.id_receiving=rcvqty.id_receiving
						WHERE
							srcv.start_tally IS NULL
						AND
							srcv.start_tally IS NULL
						GROUP BY
							rcvqty.id_barang, id_receiving
					) AS table_2
					GROUP BY
						id_receiving
				) AS rec_tally
					ON rec_tally.id_receiving=rcv.id_receiving
				LEFT JOIN
				(
					SELECT
						table_3.id_receiving, COUNT(total_putaway) AS total_putaway, SUM(total_qty_putaway) AS total_qty_putaway
					FROM
					(
						SELECT
							rcvqty.id_receiving, COUNT(*) AS total_putaway, SUM(qty) AS total_qty_putaway
						FROM
							receiving_qty rcvqty
						JOIN barang brg
							ON brg.id_barang=rcvqty.id_barang
						JOIN receiving srcv
							ON srcv.id_receiving=rcvqty.id_receiving
						WHERE
							srcv.start_tally IS NOT NULL
						AND
							srcv.finish_tally IS NOT NULL
						GROUP BY
							rcvqty.id_barang, id_receiving
					) AS table_3
					GROUP BY
						id_receiving
				) AS rec_putaway
					ON rec_putaway.id_receiving=rcv.id_receiving
				WHERE
					DATE(tanggal_receiving) = \''.$today.'\'';

		$row = $this->db->query($sql)->row_array();
		$result = $row;

		return $result;
	}

	public function startTally($post = array()){
		$result = array();
		$rectime = date('Y-m-d H:i:s');

		$sql = 'UPDATE receiving SET start_tally = \''.$rectime.'\' WHERE id_receiving=\''.$post['id_receiving'].'\'';
		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = '';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = '';
		}

		return $result;
	}

	public function getInboundDetail($post = array()){
		$result = array();

		$sql = 'SELECT
					brg.id_barang, kd_barang, nama_barang, jumlah_barang, nama_satuan
				FROM
					inbound inb
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound
				LEFT JOIN barang brg
					ON brg.id_barang=inbbrg.id_barang
				LEFT JOIN satuan stn
					ON stn.id_satuan=brg.id_satuan
				WHERE
					inb.id_inbound=\''.$post['id_inbound'].'\'
				ORDER BY
					kd_barang ASC';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

	public function getRcvCode($post = array()){
		$result = array();

		$sql = 'SELECT
					rcv.id_receiving AS id, kd_receiving AS text,
					(
						CASE
							WHEN id_receiving_qty IS NOT NULL
						THEN
							\'Saved\'
						ELSE
							\'\'
						END
					) AS flag
				FROM
					receiving rcv
				LEFT JOIN receiving_qty rcvqty
					ON rcvqty.id_receiving=rcv.id_receiving
				WHERE
					rcv.id_receiving IN('.$post['id_receiving'].')';

		$result = $this->db->query($sql)->result_array();
		$len = count($result);

		for($i = 0; $i < $len; $i++){
			$result[$i]['text'] = '<div style="">' . $result[$i]['text'] . '<label class="label label-success pull-right">' . $result[$i]['flag'] . '</label></div>';
		}

		return $result;
	}

	public function getDetailItemDataTable($post = array()){

		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'qty_doc',
			4 => 'qty_received'
		);

		$where = '';

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
				(
					SELECT
						brg.id_barang
					FROM
						inbound inb
					LEFT JOIN receiving rcv
						ON rcv.id_po=inb.id_inbound
					LEFT JOIN inbound_barang inbbrg
						ON inbbrg.id_inbound=inb.id_inbound
					LEFT JOIN receiving_qty rcvqty
						ON rcvqty.id_receiving=rcv.id_receiving
							AND rcvqty.id_barang=inbbrg.id_barang
					LEFT JOIN barang brg
						ON brg.id_barang=inbbrg.id_barang
					LEFT JOIN satuan st
						ON st.id_satuan=brg.id_satuan
					WHERE
						inb.id_inbound=\''.$post['id_inbound'].'\'
					GROUP BY
						brg.id_barang
				) AS table_1';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = 'SELECT
					brg.id_barang, brg.kd_barang,
					brg.nama_barang, IFNULL(rcvqty.qty,0) AS qty_doc,
					(
						SELECT
							COUNT(id_barang)
						FROM
							receiving_barang rcvbrg
						WHERE
							id_receiving=rcv.id_receiving
						AND
							id_barang=brg.id_barang
					) AS qty_received,
					nama_satuan
				FROM
					receiving rcv
				LEFT JOIN inbound inb
					ON rcv.id_po=inb.id_inbound
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound
				LEFT JOIN receiving_qty rcvqty
					ON rcvqty.id_receiving=rcv.id_receiving
						AND rcvqty.id_barang=inbbrg.id_barang
				LEFT JOIN barang brg
					ON brg.id_barang=inbbrg.id_barang
				LEFT JOIN satuan st
					ON st.id_satuan=brg.id_satuan
				WHERE
					rcv.id_receiving=\''.$post['id_receiving'].'\'
				GROUP BY
					brg.id_barang';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		$action = '';

		for($i=0;$i<$totalFiltered;$i++){

			$uom = $row[$i]['nama_satuan'];
			if(empty($uom))
				$uom = '';

			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['qty_doc'] .  ' ' . $uom;
			$nested[] = $row[$i]['qty_received'] .  ' ' . $uom;

			$disc = $row[$i]['qty_doc'] - $row[$i]['qty_received'];

			$nested[] = $disc . ' ' .$uom;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItemInboundReceiving($post = array()){

		$result = array();

		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_inbound_barang',
			1 => 'id_barang',
			2 => 'id_receiving',
			3 => '',
			4 => 'kd_barang',
			5 => 'nama_barang',
			6 => 'ordered',
			7 => 'qty_doc',
			8 => 'qty_received',
			9 => 'qty_putaway',
			10 => '(qty_doc - qty_received)'
		);

		$sql = 'SELECT
					COUNT(*) AS total
				FROM (
					SELECT
						COUNT(*)
					FROM
						receiving rcv
					LEFT JOIN inbound inb
						ON inb.id_inbound=rcv.id_po
					LEFT JOIN inbound_barang inbbrg
						ON inbbrg.id_inbound=inb.id_inbound
					LEFT JOIN receiving_qty rcvqty
						ON rcvqty.id_receiving=rcv.id_receiving
							AND rcvqty.id_barang=inbbrg.id_barang
					LEFT JOIN barang brg
						ON brg.id_barang=inbbrg.id_barang
					LEFT JOIN satuan st
						ON st.id_satuan=brg.id_satuan
					WHERE
						rcv.id_receiving=\''.$post['id_receiving'].'\'
					GROUP BY
						brg.id_barang
				) AS table_1';

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		/*$sql = 'SELECT
					id_inbound_barang, brg.id_barang, rcv.id_receiving, brg.kd_barang,
					brg.nama_barang, IFNULL(inbbrg.jumlah_barang,0) AS ordered, IFNULL(rcvqty.qty,0) AS qty_doc,
					IF(brg.has_qty = 0,
					(
						SELECT
							COUNT(id_barang)
						FROM
							receiving_barang rcvbrg
						WHERE
							id_receiving=rcv.id_receiving
						AND
							id_barang=brg.id_barang
					), 
					(
						SELECT
							SUM(first_qty)
						FROM
							receiving_barang rcvbrg
						WHERE
							id_receiving=rcv.id_receiving
						AND
							id_barang=brg.id_barang
					))AS qty_received,
					IFNULL(nama_satuan, \'\') AS nama_satuan
				FROM
					receiving rcv
				LEFT JOIN inbound inb
					ON inb.id_inbound=rcv.id_po
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound
				LEFT JOIN receiving_qty rcvqty
					ON rcvqty.id_receiving=rcv.id_receiving
						AND rcvqty.id_barang=inbbrg.id_barang
				LEFT JOIN barang brg
					ON brg.id_barang=inbbrg.id_barang
				LEFT JOIN satuan st
					ON st.id_satuan=brg.id_satuan
				WHERE
					rcv.id_receiving=\''.$post['id_receiving'].'\'
				GROUP BY
					brg.id_barang';*/

		$sql = 'SELECT
					id_inbound_barang,
					brg.id_barang,
					rcv.id_receiving,
					brg.kd_barang,
					brg.nama_barang,
					IFNULL(inbbrg.jumlah_barang, 0)AS ordered,
					IFNULL(rcvqty.qty,0) AS qty_doc,IFNULL(SUM(first_qty),0) as qty_received,
					IFNULL(tput.qty_putaway,0) AS qty_putaway,
				 	IFNULL(nama_satuan,"") AS nama_satuan
				FROM
					receiving rcv
				LEFT JOIN inbound inb ON inb.id_inbound = rcv.id_po
				LEFT JOIN inbound_barang inbbrg ON inbbrg.id_inbound = inb.id_inbound
				LEFT JOIN receiving_qty rcvqty ON rcvqty.id_receiving = rcv.id_receiving
				AND rcvqty.id_barang = inbbrg.id_barang
				LEFT JOIN barang brg ON brg.id_barang = inbbrg.id_barang
				LEFT JOIN satuan st ON st.id_satuan = brg.id_satuan
				left join receiving_barang rb on rb.id_receiving = rcv.id_receiving and brg.id_barang = rb.id_barang
				LEFT JOIN (	SELECT
								rb.id_barang,
								rcv.id_receiving,
								IFNULL(SUM(first_qty), 0)AS qty_putaway
							FROM
								receiving rcv
							LEFT JOIN inbound inb ON inb.id_inbound = rcv.id_po
							LEFT JOIN receiving_barang rb ON rb.id_receiving = rcv.id_receiving
							WHERE
								rcv.id_receiving = \''.$post['id_receiving'].'\'
							AND rb.putaway_time > 0
							GROUP BY
								rb.id_barang) as tput on tput.id_barang = brg.id_barang and tput.id_receiving = rcv.id_receiving
				WHERE
					rcv.id_receiving = \''.$post['id_receiving'].'\'
				GROUP BY
					brg.id_barang
				';

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();

		for($i = 0; $i < $totalFiltered; $i++){

			$qtyDoc = $row[$i]['qty_doc'];
			$rcv = $row[$i]['qty_received'];
			$put = $row[$i]['qty_putaway'];

			$nested = array();
			$nested[] = $row[$i]['id_inbound_barang'];
			$nested[] = $row[$i]['id_barang'];
			$nested[] = $row[$i]['id_receiving'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['ordered'] . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $qtyDoc . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $rcv . ' ' . $row[$i]['nama_satuan'];
			$nested[] = $put . ' ' . $row[$i]['nama_satuan'];
			$nested[] = abs($qtyDoc - $put) . ' ' . $row[$i]['nama_satuan'];
			$nested[] = '';

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function getDetailItemInboundReceiving2($post=array()){

		$sql = 'SELECT 
					kd_unik, kd_barang, kd_parent, IFNULL(DATE_FORMAT(tgl_exp, \'%d\/%m\/%Y\'), \'-\') AS tgl_exp,
					DATE_FORMAT(tgl_in, \'%d\/%m\/%Y\') AS tgl_in,
					loc_name, kd_qc, IFNULL(hu.user_name, hu1.user_name) AS user_name 
				FROM 
					receiving_barang rcvbrg 
				LEFT JOIN receiving rcv 
					ON rcv.id_receiving=rcvbrg.id_receiving 
				LEFT JOIN barang brg 
					ON brg.id_barang=rcvbrg.id_barang 
				LEFT JOIN m_loc mc
					ON mc.loc_id=rcvbrg.loc_id 
				LEFT JOIN m_qc qc 
					ON qc.id_qc=rcvbrg.id_qc
				LEFT JOIN hr_user hu 
					ON hu.user_id=rcvbrg.user_id_putaway
				LEFT JOIN hr_user hu1 
					ON hu1.user_id=rcvbrg.user_id_receiving
				WHERE 
					rcvbrg.id_receiving=\''.$post['id_receiving'].'\' 
				ORDER BY brg.id_barang';

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		
		for($i=0;$i<$totalFiltered;$i++){
			
			if($row[$i]['tgl_exp'] == '00/00/0000')
				$row[$i]['tgl_exp'] = '-';
			
			$nested = array();
			$nested[] = $i + 1;
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['kd_parent'];
			$nested[] = $row[$i]['tgl_exp'];
			$nested[] = $row[$i]['tgl_in'];
			$nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['kd_qc'];
			$nested[] = $row[$i]['user_name'];
			
			$data[] = $nested;
		}

		return $data;

	}

	public function getDetailItemInboundReceivingExcel($post = array()){

		$result = array();

		$requestData = $_REQUEST;

		$sql = 'SELECT
					id_inbound_barang,
					brg.id_barang,
					rcv.id_receiving,
					brg.kd_barang,
					brg.nama_barang,
					IFNULL(inbbrg.jumlah_barang, 0)AS ordered,
					IFNULL(rcvqty.qty,0) AS qty_doc,IFNULL(SUM(first_qty),0) as qty_received,
					IFNULL(tput.qty_putaway,0) AS qty_putaway,
				 	IFNULL(nama_satuan,"") AS nama_satuan
				FROM
					receiving rcv
				LEFT JOIN inbound inb ON inb.id_inbound = rcv.id_po
				LEFT JOIN inbound_barang inbbrg ON inbbrg.id_inbound = inb.id_inbound
				LEFT JOIN receiving_qty rcvqty ON rcvqty.id_receiving = rcv.id_receiving
				AND rcvqty.id_barang = inbbrg.id_barang
				LEFT JOIN barang brg ON brg.id_barang = inbbrg.id_barang
				LEFT JOIN satuan st ON st.id_satuan = brg.id_satuan
				left join receiving_barang rb on rb.id_receiving = rcv.id_receiving and brg.id_barang = rb.id_barang
				LEFT JOIN (	SELECT
								rb.id_barang,
								rcv.id_receiving,
								IFNULL(SUM(first_qty), 0)AS qty_putaway
							FROM
								receiving rcv
							LEFT JOIN inbound inb ON inb.id_inbound = rcv.id_po
							LEFT JOIN receiving_barang rb ON rb.id_receiving = rcv.id_receiving
							WHERE
								rcv.id_receiving = \''.$post['id_receiving'].'\'
							AND rb.putaway_time > 0
							GROUP BY
								rb.id_barang) as tput on tput.id_barang = brg.id_barang and tput.id_receiving = rcv.id_receiving
				WHERE
					rcv.id_receiving = \''.$post['id_receiving'].'\'
				GROUP BY
					brg.id_barang
				';
				
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();

		for($i = 0; $i < $totalFiltered; $i++){

			$qtyDoc = $row[$i]['qty_doc'];
			$rcv = $row[$i]['qty_received'];
			$put = $row[$i]['qty_putaway'];

			$nested = array();
			$nested[] = ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['ordered'];
			$nested[] = $qtyDoc;
			$nested[] = $rcv;
			$nested[] = $put;
			$nested[] = abs($qtyDoc - $put) ;
			$nested[] = $row[$i]['nama_satuan'];
			$nested[] = '';

			$data[] = $nested;
		}

		return $data;
	}

	public function getDetailItemReceiving($post = array()){

		$result = array();

		$sql = 'SELECT
					brg.id_barang, brg.kd_barang,
					brg.nama_barang, IFNULL(rcvqty.qty,0) AS qty_doc,
					(
						SELECT
							COUNT(id_barang)
						FROM
							receiving_barang rcvbrg
						WHERE
							id_receiving=rcv.id_receiving
						AND
							id_barang=brg.id_barang
					) AS qty_received,
					nama_satuan
				FROM
					receiving rcv
				LEFT JOIN inbound inb
					ON inb.id_inbound=rcv.id_po
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound
				LEFT JOIN receiving_qty rcvqty
					ON rcvqty.id_receiving=rcv.id_receiving
						AND rcvqty.id_barang=inbbrg.id_barang
				LEFT JOIN barang brg
					ON brg.id_barang=inbbrg.id_barang
				LEFT JOIN satuan st
					ON st.id_satuan=brg.id_satuan
				WHERE
					rcv.id_receiving=\''.$post['id_receiving'].'\'
				GROUP BY
					brg.id_barang 
				ORDER BY 	
					id_inbound_barang ASC';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['message'] = '';

		return $result;
	}

	public function getDetailItem($post = array()){

		$result = array();

		$sql = 'SELECT
					brg.id_barang, brg.kd_barang,
					brg.nama_barang, IFNULL(rcvqty.qty,0) AS qty_doc,
					(
						SELECT
							COUNT(id_barang)
						FROM
							receiving_barang rcvbrg
						WHERE
							id_receiving=rcv.id_receiving
						AND
							id_barang=brg.id_barang
					) AS qty_received,
					nama_satuan
				FROM
					inbound inb
				LEFT JOIN receiving rcv
					ON rcv.id_po=inb.id_inbound
				LEFT JOIN inbound_barang inbbrg
					ON inbbrg.id_inbound=inb.id_inbound
				LEFT JOIN receiving_qty rcvqty
					ON rcvqty.id_receiving=rcv.id_receiving
						AND rcvqty.id_barang=inbbrg.id_barang
				LEFT JOIN barang brg
					ON brg.id_barang=inbbrg.id_barang
				LEFT JOIN satuan st
					ON st.id_satuan=brg.id_satuan
				WHERE
					inb.id_inbound=\''.$post['id_inbound'].'\'
				GROUP BY
					brg.id_barang 
				ORDER BY 
					id_inbound_barang ASC';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;
		$result['message'] = '';

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$sql = 'SELECT
					rcv.id_receiving, inb.id_inbound, kd_inbound, kd_receiving,
					IFNULL(vehicle_plate,\'-\') AS vehicle_plate,
					DATE_FORMAT(tanggal_receiving, \'%d\/%m\/%Y\') AS tanggal_receiving,
					DATE_FORMAT(tanggal_receiving, \'%H:%i\') AS time_receiving,
					dock, delivery_doc, kd_supplier, nama_supplier, alamat_supplier, telepon_supplier,
					cp_supplier, nama_status, inbound_document_name,
					DATE_FORMAT(tanggal_inbound, \'%d\/%m\/%Y\') AS tanggal_inbound,
					hui.user_name AS user_name_inbound, hur.user_name AS user_name_receiving,
					start_tally, finish_tally, start_putaway, finish_putaway,
					(
						CASE
							WHEN start_tally IS NULL
								THEN \'-\'
							WHEN start_tally IS NOT NULL AND finish_tally IS NULL
								THEN CONCAT(\'Started\',\' at \',
									(
										CASE
											WHEN DATE(start_tally) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(start_tally, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(start_tally, \'%H:%i\')
										END
									)
								)
							WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL
								THEN CONCAT(\'Finished\',\' at \',
									(
										CASE
											WHEN DATE(finish_tally) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(finish_tally, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(finish_tally, \'%H:%i\')
										END
									)
								)
						END
					) AS tally,
					(
						CASE
							WHEN start_putaway IS NULL
								THEN \'-\'
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
								THEN CONCAT(\'Started\',\' at \',
									(
										CASE
											WHEN DATE(start_putaway) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(start_putaway, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(start_putaway, \'%H:%i\')
										END
									)
								)
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
								THEN CONCAT(\'Finished\',\' at \',
									(
										CASE
											WHEN DATE(finish_putaway) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(finish_putaway, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(finish_putaway, \'%H:%i\')
										END
									)
								)
						END
					) AS putaway,
					(
						SELECT
							nama_status
						FROM
							m_status_receiving
						WHERE
							id_status = (
								CASE
									WHEN start_tally IS NULL
										THEN \'1\'
									WHEN start_tally IS NOT NULL AND finish_tally IS NULL
										THEN \'2\'
									WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
										THEN \'3\'
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
										THEN \'4\'
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
										THEN \'5\'
								END
							)
					) AS status
				FROM
					receiving rcv
				LEFT JOIN inbound inb
					ON inb.id_inbound=rcv.id_po
				LEFT JOIN supplier spl
					ON spl.id_supplier=inb.id_supplier
				LEFT JOIN m_status_inbound msinb
					ON msinb.id_status_inbound=inb.id_status_inbound
				LEFT JOIN m_inbound_document minbd
					ON minbd.id_inbound_document=inb.id_inbound_document
				LEFT JOIN hr_user hui
					ON hui.user_id=inb.user_id
				LEFT JOIN hr_user hur
					ON hur.user_id=rcv.user_id
				WHERE
					id_receiving=\''.$post['id_receiving'].'\'';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function getReceivingCode($post = array()){
		$result = array();

		$where = '';

		$sql = 'SELECT
					id_receiving, kd_receiving
				FROM
					receiving rcv
				WHERE
					LOWER(kd_receiving) LIKE \'%'.$post['query'].'%\''.$where;

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getInboundList($post = array()){
		$result = array();

		$sql = 'SELECT
					id_inbound, kd_inbound
				FROM
					inbound inb
				JOIN m_status_inbound msi
					ON msi.id_status_inbound=inb.id_status_inbound
				WHERE
					inb.id_status_inbound != \'2\'
				ORDER BY
					id_inbound DESC';

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getInboundCode($post = array()){
		$result = array();

		$where = '';
		if(isset($post['params'])){
			if($post['params'] == 'open')
				$where .= ' AND inb.id_status_inbound != \'2\' ';
		}

		$sql = 'SELECT
					id_inbound, kd_inbound
				FROM
					inbound inb
				JOIN m_status_inbound msi
					ON msi.id_status_inbound=inb.id_status_inbound
				WHERE
					LOWER(kd_inbound) LIKE \'%'.$post['query'].'%\''.$where;

		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['result'] = $row;

		return $result;
	}

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_receiving',
			1 => '',
			2 => 'kd_receiving',
			3 => 'kd_inbound',
			4 => 'vehicle_plate'
		);

		$where = '';
		/*
		$search = filter_var(trim($_POST['search']['value']), FILTER_SANITIZE_STRING);

		if(!empty($search)){
			$where .= ' AND ( LOWER(username) LIKE \'%'.$search.'%\' ';
			$where .= ' OR LOWER(mailroom_name) LIKE \'%'.$search.'%\' ';
			$where .= ' OR LOWER(user_email) LIKE \'%'.$search.'%\' ';
			$where .= ' OR LOWER(user_phone_number) LIKE \'%'.$search.'%\' ';
			$where .= ' OR LOWER(user_fullname) LIKE \'%'.$search.'%\' )';
		}
		*/

		if(!empty($post['id_receiving']))
			$where .= sprintf(' AND rcv.id_receiving = \'%s\' ', $post['id_receiving']);

		if(!empty($post['id_po']))
			$where .= sprintf(' AND id_po = \'%s\' ', $post['id_po']);

		if(!empty($post['id_supplier']))
			$where .= sprintf(' AND inb.id_supplier = \'%s\' ', $post['id_supplier']);

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND DATE(tanggal_receiving) BETWEEN \'%s\' AND \'%s\' ',
				DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'),
				DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'));
		}
		
		if(!empty($post['status'])){
			
			$sts = $post['status'];

			$status = array(
				0 => '',
				1 => ' ( start_tally IS NULL )',
				2 => ' ( start_tally IS NOT NULL AND finish_tally IS NULL )',
				3 => ' ( finish_tally IS NOT NULL AND start_putaway IS NULL )',
				4 => ' ( start_putaway IS NOT NULL AND finish_putaway IS NULL )',
				5 => ' ( start_putaway IS NOT NULL AND finish_putaway IS NOT NULL )'
			);

			$where .= ' AND ( ';
			for ($i=0; $i < count($post['status']); $i++) { 

				if($i > 0){

					$where .= 'OR '.$status[$post['status'][$i]];

				}else{
					$where .= $status[$post['status'][$i]];
				}
			}

			$where .= ' )';

			// if($sts == '1')
			// 	$where .= ' AND start_tally IS NULL '; 
			
			// if($sts == '2')
			// 	$where .= ' AND start_tally IS NOT NULL AND finish_tally IS NULL ';
			
			// if($sts == '3')
			// 	$where .= ' AND start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL ';
			
			// if($sts == '4')
			// 	$where .= ' AND start_putaway IS NOT NULL AND finish_putaway IS NULL ';
			
			// if($sts == '5')
			// 	$where .= ' AND start_putaway IS NOT NULL AND finish_putaway IS NOT NULL ';
		}

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					receiving rcv
				LEFT JOIN inbound inb
					ON inb.id_inbound=rcv.id_po
				LEFT JOIN supplier spl
					ON spl.id_supplier=inb.id_supplier
				LEFT JOIN m_status_inbound msinb
					ON msinb.id_status_inbound=inb.id_status_inbound
				LEFT JOIN m_inbound_document minbd
					ON minbd.id_inbound_document=inb.id_inbound_document
				WHERE 1=1'.$where;

		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];


		$sql = 'SELECT
					rcv.id_receiving, id_inbound, kd_inbound, kd_receiving,
					IFNULL(vehicle_plate,\'-\') AS vehicle_plate,
					DATE_FORMAT(tanggal_receiving, \'%d\/%m\/%Y\') AS tanggal_receiving,
					DATE_FORMAT(tanggal_receiving, \'%H:%i\') AS time_receiving,
					DATE_FORMAT(tanggal_receiving, \'%H:%i:%s\') AS time_receiving_second,
					start_tally, finish_tally, start_putaway, finish_putaway, IFNULL(rcv.remark, \'<div align="center">-</div>\') AS remark,
					(
						CASE
							WHEN start_tally IS NULL
								THEN \'-\'
							WHEN start_tally IS NOT NULL AND finish_tally IS NULL
								THEN CONCAT(\'Started\',\'<br>\',
									(
										CASE
											WHEN DATE(start_tally) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(start_tally, \'%d\/%m\/%Y %H:%i\')
											ELSE
												DATE_FORMAT(start_tally, \'%H:%i\')
										END
									)
								)
							WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL
								THEN CONCAT(DATE_FORMAT(finish_tally, \'%H:%i\'),\'<br>\',
									CONCAT(\'<span class="small-date">\', DATE_FORMAT(finish_tally, \'%d\/%m\/%Y\'), \'</span>\'))
						END
					) AS tally,
					(
						CASE
							WHEN start_putaway IS NULL
								THEN \'-\'
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
								THEN CONCAT(\'Started\',\'<br>\',
									(
										CASE
											WHEN DATE(start_putaway) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(start_putaway, \'%d-%m-%Y %H:%i\')
											ELSE
												DATE_FORMAT(start_putaway, \'%H:%i\')
										END
									)
								)
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
								THEN CONCAT(DATE_FORMAT(finish_putaway, \'%H:%i\'),\'<br>\',
									CONCAT(\'<span class="small-date">\', DATE_FORMAT(finish_putaway, \'%d\/%m\/%Y\'), \'</span>\'))
						END
					) AS putaway,
					(
						CASE
							WHEN start_putaway IS NULL
								THEN \'-\'
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
								THEN CONCAT(\'Started\',\'<br>\',
									(
										CASE
											WHEN DATE(start_putaway) != DATE(tanggal_receiving)
												THEN DATE_FORMAT(start_putaway, \'%d-%m-%Y %H:%i:%s\')
											ELSE
												DATE_FORMAT(start_putaway, \'%H:%i:%s\')
										END
									)
								)
							WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
								THEN CONCAT(DATE_FORMAT(finish_putaway, \'%H:%i:%s\'),\'<br>\',
									CONCAT(\'<span class="small-date">\', DATE_FORMAT(finish_putaway, \'%d\/%m\/%Y\'), \'</span>\'))
						END
					) AS putaway_second,
					(
						SELECT
							nama_status
						FROM
							m_status_receiving
						WHERE
							id_status = (
								CASE
									WHEN start_tally IS NULL
										THEN \'1\'
									WHEN start_tally IS NOT NULL AND finish_tally IS NULL
										THEN \'2\'
									WHEN start_tally IS NOT NULL AND finish_tally IS NOT NULL AND start_putaway IS NULL
										THEN \'3\'
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NULL
										THEN \'4\'
									WHEN start_putaway IS NOT NULL AND finish_putaway IS NOT NULL
										THEN \'5\'
								END
							)
					) AS status,
					(
						SELECT
							IFNULL(COUNT(*), 0)
						FROM
							receiving_barang rcvbrg
						WHERE
							rcvbrg.id_receiving=rcv.id_receiving
					) AS scanned
				FROM
					receiving rcv
				LEFT JOIN inbound inb
					ON inb.id_inbound=rcv.id_po
				LEFT JOIN supplier spl
					ON spl.id_supplier=inb.id_supplier
				LEFT JOIN m_status_inbound msinb
					ON msinb.id_status_inbound=inb.id_status_inbound
				LEFT JOIN m_inbound_document minbd
					ON minbd.id_inbound_document=inb.id_inbound_document
				WHERE 1=1'.$where;

		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);

		$data = array();
		$action = '';

		for($i=0;$i<$totalFiltered;$i++){

			$class = '';
			$class1 = '';
			if($row[$i]['finish_tally'] != NULL){
			//if($row[$i]['start_tally'] != NULL){
				$class = 'disabled-link';
			}
			
			if($row[$i]['finish_tally'] != NULL){
				$class1 = 'disabled-link';
			}else{
				if($row[$i]['scanned'] <= 0)
					$class1 = 'disabled-link';
				else
					$class1 = '';
			}

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a class="data-table-tally '.$class.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-print"></i> Print Tally</a>
							</li>
							<li>
								<a class="data-table-barcode '.$class.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-barcode"></i> Print Label</a>
							</li>
							<li>
								<a class="data-table-finish-tally '.$class1.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-check-circle"></i> Finish Tally</a>
							</li>';

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$class.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$class.'" data-id="'.$row[$i]['id_receiving'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['id_receiving'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().$post['className'].'/detail/'.$row[$i]['id_receiving'].'">'.$row[$i]['kd_receiving'].'</a>';
			$nested[] = '<a href="'.base_url().'inbound/detail/'.$row[$i]['id_inbound'].'">'.$row[$i]['kd_inbound'].'</a>';
			$nested[] = $row[$i]['vehicle_plate'];

			$dateReceived = $row[$i]['tanggal_receiving'];
			$timeReceived = $row[$i]['time_receiving'];
			$timeReceivedSecond = $row[$i]['time_receiving_second'];
			$tally = $row[$i]['tally'];
			$putaway = $row[$i]['putaway'];
			$putawaySecond = $row[$i]['putaway_second'];

			$nested[] = $timeReceived.'<br><span class="small-date">'.$dateReceived.'</span>';
			//$nested[] = $timeReceived;
			$nested[] = $tally;
			$nested[] = $putaway;

			$sts = $row[$i]['status'];

			if($row[$i]['finish_putaway'] != NULL){
				$this->load->helper('diff_helper');

				preg_match_all('/<span class="small-date">(.*?)<\/span>/s', $putaway, $matches);
				$dt = $matches[1][0];

				$exp = explode('<br>', $putawaySecond);

				// if($dt != $dateReceived){
					$dtr = date('Y-m-d H:i:s', strtotime(str_replace('/','-', $dateReceived . ' ' . $timeReceivedSecond)));
					$dte = date('Y-m-d H:i:s', strtotime(str_replace('/','-', $dt . ' ' . $exp[0])));

					$nested[] = $row[$i]['status'] . ' in  ' . dateDiff($dtr, $dte);

				// }else{

					// $nested[] = $row[$i]['status'] . ' at  ' . diffTime($timeReceivedSecond, $exp[0]);
				// }

			}else{
				$nested[] = $row[$i]['status'];
			}

			$nested[] = $row[$i]['remark'];

			if($post['params'] != 'no_action')
				$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

    public function get_rcv_code(){
        $this->db->select('pre_code_rcv,inc_rcv');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        $query = $this->db->get();
        $result = $query -> row();
        return $result->pre_code_rcv.$result->inc_rcv;
    }

    public function add_rcv_code(){
        $this->db->set('inc_rcv',"inc_rcv+1",FALSE);
        $this->db->where('id_inc',1);
        $this->db->update('m_increment');
    }

    private function get_kd_barang($id_barang){
        $condition["id_barang"]=$id_barang;
        $this->db->select('kd_barang');
        $this->db->from($this->table5 );
        $this->db->where_condition($condition);
        return $this->db->get()->row()->kd_barang;
    }
    public function data($condition = array()) {
        //==========filter============================
        //$condition = array();
        $kd_receiving= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');

        if(!empty($kd_receiving)){
            $condition["a.kd_receiving like '%$kd_receiving%' "]=null;
        }
        if(!empty($id_supplier)){
            $condition["(b.kd_supplier like '%$id_supplier%' or b.nama_supplier like '%$id_supplier%') "]=null;
        }
        if(!empty($id_purchase_order)){
            $condition["c.kd_po like '%$id_purchase_order%'"]=null;
        }
        if((!empty($tanggal_awal))&&(!empty($tanggal_akhir))){
            $tanggal_awal = hgenerator::switch_tanggal($tanggal_awal);
            $tanggal_akhir = hgenerator::switch_tanggal($tanggal_akhir);
            $condition["a.tanggal_receiving >= '$tanggal_awal'"] = null ;
            $condition["a.tanggal_receiving <= '$tanggal_akhir'"] = null;
        }


        //------------end filter-----------------
        $this->db->select('*,b.id_supplier as id_supplier');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table3 . ' c',' a.id_po = c.id_inbound','left');
        $this->db->join($this->table2 . ' b',' c.id_supplier = b.id_supplier','left');
        $this->db->order_by('a.tanggal_receiving DESC,a.kd_receiving DESC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function data_barcode($id_rcv = ''){
        $this->db->select('*');
        $this->db->from('receiving_barang rb');
        $this->db->join('barang b','b.id_barang = rb.id_barang','left');
        $this->db->where('id_receiving',$id_rcv);
        return $this->db->get();
    }

    private function data_detail_po($condition = array()) {
        $id_receiving= $this->input->post("id_bbm");
        if(!empty($id_receiving)){
            $condition["e.id_receiving = '$id_receiving' "]=null;
        }
        $this->db->select('*,b.id_receiving_qty,count(f.kd_unik) as qty_doc,b.jumlah_barang as jumlah_po,c.id_barang as item_id');
        $this->db->from($this->table .' e');//receiving
        $this->db->join($this->table3  . ' a','e.id_po = a.id_po');//po
        $this->db->join($this->po_barang . ' b',' b.id_po = a.id_po','left');//po_barang
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');//barang
        $this->db->join($this->table6 . ' d',' d.id_satuan = c.id_satuan','left');
        $this->db->join($this->table4 . ' f',' e.id_receiving = f.id_receiving and f.id_barang = c.id_barang','LEFT');//receiving_barang
        $this->db->group_by('c.id_barang');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail_supplier($condition = array()) {
        $this->db->select('*');
        $this->db->from($this->table3  . ' a');
        //$this->db->join($this->table4 . ' b',' a.id_po = b.id_po','left');
        $this->db->join($this->table2 . ' f',' a.id_supplier = f.id_supplier','left');
        $this->db->join($this->table8 . ' g',' g.id_supplier = f.id_supplier','left');
        $this->db->join($this->table5 . ' c',' c.id_barang = g.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang','ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function checkExist($kd_unik){
        $this->db->select('kd_unik');
        $this->db->from('receiving_barang');
        $this->db->where('kd_unik',$kd_unik);
        $hasil = $this->db->get();
        if($hasil->num_rows()>0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function cek_rcv_qty($id_barang,$id_receiving){
        $this->db->select('kd_unik');
        $this->db->from('receiving_barang');
        $this->db->where('id_receiving',$id_receiving);
        $this->db->where('id_barang',$id_barang);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function empty_rcv_qty($id_barang,$id_receiving){
        $this->db->where('id_receiving',$id_receiving);
        $this->db->where('id_barang',$id_barang);
        $this->db->delete('receiving_barang');
    }

    public function update_rcv_qty($id_receiving,$id_barang,$qty){
        $condition['id_receiving'] = $id_receiving;
        $condition['id_barang'] = $id_barang;

        $this->db->from('receiving_qty rq');
        $this->db->where($condition);
        $cek_eksis = $this->db->get();

        if($cek_eksis->num_rows() > 0){

            $update = array('qty' => $qty);
            $this->db->where($condition);
            $this->db->update('receiving_qty', $update);

        }else{

            $insert = array('id_receiving' => $id_receiving,
                'id_barang' => $id_barang,
                'qty' => $qty);

            $this->db->insert('receiving_qty', $insert);

        }
    }

    public function data_detail_bbm(){
        $id_receiving= $this->input->post("id_bbm");
        if(!empty($id_receiving)){
            $condition["r.id_receiving = '$id_receiving' "]=null;
        }
        $this->db->select('*, rq.qty as qty_doc');
        $this->db->from('receiving r');
        $this->db->join('inbound po','po.id_inbound = r.id_po','left');
        $this->db->join('purchase_order_barang pob','pob.id_po = po.id_po','left');
        $this->db->join('receiving_qty rq','rq.id_receiving = r.id_receiving and pob.id_barang = rq.id_barang','left');
        //$this->db->join('receiving_qtya rq','rq.id_receiving = r.id_receiving','left');
        $this->db->join('v_receiving vr','rq.id_receiving = vr.id_receiving and vr.id_barang = rq.id_barang','left');
        $this->db->join('barang b','b.id_barang = pob.id_barang','left');
        $this->db->join('satuan s','s.id_satuan = b.id_satuan','left');
        $this->db->where($condition);
        return $this->db;
    }

    public function data_table_detail(){
        $id_do = $this->input->post('id_bbm');
        $id_receiving = $id_do;
        $id_barang_arr = $this->input->post('id_barang');
        //$id_po = $this->input->post('id_po');

        if(!empty($id_barang_arr)){
            foreach ($id_barang_arr as $id_barang) {
                $kd_barang = $this->get_kd_barang($id_barang);
                $receiving = $this->input->post('receiving_'.$id_receiving.'_'.$id_barang);
                $tgl_exp = hgenerator::switch_tanggal($this->input->post('tanggal_exp_'.$id_receiving.'_'.$id_barang));
                $this->update_rcv_qty($id_receiving,$id_barang,$receiving);
            }
        }
        //-----------end save data----------------
        // Total Record
        $total = $this->data_detail_bbm()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail_bbm()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $i=0;
            $id = $value->id_po_barang;
            $id_barang = $value->id_barang;
            if(!empty($value->qty_doc)){
                $discrepenci = $value->qty_doc - $value->qty_received;
            }else{
                $discrepenci = 0;
            }
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'receiving' => !empty($value->qty_doc) ? $value->qty_doc.' '. $value->nama_satuan : form_input("receiving_".$id_do.'_'.$id_barang,!empty($value->qty_doc) ?$value->qty_doc:'','id="receiving_'.$id_do."_".$id_barang.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'). form_hidden('id_barang[]',$id_barang) ,
                'received' =>$value->qty_received,
                'discrepenci' => $discrepenci
            );
            $i++;
        }

        return array('rows' => $rows, 'total' => $total);
    }


    public function edit_data_table_detail(){
        $id_do = $this->input->post('id_bbm');
        $id_receiving = $id_do;
        $id_barang_arr = $this->input->post('id_barang');
        //$id_po = $this->input->post('id_po');

/*        if(!empty($id_barang_arr)){
            foreach ($id_barang_arr as $id_barang) {
                $kd_barang = $this->get_kd_barang($id_barang);
                $receiving = $this->input->post('receiving_'.$id_receiving.'_'.$id_barang);
                $tgl_exp = hgenerator::switch_tanggal($this->input->post('tanggal_exp_'.$id_receiving.'_'.$id_barang));

                for($no=1;$no<=$receiving;$no++){

                    $cek_eksis = $this->checkExist(trim($kd_barang).'/'.$id_receiving.'/'.$no);
                    //if(!empty($jumlah_barang)&&!empty($harga_barang)&&$checkExist==0){
                    if($cek_eksis == FALSE){
                        $data_insert = array(
                            'id_receiving' => $id_receiving,
                            'tgl_exp'=> $tgl_exp,
                            'kd_unik' => trim($kd_barang).'/'.$id_receiving.'/'.$no,
                            'tgl_in' =>date('YYYY-mm-dd'),
                            'id_barang' =>$id_barang,
                        );
                        $this->db->insert($this->table4,$data_insert);
                    }

                }
            }
        }*/
        //-----------end save data----------------
        // Total Record
        $total = $this->data_detail_bbm()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail_bbm()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $i=0;
            $id = $value->id_po_barang;
            $id_barang = $value->id_barang;
            if(!empty($value->qty_doc)){
                $discrepenci = $value->qty_doc - $value->qty_received;
            }else{
                $discrepenci = 0;
            }
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'receiving' => form_input("receiving_".$id_do.'_'.$id_barang,!empty($value->qty_doc) ?$value->qty_doc:'','id="receiving_'.$id_do."_".$id_barang.'" class="span12 harga_barang" data-a-dec="," data-a-sep="."'). form_hidden('id_barang[]',$id_barang),
                'received' =>$value->qty_received,
                'discrepenci' => $discrepenci
            );
            $i++;
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function get_data_print_doc_rcv($id) {
        $condition['r.id_receiving'] = $id;
        // ===========Filtering=================
        //$condition = array();
        $loc_name= $this->input->post("loc_name");
        $loc_desc= $this->input->post("loc_desc");

        $this->db->select('kd_barang,nama_barang, qty, kd_receiving, tanggal_receiving, kd_po, kd_supplier, nama_supplier,nama_kategori, nama_satuan,r.*,po.*');
        $this->db->from('receiving r');
        $this->db->join('purchase_order po',' r.id_po = po.id_po','left');
        //$this->db->join('purchase_order_barang pob','po.id_po = pob.id_po','left');
        $this->db->join('receiving_qty rq','rq.id_receiving = r.id_receiving','left');
        $this->db->join('supplier s','po.id_supplier = s.id_supplier','left');
        $this->db->join('barang b','b.id_barang = rq.id_barang','left');
        $this->db->join('kategori k','k.id_kategori = b.id_kategori','left');
        $this->db->join('satuan sat','sat.id_satuan = b.id_satuan','left');
        $this->db->order_by('b.id_barang ASC');
        $this->db->where_condition($condition);

        return $this->db->get();
    }

    public function get_qty_print($id_rcv){
		$qty = 0;

        $this->db->select('sum(qty) as qty_print');
        $this->db->from('receiving_qty');
        $this->db->group_by('id_receiving');
        $this->db->where('id_receiving',$id_rcv);
        if($hasil = $this->db->get()->row()){
			return $qty = $hasil->qty_print;
		}

        return $qty;
    }

    public function get_inc_date(){
        $this->db->select('sn_last_date,inc_sn');
        $this->db->from('m_increment');
        $this->db->where('id_inc',1);
        return $this->db->get()->row();
    }

    public function get_next_barcode(){
        $curent_date = date('Y-m-d');
        $get_inc_date = $this->get_inc_date();
        if($curent_date != $get_inc_date->sn_last_date){
            $update = array('sn_last_date'=>$curent_date,'inc_sn'=>1);
            $this->db->where('id_inc',1);
            $this->db->update('m_increment',$update);
            $print_sn = 1;
        }else{
            $last_sn = $get_inc_date->inc_sn;
            $print_sn = $last_sn + 1;
            $update = array('sn_last_date'=>$curent_date,'inc_sn'=>$print_sn);
            $this->db->where('id_inc',1);
            $this->db->update('m_increment',$update);
        }
        $inc_lenght = strlen($print_sn);
        $leading_zero = '';
        for($i=$inc_lenght;$i<6;$i++){
            $leading_zero = $leading_zero.'0';
        }
        $full_sn = date('ymd').$leading_zero.$print_sn;
		
        // $insert = array('kd_unik'=>$full_sn);
        // $this->db->insert('receiving_barang',$insert);
		
        return $full_sn;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_receiving= $this->input->post("kd_receiving");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_receiving = $this->input->post('id_receiving');
        if(!empty($kd_receiving)){
            $condition["a.kd_receiving like '%$kd_receiving%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_receiving)){
            $condition["a.id_receiving"]=$id_receiving;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_receiving_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_receiving = b.id_receiving');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_receiving'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = $this->session->userdata('tahun_aktif');
            if(empty($tahun_aktif))
                $tahun_aktif = date('Y');
        }else{
            $set_tahun_aktif = array('tahun_aktif' => $tahun_aktif);
            $this->session->set_userdata($set_tahun_aktif);
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_receiving >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_receiving <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            //---------awal button action---------
            $id = $value->id_receiving;
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">';
            if ($this->access_right->otoritas('print')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print', array('id' => 'button-print-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/print_doc_rcv/' . $id)));
                $action .= '</li>';

            }

            if ($this->access_right->otoritas('print')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-print"></i>Print Barcode', array('id' => 'button-print-barcode-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/print_barcode/' . $id)));
                //$action .= anchor(base_url('bbm/print_barcode/' . $id), '<i class="fa fa-print"></i>Print Barcode');
                $action .= '</li>';

            }
            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';
            //------------end button action---------------
            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_receiving,
                    'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                    'kd_receiving' => anchor(null, $value->kd_receiving, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) ,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'vehicle_plate'=> $value->vehicle_plate,
                    'remark'=>$value->remark,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_receiving,
                    'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                    'kd_receiving' => anchor(null, $value->kd_receiving, array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) ,
                    'kd_po' => $value->kd_po,
                    'supplier' => $value->nama_supplier,
                    'vehicle_plate'=> $value->vehicle_plate,
                    'remark'=>$value->remark,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_excel() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_receiving >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_receiving <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $rows[] = array(
                'tanggal_receiving' => hgenerator::switch_tanggal($value->tanggal_receiving),
                'kd_receiving' => $value->kd_receiving,
                'kd_po' => $value->kd_po,
                'supplier' => $value->nama_supplier,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_receiving' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {

		$result = array();

		$sql = 'SELECT
					start_tally
				FROM
					receiving
				WHERE
					id_receiving=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		if(!empty($row['start_tally'])){

			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';

		}else{

			$sql = 'SELECT
						id_po
					FROM
						receiving
					WHERE
						id_receiving = \''.$id.'\'';

			$row = $this->db->query($sql)->row_array();

			$sql = 'UPDATE inbound SET id_status_inbound = \'1\' WHERE id_inbound = \''.$row['id_po'].'\'';
			$this->db->query($sql);

			$this->db->delete($this->table4, array('id_receiving' => $id));
			$this->db->delete($this->table, array('id_receiving' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';

		}

		return $result;
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }


    public function options($default = '--Pilih Kode BBM--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_receiving] = $row->kd_receiving ;
        }
        return $options;
    }



    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_receiving_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function validQty($id_inbound, $barang){

    	foreach ($barang as $brg) {
    		$b[] = $brg['id_barang'];
    		$qty[] = $brg['qty'];
    	}

    	$this->db
    		->select('jumlah_barang as inb_qty, sum(first_qty) as rec_qty, ib.id_barang, brg.nama_barang, st.nama_satuan')
    		->from('inbound i')
    		->join('inbound_barang ib','ib.id_inbound=i.id_inbound')
    		->join('barang brg','brg.id_barang=ib.id_barang')
    		->join('receiving r','r.id_po=ib.id_inbound','left')
    		->join('receiving_barang rb','rb.id_receiving=r.id_receiving AND rb.id_barang=ib.id_barang','left')
    		->join('satuan st','st.id_satuan=brg.id_satuan','left')
    		->where('i.id_inbound',$id_inbound)
    		->where_in('ib.id_barang', $b)
    		->group_by('ib.id_barang');

    	$result = $this->db->get()->result_array();

    	foreach ($result as $res) {
    		foreach($barang as $brg){
    			if($res['id_barang'] === $brg['id_barang']){
			    	if($res['inb_qty'] < ($res['rec_qty'] + $brg['qty'])){

			    		$Max = $res['inb_qty'] - $res['rec_qty'];

			    		return array(
			    			"response" => false,
			    			"message"  => "Barang ".$res['nama_barang']." tidak boleh lebih dari ". $Max ." ". $res['nama_satuan']
			    		);
			    	}
			    }
    		}
	    }

	    return array('response'=>true);
    }

}

?>

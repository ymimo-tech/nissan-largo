<?php
class expired_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
	}
	
	public function getItem(){
		$result = array();
		
		$sql = 'SELECT 
					id_barang, CONCAT(kd_barang, \' - \', nama_barang) AS nama_barang 
				FROM 
					barang
				WHERE has_expdate=1';
					
		$result = $this->db->query($sql)->result_array();
		
		return $result;
	}
	
	public function getListRaw($post = array()){
		$result = array();
		
		$where = '';

		if(!empty($post['id_barang']) && $post['id_barang'] != 'null')
			$where .= ' AND brg.id_barang IN('.$post['id_barang'].') '; 

		if(!empty($post['loc_id']) && $post['loc_id'] != 'null')
			$where .= ' AND tbl.loc_id IN('.$post['loc_id'].') '; 
		
		$sql = 'SELECT 
					sku, nama_barang,
					IFNULL(tbl.qty, 0) AS stock,tgl_exp, nama_satuan, loc_name
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, sum(last_qty)AS qty,loc_name,tgl_exp,loc.loc_id
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					where
						putaway_time > 0
                        AND (st_shipping = 0 or last_qty >1)
					GROUP BY 
						id_barang, tgl_exp
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan
				WHERE tbl.qty>0 and has_expdate=1 '.$where.' 
				ORDER BY 
					nama_barang ASC';
			
		echo $sql;

		$result = $this->db->query($sql)->result_array();

		return $result;
	}
	
	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'kd_barang',
			2 => 'nama_barang',
			3 => 'qty',
			4 => 'kd_satuan',
			5 => 'tgl_exp'
		);
		
		$where = '';
		$wh='';
		
		if(!empty($post['id_barang']))
			$where .= ' AND brg.id_barang IN('.implode(',', $post['id_barang']).') '; 
		
		if(!empty($post['loc_id']) && $post['loc_id'] != 'null')
			$where .= ' AND tbl.loc_id IN('.$post['loc_id'].') '; 

		if($post['params'] != 'null' && $post['params'] == 0){
			$wh .= " AND tgl_exp < CURDATE()";
		}else{
			$wh .= " AND DATEDIFF(tgl_exp, CURDATE()) <= ".$post['params'];
		}

		$sql = 'SELECT 
					COUNT(*) AS total
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, sum(last_qty)AS qty,loc.loc_id
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					where
						putaway_time > 0
                        AND (st_shipping = 0 or last_qty >1)
                    '.$wh.'
					GROUP BY 
						id_barang, tgl_exp
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan
				WHERE tbl.qty>0 and has_expdate=1 '.$where;
				
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];
		
		$sql = 'SELECT 
					brg.id_barang, kd_barang, nama_barang,
					IFNULL(tbl.qty, 0) AS stock,tgl_exp, nama_satuan ,loc_name
				FROM 
					barang brg 
				LEFT JOIN 
				(
					SELECT 
						id_barang, sum(last_qty)AS qty,tgl_exp,loc_name,loc.loc_id
					FROM 
						receiving_barang r 
					JOIN m_loc loc 
						ON loc.loc_id=r.loc_id  
					where
						putaway_time > 0
                        AND (st_shipping = 0 or last_qty >1)
                    '.$wh.'
					GROUP BY 
						id_barang, tgl_exp
				) AS tbl 
					ON tbl.id_barang=brg.id_barang 
				LEFT JOIN satuan sat 
					ON sat.id_satuan=brg.id_satuan 
				WHERE tbl.qty>0 and has_expdate=1 '.$where;
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();
		for($i=0;$i<$totalFiltered;$i++){
			$exp_date = DateTime::createFromFormat('Y-m-d', $row[$i]['tgl_exp']);
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			// $nested[] = 'TYPE';
			// $nested[] = $row[$i]['loc_name'];
			$nested[] = $row[$i]['stock'];
			$nested[] = $row[$i]['nama_satuan'];
			$nested[] = ( $row[$i]['tgl_exp'] == '0000-00-00') ? 'Not Set Yet' : $exp_date->format('d M Y');
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;
	}

	public function getExpiredList($post=array()){

		$result = array();
		$requestData = $_REQUEST;
		
		$columns = array(
			0 => '',
			1 => 'kd_unik',
			2 => 'last_qty',
			3 => 'tgl_exp'
		);
		
		$where = '';
		
		if(!empty($post['kd_barang']))
			$where .= " AND kd_barang ='".$post['kd_barang']."'"; 

		if(!empty($post['tgl_exp'])){

			if($post['tgl_exp'] != 'Not Set Yet'){
				$exp_date = new DateTime($post['tgl_exp']);
				$where .= " AND tgl_exp='".$exp_date->format('Y-m-d')."'"; 
			}else{
				$where .= " AND tgl_exp='0000-00-00'"; 
			}
		}

		$sql = "
				SELECT 
					COUNT(*) AS total
				FROM
					receiving_barang rb
				LEFT JOIN
					barang brg
					ON brg.id_barang=rb.id_barang
				WHERE
					putaway_time > 0
                AND (st_shipping = 0 or last_qty >1)
				".$where;
				
		$row = $this->db->query($sql)->row_array();
		$totalData = $row['total'];

		$sql = "
				SELECT
					kd_unik, last_qty, tgl_exp
				FROM
					receiving_barang rb
				LEFT JOIN
					barang brg
					ON brg.id_barang=rb.id_barang
				WHERE
					putaway_time > 0
                AND (st_shipping = 0 or last_qty >1)
				".$where;
		
		$sql .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		
		$row = $this->db->query($sql)->result_array();
		$totalFiltered = count($row);
		
		$data = array();

		for($i=0;$i<$totalFiltered;$i++){
			$exp_date = DateTime::createFromFormat('Y-m-d', $row[$i]['tgl_exp']);
			$nested = array();
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_unik'];
			$nested[] = $row[$i]['last_qty'];
			$nested[] = ( $row[$i]['tgl_exp'] == '0000-00-00') ? 'Not Set Yet' : $exp_date->format('d M Y');
			
			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);
		
		return $result;

	}

	public function getWidget(){

		$sql = "
				SELECT 
					sum(last_qty)AS qty
				FROM 
					receiving_barang r 
				JOIN m_loc loc 
					ON loc.loc_id=r.loc_id  
				where
					putaway_time > 0
	                AND (pl_id > 0 or last_qty >1) 
		";

		$sqls = "
			SELECT
				(
					".$sql." AND DATEDIFF(tgl_exp, CURDATE()) <= 30
				) as 30d,
				(
					".$sql." AND DATEDIFF(tgl_exp, CURDATE()) <= 60
				) as 60d,
				(
					".$sql." AND DATEDIFF(tgl_exp, CURDATE()) <= 120
				) as 120d,
				(
					".$sql." AND tgl_exp < CURDATE()
				) as expired
		";

		$rows = $this->db->query($sqls)->row_array();

		$result['data'] = $rows;
		return $result;

	}
}
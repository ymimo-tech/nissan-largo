<?php
class monthly_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'kategori_report';

    public function get_report_header(){
        $this->db->from('toko');
        $hasil = $this->db->get();
        $header = array();
        foreach ($hasil->result() as $row) {
            $header = array_merge($header,array($row->report_label,1,1));
            # code...
        }
        return $header;

    }
    private function data($condition = array()) {
        $this->db->from('barang b');
        //$this->db->order_by('a.kd_barang');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {

        $condition['id_kategori_report'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data_inbound($month='',$year=''){
        if(empty($month))
            $month = date('m');
        if(empty($year))
            $year = date('Y');
        $data = array();
        $this->db->from('v_report_in');
        $this->db->where('bulan',$month);
        $this->db->where('tahun',$year);
        $hasil = $this->db->get();
        if($hasil->num_rows()>0){
            foreach ($hasil->result() as $row) {
                $data[$row->id_barang] = $row->qty;
                # code...
            }
        }
        return $data;

    }

    public function get_report_store(){
        $this->db->from('toko');
        $hasil = $this->db->get();
        $store = array();
        foreach ($hasil->result() as $row) {
            $store = array_merge($store,array($row->report_label => $row->id_toko));
            # code...
        }
        return $store;
    }

    public function get_data_outbond($month='',$year=''){
        if(empty($month))
            $month = date('m');
        if(empty($year))
            $year = date('Y');

        $data = array();
        $this->db->from('v_report_out');
        $this->db->where('bulan',$month);
        $this->db->where('tahun',$year);
        $hasil = $this->db->get();
        foreach ($hasil->result() as $row) {
            $data[$row->id_barang][$row->id_toko] = $row->qty;
            # code...
        }
        return $data;
    }

    public function get_balance($date){
        if(empty($month))
            $month = date('m');
        if(empty($year))
            $year = date('Y');
        $balance = array();
        $this->db->select('count(rb.id_barang) as qty_in,count(rb.shipping_id) as qty_out, (count(rb.id_barang)-count(rb.shipping_id)) as balance, rb.id_barang');
        $this->db->from('receiving_barang rb');
        $this->db->join('shipping s','s.shipping_id = rb.shipping_id and s.shipping_date < "'.$date.'"','left');
        $this->db->where('rb.tgl_in < "'.$date.'"',NULL);
        $this->db->group_by('rb.id_barang');
        $hasil = $this->db->get();
        foreach ($hasil->result() as $row) {
            $balance[$row->id_barang] = $row->balance;
            # code...
        }
        return $balance;

    }

    public function data_table() {
        // Filtering
        $condition = array();
        $nama_kategori_report= $this->input->post("nama_kategori_report");
        if(!empty($nama_kategori_report)){
            $condition["nama_kategori_report like '%$nama_kategori_report%'"] = null;
        }
        $report_month = $this->input->post('report_month');
        if(!empty($report_month)){
            $full_month =strtotime('01-'.$report_month);
            $month = date('m',$full_month);
            $year = date('Y',$full_month);
            $full_month = date('Y-m-01',$full_month);
        }else{
            $month = date('m');
            $year = date('Y');
            $full_month = date('Y-m-01');
        }
        $condition = array();
        $kd_barang= $this->input->post("kd_barang");
        $nama_barang= $this->input->post("nama_barang");
        $id_satuan= $this->input->post("id_satuan");
        $id_kategori= $this->input->post("id_kategori");

        if(!empty($id_satuan)){
            $condition["b.id_satuan"]=$id_satuan;
        }
        if(!empty($id_kategori)){
            $condition["b.id_kategori"]=$id_kategori;
        }
        if(!empty($kd_barang)){
            $condition["b.kd_barang like '%$kd_barang%'"] = null;
        }
        if(!empty($nama_barang)){
            $condition["b.nama_barang like '%$nama_barang%'"] = null;
        }
        $id_owner = $this->input->post('id_owner');
        if(!empty($id_owner)){
            $condition['b.id_owner'] = $id_owner;
        }
        $id_kategori_report = $this->input->post('id_kategori_report');
        if(!empty($id_kategori_report)){
            $condition['b.id_kategori_report'] = $id_kategori_report;
        }

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        $temp_inbound = $this->get_data_inbound($month,$year);
        $outbond = $this->get_data_outbond($month,$year);
        $store = $this->get_report_store();
        $balance = $this->get_balance($full_month);
        foreach ($data->result() as $value) {
            $id = $value->id_kategori_report;
            $inbound = !empty($temp_inbound[$value->id_barang])?$temp_inbound[$value->id_barang]:0;
            $temp_balance = !empty($balance[$value->id_barang])?$balance[$value->id_barang]:0;
            $report = array(
                    'primary_key' =>$value->id_barang,
                    'kd_barang' => $value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'balance'=>$temp_balance,
                    'inbound'=>$inbound,
                );
            $total_outbond = 0;
            foreach($store as $label=>$id_toko){
                $qty_outbond = !empty($outbond[$value->id_barang][$id_toko])?$outbond[$value->id_barang][$id_toko]:0;
                $report = array_merge(
                    $report,
                    array($label => $qty_outbond)
                    );
                $total_outbond += $qty_outbond;
            }
            $ending = $temp_balance + $inbound - $total_outbond;
            $report = array_merge($report,array('ending'=>$ending));
            $rows[] = $report;
        }

        return array('rows' => $rows, 'total' => $total);
    }    

   

   
}

?>
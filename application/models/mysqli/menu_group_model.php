<?php

/**
 * Description of menu_group_model
 *
 * @author Warman Suganda
 */
class menu_group_model extends CI_Model {

    public $limit;
    public $offset;
    private $table = 'hr_grup_menu';

    public function __construct() {
        parent::__construct();
    }

    private function data($condition = array()) {
        $this->db->from($this->table);
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['kd_grup_menu'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        $this->db->order_by('urutan');
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('urutan');
        $data = $this->data($condition)->get();
        $rows = array();

        $idx_group = 0;
        foreach ($data->result() as $value) {
            $idx_group++;
            $id = $value->kd_grup_menu;

            /*$action = '';
            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu_group/edit/' . $id))) . ' ';
            }
            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('menu_group/delete/' . $id)));
            }*/

            //---------awal button action---------
            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">';
            

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu_group/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu_group/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';
            //------------end button action---------------

            $group_up = $group_down = '';
            if ($idx_group == 1) {
                $group_up = 'disabled="disabled"';
            } else if ($idx_group == $total) {
                $group_down = 'disabled="disabled"';
            }

            $urutan = anchor(null, '<i class="icon-arrow-up"></i>', 'class="btn transparant" ' . $group_up);
            $urutan .= anchor(null, '<i class="icon-arrow-down"></i>', 'class="btn transparant" style="margin-left:5px;" ' . $group_down);

            $rows[] = array(
                'icon' => '<i class="' . $value->icon . '"></i>',
                'nik' => $value->nama_grup_menu,
                'urutan' => $urutan,
                'action' => !empty($action) ? $action : '<i class="icon-lock" title="Acces Denied" style="color:#999;"></i>'
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('kd_grup_menu' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('kd_grup_menu' => $id));
    }

    public function options($default = '--Pilih Group Menu--', $key = '') {
        $data = $this->get_data();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->kd_grup_menu] = $row->nama_grup_menu;
        }
        return $options;
    }

}

/* End of file menu_group_model.php */
/* Location: ./application/models/menu_group_model.php */


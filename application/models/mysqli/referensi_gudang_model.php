<?php
class referensi_gudang_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'm_gudang';

    public function data($condition = array()) {
        $this->db->from($this->table  . ' a');
        $this->db->join('m_site', 'm_site.id_site = a.id_site', 'left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id_gudang'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kd_gudang= $this->input->post("kd_gudang");
        $nama_gudang= $this->input->post("nama_gudang");

        if(!empty($kd_gudang)){
            $condition["a.kd_gudang like '%$kd_gudang%'"]=null;
        }

        if(!empty($nama_gudang)){
            $condition["a.nama_gudang like '%$nama_gudang%'"]=null;
        }
        
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.id_gudang');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_gudang;

            $action = '<div class="btn-group">
                        <button class="btn blue dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-flash"></i>
                        Action <i class="fa fa-angle-down"></i>
                        </button>';

            $action .= '<ul class="dropdown-menu" role="menu">
                            <li>';
            $action .= anchor(null, '<i class="fa fa-cogs"></i>Detail', array('id' => 'drildown_key_t_bbm_' . $id, 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 't_bbm', 'data-source' => base_url('bbm/get_detail_bbm/' . $id))) . ' ';
            $action .= '</li>';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-edit"></i>Edit', array('id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit/' . $id))) . ' ';
                $action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
                $action .= anchor(null, '<i class="fa fa-trash-o"></i>Delete', array('id' => 'button-delete-' . $id,'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete/' . $id)));
                $action .= '</li>';

            }
            $action .= '</ul>
                    </div>';

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'kd_gudang' => $value->kd_gudang,
                    'nama_gudang' => $value->nama_gudang,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'kd_gudang' => $value->kd_gudang,
                    'nama_gudang' => $value->nama_gudang
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) { 
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_gudang' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_gudang' => $id));
    }
    
    public function options($default = '--Pilih Nama Gudang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_gudang] = $row->nama_gudang ;
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column) {
        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();
        foreach ($data as $dt) {
            $options[$dt[$value_column]] = $dt[$caption_column];
        }
        return $options;
    }
    
}

?>
<?php

class Api_loading_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_id($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    // function load(){
    //     $this->db->select("pl_name, remark");
    //     $this->db->from("picking_list");
    //     $this->db->where("pl_status", 1);
    //     return $this->db->get();
    // }

    function load(){
        $this->db->select("shipping_code as pl_name, license_plate AS license, IF(otb.id_outbound_document = 2, (SELECT nama_supplier FROM supplier WHERE id_supplier = otb.id_customer), (SELECT customer_name FROM m_customer WHERE id_customer = otb.id_customer)) as remark");
        $this->db->from("shipping shp");
        $this->db->join("shipping_picking shpick", "shp.shipping_id = shpick.shipping_id", "left");
		$this->db->join("picking_list pl", "shpick.pl_id = pl.pl_id", "left");
		$this->db->join("picking_list_outbound plo", "pl.pl_id = plo.pl_id", "left");
        $this->db->join("outbound otb", "plo.id_outbound = otb.id_outbound", "left");
        //$this->db->join("m_customer cus", "otb.id_customer = cus.id_customer", "left");
        $this->db->where("shp.st_shipping", 0);
        return $this->db->get();
    }

    // function get_picking($picking_code){
    //  $this->db->select("pl_status");
    //  $this->db->from("picking_list");
    //  $this->db->where("pl_name", $picking_code);
    //  return $this->db->get();
    // }

    function get_shipping($shipping_code){
        $this->db->select("st_shipping");
        $this->db->from("shipping");
        $this->db->where("shipping_code", $shipping_code);
        return $this->db->get();
    }

    function get($loading_code){
        $this->db->select("st_shipping");
        $this->db->from("shipping");
        $this->db->where("shipping_code", $loading_code);
        return $this->db->get();
    }

    function get_serial($picking_code, $serial_number){
        $this->db->select("st_shipping");
        $this->db->from("receiving_barang rcv");
        $this->db->join("picking_list pick", "rcv.pl_id = pick.pl_id");
        $this->db->where("kd_unik", $serial_number);
        $this->db->where("pick.pl_name", $picking_code);
        return $this->db->get();
    }

    // function retrieve($picking_code){
    //  $this->db->select("kd_barang, kd_unik");
    //     $this->db->from("receiving_barang rcv");
    //     $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
    //     $this->db->join("picking_list pick", "rcv.pl_id = pick.pl_id", "left");
    //     $this->db->where("pick.pl_name", $picking_code);
    //     $this->db->where("st_shipping", 0);
    //     $this->db->order_by("kd_barang");
    //     return $this->db->get();
    // }

    // function retrieve($shipping_code){
    //     $this->db->select("kd_barang, kd_unik");
    //     $this->db->from("receiving_barang rcv");
    //     $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
    //     $this->db->join("picking_list pick", "rcv.pl_id = pick.pl_id", "left");
    //     $this->db->join("shipping_picking shpick", "pick.pl_id = shpick.pl_id", "left");
    //     $this->db->join("shipping shp", "shpick.shipping_id = shp.shipping_id", "left");
    //     $this->db->where("shp.shipping_code", $shipping_code);
    //     $this->db->where("rcv.st_shipping", 0);
    //     $this->db->order_by("kd_barang");
    //     return $this->db->get();
    // }

    function retrieve($shipping_code){
        $this->db->select("kd_barang, kd_unik");
        $this->db->from("receiving_barang rcv");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->join("shipping shp", "rcv.shipping_id = shp.shipping_id", "left");
        $this->db->where("shp.shipping_code", $shipping_code);
        $this->db->where("rcv.st_loading", 0);
        $this->db->order_by("kd_barang");
        return $this->db->get();
    }

    function getser($serial_number){
        $this->db->select("*");
        $this->db->from("receiving_barang");
        $this->db->where("kd_unik", $serial_number);
        return $this->db->get();
    }

    function post($data){
        $id_shipping    = $this->get_id("shipping_id", "shipping_code", $data["pl_name"], "shipping")->row_array();
        $id_user        = $this->get_id("user_id", "user_name", $data["uname_pick"], "hr_user")->row_array();
        $id_item        = $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
        $serialize      = $this->getser($data["kd_unik"])->row_array();

        $data_insert = array(   'kd_unik'       => $data["kd_unik"], 
                                'loc_id_old'    => $serialize["loc_id"],
                                'user_id_pick'  => $id_user["user_id"],
                                'pick_time'     => $data["loading_time"],
                                'loc_id_new'    => 104,
                                'user_id_put'   => $id_user["user_id"],
                                'put_time'      => $data["put_time"],
								'process_name'	=> 'LOADING'
                                );
        $this->db->insert("int_transfer_detail", $data_insert);

        if($serialize["first_qty"] == 1){
            $this->db->set("loc_id", 104);
            $this->db->set("license_plate", $data["license_plate"]);
            $this->db->set("loading_time", $data["loading_time"]);
            $this->db->set("user_id_loading", $id_user["user_id"]);
            $this->db->set("st_loading", 1);
            $this->db->where("shipping_id", $id_shipping["shipping_id"]);
            $this->db->where("id_barang", $id_item["id_barang"]);
            $this->db->where("kd_unik", $data["kd_unik"]);
            $this->db->update("receiving_barang");
        } else{
            // nothing to do.
        }
    }

    function post_serial($data){
        $id_shipping    = $this->get_id("shipping_id", "shipping_code", $data["pl_name"], "shipping")->row_array();
        $id_user        = $this->get_id("user_id", "user_name", $data["uname_pick"], "hr_user")->row_array();
        $id_item        = $this->get_id("id_barang", "kd_barang", $data["kd_barang"], "barang")->row_array();
        $serialize      = $this->getser($data["kd_unik"])->row_array();

        if($serialize["first_qty"] == 1){
            $this->db->set("loc_id", 104);
            $this->db->set("license_plate", $data["license_plate"]);
            $this->db->set("loading_time", $data["loading_time"]);
            $this->db->set("user_id_loading", $id_user["user_id"]);
            $this->db->set("st_loading", 1);
            $this->db->where("shipping_id", $id_shipping["shipping_id"]);
            $this->db->where("id_barang", $id_item["id_barang"]);
            $this->db->where("kd_unik", $data["kd_unik"]);
            $this->db->update("receiving_barang");
        } else{
            // nothing to do.
        }
    }

    function start($loading_code){
        $this->db->set("shipping_start", "IFNULL(shipping_start, '" . date("Y-m-d H:i:s") . "')", FALSE);
        $this->db->where("shipping_code", $loading_code);
        $this->db->update("shipping");
    }

    function finish($loading_code){
        $this->db->set("shipping_finish", date("Y-m-d H:i:s"));
        $this->db->set("st_shipping", 1);
        $this->db->where("shipping_code", $loading_code);
        $this->db->update("shipping");
    }

}
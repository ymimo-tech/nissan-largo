<?php
class inbound_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table  = 'inbound';
    private $table2 = "supplier";
    private $table3 = "m_status_inbound";
    private $table4 = "m_inbound_document";
    private $table5 = "inbound_barang";
	private $table6 = "hr_user";
	private $table7 = "inbound_outbound";

    public function get_new_inbound(){
        $result = $this->fina->get_where('f_t_inbound_h',array('SyncStatus'=>'N'));
        return $result;
    }

    public function get_supplier($kode_supplier=''){
        $result = $this->db->get_where('supplier',array('kd_supplier'=>$kode_supplier));
        return $result;
    }

    public function get_inbound_document($inbound_document_name=''){
        $result = $this->db->get_where('m_inbound_document',array('inbound_document_name'=>$inbound_document_name));
        return $result;
    }

    public function create_inbound($data=array()){
        $result = $this->db->insert('inbound',$data);
        return $this->db->insert_id();
    }

    public function update_inbound($data=array(),$id=0){
        $result = $this->db->update('inbound',$data,array('id_inbound'=>$id));
        return $result;
    }

    public function get_inbound_item($DocEntry=0){
        $result = $this->fina->get_where('f_t_inbound_l1',array('DocEntry'=>$DocEntry));
        return $result;
    }

    public function get_barang($kd_barang=''){
        $result = $this->db->get_where('barang',array('kd_barang'=>$kd_barang));
        return $result;
    }

    public function create_inbound_barang($data=array()){
        $result = $this->db->insert('inbound_barang',$data);
        return $this->db->insert_id();
    }

    public function update_inbound_barang($data=array(),$id_inbound_barang=0){
        $result = $this->db->update('inbound_barang',$data,array('id_inbound_barang'=>$id_inbound_barang));
        return $result;
    }

    public function check_inbound($kd_inbound=''){
        $result = $this->db->get_where('inbound',array('kd_inbound'=>$kd_inbound));
        return $result;
    }

    public function check_inbound_barang($where=array()){
        $result = $this->db->get_where('inbound_barang',$where);
        return $result;
    }

    public function set_inbound_status($DocEntry=0,$SyncDate=''){
        $result = $this->fina->update('f_t_inbound_h',array('SyncStatus'=>'Y','SyncDate'=>$SyncDate,'InbStatus'=>'INPROCESS'),array('DocEntry'=>$DocEntry));
        return $result;
    }

    public function data($condition = array()) {
		$this->db->select(
			'a.id_inbound, kd_inbound, tanggal_inbound, a.id_supplier, kd_supplier, nama_supplier,
			a.id_inbound_document, inbound_document_name, user_name, nama, id_outbound,
			a.id_status_inbound, kd_status, nama_status'
		);

        $this->db->from($this->table  . ' a');
        $this->db->join($this->table2 . ' b', "a.id_supplier=b.id_supplier", "left");
        $this->db->join($this->table3 . ' c', "a.id_status_inbound=c.id_status_inbound", "left");
        $this->db->join($this->table4 . ' d', "a.id_inbound_document=d.id_inbound_document", "left");
		$this->db->join($this->table6 . ' e', "a.user_id=e.user_id", "left");
		$this->db->join($this->table7 . ' f', "a.id_inbound=f.id_inbound", "left");
        $this->db->where_condition($condition);

        return $this->db;
    }

	/*---- START ADJ -----*/

	public function getDetailOutbound($post = array()){
		$result = array();

		$sql = 'SELECT
					id_customer
				FROM
					outbound
				WHERE
					id_outbound=\''.$post['id_outbound'].'\'';

		$r = $this->db->query($sql)->row_array();
		if($r){
			$sql = 'SELECT
						id_barang, jumlah_barang
					FROM
						outbound_barang
					WHERE
						id_outbound=\''.$post['id_outbound'].'\'
					ORDER BY
						id ASC';

			$brg = $this->db->query($sql)->result_array();

			$result['status'] = 'OK';
			$result['id_customer'] = $r['id_customer'];
			$result['brg'] = $brg;
		}else{
			$result['status'] = 'OK';
			$result['id_customer'] = 0;
			$result['brg'] = array();
		}

		return $result;
	}

	public function getUomByItem($post = array()){
		$result = array();

		$sql = 'SELECT
					IFNULL(nama_satuan, \'\') AS nama_satuan
				FROM
					satuan sat
				JOIN barang brg
					ON brg.id_satuan=sat.id_satuan
				WHERE
					brg.id_barang=\''.$post['id_barang'].'\'';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

	public function checkInboundDocNumberExist($post = array()){

		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					inbound
				WHERE
					kd_inbound=\''.$post['po'].'\'';

		if(!empty($post['id_inbound'])){
			$sql = 'SELECT
						COUNT(*) AS total
					FROM
						inbound
					WHERE
						kd_inbound=\''.$post['po'].'\'
					AND
						id_inbound!=\''.$post['id_inbound'].'\'';
		}

		$row = $this->db->query($sql)->row_array();

		if($row['total'] > 0){
			$result['status'] = 'ERR';
			$result['message'] = 'Doc. number already exist';
		}else{
			$result['status'] = 'OK';
			$result['message'] = '';
		}

		return $result;
	}

	public function closePo($post = array()){
		$result = array();

		$sql = 'UPDATE
					inbound
				SET
					id_status_inbound=\'2\'
				WHERE
					id_inbound=\''.$post['id_inbound'].'\'';

		$q = $this->db->query($sql);

		if($q){
			$result['status'] = 'OK';
			$result['message'] = 'Close Inbound Document Success';
		}else{
			$result['status'] = 'ERR';
			$result['message'] = 'Close Inbound Document Failed';
		}

		return $result;
	}

	public function getSupplier($post = array()){
		$result = array();

		$sql = 'SELECT * FROM supplier ORDER BY CAST(kd_supplier AS UNSIGNED) ASC';
		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

	public function getCustomer($post = array()){
		$result = array();

		$sql = 'SELECT * FROM m_customer ORDER BY id_customer ASC';
		$row = $this->db->query($sql)->result_array();

		$result['status'] = 'OK';
		$result['data'] = $row;

		return $result;
	}

	public function updateStatusReceiving($post = array()){
		$result = array();

		$iLen = $post['id_inbound'];
		for($i = 0; $i < $iLen; $i++){

		}

		$result['status'] = 'OK';
		$result['message'] = 'Update status to receiving success';

		return $result;
	}

	public function getDetail($post = array()){
		$result = array();

		$sql = 'SELECT
					inb.id_inbound, kd_inbound, DATE_FORMAT(tanggal_inbound, \'%d/%m/%Y\') AS tanggal_inbound,
					inb.id_inbound_document, inbound_document_name,
					kd_supplier, nama_supplier, alamat_supplier, telepon_supplier, cp_supplier,
					customer_name, address, phone, nama_status, user_name,
					inb.id_status_inbound
				FROM
					inbound inb
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=inb.id_supplier
						AND inb.id_inbound_document NOT IN (\'2\', \'7\')
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=inb.id_supplier
						AND inb.id_inbound_document IN (\'2\', \'7\')
				LEFT JOIN m_status_inbound msinb
					ON msinb.id_status_inbound=inb.id_status_inbound
				LEFT JOIN m_inbound_document minbd
					ON minbd.id_inbound_document=inb.id_inbound_document
				LEFT JOIN hr_user hu
					ON hu.user_id=inb.user_id
				WHERE
					inb.id_inbound=\''.$post['id_inbound'].'\'';

		$result = $this->db->query($sql)->row_array();

		return $result;
	}

    public function get_data_excel($post=array()){
        $where='';
        if(!empty($post['id_inbound']))
			$where .= sprintf(' AND inb.id_inbound = \'%s\' ', $post['id_inbound']);

		$sql = 'SELECT
					%s
				FROM
					inbound inb
				JOIN inbound_barang inbg
					ON inbg.id_inbound=inb.id_inbound
				JOIN barang brg
					ON brg.id_barang=inbg.id_barang
				LEFT JOIN satuan st
					ON st.id_satuan=brg.id_satuan
				LEFT JOIN kategori kat
					ON kat.id_kategori=brg.id_kategori
				WHERE 1=1'.$where;

        $q = sprintf($sql, 'id_inbound_barang, kd_barang, nama_barang, COALESCE(CONCAT(inbg.jumlah_barang, \' \', st.nama_satuan), inbg.jumlah_barang) AS jumlah_barang,
							CONCAT((
								IF(brg.has_qty=0,
									(
									SELECT
										COUNT(id_barang)
									FROM
										receiving rcv
									LEFT JOIN receiving_barang rcvbrg
										ON rcvbrg.id_receiving=rcv.id_receiving
									WHERE
										id_po=inb.id_inbound
									AND
										id_barang=brg.id_barang
									),(
									SELECT
										SUM(first_qty)
									FROM
										receiving rcv
									LEFT JOIN receiving_barang rcvbrg
										ON rcvbrg.id_receiving=rcv.id_receiving
									WHERE
										id_po=inb.id_inbound
									AND
										id_barang=brg.id_barang
									))
							),\' \', st.nama_satuan) AS total_received');

        $row = $this->db->query($q)->result_array();
        return $row;
    }

	public function getDetailItem($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_inbound_barang',
			1 => '',
			2 => 'kd_barang',
			3 => 'nama_barang',
			4 => 'jumlah_barang',
			5 => 'total_received'
		);

		$where = '';
		$search = filter_var(trim($_POST['search']['value']), FILTER_SANITIZE_STRING);

		if(!empty($search)){
			$where .= ' AND ( LOWER(nama_barang) LIKE \'%'.$search.'%\' ';
			$where .= ' OR  LOWER(kd_barang) LIKE \'%'.$search.'%\' ';
			$where .= ' OR  LOWER(jumlah_barang) LIKE \'%'.$search.'%\' ) ';
		}

		if(!empty($post['id_inbound']))
			$where .= sprintf(' AND inb.id_inbound = \'%s\' ', $post['id_inbound']);

		$sql = 'SELECT
					%s
				FROM
					inbound inb
				JOIN inbound_barang inbg
					ON inbg.id_inbound=inb.id_inbound
				JOIN barang brg
					ON brg.id_barang=inbg.id_barang
				LEFT JOIN satuan st
					ON st.id_satuan=brg.id_satuan
				LEFT JOIN kategori kat
					ON kat.id_kategori=brg.id_kategori
				WHERE 1=1'.$where;

		//$q = sprintf($sql, 'COUNT(*) AS total, COALESCE(CONCAT(SUM(inbg.jumlah_barang), \' \', st.nama_satuan), SUM(inbg.jumlah_barang)) AS total_qty');
		$q = sprintf($sql, 'COUNT(*) AS total');

		$row = $this->db->query($q)->row_array();
		$totalData = $row['total'];

		$q = sprintf($sql, 'id_inbound_barang, kd_barang, nama_barang, COALESCE(CONCAT(inbg.jumlah_barang, \' \', st.nama_satuan), inbg.jumlah_barang) AS jumlah_barang,
							CONCAT((
								IF(brg.has_qty=0,
									(
									SELECT
										SUM(first_qty)
									FROM
										receiving rcv
									LEFT JOIN receiving_barang rcvbrg
										ON rcvbrg.id_receiving=rcv.id_receiving
									WHERE
										id_po=inb.id_inbound
									AND
										id_barang=brg.id_barang
									),(
									SELECT
										SUM(first_qty)
									FROM
										receiving rcv
									LEFT JOIN receiving_barang rcvbrg
										ON rcvbrg.id_receiving=rcv.id_receiving
									WHERE
										id_po=inb.id_inbound
									AND
										id_barang=brg.id_barang
									))
							),\' \', st.nama_satuan) AS total_received');

		$q .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($q)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$nested = array();
			$nested[] = $row[$i]['id_inbound_barang'];
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = $row[$i]['kd_barang'];
			$nested[] = $row[$i]['nama_barang'];
			$nested[] = $row[$i]['jumlah_barang'];
			$nested[] = $row[$i]['total_received'];

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data
		);

		return $result;
	}

	public function cek_not_usage_po($id_po){
        $this->db->from('receiving');
        $this->db->where('id_po',$id_po);
        $hasil = $this->db->get();
        if($hasil->num_rows()>0){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    public function updateInboundStatus(){
    	//--------step 1 update per masing - masing barang di inbound barang-------------
    	$sql = "SELECT
    				ib.id_inbound_barang,
					ib.id_barang,
					IFNULL(ib.jumlah_barang,0) as jumlah_barang,
					IFNULL(tq.qty_rcv,0) as qty_rcv,
					i.kd_inbound
				FROM
					inbound i
				LEFT JOIN inbound_barang ib on ib.id_inbound = i.id_inbound
				LEFT JOIN(
				SELECT
					r.id_po,
					id_barang,
					sum(first_qty)AS qty_rcv
				FROM
					receiving_barang rb
				LEFT JOIN receiving r ON r.id_receiving = rb.id_receiving
				GROUP BY
					id_barang,
					r.id_po
				)tq ON tq.id_barang = ib.id_barang
				AND ib.id_inbound = tq.id_po
				WHERE (ib.st_inbound_barang IS NULL OR ib.st_inbound_barang != 2)";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			if($row->jumlah_barang == $row->qty_rcv){
				$data_update = array(
					'st_inbound_barang' => 2);
				$this->db->where('id_inbound_barang',$row->id_inbound_barang);
				$this->db->update('inbound_barang',$data_update);
			}
			# code...
		}

		//---------step 2, update per inboundberdasarkan jumlah barang yg sudah close----------
		$sql = "SELECT
					ib.id_inbound,
					count(id_barang)AS qty_barang,
					sum(st_inbound_barang)AS qty_finish
				FROM
					inbound_barang ib
				LEFT JOIN inbound i ON i.id_inbound = ib.id_inbound
				WHERE
					(
						id_status_inbound != 2
						OR id_status_inbound IS NULL
					)
				GROUP BY
					ib.id_inbound";
		$query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			$jum_barang = $row->qty_barang * 2;
			/*if( $row->qty_finish > 0){
				$data_update = array(
					'id_status_inbound' => 3);
				$this->db->where('id_inbound',$row->id_inbound);
				$this->db->update('inbound',$data_update);
			}*/
			if($jum_barang == $row->qty_finish){
				$data_update = array(
					'id_status_inbound' => 2);
				$this->db->where('id_inbound',$row->id_inbound);
				$this->db->update('inbound',$data_update);
			}
			# code...
		}
    }

	public function getList($post = array()){
		$result = array();
		$requestData = $_REQUEST;

		$columns = array(
			0 => 'id_inbound',
			1 => '',
			2 => 'kd_inbound',
			3 => 'tanggal_inbound',
			4 => 'inbound_document_name',
			5 => 'kd_supplier',
			6 => 'total_item',
			7 => 'nama_status'
		);

		$where = '';
		/*
		$search = filter_var(trim($_POST['search']['value']), FILTER_SANITIZE_STRING);

		if(!empty($search)){
			$where .= ' AND ( LOWER(username) LIKE \'%'.$search.'%\' ';
			$where .= ' OR LOWER(mailroom_name) LIKE \'%'.$search.'%\' ';
			$where .= ' OR LOWER(user_email) LIKE \'%'.$search.'%\' ';
			$where .= ' OR LOWER(user_phone_number) LIKE \'%'.$search.'%\' ';
			$where .= ' OR LOWER(user_fullname) LIKE \'%'.$search.'%\' )';
		}
		*/

		if(!empty($post['po']))
			$where .= sprintf(' AND inb.id_inbound = \'%s\' ', $post['po']);

		if(!empty($post['id_supplier']))
			$where .= sprintf(' AND inb.id_supplier = \'%s\' ', $post['id_supplier']);

		if(!empty($post['from']) && !empty($post['to'])){
			$where .= sprintf(' AND tanggal_inbound BETWEEN \'%s\' AND \'%s\' ',
				DateTime::createFromFormat('d/m/Y', $post['from'])->format('Y-m-d'),
				DateTime::createFromFormat('d/m/Y', $post['to'])->format('Y-m-d'));
		}

		if(!empty($post['doc_type']))
			$where .= sprintf(' AND inb.id_inbound_document = \'%s\' ', $post['doc_type']);

		if(!empty($post['doc_status']))
			$where .= ' AND inb.id_status_inbound IN ('.implode(',', $post['doc_status']).')';

		$sql = 'SELECT
					%s
				FROM
					inbound inb
				LEFT OUTER JOIN supplier spl
					ON spl.id_supplier=inb.id_supplier
						AND inb.id_inbound_document NOT IN(\'2\', \'7\')
				LEFT OUTER JOIN m_customer cust
					ON cust.id_customer=inb.id_supplier
						AND inb.id_inbound_document IN(\'2\', \'7\')
				%s
				LEFT JOIN m_status_inbound msinb
					ON msinb.id_status_inbound=inb.id_status_inbound
				LEFT JOIN m_inbound_document minbd
					ON minbd.id_inbound_document=inb.id_inbound_document
				WHERE 1=1'.$where;

		//WATCH sprintf function!!!

		$q = sprintf($sql, 'COUNT(*) AS total','');

		$row = $this->db->query($q)->row_array();
		$totalData = $row['total'];

		$join = 'LEFT JOIN (
					SELECT
						inbg.id_inbound, COUNT(*) AS total_item
					FROM
						inbound_barang inbg
					JOIN barang brg
						ON brg.id_barang=inbg.id_barang
					GROUP BY
						inbg.id_inbound
				) AS tbl
					ON tbl.id_inbound=inb.id_inbound';

		$q = sprintf($sql, 'inb.id_inbound, kd_inbound, DATE_FORMAT(tanggal_inbound, \'%d/%m/%Y\') AS tanggal_inbound,
							inbound_document_name,
							IFNULL(CONCAT(kd_supplier, \' - \', nama_supplier), customer_name) AS kd_supplier, inb.id_status_inbound, nama_status,
							total_item, remark,
							(
								SELECT
									COUNT(*)
								FROM
									receiving_barang rcvbrg
								JOIN receiving rcv
									ON rcv.id_receiving=rcvbrg.id_receiving
								JOIN inbound
									ON inbound.id_inbound=rcv.id_po
								JOIN m_loc loc
									ON loc.loc_id=rcvbrg.loc_id
								WHERE
									rcv.id_po=inb.id_inbound
								AND
									loc.loc_type NOT IN('.$this->config->item('inbound_location').')
								AND
									id_status_inbound != \'2\'
							) AS total_received', $join);

		$q .=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

		$row = $this->db->query($q)->result_array();
		$totalFiltered = count($row);

		$data = array();
		for($i=0;$i<$totalFiltered;$i++){

			$action = '';

			//if($this->cek_not_usage_po($row[$i]['id_inbound']))
			//$action .= $this->buttons->actions(array('edit'=>$row[$i]['id_inbound'],'delete' => $row[$i]['id_inbound']));

			$action = '<div class="btn-group">
						<button class="btn green dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
							&nbsp;&nbsp;Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu" role="menu">';

			$disabled = '';
			if($row[$i]['total_received'] > 0)
				$disabled = '';
			else
				$disabled = 'disabled-link';

			$disabled1 = '';
			if($row[$i]['id_status_inbound'] == 2)
				$disabled1 = 'disabled-link';

			$action .= '<li>';
			$action .= '<a class="data-table-close '.$disabled.'" data-id="'.$row[$i]['id_inbound'].'"><i class="fa fa-check"></i> Close Inbound</a>';
			$action .= '</li>';

			if ($this->access_right->otoritas('edit')) {
				$action .= '<li>';
				$action .= '<a class="data-table-edit '.$disabled1.'" data-id="'.$row[$i]['id_inbound'].'"><i class="fa fa-edit"></i> Edit</a>';
				$action .= '</li>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<li>';
				$action .= '<a class="data-table-delete '.$disabled1.'" data-id="'.$row[$i]['id_inbound'].'"><i class="fa fa-trash"></i> Delete</a>';
				$action .= '</li>';
            }

			$action .= '</ul>';

			$nested = array();
			$nested[] = $row[$i]['id_inbound'];
			//$nested[] = '<input type="checkbox" name="id[]" value="'.$row[$i]['id_inbound'].'">';
			$nested[] = $_REQUEST['start'] + ($i + 1);
			$nested[] = '<a href="'.base_url().$post['className'].'/detail/'.$row[$i]['id_inbound'].'">'.$row[$i]['kd_inbound'].'</a>';
			$nested[] = $row[$i]['tanggal_inbound'];
			$nested[] = $row[$i]['inbound_document_name'];
			$nested[] = $row[$i]['kd_supplier'];
			$nested[] = $row[$i]['total_item'];
			$nested[] = $row[$i]['remark'];
			$nested[] = $row[$i]['nama_status'];
			$nested[] = $action;

			$data[] = $nested;
		}

		$result = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalFiltered ),
			"recordsFiltered" => intval( $totalData ),
			"data"            => $data,
            "current_filter"  => $this->session->userdata('current_filter')
		);

		return $result;
	}
	/*---- END ADJ -----*/

    public function get_by_id($id) {
        $condition['a.id_inbound'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function get_inbound_barang($id_inbound) {
        $sql = 'SELECT
					*
				FROM
					inbound_barang inb
				JOIN barang brg
					ON brg.id_barang=inb.id_barang
				LEFT JOIN satuan sat
					ON sat.id_satuan=brg.id_satuan
				WHERE
					id_inbound=\''.$id_inbound.'\'
				ORDER BY
					id_inbound_barang ASC';

		return $this->db->query($sql);

		//return $this->db->get_where("inbound_barang", array('id_inbound' => $id_inbound));
    }

    public function create($data, $data2) {
        $this->db->trans_start();

		$idOutbound = $data['outbound_peminjaman'];
		unset($data['outbound_peminjaman']);

        $this->db->insert($this->table, $data);
        $id_inbound = $this->db->insert_id();

		if(isset($idOutbound)){
			if(!empty($idOutbound)){
				$sql = 'INSERT INTO inbound_outbound(
							id_inbound,
							id_outbound
						)VALUES(
							\''.$id_inbound.'\',
							\''.$idOutbound.'\'
						)';

				$this->db->query($sql);
			}

		}

		if(!empty($data2)){
			foreach ($data2 as &$inbound_barang) {
				$inbound_barang['id_inbound'] = $id_inbound;
			}
			$this->db->insert_batch($this->table5, $data2);
		}

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function update($data, $data2, $id) {
        $this->db->trans_start();

		$idOutbound = $data['outbound_peminjaman'];
		unset($data['outbound_peminjaman']);

        $this->db->update($this->table, $data, array('id_inbound' => $id));


		$sql = 'UPDATE inbound_outbound SET id_outbound=\''.$idOutbound.'\'WHERE id_inbound=\''.$id.'\'';
		$this->db->query($sql);

		if(isset($idOutbound)){
			if(!empty($idOutbound)){
				$sql = 'INSERT INTO inbound_outbound(
							id_inbound,
							id_outbound
						)VALUES(
							\''.$id.'\',
							\''.$idOutbound.'\'
						)';

				$this->db->query($sql);
			}

		}
        //remove all inbound_barang which the id_inbound is $id
        $this->db->where("id_inbound", $id);
        $this->db->delete($this->table5);

        //add all new inbound_barang
		if(!empty($data2)){
			foreach ($data2 as &$inbound_barang) {
				$inbound_barang['id_inbound'] = $id;
			}
			$this->db->insert_batch($this->table5, $data2);
		}
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function delete($id) {
		$result = array();

		$sql = 'SELECT
					COUNT(*) AS total
				FROM
					receiving
				WHERE
					id_po=\''.$id.'\'';

		$row = $this->db->query($sql)->row_array();
		if($row['total'] > 0 ){

			$result['status'] = 'ERR';
			$result['message'] = 'Cannot delete this data, cause have active relation to another table.';

		}else{

			$this->db->delete($this->table, array('id_inbound' => $id));

			$result['status'] = 'OK';
			$result['message'] = 'Delete data success';

		}

        return $result;
    }

    public function options($default = '--Pilih Kode Barang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_barang] = $row->nama_barang ;
        }
        return $options;
    }

    function create_options($table_name, $value_column, $caption_column, $empty_value_text="") {

		$exp = array();
		if(strpos($caption_column,',')){
			$exp = explode(',', $caption_column);
		}

        $this->db->order_by($value_column, "ASC");
        $data = $this->db->get($table_name)->result_array();
        $options = array();

        if($empty_value_text) {
            $options[''] = $empty_value_text;
        }

		$this->load->model('setting_model');

        foreach ($data as $dt) {

			if($table_name == 'm_inbound_document'){

				$r = $this->setting_model->getData();

				if(strtolower($dt['inbound_document_name']) == 'peminjaman' && $r['peminjaman'] == 'N')
					continue;
			}

			if(!empty($exp)){
				$len = count($exp);
				$dts = '';

				for($i = 0; $i < $len; $i++){
					if($i < ($len - 1))
						$dts .= $dt[trim($exp[$i])] . ' - ';
					else{
						if(strlen($dt[trim($exp[$i])]) > 60)
							$dts .= substr($dt[trim($exp[$i])], 0, 60) . '...';
						else
							$dts .= $dt[trim($exp[$i])];
					}
				}
			}else{
				$dts = $dt[$caption_column];
			}

            $options[$dt[$value_column]] = $dts;
        }
        return $options;
    }

    public function getInDocument($id){
    	
    	$doc = $this->db->get_where('m_inbound_document',array('id_inbound_document'=>$id))->row();

    	$docName = "";

    	if($doc){

    		$docName .= $doc->inbound_document_name;
    		$docName .= $doc->separator;

			switch ($doc->year) {
			    case 1:
			    	$docName .= date("y");
			        break;
			    case 2:
			    	$docName .= date("Y");
			        break;
			    default:
			    	$docName .= ''; 
			}

			if($doc->month){
				$docName .= date("m");
			}

			$seqlen = $doc->seq_length - strlen($doc->sequence);

			for($i=0;$i<$seqlen;$i++){
				$docName .= '0';
			}

			$docName .= $doc->sequence;

    	}

    	if(empty($docName)){
    		return false;
    	}

    	return $docName;
    }

    public function addInDocument($data=array()){

    	$sql = "UPDATE m_inbound_document SET sequence = sequence + 1 WHERE id_inbound_document = ".$data['id_inbound_document'];
    	$this->db->query($sql);

    }

    public function statusOptions($default = '--Pilih Status--', $key = '') {

        $data = $this->db->get('m_status_inbound');
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_status_inbound] = $row->nama_status ;
        }
        return $options;
    }

}

?>

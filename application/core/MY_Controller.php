<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct(){

		parent::__construct();
	}

	protected function load_model($modelName){
		if(is_array($modelName)){
			foreach ($modelName as $val) {
				$this->load->model($this->db->dbdriver.'/'.$val);
			}
		}else{
			$this->load->model($this->db->dbdriver.'/'.$modelName);
		}
	}

	protected function options($table,$value="id",$text="code",$default=false,$condition=array()){

		$result = array();

		if($condition){
			$this->db->where($condition);
		}
		
		$data = $this->db->get($table)->result_array();

		if($data){
			if($default){
				$result[''] = "-- All --";
			}

			foreach ($data as $d) {
				$result[$d[$value]] = $d[$text];
			}
		}

		return $result;
	}


	public function select2Options($table,$value="id",$text="code",$default=false){

		$q = $this->input->post('query');

		if($q){
			$this->db->like('lower('.$text.')',strtolower($q));
		}
		
		$this->db->where('UPPER(status)','ACTIVE');

		$result = array();

		$data = $this->db->get($table)->result_array();

		if($data){
	        foreach ($data as $d) {
	            $result['results'][] = array(
	                'id' => $d[$value],
	                'text' => $d[$text]
	            );
	        }
		}

		echo json_encode($result);
	}

}
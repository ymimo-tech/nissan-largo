<?php if( ! defined('BASEPATH') ) exit( 'No direct script access allowed' );

if ( ! function_exists('pgconnect'))
{
    function pgconnect() {

		$conn = '';
		$driver = 'pgsql';
		$host = 'localhost';
		$port = '5432';
		$database = 'largo_middleware';
		$username = 'postgres';
		$password = 'dev123';
		
		try{
			$conn = new PDO($driver.':host='.$host.';port='.$port.';dbname='.$database, $username, $password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e) {
			echo 'Please contact Admin: '.$e->getMessage();
		}
		
		return $conn;
		
	}
}
<?php

/**
 * Description of protection_helper
 *
 * @author Warman Suganda
 */
class hprotection {

    public static function login($status = true) {	
		
        if ($status) {
            if (!self::status_login()) {
                redirect('login');
                exit;
            }
        } else {
            if (self::status_login()) {
                redirect('dashboard');
                exit;
            }
        }	 
		
		//CHECK ACTIVE SESSION FOR AJAX REQUEST
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			if(!self::status_login()){
				$response['status'] = 'ERR';
				$response['message'] = 'Not Authorized';
				
				set_status_header(401);
				echo json_encode($response);
				exit();
			}
		}
     }
	public static function status_login() {
        $ci = &get_instance();
        return $ci->session->userdata('status_login');
    }

}

/* End of file hprotection_helper.php */
/* Location: ./application/helpers/hprotection_helper.php */

<?php if( ! defined('BASEPATH') ) exit( 'No direct script access allowed' );

if ( ! function_exists('diffDay'))
{
	function diffDay($start, $end){
		
		//$hourdiff = round((strtotime($start) - strtotime($end))/3600, 1);
		//return $hourdiff;
		
		$start = new \DateTime($start);
		$end   = new \DateTime($end);
		
		$interval = $end->diff($start);

		$time = sprintf(
			'%d:%02d:%02d',
			($interval->d * 24) + $interval->h,
			$interval->i,
			$interval->s
		);
		
		return $time;
		
	}
}

if ( ! function_exists('diffTime'))
{
	function diffTime($start, $end){
		
		list($hours, $minutes, $seconds) = explode(':', $start);
		$startTimestamp = mktime($hours, $minutes, $seconds);

		list($hours, $minutes, $seconds) = explode(':', $end);
		$endTimestamp = mktime($hours, $minutes, $seconds);

		$seconds = $endTimestamp - $startTimestamp;
		//$minutes = ($seconds / 60) % 60;
		//$hours = floor($seconds / (60 * 60));		
		
		return gmdate('H:i:s', $seconds);
		
	}
}

/* Begin Andri */
if ( ! function_exists('dateDiff'))
{
	function dateDiff($time1, $time2, $precision = 6) {
		// If not numeric then convert texts to unix timestamps
		if (!is_int($time1)) {
			$time1 = strtotime($time1);
		}
		if (!is_int($time2)) {
			$time2 = strtotime($time2);
		}

		// If time1 is bigger than time2
		// Then swap time1 and time2
		if ($time1 > $time2) {
			$ttime = $time1;
			$time1 = $time2;
			$time2 = $ttime;
		}
		
		return secsToTime($time2 - $time1);

		/* comment dulu
		// Set up intervals and diffs arrays
		$intervals = array('year','month','day','hr','min','sec');
		$diffs = array();

		// Loop thru all intervals
		foreach ($intervals as $interval) {
			// Create temp time from time1 and interval
			$ttime = strtotime('+1 ' . $interval, $time1);
			// Set initial values
			$add = 1;
			$looped = 0;
			// Loop until temp time is smaller than time2
			while ($time2 >= $ttime) {
				// Create new temp time from time1 and interval
				$add++;
				$ttime = strtotime("+" . $add . " " . $interval, $time1);
				$looped++;
			}

			$time1 = strtotime("+" . $looped . " " . $interval, $time1);
			$diffs[$interval] = $looped;
		}
		
		$count = 0;
		$times = array();
		// Loop thru all diffs
		foreach ($diffs as $interval => $value) {
			// Break if we have needed precission
			if ($count >= $precision) {
				break;
			}
			// Add value and interval 
			// if value is bigger than 0
			if ($value > 0) {
				// Add s if value is not 1
				if ($value != 1) {
					$interval .= "s";
				}
				// Add value and interval to times array
				$times[] = $value . " " . $interval;
				$count++;
			}
		}

		// Return string with times
		return implode(", ", $times);
		*/
	}
}

if ( ! function_exists('secsToTime'))
{
	function secsToTime($secs) {
		$d = floor( $secs / 86400 );
		$h = floor( ( $secs % 86400 ) / 3600);
		$m = floor( ( $secs % 3600 ) / 60 );
		$s = floor( ( $secs % 3600 ) % 60 );
		
		$vd = ( $d > 1 ) ? ' days'	: ' day';
		$vh = ( $h > 1 ) ? ' hrs'	: ' hr';
		$vm = ( $m > 1 ) ? ' mins'	: ' min';
		$vs = ( $s > 1 ) ? ' secs'	: ' sec';
		
		$days		= ( $d > 0 ) ? $d.$vd : '';
		$hours		= ( $h > 0 ) ? $h.$vh : '';
		$minutes	= ( $m > 0 ) ? $m.$vm : '';
		$seconds	= ( $s > 0 ) ? $s.$vs : '';

		if( $d > 0 ) {
			$result = $days;
		} else {
			if( $h > 0 ) {
				$result = $hours;
			} else {
				$result = $minutes;
			}
		}

		return ( $result );
	}
}
/* End Andri */
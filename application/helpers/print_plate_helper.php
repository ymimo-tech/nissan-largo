<?php if( ! defined('BASEPATH') ) exit( 'No direct script access allowed' );

if ( ! function_exists('getPrintTemplate'))
{
	function getPrintTemplate($str){
		
		$ci = &get_instance();
		$orig = $str;
		$len = strlen($str);
		$zpl = '';
		
		/*
		if($len > 3){
			if(isOdd($len))
				$str = substr_replace($str, '>6', -1, 0);
		}
		*/
		
		$sql = 'SELECT 
					label_zpl 
				FROM 
					label_template
				WHERE 
					label_length=\''.$len.'\'';
					
		$row = $ci->db->query($sql)->row_array();
		
		if(!$row)
			echo 'No match template found base on length value';
		else{
			$zpl = str_replace('{{barcode}}', $str, $row['label_zpl']);
			$zpl = str_replace('{{text}}', $orig, $zpl);
		}
		
		return $zpl;
	}

	function isOdd($numb){
		if(($numb % 2) == 0)
			return false;
		
		return true;
	}
}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once "JWT/BeforeValidException.php";
require_once "JWT/ExpiredException.php";
require_once "JWT/JWT.php";
require_once "JWT/SignatureInvalidException.php";

use Firebase\JWT\JWT;

class License{

	// db Config
	private $dbDriver = "mysql";
	private $dbHost = "localhost";
	private $dbUsername = "root";
	private $dbPassword = "";
	private $dbName = "db_salubritas";
	private $dbTable = "app_license";

	// Cookie Config
	private $cookieName = "largo_license";
	private $cookieTime = 0; // day
	private $cookiePath = "/";

	// License Config
	private $secretKey = "LARGO INDONESIA";
	private $server = "http://139.162.36.59/license-server/api/license/gettoken";
	private $token = "";

	public $lifetime = "Unlimited";

	function __construct(){
		if(!array_key_exists('HTTP_TOKEN', $_SERVER)){
			$this->checkToken();
		}

	}

	public function setToken(){

		if(!isset($_COOKIE[$this->cookieName])){

			$header = apache_request_headers();

			if(!array_key_exists('Token', $header)){

				$renewToken = $this->renewToken();
				if(!$renewToken){
					$token = $this->getToken();
				}else{
					$token = $renewToken;
				}

			}else{

				$token = $header['Token'];

			}

		}else{

			$token = $_COOKIE[$this->cookieName];

		}

		return $token;

	}

	public function checkToken($data=0){

		$token = $this->setToken();

		try{

			$decode = JWT::decode($token, $this->secretKey ,['HS256']);

			if($decode->mac_address !== $this->getMacAddress()) throw new Exception("Error Token", 1);
			if($decode->status !== "1") throw new Exception("Token Not Active", 1);

			$date = new DateTime();

			if( $this->is_timestamp($decode->expd) && ($decode->expd + ((3600 * 24) * 30)) < $date->getTimestamp()) throw new Exception("Expired token", 1);

			if($data){
				return $decode;
			}

			setcookie($this->cookieName, $token, time() +  (86400 * 30), '/');

		}catch(Exception $e){

			switch ($e->getMessage()) {
				case 'Expired token':
					$message = "Masa aktif aplikasi anda telah habis";
					break;
				
				case 'Signature verification failed':
					$message = "Silahkan cek lisensi yang anda punya dengan menghubungi tim kami. [ token error ]";
					break;

				case 'Token Not Active':
					$message = "Silahkan cek lisensi yang anda punya dengan menghubungi tim kami [ Token Tidak Aktif ].";
					break;
	
				case 'Error Token':
					$message = "Silahkan cek lisensi yang anda punya dengan menghubungi tim kami [ mac address ga cocok ].";
					break;
				
				default:
					$message='Silahkan cek lisensi yang anda punya dengan menghubungi tim kami. [ token salah ]';
					break;
			}

			return $this->message_error($message);

		}

	}

	private function renewToken(){

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$this->server);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "mac_address=". $this->getMacAddress());

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);

		curl_close ($ch);

		$data = json_decode($server_output, true);

		if ($data['status'] === "OK") {

			if($data['access_token'] !== $this->getToken() ){

				$ci =& get_instance();
				$ci->db->set('access_token', $data['access_token']);
				$ci->db->update($this->dbTable);

				return $data['access_token'];

			}

		}

		return false;

	}

	private function getToken(){

		$ci =& get_instance();

		$ci->db->select('access_token');
		$db = $ci->db->get($this->dbTable);

		if($db->num_rows() < 1){
			$dataToken = [
				"access_token" => $this->token
			];

			$ci->db->insert($this->dbTable, ['access_token'=>$this->token]);

			$token = $this->token;

		}else{

			$token = $db->row_array();
			$token = $token['access_token'];

		}

		return $token;
	}

	public function getMacAddress(){

	   return "F2-3C-91-A3-25-EE";

	   ob_start();  

	   system('ipconfig /all');  
	   $mycomsys=ob_get_contents();
	   ob_clean();  
	   $find_mac = "Physical";
	   $pmac = strpos($mycomsys, $find_mac);  
	   $macaddress=substr($mycomsys,($pmac+36),17);  

	   return $macaddress;

	}

	public function data(){
		$data = $this->checkToken(1);
		return $data;
	}

	public function is_expired(){
		$subscribeDate = $this->data()->expd;
		if($subscribeDate !== NULL){
			$date = new DateTime();

			if($subscribeDate < $date->getTimestamp()){
				return true;
			}
		}

		return false;
	}

	private function message_error($message=""){
		$method = $_SERVER['REQUEST_METHOD'];

		if($method === "GET"){

			$data = $message;

		}else{

			$data['param']		= null;
			$data['status']		= 400;
			$data['message']	= $message;
			$data['response']	= false;

			$data = json_encode($data);

		}

		echo $data;
		exit;
	}

	private function is_timestamp($timestamp)
	{
		$check = (is_int($timestamp) OR is_float($timestamp))
			? $timestamp
			: (string) (int) $timestamp;
		return  ($check === $timestamp)
	        	AND ( (int) $timestamp <=  PHP_INT_MAX)
	        	AND ( (int) $timestamp >= ~PHP_INT_MAX);
	}
}
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Largo V2 | Go Large</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/global/plugins/lightslider/css/lightslider.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />

    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/global/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/global/img/favicon.ico" type="image/x-icon">
    <style>
    body,table,tr,td,th{
        font-size:3vh !important;
    }
    body h1{
        font-size:5vh !important;
        padding: 0 12px !important;
    }
    </style>
</head>
<body class="page-container-bg-solid page-boxed">
    <div class="lightslider">
        <div class="inbound">
            <h1>Inbound</h1>
            <div class="row widget-row">
                <div class="col-md-3">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">TODAY'S RECEIVING</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-green icon-bulb"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><?php echo $widget['total_item']; ?> Items</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty']; ?>">
                                <?php echo $widget['total_qty']; ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-3">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">READY FOR TALLY</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-red icon-layers"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><?php echo $widget['total_tally']; ?> Items</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty_tally']; ?>">
                                <?php echo $widget['total_qty_tally']; ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-3">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">READY FOR PUTAWAY</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><?php echo $widget['total_putaway']; ?> Items</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty_putaway']; ?>">
                                <?php echo $widget['total_qty_putaway']; ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-3">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">AVG. RECEIVE TO BIN</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">Time/Item</span>
                                <span class="widget-thumb-body-stat" style="font-size:25px" data-counter="" data-value="<?php echo $widget['average']; ?>">
                                <?php if(!empty($widget['average'])) echo $widget['average']; else echo '0'; ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered table-striped table_inbound">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Inb Doc</th>
                            <th>Rcv Doc</th>
                            <th>Dlv Doc</th>
                            <th>Supplier</th>
                            <th>Licn Plate</th>
                            <th>Tally/Putaway</th>
                            <th>Last Status</th>
                            <th>Start Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            // $no=1;
                            foreach($inbounds->result() as $inbound){
                                echo '
                                    <tr id="inbound_'.$inbound->id.'">
                                        <td>'.$inbound->id.'</td>
                                        <td>'.$inbound->inb_doc.'</td>
                                        <td>'.$inbound->rcv_doc.'</td>
                                        <td>'.$inbound->dlv_doc.'</td>
                                        <td>'.$inbound->supplier.'</td>
                                        <td>'.$inbound->licn_plate.'</td>
                                        <td>'.$inbound->qty_tally_putaway.'</td>
                                        <td>'.$inbound->last_status.'</td>
                                        <td>'.$inbound->start_time.'</td>
                                    </tr>
                                ';
                                // $no++;
                            }
                        ?>
                        <tr>
                            <td>1</td>
                            <td>INB-001</td>
                            <td>RCV-003</td>
                            <td>DO18009</td>
                            <td>Bhinneka</td>
                            <td>B 8009 NJ</td>
                            <td>60/90</td>
                            <td>Tally</td>
                            <td>9.30</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>INB-002</td>
                            <td>RCV-004</td>
                            <td>LZ09897</td>
                            <td>Lazada</td>
                            <td>Z 8770 KO</td>
                            <td>70/90</td>
                            <td>Putaway</td>
                            <td>10.00</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>INB-002</td>
                            <td>RCV-005</td>
                            <td>IND00102</td>
                            <td>Indofood</td>
                            <td>T 1233 UI</td>
                            <td>80/90</td>
                            <td>Inprocess</td>
                            <td>11.00</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="outbound">
            <h1>Outbound</h1>
            <div class="row widget-row">
                <div class="col-md-2">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">Total Out</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-green-turquoise"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><span class="total_out"><?php echo $total->total_out; ?></span> Items</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $total->total_out; ?>">
                                <span class="total_out"><?php echo $total->total_out; ?></span></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-2">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">Process</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-green icon-bulb"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><span class="total_process"><?php echo $total->process; ?></span> Items</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $total->process; ?>">
                                <span class="total_process"><?php echo $total->process; ?></span></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-2">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">Picking</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-red icon-layers"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><span class="total_picking"><?php echo $total->picking; ?></span> Items</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $total->picking; ?>">
                                <span class="total_picking"><?php echo $total->picking; ?></span></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-2">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">Packing</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><span class="total_packing"><?php echo $total->packing; ?></span> Items</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $total->packing; ?>">
                                <span class="total_packing"><?php echo $total->packing; ?></span></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-2">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">Loading</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><span class="total_loading"><?php echo $total->loading; ?></span> Items</span>
                                <span class="widget-thumb-body-stat" data-counter="" data-value="<?php echo $total->loading; ?>">
                                <span class="total_loading"><?php echo $total->loading; ?></span></span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered table-striped table_outbound">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Picking List</th>
                            <th>Qty</th>
                            <th>Last Status</th>
                            <th>Start Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            // $no=1;
                            foreach($outbounds->result() as $outbound){
                                echo '
                                    <tr id="outbound_'.$outbound->id.'">
                                        <td>'.$outbound->id.'</td>
                                        <td>'.$outbound->picking_list.'</td>
                                        <td>'.$outbound->qty.'</td>
                                        <td>'.$outbound->last_status.'</td>
                                        <td>'.$outbound->start_time.'</td>
                                    </tr>
                                ';
                                // $no++;
                            }
                        ?>
                        <tr>
                            <td>1</td>
                            <td>PL-001</td>
                            <td>5/10</td>
                            <td>Picking</td>
                            <td>11.00</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>PL-002</td>
                            <td>6/10</td>
                            <td>Packing</td>
                            <td>10.00</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>PL-003</td>
                            <td>7/10</td>
                            <td>Loading</td>
                            <td>9.00</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- <div class="col-md-1">
                &nbsp;
            </div>
            <div class="col-md-3">
                <table class="table">
                    <tr>
                        <td>Total Out</td>
                        <td> : <span class="total_out"><?php echo $total->total_out;?></span></td>
                    </tr>
                    <tr>
                        <td>Process</td>
                        <td> : <span class="total_process"><?php echo $total->process;?></span></td>
                    </tr>
                    <tr>
                        <td>Picking</td>
                        <td> : <span class="total_picking"><?php echo $total->picking;?></span></td>
                    </tr>
                    <tr>
                        <td>Packing</td>
                        <td> : <span class="total_packing"><?php echo $total->packing;?></span></td>
                    </tr>
                    <tr>
                        <td>Loading</td>
                        <td> : <span class="total_loading"><?php echo $total->loading;?></span></td>
                    </tr>
                </table>
            </div> -->
        </div>
        <div class="cycle">
            <h1>Cycle Count</h1>
            <div class="col-md-12">
                <table class="table table-bordered table-striped table_cycle">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Loc/Area</th>
                            <th>Qty</th>
                            <th>Status</th>
                            <th>Start Time</th>
                            <th>Result</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            // $no=1;
                            foreach($cycles->result() as $cycle){
                                echo '
                                    <tr id="cycle_'.$cycle->id.'">
                                        <td>'.$cycle->id.'</td>
                                        <td>'.$cycle->loc_area.'</td>
                                        <td>'.$cycle->qty.'</td>
                                        <td>'.$cycle->last_status.'</td>
                                        <td>'.$cycle->start_time.'</td>
                                        <td>'.$cycle->result.'</td>
                                    </tr>
                                ';
                                // $no++;
                            }
                        ?>
                        <tr>
                            <td>1</td>
                            <td>AA</td>
                            <td>80/90</td>
                            <td>Inprocess</td>
                            <td>11.00</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>AB</td>
                            <td>100/100</td>
                            <td>Done</td>
                            <td>9.00</td>
                            <td>Pass</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>AC</td>
                            <td>60/80</td>
                            <td>Inprocess</td>
                            <td>10.00</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>AD</td>
                            <td>70/75</td>
                            <td>Done</td>
                            <td>9.10</td>
                            <td>Desc -5</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>AE</td>
                            <td>97/90</td>
                            <td>Done</td>
                            <td>9.20</td>
                            <td>Desc +7</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/global/plugins/lightslider/js/lightslider.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    var BaseUrl = '<?php echo base_url();?>';
    var slider;
    function start_longpolling(){
    	var evtSource = new EventSource(BaseUrl+'display/longpolling');
    	evtSource.addEventListener("sync", function(e) {
    		var obj = JSON.parse(e.data);
    		var exist=0;

    		if(obj.newc.inbound!=''){
                $('.lslide .table_inbound tbody').prepend(obj.newc.inbound);
                slider.refresh();
    			exist=1;
    		}

            if(obj.newc.exp_inbound.length>0){
                $.each(obj.newc.exp_inbound,function(key,value){
                    $('#inbound_'+value).remove();
                    slider.refresh();
                });
    			exist=1;
    		}

            if(obj.newc.outbound!=''){
                $('.lslide .table_outbound tbody').prepend(obj.newc.outbound);
                slider.refresh();
    			exist=1;
    		}

            if(obj.newc.exp_outbound.length>0){
                $.each(obj.newc.exp_outbound,function(key,value){
                    $('#outbound_'+value).remove();
                    slider.refresh();
                });
    			exist=1;
    		}

            if(obj.newc.cycle!=''){
                $('.lslide .table_cycle tbody').prepend(obj.newc.cycle);
                slider.refresh();
    			exist=1;
    		}

            if(obj.newc.exp_cycle.length>0){
                $.each(obj.newc.exp_cycle,function(key,value){
                    $('#cycle_'+value).remove();
                    slider.refresh();
                });
    			exist=1;
    		}

            $('.total_out').html(obj.newc.total.total_out);
            $('.total_process').html(obj.newc.total.process);
            $('.total_picking').html(obj.newc.total.picking);
            $('.total_packing').html(obj.newc.total.packing);
            $('.total_loading').html(obj.newc.total.loading);

    		if(exist){
    			evtSource.close();
    			start_longpolling();
    		}

    	}, false);
    }

    $(document).ready(function() {
        start_longpolling();
        slider=$(".lightslider").lightSlider({
            item: 1,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,

            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////

            speed: 400, //ms'
            auto: true,
            loop: true,
            slideEndAnimation: true,
            pause: 10000,

            keyPress: false,
            controls: false,
            prevHtml: '',
            nextHtml: '',

            rtl:false,
            adaptiveHeight:true,

            vertical:false,
            verticalHeight:500,
            vThumbWidth:100,

            thumbItem:10,
            pager: false,
            gallery: false,
            galleryMargin: 5,
            thumbMargin: 5,
            currentPagerPosition: 'middle',

            enableTouch:true,
            enableDrag:true,
            freeMove:true,
            swipeThreshold: 40,

            responsive : [],

            onBeforeStart: function (el) {},
            onSliderLoad: function (el) {},
            onBeforeSlide: function (el) {},
            onAfterSlide: function (el) {},
            onBeforeNextSlide: function (el) {},
            onBeforePrevSlide: function (el) {}
        });
    });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

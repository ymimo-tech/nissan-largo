<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>WAREHOUSE TRANSFER REPORT
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div id="portlet-light" class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('print')) : ?>

                        <a href="javascript:;" id="button-export-excel" class="btn blue btn-sm btn-export-excel min-width120" 0=""><i class="fa fa-file-excel-o"></i> &nbsp;EXPORT XLS</a>

					<?php endif; ?>
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">

								<div class="row">
									<div class="col-md-6">


										<div class="form-group item-group">
											<label for="item" class="col-md-3">LPN</label>
											<div class="col-md-8">
												<input type="text" name="sn" class="form-control" id="sn">
												<span class="help-block item-msg"></span>
											</div>
										</div>

										<div class="form-group item-group">
											<label for="item" class="col-md-3">Item</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="item" id="item" style="width: 100%;" multiple data-placeholder="-- All --">

														<?php

															foreach($item as $row){
																echo '<option value="'.$row['id_barang'].'">'.$row['nama_barang'].'</option>';
															}

														?>

													</select>
												</div>
												<span class="help-block item-msg"></span>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>

									</div>
								</div>

							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table id="table-list" class="table report-table">
					<thead>
						<tr>
							<th style="vertical-align: top;text-align: center;width: 30px;">No</th>
							<th style="vertical-align: top;text-align: center;">Outbound Doc. #</th>
							<th style="vertical-align: top;text-align: center;">From WH</th>
							<th style="vertical-align: top;text-align: center;">To WH</th>
							<th style="vertical-align: top;text-align: center;">LPN</th>
							<th style="vertical-align: top;text-align: center;">Item Name</th>
							<th style="vertical-align: top;text-align: center;">Status</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
                </table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i></i><?php echo $title;?> </div>
                <div class="actions">
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
                            <th> No </th>
                            <th> Customer Name </th>
                            <th> Customer Address </th>
                            <th> Customer Contact </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                    
                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="referensi-customer-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Customer Name<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('nama_customer', '', 'class="col-md-12 form-control"') ?>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Customer Address<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('alamat_customer', '', 'class="col-md-12 form-control"') ?>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Customer Contact<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('no_telp', '', 'class="col-md-12 form-control"') ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* harus diisi</span>
                                    <button type="submit" class="btn blue"><i class="icon-save"></i> Simpan</button>
                                    <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Kembali</a>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>  
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
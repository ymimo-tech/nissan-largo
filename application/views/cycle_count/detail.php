<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>CYCLE COUNT DETAIL
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i> &nbsp;DOCUMENT INFO
					</span>
				</div>
                <div class="actions">
					<!--
					<div class="top-info">
						<label>Date : </label>
						<span class=""><?php echo $data['cc_time']; ?></span>
						<label>Created by : </label>
						<span class=""><?php echo !empty($data['user_name']) ? $data['user_name'] : '-'; ?></span>
					</div>
					-->
					<!--
					<table class="table-info">
						<tr>
							<td><strong>Date</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Doc. Type</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Created by</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
					</table>
					-->
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="cc_type" value="<?php echo $data['cc_type']; ?>" />
						<input type="hidden" name="params" value="" />

						<div class="row">
							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Cycle Count Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['cc_code']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['cc_code']; ?>
									</div>
								</div>
							</div>

							<div class="col-md-5">
								<div class="detail-caption text-left">
									<!--<h1>Detail<h1>-->
									<table>
										<tr>
											<td>Cycle Count Date</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['cc_time']; ?></td>
										</tr>
										<tr>
											<td>Cycle Count Type</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['cc_type_alias']; ?></td>
										</tr>
										<tr>
											<td>Created By</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['user_name']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">

									<h1>Cycle Count Status</h1>
									<div class="detail-status
										<?php

											if($data['nama_status'] == 'Open' || $data['nama_status'] == 'In Progress')
												echo 'status-blue';
											else
												echo 'status-green';
										?>">
										<?php echo $data['nama_status']; ?>
									</div>

								</div>
							</div>
						</div>

						<hr style="margin-top: 0;">
						<!--
						<div class="row">
							<div class="col-md-6">
								<h4>Items</h4>
							</div>
							<div class="col-md-6">
								<div class="pull-right">
									<table>
										<tr>
											<td>Total Item</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-item">0</span></strong></td>
										</tr>
										<tr>
											<td>Total Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-qty">0</span></strong></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						-->

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;
								<?php
									if($data['cc_type'] == 'SN')
										echo 'ITEM LIST';
									else
										echo 'LOCATION LIST';
								?>
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="width: 60px;"> No </th>
											<th style="">
												<?php
													if($data['cc_type'] == 'LOC'){
												?>
													<div align="center">
												<?php
													}
												?>
												<?php
													if($data['cc_type'] == 'SN')
														echo 'Item';
													else
														echo 'Location';
												?>
												<?php
													if($data['cc_type'] == 'LOC'){
												?>
												</div>
												<?php
													}
												?>
											</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
						<!--
						<hr class="top30">

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Detail
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-detail" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style=""> No </th>
											<th> <div align="center">Location</div> </th>
											<th style=""> Item Name </th>
											<th> <div align="center">Qty</div> </th>
											<th> <div align="center">Check Result</div> </th>
											<th> <div align="center">Discrepancies</div> </th>
											<th> <div align="center">Actions</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
						-->

					</div>
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">

								<div class="pull-right">
									<!--<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>-->
									<a class="btn default min-width120" href="javascript:;" onclick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
									<?php if ($this->access_right->otoritas('edit')) : ?>

										<?php if ($is_used == 'false') : ?>
											<a href="<?php echo base_url() . $page_class; ?>/edit/<?php echo $id; ?>" class="btn blue min-width120"><i class="fa fa-pencil"></i> EDIT</a>
										<?php endif; ?>

									<?php endif; ?>

									<?php if ($this->access_right->otoritas('print')) : ?>

										<?php if ($is_used == 'false') : ?>
											<button id="print-cc" type="button" class="btn default min-width120"><i class="fa fa-print"></i> PRINT CC DOCUMENT</button>
										<?php endif; ?>

									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

				</form>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

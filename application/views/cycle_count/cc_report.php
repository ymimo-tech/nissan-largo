<html>
<head>
	<title>Cycle Count</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>CYCLE COUNT</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>CC Number</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $cc['cc_code']; ?>&size=65" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $cc['cc_code']; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Cycle Count Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $cc['cc_time']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Cycle Count Type</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $cc['cc_type_alias']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> # </th>
				<th style=""> Location </th>
				<!--<th style=""> QTY </th>-->
				<th> Item Name/SKU </th>
			</tr>
		</thead>
		<tbody>
			<?php
				//<td> '.$row['qty'].' '.$row['nama_satuan'].' </td>

				$i = 1;
				foreach($items['data'] as $row){

					$barcode = '';
					if(!empty($row['kd_barang']))
						$barcode = '<img style="margin-top: 5px;" alt="" src="'.base_url().'barcode?text='.$row['kd_barang'].'&size=35">';

					$str = '<tr>
								<td> '.$i.' </td>
								<td> '.$row['loc_name'].' </td>
								<td {{ align }}>
									<label>'.$row['nama_barang'].'</label>
									<br>
									'.$barcode.'
								</td>
							</tr>';

					if(($i % 2) == 0){
						$str = str_replace('{{ align }}', 'align="right"', $str);
					}

					echo $str;
					$i++;
				}
			?>
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>

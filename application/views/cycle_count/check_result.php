<div class="row">
	
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Stok Opname
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i>
						<?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php 
						if($is_finish == 'true'){ 
							if ($this->access_right->otoritas('import')) {
						?>

						<button type="button" id="pdf" class="btn red min-width120"><i class="fa fa-file-pdf-o"></i> Print PDF </button>
						
					<?php 
							}
						} 
					?>
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">	
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="cc_type" value="<?php echo $data['cc_type']; ?>" />
						<input type="hidden" name="params" value="" />
						
						<div class="row">
							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Stok Opname Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['cc_code']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['cc_code']; ?>
									</div>
								</div>
							</div>
							
							<div class="col-md-5">
								<div class="detail-caption text-left">
									<!--<h1>Detail<h1>-->
									<table>
										<tr>
											<td>Stok Opname Date</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['cc_time']; ?></td>
										</tr>
										<tr>
											<td>Stok Opname Type</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['cc_type_alias']; ?></td>
										</tr>
										<tr>
											<td>Created By</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['user_name']; ?></td>
										</tr>
									</table>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="detail-caption">
									
									<h1>Stok Opname Status</h1>
									<div class="detail-status
										<?php
											
											if($data['nama_status'] == 'Open' || $data['nama_status'] == 'In Progress')
												echo 'status-blue';
											else
												echo 'status-green';
										?>">
										<?php echo $data['nama_status']; ?>
									</div>
									
								</div>
							</div>
						</div>
						
						<hr style="margin-top: 0;">
						<!--
						<div class="row">
							<div class="col-md-6">
								<h4>Items</h4>
							</div>
							<div class="col-md-6">
								<div class="pull-right">
									<table>
										<tr>
											<td>Total Item</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-item">0</span></strong></td>
										</tr>
										<tr>
											<td>Total Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-qty">0</span></strong></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						-->
						<!--
						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;
								<?php 
									if($data['cc_type'] == 'SN')
										echo 'Item';
									else 
										echo 'Location';
								?>
							</span>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="width: 60px;"> No </th>
											<th style=""> 
												<?php 
													if($data['cc_type'] == 'LOC'){
												?>
													<div align="center">	
												<?php 
													}
												?>
												<?php 
													if($data['cc_type'] == 'SN')
														echo 'Item';
													else 
														echo 'Location';
												?>
												<?php 
													if($data['cc_type'] == 'LOC'){
												?>
												</div>
												<?php 
													}
												?>
											</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					
						<hr class="top30">
						-->
						
						<div class="detail-title">
							<div class="row">
								<div class="col-md-6">
									<span class="caption-subject font-green-sharp bold uppercase">
										<i class="fa fa-th-list"></i></i> &nbsp;Check Result
									</span>
								</div>
								
								<div class="col-md-6">
									<div class="pull-right">
										<?php if($data['nama_status']== 'In Progress'){ ?>
											<button id="cancel-all" type="button" class="btn default min-width120"><i class="fa fa-close"></i> ADJUST TO SYSTEM QTY</button>
											<button id="adjust-all" type="button" class="btn green min-width120"><i class="fa fa-refresh"></i> ADJUST TO SCANNED QTY</button>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<table id="table-detail" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th class="table-checkbox">
												<input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> 
											</th>
											<th style="display: none;"> cc_id </th>
											<th style="display: none;"> id_barang </th>
											<th style=""> No </th>
											<th style=""> Item Name </th>
											<!-- <th> <div align="center">Serial Number</div> </th> -->
											<th> <div align="center">System Qty</div> </th>
											<th> <div align="center">Scanned Qty</div> </th>
											<th> <div align="center">Discrepancies</div> </th>
											<th> <div align="center">Status</div> </th>
											<th> <div align="center">Actions</div> </th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								
								<div class="pull-right">
									<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
									 <?php if ($this->access_right->otoritas('add')) : ?>
									 
										<?php if($is_finish == 'true' && $is_close == 'false'){ ?>


											<button type="button" id="save" class="btn blue min-width120"><i class="fa fa-check"></i> SAVE</a>
											
										<?php } ?>
										
									 <?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					
				</form>
            </div>
     
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="modal-remark" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal" class="form-horizontal">
				<input type="hidden" name="id"/>
				
				<div class="modal-header">
					<button type="button" class="close form-close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Close Stok Opname</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
						   <div class="form-group do-group">
								<label class="col-md-2 control-labe">Remark</label>
								<div class="controls col-md-9">
									<textarea class="form-control" id="remark" name="remark" placeholder="remark"></textarea>
								</div>
							</div> 
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-remark" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-check"></i> &nbsp;SUBMIT</button>
					</div>
				</div>
			
			</form>
		</div>
	</div>
</div> 
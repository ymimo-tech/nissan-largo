<html>
<head>
	<title>Report - Cycle Count</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Report - Cycle Count</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>CC Number</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $cc['cc_code']; ?>&size=65" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $cc['cc_code']; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Cycle Count Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $cc['cc_time']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Cycle Count Type</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $cc['cc_type_alias']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Operator</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<?php
									foreach ($actor as $act) {
									?>
									<span>
										- <?php echo $act['user_name']; ?><br/>
									</span>
									<?php
									}
									?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> No </th>
				<th> Item Name </th>
				<th> System Qty </th>
				<th> Scanned Qty </th>
				<th> Discrepancies </th>
			</tr>
		</thead>
		<tbody>
			<?php
				//<td> '.$row['qty'].' '.$row['nama_satuan'].' </td>

				$i = 1;
				foreach($cc_result as $row){

					$str = '<tr>
								<td> '.$i.' </td>
								<td> '.$row['nama_barang'].' </td>
								<td>
									<label>'.$row['system_qty'].' '. $row['nama_satuan'] .'</label>
								</td>
								<td>
									<label>'.$row['scanned_qty'].' '. $row['nama_satuan'] .'</label>
								</td>
								<td>
									<label>'.($row['system_qty'] - $row['scanned_qty']).' '. $row['nama_satuan'] .'</label>
								</td>';

					foreach($sa as $sa_aktor){
						if(isset($sc[$row['id_barang']][$sa_aktor])){
							$str.='<td>'.$sc[$row['id_barang']][$sa_aktor].'</td>';
						}else{
							$str.='<td>0</td>';
						}
					}

					$str.='<td>
									<label></label>
								</td>
								<td>
									<label></label>
								</td>
								<td>
									<label></label>
								</td>
							</tr>';

					if(($i % 2) == 0){
						$str = str_replace('{{ align }}', 'align="right"', $str);
					}

					echo $str;
					$i++;
				}
			?>
			<tr style="border:0;margin-top:40px;"><td colspan="8">&nbsp;</td></tr>
			<!-- BUG: lokasi kalau bisa jangan di hard-code! -->
			<tr style="border:0;margin-top:40px;"><td colspan="8">Jakarta, <?php echo DateTime::createFromFormat('Y-m-d', date('Y-m-d'))->format('d/m/Y');?></td></tr>
		</tbody>
	</table><br/><br/>
	<!-- end table -->
</body>
</html>

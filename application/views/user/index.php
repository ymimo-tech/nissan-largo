<style type="text/css">
    .select2-container--open{
        z-index:99999;
    }
</style>
<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>USER MANAGEMENT
				<small>List of Largo app users</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
                    	<?php if(!empty($subTitle)) echo $subTitle;?>
                	</span>
				</div>
				<div class="actions">
					<?php if ($this->access_right->otoritas('edit')) : ?>
						<a class="btn blue btn-sm data-table-renew min-width120">
							<i class="fa fa-key"></i> Renew License </a>
					<?php endif; ?>
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>

					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
				<div class="row">
					<div class="col-md-12">

		                <div class="portlet box blue-hoki filter-container">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            Filter
		                        </div>
		                        <div class="tools">
		                            <a href="javascript:;" class="filter-indicator">Show filter</a>
		                            <a href="javascript:;" class="expand show-hide">
		                            </a>
		                        </div>
		                    </div>
		                    <div class="portlet-body" style="display: none;">

			                        <!--YOUR CUSTOM FILTER HERE-->
			                        <form id="form-search" class="form-horizontal" role="form">
			                            <div class="form-body">


			                                <div class="row">
			                                    <div class="col-md-6">

			                                        <div class="form-group po-group">
			                                            <label for="po" class="col-md-3">Full Name</label>
			                                            <div class="col-md-8">
			                                                <div class="custom-select select-type4 high location">

			                                                	<select class="form-control" id="name" name="name" style="width: 100%;"></select>
			                                                </div>
			                                                <span class="help-block po-msg"></span>
			                                            </div>
			                                        </div>
			                                         <div class="form-group po-group">
			                                            <label for="po" class="col-md-3">Username</label>
			                                            <div class="col-md-8">
			                                                <div class="custom-select select-type4 high location">
			                                                	<select class="form-control" id="username" name="username" style="width: 100%;"></select>
			                                                </div>
			                                                <span class="help-block po-msg"></span>
			                                            </div>
			                                        </div>

			                                    </div>


			                                    <div class="col-md-6">

			                                        <div class="form-group po-group">
			                                            <label for="po" class="col-md-3">Role</label>
			                                            <div class="col-md-8">
			                                                <div class="custom-select select-type4 high location">
			                                                	<select class="form-control" id="roles" name="roles" style="width: 100%;"></select>
			                                                </div>
			                                                <span class="help-block po-msg"></span>
			                                            </div>
			                                        </div>
			                                         <div class="form-group po-group">
			                                            <label for="po" class="col-md-3">Status</label>
			                                            <div class="col-md-8">
			                                                <div class="custom-select select-type4 high location">
			                                                	<select class="form-control" id="status" name="status" style="width: 100%;">
			                                                		<option>Online</option>
			                                                		<option>Offline</option>
			                                                	</select>
			                                                </div>
			                                                <span class="help-block po-msg"></span>
			                                            </div>
			                                        </div>

			                                    </div>

			                                </div>
			                            </div>
			                            <div class="form-actions">
			                                <div class="row">
			                                    <div class="col-md-12">
			                                        <div class="pull-right">
			                                            <button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
			                                            <button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </form>
			                        <!--END-->
		                    </div>
		                </div>

						<table id="table_user" class="table table-striped table-bordered table-hover table-checkable order-column">
							<thead>
								<tr>
									<th style="vertical-align: top;text-align: center;width: 30px;"> No </th>
									<th style="vertical-align: top;text-align: center;"> Full Name </th>
									<th style="vertical-align: top;text-align: center;"> Username </th>
									<th style="vertical-align: top;text-align: center;"> Password </th>
									<th style="vertical-align: top;text-align: center;"> Role </th>
									<th style="vertical-align: top;text-align: center;"> Status </th>
									<th style="vertical-align: top;text-align: center;"> Action </th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>
            </div>
     		<?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
	            <div id="form-content" class="modal fade" role="basic">
	                <div class="modal-dialog">
	                    <div class="modal-content">
	                        <form id="data-table-form" class="form-horizontal">
	                            <input type="hidden" name="id"/>
	                            <div class="modal-header">
	                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
	                            </div>
	                            <div class="modal-body">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                       	<div class="form-group">
	                                            <label class="col-md-3 control-label">NIK<span class="required">*</span></label>
	                                            <div class="controls col-md-9">
	                                                <?php echo form_input('nik','', 'class="col-md-12 form-control"') ?>
	                                            </div>
	                                        </div>
	                                       	<div class="form-group">
	                                            <label class="col-md-3 control-label">Full Name<span class="required">*</span></label>
	                                            <div class="controls col-md-9">
	                                                <?php echo form_input('nama','', 'class="col-md-12 form-control"') ?>
	                                            </div>
	                                        </div>
	                                       	<div class="form-group">
	                                            <label class="col-md-3 control-label">Role<span class="required">*</span></label>
	                                            <div class="controls col-md-9">
	                                                <?php echo form_dropdown('grup', $grups, '', 'data-live-search="true" class="selectpicker col-md-12 form-control"') ?>
	                                            </div>
	                                        </div>
	                                       	<div class="form-group">
	                                            <label class="col-md-3 control-label">Username<span class="required">*</span></label>
	                                            <div class="controls col-md-9">
	                                                <?php echo form_input('user_name','', 'class="col-md-12 form-control"') ?>
	                                            </div>
	                                        </div>
	                                       	<div class="form-group">
	                                            <label class="col-md-3 control-label">Password<span class="required">*</span></label>
	                                            <div class="controls col-md-9">
	                                                <?php echo form_password('password','', 'class="col-md-12 form-control"') ?>
	                                            </div>
	                                        </div>
	                                        <div class="form-group">
	                                            <label class="col-md-3 control-label">Assigned Warehouse</label>
	                                            <div class="controls col-md-9">
	                                                <?php
	                                                    echo form_dropdown('warehouse[]', $warehouses, '', 'class="col-md-12 form-control" style="width: 100%;" id="warehouse" multiple data-placeholder="--Please Select--"');
	                                                ?>
	                                            </div>
											</div>
											<div class="form-group">
	                                            <label class="col-md-3 control-label">Assigned Zone</label>
	                                            <div class="controls col-md-9">
	                                                <?php
	                                                    echo form_dropdown('zone[]', $zones, '', 'class="col-md-12 form-control" style="width: 100%;" id="zone" multiple data-placeholder="--Please Select--"');
	                                                ?>
	                                            </div>
	                                        </div>
	                                       	<div class="form-group">
	                                            <label class="col-md-3 control-label">Status<span class="required">*</span></label>
	                                            <div class="controls col-md-9">
	                                                <?php echo form_dropdown('status', $statuses, '', 'data-live-search="true" class="selectpicker col-md-12 form-control"') ?>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="modal-footer">
	                                <div class="form-actions">
										<span class="required pull-left">* is required</span>
										<a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
										<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
										<!--
	                                    <span class="required pull-left">* harus diisi</span>
	                                    <button type="submit" class="btn blue"><i class="icon-save"></i> Simpan</button>
	                                    <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Kembali</a>
										-->
	                                </div>

	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>REPLENISHMENT
				<small>This list shows bin transfer (or bin-to-bin) movement performed on RF</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div id="portlet-light" class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
											<i class="fa fa-th-list"></i> Bin Movements
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('print')) : ?>

						<a href="javascript:;" id="button-export-excel" class="btn blue btn-sm btn-export-excel min-width120" 0=""><i class="fa fa-file-excel-o"></i> &nbsp;EXPORT</a>
						<a href="javascript:;" id="button-export" class="btn green btn-sm btn-export min-width120" 0=""><i class="fa fa-file-pdf-o"></i> &nbsp;EXPORT</a>

					<?php endif; ?>
					<!--
                    <a class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
					-->
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">

								<div class="row">
									<div class="col-md-6">

										<div class="form-group item-group">
											<label for="item" class="col-md-3">Item</label>
											<div class="col-md-8">
												<select class="form-control"
													name="item" id="item" style="width: 100%;" multiple
													data-placeholder="-- All --">

													<?php
														foreach($item as $r){
															echo '<option value="'.$r['id_barang'].'">'.$r['nama_barang'].'</option>';
														}
													?>

												</select>
												<span class="help-block item-msg"></span>
											</div>
										</div>

									</div>
								</div>

							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table id="table-list" class="table table-striped table-bordered table-hover report-table dataTable no-footer">
					<thead>
						<tr class="uppercase">
							<th style="vertical-align: top;text-align: center;"> No </th>
							<th style="vertical-align: top;text-align: center;"> Item Code </th>
							<th style="vertical-align: top;text-align: center;"> Item Name </th>
							<th style="vertical-align: top;text-align: center;"> Min Replenish Qty </th>
							<th style="vertical-align: top;text-align: center;"> Pick Qty </th>
							<th style="vertical-align: top;text-align: center;"> Put Qty </th>
							<th style="vertical-align: top;text-align: center;"> Total Qty </th>
						</tr>
					</thead>
					<tbody>
					</tbody>
                </table>
            </div>
						<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

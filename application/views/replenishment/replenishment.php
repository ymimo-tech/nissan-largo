<html>
<head>
	<title>Replenishment</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, .table-pdf tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
		.table-pdf tbody tr td.header-report-table{
			background: #fafafa;
			padding: 10px;
			font-weight: bold;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Replenishment</h3>
	<hr>
	<br /><br />
	<!-- begin table -->
	<table id="table-pdf" class="table report-table table-pdf">
		<thead>
			<tr>
				<th style="vertical-align: top;text-align: center;"> No </th>
				<th style="vertical-align: top;text-align: center;"> Item Code </th>
				<th style="vertical-align: top;text-align: center;"> Item Name </th>
				<th style="vertical-align: top;text-align: center;"> Min Replenish Qty </th>
				<th style="vertical-align: top;text-align: center;"> Pick Qty </th>
				<th style="vertical-align: top;text-align: center;"> Put Qty </th>
				<th style="vertical-align: top;text-align: center;"> Total Qty </th>
			</tr>
		</thead>
		<tbody>

			<?php

				$data = $data;
				$len = count($data);

				$tpl = '<tr>
							<td style="text-align: center;">{{ no }}</td>
							<td style="text-align: left;">{{ sku }}</td>
							<td style="text-align: left;">{{ item }}</td>
							<td style="text-align: right;">{{ minimum_stock }}</td>
							<td style="text-align: right;">{{ qty_picking }}</td>
							<td style="text-align: right;">{{ qty_putaway }}</td>
							<td style="text-align: right;">{{ total }}</td>
					   </tr>';
				$row = '';

				if($len > 0){

					for($i = 0; $i < $len; $i++){
						$total = $data[$i]['qty_picking'] + $data[$i]['qty_putaway'];
						$row .= $tpl;
						$row = str_replace('{{ no }}', ($i + 1), $row);
						$row = str_replace('{{ sku }}', $data[$i]['kd_barang'], $row);
						$row = str_replace('{{ item }}', $data[$i]['nama_barang'], $row);
						$row = str_replace('{{ minimum_stock }}', $data[$i]['minimum_stock'], $row);
						$row = str_replace('{{ qty_picking }}', $data[$i]['qty_picking'], $row);
						$row = str_replace('{{ qty_putaway }}', $data[$i]['qty_putaway'], $row);
						$row = str_replace('{{ total }}', $total, $row);  //total??????
					}

					echo $row;

				}else{

					echo '<tr><td colspan="7" style="text-align: center;">No data</td></tr>';

				}
			?>

		</tbody>
	</table>
	<!-- end table -->
</body>
</html>

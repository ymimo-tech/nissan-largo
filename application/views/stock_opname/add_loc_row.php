<tr class="added_row">
	<td>#</td>
	<td class="form-group">
		<select class="form-control dnumber add_rule area_select" name="area_id[]">
			<?php
				echo '<option value="">-- Choose Area --</option>';
				foreach($areas as $row){

					$sel = '';
					if(isset($location)){
						if($location['area_id'] == $row['loc_area_id'])
							$sel = 'selected';
					}

					echo '<option value="'.$row['loc_area_id'].'"
							data-name="'.$row['loc_area_name'].'" '.$sel.'>'.$row['loc_area_name'].'</option>"';
				}
			?>
		</select>
	</td>
	<td class="form-group">
		<select class="form-control dnumber add_rule loc_select" name="loc_id[]">
			<?php
				echo '<option value="">-- Choose Area First --</option>';
				foreach($loc as $row){

					$sel = '';
					if(isset($location)){
						if($location['loc_id'] == $row['loc_id'])
							$sel = 'selected';
					}

					echo '<option value="'.$row['loc_id'].'"
							data-name="'.$row['loc_name'].'" '.$sel.'>'.$row['loc_name'].'</option>"';
				}
			?>
		</select>
	</td>
	<td class="remove_row">
		<button class="btn btn-delete btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
	</td>
</tr>

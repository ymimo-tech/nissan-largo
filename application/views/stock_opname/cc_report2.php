<html>
<head>
	<title>Cycle Count</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<?php
		$nama_aktor='';
		$sc=array();
		$sa=array();
		foreach($scans as $scan){
			$nama_aktor.=$scan['nama'].'<br/>';
			$sc[$scan['id_barang']][$scan['nama']]=$scan['qty'];
			$sa[]=$scan['nama'];
		}
	?>
	<h3>Cycle Count</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>CC Number</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $cc['cc_code']; ?>&size=65" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $cc['cc_code']; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Cycle Count Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $cc['cc_time']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Cycle Count Type</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $cc['cc_type_alias']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120" style="vertical-align:top;">
									<label>Actor</label>
								</td>
								<td class="width20" style="vertical-align:top;">
								:
								</td>
								<td style="vertical-align:top;">
									<span>
										<?php echo $nama_aktor;?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> # </th>
				<th style=""> Location </th>
				<!--<th style=""> QTY </th>-->
				<th> SKU </th>
				<th> Item Name </th>
				<th> QTY </th>
				<!-- LOOP ACTOR & QTY -->
				<?php
					foreach($sa as $sa_aktor){
						echo '<th>'.$sa_aktor.'</th>';
					}
				?>
				<!-- LOOP ACTOR & QTY -->
				<th> Dif </th>
				<th> Cost </th>
				<th> Sign </th>
			</tr>
		</thead>
		<tbody>
			<?php
				//<td> '.$row['qty'].' '.$row['nama_satuan'].' </td>

				$i = 1;
				foreach($items['data'] as $row){

					$str = '<tr>
								<td> '.$i.' </td>
								<td> '.$row['loc_name'].' </td>
								<td>
									<label>'.$row['kd_barang'].'</label>
								</td>
								<td>
									<label>'.$row['nama_barang_ori'].'</label>
								</td>
								<td>
									<label>'.$row['qty'].'</label>
								</td>';

					foreach($sa as $sa_aktor){
						if(isset($sc[$row['id_barang']][$sa_aktor])){
							$str.='<td>'.$sc[$row['id_barang']][$sa_aktor].'</td>';
						}else{
							$str.='<td>0</td>';
						}
					}

					$str.='<td>
									<label></label>
								</td>
								<td>
									<label></label>
								</td>
								<td>
									<label></label>
								</td>
							</tr>';

					if(($i % 2) == 0){
						$str = str_replace('{{ align }}', 'align="right"', $str);
					}

					echo $str;
					$i++;
				}
			?>
			<tr style="border:0;margin-top:40px;"><td colspan="8">&nbsp;</td></tr>
			<tr style="border:0;margin-top:40px;"><td colspan="8">Jakarta, <?php echo DateTime::createFromFormat('Y-m-d', date('Y-m-d'))->format('d/m/Y');?></td></tr>
		</tbody>
	</table><br/><br/>
	<table class="table-pdf" style="width:100%">
		<tr style="border:0;">
	<?php
		foreach($sa as $sa_aktor){
			echo '<td>'.$sa_aktor.'</td>';
		}
	?>
		</tr>
	</table>
	<!-- end table -->
</body>
</html>

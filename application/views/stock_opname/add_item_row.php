<tr class="added_row">
	<td>#</td>
	<td class="form-group">
		<?php //echo form_dropdown("id_barang[]", $items, isset($item) ? $item['item_id'] : "", "class='form-control add_rule'"); ?>
		
		<select class="form-control add_rule" name="id_barang[]">
			<?php
				echo '<option value="">-- Choose item --</option>';
				foreach($items as $row){
					
					$sel = '';
					if(isset($item)){
						if($item['item_id'] == $row['id_barang'])
							$sel = 'selected';
					}
					
					echo '<option value="'.$row['id_barang'].'"
							data-name="'.$row['nama_barang'].'" '.$sel.'>'.$row['nama_barang'].'</option>"';
				}
			?>
		</select>
	</td>
	<td class="remove_row">
		<button class="btn btn-delete btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
	</td>
</tr>
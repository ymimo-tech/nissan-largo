<html>
<head>
	<title>Surat Jalan</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			background:#000;
			color:#fff;
			font-weight:normal;
		}
		.table-pdf tr td{
			background:#fff;
			color:#000;
			border:1px solid #000;
		}
		.no-border tr td{
			border-bottom:0;
			background:transparent;
			color:#000;
			border:0;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 22px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<table class="table-pdf no-border">
		<tr>
			<td>
				<img src="<?php echo base_url('assets/global/img/logo-gojek.png');?>" style="width:200px;"/>
			</td>
			<td style="text-align:right">
				<h4>SURAT JALAN <br/><?php echo date('Y-m-d H:i:s');?></h4>
			</td>
		</tr>
	</table>
	<table style="width:100%;margin-top:20px;">
		<tr>
			<td style="width: 240px; text-align: left; vertical-align: top;padding-left:20px;padding-bottom:20px;">
				ORDER ID #<?php echo $data[0]['outbound_code'];?><br/>
				Sender : www.go-jek.com
			</td>
			<td style="padding-left:65px;vertical-align:top;" rowspan="2">
				<b>DIKIRIM KEPADA</b><br/>
				<?php echo $data[0]['destination_name'];?><br/>
				<?php echo $data[0]['address_1'];?><br/>
				<?php echo $data[0]['address_2'];?><br/>
				<?php echo $data[0]['address_3'];?><br/>
			</td>
		</tr>
		<tr>
			<td style="width: 240px; text-align: center; vertical-align: top;">
				<label>Packing Note</label><br>
				<!-- <img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $pack['packing_number'].'-'.$key.'/'.$maks;?>&size=45" style=""> -->
				<br>
				<span class="tally-rcv-code">
				</span>
			</td>
		</tr>
	</table>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> SKU ID</th>
				<th> Nama SKU </th>
				<th> Variant </th>
				<th> Qty </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($data as $d){
				echo '<tr><td>'.$d['item_code'].'</td><td>'.$d['item_name'].'</td><td>-</td><td>'.$d['qty'].'</td></tr>';
			}
			?>
		</tbody>
	</table>
	<!-- end table -->

	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> License Plate </th>
				<th> Outer Label </th>
				<th> Qty </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($data['truck'] as $d){
				echo '<tr><td>'.$d['license_plate'].'</td><td>'.$d['outerlabel'].'</td><td>'.$d['qty'].'</td></tr>';
			}
			?>
		</tbody>
	</table>


	<!-- end table -->

</body>
</html>

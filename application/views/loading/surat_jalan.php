<html>

<head>
	<title>Surat Jalan</title>
	<style>
	.footer {
		  position: fixed;
		  left: 0;
		  bottom: 0;
		  width: 100%;
		  text-align: center;
		  padding-bottom:70px;
		}
	</style>
</head>

<body style="font-family: Arial;">
	<?php for($i=0; $i<count($data);$i++){?>
		<div class="wrapper">
			<div class="wrapper-title" style="text-align: right;">
				<h2>SURAT JALAN | <?php echo $data[$i]['prio']['m_priority_name']; ?> PRIORITY</h2>
				<span>Printed on : <?php echo date('Y/m/d H:i');?></span>
			</div>
			<div class="wrapper-body" style="margin-top: 20px;">

				<div class="">
					<div style="float: left;width: 49.8%;">
						<img alt=""
							src="<?php echo base_url(); ?>barcode?text=<?php echo $data[$i]['outbound_code'];?>&size=65"
							style="border:1px solid;">
						<table style="">
							<tr>
								<td>Order ID # </td>
								<td><?php echo $data[$i]['outbound_code'];?></td>
							</tr>
						</table>
					</div>
					<div style="float: left;width: 49.8%; text-align: right;">
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data[$i]['shipping_code'];?>&size=65"
							style="border:1px solid;" style="border:1px solid;">
						<table align="right">
							<tr>
								<td><?php echo $data[$i]['shipping_code'];?></td>
							</tr>
						</table>
					</div>
				</div>

				<div class="" style="margin-top: 20px;">
					<div style="float: left; width: 49.5%;">
						<table>
							<tr>
								<td style="vertical-align: top;">Sender:</td>
								<td>
									<?php echo $data[$i]['warehouse_name']; ?>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>Driver</td>
								<td><?php echo $data[$i]['driver']; ?></td>
							</tr>
							<tr>
								<td>License Plate</td>
								<td><?php echo $data[$i]['license_plate']; ?></td>
							</tr>dia
						</table>
					</div>
					<div style="float: left; width: 49.5%;">
						<table>
							<tr>
								<td>DIKIRIM KEPADA</td>
							</tr>
							<tr>
								<td>
									<?php echo $data[$i]['destination_name'];?><br />
								</td>
							</tr>
							<tr>
								<td>
									<?php echo $data[$i]['address'];?><br />
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<div class="" style="margin-top: 50px;">

				<style type="text/css">
					table.table {
						border-collapse: collapse;
						text-align: center;
					}

					table.table thead tr th {
						border: 1px solid black;
						background-color: #dedede;
						font-weight: normal !important;
					}

					table.table tbody tr td {
						border: 1px solid;
						padding: 10px 0;
					}

					table.table tfoot tr td {
						border: 1px solid;
					}
				</style>

				<table width="100%" class="table">
					<thead>
						<tr>
							<th>List DN</th>
							<th>Dealer</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<?php for($x=0; $x<count($data[$i]['detail_item']);$x++){ 
							$name .= $data[$i]['detail_item'][$x]['name'];
							$total += $data[$i]['detail_item'][$x]['total_colly'];
							if($x != count($data[$i]['detail_item'])-1){
								$name .= ', ';
							}
						}
						$name = implode(', ',array_unique(explode(', ', $name)));
						?>
								<tr>
								<td><?php echo $name; ?><td>
								<td><?php echo $total.' Colly';?></td>
								</tr>
					</tbody>
				</table>
			</div>
		</div>
		<?php if(($i != (count($data)-1)) || ($i == count($data)-1)){?> 
			<div class="footer">
				<div class="col-sm">
					<table>
						<thead >
							<tr>
								<th>NMDI</th>
								<th>Logistic</th>
								<th>Dealer</th>
							</tr>
						</thead>
						<tbody>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr><td></td><td></td><td></td></tr>
							<tr>
								<td>__________________________</td>
								<td>__________________________</td>
								<td>__________________________</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm mt-4">
					<p style="font-size: 10px;" align="left">
					1. Asli kembali ke NMDI SPC <br>
					2. Dealer Parts <br>
					3. Delaer Finance <br>
					4. Logistic <br>
					5. NMDI SPC <br>
					</p>
				</div>
			</div>
			<?php if($i !== count($data)-1) echo '<pagebreak>';$name='';$total=0;?> 
				<?php }?>
				<?php }?>
			</body>
</html>

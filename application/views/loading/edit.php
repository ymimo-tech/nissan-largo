<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Loading
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
	<div class="col-md-12 col-sm-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i><?php echo $title; ?>
					</span>
				</div>
				<div class="actions">
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
				</div>
			</div>
			<div class="portlet-body form">
				<div class="form-body">
					<form id="data-table-form" class="form-horizontal">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="params" value="" />
						<input type="hidden" name="not_used" value="<?php echo $not_usage; ?>" />

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-2">Manifest Number<span class="required">*</span></label>
									<div class="controls col-md-6">
										<div style="">
											<input type="hidden" id="loading-code" name="loading_code"
												class="form-control" value="<?php echo $shipping_code; ?>"
												maxLength="50" />
											<strong><span
													class="loading-code"><?php echo $shipping_code; ?></span></strong>
										</div>
									</div>
								</div>
								<div class="form-group tanggal-loading-group top20">
									<label class="col-md-2">Loading Date<span class="required">*</span></label>
									<div class="col-md-6">
										<div class="input-group">
											<!-- <input type="text" class="form-control" id="tanggal-loading"
												name="tanggal_loading" placeholder="dd/mm/yyyy"
												value="<?php echo $today; ?>" maxLength="10" />
											<span class="input-group-btn">
												<button class="btn default" type="button"><i
														class="fa fa-calendar"></i></button>
											</span> -->
											<strong><span class="loading-code"><?php echo $today; ?></span></strong>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Gate<span class="required">*</span></label>
									<div class="controls col-md-6">
										<!-- <select name="gates_id" id="gates_id" class="form-control">
										<?php foreach($gates as $key => $gate){ ?>
											<option value="<?php echo $gate['id'] ?>"> <?php echo $gate['gate_code'] ?></option>
										<?php } ?>
										</select> -->
										<strong><span class="loading-code"><?php echo $gate; ?></span></strong>
										<input type="hidden" id="gates_id" name="gates_id"
												class="form-control" value="<?php echo $gate_id; ?>"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Driver<span class="required">*</span></label>
									<div class="controls col-md-3">
										<input type="text" id="driver" name="driver" class="form-control" value="<?php echo $driver?>"
											maxLength="100" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Plate Number<span class="required">*</span></label>
									<div class="controls col-md-3">
										<input type="text" id="plate" name="plate" class="form-control" value="<?php echo $license_plate?>"
											maxLength="20" />
										<!--select id="plate" name="plate" class="form-control" value=""></select-->
									</div>
								</div>
								<!-- 							   <div class="form-group">
									<label class="col-md-2">Barcode<span class="required">*</span></label>
									<div class="controls col-md-6">
										<input type="text" id="barcode" name="barcode" class="form-control" value="" maxLength="20" />
									</div>
								</div>
 -->
								<div class="form-group">
									<label class="col-md-2">Packing Number (loaded)</label>
									<div class="controls col-md-6">
										<strong><span class="loading-code"><?php if(count($doc_number)>0){echo $doc_number;}else{echo '-';} ?></span></strong>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Add Packing Number<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select id="outbound_document" name="outbound_document" class="form-control" multiple>
											<?php
												for($z=0;$z<count($undoc_number);$z++){
													echo '<option value="'.$undoc_number[$z]['id'].'" selected>'.$undoc_number[$z]['packing_number'].'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<!--
								<div class="form-group">
									<label class="col-md-2">Doc. Number<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select id="outbound" name="outbound" class="form-control">
											<?php
												//echo '<option value="">-- Choose outbound document --</option>';
												//foreach($doc_number as $row){
													//echo '<option value="'.$row['id_outbound'].'">'.$row['kd_outbound'].'</option>';
												//}

											?>
										</select>
									</div>
								</div>
								-->
								<!--
								<div class="form-group">
									<label class="col-md-2">Document Number<span class="required">*</span></label>
									<div class="col-md-6">

										<select id="outbound" name="outbound" class="form-control">
											<?php
											/*
												foreach($doc_number as $row){
													echo '<option value="'.$row['id_outbound'].'">'.$row['kd_outbound'].'</option>';
												}
											*/
											?>
										</select>

									</div>
								</div>
								-->
							</div>
						</div>

						<!-- <hr> -->
						<!--<h4>Items</h4>-->


						<div class="form-actions top30">
							<div class="row">
								<div class="col-md-12">
									<span class="required pull-left">* is required</span>
									<div class="pull-right">

										<?php if(empty($id)) { ?>
										<a class="btn default min-width120"
											href="<?php echo base_url().$page_class; ?>"><i
												class="fa fa-angle-double-left"></i> BACK</a>
										<?php } else { ?>
										<a class="btn default min-width120" href="javascript:;"
											onClick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i>
											BACK</a>
										<?php } ?>

										<?php if($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')){ ?>

										<button id="save" type="submit" class="btn blue min-width120"> SAVE</button>

										<?php if(empty($id)){ ?>
										<button id="save-new" type="button" class="btn green-haze min-width120"> SAVE &
											NEW</button>
										<?php } ?>

										<?php } ?>

									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

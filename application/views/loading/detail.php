<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>LOADING DETAIL
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i>
						<?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<div class="top-info">
						<label>Date : </label>
						<span class=""><?php echo isset($data[0]['shipping_date']) ? $data[0]['shipping_date'] : '' ; ?></span>
						<label>Created by : </label>
						<span class=""><?php echo !empty($data[0]['user_name']) ? $data[0]['user_name'] : '-'; ?></span>
					</div>
					<!--
					<table class="table-info">
						<tr>
							<td><strong>Date</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Doc. Type</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Created by</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
					</table>
					-->
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="manifest_code" value="<?php echo $manifest_number; ?>" />
						<input type="hidden" name="params" value="" />

						<div class="row">
							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Loading Number<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo isset($data['shipping_code']) ? $data['shipping_code'] : ''; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $manifest_number; ?>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>Destination<h1>
										<div class="bottom5 sup-title">
											<strong><?php echo (count($data) > 1) ? "Consolidate" : (isset($data[0]['customer_name']) ? $data[0]['customer_name'] : ''); ?></strong>
										</div>
										<table>
											<tr>
												<td>Code</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo (count($data) > 1) ? "Consolidate" : (isset($data[0]['kd_supplier']) ? $data[0]['kd_supplier'] : ''); ?></td>
											</tr>
											<tr>
												<td>Address</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo (count($data) > 1) ? "Consolidate" : (isset($data[0]['alamat_supplier']) ? $data[0]['alamat_supplier'] : ''); ?></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo (count($data) > 1) ? "Consolidate" : (isset($data[0]['telepon_supplier']) ? $data[0]['telepon_supplier'] : ''); ?></td>
											</tr>
											<tr>
												<td style="width: 100px;">Contact Person</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo (count($data) > 1) ? "Consolidate" : (isset($data[0]['cp_supplier']) ? $data[0]['cp_supplier'] : ''); ?></td>
											</tr>
										</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>&nbsp;</h1>
									<table>
										<tr>
											<td>Doc. Name</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong>
												<?php echo $manifest_number
												//
													// if(count($datas) > 1){
													// 	for ($i=0; $i < count($datas); $i++){

													// 		echo $datas[$i]['kd_outbound'];
													// 		if(count($datas) != $i+1){
													// 			echo ", ";
													// 		}
													// 	}
													// }
												?>
											</strong></td>
										</tr>
										<tr>
											<td>Driver</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo isset($data[0]['driver_name']) ? $data[0]['driver_name'] : ''; ?></td>
										</tr>
										<tr>
											<td>Transporter #</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo isset($data[0]['license_plate']) ? $data[0]['license_plate'] : ''; ?></td>
										</tr>
										<tr>
											<td>Start</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo isset($data[0]['shipping_start']) ? $data[0]['shipping_start'] : ''; ?></td>
										</tr>
										<tr>
											<td>Finish</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo isset($data[0]['shipping_finish']) ? $data[0]['shipping_finish'] : ''; ?></td>
										</tr>
									</table>
									<!--
									<h1>Outbound Status</h1>
									<div class="detail-status
										<?php
											/*
											if($data['nama_status'] == 'Open' || $data['nama_status'] == 'In Progress')
												echo 'status-blue';
											else
												echo 'status-green';*/
										?>">
										<?php //echo $data['nama_status']; ?>
									</div>
									-->
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Loading Status</h1>
									<div class="detail-status
										<?php
											if(isset($data[0]['nama_status']) == 'Open' || isset($data[0]['nama_status']) == 'In Progress')
												echo 'status-blue';
											else
												echo 'status-green';
										?>">
										<?php echo isset($data[0]['nama_status']) ? $data[0]['nama_status'] : ''; ?>
									</div>
								</div>
							</div>
						</div>

						<hr style="margin-top: 0;">
						<!--
						<div class="row">
							<div class="col-md-6">
								<h4>Items</h4>
							</div>
							<div class="col-md-6">
								<div class="pull-right">
									<table>
										<tr>
											<td>Total Item</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-item">0</span></strong></td>
										</tr>
										<tr>
											<td>Total Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-qty">0</span></strong></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						-->

						<!-- IMPROVEMENT: Bagian setelah ini dirubah agar fokusnya ke info Packing dan Item, bukan lagi Pick List -->
						<!-- <div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Picking List
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> pl_id </th>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Picking Number</div> </th>
											<th> <div align="center">Picking date</div> </th>
											<th> Remark </th>
											<th> <div align="center">Start</div> </th>
											<th> <div align="center">Finish</div> </th>
											<th> <div align="center">Status</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<hr class="top30"> -->

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Packing List
							</span>
						</div>
						<div class="row">
							<div class="col-md-12">
								<table id="table-list-packing" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<!-- <th style="display: none;"> pl_id </th> -->
											<th style="width: 30px;"> <div align="center">No </div></th>
											<th style="width: 130px;"> <div align="center">Packing Number</div> </th>
											<th> <div align="center">Packing date</div> </th>
											<th> <div align="center">Remark</div> </th>
											<!-- <th> <div align="center">Start</div> </th>
											<th> <div align="center">Finish</div> </th> -->
											<th> <div align="center">Action</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<hr class="top30">

						<div class="detail-title" style="display:none">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Items
							</span>
						</div>

						<div class="row" style="display:none">
							<div class="col-md-12">
								<table id="table-list-item" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> pl_id_qty </th>
											<th style="display: none;"> shipping_id </th>
											<th style="display: none;"> id_barang </th>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Item Code</div> </th>
											<th> Item Name </th>
											<th> <div align="center">Outbound Doc.</div></th>
											<th style="width: 120px;"> <div align="center">Qty</div> </th>
											<th> <div align="center">LPN</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

					</div>
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">

								<div class="pull-right">
									<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
									 <?php if ($this->access_right->otoritas('edit')) : ?>

										<?php if ($not_usage) : ?>
											<a href="<?php echo base_url() . $page_class; ?>/edit/<?php echo $id; ?>" class="btn blue min-width120"><i class="fa fa-pencil"></i> EDIT</a>
										<?php endif; ?>

									 <?php endif; ?>

									 <?php if ($this->access_right->otoritas('print')) : ?>

										<?php if ($not_usage) : ?>
											<button type="button" class="btn default min-width120 print-manifest"><i class="fa fa-print"></i> PRINT MANIFEST</a>
										<?php endif; ?>

									 <?php endif; ?>
								</div>
							</div>
						</div>
					</div>

				</form>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

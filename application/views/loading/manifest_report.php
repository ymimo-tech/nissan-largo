<html>
<head>
	<title>Loading Manifest</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			/* padding: 10px; */
			border-bottom: 1px solid #eee;
		}

		.table-pdf tr td{
			font-size: 11px;
		}

		table{
			font-size:11px;
		}

		.width120{
			width: 120px;
		}
		.bottom10{
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
		.footer {
		   position: fixed;
		   left: 0;
		   bottom: 0;
		   width: 100%;
		   color: black;
		   text-align: left;
		   padding-bottom:70px;
		}
		.priority {
		   position: fixed;
		   top: 0;
		   right : 0;
		   width: 100%;
		   color: black;
		   text-align: right;
		}
		
		.footer {
		  position: fixed;
		  left: 0;
		  bottom: 0;
		  width: 100%;
		  text-align: center;
		}
		
		td {
		  padding-top: 2px !important;
		  padding-bottom: 2px !important;
		}

	</style>
</head>
<body style="font-family: Arial;">
	<h3>Loading Manifest | <?php echo $prio['m_priority_name']; ?> PRIORITY</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>Manifest Number</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $header['manifest_number']; ?>&size=25" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $header['manifest_number']; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Loading Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $header['create_at']; ?>
									</span>
								</td>
							</tr>
						</table>
						
						<!-- <table class="bottom10">
							<tr>
								<td class="width120">
									<label>Address</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['address']; ?>
									</span>
								</td>
							</tr>
						</table> -->
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Driver Name</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $header['driver']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Plate Number</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $header['license_plate']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th>No</th>
				<th style="width: 50%;"> Total Colly </th>
				<th>Customer</th>
				<th> Weight </th>
				<th> Volume </th>
			</tr>
		</thead>
		<tbody>
			<?php
				// dd($items);
				for($i = 0; $i< count($items); $i++){
					$str = '
						<tr>
							<td>'.($i+1).'</td>
							<td>'.$items[$i]['total_colly'].' Colly </td>
							<td>'.$items[$i]['destination_name'].'</td>
							<td>'.$items[$i]['weight'].'</td>
							<td>'.$items[$i]['volume'].'</td>
						</tr>
					';
					echo $str;
				}
			?>
		</tbody>
	</table>
	<!-- end table -->
	
	<div class="footer">
		<table class="table" style="border:none;">
			<thead >
				<tr>
					<th>NMDI</th>
					<th>Logistic</th>
					<th>Security</th>
				</tr>
			</thead>
			<tbody>
				<tr><td></td><td></td><td></td></tr>
				<tr><td></td><td></td><td></td></tr>
				<tr><td></td><td></td><td></td></tr>
				<tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr>
				<tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr>
				<tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr>
				<tr>
					<td>__________________________</td>
					<td>__________________________</td>
					<td>__________________________</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>

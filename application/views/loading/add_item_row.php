<tr class="added_row">
	<td>#</td>
	<td class="form-group">
		<select class="form-control dnumber add_rule" name="pl_id[]">
			<?php
				if(!isset($edit))
					echo '<option value="">-- Choose picking document --</option>';
			?>
			<?php
				foreach($doc as $row){
					echo '<option value="'.$row['pl_id'].'"
							data-date="'.$row['pl_date'].'" 
							data-name="'.$row['customer_name'].'">'.$row['pl_name'].'</option>"';
				}
			?>
		</select>
	</td>
	<td class="form-group">
		<span class="ddate"><div align="center">-</div></span>
	</td>
	<td class="form-group">
		<span class="dcust"><div align="center">-</div></span>
	</td>
	<td class="remove_row">
		<button class="btn btn-delete btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
	</td>
</tr>
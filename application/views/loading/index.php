<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>LOADING JOBS
				<small>List of loading jobs</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
				<a  class="btn btn-default btn-sm gi-download">
                        <i class="fa fa-plus"></i> Download GI</a>
					<?php if ($this->access_right->otoritas('add')) : ?>

						<?php echo hgenerator::render_button_group($button_group); ?>

					<?php endif; ?>
					<!--
                    <a class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-up"></i> Create Picking </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

										<div class="form-group loading-group">
											<label for="loading" class="col-md-3">Loading Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="loading" id="loading">
													</select>
												</div>
												<span class="help-block loading-msg"></span>
											</div>
										</div>

										<div class="form-group pl-group">
											<label for="pl" class="col-md-3">Picking Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="pl" id="pl">
													</select>
												</div>
												<span class="help-block pl-msg"></span>
											</div>
										</div>
										
										<div class="form-group pl-group">
											<label for="pl" class="col-md-3">GI STATUS</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control" name="gi-status" id="gi-status">
														<option value="">---GI Status--</option>
														<option value="1">Success</option>
														<option value="2">Failed</option>
														<!-- <option value="3">No Response</option> -->
														<option value="4">Null</option>
													</select>
												</div>
												<span class="help-block pl-msg"></span>
											</div>
										</div>

										<div class="form-group status-group">
											<label for="status" class="col-md-3">Doc. Status</label>
											<div class="col-md-8">

												<select id="status" class="form-control" data-placeholder="-- All --">
													<?php

															foreach($status as $k => $v){
																echo '<option value="'.$k.'">'.$v.'</option>';
															}

														?>
												</select>

												<span class="help-block status-msg"></span>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>
										<!--
										<div class="form-group doc-status-group">
											<label for="doc-status" class="col-md-3">Doc. Status</label>
											<div class="col-md-6">
												<div class="custom-select select-type4 high location">
													<select id="doc-status" class="form-control">
														<?php
															/*
															foreach($doc_status as $k => $v){
																echo '<option value="'.$k.'">'.$v.'</option>';
															}
															*/
														?>
													</select>
												</div>
												<span class="help-block doc-status-msg"></span>
											</div>
										</div>
										-->
									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
					<thead>
						<tr>
							<th style="display: none;"> id_shipping </th>
							<th style="width: 30px;"> No </th>
							<th style="vertical-align:top"> <div align="center">Loading Doc #</div> </th>
							<!-- <th style="vertical-align:top"> <div align="center">Outbound Doc #</div> </th> -->
							<th style="vertical-align:top"> <div align="center">Destination Name</div> </th>
							<th style="vertical-align:top"> <div align="center">Weight</div> </th>
							<th style="vertical-align:top"> <div align="center">Volume</div> </th>
							<th style="vertical-align:top"> <div align="center">Total Colly</div> </th>
							<th style="vertical-align:top"> <div align="center">Loading Date</div> </th>
							<th style="vertical-align:top"> <div align="center">Driver Name </th>
							<th style="vertical-align:top"> <div align="center">Transporter # </th>
							<th style="vertical-align:top"> <div align="center">Start</div> </th>
							<th style="vertical-align:top"> <div align="center">Finish</div> </th>
							<th style="vertical-align:top"> <div align="center">Status</div> </th>
							<th style="vertical-align:top"> <div align="center">Actions</div> </th>
						</tr>
					</thead>
					<tbody>

					</tbody>
                </table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="modal-do" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal-form" class="form-horizontal">
				<input type="hidden" name="id"/>

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Delivery Order</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
						   <div class="form-group do-group">
								<label class="col-md-4 control-labe">Delivery Order Number<span class="required">*</span></label>
								<div class="controls col-md-5">
									<input type="text" id="do-number" name="do_number" placeholder="Delivery Order Number" class="form-control" />
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-do" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-print"></i> &nbsp;PRINT</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>CREATE NEW INBOUND DOCUMENT
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i><?php echo $title; ?>
					</span>
				</div>
                <div class="actions">
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<div class="form-body">
					<form id="data-table-form" class="form-horizontal">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="params" value="" />
						<input type="hidden" name="not_used" value="<?php echo $not_usage; ?>" />

						<div class="row">
							<div class="col-md-12">
							   <div class="form-group">
									<label class="col-md-2">Inbound Doc. #<span class="required">*</span></label>
									<div class="controls col-md-6">
										<input type="hidden" name="kd_inbound" class="form-control" placeholder="No. Inbound Document" />
										<strong><span class="inbound-code"></span></strong>
									</div>
								</div>
							   <div class="form-group tanggal-inbound-group">
									<label class="col-md-2">Inbound Date<span class="required">*</span></label>
									<div class="col-md-6">
										<div class="input-group input-medium date date-picker">
											<input type="text" class="form-control" id="tanggal-inbound" name="tanggal_inbound" placeholder="dd/mm/yyyy"
												value="<?php echo $today; ?>" maxLength="10" />
											<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>
							   </div>
							   <div class="form-group">
									<label class="col-md-2">Doc Type<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select name="id_inbound_document" class="form-control"></select>
										<!-- <?php echo form_dropdown("id_inbound_document", $inbound_docs, "", "class='form-control'"); ?> -->
									</div>
								</div>

<!-- 								<?php

									if($setting['peminjaman'] == 'A'){

										$div = '<div class="form-group outbound-group" style="display: none;">
														<label class="col-md-2">Outbound Doc. #<span class="required">*</span></label>
														<div class="controls col-md-6">
															<select id="outbound-peminjaman" name="outbound_peminjaman" class="form-control"
																style="width: 100%;">
																<option value="">-- Choose Outbound Doc. # --</option>
																{{ option }}
															</select>
														</div>
												  </div>';
										$opt = '<option value="{{ id }}">{{ name }}</option>';
										$row = '';

										foreach($outbound_peminjaman as $r){
											$row .= $opt;
											$row = str_replace('{{ id }}', $r['id_outbound'], $row);
											$row = str_replace('{{ name }}', $r['kd_outbound'], $row);
										}

										$div = str_replace('{{ option }}', $row, $div);

										echo $div;
									}

								?>
 -->
							   <div class="form-group">
									<label class="col-md-2">Source<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select id="source" name="source" class="form-control">
										</select>
									</div>
								</div>

							   <div class="form-group">
									<label class="col-md-2">Remark<span class="required">*</span></label>
									<div class="controls col-md-6">
										<textarea name="remark" id="remark" rows="6" class="form-control" placeholder="Remark"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2">Job for Warehouse<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select id="warehouse-form" name="warehouse" class="form-control">
										</select>
									</div>
								</div>

								<?php if(!empty($id)) { ?>
									<!--
									<div class="form-group">
										<label class="col-md-2">Status<span class="required">*</span></label>
										<div class="controls col-md-6">
											<?php echo form_dropdown("id_inbound_status", $inbound_status, "", "class='form-control'"); ?>
										</div>
									</div>
									-->
								<?php } ?>
							</div>
						</div>

						<hr>
						<!--<h4>Items</h4>-->
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped table-bordered table-hover table-checkable order-column" id="inbound-barang">
									<thead>
										<tr>
											<th> No </th>
											<th> Item Name </th>
											<th style="text-align: center; width: 200px;"> Quantity </th>
											<!-- <th style="text-align: center; width: 200px;"> Cost Center </th> -->
											<th style="text-align: center"> Action </th>
										</tr>
									</thead>
									<tbody id="added_rows">
									</tbody>
									<tbody>
										<tr>
											<td colspan="5" class="add_item">
												<button type="button" class="btn blue min-width120 add-item"
													<?php if(empty($id)) { ?>data-toggle="tooltip" title="Click here to add item" data-placement="bottom"<?php } ?>><i class="fa fa-plus"></i> Add Item</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<span class="required pull-left">* is required</span>
									<div class="pull-right">

										<?php if(empty($id)) { ?>
											<a class="btn default min-width120" href="<?php echo base_url().$page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
										<?php } else { ?>
											<a class="btn default min-width120" href="javascript:;" onClick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
										<?php } ?>

										<?php if($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')){ ?>
											<button id="save" type="submit" class="btn blue min-width120"> SAVE</button>

											<?php if(empty($id)){ ?>
												<button id="save-new" type="button" class="btn green-haze min-width120"> SAVE & NEW</button>
											<?php } ?>

										<?php } ?>

									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

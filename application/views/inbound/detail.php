<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>INBOUND DETAIL
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i>
						<?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<div class="top-info">
						<label>Date : </label>
						<span class=""><?php echo $data['tanggal_inbound']; ?></span>
						<label>Doc. Type : </label>
						<span class=""><?php echo !empty($data['inbound_document_name']) ? $data['inbound_document_name'] : '-'; ?></span>
						<label>Created by : </label>
						<span class=""><?php echo !empty($data['user_name']) ? $data['user_name'] : '-'; ?></span>
					</div>
					<!--
					<table class="table-info">
						<tr>
							<td><strong>Date</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Doc. Type</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Created by</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
					</table>
					-->
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="params" value="" />

						<div class="row">
							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Inbound Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['kd_inbound']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['kd_inbound']; ?>
									</div>
								</div>
							</div>

							<div class="col-md-5">
								<div class="detail-caption text-left">
									<h1>Source<h1>
									<div class="bottom5 sup-title">
										<strong><?php echo $data['nama_supplier']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Code</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['kd_supplier']; ?></td>
										</tr>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['alamat_supplier']; ?></td>
										</tr>
										<tr>
											<td>Phone</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['telepon_supplier']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Contact Person</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['cp_supplier']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Inbound Status</h1>
									<div class="detail-status
										<?php
											if($data['nama_status'] == 'Open' || $data['nama_status'] == 'In Progress')
												echo 'status-blue';
											else
												echo 'status-green';
										?>">
										<?php echo $data['nama_status']; ?>
									</div>
								</div>
							</div>
						</div>

						<hr style="margin-top: 0;">
						<!--
						<div class="row">
							<div class="col-md-6">
								<h4>Items</h4>
							</div>
							<div class="col-md-6">
								<div class="pull-right">
									<table>
										<tr>
											<td>Total Item</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-item">0</span></strong></td>
										</tr>
										<tr>
											<td>Total Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-qty">0</span></strong></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						-->

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Ordered Items
							</span>
							<a  class="btn green btn-sm inbound-item-export min-width120 pull-right" id_inbound="<?php echo $id;?>">
								<i class="fa fa-file-excel-o"></i> EXPORT </a>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id</th>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Item Code</div> </th>
											<th> Item Name </th>
											<th style="width: 120px;"> <div align="center">Ordered</div> </th>
											<th style="width: 120px;"> <div align="center">Received</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<hr class="top30">
						<div class="row">
							<div class="col-md-6">
								<div class="detail-title">
									<span class="caption-subject font-green-sharp bold uppercase">
										<i class="fa fa-th-list"></i></i> &nbsp;Receiving
									</span>
								</div>
							</div>

							<?php // begin andri edit ?>
							<?php if ($this->access_right->otoritas('add')) : ?>
								<?php if ($data['id_status_inbound'] != 2) : ?>
									<div class="col-md-6">
										<div class="pull-right">
											<!-- <a href="<?php echo base_url(); ?>receiving/add/<?php echo $id; ?>" id="button-add" class="btn blue btn-sm min-width120"><i class="fa fa-file-o"></i> &nbsp;NEW RECEIVING</a> -->
										</div>
									</div>
								<?php endif; ?>
							<?php endif; ?>
							<?php // end andri edit ?>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list-r" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id_receiving </th>
											<th style="width: 30px;"> No </th>
											<th> <div align="center">Receiving Doc. #</div> </th>
											<th style="display: none;">Inbound Doc. #</th>
											<th> <div align="center">Source</div></th>
											<th> <div align="center">Transporter #</div> </th>
											<th> <div align="center">Receive Doc</div></th>
											<!--<th> <div align="center">Receive</div> </th>-->
											<th> <div align="center">Tally</div> </th>
											<th> <div align="center">Putaway</div> </th>
											<th> <div align="center">Status</div> </th>
											<th> <div align="center">Action</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">

								<div class="pull-right">
									<!--<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>-->
									<a class="btn default min-width120" href="javascript:;" onclick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
									<?php if ($this->access_right->otoritas('edit')) : ?>

										<?php if ($not_usage) : ?>
											<a href="<?php echo base_url() . $page_class; ?>/edit/<?php echo $id; ?>" class="btn blue min-width120"><i class="fa fa-pencil"></i> EDIT</a>
										<?php endif; ?>

									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

				</form>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row">
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>INBOUND DOCUMENTS
			<small>List of documents for incoming items/materials</small>
			</h1>
			<br/>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>

						<?php echo hgenerator::render_button_group($button_group); ?>

					<?php endif; ?>
					<!-- <a  class="btn green-haze btn-sm btn-sync min-width120"><i class="fa fa-refresh"></i> SYNC </a> -->
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm btn-receiving">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show Filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

										<div class="form-group po-group">
											<label for="po" class="col-md-3">Inbound Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="po" id="po">
													</select>
												</div>
												<span class="help-block po-msg"></span>
											</div>
										</div>

										<div class="form-group supplier-group">
											<label for="supplier" class="col-md-3">Source</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="supplier" class="form-control">
													<?php
														foreach($suppliers as $k => $v){
															echo '<option value="'.$k.'">'.$v.'</option>';
														}
													?>
													</select>
												</div>
												<span class="help-block supplier-msg"></span>
											</div>
										</div>

										<div class="form-group doc-type-group">
											<label for="doc-type" class="col-md-3">Doc. Type</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="doc-type" class="form-control">
													<?php
														foreach($inbound_docs as $k => $v){
															echo '<option value="'.$k.'">'.$v.'</option>';
														}
													?>
													</select>
												</div>
												<span class="help-block doc-type-msg"></span>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>

										<div class="form-group status-group">
											<label for="status" class="col-md-3">Doc. Status</label>
											<div class="col-md-8">

												<div class="custom-select select-type4 high location">
													<select class="form-control" name="doc-status" id="doc-status" style="width: 100%;" multiple data-placeholder="-- All --">

														<?php
															foreach($inbound_status as $k => $v){
																echo '<option value="'.$k.'">'.$v.'</option>';
															}
														?>

													</select>
												</div>

												<span class="help-block status-msg"></span>
											</div>
										</div>

									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
							<th style="display: none;"> inbound_id </th>
							<!--
                            <th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>-->
                            <th style="vertical-align:top;text-align:center"> No </th>
                            <th style="vertical-align:top;text-align:center"> Inbound Doc. # </th>
                            <th style="vertical-align:top;text-align:center;width: 70px;"> Date </th>
                            <th style="vertical-align:top;text-align:center"> Doc. Type </th>
                            <th style="vertical-align:top;text-align:center"> Source </th>
														<th style="vertical-align:top;text-align:center"> Total Items </th>
														<th style="vertical-align:top;text-align:center"> Remarks </th>
                            <th style="vertical-align:top;text-align:center"> Status </th>
                            <?php
                                if($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
                                    echo "<th style='vertical-align:top;text-align:center'> Actions </th>";
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody> </tbody>
					<!--
                    <tfoot>
						<tr>
							<th colspan="6" style="vertical-align: middle;"><strong>TOTAL</strong></th>
							<th style="text-align: right;"></th>
							<th style="text-align: right;"></th>
							<th colspan="2"></th>
						</tr>
					</tfoot>
					-->
                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form id="data-table-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitled'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Inbound Code<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('kd_inbound','', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Date<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('tanggal_inbound','', 'class="col-md-12 form-control mdatepicker"') ?>

                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Supplier<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_dropdown("id_supplier", $suppliers, "", "class='selectpicker form-control' data-live-search='true'"); ?>
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Doc Type<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_dropdown("id_inbound_document", $inbound_docs, "", "class='selectpicker form-control' data-live-search='true'"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="inbound-barang">
                                            <thead>
                                                <tr>
                                                    <th> No </th>
                                                    <th> Item Name </th>
                                                    <th> Quantity </th>
                                                    <th> Action </th>
                                                </tr>
                                            </thead>
                                            <tbody id="added_rows">
                                            </tbody>
                                            <tbody>
                                                <tr><td colspan="4" class="add_item">Add Item</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* required</span>
                                    <button type="submit" class="btn blue"><i class="icon-save"></i> Save</button>
                                    <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Close</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

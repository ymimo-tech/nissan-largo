<tr class="added_row">
	<td><?php echo $seq+1; ?></td>
	<td class="form-group">
		<?php echo form_dropdown("id_barang[]", $items, isset($inbound_barang) ? $inbound_barang['id_barang'] : "", "class='form-control add_rule'"); ?>
	</td>
	<td class="form-group">
		<div class="pull-right">
			<?php echo form_input("item_quantity[]", isset($inbound_barang) ? $inbound_barang['jumlah_barang'] : "", 'class="col-md-12 form-control add_rule qty-inbound" style="width: 100px;"') ?>
			<?php
				$poItem = isset($inbound_barang['po_item']) ? $inbound_barang['po_item'] : "";
			?>
			<input type="hidden" name="po_item[]" value="<?php echo $poItem; ?>" class="col-md-12 form-control po-item-inbound" style="width: 100px;">
			&nbsp;&nbsp;
			<label class="label-control uom" style="margin-top: 7px;">
				<?php
					if(isset($inbound_barang['nama_satuan']))
						echo $inbound_barang['nama_satuan'];
					else
						echo 'UoM';
				?>
			</label>
		</div>
	</td>
<!-- 	<td>
		<?php
			if(!isset($enable_cost_center)){
				echo form_dropdown("cost_center[]", $cost_center, isset($inbound_barang['cost_center']) ? $inbound_barang['cost_center'] : "", "class='form-control cost_center'");
			}else{
				if($enable_cost_center){
					echo form_dropdown("cost_center[]", $cost_center, isset($inbound_barang['cost_center']) ? $inbound_barang['cost_center'] : "", "class='form-control' cost_center");
				}else{
					echo " - ";
				}
			}
		?>
	</td>
 -->	<td class="remove_row">
				<div style="text-align:center"><button class="btn btn-delete btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button></div>
			</td>
</tr>

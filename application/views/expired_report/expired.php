<html>
<head>
	<title>Expired Report</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, .table-pdf tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
		.table-pdf tbody tr td.header-report-table{
			background: #fafafa;
			padding: 10px;
			font-weight: bold;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Expired Report</h3>
	<hr>
	<br /><br />
	<!-- begin table -->
	<table id="table-pdf" class="table report-table table-pdf">
		<thead>
			<tr class="uppercase">
				<th style="text-align: center;"> No </th>
				<th style="text-align: center;"> SKU </th>
				<th style="text-align: center;"> Product Name </th>
				<th style="text-align: center;"> Type </th>
				<th style="text-align: center;"> Location </th>
				<th style="text-align: center;"> Quantity </th>
				<th style="text-align: center;"> Uom </th>
				<th style="text-align: center;"> Expired Date </th>
			</tr>
		</thead>
		<tbody>
			
			<?php
				
				$data = $data;
				$len = count($data);

				$tpl = '<tr>
							<td style="text-align: center;">{{ no }}</td>
							<td style="text-align: left;">{{ sku }}</td>
							<td style="text-align: left;">{{ nama_barang }}</td>
							<td style="text-align: left;">{{ type }}</td>
							<td style="text-align: left;">{{ location }}</td>
							<td style="text-align: right;">{{ qty }}</td>
							<td style="text-align: right;">{{ uom }}</td>
							<td style="text-align: right;">{{ exp }}</td>
					   </tr>';
				$row = ''; 
				
				if($len > 0){
					
					for($i = 0; $i < $len; $i++){
						$exp_date = DateTime::createFromFormat('Y-m-d', $data[$i]['tgl_exp']);
						$row .= $tpl;
						$row = str_replace('{{ no }}', ($i + 1), $row);
						$row = str_replace('{{ sku }}', $data[$i]['sku'], $row);
						$row = str_replace('{{ nama_barang }}', $data[$i]['nama_barang'], $row);
						$row = str_replace('{{ type }}', 'TYPE', $row);
						$row = str_replace('{{ location }}', $data[$i]['loc_name'], $row);
						$row = str_replace('{{ qty }}', $data[$i]['stock'], $row);
						$row = str_replace('{{ uom }}', $data[$i]['nama_satuan'], $row);
						$row = str_replace('{{ exp }}', $exp_date->format('d M Y'), $row);
					}
					
					echo $row;
					
				}else{
					
					echo '<tr><td colspan="4" style="text-align: center;">No data</td></tr>';
					
				}
			?>
			
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>
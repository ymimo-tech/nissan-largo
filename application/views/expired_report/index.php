<style>
	.qty h1{
		font-size: 46px !important;
		font-weight: bold;
		color: #fff !important;
	}

	.qty h1 a{
		color: #fff !important;
	}

	.qty h1 a:hover{
		text-decoration: none;
	}

	.qty h1 a:active{
		text-decoration: none;
	}

	.qty h1 a:focus{
		text-decoration: none;
	}

	.qty span{
		font-weight: bold;
		color: #fff;
	}

	.widget-thumb{
		padding: 10px;
	}
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>EXPIRATION REPORT
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div id="portlet-light" class="portlet light">
            <div class="portlet-title">
                <div class="caption col-md-12">
                    <span class="caption-subject font-green-sharp bold uppercase">
											<i class="fa fa-th-list"></i> Expiration Periods (Within Days)
					</span>
				</div>

				<div class="row qty">
					<!--<div class="col-md-2 total" style="text-align: center;">
						<div class="widget-thumb bg-grey-salt text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat total-inbound">0</h1>
								</div>
							</div>
						</div>
					</div>-->
					<div class="col-md-3 30d" style="text-align: center;">

						<div class="widget-thumb bg-blue text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
						<!--<span></span>
						<h1></h1>-->
					</div>
					<div class="col-md-3 60d" style="text-align: center;">
						<div class="widget-thumb bg-green text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 120d" style="text-align: center;">
						<div class="widget-thumb bg-red-sunglo text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 expired" style="text-align: center;">
						<div class="widget-thumb bg-red-intense text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>

					<!--
					<table id="table-qty" class="table table-striped table-bordered table-hover table-checkable order-column">
						<thead>
							<tr>
								<th style="width: 30px;"> No </th>
								<th style="text-align: center;"> Receiving Qty </th>
								<th style="text-align: center;"> On Hand Qty </th>
								<th style="text-align: center;"> Allocated Qty </th>
								<th style="text-align: center;"> Suspend Qty </th>
								<th style="text-align: center;"> Available Qty </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
					-->

				</div>
				<div>
					&nbsp;
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('print')) : ?>

						<a href="javascript:;" id="button-export-excel" class="btn blue btn-sm btn-export-excel min-width120" 0=""><i class="fa fa-file-excel-o"></i> &nbsp;EXPORT XLS</a>
						<a href="javascript:;" id="button-export" class="btn green btn-sm btn-export min-width120" 0=""><i class="fa fa-file-pdf-o"></i> &nbsp;EXPORT PDF</a>

					<?php endif; ?>
					<!--
                    <a class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
					-->
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">

								<div class="row">
									<div class="col-md-6">

										<div class="form-group item-group">
											<label for="item" class="col-md-3">Item</label>
											<div class="col-md-8">
												<select class="form-control"
													name="item" id="item" style="width: 100%;" multiple
													data-placeholder="-- All --">

													<?php
														foreach($item as $r){
															echo '<option value="'.$r['id_barang'].'">'.$r['nama_barang'].'</option>';
														}
													?>

												</select>
												<span class="help-block item-msg"></span>
											</div>
										</div>
										<div class="form-group location-group">

										<label for="location" class="col-md-3">Location</label>
										<div class="col-md-8">
											<div class="custom-select select-type4 high location">
												<select class="form-control"
													name="location" id="location" style="width: 100%;">

													<?php

														echo '<option value="">-- All --</option>';
														foreach($location as $row){
															echo '<option value="'.$row['loc_id'].'">'.$row['loc_name'].'</option>';
														}
													?>

												</select>
											</div>
											<span class="help-block location-msg"></span>
										</div>
										</div>

									</div>
								</div>

							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table id="table-list" class="table table-striped table-bordered table-hover report-table dataTable no-footer">
					<thead>
						<tr>
							<th style="text-align: center;"> No </th>
							<th style="text-align: center;"> Item Code </th>
							<th style="text-align: center;"> Item Name </th>
							<!-- <th style="text-align: center;"> Type </th> -->
							<!-- <th style="text-align: center;"> Location </th> -->
							<th style="text-align: center;"> Quantity </th>
							<th style="text-align: center;"> UoM </th>
							<th style="text-align: center;"> Expired Date </th>
							<th style="text-align: center;"> Action </th>
						</tr>
					</thead>
					<tbody>
					</tbody>
                </table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

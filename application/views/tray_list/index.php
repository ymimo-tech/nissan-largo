<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Delivery Note
				<small>Print Delivery Note references on Picking List and Tray Location</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>


<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>

						<?php echo hgenerator::render_button_group($button_group); ?>

					<?php endif; ?>
					<!-- <a  href="<?php echo base_url() . 'tray_list/add'?>" id="button-add" class="btn blue btn-sm min-width120" data-source="<?php echo base_url() . 'picking_list/add'?>">
						<i class="fa fa-file-o"></i> &nbsp;Create DN </a> -->
					
						<a href="javascript:;" class="btn green btn-sm btn-print min-width120">
							<i class="fa fa-print"></i> PRINT </a>

						<a id='export-dn' class="btn blue btn-sm min-width120" target="_blank">
							<i class="fa fa-file-excel-o"></i> Export DN </a>
					<!--
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-up"></i> Create Picking </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
			<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-filter" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

									<div class="form-group pl-group">
											<label for="pl" class="col-md-4">Picking Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="pl" id="pl">
													</select>
												</div>
												<span class="help-block pl-msg"></span>
											</div>
										</div>
										
										<div class="form-group pl-group">
											<label for="pl" class="col-md-4">Delivery Note</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="dn_search" id="dn_search">
													</select>
												</div>
												<span class="help-block pl-msg"></span>
											</div>
										</div>

										

										<div class="form-group status-group">
											<label for="status" class="col-md-4">Doc. Status</label>
											<div class="col-md-8">

												<select id="status" class="form-control" style="width: 100%;" data-placeholder="-- All --">
													<option value="" selected>-- All --</option>
													<option value="0">OPEN</option>
													<option value="1">COMPLETE</option>
												</select>

												<span class="help-block status-msg"></span>
											</div>
										</div>


									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>

									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="filter" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Create
						</div>
						<!-- <div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div> -->
					</div>
					<div class="portlet-body">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">
										<div class="form-group do-group">
											<label for="do" class="col-md-4">Tray Location</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="tl" id="tl" required>
													</select>
												</div>
												<span class="help-block do-msg"></span>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="pull-right">
												<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
												<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-file-o"></i> Create DN</button>
										</div>
									</div>

								</div>
							</div>
							<!-- <div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-file-o"></i> Print</button>
										</div>
									</div>
								</div>
							</div> -->
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

				<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
					<thead>
						<tr>
							<th style="display: none;"> id_picking </th>
							<th style="text-align:center" class="table-checkbox" width="10px">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
							<th style="vertical-align: top;width: 30px;"> No </th>
							<th style="vertical-align: top;"> <div align="center">Delivery Note #</div> </th>
							<th style="vertical-align: top;"> <div align="center">Picking List</div> </th>
							<th style="vertical-align: top;"> <div align="center">TRAY</div> </th>
							<th style="vertical-align: top;"> <div align="center">Date</div> </th>
							<th style="vertical-align: top;"> <div align="center">User</div> </th>
							<th style="vertical-align: top;"> <div align="center">Status</div> </th>
							<th style="vertical-align: top;"> <div align="center">Actions</div> </th>
						</tr>
					</thead>
					<tbody>

					</tbody>
                </table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?php if ($this->access_right->otoritas('print')) { ?>
	<?php include 'print_modal.php'; ?>
<?php }?>


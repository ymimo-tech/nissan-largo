<html>
<head>
	<title>Picking List</title>
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			/* padding: 10px; */
			border-bottom: 1px solid #eee;
		}

		.table-pdf tr td{
			font-size: 11px;
		}

		table{
			font-size:11px;
		}

		.width120{
			width: 120px;
		}
		.bottom10{
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
		.footer {
		   position: fixed;
		   left: 0;
		   bottom: 0;
		   width: 100%;
		   color: black;
		   text-align: left;
		}
		.priority {
		   position: fixed;
		   top: 0;
		   right : 0;
		   width: 100%;
		   color: black;
		   text-align: right;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<!-- <div class="priority"><h2><?php echo $pick['priority']; ?> PRIORITY</h2></div> -->
	<h3>Delivery Note | <?php echo $tray[0]['m_priority_name']." PRIORITY" ?></h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>Delivery Note</label>
						<br><br>
						<img src="https://chart.googleapis.com/chart?chs=125x125&cht=qr&chl=<?php print_r($pick[0]['name']); ?>&choe=UTF-8" alt="" />
						<!-- <img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $pick[0]['name']; ?>&size=25" style="border:1px solid;"> -->
						<!-- <img alt="" src="https://api.qrserver.com/v1/create-qr-code/?data=<?php print_r($pick[0]['name']); ?>&amp;size=50x50" width='65px' Height='65px'> -->
						<!-- <img alt="" src="https://api.qrserver.com/v1/create-qr-code/?size=50x50&data=<?php echo $pick[0]['name']; ?>"> -->

						<br>
						<span class="tally-rcv-code"><?php print_r($pick[0]['name']); ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Tray</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php print_r($pick[0]['location']); ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php print_r($pick[0]['date']); ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Printed By</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php print_r($pick[0]['user']); ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Dealer</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>

										<?php
												echo $tray[0]['destination_name'].' - '.$tray[0]['destination_address']
										?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<!--th style="width: 50%;text-align: left;"> Location </th>
				<th style="width: 10%;text-align: left;"> QTY </th>
				<th style="width: 30%;text-align: left;"> Item Name/SKU </th-->
				<th style="width:5%;text-align: left;">NO</th>
				<th style="width:12.5%;text-align: left;">Item Code<br/>OEM</th>
				<th style="width:35%;text-align: left;">Item Name</th>
				<th style="width:5%;text-align: left;">Qty<br/>UOM</th>
				<th style="width:5%;text-align: left;">PL</th>
				<th style="width:5%;text-align: left;">Location Name</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i = 1;
				$no = 1;
				for($j = 0; $j < count($tray); $j++){
					$tab = '<tr>';
					$tab .= '<td>'.$no.'</td>';
					$tab .= '<td>'.$tray[$j]['item_code'].'</td>';
					$tab .= '<td>'.$tray[$j]['item_name'].'</td>';
					$tab .= '<td>'.$tray[$j]['qty'].'</td>';
					$tab .= '<td>'.$tray[$j]['pl_name'].'</td>';
					// $tab .= '<td>'.$tray[$j]['location_name'].'</td>';
					$tab .= '<td>'.$pick[0]['location'].'</td>';
					$tab .= '</tr>';
                    $no++;
					echo $tab;
				}
			?>
		</tbody>
	</table>
	<!-- end table -->
	<br>
	<p style="font-size:11px;">Note : <?php echo $tray[0]['note']; ?></p>
	<?php
		// if(strpos($pick['note'],'||')) {
		// 	$text = $pick['note'];
		// 	$explode = explode('||',$text);
		// 	if(count($explode) > 1) {
		// 		echo $explode[0] . '<br/>' . $explode[1];
		// 	}
		// } else {
		// 	echo 'NOTE : ' . $pick['note'];
		// 	echo '<br/><br/>';
		// }
		// echo 'EKSPEDISI : ' . $pick['ekspedisi'];
	?>
	<div class="footer">
		<p>KARUNG .......... @ + DUS .......... @ + KANTONG .......... @ = .......... COLLY</p>
	</div>
</body>
</html>

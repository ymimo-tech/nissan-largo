<html>
<head>
	<title>Cycle Count Report</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, .table-pdf tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
		.table-pdf tbody tr td.header-report-table{
			background: #fafafa;
			padding: 10px;
			font-weight: bold;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Cycle Count Report</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 400px; vertical-align: top;">
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>CC. Doc. #</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['cc_code']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
					<td style="vertical-align:top;">

						<table class="bottom10">
							<tr>
								<td valign="top" class="width120">
									<label>From</label>
								</td>
								<td valign="top" class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['from']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>To</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['to']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<br /><br />
	<!-- begin table -->
	<table id="table-pdf" class="table report-table table-pdf">
		<thead>
			<tr>
				<th style="width: 30px;"><div align="center">No</div></th>
				<th><div align="center">CC. Doc. #</div></th>
				<th><div align="center">CC. Date</div></th>
				<th><div align="center">CC. Type</div></th>
				<th><div align="center">Location</div></th>
				<th><div align="center">Item Name</div></th>
				<th><div align="center">Serial Number</div></th>
				<th><div align="center">Status</div></th>
				<th><div align="center">System Qty</div></th>
				<th><div align="center">Scanned Qty</div></th>
			</tr>
		</thead>
		<tbody>

			<?php

				$data = $data;
				$len = count($data);

				$tpl = '<tr>
							<td style="text-align: center;">{{ no }}</td>
							<td style="text-align: center;">{{ code }}</td>
							<td style="text-align: center;">{{ date }}</td>
							<td style="text-align: center;">{{ type }}</td>
							<td style="text-align: center;">{{ loc }}</td>
							<td>{{ item }}</td>
							<td style="text-align: center;">{{ sn }}</td>
							<td style="text-align: center;">{{ status }}</td>
							<td style="text-align: center;">{{ system_qty }}</td>
							<td style="text-align: center;">{{ scanned_qty }}</td>
					   </tr>';
				$row = '';

				if($len > 0){

					for($i = 0; $i < $len; $i++){
						$row .= $tpl;
						$row = str_replace('{{ no }}', ($i + 1), $row);
						$row = str_replace('{{ code }}', $data[$i]['cc_code'], $row);
						$row = str_replace('{{ date }}', $data[$i]['cc_time'], $row);
						$row = str_replace('{{ type }}', $data[$i]['cc_type'], $row);
						$row = str_replace('{{ loc }}', $data[$i]['loc_name'], $row);
						$row = str_replace('{{ item }}', $data[$i]['nama_barang'], $row);
						$row = str_replace('{{ sn }}', $data[$i]['kd_unik'], $row);
						$row = str_replace('{{ status }}', $data[$i]['status'], $row);
						$row = str_replace('{{ system_qty }}', $data[$i]['system_qty'], $row);
						$row = str_replace('{{ scanned_qty }}', $data[$i]['scanned_qty'], $row);
					}

					echo $row;

				}else{

					echo '<tr><td colspan="10" style="text-align: center;">No data</td></tr>';

				}
			?>

		</tbody>
	</table>
	<!-- end table -->
</body>
</html>

<html>
<head>
	<title>Material Out</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, .table-pdf tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
		.table-pdf tbody tr td.header-report-table{
			background: #fafafa;
			padding: 10px;
			font-weight: bold;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Material Out Report</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 400px; vertical-align: top;">
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Group By</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo ucfirst(strtolower($params['group_by'])); ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Destination Type</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo ucfirst($params['destination_type']); ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Destination Name</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo ucfirst($params['destination_name']); ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
					<td style="vertical-align:top;">
						
						<table class="bottom10">
							<tr>
								<td valign="top" class="width120">
									<label>From</label>
								</td>
								<td valign="top" class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['from']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>To</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['to']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<br /><br />
	<!-- begin table -->
	<table id="table-pdf" class="table report-table table-pdf">
		<thead>
			<tr>
				<th style="width: 30px;">No</th>
				<th>Group By <span class="group-by-title">
					<?php 
						if(strtolower($params['group_by']) == 'date')
							echo 'Periode';
						else
							echo 'Source';
					?>
				</span></th>
				<th>Outbound Doc.</th>
				<th>Destination</th>
				<th>PL. Doc</th>
				<th>Items</th>
				<th>Packing Number</th>
				<th>Doc. Qty</th>
				<th>Load. Qty</th>
				<th>Remark</th>
				<th>User</th>
			</tr>
		</thead>
		<tbody>
			
			<?php
				
				$data = $data['data'];
				
				$len = count($data);
				$groupTpl = '<tr>
								<td class="header-report-table"></td>
								<td class="header-report-table" colspan="10"><strong>{{ group_header }}</strong></td>
							</tr>';
				$tpl = '<tr>
							<td></td>
							<td></td>
							<td>{{ inbound_doc }}</td>
							<td>{{ destination }}</td>
							<td>{{ pl_doc }}</td>
							<td>{{ items }}</td>
							<td>{{ packing_number }}</td>
							<td style="text-align: right;">{{ doc_qty }}</td>
							<td style="text-align: right;">{{ load_qty }}</td>
							<td>{{ remark }}</td>
							<td>{{ user }}</td>
					   </tr>';
				$row = ''; 
				
				if($len > 0){
					
					if(strtolower($params['group_by']) == 'date'){
					
						foreach($data as $k => $v){
							
							$header = '';
							$detail = '';
							
							$dt = key($data[$k]);
							
							$header .= $groupTpl;
							$header = str_replace('{{ group_header }}', $dt, $header);
							
							$detailData = $v[$dt];
							$iLen = count($detailData);
								
							for($j = 0; $j < $iLen; $j++){
								$detail .= $tpl;
								$detail = str_replace('{{ inbound_doc }}', $detailData[$j]['kd_outbound'], $detail);
								$detail = str_replace('{{ destination }}', $detailData[$j]['destination'], $detail);
								$detail = str_replace('{{ pl_doc }}', $detailData[$j]['pl_name'], $detail);
								$detail = str_replace('{{ items }}', $detailData[$j]['item'], $detail);
								$detail = str_replace('{{ packing_number }}', $detailData[$j]['packing_number'], $detail);
								$detail = str_replace('{{ doc_qty }}', $detailData[$j]['doc_qty'] . ' ' . $detailData[$j]['nama_satuan'], $detail);
								$detail = str_replace('{{ load_qty }}', $detailData[$j]['load_qty'] . ' ' . $detailData[$j]['nama_satuan'], $detail);
								$detail = str_replace('{{ remark }}', $detailData[$j]['remark'], $detail);
								$detail = str_replace('{{ user }}', $detailData[$j]['nama'], $detail);
							}
							
							$row .= $header;
							$row .= $detail;
						}
						
						echo $row;
						
					}else{
						
						foreach($data as $k => $v){
								
							$header = '';
							$detail = '';
							
							$dt = key($data[$k]);
							
							$header .= $groupTpl;
							$header = str_replace('{{ group_header }}', $dt, $header);
							
							$detailData = $v[$dt];
							$iLen = count($detailData);
								
							for($j = 0; $j < $iLen; $j++){
								$detail .= $tpl;
								$detail = str_replace('{{ inbound_doc }}', $detailData[$j]['kd_outbound'], $detail);
								$detail = str_replace('{{ destination }}', $detailData[$j]['destination'], $detail);
								$detail = str_replace('{{ pl_doc }}', $detailData[$j]['pl_code'], $detail);
								$detail = str_replace('{{ items }}', $detailData[$j]['item'], $detail);
								$detail = str_replace('{{ packing_number }}', $detailData[$j]['packing_number'], $detail);
								$detail = str_replace('{{ doc_qty }}', $detailData[$j]['doc_qty'] . ' ' . $detailData[$j]['nama_satuan'], $detail);
								$detail = str_replace('{{ load_qty }}', $detailData[$j]['rec_qty'] . ' ' . $detailData[$j]['nama_satuan'], $detail);
								$detail = str_replace('{{ remark }}', $detailData[$j]['remark'], $detail);
								$detail = str_replace('{{ user }}', $detailData[$j]['nama'], $detail);
							}
							
							$row .= $header;
							$row .= $detail;
							
						}
						
						echo $row;
					}	
					
				}else{
					
					echo '<tr><td colspan="10"><div align="center">No data</div></td></tr>';
					
				}
			
			?>
			
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>
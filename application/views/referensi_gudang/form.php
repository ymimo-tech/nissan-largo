<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>

        <div class="control-group">
            <label for="select" class="control-label">Kode Kategori <span class="required">*</span></label>
            <div class='controls'>
                <?php echo form_input('kd_kategori', !empty($default->kd_kategori) ? $default->kd_kategori : '', 'class="span4"') ?>
            </div>
        </div>

       


        <div class="control-group">
            <label for="select" class="control-label">Nama Kategori <span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('nama_kategori', !empty($default->nama_kategori) ? $default->nama_kategori : '', 'class="span4"') ?>
                
            </div>
        </div>

        
        <span class="required">* harus diisi</span>
        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.chosen').chosen();
        $('.numeric').numeric();
    });
</script>

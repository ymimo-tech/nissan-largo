<html>
<head>
	<title>Surat Jalan</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			background:#000;
			color:#fff;
			font-weight:normal;
		}
		.table-pdf tr td{
			background:#fff;
			color:#000;
			border:1px solid #000;
		}
		.no-border tr td{
			border-bottom:0;
			background:transparent;
			color:#000;
			border:0;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 22px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<?php
		$packing=array();
		$i=1;
		foreach($data->result() as $dd){
			$packing[$dd->inc]['kd_outbound']=$dd->kd_outbound;
			$packing[$dd->inc]['DestinationName']=$dd->DestinationName;
			$packing[$dd->inc]['DestinationAddressL1']=$dd->DestinationAddressL1;
			$packing[$dd->inc]['DestinationAddressL2']=$dd->DestinationAddressL2;
			$packing[$dd->inc]['DestinationAddressL3']=$dd->DestinationAddressL3;
			$packing[$dd->inc]['DestinationAddress']=$dd->DestinationAddress;
			$packing[$dd->inc]['packing_number']=$dd->packing_number;
			$packing[$dd->inc]['barang'][$dd->kd_barang]['nama_barang']=$dd->nama_barang;
			if(isset($packing[$dd->inc]['barang'][$dd->kd_barang]['qty'])){
				$packing[$dd->inc]['barang'][$dd->kd_barang]['qty']+=$dd->qty;
			}else{
				$packing[$dd->inc]['barang'][$dd->kd_barang]['qty']=$dd->qty;
			}
		}
		$maks=count($packing);
		// echo '<pre>';
		// print_r($packing);
	?>
	<?php foreach($packing as $key=>$pack){?>
	<table class="table-pdf no-border">
		<tr>
		<td>
			<img src="<?php echo base_url('assets/global/img/logo.jpeg');?>" style="width:200px;"/>
		</td>
		<td style="text-align:right">
			<h4>Perintah Keluar Barang <?php echo date('Y-m-d H:i:s');?></h4>
		</td>
		</tr>
	</table>
	<table style="width:100%;margin-top:20px;">
		<tr>
			<td style="width: 240px; text-align: left; vertical-align: top;padding-left:20px;padding-bottom:20px;">
				ORDER ID #<?php echo $pack['kd_outbound'];?><br/>
				Sender : www.salubritas.id
			</td>
			<td style="padding-left:65px;vertical-align:top;" rowspan="2">
				<b>DIKIRIM KEPADA</b><br/>
				<?php echo $pack['DestinationName'];?><br/>
				<?php echo $pack['DestinationAddress'];?><br/>
<!-- 				<?php echo $pack['DestinationAddressL1'];?><br/>
				<?php echo $pack['DestinationAddressL2'];?><br/>
				<?php echo $pack['DestinationAddressL3'];?><br/>
 -->
			</td>
		</tr>
		<tr>
			<td style="width: 240px; text-align: center; vertical-align: top;">
				<label>Packing Note</label><br>
				<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $pack['packing_number'].'-'.$key.'/'.$maks;?>&size=45" style="">
				<br>
				<span class="tally-rcv-code"><?php 
				if($key<10){
					$pad = '0';
				}else{
					$pad = '';
				}
				if($maks<10){
					$pad2 = '0';
				}else{
					$pad2 = '';
				}
				echo $pack['packing_number'].$pad.$key.$pad2.$maks;?></span>
			</td>
		</tr>
	</table>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> SKU ID</th>
				<th> Nama SKU </th>
				<th> Variant </th>
				<th> Qty </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($pack['barang'] as $kd_barang=>$barang){
				echo '<tr><td>'.$kd_barang.'</td><td>'.$barang['nama_barang'].'</td><td>-</td><td>'.$barang['qty'].'</td></tr>';
			}
			?>
		</tbody>
	</table>
	<?php if($i!=$maks){?>
	<pagebreak />
	<?php }?>
	<?php $i++;}?>
	<!-- end table -->
</body>
</html>

<style type="text/css">
    .select2-container--open{
        z-index:99999;
    }
</style>
<div id="modal-print" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal-form" class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit Weight and Volume</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">

						   <div class="form-group qty-print-group">
								<label class="col-md-3 control-label">Total Item<span class="required">*</span></label>
								<div class="controls col-md-6">
									<input type="hidden" id="type-print" name="type_print"/>
									<input type="text" id="qty-print" name="qty_print" placeholder="Qty Print" class="form-control" />
									<span class="qty-print-receiving" style="display: none; position: relative; top: 8px;">0</span>
								</div>
							</div> 

						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-print" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-print"></i> &nbsp;Save</button>
					</div>
				</div>
			
			</form>
		</div>
	</div>
</div> 
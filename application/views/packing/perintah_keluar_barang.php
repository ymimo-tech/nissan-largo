<html>
<head>
	<title>Perintah Keluar Barang</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<style type="text/css">

		@page *{
		    margin-top: 1000cm;
		}

		.tblItem{
			width: 100%;
		}

		.tblItem tr th,
		.tblItem tr td
		{
			border:1px solid;
		}

	</style>
</head>
<body style="font-family: Arial;">
	<?php
		$packing=array();
		$i=1;
		foreach($data->result() as $dd){
			$packing[$dd->inc]['kd_outbound']=$dd->kd_outbound;
			$packing[$dd->inc]['DestinationName']=$dd->DestinationName;
			$packing[$dd->inc]['DestinationAddressL1']=$dd->DestinationAddressL1;
			$packing[$dd->inc]['DestinationAddressL2']=$dd->DestinationAddressL2;
			$packing[$dd->inc]['DestinationAddressL3']=$dd->DestinationAddressL3;
			$packing[$dd->inc]['packing_number']=$dd->packing_number;
			$packing[$dd->inc]['barang'][$dd->kd_barang]['nama_barang']=$dd->nama_barang;
			$packing[$dd->inc]['barang'][$dd->kd_barang]['qty']=$dd->qty;
			// if(isset($packing[$dd->inc]['barang'][$dd->kd_barang]['qty'])){
			// 	$packing[$dd->inc]['barang'][$dd->kd_barang]['qty']+=1;
			// }else{
			// 	$packing[$dd->inc]['barang'][$dd->kd_barang]['qty']=1;
			// }
		}
		$maks=count($packing);
	?>


<div style="border:1px solid; padding-bottom: 100px;">
	<div style="width: 80%; margin:0 auto;">

		<h4 style="text-align: center;margin-bottom: 0;">
			<center>
				<strong> PASS KELUAR BARANG </strong>
			</center>
		</h4>
		<hr style="margin-top: 10px;">

		<table class="tableInformation">
			<tr>
				<td>No Doc</td>
				<td width="10%" align="center">:</td>
				<td>
					<?php
						$pc = substr($packing[1]['packing_number'], 9);
					?>
					PKB/WH/KRG/<?php echo date('Y/m').'/0'.$pc;?>
				</td>
			</tr>
			<tr>
				<td>Date</td>
				<td width="10%" align="center">:</td>
				<td><?php echo date('d/m/Y');?></td>
			</tr>
			<tr>
				<td>Delivery to</td>
				<td width="10%" align="center">:</td>
				<td><?php echo $packing[1]['DestinationName'];?></td>
			</tr>
			<tr>
				<td height="50">No picking</td>
				<td width="10%" align="center">:</td>
				<td>
					<span class="tally-rcv-code"><?php echo $packing[1]['packing_number']?></span>
				</td>
			</tr>
			<tr>
				<td>No Transaksi</td>
				<td width="10%" align="center">:</td>
				<td>
					<span class="tally-rcv-code"><?php echo $packing[1]['kd_outbound']; ?></span>

				</td>
			</tr>
		</table>
	</div>

	<div style="margin: 30px auto;">
		<table class="tblItem">
			<thead>
				<tr>
					<th align="center" style="padding: 0;">No</th>
					<th width="20%" style="padding-left: 10px;">Item Code</th>
					<th width="50%" style="padding-left: 10px;">Item Name</th>
					<th align="center">Qty</th>
					<th align="center">Colly</th>
				</tr>
			</thead>
			<tbody>

	<?php
		$r = 1;

		foreach($packing as $key=>$pack){

				$i = 1;
				foreach($pack['barang'] as $kd_barang=>$barang){

			?>
		
				<tr>
					<td align="center"><?php echo $i; ?></td>
					<td style="padding-left: 10px;"><?php echo $kd_barang;?> </td>
					<td style="padding-left: 10px;"><?php echo $barang['nama_barang'];?></td>
					<td align="center"><?php echo $barang['qty'];?></td>
					<td align="center"><?php echo $key.'/'.$qty ?></td>
				</tr>
		
			<?php

					$i++;
				}

			$r++;
		}
?>
			</tbody>
		</table>
	</div>

	<div style="margin:0 auto;width: 90%:">
		<table width="100%">
			<tr>			
				<td width="30%">Request by</td>
				<td width="30%">Warehouse</td>
				<td width="30%">Courier</td>
				<td>Approve</td>
			</tr>
		</table>
	</div>
</div>

</body>
</html>

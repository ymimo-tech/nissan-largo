<div class="row">
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>NEW PACKING JOB
				<small>Start packing items/materials based on picking list(s)</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>
<div class="row">
<div class="col-md-12 col-sm-12">
	<!-- BEGIN EXAMPLE TABLE PORTLET-->
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption">
				<span class="caption-subject font-green-sharp bold uppercase">
				<i class="fa fa-th-list"></i><?php echo $title;?>
				</span>
			</div>
			<div class="actions">
				<?php if ($this->access_right->otoritas('add')) : ?>
				<?php echo hgenerator::render_button_group($button_group); ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="portlet-body">
			<style>
				.tr_selected,table.dataTable .tr_selected td.sorting_1{
				background-color: #207ab7 !important;
				color:#fff !important;
				}
			</style>
			<div class="picking_list_container">
				<div class="rec_list_container">
					<div>
						<form id="search_rec" class="form form-horizontal">
							<div class="row" style="margin-bottom:15px;">
								<div class="col-md-4">
									<div class="row">
										<div class="col-md-6">Packing Number</div>
										<div class="controls col-md-6">
											<?php
												$pc = isset($pc_code) ? $pc_code : '';
											?>
											<input type="text" id="pc_code" name="pc_code" value="<?php echo $pc; ?>" class="form-control"/>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-6" style="text-align:right;">Destination</div>
										<input type="hidden" id="destination_code" />
										<div class="controls col-md-6">
											<p id="destination" style="position:absolute;"></p>
										</div>
									</div>
								</div>
							</div>
							<!--div class="row" style="margin-bottom:15px;">
								<div class="col-md-4">
									<div class="row">
										<div class="col-md-6">Scan Carton</div>
										<div class="controls col-md-6">
											<input type="text" id="carton" name="carton" class="form-control"/>
										</div>
									</div>
								</div>
							</div-->
							<div class="row" style="margin-bottom:15px;">
								<div class="col-md-4">
									<div class="row">
										<div class="col-md-6">Scan DN</div>
										<div class="controls col-md-6">
											<input type="text" id="picking_list" name="picking_list" class="form-control"/>
										</div>
									</div>
								</div>
								
								<!--div class="col-md-4">
									<div class="row">
										<div class="controls col-md-4">
											<a type="button" id="new_pl" class="btn green new_pl" href="#">NEW DN</a>
										</div>
									</div>
								</div-->
							</div>
							<div class="row" style="margin-bottom:15px;">
								<div class="col-md-4">
									<div class="row">
										<div class="col-md-6">Scan Item Code</div>
										<div class="controls col-md-6">
											<input type="text" id="item_code" name="item_code" class="form-control"/>
										</div>
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="row">
										<div class="controls col-md-4">
											<a type="button" id="confirm" class="btn green confirm" href="#">CONFIRM PACK</a>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-bottom:10px;">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-4"></div>
										
										<div class="col-md-2">Quantity</div>
										<div class="controls col-md-2">
											<input type="text" id="quantity" name="quantity" class="form-control"/>
										</div>
										<p id="uom">UoM</p>
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-bottom:15px;">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-4"></div>
										
										<div class="col-md-2">Weight</div>
										<div class="controls col-md-2">
											<input type="text" id="weight" name="weight" class="form-control"/>
										</div>
										KG
									</div>
								</div>
							</div>
							
							<div class="row" style="margin-bottom:15px;">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-4"></div>
										
										<div class="col-md-2">Volume</div>
										<div class="controls col-md-2">
											<input type="text" id="volume" name="volume" class="form-control"/>
										</div>
										CBM
									</div>
								</div>
							</div>
						</form>
					</div>
					
					<table id="rec-list" class="table table-striped table-bordered table-hover table-checkable order-column">
						<thead>
							<tr>
								<!--th style="display: none;"> id_picking_recomendation </th-->
								<th style="width: 30px;"> No </th>
								<th> <div align="center">Item Code</div> </th>
								<th> <div align="center">Item Name</div></th>
								<th> <div align="center">Outbound Doc.</div></th>
								<th> <div align="center">Picking Doc.</div></th>
								<th> <div align="center">Picked</div> </th>
								<th> <div align="center">Packed</div> </th>
								<!--th> <div align="center">Notes</div> </th-->
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					
					<div class="pull-right" style="margin-top: 25px;">
						<a type="button" class="btn blue print_label" href="#">PRINT LABEL</a>
						<a type="button" class="btn blue new_packing" href="#">NEW PACKING</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modal-cc" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal" class="form-horizontal">
				<input type="hidden" name="id"/>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Close Packing</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group do-group">
								<label class="col-md-2 control-labe">Remark</label>
								<div class="controls col-md-9">
									<textarea class="form-control" id="remark" name="remark" placeholder="remark"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-cc" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-check"></i> &nbsp;SUBMIT</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
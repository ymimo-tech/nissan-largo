<thead>
    <tr>
        <th><?php echo form_checkbox('cb_select_menu', '', false, 'id="cb_select_menu" target-selected="cb_select" onchange="select_all(this.id)"'); ?></th>
        <th>Menu Name</th>
        <th>Operator</th>
        <th>Supervisor</th>
<!--         <th>Edit</th>
        <th>Delete</th>
        <th>Approve</th>
        <th>Import</th>
        <th>Export</th> -->
    </tr>
</thead>
<?php foreach ($menu_group->result() as $group) : ?>
    <tr id="default_row_select">
        <td align="center"></td>
        <td colspan="3" style="font-weight: bold;"><?php echo $group->nama_grup_menu; ?></td>
    </tr>
    <tbody id="menu_group_<?php echo $group->kd_grup_menu ?>">
        <?php
        if (isset($menu[$group->kd_grup_menu])) :
            if (isset($menu[$group->kd_grup_menu][0])) :
                foreach ($menu[$group->kd_grup_menu][0] as $value) :
                    ?>
                    <tr>
                        <td align="center"><i class="icon-menu"></i></td>
                        <td style="font-weight: bold;"><?php echo $value['nama_menu']; ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_view[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_operator']) ? true : false, 'class="cb_select" target-selected="cb_select_baris_' . $value['kd_menu'] . '" id="cb_select_menu_' . $value['kd_menu'] . '" onchange="select_all(this.id)" '); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_add[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_spv']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                       <!--  <td align="center"><?php echo form_checkbox('otoritas_menu_edit[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_delete[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_approve[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_export[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_print[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td> -->
                    </tr>

                    <?php
                    if (isset($menu[$group->kd_grup_menu][$value['kd_menu']])) :
                        foreach ($menu[$group->kd_grup_menu][$value['kd_menu']] as $value2) :
                            ?>
                            <tr>
                                <td style="padding-right: 16px;" align="right"><i class="icon-menu"></i></td>
                                <td style="font-weight: bold;"><?php echo $value2['nama_menu']; ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_view[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_operator']) ? true : false, 'class="cb_select" target-selected="cb_select_baris_' . $value2['kd_menu'] . '" id="cb_select_menu_' . $value2['kd_menu'] . '" onchange="select_all(this.id)" '); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_add[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_spv']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                               <!--  <td align="center"><?php echo form_checkbox('otoritas_menu_edit[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_delete[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_approve[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_export[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_print[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td> -->
                            </tr>
                            <?php
                            if (isset($menu[$group->kd_grup_menu][$value2['kd_menu']])) :
                                $total_menu_3 = count($menu[$group->kd_grup_menu][$value2['kd_menu']]);
                                $idx_menu_3 = 0;
                                foreach ($menu[$group->kd_grup_menu][$value2['kd_menu']] as $value3) :
                                    $idx_menu_3++;
                                    $id3 = $value3['kd_menu'];
                                    $group_menu_3_up = '';
                                    $group_menu_3_down = '';
                                    if ($idx_menu_3 == 1) {
                                        $group_menu_3_up = 'disabled="disabled"';
                                    }
                                    if ($idx_menu_3 == $total_menu_3) {
                                        $group_menu_3_down = 'disabled="disabled"';
                                    }
                                    ?>

                                    <tr>
                                        <td align="right"><i class="icon-menu"></i></td>
                                        <td style="font-weight: bold;"><?php echo $value3['nama_menu']; ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_view[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_operator']) ? true : false, 'class="cb_select" target-selected="cb_select_baris_' . $value3['kd_menu'] . '" id="cb_select_menu_' . $value3['kd_menu'] . '" onchange="select_all(this.id)" '); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_add[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_spv']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <!-- <td align="center"><?php echo form_checkbox('otoritas_menu_edit[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_delete[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_approve[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_export[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_print[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td> -->
                                    </tr>

                                    <?php
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                endforeach;
            endif;
        endif;
        ?>
    </tbody>
<?php endforeach; ?>

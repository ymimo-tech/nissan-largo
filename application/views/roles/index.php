<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>ROLE MANAGEMENT
				<small>List of User roles</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
                    	<?php if(!empty($subTitle)) echo "<i class='icon-check'></i> Roles List"?>
                	</span>
				</div>

                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<table id="table_roles" class="table table-striped table-bordered table-hover table-checkable order-column">
							<thead>
								<tr>
									<th style="vertical-align: top;text-align: center;width: 30px;"> No </th>
									<th style="vertical-align: top;text-align: center;"> Role Name </th>
									<th style="vertical-align: top;text-align: center;"> Description </th>
									<th style="vertical-align: top;text-align: center;"> Action </th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?php if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) { ?>
<div id="form-content" class="modal fade" role="basic">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="data-table-form" class="form-horizontal">
                <input type="hidden" name="id"/>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitled'; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
	                    <div class="col-md-12">
	                        <div class="form-group">
	                            <label class="col-md-3 control-label">Role Name<span class="required">*</span></label>
	                            <div class="controls col-md-9">
	                                <?php echo form_input('grup_name','', 'class="col-md-12 form-control"') ?>

	                            </div>
	                        </div>
	                        <div class="form-group">
		                        <label class="col-md-3 control-label">Description</label>
		                        <div class="controls col-md-9">
		                            <?php echo form_textarea('description','', 'class="col-md-12 form-control"') ?>
		                        </div>
		                    </div>
	                    </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
						<!--
                        <span class="required pull-left">* harus diisi</span>
                        <button type="submit" class="btn blue"><i class="icon-save"></i> Simpan</button>
                        <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Kembali</a>
						-->
												<span class="required pull-left">* is required</span>
												<a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
												<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div id="form-content2" class="modal fade" role="basic">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="data-table-form" class="form-horizontal">
                <input type="hidden" name="id"/>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">User Menu Access</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
	                    <div class="col-md-12">
	                        <div class="form-group">
	                            <label class="col-md-3 control-label">Role Name</label>
	                            <div class="controls col-md-6">
	                                <?php echo form_input('grup_name','', 'class="col-md-12 form-control"') ?>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                    		<table id="table_roles" class="table table-striped table-bordered table-hover table-checkable order-column">
								<!-- content is loaded via ajax call -->
							</table>
                    	</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
						<!--
                        <span class="required pull-left">* harus diisi</span>
                        <button type="submit" class="btn blue"><i class="icon-save"></i> Simpan</button>
                        <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Kembali</a>
						-->
						<span class="required pull-left">* is required</span>
						<a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
						<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div id="form-content3" class="modal fade" role="basic">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="data-table-form" class="form-horizontal">
                <input type="hidden" name="id"/>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">User Menu Access</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
	                    <div class="col-md-12">
	                        <div class="form-group">
	                            <label class="col-md-3 control-label">Grup Name</label>
	                            <div class="controls col-md-6">
	                                <?php echo form_input('grup_name','', 'class="col-md-12 form-control"') ?>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                    		<table id="table_roles3" class="table table-striped table-bordered table-hover table-checkable order-column">
								<!-- content is loaded via ajax call -->
							</table>
                    	</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-actions">
						<!--
                        <span class="required pull-left">* harus diisi</span>
                        <button type="submit" class="btn blue"><i class="icon-save"></i> Simpan</button>
                        <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Kembali</a>
						-->
						<span class="required pull-left">* is required</span>
						<a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
						<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>

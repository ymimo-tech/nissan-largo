<thead>
    <tr>
        <th><?php echo form_checkbox('cb_select_menu', '', false, 'id="cb_select_menu"'); ?></th>
        <th>Menu Name</th>
        <th>View</th>
        <th>Add</th>
        <th>Edit</th>
        <th>Delete</th>
        <th>Approve</th>
        <th>Import</th>
        <th>Export</th>
    </tr>
</thead>
<?php foreach ($menu_group->result() as $group) : ?>
    <tr id="default_row_select">
        <td align="center"></td>
        <td colspan="8" style="font-weight: bold;"><?php echo $group->nama_grup_menu; ?></td>
    </tr>
    <tbody id="menu_group_<?php echo $group->kd_grup_menu ?>">
        <?php
        if (isset($menu[$group->kd_grup_menu])) :
            if (isset($menu[$group->kd_grup_menu][0])) :
                foreach ($menu[$group->kd_grup_menu][0] as $value) :

                    $kdMenu = $value['kd_menu'];
                    $checkAll = false;
                    if(in_array($kdMenu, $roles['is_view']) && in_array($kdMenu, $roles['is_edit']) && in_array($kdMenu, $roles['is_add']) && in_array($kdMenu, $roles['is_delete']) && in_array($kdMenu, $roles['is_approve']) && in_array($kdMenu, $roles['is_import']) && in_array($kdMenu, $roles['is_print'])){
                        $checkAll = true;
                    }
                    ?>
                    <tr>
                        <td align="center"><?php echo form_checkbox('selectAll', $value['kd_menu'], $checkAll); ?></td>
                        <td style="font-weight: bold;"><?php echo $value['nama_menu']; ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_view[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_view']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] .'"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_add[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_add']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_edit[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_delete[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_approve[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_export[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        <td align="center"><?php echo form_checkbox('otoritas_menu_print[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                    </tr>

                    <?php
                    if (isset($menu[$group->kd_grup_menu][$value['kd_menu']])) :
                        foreach ($menu[$group->kd_grup_menu][$value['kd_menu']] as $value2) :
                            $kdMenu = $value2['kd_menu'];
                            $checkAll = false;
                            if(in_array($kdMenu, $roles['is_view']) && in_array($kdMenu, $roles['is_edit']) && in_array($kdMenu, $roles['is_add']) && in_array($kdMenu, $roles['is_delete']) && in_array($kdMenu, $roles['is_approve']) && in_array($kdMenu, $roles['is_import']) && in_array($kdMenu, $roles['is_print'])){
                                $checkAll = true;
                            }
                            ?>
                            <tr>
                                <td align="center"><?php echo form_checkbox('selectAll', $value2['kd_menu'], $checkAll); ?></td>
                                <td style="font-weight: bold;"><?php echo $value2['nama_menu']; ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_view[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_view']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] .'"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_add[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_add']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_edit[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_delete[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_approve[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_export[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                <td align="center"><?php echo form_checkbox('otoritas_menu_print[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                            </tr>
                            <?php
                            if (isset($menu[$group->kd_grup_menu][$value2['kd_menu']])) :
                                $total_menu_3 = count($menu[$group->kd_grup_menu][$value2['kd_menu']]);
                                $idx_menu_3 = 0;
                                foreach ($menu[$group->kd_grup_menu][$value2['kd_menu']] as $value3) :
                                    $idx_menu_3++;
                                    $id3 = $value3['kd_menu'];
                                    $group_menu_3_up = '';
                                    $group_menu_3_down = '';
                                    if ($idx_menu_3 == 1) {
                                        $group_menu_3_up = 'disabled="disabled"';
                                    }
                                    if ($idx_menu_3 == $total_menu_3) {
                                        $group_menu_3_down = 'disabled="disabled"';
                                    }

                                    $kdMenu = $value3['kd_menu'];
                                    $checkAll = false;
                                    if(in_array($kdMenu, $roles['is_view']) && in_array($kdMenu, $roles['is_edit']) && in_array($kdMenu, $roles['is_add']) && in_array($kdMenu, $roles['is_delete']) && in_array($kdMenu, $roles['is_approve']) && in_array($kdMenu, $roles['is_import']) && in_array($kdMenu, $roles['is_print'])){
                                        $checkAll = true;
                                    }

                                    ?>

                                    <tr>
                                        <td align="center"><?php echo form_checkbox('selectAll', $value3['kd_menu'], $checkAll); ?></td>
                                        <td style="font-weight: bold;"><?php echo $value3['nama_menu']; ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_view[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_view']) ? true : false,  'class="cb_select cb_select_baris_' . $value3['kd_menu'] .'"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_add[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_add']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_edit[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_delete[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_approve[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_export[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                        <td align="center"><?php echo form_checkbox('otoritas_menu_print[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"'); ?></td>
                                    </tr>

                                    <?php
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                endforeach;
            endif;
        endif;
        ?>
    </tbody>
<?php endforeach; ?>

<script type="text/javascript">

    $(document).ready(function(){
        if( $('input[type="checkbox"]:checked').length == ($('input[type="checkbox"]').length - 1)){
            $('input[name="cb_select_menu"]').prop("checked", true);
        }else{
            $('input[name="cb_select_menu"]').prop("checked", false);
        }

        $('input[type="checkbox"]').change(function(){
            var id = $(this).val();
            var checkbox = $('input[class="cb_select cb_select_baris_'+id+'"]');
            if($(this).attr('name') == "selectAll"){
                if($(this).is(":checked")){
                    checkbox.prop("checked",true);
                }else{
                    checkbox.prop("checked",false);
                }
            }else if($(this).attr("name") == "cb_select_menu"){
                if($(this).is(":checked")){
                    $('input[type="checkbox"]').prop("checked", true);
                }else{
                    $('input[type="checkbox"]').prop("checked", false);
                }
            }else{
                if($('input[class="cb_select cb_select_baris_'+id+'"]:checked').length == checkbox.length){
                    $('input[name="selectAll"][value="'+id+'"]').prop("checked", true);
                }else{
                    $('input[name="selectAll"][value="'+id+'"]').prop("checked", false);
                }
            }

            if( $('input[type="checkbox"]:checked').length >= ($('input[type="checkbox"]').length - 1)){
                $('input[name="cb_select_menu"]').prop("checked", true);
            }else{
                $('input[name="cb_select_menu"]').prop("checked", false);
            }

        })
    });
</script>

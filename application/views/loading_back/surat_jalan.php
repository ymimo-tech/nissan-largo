<html>

<head>
	<title>Surat Jalan</title>
</head>

<body style="font-family: Arial;">
	<?php foreach($data as $key => $value){ ?>
	<div class="wrapper">
		<div class="wrapper-title" style="text-align: right;">
			<h2>SURAT JALAN</h2>
			<span>Printed on : <?php echo date('Y/m/d H:i');?></span>
		</div>
		<div class="wrapper-body" style="margin-top: 20px;">

			<div class="">
				<div style="float: left;width: 49.8%;">
					<img alt=""
						src="<?php echo base_url(); ?>barcode?text=<?php echo $value['outbound_code'];?>&size=65"
						style="border:1px solid;">
					<table style="">
						<tr>
							<td>Order ID # </td>
							<td><?php echo $value['outbound_code'];?></td>
						</tr>
					</table>
				</div>
				<div style="float: left;width: 49.8%; text-align: right;">
					<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $value['sj_code'];?>&size=65"
						style="border:1px solid;" style="border:1px solid;">
					<table align="right">
						<tr>
							<td><?php echo $value['sj_code'];?></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="" style="margin-top: 20px;">
				<div style="float: left; width: 49.5%;">
					<table>
						<tr>
							<td style="vertical-align: top;">Sender:</td>
							<td>
								<?php echo $value['warehouse_name']; ?>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Driver</td>
							<td><?php echo $value['driver_name']; ?></td>
						</tr>
						<tr>
							<td>License Plate</td>
							<td><?php echo $value['plate_number']; ?></td>
						</tr>dia
					</table>
				</div>
				<div style="float: left; width: 49.5%;">
					<table>
						<tr>
							<td>DIKIRIM KEPADA</td>
						</tr>
						<tr>
							<td>
								<?php echo $value['destination_name'];?><br />
							</td>
						</tr>
						<tr>
							<td>
								<?php echo $value['address'];?><br />
							</td>
						</tr>
						<!-- 						<tr>
							<td>
								<?php echo $value['address_1'];?><br/>
							</td>
						</tr>
						<tr>
							<td>
								<?php echo $value['address_2'];?><br/>
							</td>
						</tr>
						<tr>
							<td>
								<?php echo $value['address_3'];?><br/>
							</td>
						</tr>
 -->
					</table>
				</div>
			</div>
		</div>

		<div class="" style="margin-top: 50px;">

			<style type="text/css">
				table.table {
					border-collapse: collapse;
					text-align: center;
				}

				table.table thead tr th {
					border: 1px solid black;
					background-color: #dedede;
					font-weight: normal !important;
				}

				table.table tbody tr td {
					border: 1px solid;
					padding: 10px 0;
				}

				table.table tfoot tr td {
					border: 1px solid;
				}
			</style>

			<table width="100%" class="table">
				<?php foreach($data2[$key] as $key_data => $value){?>
					<?php foreach($value as $key_packing => $data_packing){ $i = 0;?>
						<thead>
							<tr>
								<td for=""><?php echo $key_data?></td>
							</tr>
							<tr>
								<th>SKU ID</th>
								<th>Nama SKU</th>
								<th>Variant</th>
								<th>Qty / Qolly</th>
								<th>Total Colly</th>
								<th>Qty (pcs)</th>
								<th>Weight </th>
								<th>Volume</th>
							</tr>
						</thead>
						<tbody>
							<tr>
							<td><?php echo $data_packing['item_code']?></td>
							<td><?php echo $data_packing['name']?></td>
							<td> - </td>
							<td><?php echo $data_packing['qty']?></td>
							<td>-</td>
							<td><?php echo $data_packing['qty']?></td>
							<td>100 Kg</td>
							<td>100 M3</td>
							</tr>
						</tbody>
					<?php $i++; }?>
				<?php }?>

				<!-- <thead>
					<tr>
						<th>SKU ID</th>
						<th>Nama SKU</th>
						<th>Variant</th>
						<th>Qty / Qolly</th>
						<th>Total Colly</th>
						<th>Qty (pcs)</th>
						<th>Weight </th>
						<th>Volume</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$totalColly = 0; 
						$qtyPcs = 0; 
						foreach($value['items'] as $d){

							$totalColly = $totalColly + $d['total_colly'];
							$qtyPcs = $qtyPcs + $d['qty_pcs'];
					?>
					<tr>
						<td><?php echo $d['item_code']; ?></td>
						<td><?php echo $d['item_name']; ?></td>
						<td><?php echo $d['parent_code']; ?></td>
						<td><?php echo $d['qty_per_colly']; ?></td>
						<td><?php echo $d['total_colly']; ?></td>
						<td><?php echo $d['qty_pcs']; ?></td>
						<td><?php echo $d['weight']; ?></td>
						<td><?php echo $d['volume']; ?></td>
					</tr>
					<?php
						}
					?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="2" style="border:none;"></td>
						<td colspan="2" style="background-color: #dedede;">TOTAL</td>
						<td><?php echo $totalColly; ?></td>
						<td><?php echo $qtyPcs; ?></td>
						<td colspan="2" style="background-color: #dedede;"></td>
					</tr>
				</tfoot> -->
			</table>

		</div>

		<div style="margin-top: 150px;">
			<table style="width: 100%;">
				<tbody>
					<tr>
						<td style="width: 10%;"></td>
						<td style="font-weight: normal !important;">Picker</td>
						<td style="width: 10%;"></td>
						<td style="font-weight: normal !important;">Checker</td>
						<td style="width: 10%;"></td>
						<td style="font-weight: normal !important;">Driver</td>
						<td style="width: 10%;"></td>
						<td style="font-weight: normal !important;">Receiver</td>
					</tr>
					<tr>
						<td style="width: 10%;height: 100px;"></td>
						<td style="border-bottom: 1px solid;"></td>
						<td></td>
						<td style="border-bottom: 1px solid;"></td>
						<td></td>
						<td style="border-bottom: 1px solid;"></td>
						<td></td>
						<td style="border-bottom: 1px solid;"></td>
					</tr>
					<tr>
						<td align="right">Name :</td>
						<td></td>
						<td align="right">Name :</td>
						<td></td>
						<td align="right">Name :</td>
						<td></td>
						<td align="right">Name :</td>
						<td></td>
					</tr>
					<tr>
						<td align="right">Date & Time :</td>
						<td></td>
						<td align="right">Date & Time :</td>
						<td></td>
						<td align="right">Date & Time :</td>
						<td></td>
						<td align="right">Date & Time :</td>
						<td></td>
					</tr>
				</tbody>
			</table>

		</div>

	</div>
	<?php }?>
</body>

</html>
<html>
<head>
	<title>Delivery Order</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Delivery Order</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>Delivery Order Number</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $do_number; ?>&size=65" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $do_number; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Doc. Number</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['kd_outbound']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Loading Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['shipping_date']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>To</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['customer_name']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Address</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['address']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Driver Name</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['driver_name']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Plate Number</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['license_plate']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> # </th>
				<th style="width: 70%;"> Item Name/SKU </th>
				<th> QTY </th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i = 1;
				foreach($items['data'] as $row){
					$str = '<tr>
								<td> '.$i.' </td>
								<td>
									<label>'.$row['nama_barang'].'</label>
								</td>
								<td><div style="text-align: right;"> '.$row['qty'].' '.$row['nama_satuan'].' </div></td>
							</tr>';
					
					echo $str;
					$i++;
				}
				
			?>
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>
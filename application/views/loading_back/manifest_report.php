<html>
<head>
	<title>Loading Manifest</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Loading Manifest</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>Manifest Number</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $manifest_code; ?>&size=65" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $manifest_code; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Doc. Number</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $doc_number; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Loading Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['shipping_date']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>To</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['shipping_route']; ?>
									</span>
								</td>
							</tr>
						</table>
						<!-- <table class="bottom10">
							<tr>
								<td class="width120">
									<label>Address</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['address']; ?>
									</span>
								</td>
							</tr>
						</table> -->
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Driver Name</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['driver_name']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Plate Number</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $loading['license_plate']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> # </th>
				<th style="width: 50%;"> Packing Code </th>
				<th>Customer</th>
				<th> Weight </th>
				<th> Volume </th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i = 1;
				$index = 0;
				foreach($packing_lists as $key => $row){
					$str = '<tr>
								<td> '.$i.' </td>
								<td> <img style="margin-top: 5px;" alt="" src="'.base_url().'barcode?text='.$key.'&size=30">
								<br>
								<label>'.$key.'</label>
								</td>
								<td> 
								<label>'.$row[$index]['customer'].'</label>
								</td>
								<td {{ align }}>
								<label>'.$row[$index]['weight'].' '.WEIGHT_UOM.'</label>
								</td>
								<td>
								<label>'.$row[$index]['volume'].' '.VOLUME_UOM.'</label>
								</td>
							</tr>';

					// $table_unique = '';
					// $table_qty = '';
					// foreach($rows as $row){
					// 	$table_unique .= '<label><center>'.$row['name'].'</center></label>
					// 				<br>
					// 				<img style="margin-top: 5px;" alt="" src="'.base_url().'barcode?text='.$row['unique_code'].'&size=20">
					// 				<br>
					// 				<label><center>'.$row['unique_code'].'</center></label>
					// 				<br>';
					// 	$table_qty .= '<label><center>'.$row['qty'].' '.$row['name_uom'].'</center></label><br>';			
					// 			}
					// $str =  str_replace('{{ data_unique_code }}', $table_unique, $str);
					// $str =  str_replace('{{ data_qty }}', $table_qty, $str);

							
					if(($i % 2) == 0){
						$str = str_replace('{{ align }}', 'align="right"', $str);
					}
					
					echo $str;
					$i++;
					$index++;
				}
				
			?>
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>
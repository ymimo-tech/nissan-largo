<style>
    .filter-container .select2-container{
        width: 100% !important;
        z-index: 9;
    }

    .select2-container{
        z-index: 100000;
    }
</style>

<div class="row">
	
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Customer
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-cogs"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
                                
						<a  class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
							
					<?php endif; ?>
					
					<?php if ($this->access_right->otoritas('print')) : ?>
                                
						<a  class="btn green btn-sm data-table-export min-width120">
							<i class="fa fa-file-excel-o"></i> EXPORT </a>
							
					<?php endif; ?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table-list">
                    <thead>
                        <tr>
                            <th> No </th>
                            <th> Customer Code </th>
                            <th> Customer Name </th>
							<th> Customer Phone </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                    
                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="customer-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Customer Code<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <input type="text" class="form-control" id="customer-code" name="customer_code" value="" placeholder="Customer Code" />
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Customer Name<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <input type="text" class="form-control" id="customer-name" name="customer_name" value="" placeholder="Customer Name" />
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-3 control-label">Address<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <textarea class="form-control" id="customer-address" name="customer_address" placeholder="Customer Address"></textarea>                                                
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-3 control-label">City</label>
                                            <div class="controls col-md-9">
                                                <input type="text" class="form-control" id="customer-city" name="customer_city" value="" placeholder="Customer City" />                                                
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-3 control-label">Postal Code</label>
                                            <div class="controls col-md-9">
                                                <input type="text" class="form-control" id="customer-postcode" name="customer_postcode" value="" placeholder="Customer Postal Code" />                                                
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-3 control-label">Country</label>
                                            <div class="controls col-md-9">
                                                <input type="text" class="form-control" id="customer-country" name="customer_country" value="" placeholder="Customer Country" />                                                
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-3 control-label">Phone</label>
                                            <div class="controls col-md-9">
                                                <input type="text" class="form-control" id="customer-phone" name="customer_phone" value="" placeholder="Customer Phone" />                                                
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-3 control-label">Fax</label>
                                            <div class="controls col-md-9">
                                                <input type="text" class="form-control" id="customer-fax" name="customer_fax" value="" placeholder="Customer Fax" />                                                
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-3 control-label">Contact Person</label>
                                            <div class="controls col-md-9">
                                                <input type="text" class="form-control" id="customer-cp" name="customer_cp" value="" placeholder="Customer Contact Person" />                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Warehouse</label>
                                            <div class="controls col-md-9">
                                                <?php
                                                    echo form_dropdown('warehouse[]', $warehouses, '', 'class="col-md-12 form-control" style="width: 100%;" id="warehouse" multiple data-placeholder="--Please Select--"');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* is required</span>
									<a class="form-close btn default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> BACK</a>
                                    <button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>  
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
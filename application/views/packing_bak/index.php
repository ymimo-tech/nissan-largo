<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>NEW PACKING JOB
				<small>Start packing items/materials based on picking list(s)</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>

						<?php echo hgenerator::render_button_group($button_group); ?>

					<?php endif; ?>
					<!--
                    <a class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-up"></i> Create Picking </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
				<style>
					.tr_selected,table.dataTable .tr_selected td.sorting_1{
						background-color: #207ab7 !important;
						color:#fff !important;
					}
				</style>
				<div class="picking_list_container">
					<div>
						<form id="search_picking" class="form form-horizontal">
							<div class="row">
								<div class="col-md-4">
									<div class="row">
										<div class="col-md-12">Scan Picking List to Start Packing</div>
										<div class="controls col-md-12">
											<input type="text" id="pl_name" name="pl_name" class="form-control"/>
										</div>
									</div>
								</div>
							</div>
						</form>
						<br/>
						<div class="row col-md-4">&nbsp;or select from the list below</div>
						<br/>
					</div>
					<table id="picking-list" class="table table-striped table-bordered table-hover table-checkable order-column">
						<thead>
							<tr>
								<th style="display: none;"> id_picking </th>
								<th style="width: 30px;"> No </th>
								<th style="vertical-align:top;"> <div align="center">Picking Doc. #</div> </th>
								<th style="vertical-align:top;"> <div align="center">Destination</div> </th>
								<th style="vertical-align:top;"> <div align="center"> Picking Date </th>
								<th style="vertical-align:top;"> <div align="center"> Remark </th>
								<th style="vertical-align:top;"> <div align="center"> Status </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
	        </table>
					<div class="form col-md-12 top20">
						<div class="form-actions">
							<div class="pull-right">
								<button type="button" class="btn blue open_picking">NEXT >></button>
							</div>
						</div>
					</div>
				</div>
				<div class="outbound_list_container" style="display:none">
					<div>
						Selected Picking List: <span class="picking_list_name"></span><br/><br/>
						Pick the Outbound Doc below:
					</div>
					<table id="outbound-list" class="table table-striped table-bordered table-hover table-checkable order-column">
						<thead>
							<tr>
								<th style="display: none;"> id_picking </th>
								<th style="width: 30px;"> <div align="center"> No</div></th>
								<th> <div align="center">Outbound Doc.</div> </th>
								<th> <div align="center">Destination</div> </th>
								<th> <div align="center">Address</div> </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
	                </table>
					<div class="form col-md-12 top20">
						<div class="form-actions">
							<div class="pull-right">
								<button type="button" class="btn default back_picking"><i class="fa fa-angle-double-left"></i> BACK</button> <button type="button" class="btn blue open_outbound">NEXT >></button>
							</div>
						</div>
					</div>
				</div>
            </div>
			<div class="rec_list_container" style="display:none">
				<div>
					<form id="search_rec" class="form form-horizontal">
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-6">Scan SN</div>
									<div class="controls col-md-6">
										<input type="text" id="kd_unik" name="kd_unik" class="form-control"/>
										<input type="hidden" id="id_outbound" name="id_outbound"/>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-2">QTY</div>
									<div class="controls col-md-4">
										<input type="text" id="qty" name="qty" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="pull-right">
									<a type="button" class="btn blue print_label disabled-link" href="#">PRINT LABEL</a>
									<a type="button" class="btn blue new_packing" href="#">NEW PACKING</a>
									<a type="button" class="btn blue print_surat_jalan disabled-link" href="#" target="_blank" style="display:none">PRINT SURAT JALAN</a>
									<a type="button" class="btn blue print_pkb disabled-link" href="#" target="_blank" style="display:none">PRINT PKB</a>
								</div>
							</div>
						</div>
						<div class="row" style="margin-bottom:5px;margin-top:10px;">
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-3">Picking List</div>
									<div class="controls col-md-6">
										: <span class="picking_list_name"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-bottom:5px;margin-top:10px;">
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-3">Outbound Doc.</div>
									<div class="controls col-md-6">
										: <span class="otb_kd"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-bottom:5px;margin-top:10px;">
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-3">Destination</div>
									<div class="controls col-md-6">
										: <span class="otb_name"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-bottom:5px;margin-top:10px;">
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-3">Address</div>
									<div class="controls col-md-6">
										: <span class="otb_alamat"></span>
									</div>
								</div>
							</div>
						</div>
						<br/>

						<!-- Dirubah formatnya menjadi:
							   Packing No: XXXXXXX
								 Tabel item2 yang di-scan utk XXXXXXX

								 Packing No: YYYYYYY
								 Tabel item2 yang di-scan utk YYYYYYY
					  -->
						
						<div class="row" style="margin-bottom:5px;margin-top:10px;">
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-6">Weight</div>
									<div class="controls col-md-4">
										<input type="text" id="weight" name="weight" class="form-control"/>
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-bottom:5px;margin-top:10px;">
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-6">Volume</div>
									<div class="controls col-md-4">
										<input type="text" id="volume" name="volume" class="form-control"/>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row" style="margin-bottom:5px;margin-top:10px;">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-4">Packing Number</div>
									<div class="controls col-md-6">
										<input type="text" id="pc_code" name="pc_code" class="form-control"/>
									</div>
								</div>
							</div>
							<!--div class="col-md-8">
								<div class="row">
									<div class="col-md-3">Packing Number</div>
									<div class="controls col-md-6">
										: <span class="otb_packing_numbers"></span>
									</div>
								</div>
							</div-->
						</div>
					</form>
				</div>
				<table id="rec-list" class="table table-striped table-bordered table-hover table-checkable order-column">
					<thead>
						<tr>
							<th style="display: none;"> id_picking_recomendation </th>
							<th style="width: 30px;"> No </th>
							<th> <div align="center">Item Code</div> </th>
							<th> <div align="center">Item Name</div></th>
							<th> <div align="center">Order Qty</div> </th>
							<th> <div align="center">Scanned Qty</div> </th>
							<th> <div align="center">Status</div> </th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
				<div class="form col-md-12 top20">
					<div class="form-actions">
						<div class="pull-right">
							<button type="button" class="btn default back_outbound"><i class="fa fa-angle-double-left"></i> BACK</button>
						</div>
					</div>
				</div>
			</div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="modal-cc" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal" class="form-horizontal">
				<input type="hidden" name="id"/>

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Close Packing</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
						   <div class="form-group do-group">
								<label class="col-md-2 control-labe">Remark</label>
								<div class="controls col-md-9">
									<textarea class="form-control" id="remark" name="remark" placeholder="remark"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-cc" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-check"></i> &nbsp;SUBMIT</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

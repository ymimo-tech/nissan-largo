<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>REPACK JOB
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i><?php echo $title; ?>
					</span>
				</div>
                <div class="actions">
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<div class="form-body">
					<form id="data-table-form" class="form-horizontal">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="params" value="" />

						<div class="row">
							<div class="col-md-12">
							   <div class="form-group">
									<label class="col-md-2">Repack Doc. #<span class="required">*</span></label>
									<div class="controls col-md-6">
										<div style="">
											<input type="text" id="kd-order_kitting" name="kd_order_kitting" class="form-control" value="<?php echo $doc_number; ?>" maxLength="50" />
										</div>
									</div>
								</div>
							   <div class="form-group tanggal-order_kitting-group">
									<label class="col-md-2">Repack Doc. Date<span class="required">*</span></label>
									<div class="col-md-6">
										<div class="input-group input-medium date date-picker">
											<input type="text" class="form-control" id="tanggal-order_kitting" name="tanggal_order_kitting" placeholder="dd/mm/yyyy"
												value="<?php echo $today; ?>" maxLength="10" />
											<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>
							   </div>
							   <div class="form-group">
									<label class="col-md-2">Type<span class="required">*</span></label>
									<div class="controls col-md-6">
										<div>
											<?php echo form_dropdown('type', $types,'','class="form-control" id="type"'); ?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Job for Warehouse<span class="required">*</span></label>
									<div class="controls col-md-6">
										<?php
											echo form_dropdown('warehouse', $warehouses, '', 'class="col-md-12 form-control" style="width: 100%;" id="warehouse" data-placeholder="--Please Select--"');
										?>
									</div>
								</div>
								<?php if(!empty($id)) { ?>
									<!--
									<div class="form-group">
										<label class="col-md-2">Status<span class="required">*</span></label>
										<div class="controls col-md-6">

										</div>
									</div>
									-->
								<?php } ?>
							</div>
						</div>

						<hr>
						<!--<h4>Items</h4>-->
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped table-bordered table-hover table-checkable order-column" id="inbound-barang">
									<thead>
										<tr>
											<th> No </th>
											<th> Item Name </th>
											<th style="text-align: center; width: 200px;"> Quantity </th>
											<th style="text-align: center"> Action </th></th>
										</tr>
									</thead>
									<tbody id="added_rows">
									</tbody>
									<tbody>
										<tr>
											<td colspan="4" class="add_item">
												<button type="button" class="btn blue min-width120 add-item"
													<?php if(empty($id)) { ?>data-toggle="tooltip" title="Click here to add item" data-placement="bottom"<?php } ?>><i class="fa fa-plus"></i> Add Item</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<span class="required pull-left">* is required</span>
									<div class="pull-right">

										<?php if(empty($id)) { ?>
											<a class="btn default min-width120" href="<?php echo base_url().$page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
										<?php } else { ?>
											<a class="btn default min-width120" href="javascript:;" onClick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
										<?php } ?>

										<?php if($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')){ ?>
											<button id="save" type="submit" class="btn blue min-width120"> SAVE</button>

											<?php if(empty($id)){ ?>
												<button id="save-new" type="button" class="btn green-haze min-width120"> SAVE & NEW</button>
											<?php } ?>

										<?php } ?>

									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

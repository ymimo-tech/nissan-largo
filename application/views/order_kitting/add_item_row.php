<tr class="added_row">
	<td>#</td>
	<td class="form-group">
		<?php echo form_dropdown("id_kitting[]", $items, isset($order_kitting_barang) ? $order_kitting_barang['id_kitting'] : "", "class='form-control add_rule item-order_kitting'"); ?>
	</td>
	<td class="form-group">
		<div class="pull-right">
			<?php echo form_input("item_quantity[]", isset($order_kitting_barang) ? $order_kitting_barang['jumlah_kitting'] : "", 'class="col-md-12 form-control add_rule" style="width: 100px;"'); ?>
			&nbsp;&nbsp;
			<!--<label class="label-control uom" style="margin-top: 7px;">
				<?php
					if(isset($order_kitting_barang['nama_satuan']))
						echo $order_kitting_barang['nama_satuan'];
					else
						echo'UoM';
				?>
			</label>-->
		</div>
	</td>
	<td class="remove_row">
		<button class="btn btn-delete btn-danger btn-xs" type="button"><span class="glyphicon glyphicon-remove"></span></button>
	</td>
</tr>

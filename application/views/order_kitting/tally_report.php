<html>
<head>
	<title>Order Repack Dokument</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Order Repack Dokument</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>Repack Doc. #</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $receiving['kd_order_kitting'] ?>&size=65" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $receiving['kd_order_kitting']; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Repack Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $receiving['tanggal_order_kitting']; ?>
									</span>
								</td>
							</tr>
						</table>
						<!-- <table class="bottom10">
							<tr>
								<td class="width120">
									<label>Vehicle Plate</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $receiving['vehicle_plate']; ?>
									</span>
								</td>
							</tr>
						</table> -->
						
					</td>
				</tr>
			</table>
		</div>
	</div>

	<table class="table-pdf">
		<thead>
			<tr>
				<th colspan = '3'> Item Production Product </th>
			</tr>
		</thead>
		<thead>
			<tr>
				<th> # </th>
				<th style="width: 30%;"> QTY </th>
				<th> Item Name/SKU </th>
			</tr>
		</thead>
		
		<tbody>
			<?php
				$i = 1;
				foreach($temp2['data'] as $row){
					$str = '<tr>
								<td> '.$i.' </td>
								<td> '.$row['jumlah_kitting'].' '.$row['nama_satuan'].' </td> 
								<td {{ align }}>
									<label>'.$row['nama_barang'].' - '.$row['kd_barang'].'</label>
									<br>
									<img style="margin-top: 5px;" alt="" src="'.base_url().'barcode?text='.$row['kd_barang'].'&size=35">
								</td>
							</tr>';
							
					if(($i % 2) == 0){
						$str = str_replace('{{ align }}', 'align="right"', $str);
					}
					
					echo $str;
					$i++;
				}
			?>
		</tbody>
	</table>
	<!-- begin table -->
	
	<table class="table-pdf">
		<thead>
			<tr>
				<th colspan = '3'> Item Material </th>
			</tr>
		</thead>
		<thead>
			<tr>
				<th> # </th>
				<th style="width: 30%;"> QTY </th>
				<th> Item Name/SKU </th>
			</tr>
		</thead>
		
		<tbody>
			<?php
				$i = 1;
				foreach($items['data'] as $row){
					$str = '<tr>
								<td> '.$i.' </td>
								<td> '.$row['qty_doc'].' '.$row['nama_satuan'].' </td> 
								<td {{ align }}>
									<label>'.$row['nama_barang'].' - '.$row['kd_barang'].'</label>
									<br>
									<img style="margin-top: 5px;" alt="" src="'.base_url().'barcode?text='.$row['kd_barang'].'&size=35">
								</td>
							</tr>';
							
					if(($i % 2) == 0){
						$str = str_replace('{{ align }}', 'align="right"', $str);
					}
					
					echo $str;
					$i++;
				}
			?>
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>
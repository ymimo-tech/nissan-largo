<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>REPACK JOB DETAIL
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
									<span class="caption-subject font-green-sharp bold uppercase">
									<i class="fa fa-file"></i>
									<?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<div class="top-info">
						<label>Date : </label>
						<span class=""><?php echo $data['tanggal_order_kitting']; ?></span>
						<label>Created by : </label>
						<span class=""><?php echo !empty($data['nama']) ? $data['nama'] : '-'; ?></span>
					</div>
					<!--
					<table class="table-info">
						<tr>
							<td><strong>Date</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Doc. Type</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Created by</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
					</table>
					-->
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="params" value="" />

						<div class="row">
							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Order Repack Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['kd_order_kitting']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['kd_order_kitting']; ?>
									</div>
								</div>
							</div>

							<div class="col-md-5">
								<div class="detail-caption text-left">

								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Order Repack Status</h1>
									<div class="detail-status
										<?php
											if($data['status_order_kitting'] == 'Open' || $data['status_order_kitting'] == 'In Progress')
												echo 'status-blue';
											else
												echo 'status-green';
										?>">
										<?php echo $data['status_order_kitting']; ?>
									</div>
								</div>
							</div>
						</div>

						<hr style="margin-top: 0;">


						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Ordered Items
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id </th>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Item Code</div> </th>
											<th> Item Name </th>
											<th style="width: 120px;"> <div align="center">Ordered</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<hr class="top30">

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Material Repack
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list-material" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id </th>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Item Code</div> </th>
											<th> Item Name </th>
											<th style="width: 120px;"> <div align="center">Picked</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<hr class="top30">

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Picked Items
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list-picked" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id </th>
											<th style="width: 30px;"> No </th>
											<th> <div align="center">Item</div> </th>
											<th> Serial Number </th>
											<th style="width: 120px;"> <div align="center">Picked Qty</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<hr class="top30">

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Tally Items
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list-tally" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id </th>
											<th style="width: 30px;"> No </th>
											<th> <div align="center">Item</div> </th>
											<th> Serial Number </th>
											<th> Location </th>
											<th style="width: 120px;"> <div align="center">Qty</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>


					</div>
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">



								<div class="pull-right">

									<?php
										$class = '';
										/*if($data['finish_tally'] != NULL){
											$class = 'disabled-link';
										}*/
									?>
									<!--<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>-->
									<a class="btn default min-width120" href="javascript:;" onclick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
									<?php
										if ($this->access_right->otoritas('edit')) :
											if($data['st_repack'] == 0):
									?>
												<a href="<?php echo base_url() . $page_class; ?>/edit/<?php echo $id; ?>" class="btn blue min-width120"><i class="fa fa-pencil"></i> EDIT</a>
									<?php
											endif;
										endif;
									?>

								</div>
							</div>
						</div>
					</div>

				</form>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<style>
	.select2-container{
		z-index:100000;
	}
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>BIN LOCATIONS
				<small>List of all bin locations assigned in the warehouse</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-cogs"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a  class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>

					<?php if ($this->access_right->otoritas('print')) : ?>
						<a href="javascript:;" class="btn green btn-sm btn-print min-width120">
							<i class="fa fa-print"></i> PRINT </a>

						<a  class="btn green-haze btn-sm data-table-export min-width120">
							<i class="fa fa-file-excel-o"></i> EXPORT </a>

					<?php endif; ?>

                </div>
            </div>
            <div class="portlet-body">
                <!--COPY THIS SECTION FOR FILTER CONTAINER-->
                <div class="portlet box blue-hoki filter-container">
                    <div class="portlet-title">
                        <div class="caption">
                            Filter
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="filter-indicator">Show filter</a>
                            <a href="javascript:;" class="expand show-hide">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display: none;">
                        <!--YOUR CUSTOM FILTER HERE-->
                        <form id="form-search" class="form-horizontal" role="form">
                            <div class="form-body">


                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group po-group">
                                            <label for="po" class="col-md-3">Bin Name</label>
                                            <div class="col-md-8">
                                                <div class="custom-select select-type4 high location">

                                                    <input type="teks" name="bin-name" id="bin-name" class="form-control">
                                                </div>
                                                <span class="help-block po-msg"></span>
                                            </div>
                                        </div>
                                         <div class="form-group po-group">
                                            <label for="po" class="col-md-3">Bin Description</label>
                                            <div class="col-md-8">
                                                <div class="custom-select select-type4 high location">

                                                    <input type="teks" name="bin-desc" id="bin-desc" class="form-control">
                                                </div>
                                                <span class="help-block po-msg"></span>
                                            </div>
                                        </div>

                                        <div class="form-group supplier-group">
                                            <label for="supplier" class="col-md-3">Category</label>
                                            <div class="col-md-8">
                                                <div class="custom-select select-type4 high location">
                                                    <select id="category" class="form-control">
                                                    <?php
                                                        foreach($loc_category as $k => $v){
                                                            echo '<option value="'.$k.'">'.$v.'</option>';
                                                        }
                                                    ?>
                                                    </select>
                                                </div>
                                                <span class="help-block supplier-msg"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group doc-type-group">
                                            <label for="doc-type" class="col-md-3">Type</label>
                                            <div class="col-md-8">
                                                <div class="custom-select select-type4 high location">
                                                    <select id="type" class="form-control">
                                                    <?php
                                                        foreach($loc_type as $k => $v){
                                                            echo '<option value="'.$k.'">'.$v.'</option>';
                                                        }
                                                    ?>
                                                    </select>
                                                </div>
                                                <span class="help-block doc-type-msg"></span>
                                            </div>
                                        </div>
                                        <div class="form-group doc-type-group">
                                            <label for="doc-type" class="col-md-3">Area</label>
                                            <div class="col-md-8">
                                                <div class="custom-select select-type4 high location">
                                                    <select id="area" class="form-control">
                                                    <?php
                                                        foreach($loc_area as $k => $v){
                                                            echo '<option value="'.$k.'">'.$v.'</option>';
                                                        }
                                                    ?>
                                                    </select>
                                                </div>
                                                <span class="help-block doc-type-msg"></span>
                                            </div>
                                        </div>
										<div class="form-group status-group">
											<label for="status" class="col-md-3">Status</label>
											<div class="col-md-8">
												<select id="status" class="form-control">
												</select>
												<span class="help-block status-msg"></span>
											</div>
										</div>

                                    </div>

                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-right">
                                            <button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
                                            <button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--END-->
                    </div>
                </div>
                <!--END SECTION-->
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <th style="text-align:center" class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
                            <th style="vertical-align: top;text-align:center"> No </th>
                            <th style="vertical-align: top;text-align:center"> Bin Name </th>
                            <th style="vertical-align: top;text-align:center"> Bin Description </th>
                            <th style="vertical-align: top;text-align:center"> Category </th>
                            <th style="vertical-align: top;text-align:center"> Type </th>
                            <th style="vertical-align: top;text-align:center"> Area </th>
                            <th style="vertical-align: top;text-align:center"> Status </th>
                            <th style="vertical-align: top;text-align:center"> Action </th>
                        </tr>
                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="referensi-lokasi-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body" style="overflow: hidden;">
                                <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                            <label class="col-md-4 control-label">Bin Name<span class="required">*</span></label>
                                            <div class="controls col-md-8">
                                                <?php echo form_input('loc_name','', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Bin Description </label>
                                            <div class="controls col-md-8">
                                                <?php echo form_input('loc_desc', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
										<!-- <div class="form-group type-group">
                                            <label class="col-md-4 control-label">Location Type<span class="required">*</span></label>
                                            <div class="controls col-md-8">
                                                <select id="loc-type" name="loc_type" style="width: 100%;">
													<?php
														foreach($location_type as $k => $v){
															echo '<option value="'.$k.'">'.$v.'</option>';
														}
													?>
												</select>
                                            </div>
                                        </div> -->
										<div class="form-group loc-cat-group">
											<label class="col-md-4 control-label">Location Category<span class="required">*</span></label>
											<div class="col-md-8">
												<?php echo form_dropdown('loc_category_id', $loc_category, '', 'class="col-md-12 form-control" style="width: 100%;" id="loc_category_id"'); ?>
												<span class="help-block cat-msg"></span>
											</div>
										</div>
										<div class="form-group loc-cat-group">
											<label class="col-md-4 control-label">Location Area<span class="required">*</span></label>
											<div class="col-md-8">
												<?php echo form_dropdown('loc_area_id', $loc_area, '', 'class="col-md-12 form-control" style="width: 100%;" id="loc_area_id"'); ?>
												<span class="help-block cat-msg"></span>
											</div>
										</div>
										<div class="form-group loc-cat-group">
											<label class="col-md-4 control-label">Location Type<span class="required">*</span></label>
											<div class="col-md-8">
												<?php echo form_dropdown('loc_type_id', $loc_type, '', 'class="col-md-12 form-control" style="width: 100%;" id="loc_type_id"'); ?>
												<span class="help-block cat-msg"></span>
											</div>
										</div>
										<div class="form-group loc-cat-group">
											<label class="col-md-4 control-label">Color Code Level</label>
											<div class="col-md-8">
                                                <?php echo form_input('color_code_level', '', 'class="col-md-12 form-control"') ?>
												<span class="help-block cat-msg"></span>
											</div>
										</div>
										<div class="form-group loc-cat-group">
											<label class="col-md-4 control-label">Color Code Bin</label>
											<div class="col-md-8">
                                                <?php echo form_input('color_code_bin', '', 'class="col-md-12 form-control"') ?>
												<span class="help-block cat-msg"></span>
											</div>
										</div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="controls col-md-8">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        Rack<br/>
                                                        <?php echo form_input('rack', '', 'class="col-md-12 form-control"') ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        Row<br/>
                                                        <?php echo form_input('row', '', 'class="col-md-12 form-control"') ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        Level<br/>
                                                        <?php echo form_input('level', '', 'class="col-md-12 form-control"') ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        Bin<br/>
                                                        <?php echo form_input('bin', '', 'class="col-md-12 form-control"') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* is required</span>
									<a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
                                    <button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?php if ($this->access_right->otoritas('print')) { ?>
	<?php include 'print_modal.php'; ?>
<?php }?>

<style>
	.qty h1{
		font-size: 46px !important;
		font-weight: bold;
		color: #fff !important;
	}

	.qty h1 a{
		color: #fff !important;
	}

	.qty h1 a:hover{
		text-decoration: none;
	}

	.qty h1 a:active{
		text-decoration: none;
	}

	.qty h1 a:focus{
		text-decoration: none;
	}

	.qty span{
		font-weight: bold;
		color: #fff;
	}

	.widget-thumb{
		padding: 10px;
	}
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1><?php echo $title ?>
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
                    	<?php if(!empty($subTitle)) echo $subTitle;?>
                	</span>
				</div>
            </div>
            <div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<h3><strong><?php echo $location->loc_name . " | " . $location->loc_desc ?></strong></h3>
					</div>
					<div class="col-md-6">

						<table>
							<tr>
								<td>Location Category</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $location->nama_kategori; ?></td>
							</tr>
							<tr>
								<td>Location Type</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $location->loc_type_name; ?></td>
							</tr>
							<tr>
								<td>Location Area</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $location->loc_area_name; ?></td>
							</tr>
						</table>
						<br>
					</div>

					<div class="col-md-6">
						<table>
							<tr>
								<td>Location Function</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $location->loc_type; ?></td>
							</tr>
							<tr>
								<td>Location Warehouse</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $location->warehouse_name; ?></td>
							</tr>
						</table>
						<br>
					</div>
				</div>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase"><i class="fa fa-th-list"></i> ITEM LIST</span>
				</div>
				<div class="tools">
					<button id="export-detail-item" type="button" class="btn green" style="min-width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT</button>
            </div>
        </div>
        <div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<table id="table-item-list" class="table table-striped table-bordered table-hover table-checkable order-column">
								<thead>
									<tr>
										<th> No. </th>
										<th style="text-align: center;"> Item Code </th>
										<th style="text-align: center;"> Item Name </th>
										<th style="text-align: center;"> Qty </th>
										<th style="text-align: center;"> Action </th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!--
				<div class="form-actions top40">
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
							</div>
						</div>
					</div>
				</div>
				-->
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<!-- <div class="row">
    <div class="col-md-12 col-sm-12"> -->
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <!-- <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase"><i class="fa fa-th-list"></i> SERIAL NUMBER LIST</span>
				</div>
				<div class="tools">
					<button id="export-detail" type="button" class="btn green" style="min-width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT</button>
                </div>
            </div>
            <div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<table id="table_receiving" class="table table-striped table-bordered table-hover table-checkable order-column">
								<thead>
									<tr>
										<th class="table-checkbox">
											<input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" />
										</th>
										<th style="width: 30px;"> No </th>
										<th style="text-align: center;"> Serial Number </th>
										<th style="text-align: center;"> License Plate </th>
										<th style="text-align: center;"> Qty </th>
										<th style="text-align: center;"> Doc. Reference # </th>
										<th style="text-align: center;"> Exp Date </th>
										<th style="text-align: center;"> In Date </th>
										<th style="text-align: center;"> Item Status </th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div> -->
				<!--
				<div class="form-actions top40">
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
							</div>
						</div>
					</div>
				</div>
				-->
            <!-- </div>

        </div> -->
        <!-- END EXAMPLE TABLE PORTLET-->
    <!-- </div>
</div> -->

<?php if ($this->access_right->otoritas('print')) { ?>
	<?php include 'print_modal.php'; ?>
<?php }?>

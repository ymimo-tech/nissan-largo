<div class="row">

    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="custom-page-title">
            <h1>ACTIVE LOCATIONS
                <small>Show all bin locations and their contents</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ALERTS PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">

				<div class="caption col-md-12">

					<button id="export" type="button" class="btn green pull-right" style="width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT</button>
                    <span class="caption-subject font-green-sharp bold uppercase"><?php if(!empty($subTitle)) echo $subTitle;?></span>

                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

						<!--COPY THIS SECTION FOR FILTER CONTAINER-->
						<div class="portlet box blue-hoki filter-container">
							<div class="portlet-title">
								<div class="caption">
									Filter
								</div>
								<div class="tools">
									<a href="javascript:;" class="filter-indicator">Show filter</a>
									<a href="javascript:;" class="expand show-hide">
									</a>
								</div>
							</div>
							<div class="portlet-body" style="display: none;">
								<!--YOUR CUSTOM FILTER HERE-->
								<form id="form-search" class="form-horizontal" role="form">
									<div class="form-body">


										<div class="row">

											<div class="col-md-6">

												<div class="form-group loc-group">
													<label for="loc" class="col-md-3">Location</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="loc" id="loc" style="width: 100%;" multiple data-placeholder="-- All --">
															</select>
														</div>
														<span class="help-block loc-msg"></span>
													</div>
												</div>

												<div class="form-group loc-type-group">
													<label for="loc-type" class="col-md-3">Location Type</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="loc_type" id="loc-type" style="width: 100%;" multiple data-placeholder="-- All --">

																<?php

																	//echo '<option value="">-- All --</option>';

																	foreach($location_type as $k => $v ){
																		echo '<option value="'.$k.'">'.$v.'</option>';
																	}

																?>

															</select>
														</div>
														<span class="help-block loc-type-msg"></span>
													</div>
												</div>

											</div>

											<div class="col-md-6">


												<div class="form-group item-group">
													<label for="item" class="col-md-3">Item / Part Number</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="item" id="item" style="width: 100%;" multiple data-placeholder="-- All --">
															</select>
														</div>
														<span class="help-block item-msg"></span>
													</div>
												</div>

												<div class="form-group cat-group">
													<label for="cat" class="col-md-3">Location Area</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="cat" id="cat" style="width: 100%;" multiple data-placeholder="-- All --">

																<?php

																	//echo '<option value="">-- All --</option>';

																	foreach($category as $row){
																		echo '<option value="'.$row['id_qc'].'">'.$row['kd_qc'].'</option>';
																	}

																?>

															</select>
														</div>
														<span class="help-block cat-msg"></span>
													</div>
												</div>

											</div>

										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-12">
												<div class="pull-right">
													<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
													<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
												</div>
											</div>
										</div>
									</div>
								</form>
								<!--END-->
							</div>
						</div>
						<!--END SECTION-->

                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <!--<div class="portlet light">
                            <div class="portlet-body">-->
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                                    <thead>
                                        <tr>
											<!--
                                            <th class="table-checkbox">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
											-->
                                            <th> No </th>
                                            <th style="text-align: center;"> Location Name </th>
											                      <th style="text-align: center;"> Location Area </th>
                                            <th style="text-align: center;"> Location Type </th>
											                      <th style="text-align: center;"> Items </th>
                                            <th style="text-align: center;"> Total Items </th>
                                        </tr>
                                    </thead>
                                    <tbody> </tbody>

                                </table>
								<div>&nbsp;</div>
							<!--
                            </div>
                            <div>&nbsp;</div>
                        </div>-->
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- /.modal -->
            </div>
        </div>
        <!-- END ALERTS PORTLET-->
    </div>
</div>

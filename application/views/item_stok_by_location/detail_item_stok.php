
<div class="inner_content">

    <div class="widgets_area">
        <div class="row-fluid">
            <div class="span6">
                <div id="index-content" class="well-content">
                    <div class="well">
                        <div class="pull-left">
                            <?php echo hgenerator::render_button_group($button_group); ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            if ($this->access_right->otoritas('print')) 
                                echo hgenerator::render_button_group($print_group);
                            ?>
                        </div>
                    </div>
                    <?php echo form_open_multipart('', array('id' => 'ffilter_detail_'.$id_barang,'tools-filter'=>FALSE)); ?>
                    <?php echo form_hidden('id_barang',$id_barang);?>
                    <?php echo form_hidden('tahun_aktif',$tahun_aktif);?>
                    <?php echo form_close(); ?>
                    <div class="form_row">
                        <div class="pull-left span6">
                            <div>
                                Jumlah Awal : <?php echo $jumlah_awal;?>
                            </div>
                        </div>  
                    </div>
                    <div class="form_row">
                        <div class="pull-left span6">
                            <div>
                                Jumlah Stok Opname : <?php echo $jumlah_son;?>
                            </div>
                        </div>  
                    </div>
                    <div class="form_row">
                        <div class="pull-left span6">
                            <div>
                                Jumlah BBM : <?php echo $jumlah_bbm;?>
                            </div>
                        </div>  
                    </div>  
                    <div class="form_row">
                        <div class="pull-left span6">
                            <div>
                                Jumlah BBK : <?php echo $jumlah_bbk;?>
                            </div>
                        </div>  
                    </div>  
                    <div class="form_row">
                        <div class="pull-left span6">
                            <div>
                                <div id="content_table_detail_m_<?php echo $id_barang;?>" data-source="<?php echo $data_source_bbm; ?>" data-filter="#ffilter_detail_<?php echo $id_barang;?>"></div>
                            </div>
                        </div>  
                        <div class="pull-left span6">
                            <div>
                                <div id="content_table_detail_k_<?php echo $id_barang;?>" data-source="<?php echo $data_source_bbk; ?>" data-filter="#ffilter_detail_<?php echo $id_barang;?>"></div>
                            </div>
                        </div>  
                        <script type="text/javascript">
                            $(function() {
                                load_table('#content_table_detail_m_<?php echo $id_barang;?>', 1);
                                load_table('#content_table_detail_k_<?php echo $id_barang;?>', 1);
                            });
                        </script>
                    </div>

                </div>
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.onchange').change(function(){
            load_table('#content_table_detail_<?php echo $id_barang;?>', 1);
        });
    });

    function refresh_filter(){ 
        load_table('#content_table_detail_<?php echo $id_barang;?>', 1,function(){

        });
    }

</script>


<style>
    .filter-container .select2-container{
        width: 100% !important;
        z-index: 9;
    }

    .select2-container{
        z-index: 100000;
    }
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Integration log
				<small>List of Integration log</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-cogs"></i><?php echo $title;?>
					</span>
				</div>
                <!-- <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>

					<?php if ($this->access_right->otoritas('print')) : ?>

						<a  class="btn green btn-sm data-table-export min-width120">
							<i class="fa fa-file-excel-o"></i> EXPORT </a>

					<?php endif; ?>
                </div> -->
            </div>
            <div class="portlet-body">


                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <th style="vertical-align:top;text-align:center"> No </th>
                            <th style="vertical-align:top;text-align:center"> Log Name </th>
                            <th style="vertical-align:top;text-align:center"> Api Url </th>
                            <th style="vertical-align:top;text-align:center"> Parameter </th>
                            <th style="vertical-align:top;text-align:center"> Response </th>
                            <th style="vertical-align:top;text-align:center"> Time </th>

                        </tr>
                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="referensi-supplier-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Transporter Name<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('vehicle_name', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Transporter No<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('vehicle_plate', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* is required</span>
                                    <a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
									<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

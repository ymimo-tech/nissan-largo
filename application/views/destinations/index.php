<style>
    .filter-container .select2-container{
        width: 100% !important;
        z-index: 9;
    }

    .select2-container{
        z-index: 100000;
    }
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>DESTINATIONS
				<small>List of recipients of outgoing items/materials</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-cogs"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>

					<?php if ($this->access_right->otoritas('print')) : ?>

						<a  class="btn green btn-sm data-table-export min-width120">
							<i class="fa fa-file-excel-o"></i> EXPORT </a>

					<?php endif; ?>
                </div>
            </div>
            <div class="portlet-body">

                <!--COPY THIS SECTION FOR FILTER CONTAINER-->
                <div class="portlet box blue-hoki filter-container">
                    <div class="portlet-title">
                        <div class="caption">
                            Filter
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="filter-indicator">Show filter</a>
                            <a href="javascript:;" class="expand show-hide">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display: none;">
                        <!--YOUR CUSTOM FILTER HERE-->
                        <form id="form-search" class="form-horizontal" role="form">
                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group item-group">
                                            <label for="item" class="col-md-3">Destination Code</label>
                                            <div class="col-md-6">
                                                <input type="text" name="destination_code_filter" class="form-control">
                                                <span class="help-block item-msg"></span>
                                            </div>
                                        </div>

                                        <div class="form-group item-group">
                                            <label for="item" class="col-md-3">Destination Name</label>
                                            <div class="col-md-6">
                                                <input type="text" name="destination_name_filter" class="form-control">
                                                <span class="help-block item-msg"></span>
                                            </div>
                                        </div>

										<div class="form-group status-group">
											<label for="status" class="col-md-3">Status</label>
											<div class="col-md-6">
												<select id="status" class="form-control">
												</select>
												<span class="help-block status-msg"></span>
											</div>
										</div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group item-group">
                                            <label for="item" class="col-md-3">Destination Contact Person</label>
                                            <div class="col-md-6">
                                                <input type="text" name="destination_contact_person_filter" class="form-control">
                                                <span class="help-block item-msg"></span>
                                            </div>
                                        </div>

                                        <div class="form-group item-group">
                                            <label for="item" class="col-md-3">Destination City</label>
                                            <div class="col-md-6">
                                                <input type="text" name="destination_city_filter" class="form-control">
                                                <span class="help-block item-msg"></span>
                                            </div>
                                        </div>

                                        <div class="form-group item-group">
                                            <label for="item" class="col-md-3">Destination Type</label>
                                            <div class="col-md-6">
                                                <?php echo form_dropdown('destination_type_filter',$sourceType,['class'=>'form-control'])?>
                                                <span class="help-block item-msg"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-right">
                                            <button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
                                            <button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--END-->
                    </div>
                </div>
                <!--END SECTION-->

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <th style="vertical-align:top;text-align:center"> No </th>
                            <th style="vertical-align:top;text-align:center"> Destination Code </th>
                            <th style="vertical-align:top;text-align:center"> Destination Name </th>
                            <th style="vertical-align:top;text-align:center"> Destination Address </th>
                            <th style="vertical-align:top;text-align:center"> Destination Phone </th>
                            <th style="vertical-align:top;text-align:center"> Destination CP </th>
                            <th style="vertical-align:top;text-align:center"> Destination Email </th>
                            <th style="vertical-align:top;text-align:center"> Destination City </th>
                            <th style="vertical-align:top;text-align:center"> Type </th>
                            <th style="vertical-align:top;text-align:center"> Status </th>
                            <th style="vertical-align:top;text-align:center"> Gate </th> 
                            <th style="vertical-align:top;text-align:center"> Action </th>
                        </tr>
                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="referensi-supplier-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Destination Code<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('destination_code','', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Destination Name<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('destination_name', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Destination Address<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('destination_address', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Destination Phone </label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('destination_phone', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Destination CP </label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('destination_contact_person', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Destination Email </label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('destination_email', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Destination City </label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('destination_city', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Type</label>
                                            <div class="controls col-md-9">
                                                    <?php
                                                        echo form_dropdown('source_category_id', $categories, '', 'class="col-md-12 form-control" style="width: 100%;" id="source_category_id" data-placeholder="--Please Select--"');
                                                    ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Gate</label>
                                            <div class="controls col-md-9">
                                              <select class="gates_id form-control" style="width:100%;" name="gates_id" id="gates_id">
                                                <option value="">--Please Select--</option>
                                                <?php
                    															foreach($gates as $key => $value){
                    																echo '<option value="'.$value['id'].'">'.$value['gate_code'].' - '.$value['gate_province'].'</option>';
                    															}
                    														?>
                                              </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* is required</span>
                                    <a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
									<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

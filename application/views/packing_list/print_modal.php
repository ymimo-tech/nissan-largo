<style type="text/css">
    .select2-container--open{
        z-index:99999;
    }
</style>
<div id="modal-print" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal-form1" class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit Weight and Volume</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-3 control-label">Weight<span class="required">*</span></label>
						<input type="number" step="0.01" id="weight_field" style="width:50%;" name="weight" placeholder="Weight" class="form-control">
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Volume<span class="required">*</span></label>
						<input type="number" step="0.01" id="volume_field" style="width:50%;" name="volume" placeholder="Volume" class="form-control">
					</div>
				</div>

				<input type="hidden" id="id_packing" name="id_packing">
				
				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-print" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-print"></i> &nbsp;Save</button>
					</div>
				</div>
			
			</form>
		</div>
	</div>
</div> 
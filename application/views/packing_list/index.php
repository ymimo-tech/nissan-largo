<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Packing Lists
				<small>List of all packed items</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
				<div class="actions">
					<a id='export-pack' class="btn blue btn-sm min-width120" target="_blank">
							<i class="fa fa-file-excel-o"></i> Export Packing </a>
				</div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

										<div class="form-group loading-group">
											<label for="loading" class="col-md-3">Loading Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="loading" id="loading">
													</select>
												</div>
												<span class="help-block loading-msg"></span>
											</div>
										</div>

										<div class="form-group pccode-group">
											<label for="pccode" class="col-md-3">Packing Number</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control" multiple="multiple"
														name="pccode" id="pccode">
													</select>
												</div>
												<span class="help-block pl-msg"></span>
											</div>
										</div>

										<div class="form-group outbound-group">
											<label for="outbound" class="col-md-3">Outbound Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="outbound" id="outbound">
													</select>
												</div>
												<span class="help-block outbound-msg"></span>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>
										<!--
										<div class="form-group doc-status-group">
											<label for="doc-status" class="col-md-3">Doc. Status</label>
											<div class="col-md-6">
												<div class="custom-select select-type4 high location">
													<select id="doc-status" class="form-control">
														<?php
															/*
															foreach($doc_status as $k => $v){
																echo '<option value="'.$k.'">'.$v.'</option>';
															}
															*/
														?>
													</select>
												</div>
												<span class="help-block doc-status-msg"></span>
											</div>
										</div>
										-->
									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
					<thead>
						<tr>
							<th style="display: none;"> id </th>
							<th style="width: 30px;"> No </th>
							<th style="vertical-align:center"> <div align="center">Packing #</div> </th>
							<th style="vertical-align:center"> <div align="center">Loading Doc #</div> </th>
							<!--th style="vertical-align:center"> <div align="center">Outbound Doc #</div> </th-->
							<th style="vertical-align:center"> <div align="center">Weight</div> </th>
							<th style="vertical-align:center"> <div align="center">Volume</div> </th>
							<th style="vertical-align:center"> <div align="center">User</div> </th>
							<th style="width: 40px;vertical-align:center"> <div align="center">Items Packed</div> </th>
							<th style="vertical-align:center"> <div align="center">Packing Time</div> </th>
							<th style="vertical-align:center"> <div align="center">Actions</div> </th>
						</tr>
					</thead>
					<tbody>

					</tbody>
                </table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<?php include('print_modal.php');?>
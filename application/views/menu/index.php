<div class="row">
    
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="custom-page-title">
            <h1><?php echo $title ?>
                <small></small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    </i><?php if(!empty($subTitle)) echo $subTitle;?>
                </div>
                <div class="actions">
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                            <thead>
                                <tr>
                                    <th style="width: 50px;"><i class="icon-sitemap"></i></th>
                                    <th>Nama Menu</th>
                                    <th>Grup Menu</th>
                                    <th>Dekripsi</th>
                                    <th>URL</th>
                                    <th>Urutan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
                <div id="form-content" class="modal fade" role="basic">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form id="data-table-form" class="form-horizontal">
                                <input type="hidden" name="id"/>
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Menu name<span class="required">*</span></label>
                                                <div class="controls col-md-9">
                                                    <?php echo form_input('nama_menu','', 'class="col-md-12 form-control"') ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">URL<span class="required">*</span></label>
                                                <div class="controls col-md-9">
                                                    <?php echo form_input('url','', 'class="col-md-12 form-control"') ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description </label>
                                                <div class="controls col-md-9">
                                                    <?php echo form_textarea('deskripsi','', 'class="col-md-12 form-control"') ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Menu Group<span class="required">*</span></label>
                                                <div class="controls col-md-9">
                                                    <?php echo form_dropdown('kd_grup_menu', $menu_group, '', 'data-live-search="true" class="selectpicker col-md-12 form-control"') ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Parent Menu </label>
                                                <div class="controls col-md-9">
                                                    <?php echo form_dropdown('kd_parent', array(), '', 'data-live-search="true" class="selectpicker col-md-12 form-control"') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-actions">
                                        <span class="required pull-left">* harus diisi</span>
                                        <button type="submit" class="btn blue"><i class="icon-save"></i> Simpan</button>
                                        <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Kembali</a>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>  
            <?php }?>
        </div>
    </div>
        <!-- END EXAMPLE TABLE PORTLET-->
</div>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                    $hidden_form = array('id' => !empty($id) ? $id : '');
                    echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama Menu<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('nama_menu', !empty($default->nama_menu) ? $default->nama_menu : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">URL<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('url', !empty($default->url) ? $default->url : '', 'class="col-md-12 form-control"') ?>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Deskripsi<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_supplier', !empty($default->deskripsi) ? $default->deskripsi : '', 'class="col-md-12 form-control"') ?> 
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Group Menu<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_dropdown('group_menu',$menu_group_options, !empty($default->kd_grup_menu) ? $default->kd_grup_menu : '', 'class="col-md-12 form-control" id="group_menu"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Parent<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_dropdown('parent',array('' => 'NULL'), !empty($default->parent) ? $default->parent : '', 'class="col-md-12 form-control" data-source="' . $parent_source . '" id="parent"') ?>
                            
                        </div>
                    </div> 
                    <span class="required">* harus diisi</span>
                    
                    
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                   <?php 
                    if(!empty($st_delete)){
                        echo anchor(null, '<i class="icon-trash"></i> Hapus', array('id' => 'button-save', 'class' => 'red btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')"));
                    }else{
                        echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')"));
                    } ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        get_options('#parent', {kode: '<?php echo!empty($default->kd_grup_menu) ? $default->kd_grup_menu : ''; ?>', selected: '<?php echo!empty($default->kd_parent) ? $default->kd_parent : ''; ?>'}, true);
        $('#group_menu').change(function() {
            var id = $(this).val();
            var selected = '';
            get_options('#parent', {kode: id, selected: selected}, true);
        });
    });
    
</script>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>CREATE BIN TRANSFER JOB
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i><?php echo $title; ?>
					</span>
				</div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body form">
				<div class="form-body">
					<form id="data-table-form" class="form-horizontal">
						<input type="hidden" name="id" value="<?php echo (isset($id)) ? $id : ''; ?>" />
						<div class="row">
							<div class="col-md-12">

							    <div class="form-group">
									<label class="col-md-2">Bin Transfer Doc. #<span class="required">*</span></label>
									<div class="controls col-md-6">
										<div style="">
											<input type="hidden" name="bin_transfer_code" id="bin_transfer_code" class="form-control" placeholder="No. Bin Transfer Document" />
											<strong><span id="bin_transfer_code_text"></span></strong>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2"> Warehouse <span class="required">*</span></label>
									<div class="controls col-md-6">
										<?php
                                            echo form_dropdown('warehouse', $warehouses, '', 'class="col-md-12 form-control" style="width: 100%;" id="warehouse" data-placeholder="--Please Select--"');
										?>
									</div>
								</div>

							    <div class="form-group">
								   <label class="col-md-2">Remark</label>
								   <div class="controls col-md-6">
									   <div style="">
										   <textarea id="remark" name="remark" class="form-control" value=""></textarea>
									   </div>
								   </div>
							    </div>

							</div>
						</div>

						<hr>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped table-bordered table-hover table-checkable order-column" id="inbound-barang">
									<thead>
										<tr>
											<th> No </th>
											<th> Item Name </th>
											<th style="text-align: center; width: 200px;"> Quantity </th>
                                            <th style="text-align: center; width: 200px;"> UoM </th>
											<th style="text-align: center; display: none;"> Batch </th>
											<th style="text-align: center"> Action </th></th>
										</tr>
									</thead>
									<tbody id="added_rows">
									</tbody>
									<tbody>
										<tr>
                                            <td colspan="5" class="add_item">
												<button type="button" class="btn blue min-width120 add-item"
													<?php if(empty($id)) { ?>data-toggle="tooltip" title="Click here to add item" data-placement="bottom"<?php } ?>><i class="fa fa-plus"></i> Add Item</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<span class="required pull-left">* is required</span>
									<div class="pull-right">

										<?php if(empty($id)) { ?>
											<a class="btn default min-width120" href="<?php echo base_url().$page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
										<?php } else { ?>
											<a class="btn default min-width120" href="javascript:;" onClick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
										<?php } ?>

										<?php if($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')){ ?>
											<button id="save" type="submit" class="btn blue min-width120"> SAVE</button>
										<?php } ?>

									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

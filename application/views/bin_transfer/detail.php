<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Bin Transfer Job Detail
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i>
						<?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<div class="top-info">
						<label>For Warehouse : </label>
						<?php echo $data['warehouse_name']; ?>
						<label>Date : </label>
						<span class=""><?php echo $data['created_at']; ?></span>
						<label>Created by : </label>
						<span class=""><?php echo !empty($data['user_name']) ? $data['user_name'] : '-'; ?></span>
					</div>

                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="params" value="" />

						<div class="row">
							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Bin Transfer Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['bin_transfer_code']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['bin_transfer_code']; ?>
									</div>
								</div>
							</div>
							<div class="col-md-4">
<!-- 								<div class="detail-caption text-left">
									<h1>From<h1>
									<div class="bottom5 sup-title">
										<strong><?php echo $data['warehouse_name']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td>
												<?php echo $data['warehouse_address']; ?><br/>
											</td>
										</tr>
									</table>
								</div>
 -->							</div>

							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Bin Transfer Status</h1>
									<div class="detail-status
										<?php
											if($data['status_name'] == 'Open' || $data['status_name'] == 'In Progress')
												echo 'status-blue';
											else
												echo 'status-green';
										?>">
										<?php echo $data['status_name']; ?>
									</div>
								</div>
							</div>
						</div>

						<hr style="margin-top: 0;">

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Ordered Items
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id </th>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Item Code</div> </th>
											<th> Item Name </th>
											<th style="width: 120px;"> <div align="center">Ordered</div> </th>
											<th style="width: 120px;"> <div align="center">Picked</div> </th>
											<th> <div align="center">Action</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>


					</div>
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">

								<div class="pull-right">
									<a class="btn default min-width120" href="javascript:;" onclick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
									<?php if ($this->access_right->otoritas('edit')) : ?>

										<?php if (!$data['bin_transfer_start']) : ?>
											<a href="<?php echo base_url() . $page_class; ?>/edit/<?php echo $id; ?>" class="btn blue min-width120"><i class="fa fa-pencil"></i> EDIT</a>
										<?php endif; ?>

									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

				</form>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

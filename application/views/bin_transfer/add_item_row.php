<tr class="added_row">
	<td>#</td>
	<td class="form-group">
		<?php echo form_dropdown("id_barang[]", $items, isset($data) ? $data['item_id'] : "", "class='form-control add_rule item-outbound'"); ?>
	</td>
	<td class="form-group">
		<div class="pull-right">
			<?php echo form_input("item_quantity[]", isset($data) ? $data['quantity'] : "", 'class="col-md-12 form-control add_rule" style="width: 100px;"'); ?>
		</div>
	</td>
    <td class="form-group">
        <div class="pull-right">
            <select name="unit[]" class="form-control uom">
                <?php
	                if(isset($data)){
                        echo "<option value='".$data['unit_id']."'>".$data['unit_code']."</option>";
	                }
                ?>
            </select>
        </div>
    </td>
	<td class="remove_row">
		<button class="btn btn-delete btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
	</td>
</tr>
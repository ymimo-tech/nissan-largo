<style type="text/css">
    .select2-container--open{
        z-index:99999;
    }
</style>
<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>OUTBOUND DOCUMENT TYPES
				<small>List of outbound document types for used when creating Outbound Documents</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-cogs"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a  class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>
					<!--
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <!--th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th-->
                            <th> No </th>
                            <th> Name  </th>
                            <?php
                                if($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
                                    echo "<th> Actions </th>";
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="data-table-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">

<!-- Movement Type - GOJEK -->
<!--                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Movement Type<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('movement_type','', 'class="col-md-12 form-control"') ?>
                                            </div>
                                        </div>
 -->
<!-- End - Movement Type - GOJEK -->

                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Out Doc Type Name<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('outbound_document_name','', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Input Document Code</label>
                                            <div class="controls col-md-9">

                                                <?php
                                                    echo form_dropdown('auto_generate', [0=>'Manual', 1=>'Auto Generate'], '', 'class="col-md-12 form-control" style="width: 100%;" id="auto_generate"');

                                                ?>
                                            </div>
                                        </div>

                                    <div id="auto_generate_form">

                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Out Docs Type Name<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('outbound_document_desc','', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Out Doc Type Initial<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('outbound_document_code','', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Separator<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <input type="text" name="separator" maxlength="1" class="col-md-12 form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Sequence<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <input type="number" name="sequence" min="2" max="6" value="2" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="col-md-6 control-label">Year</label>
                                            <div class="controls col-md-6">

                                                <?php
                                                    $year = array(
                                                        0=>"None",
                                                        1=>"YY",
                                                        2=>"YYYY"
                                                    );

                                                    echo form_dropdown('year', $year, '', 'class="col-md-12 form-control" style="width: 100%;" id="year"');
                                                ?>

                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label class="col-md-6 control-label">Month</label>
                                            <div class="controls col-md-6">

                                                <?php
                                                    $month = array(
                                                        0=>"None",
                                                        1=>"MM"
                                                    );

                                                    echo form_dropdown('month', $month, '', 'class="col-md-12 form-control" style="width: 100%;" id="month"');
                                                ?>
                                            </div>
                                        </div>

                                    </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Destination Type</label>
                                            <div class="controls col-md-9">

                                                <?php

                                                    echo form_dropdown('source_type', $source_type, '', 'class="col-md-12 form-control" style="width: 100%;" id="source_type"');

                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Warehouse</label>
                                            <div class="controls col-md-9">
                                                <?php
                                                    echo form_dropdown('warehouse[]', $warehouses, '', 'class="col-md-12 form-control" style="width: 100%;" id="warehouse" multiple data-placeholder="--Please Select--"');
                                                ?>
                                            </div>
                                        </div>

<!-- Cost Center - GOJEK -->
<!--                                         <div class="form-group">
                                            <label class="col-md-3 control-label">Cost Center</label>
                                            <div class="controls col-md-9">
                                                <?php
                                                    echo form_dropdown('cost_center[]', $cost_center, '', 'class="col-md-12 form-control" style="width: 100%;" id="cost_center" multiple data-placeholder="--Please Select--"');
                                                ?>
                                            </div>
                                        </div>
 -->
<!-- Cost Center - GOJEK -->


                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Status</label>
                                            <div class="controls col-md-9">

                                                <?php
                                                    $status = array(
                                                        0=>"Inactive",
                                                        1=>"Active"
                                                    );

                                                    echo form_dropdown('status', $status, '', 'class="col-md-12 form-control" style="width: 100%;" id="stat"');
                                                ?>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* is required</span>
                                    <a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
									<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<style>
	.filter-container .select2-container{
		width: 100% !important;
		z-index: 9;
	}

	.select2-container{
		z-index: 100000;
	}
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Kitting
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a  class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>

					<?php if ($this->access_right->otoritas('print')) : ?>

						<a  class="btn green btn-sm data-table-export min-width120">
							<i class="fa fa-file-excel-o"></i> EXPORT </a>

					<?php endif; ?>
					<!--
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

										<div class="form-group item-group">
											<label for="item" class="col-md-4">Kitting</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<?php echo form_dropdown('kitting', $kitting, '', 'class="form-control" style="width: 100%;" id="kitting"'); ?>
												</div>
												<span class="help-block item-msg"></span>
											</div>
										</div>


									</div>

									<div class="col-md-6">



									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <!--th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th-->
                            <th> No </th>
                            <th> Kitting Code </th>
                            <th> Kitting Name </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form id="referensi-kitting-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Item<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_dropdown('id_barang_main', $item, '', 'class="form-control selectpicker" style="width: 100%;"'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="controls col-md-12">
												<table class="table table-bordered table-hover table-striped table-condensed">
													<thead>
														<tr>
															<th>Item Part</th>
															<th style="width:100px" class="text-right">Qty</th>
															<th style="width:100px">Action</th>
														</tr>
													</thead>
													<tbody class="kitting_product">
													</tbody>
													<tbody>
														<tr><td class="text-center" colspan="3"><button type="button" class="btn blue min-width120 add_item" title="" data-placement="bottom" data-original-title="Click here to add item"><i class="fa fa-plus"></i> Add Item</button></td></tr>
													</tbody>
												</table>
                                            </div>
                                        </div>
										<div class="for_del_container" style="display:none">

										</div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* is required</span>
                                    <a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
									<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>PUTAWAY LOGS
				<small>List of all performed putaway (or put-to-bin) transactions</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
                    <a  class="btn btn-default btn-sm gr">
                        <i class="fa fa-plus"></i> Download GR</a>
					<!--
                    <a href="javascript:;" class="btn btn-default btn-sm btn-receiving">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">
								<input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />

								<div class="row">
									<div class="col-md-6">

										<div class="form-group rcv-group">
											<label for="rcv" class="col-md-3">Rcv. Number</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="rcv" id="rcv">
													</select>
												</div>
												<span class="help-block rcv-msg"></span>
											</div>
										</div>

										<div class="form-group inbound-group">
											<label for="inbound" class="col-md-3">Doc. Number</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="inbound" id="inbound">
													</select>
												</div>
												<span class="help-block inbound-msg"></span>
											</div>
										</div>

										<div class="form-group inbound-group">
											<label for="inbound" class="col-md-3">Status GR</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="gr" id="gr">
														<option value="" disabled selected>Select your option</option>
														<option value='1'>Success</option>
														<option value='2'>Failed</option>
														<!-- <option value='3'>No Response</option> -->
													</select>
												</div>
												<span class="help-block inbound-msg"></span>
											</div>
										</div>

										<!--div class="form-group supplier-group">
											<label for="supplier" class="col-md-3">Supplier</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="supplier" class="form-control">
													<?php
														foreach($suppliers as $k => $v){
															echo '<option value="'.$k.'">'.$v.'</option>';
														}
													?>
													</select>
												</div>
												<span class="help-block supplier-msg"></span>
											</div>
										</div-->

									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Periode</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Location</label>
											<div class="col-md-6">

												<select id="location" class="form-control">
												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table-list">
                    <thead>
                        <tr>
                            <th> No </th>
                            <th> Doc. Number </th>
                            <th> Rcv. Number </th>
                            <th> Rcv. Date </th>
                            <th> LPN (License Plate #)</th>
														<th style="text-align:center"> Item Code </th>
                            <th> Status GR </th>
														<th style="text-align:center"> Location </th>
                        </tr>
                    </thead>
                    <tbody>
					</tbody>
                </table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row">
	
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Peminjaman List
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div id="portlet-light" class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?> 
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('print')) : ?>
                                
						<a href="javascript:;" id="button-export" class="btn green btn-sm btn-export min-width120" 0=""><i class="fa fa-file-pdf-o"></i> &nbsp;EXPORT</a>
							
					<?php endif; ?>
					<!--
                    <a class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
			
				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">	
								
								<div class="row">
									<div class="col-md-6">
										<div class="form-group customer-group">
											<label for="customer" class="col-md-3">Customer</label>
											<div class="col-md-8">

												<select id="customer" class="form-control">
													<?php 
														echo '<option value="">-- All --</option>';
														foreach($customer as $r){
															echo '<option value="'.$r['id_customer'].'">'.$r['customer_name'].'</option>';
														}
													?>
												</select>
											
												<span class="help-block customer-msg"></span>
											</div>
										</div>
									</div>
									
									<div class="col-md-6">
										<!--
										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">
													
												</select>
											
												<span class="help-block year-msg"></span>
											</div>
										</div>
									
										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>" 
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group --
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>
										-->
									</div>	
								</div>
								
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->
			
                <table id="table-list" class="table table-striped table-bordered table-hover report-table dataTable no-footer">
					<thead>
						<tr class="uppercase">
							<th style="text-align: center;"> No </th>
							<th style="text-align: center;"> Destination </th>
							<th style="text-align: center;"> On Loan </th>
							<th style="text-align: center;"> Return </th>
						</tr>
					</thead>
					<tbody>
					</tbody>
                </table>
            </div>
			<div>&nbsp;</div>			
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div> 
<html>
<head>
	<title>Peminjaman Report</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, .table-pdf tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
		.table-pdf tbody tr td.header-report-table{
			background: #fafafa;
			padding: 10px;
			font-weight: bold;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Peminjaman Report</h3>
	<hr>
	<br /><br />
	<!-- begin table -->
	<table id="table-pdf" class="table report-table table-pdf">
		<thead>
			<tr>
				<th style="width: 30px;">No</th>
				<th>Destination</th>
				<th>On Loan</th>
				<th>Return</th>
			</tr>
		</thead>
		<tbody>
			
			<?php
				
				$data = $data;
				$len = count($data);

				$tpl = '<tr>
							<td style="text-align: center;">{{ no }}</td>
							<td style="text-align: left;">{{ destination }}</td>
							<td style="text-align: right;">{{ in }}</td>
							<td style="text-align: right;">{{ out }}</td>
					   </tr>';
				$row = ''; 
				
				if($len > 0){
					
					for($i = 0; $i < $len; $i++){
						$row .= $tpl;
						$row = str_replace('{{ no }}', ($i + 1), $row);
						$row = str_replace('{{ destination }}', $data[$i]['customer_name'], $row);
						$row = str_replace('{{ in }}', $data[$i]['stock'] . ' ' . $data[$i]['in_number'], $row);
						$row = str_replace('{{ out }}', $data[$i]['min'] . ' ' . $data[$i]['out_number'], $row);
					}
					
					echo $row;
					
				}else{
					
					echo '<tr><td colspan="4" style="text-align: center;">No data</td></tr>';
					
				}
			?>
			
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                    $hidden_form = array('id' => !empty($id) ? $id : '');
                    echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Supplier Code<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_supplier', !empty($default->kd_supplier) ? $default->kd_supplier : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Supplier Name<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('nama_supplier', !empty($default->nama_supplier) ? $default->nama_supplier : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Supplier Address<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('alamat_supplier', !empty($default->alamat_supplier) ? $default->alamat_supplier : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Supplier Phone<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('telpon_supplier', !empty($default->telpon_supplier) ? $default->telpon_supplier : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Supplier CP<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('cp_supplier', !empty($default->cp_supplier) ? $default->cp_supplier : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Supplier Email<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('email_supplier', !empty($default->email_supplier) ? $default->email_supplier : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 

                    <span class="required">* harus diisi</span>
                    
                    
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
       /* $('.chosen').chosen();*/
        /*$('.numeric').numeric();*/
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

<html>
<head>
	<title>Inventory Transfer</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, .table-pdf tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
		.table-pdf tbody tr td.header-report-table{
			background: #fafafa;
			padding: 10px;
			font-weight: bold;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Inventory Transfer Report</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 400px; vertical-align: top;">
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Items</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['item_name']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Location</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['loc_name']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
					<td style="vertical-align:top;">
						
						<table class="bottom10">
							<tr>
								<td valign="top" class="width120">
									<label>From</label>
								</td>
								<td valign="top" class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['from']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>To</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $params['to']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<br /><br />
	<!-- begin table -->
	<table id="table-pdf" class="table report-table table-pdf">
		<thead>
			<tr>
				<th rowspan="2" style="width: 30px;vertical-align: middle; text-align: center;">No</th>
				<th rowspan="2" style="vertical-align: middle; text-align: center;">Item Code</th>
				<th rowspan="2" style="vertical-align: middle; text-align: center;">Item Name</th>
				<th rowspan="2" style="vertical-align: middle; text-align: center;">Serial Number</th>
				<th colspan="3" style="text-align: center;">From</th>
				<th colspan="3" style="text-align: center;">To</th>
			</tr>
			<tr>
				<th style="text-align: center;">Location</th>
				<th style="text-align: center;">By</th>
				<th style="text-align: center;">Time</th>
				<th style="text-align: center;">Location</th>
				<th style="text-align: center;">By</th>
				<th style="text-align: center;">Time</th>
			</tr>
		</thead>
		<tbody>
			
			<?php
				
				$data = $data;
				$len = count($data);

				$tpl = '<tr>
							<td style="text-align: center;">{{ no }}</td>
							<td style="text-align: center;">{{ code }}</td>
							<td>{{ item }}</td>
							<td style="text-align: center;">{{ sn }}</td>
							<td style="text-align: center;">{{ loc_from }}</td>
							<td style="text-align: center;">{{ pick_by }}</td>
							<td style="text-align: center;">{{ pick_time }}</td>
							<td style="text-align: center;">{{ loc_to }}</td>
							<td style="text-align: center;">{{ put_by }}</td>
							<td style="text-align: center;">{{ put_time }}</td>
					   </tr>';
				$row = ''; 
				
				if($len > 0){
					
					for($i = 0; $i < $len; $i++){
						$row .= $tpl;
						$row = str_replace('{{ no }}', ($i + 1), $row);
						$row = str_replace('{{ code }}', $data[$i]['kd_barang'], $row);
						$row = str_replace('{{ item }}', $data[$i]['nama_barang'], $row);
						$row = str_replace('{{ sn }}', $data[$i]['kd_unik'], $row);
						$row = str_replace('{{ loc_from }}', $data[$i]['loc_from'], $row);
						$row = str_replace('{{ pick_by }}', $data[$i]['pick_by'], $row);
						$row = str_replace('{{ pick_time }}', $data[$i]['pick_time'], $row);
						$row = str_replace('{{ loc_to }}', $data[$i]['loc_to'], $row);
						$row = str_replace('{{ put_by }}', $data[$i]['put_by'], $row);
						$row = str_replace('{{ put_time }}', $data[$i]['put_time'], $row);
					}
					
					echo $row;
					
				}else{
					
					echo '<tr><td colspan="10" style="text-align: center;">No data</td></tr>';
					
				}
			?>
			
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>
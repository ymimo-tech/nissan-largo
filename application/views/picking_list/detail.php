<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>PICKING DETAIL
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i>
						<?php echo $title;?>
					</span>
				</div>

        <div class="actions">
					<div class="top-info">
						<label>Date : </label>
						<span class=""><?php echo $data['pl_date']; ?></span>
						<label>Created by : </label>
						<span class=""><?php echo !empty($data['user_name']) ? $data['user_name'] : '-'; ?></span>
					</div>
					<!--
					<table class="table-info">
						<tr>
							<td><strong>Date</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Outbound Doc #</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Created by</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
					</table>
					-->
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="id_outbound" value="<?php echo $data['id_outbound']; ?>" />
						<input type="hidden" name="params" value="" />

						<div class="row">
							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Picking Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['pl_name']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['pl_name']; ?>
									</div>
								</div>
							</div>

							<div class="col-md-5 col-sm-5">
								<div class="detail-caption text-left">
									<table>
										<tr>
											<td style="width: 40%;">Outbound Doc. #</td>
											<td style="width: 10%;" align="center">:</td>
											<td>
												<?php
												foreach($otbs as $otb){
												?>
												<strong><?php echo $otb['kd_outbound']; ?> - <?php echo $otb['DestinationName']; ?></strong><br/>
												<?php }?>
											</td>
										</tr>
										<!--<tr>
											<td>Customer Name</td>
											<td style="width: 10%;" align="center">:</td>
											<td><?php echo $data['DestinationName']; ?></td>
										</tr>-->
										<tr>
											<td>Start Picking</td>
											<td align="center">:</td>
											<td><?php echo $data['pl_start']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Finish Picking</td>
											<td align="center">:</td>
											<td><?php echo $data['pl_finish']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">From Warehouse</td>
											<td align="center">:</td>
											<td><?php echo $data['warehouse_name']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Destination</td>
											<td align="center">:</td>
											<td><?php
												if(count($otbs) > 1){
													echo "Consolidate";
												}else{
													if(!empty($data['warehouse_name_to'])){
														echo $data['warehouse_name_to'] .' - '.$data['destination_name'];
													}else{
														echo $data['destination_name'];
													}
												}
											?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Picking Status</h1>
									<div class="detail-status status-blue" style="font-size: 26px;"><?php echo $data['nama_status']; ?></div>
								</div>
							</div>
						</div>

						<hr style="margin-top: 0;">
						<!--
						<div class="row">
							<div class="col-md-6">
								<h4>Items</h4>
							</div>
							<div class="col-md-6">
								<div class="pull-right">
									<table>
										<tr>
											<td>Total Item</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-item">0</span></strong></td>
										</tr>
										<tr>
											<td>Total Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-qty">0</span></strong></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						-->

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Picking Report
							</span>
							<a href="javascript:;" id="button-export-excel" class="btn blue btn-sm btn-export min-width120 pull-right" data-id="<?php echo $id; ?>"><i class="fa fa-file-excel-o"></i> &nbsp;EXPORT EXCEL</a>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id </th>
											<th style="display: none;"> pl_id </th>
											<th style="display: none;"> id_barang </th>
											<th style="vertical-align:top"> No </th>
											<th style="vertical-align:top"> <div align="center">Item Code</div> </th>
											<th style="vertical-align:top"> Item Name </th>
											<th style="vertical-align:top"> <div align="center">Doc. Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Available Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Previous Pick</div> </th>
											<th style="vertical-align:top"> <div align="center">Remaining Qty</div> </th>
											<th style="vertical-align:top;width: 120px;"> <div align="center">Picked Qty</div> </th>
											<th style="vertical-align:top;display: none;"> <div align="center">Batch</div> </th>
											<th style="vertical-align:top"> <div align="center">Scanned Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Action</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

					</div>
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">

								<div class="pull-right">
									<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>

									 <?php
									 $class = '';
									 if($data['pl_start'] != '-'){
										$class = 'disabled-link';
									 }
									 ?>

									 <?php if ($this->access_right->otoritas('edit')) : ?>

										<?php if ($not_usage) : ?>

										<?php endif; ?>

										<?php if(empty($class)){ ?>
											<a href="<?php echo empty($class) ? base_url() . $page_class . '/edit/' . $id : 'javascript:;'; ?>" class="btn blue min-width120 <?php echo $class; ?>"><i class="fa fa-pencil"></i> EDIT</a>
										<?php } ?>

									 <?php endif; ?>

									 <?php if ($this->access_right->otoritas('print')) : ?>

										<?php if(empty($class)){ ?>
											<button id="button-print-picking" class="btn default min-width120 <?php echo $class; ?>" data-id="<?php echo $id; ?>"><i class="fa fa-print"></i> PRINT PICKING</a>
										<?php } ?>

									 <?php endif; ?>
								</div>
							</div>
						</div>
					</div>

				</form>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?php include('print_modal_sub.php'); ?>
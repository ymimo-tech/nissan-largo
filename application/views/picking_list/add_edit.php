<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Picking List
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i><?php echo $title; ?>
					</span>
				</div>
                <div class="actions">
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<div class="form-body">
					<form id="data-table-form" class="form-horizontal">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="id_outbound" value="<?php echo $id_outbound; ?>" />


						<div class="row">
							<div class="col-md-12">
							   <div class="form-group">
									<label class="col-md-2">Picking Number<span class="required">*</span></label>
									<div class="controls col-md-6">
										<div style="">
											<input type="hidden" id="pl-name" name="pl_name" class="form-control" value="<?php echo $pl_name; ?>" />
											<input type="hidden" id="params" name="params" />
											<strong><span class="pl-name"><?php echo $pl_name; ?></span></strong>
										</div>
									</div>
								</div>
							   <div class="form-group tanggal-picking-group">
									<label class="col-md-2">Picking Date<span class="required">*</span></label>
									<div class="col-md-6">
										<div class="input-group input-medium date date-picker">
											<input type="text" class="form-control" id="tanggal-picking" name="tanggal_picking" placeholder="dd/mm/yyyy"
												value="<?php echo $today; ?>" maxLength="10" />
											<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>
							   </div>
							   <div class="form-group">
									<label class="col-md-2">Outbound Doc #<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select id="id-outbound" name="id_outbound" class="form-control">
										<option value="">-- Choose Outbound Document --</option>
										<?php
											foreach($doc_number as $row){
												$selected='';
												if(in_array($row['id_outbound'],$otbs)){
													$selected=' selected';
												}
												echo '<option value="'.$row['id_outbound'].'"'.$selected.'>'.$row['kd_outbound'].'</option>';
											}
										?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Picking Area<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select id="id-area" name="id_outbound" class="form-control">
										<option value="">-- Choose Picking Area --</option>
										<?php
											foreach($picking_areas as $row){
												$selected='';
												if(in_array($row['item_area'],$pickas)){
													$selected=' selected';
												}
												echo '<option value="'.$row['item_area'].'"'.$selected.'>'.$row['item_area'].'</option>';
											}
										?>
										</select>
									</div>
								</div>

							</div>
						</div>

						<hr>
						<!--<h4>Items</h4>-->
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped table-bordered table-hover table-checkable order-column" id="table-list">
									<thead>
										<tr>
											<th> No </th>
											<th> <div align="center">Item Code</div> </th>
											<th> Item Name </th>
											<th> Item Area </th>
											<th> Outbound Doc. </th>
											<th> <div align="center">Doc. Qty</div> </th>
											<th> <div align="center">Available Qty</div> </th>
											<th> <div align="center">Previous Pick</div> </th>
											<th style="width: 200px;"> <div align="center">Pick Qty</div> </th>
											<th style="display: none;"> <div align="center">Batch</div> </th>
											<th> <div align="center">Remaining Qty</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<span class="required pull-left">* is required</span>
									<div class="pull-right">

										<a class="btn default min-width120" href="javascript:;" onClick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>

										<?php if($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')){ ?>

											<?php
												$class = '';
												if($data['pl_start'] != '-'){
													$class = 'disabled-link';
												}
											?>

											<button id="save" type="submit" <?php if(!empty($class)) echo 'disabled'; ?> class="btn blue min-width120 <?php echo $class; ?>"> SAVE</button>

											<?php if(empty($id)){ ?>
												<button id="save-new" type="button" class="btn green-haze min-width120"> SAVE & NEW</button>
											<?php } ?>

										<?php } ?>

									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

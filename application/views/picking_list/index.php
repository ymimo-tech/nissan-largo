<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>PICKING JOBS
				<small>List of all the picking jobs for outgoing items/materials</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row widget-row">
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">TODAY'S PICKING</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-green icon-bulb"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle"><?php echo $widget['total_item']; ?> Items</span>
					<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty']; ?>">
					<?php echo $widget['total_qty']; ?></span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">READY FOR PICK</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-red icon-layers"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle"><?php echo $widget['total_picking']; ?> Items</span>
					<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty_picking']; ?>">
					<?php echo $widget['total_qty_picking']; ?></span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">READY FOR LOADING</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle"><?php echo $widget['total_loading']; ?> Items</span>
					<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty_loading']; ?>">
					<?php echo $widget['total_qty_loading']; ?></span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">AVG. PICK TO LOAD</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle">Time/Item</span>
					<span class="widget-thumb-body-stat" data-counter="" data-value="<?php echo $widget['average']; ?>">
					<?php if(!empty($widget['average'])) echo $widget['average']; else echo '0'; ?></span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>

						<?php echo hgenerator::render_button_group($button_group); ?>

					<?php endif; ?>
					<!-- <a  href="<?php echo base_url() . 'picking_list/add'?>" id="button-add" class="btn blue btn-sm min-width120" data-source="<?php echo base_url() . 'picking_list/add'?>">
						<i class="fa fa-file-o"></i> &nbsp;NEW </a> -->

						<a href="javascript:;" class="btn green btn-sm btn-print min-width120">
							<i class="fa fa-print"></i> PRINT </a>
						<!-- 
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-up"></i> Create Picking </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

										<div class="form-group pl-group">
											<label for="pl" class="col-md-4">Picking Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="pl" id="pl">
													</select>
												</div>
												<span class="help-block pl-msg"></span>
											</div>
										</div>

										<div class="form-group do-group">
											<label for="do" class="col-md-4">Outbound Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="do" id="do">
													</select>
												</div>
												<span class="help-block do-msg"></span>
											</div>
										</div>

										<div class="form-group supplier-group">
											<label for="supplier" class="col-md-4">Destination</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="customer" class="form-control">
													</select>
												</div>
												<span class="help-block supplier-msg"></span>
											</div>
										</div>

										<div class="form-group supplier-group">
											<label for="supplier" class="col-md-4">Area</label>
											<div class="col-md-8">
												<input type="text" name="area" id="area" class="form-control">
												<span class="help-block supplier-msg"></span>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>
										

										<div class="form-group status-group">
											<label for="status" class="col-md-3">Doc. Status</label>
											<div class="col-md-8">

												<select id="status" class="form-control" style="width: 100%;" multiple data-placeholder="-- All --">
													<?php
														echo '<option value="">-- All --</option>';

														foreach($status as $row){
															echo '<option value="'.$row['id_status_picking'].'">'.$row['nama_status'].'</option>';
														}
													?>
													<option value='3'>OPEN</option>
												</select>

												<span class="help-block status-msg"></span>
											</div>
										</div>

										<div class="form-group status-group">
											<label for="status" class="col-md-3">Gate</label>
											<div class="col-md-8">

												<select id="gate" class="form-control" style="width: 100%;" data-placeholder="-- All --">
													
												</select>

												<span class="help-block status-msg"></span>
											</div>
										</div>


										<!--
										<div class="form-group doc-status-group">
											<label for="doc-status" class="col-md-3">Doc. Status</label>
											<div class="col-md-6">
												<div class="custom-select select-type4 high location">
													<select id="doc-status" class="form-control">
														<?php
															/*
															foreach($doc_status as $k => $v){
																echo '<option value="'.$k.'">'.$v.'</option>';
															}
															*/
														?>
													</select>
												</div>
												<span class="help-block doc-status-msg"></span>
											</div>
										</div>
										-->
									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
					<thead>
						<tr>
							<th style="display: none;"> id_picking </th>
							<th style="text-align:center" class="table-checkbox" width="10px">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
							<th style="vertical-align: top;width: 30px;"> No </th>
							<th style="vertical-align: top;"> <div align="center">Picking Doc. #</div> </th>
							<th style="vertical-align: top;"> <div align="center">Outbound Doc. #</div> </th>
							<th style="vertical-align: top;"> <div align="center">Picking Date</div> </th>
							<th style="vertical-align: top;"> <div align="center">Destination</div> </th>
							<th style="vertical-align: top;"> <div align="center">Gate</div> </th>
							<th style="vertical-align: top;"> <div align="center">Start</div> </th>
							<th style="vertical-align: top;"> <div align="center">Finish</div> </th>
							<th style="vertical-align: top;"> <div align="center">Priority</div> </th>
							<th style="vertical-align: top;"> <div align="center">Status</div> </th>
							<th style="vertical-align: top;"> <div align="center">Actions</div> </th>
						</tr>
					</thead>
					<tbody>

					</tbody>
                </table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?php if ($this->access_right->otoritas('print')) { ?>
	<?php include 'print_modal.php'; ?>
<?php }?>

<div id="modal-sub" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal-form" class="form-horizontal">
				<input type="hidden" name="id"/>
				<input type="hidden" id="sid" name="sid" value="<?php echo session_id(); ?>" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Choose Item Subtitute</h4>
				</div>
				<div class="modal-body">
					<input type="hidden" name="temp_qty" id="temp_qty"/>
					<table class='table sub'>
						<thead>
							<th>Item</th>
							<th>Qty</th>
							<th>Location</th>
							<th>Action</th>
						</thead>
						<tbody id="table-sub-id">
						</tbody>
					</table>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<?php if($data['nama_status'] == 'Open'){
							echo '<button id="submit-sub" type="submit" class="btn blue min-width120" data-loading-text="Loading...">&nbsp;Save</button>';
						}
						?>
					</div>
				</div>
			
			</form>
		</div>
	</div>
</div> 
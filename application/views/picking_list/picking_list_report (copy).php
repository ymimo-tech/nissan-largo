<html>
<head>
	<title>Picking List</title>
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}

		.table-pdf tr td{
			font-size: 13px;
		}

		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Picking List</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>Picking Doc. #</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>/barcode?text=<?php echo $pick['pl_name']; ?>&size=65" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $pick['pl_name']; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Outbound Doc. #</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $pick['kd_outbound']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Picking List Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $pick['pl_date']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>From</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $pick['warehouse_from']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>To</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php 
											if(!empty($pick['warehouse_to'])){
												echo $pick['warehouse_to'] .' - '.$pick['destination_name']; 
											}else{
												echo $pick['destination_name']; 
											}
										?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th style="width: 50%;text-align: left;"> Location </th>
				<th style="width: 10%;text-align: left;"> QTY </th>
				<th style="width: 30%;text-align: left;"> Item Name/SKU </th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i = 1;
				foreach($items['data'] as $row){

					if(!empty($row['locations'])){

						$locEx = explode(",", $row['locations']);
						$locations ="";

						$i=0;
						foreach ($locEx as $loc) {
							if($i < 3){
								$locations .= $loc.", ";
								$i++;
							}else{
								$locations .= $loc."<br/>";
								$i=0;
							}
						}

					}
	
					$str = '<tr>
								<td> '.$locations.' </td> 
								<td> '.$row['qty'].' '.$row['nama_satuan'].' </td> 
								<td {{ align }}>
									<label>'.$row['nama_barang'].'</label>
									<br>
									<img style="margin-top: 5px;" alt="" src="'.base_url().'/barcode?text='.$row['kd_barang'].'&size=35">
								</td>
							</tr>';
							
					if(($i % 2) == 0){
						$str = str_replace('{{ align }}', 'align="right"', $str);
					}
					
					echo $str;
					$i++;
				}
				
			?>
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>

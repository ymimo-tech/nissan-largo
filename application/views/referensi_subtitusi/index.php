<style type="text/css">
    .select2-container--open{
        z-index:99999;
    }
</style>
<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Items Subtitute
				<small>List of Items Subtitute for used when need to Subtitute Items </small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-cogs"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a  class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>
					<!--
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <!-- <th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th> -->
                            <th>No </th>
                            <th>Item Code - Name</th>
                            <th>Item Subtitute Code - Name</th>
                            <th>Status</th>
                            <?php
                                if($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
                                    echo "<th> Actions </th>";
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="data-table-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">

<!-- Movement Type - GOJEK -->
<!--
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Movement Type<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('movement_type','', 'class="col-md-12 form-control"') ?>
                                            </div>
                                        </div>
 -->
<!-- End - Movement Type - GOJEK -->


                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Items Parent</label>
                                            <div class="controls col-md-9">
                                              <select class="col-md-12 form-control" name="item_id" id="item_id" style="width: 100%;">
                                                <option value="">----Please Select-----</option>
                                                <?php foreach($items as $key => $value){ ?>
                                                <option value="<?php echo $value['id']?>"><?php echo $value['code']?> - <?php echo $value['name']?></option>
                                                <?php } ?>
                                              </select>
                                            </div>
                                        </div>


<!-- Show when auto generate -->

<!-- End  -->

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Items Subtitute</label>
                                            <div class="controls col-md-9">

                                              <select class="col-md-12 form-control" name="item_id_subs" id="item_id_subs" style="width: 100%;">
                                                <option value="">----Please Select-----</option>
                                                <?php foreach($items as $key => $value){ ?>
                                                <option value="<?php echo $value['id']?>"><?php echo $value['code']?> - <?php echo $value['name']?></option>
                                                <?php } ?>
                                              </select>
                                            </div>
                                        </div>



      <div class="form-group">
                                            <label class="col-md-3 control-label">Status</label>
                                            <div class="controls col-md-9">
                                                <select class="col-md-12 form-control" name="item_status" style="width: 100%;" id="item_status">
  																								<option value="">----Please Select-----</option>
  																								<option value="ACTIVE">ACTIVE</option>
  																								<option value="DEACTIVE">DEACTIVE</option>
  																							</select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* is required</span>
                                    <a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
									<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

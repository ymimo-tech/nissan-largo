<?php if($this->access_right->otoritas('import')){?>
    <div id="form-import" class="modal fade" role="basic">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="data-table-form-import" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                               <div class="form-group">
                                    <label class="col-md-3 control-label">Import File Master <span class="required">*</span></label>
                                    <div class="controls col-md-9">
                                        <?php echo form_upload('file','', 'class="col-md-12 form-control" id="file"') ?>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <div class="form-actions">
                            <a id="download-template" class="btn green pull-left"><i class="fa fa-download"></i> Download Template </a>
                            <a class="form-close-import btn default"><i class="fa fa-close"></i> BACK</a>
                            <button type="submit" id="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
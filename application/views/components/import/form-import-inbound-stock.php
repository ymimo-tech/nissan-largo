<style>
    .filter-container .select2-container{
        width: 100% !important;
        z-index: 9;
    }

    .select2-container{
        z-index: 100000;
    }
</style>
<?php if($this->access_right->otoritas('import')){?>
    <div id="form-import-inbound-stock" class="modal fade" role="basic">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="data-table-form-import-inbound-stock" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Inbound Stock Import</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Warehouse</label>
                                    <div class="controls col-md-9">
                                        <?php
                                            echo form_dropdown('warehouse-inbound-stock', $warehouses, '', 'class="col-md-12 form-control" style="width: 100%;" id="warehouse-inbound-stock" data-placeholder="--Please Select--"');
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Import File Master <span class="required">*</span></label>
                                    <div class="controls col-md-9">
                                        <?php echo form_upload('file-inbound-stock','', 'class="col-md-12 form-control" id="file-inbound-stock"') ?>
                                    </div>
                                </div> 

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <div class="form-actions">
                            <a id="download-template-inbound-stock" class="btn green pull-left"><i class="fa fa-download"></i> Download Template </a>
                            <a class="form-close-import-inbound-stock btn default"><i class="fa fa-close"></i> BACK</a>
                            <button type="submit" id="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
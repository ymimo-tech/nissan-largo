<?php if($this->access_right->otoritas('import')){?>

    <div id="form-add-warehouse" class="modal fade" role="basic">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="data-table-form-add-warehouse" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Add to warehouse</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                               <div class="form-group">
                                    <label class="col-md-3 control-label">Warehouse <span class="required">*</span></label>
                                    <div class="controls col-md-9">
                                        <select class="form-control" style="width:100%;" name="add-warehouse-select" id="add-warehouse-select" multiple="true"></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <div class="form-actions">
                            <a class="form-close-add-warehouse btn default"><i class="fa fa-close"></i> BACK</a>
                            <button type="submit" id="submit" class="btn blue"><i class="fa fa-plus"></i> Add</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="form-remove-warehouse" class="modal fade" role="basic">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="data-table-form-remove-warehouse" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Remove from warehouse</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                               <div class="form-group">
                                    <label class="col-md-3 control-label">Warehouse <span class="required">*</span></label>
                                    <div class="controls col-md-9">
                                        <select class="form-control" style="width:100%;" name="remove-warehouse-select" id="remove-warehouse-select" multiple="true"></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <div class="form-actions">
                            <a class="form-close-remove-warehouse btn default"><i class="fa fa-close"></i> BACK</a>
                            <button type="submit" id="submit" class="btn red"><i class="fa fa-trash"></i> Remove</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

<?php } ?>
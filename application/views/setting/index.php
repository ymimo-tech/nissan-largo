<div class="row">
	
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1><?php echo $title;?>
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-cogs"></i>&nbsp;&nbsp;NODE3 Module
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
                                
						<!--<a  class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>-->
							
					<?php endif; ?>
					
					<?php if ($this->access_right->otoritas('print')) : ?>
                                
						<!--<a  class="btn green btn-sm data-table-export min-width120">
							<i class="fa fa-file-excel-o"></i> EXPORT </a>-->
							
					<?php endif; ?>
                </div>
            </div>
            <div class="portlet-body form">
                <form id="form-action" class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-2">Peminjaman</label>
							<div class="controls col-md-3">
								<label>
									<input type="checkbox" class="form-control" id="pinjaman" name="pinjaman" value=""
										<?php 
											if($setting['peminjaman'] == 'A' )
												echo 'checked';
										?>
									/>
									&nbsp;&nbsp;Enable
								</label>
							</div>
						</div>
					</div>
					
					<div class="form-actions">
						<div class="pull-right">
							<button type="submit" id="save" class="btn green-haze" style="min-width: 120px;">
								<i class="fa fa-check"></i> SAVE
							</button>
						</div>	
					</div>
					
				</form>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                    $hidden_form = array('id' => !empty($id) ? $id : '');
                    echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Item Code<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_barang', !empty($default->kd_barang) ? $default->kd_barang : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Item Name<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('nama_barang', !empty($default->nama_barang) ? $default->nama_barang : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Category Code<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_kategori', !empty($default->kd_kategori) ? $default->kd_kategori : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">PCS Code<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_satuan', !empty($default->kd_satuan) ? $default->kd_satuan : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Item Type<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('tipe_barang', !empty($default->tipe_barang) ? $default->tipe_barang : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Shipment Type<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('shipment_type', !empty($default->shipment_type) ? $default->shipment_type : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">FIFO Period<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('fifo_period', !empty($default->fifo_period) ? $default->fifo_period : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Owner Name<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('owner_name', !empty($default->owner_name) ? $default->owner_name : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 

                    <span class="required">* harus diisi</span>
                    
                    
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
       /* $('.chosen').chosen();*/
        /*$('.numeric').numeric();*/
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

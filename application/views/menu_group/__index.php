<div class="inner_content">
    <div class="user_bar">
        <div class="row-fluid">
            <div class="float_left">
                <h3><span><?php echo (isset($title)) ? $title : 'Untitle'; ?></span></h3>
            </div>
        </div>
    </div>
    <div class="widgets_area">
        <div class="row-fluid">
            <div class="span6">
                <div class="well-content no-search">

                    <div class="well">
                        <div class="pull-left">
                            <?php echo hgenerator::render_button_group($button_group); ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span12">
                            <div class="well">
                                <div class="well-content clearfix">

                                    <?php echo form_open_multipart('', array('id' => 'ffilter')); ?>
                                    <div class="form_row">
                                        <div class="pull-left span4">
                                            <div>
                                                <label>Pencarian :</label>
                                            </div>
                                            <div>
                                                <?php echo form_input('kata_kunci', '', 'class="span11 onchange"') ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>

                                </div>
                            </div> 
                        </div>
                    </div>

                    <div id="content_table" data-source="<?php echo $data_source; ?>" data-filter="#ffilter" pagination="false"></div>
                    <div>&nbsp;</div>
                </div>

                <div id="form-content" class="modal fade modal-xlarge"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {

        load_table('#content_table', 1, '#ffilter');

        $('.onchange').change(function() {
            load_table('#content_table', 1, '#ffilter');
        });


    });
</script>
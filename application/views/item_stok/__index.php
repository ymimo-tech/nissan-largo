<div class="inner_content">
    <div class="user_bar">
        <div class="row-fluid">
            <div class="float_left">
                <h2><span><?php echo (isset($title)) ? $title : 'Untitle'; ?></span></h2>
            </div>
        </div>
    </div>
    <div class="widgets_area">
        <div class="row-fluid">
            <div class="span6">
                <div id="index-content" class="well-content">
                    <div class="well">
                        <div class="pull-left">
                            <?php echo hgenerator::render_button_group($button_group); ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            if ($this->access_right->otoritas('print')) 
                                echo hgenerator::render_button_group($print_group);
                            ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span12">
                            <div class="well">
                                <div class="well-content clearfix">
                                    <?php echo form_open_multipart('', array('id' => 'ffilter')); ?>
                                        <div class="form_row">
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Kode Barang :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_input('kd_barang', '', '" class="span11 onchange" id="kd_barang"'); ?>
                                                </div>
                                            </div>        
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Nama Barang :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_input('nama_barang', '', '" class=" span11 onchange" id="nama_barang"'); ?>
                                                </div>
                                            </div>  
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Satuan :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_dropdown('id_satuan', $options_satuan, '','" class="chosen span11 onchange" id="id_satuan"'); ?>
                                                </div>
                                            </div>  
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Kategori :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_dropdown('id_kategori', $options_kategori, '', '" class="chosen span11 onchange" id="jabatan"'); ?>
                                                </div>
                                            </div>  
                                                           
                                        </div>
                                        <div class="form_row">                                    
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Tahun Aktif :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_dropdown('tahun_aktif',$tahun_aktif,date('Y'), 'class="span12 onchange chosen"') ?>
                                                </div>
                                            </div> 
                                        </div>

                                    <?php echo form_close(); ?>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div id="content_table" data-source="<?php echo $data_source; ?>" data-filter="#ffilter"></div>
                    <div>&nbsp;</div>
                </div>
                
                <div id="form-content" class="modal fade modal-xlarge"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        load_table('#content_table', 1);
        $('.onchange').change(function(){
            load_table('#content_table', 1);
        });
    });

    function refresh_filter(){ 
        load_table('#content_table', 1,function(){
        get_options('#kelas', {}, true);
        get_options('#jenis_perawatan', {}, true);
        get_options('#jabatan', {}, true);
        get_options('#satuan', {}, true);
        });
    }

</script>


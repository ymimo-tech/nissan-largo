<style>
	.qty h1{
		font-size: 46px !important;
		font-weight: bold;
		color: #fff !important;
	}

	.qty h1 a{
		color: #fff !important;
	}

	.qty h1 a:hover{
		text-decoration: none;
	}

	.qty h1 a:active{
		text-decoration: none;
	}

	.qty h1 a:focus{
		text-decoration: none;
	}

	.qty span{
		font-weight: bold;
		color: #fff;
	}

	.widget-thumb{
		padding: 10px;
	}
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1><?php echo $title ?>
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
                    	<?php if(!empty($subTitle)) echo $subTitle;?>
                	</span>
				</div>
            </div>
            <div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<h3><strong><?php echo $barang->kd_barang . " | " . $barang->nama_barang ?></strong></h3>
					</div>
					<div class="col-md-6">

						<table>
							<tr>
								<td>Category 1</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->kd_kategori . ' - '. $barang->nama_kategori; ?></td>
							</tr>
							<tr>
								<td>Category 2</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->kd_kategori_2 . ' - '. $barang->nama_kategori_2; ?></td>
							</tr>
							<tr>
								<td>UoM</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->nama_satuan; ?></td>
							</tr>
							<tr>
								<td>Picking Strategy</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->shipment_type; ?></td>
							</tr>
						</table>
						<br>
					</div>

					<div class="col-md-6">
						<table>

							<tr>
								<td>FIFO/FEFO Period</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->fifo_period; ?></td>
							</tr>
							<!-- <tr>
								<td>Owner</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php //echo $barang->owner_name; ?></td>
							</tr> -->
							<tr>
								<td>Has Multi Qty</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php
								if($barang->has_qty == 1){
									echo 'YES';
								}else{
									echo 'NO';
								}
								 ?></td>
							</tr>
							<tr>
								<td>Default Qty</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->def_qty; ?></td>
							</tr>
						</table>
						<br>
					</div>
				</div>
				<div class="row qty">

					<div class="col-md-12">
						<h3 style="font-weight: bold;">TOTAL QTY : <span class="total" style="color: #323232;">0</span> <?php echo $barang->nama_satuan; ?></h3>
					</div>
					<br><br>
					<!--<div class="col-md-2 total" style="text-align: center;">
						<div class="widget-thumb bg-grey-salt text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat total-inbound">0</h1>
								</div>
							</div>
						</div>
					</div>-->
					<div class="col-md-2 rec" style="text-align: center;">

						<div class="widget-thumb bg-blue text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
						<!--<span></span>
						<h1></h1>-->
					</div>
					<div class="col-md-2 on" style="text-align: center;">
						<div class="widget-thumb bg-green text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 all" style="text-align: center;">
						<div class="widget-thumb bg-red-sunglo text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 sus" style="text-align: center;">
						<div class="widget-thumb bg-red-intense text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 ng" style="text-align: center;">
						<div class="widget-thumb bg-red-soft text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 ava" style="text-align: center;">
						<div class="widget-thumb bg-green-turquoise text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>

					<!--
					<table id="table-qty" class="table table-striped table-bordered table-hover table-checkable order-column">
						<thead>
							<tr>
								<th style="width: 30px;"> No </th>
								<th style="text-align: center;"> Receiving Qty </th>
								<th style="text-align: center;"> On Hand Qty </th>
								<th style="text-align: center;"> Allocated Qty </th>
								<th style="text-align: center;"> Suspend Qty </th>
								<th style="text-align: center;"> Available Qty </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
					-->

				</div>
				<div>
					&nbsp;
				</div>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase"><i class="fa fa-th-list"></i> LPN LIST </span>
				</div>
				<div class="tools">
					<button id="export-detail" type="button" class="btn green" style="min-width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT </button>
					<?php if ($this->access_right->otoritas('print')) { ?>
						<button id="reprint" type="button" class="btn blue" style="min-width: 120px;"><i class="fa fa-print"></i> REPRINT LPN </button>
					<?php } ?>
					<?php if ($this->access_right->otoritas('print')) { ?>
						<button id="reprint-license-plate" type="button" class="btn blue" style="min-width: 120px;"><i class="fa fa-print"></i> REPRINT GID </button>
					<?php } ?>
					<button id="reload" type="button" class="btn green" style="min-width: 120px;"><i class="fa fa-list"></i> SHOW ALL</button>
                </div>
            </div>
            <div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<table id="table_receiving" class="table table-striped table-bordered table-hover table-checkable order-column">
								<thead>
									<tr>
										<th class="table-checkbox">
											<input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" />
										</th>
										<th style="vertical-align: top;text-align: center;width: 30px;"> No </th>
										<th style="vertical-align: top;text-align: center;"> LPN </th>
										<th style="vertical-align: top;text-align: center;text-align: center;"> GID </th>
										<th style="vertical-align: top;text-align: center;text-align: center;"> Qty </th>
										<th style="vertical-align: top;text-align: center;text-align: center;"> Doc. Reference # </th>
										<th style="vertical-align: top;text-align: center;text-align: center;"> Exp Date </th>
										<th style="vertical-align: top;text-align: center;text-align: center;"> In Date </th>
										<th style="vertical-align: top;text-align: center;text-align: center;"> Location </th>
										<th style="vertical-align: top;text-align: center;text-align: center;"> Location Type </th>
										<th style="vertical-align: top;text-align: center;text-align: center;"> Item Status </th>
										<!-- <th style="vertical-align: top;text-align: center;text-align: center;"> Item Status </th> -->

									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!--
				<div class="form-actions top40">
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
							</div>
						</div>
					</div>
				</div>
				-->
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light stock-card">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase"><i class="fa fa-th-list"></i> STOCK CARD</span>

				</div>
				<div class="tools" style="padding: 0;">
					<?php if ($this->access_right->otoritas('print')) { ?>
						<button id="export-stockcard" type="button" class="btn green" style="min-width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT</button>
					<?php } ?>
                </div>
            </div>
            <div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
							<!-- <input type="hidden" name="card_total" value="<?php echo $stock_card_total['total']; ?>" /> -->
							<table id="stock-card-table" class="table table-striped table-bordered table-hover table-checkable order-column">
								<thead>
									<tr>
										<th style="text-align: center;"> Date </th>
										<th style="text-align: center;"> In </th>
										<th style="text-align: center;"> Out </th>
										<th style="text-align: center;"> Balance </th>
										<th style="text-align: center;"> By </th>
										<th style="text-align: center;"> Ref. Doc </th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="text-align: center;">
							<button type="button" class="btn default more" style="display: none;">Load more</button>
						</div>
					</div>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-12">

							<div class="pull-right">
								<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
							</div>
						</div>
					</div>
				</div>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?php if ($this->access_right->otoritas('print')) { ?>
	<?php include 'print_modal.php'; ?>
<?php }?>

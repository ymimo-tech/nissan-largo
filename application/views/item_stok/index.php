<style>
	.qty h1{
		font-size: 46px !important;
		font-weight: bold;
		color: #fff !important;
	}

	.qty h1 a{
		color: #fff !important;
	}

	.qty h1 a:hover{
		text-decoration: none;
	}

	.qty h1 a:active{
		text-decoration: none;
	}

	.qty h1 a:focus{
		text-decoration: none;
	}

	.qty span{
		font-weight: bold;
		color: #fff;
	}

	.widget-thumb{
		padding: 10px;
	}
</style>

<div class="row">

    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="custom-page-title">
            <h1>ACTIVE INVENTORY
                <small>List of all the inventory items / materials</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ALERTS PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">

				<div class="caption col-md-12">
                    <span class="caption-subject font-green-sharp bold uppercase"><?php if(!empty($subTitle)) echo $subTitle;?></span>
                </div>

				<div class="row">
<!-- 					<div class="col-md-6">
						<h3 style="font-weight: bold; margin-top: 10px;">TOTAL QTY : <span class="total" style="color: #323232;">0</span> <?php  ?></h3>
					</div>
 -->
 					<!-- BUG: Cek kembali angka low-qty ini apakah jumlah item yang stoknya di bawah level restock/replenish -->
 					<div class="col-md-12">
						<div class="pull-right" style="margin-top: -30px;">
							<label class="label label-danger" style="font-weight: bold;">LOW ITEM : <span class="low-qty" style=""></span> </label>
						</div>
					</div>
				</div>

				<!-- <div class="row qty"> -->
					<!--<div class="col-md-2 total" style="text-align: center;">
						<div class="widget-thumb bg-grey-salt text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat total-inbound">0</h1>
								</div>
							</div>
						</div>
					</div>-->
<!-- 					<div class="col-md-2 rec" style="text-align: center;">

						<div class="widget-thumb bg-blue text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 on" style="text-align: center;">
						<div class="widget-thumb bg-green text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 all" style="text-align: center;">
						<div class="widget-thumb bg-red-sunglo text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 sus" style="text-align: center;">
						<div class="widget-thumb bg-red-intense text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 ng" style="text-align: center;">
						<div class="widget-thumb bg-red-soft text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 ava" style="text-align: center;">
						<div class="widget-thumb bg-green-turquoise text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div> -->

					<!--
					<table id="table-qty" class="table table-striped table-bordered table-hover table-checkable order-column">
						<thead>
							<tr>
								<th style="width: 30px;"> No </th>
								<th style="text-align: center;"> Receiving Qty </th>
								<th style="text-align: center;"> On Hand Qty </th>
								<th style="text-align: center;"> Allocated Qty </th>
								<th style="text-align: center;"> Suspend Qty </th>
								<th style="text-align: center;"> Available Qty </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
					-->

				<!-- </div> -->

                <div class="tools">
									<button id="sync" type="button" class="btn blue" style="width: 120px;"><i class="fa fa-refresh"></i> SYNC</button>
									<button id="export" type="button" class="btn green" style="width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT LPN</button>
									<button id="export-format2" type="button" class="btn green" style="width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT 2</button>
									<!--<button id="export-format-soh" type="button" class="btn green" ><i class="fa fa-file-excel-o"></i> EXPORT SOH </button>-->
                </div>
            </div>
            <div class="portlet-body">
				<!--
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <?php if ($this->access_right->otoritas('add')) : ?>

                                <?php echo hgenerator::render_button_group($button_group); ?>

                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">

                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">

                                    <li>
                                        <a href="<?php echo base_url('item_stok/save_pdf');?>" target="_blank">
                                        Save as PDF </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="well-content clearfix">
                                    <?php echo form_open_multipart('', array('id' => 'ffilter')); ?>
                                        <div class="row">
                                            <div class="pull-left form-group col-md-3">
                                                <div>
                                                    <label>Item Code :</label>
                                                </div>
                                                <div>
                                                    <?php //echo form_input('kd_barang','', 'class="col-md-12 onchange form-control"') ?>
                                                </div>
                                            </div>
                                            <div class="pull-left col-md-3">
                                                <div>
                                                    <label>Item Name :</label>
                                                </div>
                                                <div>
                                                    <?php //echo form_input('nama_barang', '', 'class="col-md-12 onchange form-control chosen"') ?>
                                                </div>
                                            </div>
                                            <div class="pull-left col-md-3">
                                                <div>
                                                    <label>Oum :</label>
                                                </div>
                                                <div>
                                                    <?php //echo form_dropdown('id_satuan',$options_satuan,'', 'class="col-md-12 onchange form-control chosen"') ?>
                                                </div>
                                            </div>
                                        </div>

                                    <?php echo form_close(); ?>
                                </div>

                        </div>
                    </div>
                </div>
				-->
                <div class="row">
                    <div class="col-md-12 col-sm-12">

						<!--COPY THIS SECTION FOR FILTER CONTAINER-->
						<div class="portlet box blue-hoki filter-container">
							<div class="portlet-title">
								<div class="caption">
									Filter
								</div>
								<div class="tools">
									<a href="javascript:;" class="filter-indicator">Show filter</a>
									<a href="javascript:;" class="expand show-hide">
									</a>
								</div>
							</div>
							<div class="portlet-body" style="display: none;">
								<!--YOUR CUSTOM FILTER HERE-->
								<form id="form-search" class="form-horizontal" role="form">
									<div class="form-body">


										<div class="row">
											<div class="col-md-6">

												<div class="form-group item-group">
													<label for="item" class="col-md-3">Item</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="item" id="item" style="width: 100%;" multiple data-placeholder="-- All --">

															</select>
														</div>
														<span class="help-block item-msg"></span>
													</div>
												</div>

												<div class="form-group status-group">
													<label for="status" class="col-md-3">Item Status</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="status" id="status" style="width: 100%;" multiple data-placeholder="-- All --">

																<?php

																	//echo '<option value="">-- All --</option>';

																	foreach($status as $row){
																		echo '<option value="'.$row['id_qc'].'">'.$row['kd_qc'].'</option>';
																	}

																?>

															</select>
														</div>
														<span class="help-block status-msg"></span>
													</div>
												</div>

												<div class="form-group cat-group">
													<label for="cat" class="col-md-3">Item Category</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="cat" id="cat" style="width: 100%;" multiple data-placeholder="-- All --">

																<?php

																	//echo '<option value="">-- All --</option>';

																	foreach($category as $row){
																		echo '<option value="'.$row['id_kategori'].'">'.$row['nama_kategori'].'</option>';
																	}

																?>

															</select>
														</div>
														<span class="help-block cat-msg"></span>
													</div>
												</div>

											</div>

											<div class="col-md-6">

												<div class="form-group loc-group">
													<label for="loc" class="col-md-3">Location</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="loc" id="loc" style="width: 100%;" multiple data-placeholder="-- All --">
															</select>
														</div>
														<span class="help-block loc-msg"></span>
													</div>
												</div>

												<div class="form-group loc-type-group">
													<label for="loc-type" class="col-md-3">Location Type</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="loc_type" id="loc-type" style="width: 100%;" multiple data-placeholder="-- All --">

																<?php

																	//echo '<option value="">-- All --</option>';

																	foreach($location_type as $k => $v ){
																		echo '<option value="'.$k.'">'.$v.'</option>';
																	}

																?>

															</select>
														</div>
														<span class="help-block loc-type-msg"></span>
													</div>
												</div>

												<div class="form-group uom-group">
													<label for="uom" class="col-md-3">UoM</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="uom" id="uom" style="width: 100%;" multiple data-placeholder="-- All --">

																<?php

																	//echo '<option value="">-- All --</option>';

																	foreach($uom as $row){
																		echo '<option value="'.$row['id_satuan'].'">'.$row['nama_satuan'].'</option>';
																	}

																?>

															</select>
														</div>
														<span class="help-block uom-msg"></span>
													</div>
												</div>

											</div>

										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-12">
												<div class="pull-right">
													<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
													<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
												</div>
											</div>
										</div>
									</div>
								</form>
								<!--END-->
							</div>
						</div>
						<!--END SECTION-->

                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <!--<div class="portlet light">
                            <div class="portlet-body">-->
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                                    <thead>
                                        <tr>
											<!--
                                            <th class="table-checkbox">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
											-->
                                            <th> No </th>
                                            <th style="text-align: center;"> Item Code </th>
                                            <th> Item Name </th>
																						<th style="text-align: center;"> Category </th>
                                            <th style="text-align: center;"> UoM </th>
																						<th style="text-align: center;"> Location </th>
																						<th style="text-align: center; width: 90px;"> Loc. Type </th>
                                            <th style="text-align: center;"> QTY </th>
																						<th style="text-align: center;"> Min. Stock </th>
                                        </tr>
                                    </thead>
                                    <tbody> </tbody>

                                </table>
								<div>&nbsp;</div>
							<!--
                            </div>
                            <div>&nbsp;</div>
                        </div>-->
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- /.modal -->
            </div>
        </div>
        <!-- END ALERTS PORTLET-->
    </div>
</div>

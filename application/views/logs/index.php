<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>SAP Logs
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i>
						<?php echo $title;?>
					</span>
				</div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table-list">
                    <thead>
                        <tr>
							<th style="display: none;"> refId </th>
							<th style="width: 30px;"> No </th>
							<th style="text-align: center;">Reference Doc. #</th>
							<th style="text-align: center;"> Status </th>
							<th style="text-align: center;"> Message </th>
							<th style="text-align: center;"> Last Sync </th>
                            <?php
                                if($this->access_right->otoritas('add') || $this->access_right->otoritas('edit'))
                                    echo '<th style="text-align: center;"> Actions </th>';
							?>
                        </tr>
                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="modal-remark" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal" class="form-horizontal">
				<input type="hidden" name="id"/>

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Finish Tally</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
						   <div class="form-group do-group">
								<label class="col-md-2 control-labe">Remark</label>
								<div class="controls col-md-9">
									<textarea class="form-control" id="remark" name="remark" placeholder="remark"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-remark" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-check"></i> &nbsp;SUBMIT</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

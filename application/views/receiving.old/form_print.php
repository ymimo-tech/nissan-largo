<div class="modal-dialog">
    <div class="modal-content " >
        <div class="modal-body " >
            <div id="section-to-print"><div id="section-to-print">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>

            <div class="modal-body"  id="finput2">
                <div class="row">
                    <div class="col-md-12">
                    
                    <?php
                    $hidden_form = array('id' => !empty($id) ? $id : '');
                    echo form_open_multipart($form_action, array('id' => 'finput' ), $hidden_form);
                    ?>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label><?php $kd_receiving = !empty($kd_receiving) ? $kd_receiving : '';
                            echo 'Receiving Code: '.$kd_receiving;?></label>
                            <div class="input-group">
                                <?php
                            echo '<img alt="testing" src="'.base_url("code/barcode.php").'?text='.$kd_receiving.'&size=45" />';
                            ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-8 form-horizontal">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Receiving Date<span class="required">*</span></label>
                            <div class=" col-xs-8">
                                <span class="form-control-static">
                                    <?php $tanggal_receiving = !empty($row->tanggal_receiving) ? $row->tanggal_receiving : ''; echo $tanggal_receiving;?>
                                </span>
                                
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Vehicle Plate<span class="required">*</span></label>
                            <div class=" col-xs-8">
                                <span class="form-control-static">
                                    <?php $vehicle_plate = !empty($row->vehicle_plate) ? $row->vehicle_plate : ''; echo $vehicle_plate;?>
                                </span>
                                
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-xs-4 control-label">Dock<span class="required">*</span></label>
                            <div class=" col-xs-8">
                                <span class="form-control-static">
                                    <?php $dock = !empty($row->dock) ? $row->dock : ''; echo $dock;?>
                                </span>
                                
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Supplier<span class="required">*</span></label>
                            <div class="col-xs-8">
                                <span class="form-control-static">
                                    <?php $kd_supplier = !empty($row->kd_supplier) ? $row->kd_supplier : '';echo $kd_supplier.'/'.$row->nama_supplier;?>
                                </span>
                                
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Delivery Doc<span class="required">*</span></label>
                            <div class=" col-xs-8">
                                <span class="form-control-static">
                                    <?php $delivery_doc = !empty($row->delivery_doc) ? $row->delivery_doc : ''; echo $delivery_doc;?>
                                </span>
                                
                            </div>
                        </div> 
                    </div>

                    <!-- begin table -->
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Item Name/SKU </th>
                                <th> QTY </th>
                               
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $temp_kd_barang = '';
                        $i=1;
                        foreach ($default->result() as $row) { ?>
                            <tr>
                                <td> <?php echo $i; $i++;?> </td>
                                <td> 
                                    <div class="form-group">
                                        <label>
                                            <?php $nama_barang = !empty($row->nama_barang) ? $row->nama_barang : '';
                                        echo $nama_barang;?> - 
                                            <?php $kd_barang = !empty($row->kd_barang) ? $row->kd_barang : '';
                                        echo $kd_barang;?></label>
                                        <div class="input-group">
                                            <?php
                                        echo '<img alt="testing" src="'.base_url("code/barcode.php").'?text='.$kd_barang.'&size=20" />';
                                        ?>
                                        </div>
                                    </div>
                                </td>
                                <td> <?php echo $row->qty;?> <?php echo $row->nama_satuan;?> </td>                                
                            </tr>
                            <?php  } ?>
                            
                        </tbody>
                    </table>
                    <!-- end table -->

                    
                    

                 
                    </div>
                </div>
            </div>
            </div></div>
            <div class="modal-footer">
                <div class="form-actions">
                    <?php echo anchor(null, '<i class="icon-printer"></i> Print', array('id' => 'button-save', 'class' => 'green btn', 'onclick' => "print_barcode(this.id, '#finput2', '#button-back')")); ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
       /* $('.chosen').chosen();*/
        /*$('.numeric').numeric();*/
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

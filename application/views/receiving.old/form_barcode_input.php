<div class="modal-dialog">
    <div class="modal-content " >
        <div class="modal-body " >
            <div id="section-to-print"><div id="section-to-print">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>

            <div class="modal-body"  id="finput2">
                <div class="row">
                    <div class="col-md-12">

            <div class="form-group">
                        <label class="col-md-3 control-label">Qty Label Print<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('qty_print','', 'class="col-md-12 form-control" id=qty_print') ?>
                            
                        </div>
                    </div>

			<div class="form-group row">
				<div class="col-md-12" align="center">
					<!-- OPSI CANVAS -->
					<!-- <canvas id="barcode_modal" width="200px" height="70px"> </canvas>-->

					<!-- OPSI HTML -->
					<div>
						<span id="list_printer">
							
						</span>
					</div>
 				</div>
			</div>
			<div class="modal-footer">
                <div class="form-actions">
                    <?php echo anchor(null, '<i class="icon-printer"></i> Print', array('id' => 'button-save', 'class' => 'green btn', 'onclick' => "printZPL()",'id'=>"load-print")); ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
		</div>
	
</form>

                </div>
            </div>
            </div></div>
            
        <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
       /* $('.chosen').chosen();*/
        /*$('.numeric').numeric();*/
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>
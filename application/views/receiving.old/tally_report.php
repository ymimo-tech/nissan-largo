<html>
<head>
	<title>Tally Report</title>
	<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<style>
		.table-pdf{
			width: 100%;
			margin-top: 40px;
		}
		.table-pdf tr td, tr th{
			padding: 10px;
			border-bottom: 1px solid #eee;
		}
		.width120{
			width: 120px;
		}
		.bottom10{
			margin-bottom: 10px;
		}
		.width20{
			width: 20px;
		}
		.tally-rcv-code{
			font-size: 26px;
		}
	</style>
</head>
<body style="font-family: Arial;">
	<h3>Receiving Tally Report</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td style="width: 300px; text-align: center; vertical-align: top;">
						<label>Receiving Doc. #</label>
						<br><br>
						<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $receiving['kd_receiving'] ?>&size=65" style="">
						<br>
						<span class="tally-rcv-code"><?php echo $receiving['kd_receiving']; ?></span>
					</td>
					<td>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Receiving Date</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $receiving['tanggal_receiving']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Vehicle Plate</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $receiving['vehicle_plate']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Driver</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $receiving['dock']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td valign="top" class="width120">
									<label>Supplier</label>
								</td>
								<td valign="top" class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $receiving['kd_supplier'] . ' - ' . $receiving['nama_supplier']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Delivery Doc</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $receiving['delivery_doc']; ?>
									</span>
								</td>
							</tr>
						</table>
						<table class="bottom10">
							<tr>
								<td class="width120">
									<label>Inbound Doc</label>
								</td>
								<td class="width20">
								:
								</td>
								<td>
									<span>
										<?php echo $receiving['kd_inbound']; ?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- begin table -->
	<table class="table-pdf">
		<thead>
			<tr>
				<th> # </th>
				<th style="width: 30%;"> QTY </th>
				<th> Item Name/SKU </th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i = 1;
				foreach($items['data'] as $row){
					$str = '<tr>
								<td> '.$i.' </td>
								<td> '.$row['qty_doc'].' '.$row['nama_satuan'].' </td> 
								<td {{ align }}>
									<label>'.$row['nama_barang'].' - '.$row['kd_barang'].'</label>
									<br>
									<img style="margin-top: 5px;" alt="" src="'.base_url().'barcode?text='.$row['kd_barang'].'&size=35">
								</td>
							</tr>';
							
					if(($i % 2) == 0){
						$str = str_replace('{{ align }}', 'align="right"', $str);
					}
					
					echo $str;
					$i++;
				}
			?>
		</tbody>
	</table>
	<!-- end table -->
</body>
</html>
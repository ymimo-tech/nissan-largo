<div class="row">
	
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Receiving
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i><?php echo $title;?> 
					</span>
				</div>
                <div class="actions">
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<input type="hidden" name="id" value="<?php echo $id; ?>" />
					<input type="hidden" name="id_inbound" value="<?php echo $id_inbound; ?>" />
					<input type="hidden" name="params" value="" />
					<div class="form-body">	
						<div class="row">
							<div class="col-md-12">
							   <div class="form-group rcv-code-group" style="margin-bottom: 20px;">
									<label class="col-md-2">Receiving Doc. #<span class="required">*</span></label>
									<div class="controls col-md-6">
										<div style="">
											<input type="hidden" name="kd_receiving" value="<?php echo $rcv_code; ?>" />
											<strong><span class="rcv-code"><?php echo $rcv_code; ?></span></strong>
										</div>
									</div>
								</div> 
								
								<div class="form-group rcv-date-group">
									<label class="col-md-2">Receiving Date<span class="required">*</span></label>
									<div class="col-md-6">
										<div class="input-group input-medium date date-picker">
											<input type="text" class="form-control" id="rcv-date" name="rcv_date" placeholder="dd/mm/yyyy" 
												value="<?php echo $today; ?>" maxLength="10" />
											<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group inb-group">
									<label class="col-md-2">Inbound Doc. #<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select class="form-control" 
											name="inbound" id="inbound">
											<option value="">-- Choose Inbound Document --</option>
											<?php 
												foreach($inbound['result'] as $row){
													echo '<option value="'.$row['id_inbound'].'">'.$row['kd_inbound'].'</option>';
												}
											?>
										</select>
										<span class="help-block inb-code-msg"></span>
									</div>
								</div> 
								<div class="form-group plate-group">
									<label class="col-md-2">Vehicle Plate</label>
									<div class="controls col-md-6">
										<input type="text" id="plate" name="plate" class="form-control" value="" placeholder="B 123 ABC" maxLength="20" />
										<span class="help-block plate-msg"></span>
									</div>
								</div> 
								
								<div class="form-group driver-group">
									<label class="col-md-2">Driver</label>
									<div class="controls col-md-6">
										<input type="text" id="driver" name="driver" class="form-control" value="" placeholder="Driver Name" maxLength="100" />
										<span class="help-block driver-msg"></span>
									</div>
								</div> 
								
								<div class="form-group doc-group">
									<label class="col-md-2">Delivery Doc.</label>
									<div class="controls col-md-6">
										<input type="text" id="doc" name="doc" class="form-control" value="" placeholder="Delivery Document" maxLength="100" />
										<span class="help-block doc-msg"></span>
									</div>
								</div> 

								<div class="form-group doc-group">
									<label class="col-md-2">Cross Doc.</label>
									<div class="controls col-md-6">
										<input type="checkbox" id="crossdoc" name="crossdoc" class="form-control" />
										<span class="help-block doc-msg"></span>
									</div>
								</div> 
								
							</div>
						</div>
					</div>
					
					<div class="detail-item" style="display: none;">
						<hr>
						<!--<h4>Items</h4>-->
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped table-bordered table-hover table-checkable order-column" id="table-list">
									<thead>
										<tr>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Item Code<div> </th>
											<th> Item Name </th>
											<th style="width: 200px;"> <div align="center">Doc. Qty</div> </th>
											<th style="width: 120px;"> <div align="center">Received</div> </th>
											<th style="width: 120px;"> <div align="center">Discrepancies</div> </th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<!-- <span class="required pull-left">* is required</span> -->
								<div class="pull-right">
								
									<a class="btn default min-width120" href="javascript:;" onClick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
									
									<?php if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) : ?>
										<button type="submit" class="btn blue min-width120"> SAVE</button>

										<?php if(empty($id)) : ?>
											<button id="save-new" type="button" class="btn green-haze min-width120"> SAVE & NEW</button>
										<?php endif; ?>
									<?php endif; ?>
									
								</div>
							</div>
						</div>
					</div>
					
				</form>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
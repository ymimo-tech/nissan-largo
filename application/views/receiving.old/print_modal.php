<style type="text/css">
    .select2-container--open{
        z-index:99999;
    }
</style>
<div id="modal-print" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal-form" class="form-horizontal">
				<input type="hidden" name="id"/>
				<input type="hidden" id="pid" name="pid" value="<?php echo $pid; ?>" />
				<input type="hidden" id="sid" name="sid" value="<?php echo session_id(); ?>" />
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Barcode label printing</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">

							<!-- Print Label SN with SKU -->

<!-- 						   <div class="form-group item-print-group">
								<label class="col-md-3 control-label">Item<span class="required">*</span></label>
								<div class="controls col-md-6">
									<select class="form-control" id="item" style="width:100%;">
										<?php
											foreach ($items as $itm) {
												echo "<option value='".$itm['code']."''>".$itm['code']." - ".$itm['name']."</option>";
											}
										?>
									</select>
									<span class="item-label" style="display: none; position: relative; top: 8px;">0</span>
								</div>
							</div> 
 -->
							<!-- END - Print Label SN with SKU -->

						   <div class="form-group qty-print-group">
								<label class="col-md-3 control-label">Qty<span class="required">*</span></label>
								<div class="controls col-md-6">
									<input type="hidden" id="type-print" name="type_print"/>
									<input type="text" id="qty-print" name="qty_print" placeholder="Qty Print" class="form-control" />
									<span class="qty-print-receiving" style="display: none; position: relative; top: 8px;">0</span>
								</div>
							</div> 

						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-print" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-print"></i> &nbsp;PRINT</button>
					</div>
				</div>
			
			</form>
		</div>
	</div>
</div> 
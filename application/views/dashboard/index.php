<style>
	#chart_6 a, #chart_7 a, #chart_inbound a, #chart_outbound a{
		display: none !important;
	}
</style>

<div class="row">
	<input type="hidden" id="sessId" value="<?php echo session_id(); ?>" />
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<!-- BEGIN PAGE TITLE -->
				<div class="custom-page-title">
					<h1>DASHBOARD
						<small>High-level overview of Warehouse status</small>
					</h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<div class="col-md-6">

				<div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp;
					<span class="thin uppercase hidden-xs"></span>&nbsp;
					<i class="fa fa-angle-down"></i>
				</div>

			</div>
		</div>
	</div>

</div>

<div class="row widget-row">
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">OPEN INBOUND</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-green icon-bulb"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle inbound-item">0 Items</span>
					<span class="widget-thumb-body-stat total-inbound">0</span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">RECEIVING</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-red icon-layers"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle receiving-item">0 Items</span>
					<span class="widget-thumb-body-stat total-receiving">0</span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">OPEN OUTBOUND</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle outbound-item">0 Items</span>
					<span class="widget-thumb-body-stat total-outbound">0</span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">PICKING</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle picking-item">0 Items</span>
					<span class="widget-thumb-body-stat total-picking">0</span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered" style="min-height: 395px;">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Received QTY</span>
                    <span class="caption-helper"></span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="site_statistics_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="site_statistics_content" class="display-none">
                    <div id="site_statistics" class="chart"> </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered" style="min-height: 395px;">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">Loaded QTY</span>
                    <span class="caption-helper"></span>
                </div>

            </div>
            <div class="portlet-body">
                <div id="site_activities_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="site_activities_content" class="display-none">
                    <div id="site_activities" class="chart" > </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered" style="min-height: 329px;">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green"></i>
                    <span class="caption-subject font-green bold uppercase">INBOUND</span>
                    <span class="caption-helper">TOP 5 Item Received</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="top_received_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" />
				</div>

				<table class="table-top-received table table-hover table-light table-striped">
					<thead>
						<tr class="uppercase">
							<th style="text-align: center;"> ITEM </th>
							<th style="text-align: center;"> QTY </th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered" style="min-height: 329px;">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">OUTBOUND</span>
                    <span class="caption-helper">TOP 5 Item Loaded</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="top_ordered_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" />
				</div>

				<table class="table-top-ordered table table-hover table-light table-striped">
					<thead>
						<tr class="uppercase">
							<th style="text-align: center;"> ITEM </th>
							<th style="text-align: center;"> QTY </th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Today Inbound Status</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="today_inbound_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="chart_inbound" class="" style="height: 325px;"> </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">Today Outbound Status</span>
                </div>

            </div>
            <div class="portlet-body">
                <div id="today_outbound_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="chart_outbound" class="" style="height: 325px;"> </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Current Active Stock</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="current_stock_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="chart_6" class="" style="height: 325px;"> </div>
				<br>
				<div align="center">Total : <span class="total-stock">0</span></div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">Current Status Stock</span>
                </div>

            </div>
            <div class="portlet-body">
                <div id="current_status_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="chart_7" class="" style="height: 325px;"> </div>
				<br>
				<div align="center">Total : <span class="total-status">0</span></div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Replenishment</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="replenishment_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" />
				</div>

				<table class="table-replenishment table table-hover table-light table-striped">
					<thead>
						<tr class="uppercase">
							<th style="text-align: center;"> ITEM </th>
							<th style="text-align: center;"> CURRENT STOCK </th>
							<th style="text-align: center;"> MINIMUM STOCK </th>
							<th style="text-align: center; display: none;"> satuan </th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<!--
<div class="row">
	<div class="col-md-6 col-sm-6">
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart font-green"></i>
					<span class="caption-subject font-green uppercase bold">Sales Summary</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<label class="btn btn-transparent grey-salsa btn-outline btn-circle btn-sm active">
							<input type="radio" name="options" class="toggle" id="option1">Today</label>
						<label class="btn btn-transparent grey-salsa btn-outline btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Week</label>
						<label class="btn btn-transparent grey-salsa btn-outline btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Month</label>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm"> Total Sales </div>
						<div class="uppercase font-hg font-red-flamingo"> 13,760
							<span class="font-lg font-grey-mint">$</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm"> Revenue </div>
						<div class="uppercase font-hg theme-font"> 4,760
							<span class="font-lg font-grey-mint">$</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm"> Expenses </div>
						<div class="uppercase font-hg font-purple"> 11,760
							<span class="font-lg font-grey-mint">$</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm"> Growth </div>
						<div class="uppercase font-hg font-blue-sharp"> 9,760
							<span class="font-lg font-grey-mint">$</span>
						</div>
					</div>
				</div>
				<ul class="list-separated list-inline-xs hide">
					<li>
						<div class="font-grey-mint font-sm"> Total Sales </div>
						<div class="uppercase font-hg font-red-flamingo"> 13,760
							<span class="font-lg font-grey-mint">$</span>
						</div>
					</li>
					<li> </li>
					<li class="border">
						<div class="font-grey-mint font-sm"> Revenue </div>
						<div class="uppercase font-hg theme-font"> 4,760
							<span class="font-lg font-grey-mint">$</span>
						</div>
					</li>
					<li class="divider"> </li>
					<li>
						<div class="font-grey-mint font-sm"> Expenses </div>
						<div class="uppercase font-hg font-purple"> 11,760
							<span class="font-lg font-grey-mint">$</span>
						</div>
					</li>
					<li class="divider"> </li>
					<li>
						<div class="font-grey-mint font-sm"> Growth </div>
						<div class="uppercase font-hg font-blue-sharp"> 9,760
							<span class="font-lg font-grey-mint">$</span>
						</div>
					</li>
				</ul>
				<div id="sales_statistics" class="portlet-body-morris-fit morris-chart" style="height: 267px"> </div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6">
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart font-red"></i>
					<span class="caption-subject font-red bold uppercase">Member Activity</span>
					<span class="caption-helper">weekly stats...</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<label class="btn btn-transparent green btn-outline btn-circle btn-sm active">
							<input type="radio" name="options" class="toggle" id="option1">Today</label>
						<label class="btn btn-transparent green btn-outline btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Week</label>
						<label class="btn btn-transparent green btn-outline btn-circle btn-sm">
							<input type="radio" name="options" class="toggle" id="option2">Month</label>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row number-stats margin-bottom-30">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="stat-left">
							<div class="stat-chart">
								<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break
								<div id="sparkline_bar"></div>
							</div>
							<div class="stat-number">
								<div class="title"> Total </div>
								<div class="number"> 2460 </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="stat-right">
							<div class="stat-chart">
								<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break
								<div id="sparkline_bar2"></div>
							</div>
							<div class="stat-number">
								<div class="title"> New </div>
								<div class="number"> 719 </div>
							</div>
						</div>
					</div>
				</div>
				<div class="table-scrollable table-scrollable-borderless">
					<table class="table table-hover table-light">
						<thead>
							<tr class="uppercase">
								<th colspan="2"> MEMBER </th>
								<th> Earnings </th>
								<th> CASES </th>
								<th> CLOSED </th>
								<th> RATE </th>
							</tr>
						</thead>
						<tr>
							<td class="fit">
								<img class="user-pic rounded" src="../assets/pages/media/users/avatar4.jpg"> </td>
							<td>
								<a href="javascript:;" class="primary-link">Brain</a>
							</td>
							<td> $345 </td>
							<td> 45 </td>
							<td> 124 </td>
							<td>
								<span class="bold theme-font">80%</span>
							</td>
						</tr>
						<tr>
							<td class="fit">
								<img class="user-pic rounded" src="../assets/pages/media/users/avatar5.jpg"> </td>
							<td>
								<a href="javascript:;" class="primary-link">Nick</a>
							</td>
							<td> $560 </td>
							<td> 12 </td>
							<td> 24 </td>
							<td>
								<span class="bold theme-font">67%</span>
							</td>
						</tr>
						<tr>
							<td class="fit">
								<img class="user-pic rounded" src="../assets/pages/media/users/avatar6.jpg"> </td>
							<td>
								<a href="javascript:;" class="primary-link">Tim</a>
							</td>
							<td> $1,345 </td>
							<td> 450 </td>
							<td> 46 </td>
							<td>
								<span class="bold theme-font">98%</span>
							</td>
						</tr>
						<tr>
							<td class="fit">
								<img class="user-pic rounded" src="../assets/pages/media/users/avatar7.jpg"> </td>
							<td>
								<a href="javascript:;" class="primary-link">Tom</a>
							</td>
							<td> $645 </td>
							<td> 50 </td>
							<td> 89 </td>
							<td>
								<span class="bold theme-font">58%</span>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
-->

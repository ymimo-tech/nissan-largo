<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Dashboard 
        </h1>
    </div>
    <!-- END PAGE TITLE -->
    <!-- BEGIN PAGE TOOLBAR -->
    <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">
        <i class="icon-calendar"></i>&nbsp;
        <span class="thin uppercase hidden-xs"></span>&nbsp;
        <i class="fa fa-angle-down"></i>
    </div>
    <!-- END PAGE TOOLBAR -->
</div>

<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup" data-value="<?php echo $open_po;?>"><?php echo $open_po;?></span>
                    </h3>
                    <small>OPEN PO</small>
                </div>
                <div class="icon">
                    <i class="icon-basket"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                        <!--span class="sr-only">45% grow</span-->
                    </span>
                </div>
                <div class="status">
                    <!-- <div class="status-title"> to total po </div>
                    <div class="status-number"> 45% </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="<?php echo $total_inbound;?>"><?php echo $total_inbound;?></span>
                        
                    </h3>
                    <small>TOTAL INBOUND</small>
                </div>
                <div class="icon">
                    <i class="icon-arrow-left"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <!-- <div class="status-title"> compared to last month </div>
                    <div class="status-number"> 76% </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup" data-value="<?php echo $open_do;?>"><?php echo $open_do;?></span>
                    </h3>
                    <small>OPEN DO</small>
                </div>
                <div class="icon">
                    <i class="icon-basket"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
                        <!--span class="sr-only">45% grow</span-->
                    </span>
                </div>
                <div class="status">
                    <!-- <div class="status-title"> to total po </div>
                    <div class="status-number"> 45% </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup" data-value="<?php echo $total_outbound;?>"><?php echo $total_outbound;?></span>
                    </h3>
                    <small>TOTAL OUTBOND</small>
                </div>
                <div class="icon">
                    <i class="icon-arrow-right"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 100%;" class="progress-bar progress-bar-success red-haze">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <!-- <div class="status-title"> compared to last month </div>
                    <div class="status-number"> 85% </div> -->
                </div>
            </div>
        </div>
    </div>

 <div class="row">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Inbound QTY</span>
                    <span class="caption-helper">monthly stats...</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="site_statistics_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="site_statistics_content" class="display-none">
                    <div id="site_statistics" class="chart"> </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo hide"></i>
                    <span class="caption-subject font-red-sunglo bold uppercase">Outbond QTY</span>
                    <span class="caption-helper">monthly stats...</span>
                </div>

            </div>
            <div class="portlet-body">
                <div id="site_activities_loading">
                    <img src="<?php echo base_url(); ?>assets/global/img/loading.gif" alt="loading" /> </div>
                <div id="site_activities_content" class="display-none">
                    <div id="site_activities" class="chart" > </div>
                </div>
                
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>


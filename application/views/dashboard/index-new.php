<style>
	#chart_6 a, #chart_7 a, #chart_inbound a, #chart_outbound a{
		display: none !important;
	}

    .text-bold{
        font-weight:bold;
    }

    .progress-bar-stroge{
        width: 0%;
    }

    #chartdiv{
        margin-bottom:40px;
    }

    #chartdiv-inbound,#chartdiv-outbound {
      width     : 100%;
      height    : 250px;
      font-size : 11px;
    }

    .amcharts-chart-div > a {
        display: none !important;
    }

    .canvasjs-chart-credit{
        display: none;
    }


    .chart-pie-widget-box{
    }

    .chart-pie-widget-box div{
        height: 200px;
    }

    .inbound-average-box .line-divider, .outbound-average-box .line-divider{
        border:0;
        border-bottom:1px solid #dedede;
        border-style: dashed;
    }

    .inbound-average, .outbound-average{
        width: 100%;
    }

    .inbound-average tbody tr, .outbound-average tbody tr{
        border:0;
        border-bottom:1px solid #dedede;
        border-style: dashed;
        height:45px;
    }

    .inbound-average tbody tr:nth-child(1),
    .inbound-average tbody tr:nth-child(3),
    .outbound-average tbody tr:nth-child(3){
        border:0;
    }

    .inbound-average tbody tr td,
    .outbound-average tbody tr td{
        color:#B9BBC4;
    }

    .label-chart-container{
        height:200px;
        position: relative;
    }

    .label-chart-box{
        width: 100%;
        height: 100px;
        position: relative;
        top: 50%;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .label-chart-box tbody tr{
        /*border:1px solid;*/
    }

    .label-chart-box tbody tr td{
        font-size: 11px;
        color: #cecece;
        /*border:1px solid;*/
    }

    .label-chart{
        width: 20px;
        height: 5px;
        border-radius: 10px !important;
    }

    .label-chart-purple{
        background-color: #716ACA;
    }

    .label-chart-yellow{
        background-color: #FFB822;
    }

    .label-chart-blue{
        background-color: #00C5DC;
    }

    .text-darkgrey{
        color:#666674;
    }

    .text-lightgrey{
        color:#B9BBC4;
    }

    .text-blue{
        color:#00C5DC;
    }

    .text-purple{
        color: #716ACA;
    }

    .text-yellow{
        color:#FFB822;;
    }

    .text-orange{
        color:#FF6406;
    }

    .text-red{
        color:#F4516C;
    }

    .text-green{
        color: #34BFA3;
    }

    .bg-yellow{
        background-color: #FFB822;
    }

    .bg-orange{
        background-color:#FF6406;
    }

    .bg-red{
        background-color:#F4516C;
    }

    .bg-green{
        background-color:#34BFA3;
    }

    .bg-blue{
        background-color:#36A3F7;
    }

    .bg-purple{
        background-color:#716ACA;
    }

    #bin-location-percentage{
        line-height:50px;
        font-size:30px;
        font-weight:bold;
        color: #000000;
    }

    #table-inbound thead tr th,
    #table-outbound thead tr th
    {
        color:#94A0BF;
        font-size: 11.5px;
    }

    #table-inbound tbody tr td
    {
        border:0;
        border-bottom:1px solid #dedede;
        border-style: dashed;
    }

    #table-inbound tbody tr td,
    #table-outbound tbody tr td
    {
        color: #A8A9B6;
    }

    #table-inbound tbody tr td:last-child,
    #table-outbound tbody tr td:last-child
    {
        color: #716ACA;
    }

     #chartdiv-inbound, #chartdiv-outbound{
        height: 250px; width: 100%;
     }

</style>

<div class="row">
	<input type="hidden" id="sessId" value="<?php echo session_id(); ?>" />
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<!-- BEGIN PAGE TITLE -->
				<div class="custom-page-title">
					<h1>DASHBOARD
						<small>Overview of Warehouse status</small>
					</h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<div class="col-md-6">
			</div>
		</div>
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-body">

                <div class="row">

                    <div class="col-md-4">

                        <div class="row">
                            <div class="col-md-7">
                                <h4 class="text-bold text-darkgrey">Active SKU</h4>
                                <h5 class="text-lightgrey">Registered Products</h5>
                            </div>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-3 text-right">
                                <h3 class="text-bold text-purple" id="active-sku">0</h3>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-7">
                                <h4 class="text-bold text-darkgrey">Hold SKU</h4>
                                <h5 class="text-lightgrey">Items currently on hold status</h5>
                            </div>
                            <div class="col-md-2">
                                <h3><i class="icon-clock text-red"></i></h3>
                            </div>
                            <div class="col-md-3 text-right">
                            <h3 class="text-bold text-red" id="hold-sku">0</h3>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-7">
                                <h4 class="text-bold text-darkgrey">QC SKU</h4>
                                <h5 class="text-lightgrey">Items in Quality Control</h5>
                            </div>
                            <div class="col-md-2">
                                <h3><i class="icon-clock text-red"></i></h3>
                            </div>
                            <div class="col-md-3 text-right">
                            <h3 class="text-bold text-green" id="qc-sku">0</h3>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-8">

                        <div class="row">
                            <div class="col-md-6">
                                <h4><b>Warehouse Capacity</b></h4>
                                <h5>Snapshot of your warehouse</h5>
                            </div>

                            <div class="col-md-3 pull-right text-right">
                                <h4 class="text-bold text-purple" id="filled-bin-location">0</h4>
                                <h5>Filled Bin Location</h5>
                            </div>

                            <div class="col-md-3 pull-right text-right">
                            <h4 class="text-bold text-purple" id="bin-locations">0</h4>
                                <h5>Bin Locations</h5>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-12">

                                <div class="progress" style="height:50px;">
                                    <div class="progress-bar text-left bg-orange " id="bin-location-percentage" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100" style="padding-left:20px">
                                    0%
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-3">

                                <h4>
                                    <b id="general-storage-capt" class="text-darkgrey">0%</b>
                                    <small class="pull-right text-lightgrey">General Items</small>
                                </h4>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger bg-red" id="general-storage-bar" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <h4>
                                    <b id="cold-room-storage-capt"  class="text-darkgrey">0%</b>
                                    <small class="pull-right text-lightgrey">Cold Storage</small>
                                </h4>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning bg-yellow" id="cold-room-storage-bar" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <h4>
                                    <b id="high-value-storage-capt" class="text-darkgrey">0%</b>
                                    <small class="pull-right text-lightgrey">High Value Items</small>
                                </h4>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-success bg-green" id="high-value-storage-bar" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <h4>
                                    <b id="damage-item-storage-capt" class="text-darkgrey">0%</b>
                                    <small class="pull-right text-lightgrey">Damage Items</small>
                                </h4>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-info bg-blue" id="high-value-storage-bar" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-body">

                <div class="row">

                    <div class="col-md-3">

                        <div class="">
                            <h4 class="text-bold text-darkgrey">Replenishment</h4>
                            <h5 class="text-lightgrey">Items required re-stock or re-Order</h5>
                            <h1 class="text-right text-bold" id="replenishment" style="color:#716ACA;">0</h1>
                        </div>

                    </div>

                    <div class="col-md-3">

                        <div class="">
                            <h4 class="text-bold text-darkgrey">Expired</h4>
                            <h5 class="text-lightgrey">Items that are already expired</h5>
                            <h1 class="text-right text-bold" style="color:#00C5DC;" id="expired">0</h1>
                        </div>

                    </div>

                    <div class="col-md-3">

                        <div class="">
                            <h4 class="text-bold text-darkgrey">Non Moving</h4>
                            <h5 class="text-lightgrey">Items with no transaction in 6 months</h5>
                            <h1 class="text-right text-bold" style="color:#F4516C;" id="non-moving">0</h1>
                        </div>

                    </div>

                    <div class="col-md-3">

                        <div class="">
                            <h4 class="text-bold text-darkgrey">Shelf Life</h4>
                            <h5 class="text-lightgrey">Items with Shelf Life Monitoring</h5>
                            <h1 class="text-right text-bold" style="color:#34BFA3" id="shelf-life">0</h1>
                        </div>

                    </div>

                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <div class="">
                        <h4 class="text-bold text-darkgrey">WAREHOUSE ACTIVITIES</h4>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row" style="margin-bottom: 50px;">
                    <div class="col-md-9">
                        <div class="row" id="chartdiv">
                            <div class="col-md-7">
																<h4>Today's Inbound Activity</h4>
                                <div id="chartdiv-inbound"></div>
                            </div>

                            <div class="col-md-5">
                                <div class="row inbound-average-box">
                                    <div class="col-md-12">
                                        <h4>Inbound Processing Time</h4>
                                    </div>
                                    <div class="col-md-7">
                                        <table class="inbound-average">
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td align="right"></td>
                                                </tr>
                                                <tr>
                                                    <td>Tally</td>
                                                    <td align="right"><span id="tally-average">0</span> min/job</td>
                                                </tr>
                                                <tr>
                                                    <td>Putaway</td>
                                                    <td align="right"><span id="putaway-average">0</span> min/job</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <h4 class="text-uppercase">Average</h4>
                                        <h1 id="total-inbound-average" class="text-blue text-bold">0</h1>
                                        <span class="text-lightgrey">min/job</span>
                                    </div>
                                    <div class="col-md-11">
                                        <div class="line-divider"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-3">

                                <div class="text-center">
                                    <h4 class="text-bold text-darkgrey">New Inbound</h4>
                                    <h1 class="text-bold text-blue" id="new-inbound-total">0</h1>
                                    <div>
                                        <span id="new-inbound-total-item " class="text-lightgrey">0 SKUs</span>
                                        <br/>
                                        <span id="new-inbound-total-item-qty" class="text-lightgrey">0 pcs</span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="text-center">
                                    <h4 class="text-bold text-darkgrey">Open Inbound</h4>
                                    <h1 class="text-bold text-blue" id="open-inbound-total">0</h1>
                                    <div>
                                        <span id="open-inbound-total-item" class="text-lightgrey">0 SKUs</span>
                                        <br/>
                                        <span id="open-inbound-total-item-qty" class="text-lightgrey">0 pcs</span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="text-center">
                                    <h4  class="text-bold text-darkgrey">On Progress</b></h4>
                                    <h1 class="text-bold text-blue" id="on-progress-inbound-total">0</h1>
                                    <div>
                                        <span id="on-progress-inbound-total-item" class="text-lightgrey">0 SKUs</span>
                                        <br/>
                                        <span id="on-progress-inbound-total-item-qty" class="text-lightgrey">0 pcs</span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="text-center">
                                    <h4 class="text-bold text-darkgrey">Complete</h4>
                                    <h1 class="text-bold text-blue" id="complete-inbound-total">0</h1>
                                    <div>
                                        <span id="complete-inbound-total-item" class="text-lightgrey">0 SKUs</span>
                                        <br/>
                                        <span id="complete-inbound-total-item-qty" class="text-lightgrey">0 pcs</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
										<div class="col-md-3">
                        <div>
                            <button onclick="location.href=BaseUrl+'receiving'" class="btn btn-default pull-right" style="font-size:10px;border-radius:20px !important;">Go To List</button>
                            <h4>Open Receiving Jobs</h4>
                        </div>
                        <table class="table" id="table-inbound">
                            <thead>
                                <tr>
                                    <th>In. Doc</th>
                                    <th>Source</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
								<hr/>
                <div class="row">

                    <div class="col-md-9">
                        <div class="row" id="chartdiv">
                            <div class="col-md-7">
																<h4>Today's Outbound Activity</h4>
                                <div id="chartdiv-outbound"></div>
                            </div>
                            <div class="col-md-5">
                                <div class="row outbound-average-box">
                                    <div class="col-md-12">
                                        <h4>Outbound Processing Time</h4>
                                    </div>
                                    <div class="col-md-7">
                                        <table class="outbound-average">
                                            <tbody>
                                                <tr>
                                                    <td>Picking</td>
                                                    <td align="right"><span id="picking-average">0</span> min/job</td>
                                                </tr>
                                                <tr>
                                                    <td>Packing</td>
                                                    <td align="right"><span id="packing-average">0</span> min/job</td>
                                                </tr>
                                                <tr>
                                                    <td>Loading</td>
                                                    <td align="right"><span id="loading-average">0</span> min/job</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <h4 class="text-uppercase">Average</h4>
                                        <h1 id="total-outbound-average" class="text-purple text-bold">0</h1>
                                        <span class="text-lightgrey">min/job</span>
                                    </div>
                                    <div class="col-md-11">
                                        <div class="line-divider"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">

                                <div class="text-center">
                                    <h4 class="text-bold text-darkgrey">New Outbound</h4>
                                    <h1 class="text-bold text-purple" id="new-outbound-total">0</h1>
                                    <div>
                                        <span id="new-outbound-total-item" class="text-lightgrey">0 SKUs</span>
                                        <br/>
                                        <span id="new-outbound-total-item-qty" class="text-lightgrey">0 pcs</span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="text-center">
                                    <h4 class="text-bold text-darkgrey">Open Outbound</h4>
                                    <h1 class="text-bold text-purple" id="open-outbound-total">0</h1>
                                    <div>
                                        <span id="open-outbound-total-item" class="text-lightgrey">0 SKUs</span>
                                        <br/>
                                        <span id="open-outbound-total-item-qty" class="text-lightgrey">0 pcs</span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="text-center">
                                    <h4 class="text-bold text-darkgrey">On Progress</h4>
                                    <h1 class="text-bold text-purple" id="on-progress-outbound-total">0</h1>
                                    <div>
                                        <span id="on-progress-outbound-total-item" class="text-lightgrey">0 SKUs</span>
                                        <br/>
                                        <span id="on-progress-outbound-total-item-qty" class="text-lightgrey">0 pcs</span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">

                                <div class="text-center">
                                    <h4 class="text-bold text-darkgrey">Complete</h4>
                                    <h1 class="text-bold text-purple" id="complete-outbound-total">0</h1>
                                    <div>
                                        <span id="complete-outbound-total-item" class="text-lightgrey">0 SKUs</span>
                                        <br/>
                                        <span id="complete-outbound-total-item-qty" class="text-lightgrey">0 pcs</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

										<div class="col-md-3">

                        <div>
                            <button onclick="location.href=BaseUrl+'picking_list'" class="btn btn-default pull-right" style="font-size:10px;border-radius:10px !important;">Go To List</button>
                            <h4>Open Picking Jobs</h4>
                        </div>

                        <table class="table" id="table-outbound">
                            <thead>
                                <tr>
                                    <th>Out. Doc</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

<!-- BEGIN HIDDEN UNTIL NEEDED
<div class="row widget-row">

	<div class="col-md-4">

		<div class="widget-thumb widget-bg-color-white margin-bottom-20 ">

            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-bold text-darkgrey">Quality Controller</h4>
                    <small  class="text-lightgrey">Outcome summary of QC</small>
                </div>

                <div class="col-md-6">
                    <div class="chart-pie-widget-box">
                        <div id="chart-quality-control" class="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="label-chart-container">
                        <table class="label-chart-box">
                            <tbody>
                                <tr>
                                    <td><div class="label-chart label-chart-blue"></div></td>
                                    <td><span id="label-chart-damage">0</span>%</td>
                                    <td>Damage</td>
                                </tr>
                                <tr>
                                    <td><div class="label-chart label-chart-yellow"></div></td>
                                    <td><span id="label-chart-good">0</span>%</td>
                                    <td>Good</td>
                                </tr>
                                <tr>
                                    <td><div class="label-chart label-chart-purple"></div></td>
                                    <td><span id="label-chart-qc">0</span>%</td>
                                    <td>On Progress</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

		</div>

	</div>

	<div class="col-md-4">

		<div class="widget-thumb widget-bg-color-white margin-bottom-20 ">

            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-bold text-darkgrey">Return Products</h4>
                    <small class="text-lightgrey">Reasons why products are returned</small>
                </div>

                <div class="col-md-6">
                    <div class="chart-pie-widget-box">
                        <div id="chart-return-product" class="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="label-chart-container">
                        <table class="label-chart-box">
                            <tbody>
                                <tr>
                                    <td><div class="label-chart label-chart-blue"></div></td>
                                    <td><span>37%</span></td>
                                    <td>Damage</td>
                                </tr>
                                <tr>
                                    <td><div class="label-chart label-chart-yellow"></div></td>
                                    <td><span>42%</span></td>
                                    <td>Wrong Item</td>
                                </tr>
                                <tr>
                                    <td><div class="label-chart label-chart-purple"></div></td>
                                    <td><span>19%</span></td>
                                    <td>Expired</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


		</div>

	</div>

	<div class="col-md-4">

		<div class="widget-thumb widget-bg-color-white margin-bottom-20 " style="background:#7B77CC;color:white;">

            <div class="row">
                <div class="col-md-12">
                    <h4><b>Failed Deliveries</b></h4>
                    <small>Reasons why deliveries are failed</small>
                </div>

                <div class="col-md-6">
                    <div class="chart-pie-widget-box">
                        <div id="chart-failed-deliveries" class="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="label-chart-container">
                        <table class="label-chart-box">
                            <tbody>
                                <tr>
                                    <td><div class="label-chart label-chart-blue"></div></td>
                                    <td><span>37%</span></td>
                                    <td>Close</td>
                                </tr>
                                <tr>
                                    <td><div class="label-chart label-chart-yellow"></div></td>
                                    <td><span>42%</span></td>
                                    <td>Warehouse Full</td>
                                </tr>
                                <tr>
                                    <td><div class="label-chart label-chart-purple"></div></td>
                                    <td><span>19%</span></td>
                                    <td>Vehicle Issue</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>

	</div>

</div>

<div class="row" id="inventory-accuracy">
    <div class="col-md-12 col-sm-12">

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">

                    <div class="">
                        <h4><b>Inventory Accuracy</b></h4>
                        <h5>Warehouse health conditions</h5>
                    </div>

                </div>
            </div>
            <div class="portlet-body">


                <div class="row">

                    <div class="col-md-3">

                        <div class="">
                            <h4><b>Last Cycle Count</b></h4>
                            <h5>When you had your last cycle count</h5>
                            <h1 class="text-right text-bold" style="color:lightblue;" id="last-cycle-count">
                                No Data
                            </h1>
                            <span class="pull-right text-right" id="last-cycle-count-">9 months ago</span>
                        </div>

                    </div>

                    <div class="col-md-3">

                        <div class="">
                            <h4><b>SKU Checked</b></h4>
                            <h5>% SKU checked this year</h5>
                            <h2 class="text-right text-bold" style="color:lightblue;" id="sku-checked">0</h2>

                            <div class="">

                                <h5 class="text-bold" id="sku-checked-bar-title">0%</h5>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" id="sku-checked-bar-body" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-3">

                        <div class="">
                            <h4><b>Accuracy Level</b></h4>
                            <h5>How accurate the cycle count taken</h5>
                            <h1 class="text-right text-bold" id="accuracy-level" style="color:lightgreen">0%</h1>
                        </div>

                    </div>


                    <div class="col-md-3">

                        <div class="">
                            <h4><b>Error Rate</b></h4>
                            <h5>Discrepancies SKUs during cycle count</h5>
                            <h1 class="text-right text-bold" id="error-rate" style="color:lightgreen">0</h1>
                        </div>

                    </div>

                </div>

            </div>
			<div>&nbsp;</div>
        </div>

    </div>
</div>
END OF HIDDEN DASHBOARD PART -->

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>RECEIVING JOB DETAIL
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i> &nbsp;Document Info
					</span>
				</div>
                <div class="actions">
					<div class="top-info">
						<label>Created by : </label>
						<span class=""><?php echo !empty($data['user_name_receiving']) ? $data['user_name_receiving'] : '-'; ?></span>
					</div>
					<!--
					<div class="top-info">
						<label>Inbound Date : </label>
						<span class=""><?php echo $data['tanggal_inbound']; ?></span>
						<label>Doc. Type : </label>
						<span class=""><?php echo !empty($data['inbound_document_name']) ? $data['inbound_document_name'] : '-'; ?></span>
						<label>Created by : </label>
						<span class=""><?php echo !empty($data['user_name_inbound']) ? $data['user_name_inbound'] : '-'; ?></span>
					</div>
					-->
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="id_inbound" value="<?php echo $data['id_inbound']; ?>" />
						<input type="hidden" name="params" value="" />
						<!--
						<div class="row">
							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Document Number<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['kd_inbound']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['kd_inbound']; ?>
									</div>
								</div>
							</div>

							<div class="col-md-5">
								<div class="detail-caption text-left">
									<h1>Supplier<h1>
									<div class="bottom5 sup-title">
										<strong><?php echo $data['nama_supplier']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Code</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['kd_supplier']; ?></td>
										</tr>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['alamat_supplier']; ?></td>
										</tr>
										<tr>
											<td>Phone</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['telepon_supplier']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Contact Person</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $data['cp_supplier']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Status PO</h1>
									<div class="detail-status
										<?php
											if($data['nama_status'] == 'Open')
												echo 'status-blue';
											else
												echo 'status-green';
										?>">
										<?php echo $data['nama_status']; ?>
									</div>
								</div>
							</div>
						</div>

						<hr style="margin-top: 0;">
						-->

						<div class="row">
							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Receiving Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['kd_receiving']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['kd_receiving']; ?>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="detail-caption text-left">
									<table>
										<tr>
											<td>Inbound Doc. #</td>
											<td style="width: 60px;" align="center">:</td>
											<td><strong><?php echo anchor(base_url('inbound/detail/'.$data['id_inbound']),$data['kd_inbound']); ?></strong></td>
										</tr>
										<tr>
											<td>Transporter #</td>
											<td align="center">:</td>
											<td><?php echo $data['vehicle_plate']; ?></td>
										</tr>
										<tr>
											<td>Driver Name</td>
											<td align="center">:</td>
											<td><?php echo $data['dock']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Delivery Doc. #</td>
											<td align="center">:</td>
											<td><?php echo $data['delivery_doc']; ?></td>
										</tr>
										<tr>
											<td style="width: 120px;">Receive Doc. Date</td>
											<td align="center">:</td>
											<td><?php echo $data['tanggal_receiving']; ?> <?php echo $data['time_receiving']; ?></td>
										</tr>
										<!--
										<tr>
											<td style="width: 100px;">Time Receiving</td>
											<td align="center">:</td>
											<td><?php echo $data['time_receiving']; ?></td>
										</tr>
										-->
										<tr>
											<td>Finish Tally</td>
											<td align="center">:</td>
											<td><?php echo $data['tally']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Finish Putaway</td>
											<td align="center">:</td>
											<td><?php echo $data['putaway']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-4">
								<div class="detail-caption">
									<h1>Status</h1>
									<div class="detail-status receiving-status">
										<?php echo $data['status']; ?>
									</div>
								</div>
							</div>
						</div>

						<hr>

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Tally Report
							</span>
							<a href="javascript:;" id="button-export-excel" class="btn blue btn-sm btn-export min-width120 pull-right" data-id="<?php echo $id; ?>"><i class="fa fa-file-excel-o"></i> &nbsp;EXPORT EXCEL</a>
							<!-- <a href="javascript:;" id="print-ol" class="btn blue btn-sm btn-export min-width120 pull-right" data-id="<?php echo $id; ?>" style="margin-right:20px;"><i class="fa fa-print"></i> &nbsp;Print All Outer Label</a> -->
						</div>

						<div class="row">
							<div class="col-md-12 table-responsive">
								<table id="table-list-inb" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th class="hide"> id_inbound_barang </th>
											<th class="hide"> id_barang </th>
											<th class="hide"> id_receiving </th>
											<th style="vertical-align:top;width: 30px;"> No </th>
											<th style="vertical-align:top"> <div align="center">Item Code</div> </th>
											<th style="vertical-align:top"> Item Name </th>
											<th style="vertical-align:top"> <div align="center">Item Substitute</div> </th>
											<th style="vertical-align:top"> <div align="center">Item Substitute Code</div> </th>
											<th style="vertical-align:top"> <div align="center">Ordered Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Expected Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Tallied Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Putaway Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Discrepancy Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Good Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">NG Qty</div> </th>
											<th style="vertical-align:top"> <div align="center">Routing</div> </th>
											<th style="vertical-align:top"> <div align="center">GR</div> </th>
											<th style="vertical-align:top"> <div align="center">Action</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
						<!--
						<hr>

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Receiving
							</span>
						</div>

						<div class="row">
							<div class="col-md-6">
								<!--<h4>Items</h4>--
							</div>
							<!--
							<div class="col-md-6">
								<div class="pull-right">
									<table>
										<tr>
											<td>Total Item</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-item">0</span></strong></td>
										</tr>
										<tr>
											<td>Total Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-qty">0</span></strong></td>
										</tr>
									</table>
								</div>
							</div>
							--
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Item Code</div> </th>
											<th> Item Name </th>
											<th style="width: 120px;"> <div align="center">Doc. Qty</div> </th>
											<th style="width: 120px;"> <div align="center">Received Qty</div> </th>
											<th style="width: 120px;"> <div align="center">Discrepancies</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
						-->
					</div>
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">

								<div class="pull-right">
									<a class="btn default min-width120" href="javascript:;" onclick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>

									<?php
									$class = '';
									if($data['finish_tally'] != NULL){
										$class = 'disabled-link';
									}
									?>

									<?php if ($this->access_right->otoritas('edit')) : ?>
										<?php if(empty($class)){ ?>
											<a href="<?php echo base_url() . $page_class; ?>/edit/<?php echo $id; ?>" class="btn blue min-width120"><i class="fa fa-pencil"></i> EDIT</a>
										<?php }else{ ?>
											<!--<a href="javascript:;" class="btn blue min-width120 <?php echo $class; ?>"><i class="fa fa-pencil"></i> EDIT</a>-->
										<?php } ?>

									<?php endif; ?>

									<?php if ($this->access_right->otoritas('print')) : ?>
										<?php if(empty($class)){ ?>
										<button id="button-print-tally" class="btn default min-width120 <?php echo $class; ?>" style="margin-right:5px" data-id="<?php echo $id; ?>"><i class="fa fa-print"></i> PRINT TALLY</a>
										<?php } ?>
									<?php endif; ?>

									<?php if ($this->access_right->otoritas('print')) : ?>
										<?php if(empty($class)){ ?>
										<button id="button-print-qty-barcode" class="btn default min-width120 <?php echo $class; ?>"><i class="fa fa-barcode"></i> PRINT LABEL</a>
										<?php } ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

				</form>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?php if ($this->access_right->otoritas('print')) { ?>
	<?php include 'print_modal.php'; ?>
<?php }?>

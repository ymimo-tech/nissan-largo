<div class="row">
	
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Process Receiving
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i></i><?php echo $title;?> 
				</div>
                <div class="actions">
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">	
					<input type="hidden" name="params" value="" />

					<div class="row">
						<div class="col-md-12">
							<div class="form-group rcv-code-group bottom0">
								<label class="col-md-2"><strong>Receiving Code</strong></label>
								<div class="controls col-md-4">
									<input type="hidden" id="rcv" name="rcv" value="<?php echo $rcv_id; ?>" />
									<select id="rcv-code" name="rcv_code" class="form-control">
										
									</select>
								</div>
							</div> 
							
							<div class="form-group rcv-date-group bottom0" style="margin-top: 20px;">
								<label class="col-md-2">Receiving Date</label>
								<div class="col-md-4">
									<span class="rcv-date"></span>
								</div>
							</div>
							<div class="form-group inb-group bottom0">
								<label class="col-md-2">Inbound Code</label>
								<div class="controls col-md-4">
									<span class="inb-code"></span>
								</div>
							</div> 
							<div class="form-group plate-group bottom0">
								<label class="col-md-2">Vehicle Plate</label>
								<div class="controls col-md-4">
									<span class="plate"></span>
								</div>
							</div> 
							
							<div class="form-group driver-group bottom0">
								<label class="col-md-2">Driver</label>
								<div class="controls col-md-4">
									<span class="driver"></span>
								</div>
							</div> 
							
							<div class="form-group doc-group">
								<label class="col-md-2">Delivery Doc.</label>
								<div class="controls col-md-4">
									<span class="doc"></span>
								</div>
							</div> 
						   
						</div>
					</div>
					
					<hr style="margin-top: 0;">
					
					<div class="row">
						<div class="col-md-6">
							<h4>Items</h4>
						</div>
						<div class="col-md-6">
							<!--
							<div class="pull-right">
								<table>
									<tr>
										<td>Total Item</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><strong><span class="t-item">0</span></strong></td>
									</tr>
									<tr>
										<td>Total Qty</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><strong><span class="t-qty">0</span></strong></td>
									</tr>
								</table>
							</div>
							-->
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
								<thead>
									<tr>
										<th style="width: 30px;"> No </th>
										<th style="width: 130px;"> <div align="center">Item Code<div> </th>
										<th> Item Name </th>
										<th style="width: 120px;"> <div align="center">Quantity</div> </th>
										<th style="width: 120px;"> <div align="center">Received</div> </th>
										<th style="width: 120px;"> <div align="center">Discrapencies</div> </th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
			
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">
								
								<div class="pull-right">
									<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> Back</a>
									 <?php if ($this->access_right->otoritas('add')) : ?>
										
										<button type="button" class="btn blue min-width120" id="save" name="save">Save</button>
										
									 <?php endif; ?>
									 
									 <?php if ($this->access_right->otoritas('print')) : ?>
										
										<button id="button-print-qty-barcode" class="btn default min-width120"><i class="fa fa-print"></i> Print Barcode</a>
										
									 <?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					
				</form>
            </div>
     
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<?php if ($this->access_right->otoritas('print')) { ?>
	<?php include 'print_modal.php'; ?>
<?php }?>
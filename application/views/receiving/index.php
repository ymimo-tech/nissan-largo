<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>RECEIVING JOBS
				<small>List of receiving jobs for incoming items/materials</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row widget-row">
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">TODAY'S RECEIVING</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-green icon-bulb"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle"><?php echo $widget['total_item']; ?> Items</span>
					<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty']; ?>">
					<?php echo $widget['total_qty']; ?></span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">READY FOR TALLY</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-red icon-layers"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle"><?php echo $widget['total_tally']; ?> Items</span>
					<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty_tally']; ?>">
					<?php echo $widget['total_qty_tally']; ?></span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">READY FOR PUTAWAY</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle"><?php echo $widget['total_putaway']; ?> Items</span>
					<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $widget['total_qty_putaway']; ?>">
					<?php echo $widget['total_qty_putaway']; ?></span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
	<div class="col-md-3">
		<!-- BEGIN WIDGET THUMB -->
		<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
			<h4 class="widget-thumb-heading">AVG. RECEIVE TO BIN</h4>
			<div class="widget-thumb-wrap">
				<i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
				<div class="widget-thumb-body">
					<span class="widget-thumb-subtitle">Time/Item</span>
					<span class="widget-thumb-body-stat" style="font-size:25px" data-counter="" data-value="<?php echo $widget['average']; ?>">
					<?php if(!empty($widget['average'])) echo $widget['average']; else echo '0'; ?></span>
				</div>
			</div>
		</div>
		<!-- END WIDGET THUMB -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i>
						<?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<!-- <?php if ($this->access_right->otoritas('add')) : ?>

						<?php echo hgenerator::render_button_group($button_group); ?>

					<?php endif; ?> -->
					<?php if ($this->access_right->otoritas('print')) : ?>

						<?php echo hgenerator::render_button_group($print_group); ?>

					<?php endif; ?>
					<!--
					<div class="btn-group">
						<a href="http://localhost:81/largo-v2/putaway" class="btn default btn-sm green-haze min-width120">
							<i class="fa fa-location-arrow"></i> PUT AWAY
						</a>
					</div>
					-->

					<!--
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-plus"></i> Add </a>
					<a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-right"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

										<div class="form-group rcv-group">
											<label for="rcv" class="col-md-3">Receiving Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="rcv" id="rcv">
													</select>
												</div>
												<span class="help-block rcv-msg"></span>
											</div>
										</div>

										<div class="form-group inbound-group">
											<label for="inbound" class="col-md-3">Inbound Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="inbound" id="inbound">
													</select>
												</div>
												<span class="help-block inbound-msg"></span>
											</div>
										</div>

										<!--div class="form-group supplier-group">
											<label for="supplier" class="col-md-3">Supplier</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="supplier" class="form-control">
													<?php
														foreach($suppliers as $k => $v){
															echo '<option value="'.$k.'">'.$v.'</option>';
														}
													?>
													</select>
												</div>
												<span class="help-block supplier-msg"></span>
											</div>
										</div-->

<!-- 										<div class="form-group has-preoder-group">
											<label for="supplier" class="col-md-3">Has Preorder Item</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="has-preorder" class="form-control" style="width: 100%;">
													</select>
												</div>
												<span class="help-block supplier-msg"></span>
											</div>
										</div>
 -->
									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>

										<div class="form-group status-group">
											<label for="status" class="col-md-3">Doc. Status</label>
											<div class="col-md-8">

												<div class="custom-select select-type4 high location">
													<select class="form-control" name="status" id="status" style="width: 100%;" multiple data-placeholder="-- All --">

														<?php

															//echo '<option value="">-- All --</option>';

															foreach($status as $row){
																echo '<option value="'.$row['id_status'].'">'.$row['nama_status'].'</option>';
															}

														?>

													</select>
												</div>

												<span class="help-block status-msg"></span>
											</div>
										</div>

									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table-list">
                    <thead>
                        <tr>
														<th style="display: none;"> id_receiving </th>
														<th style="width: 30px;"> No </th>
														<th style="vertical-align:top"> <div align="center">Receiving Doc. #</div> </th>
														<th style="vertical-align:top"> <div align="center">Inbound Doc. #</div> </th>
														<th style="vertical-align:top"> <div align="center">Source</div> </th>
														<th style="vertical-align:top"> <div align="center">Transporter #</div> </th>
														<th style="vertical-align:top"> <div align="center">Doc. Time</div> </th>
														<!--<th> <div align="center">Receive</div> </th>-->
														<th style="vertical-align:top"> <div align="center">Finish Tally</div> </th>
														<th style="vertical-align:top"> <div align="center">Finish Putaway</div> </th>
														<th style="vertical-align:top"> <div align="center">Status</div> </th>
														<!-- <th> <div align="center">Has Pre-Order</div> </th> -->
														<th style="vertical-align:top"> <div align="center">Remark</div> </th>
                            <?php
                                if($this->access_right->otoritas('add') || $this->access_right->otoritas('edit'))
                                    echo "<th style='vertical-align:top'><div align='center'> Actions</div> </th>";
													  ?>
                        </tr>
                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="modal-remark" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal" class="form-horizontal">
				<input type="hidden" name="id"/>

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Finish Tally</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
						   <div class="form-group do-group">
								<label class="col-md-2 control-labe">Remark</label>
								<div class="controls col-md-9">
									<textarea class="form-control" id="remark" name="remark" placeholder="remark"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-remark" type="submit" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-check"></i> &nbsp;SUBMIT</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

<?php if ($this->access_right->otoritas('print')) { ?>
	<?php include 'print_modal.php'; ?>
<?php }?>

<script type="text/javascript">
    /*$(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        load_table('#content_table', 1);
        $('.onchange').change(function(){
            load_table('#content_table', 1);
        });
    });

    function refresh_filter(){
        load_table('#content_table', 1,function(){

        });
    }*/

</script>

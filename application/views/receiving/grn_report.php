<!DOCTYPE html>
<html>
<head>
	<title>Goods Receipt Note</title>
	<style type="text/css">
		body{
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		}
	</style>
</head>
<body>

	<div style="border:1px solid;text-align: center;font-weight: bold;font-size:20px;padding:10px;background:#FFF4C9">
		Good Receipt Note
	</div>

	<div style="margin: 20px 0;">
		<div style="float: left;width: 49.5%; ">
			<table style="font-size: 12px;">
				<tr>
					<td>Receipt No</td>
					<td>:</td>
					<td><?php echo $inbound['receiving_code']; ?></td>
				</tr>
				<tr>
					<td>Inbound Doc no</td>
					<td>:</td>
					<td><?php echo $inbound['inbound_code']?></td>
				</tr>
				<tr>
					<td>Delivery Doc</td>
					<td>:</td>
					<td><?php echo $inbound['delivery_doc']; ?></td>
				</tr>
			</table>
		</div>
		<div style="float: left;width: 49.5%;">
			<table style="font-size: 12px;">
				<tr>
					<td>Source</td>
					<td>:</td>
					<td><?php echo $inbound['source']; ?></td>
				</tr>
				<tr>
					<td>Truck</td>
					<td>:</td>
					<td><?php echo $inbound['truck']; ?></td>
				</tr>
				<tr>
					<td>Date</td>
					<td>:</td>
					<td><?php echo $inbound['receiving_date']; ?></td>
				</tr>
			</table>
		</div>
	</div>

	<div>
		<style type="text/css">
			table.table{
				border:1px solid;
				border:1px solid black;
				border-collapse: collapse;
			}

			table.table thead tr{
				background-color: #FFF4C9;
				font-weight: normal !important;
			}
			table.table thead tr{
				font-weight: normal !important;
			}

			table.table thead tr th{
				border:1px solid black;
				background-color: #FFF4C9;
			}

			table.table tbody tr td{
				border:1px solid;
			}
		</style>
		<table class="table" width="100%">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Items</th>
					<th rowspan="2">Description</th>
					<th rowspan="2">Preorder</th>
					<th colspan="3">QTY</th>
					<th rowspan="2">UoM</th>
					<th rowspan="2">Remark</th>
				</tr>
				<tr>
					<th>Expected</th>
					<th>Received</th>
					<th>Discrepancy</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i=1;
					foreach ($receivings as $r) {
				?>
				<tr>
					<td><?php echo $i;?></td>
					<td><?php echo $r['item_code'];?></td>
					<td><?php echo $r['item_name'];?></td>
					<td><?php echo $r['preorder'];?></td>
					<td style="text-align:center"><?php echo $r['doc_qty'];?></td>
					<td style="text-align:center"><?php echo $r['rec_qty'];?></td>
					<td style="text-align:center"><?php echo ($r['doc_qty']-$r['rec_qty']);?></td>
					<td style="text-align:center"><?php echo $r['unit'];?></td>
					<td style="text-align:center"><?php echo $r['remark'];?></td>
				</tr>
				<?php
				$i++;
				}
				?>
			</tbody>
		</table>
	</div>


	<div style="margin: 20px 0;margin-top: 40px;font-weight: bold;">
		-- QC DETAILS --
	</div>

	<div>
		<table class="table" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Item</th>
					<th>Description</th>
					<th>Preorder</th>
					<th>Qty Received</th>
					<th>UoM</th>
					<th>Locator</th>
					<th>Status (GOOD/REJECT)</th>
				</tr>
			</thead>
			<tbody>

				<?php
					$i=1;
					foreach ($receivingItem as $rItem) {
				?>
					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $rItem['item_code'];?></td>
						<td><?php echo $rItem['item_name'];?></td>
						<td><?php echo $rItem['preorder'];?></td>
						<td style="text-align:center"><?php echo $rItem['qty'];?></td>
						<td style="text-align:center"><?php echo $r['unit'];?></td>
						<td style="text-align:center"><?php echo $rItem['location'];?></td>
						<td style="text-align:center"><?php echo $rItem['qc'];?></td>
					</tr>
				<?php
					$i++;
					}
				?>
			</tbody>
		</table>
	</div>

	<div>
		<table width="100%">
			<tr>
				<td width="20" style="height: 200px;">Created By</td>
				<td width="20">Checked By</td>
				<td width="20">Acknowledged By</td>
			</tr>
			<tr>
				<td style="font-size: 12px;">(Admin)</td>
				<td style="font-size: 12px;">(Checker)</td>
				<td style="font-size: 12px;">(Warehouse SPV)</td>
			</tr>
		</table>
	</div>

</body>
</html>

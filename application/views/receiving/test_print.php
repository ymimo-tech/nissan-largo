<div class="modal-dialog">
    <div class="modal-content " >
        <div class="modal-body " >
            <div id="section-to-print"><div id="section-to-print">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>

            <div class="modal-body"  id="finput2">
                <div class="row">
                    <div class="col-md-12">

<?php 
    //session_start();

    include 'WebClientPrint.php';
    use Neodynamic\SDK\Web\WebClientPrint;
    use Neodynamic\SDK\Web\Utils;
    use Neodynamic\SDK\Web\DefaultPrinter;
    use Neodynamic\SDK\Web\InstalledPrinter;
    use Neodynamic\SDK\Web\ClientPrintJob;
    $tujuan = 'Tujuan:';
            echo $tujuan;
    // Process request
    // Generate ClientPrintJob? only if clientPrint param is in the query string
    $urlParts = parse_url($_SERVER['REQUEST_URI']);
    
    if (isset($urlParts['query'])){
        $rawQuery = $urlParts['query'];
        parse_str($rawQuery, $qs);
        if(isset($qs[WebClientPrint::CLIENT_PRINT_JOB])){
            $tujuan = 'Tujuan:';
            echo $tujuan;
            $useDefaultPrinter = ($qs['useDefaultPrinter'] === 'checked');
            $printerName = urldecode($qs['printerName']);

            //Create Zebra ZPL commands for sample label
            $cmds =  "^XA";
            $cmds .= "^FO20,30^GB750,1100,4^FS";
            $cmds .= "^FO20,30^GB750,200,4^FS";
            $cmds .= "^FO20,30^GB750,400,4^FS";
            $cmds .= "^FO20,30^GB750,700,4^FS";
            $cmds .= "^FO20,226^GB325,204,4^FS";
            $cmds .= "^FO30,40^ADN,36,20^FD".$tujuan."^FS";
            $cmds .= "^FO30,260^ADN,18,10^FDPart number #^FS";
            $cmds .= "^FO360,260^ADN,18,10^FDDescription:^FS";
            $cmds .= "^FO30,750^ADN,36,20^FDFrom:^FS";
            $cmds .= "^FO150,125^ADN,36,20^FDAcme Printing^FS";
            $cmds .= "^FO60,330^ADN,36,20^FD14042^FS";
            $cmds .= "^FO400,330^ADN,36,20^FDScrew^FS";
            $cmds .= "^FO70,480^BY4^B3N,,200^FD12345678^FS";
            $cmds .= "^FO150,800^ADN,36,20^FDMacks Fabricating^FS";
            $cmds .= "^XZ";

            /*$cmds = "^XA
                ^MMA
                ^PW320
                ^LL0240
                ^LS0
                ^BY3,3,71^FT38,104^BCN,,Y,N
                ^FD>;C001/3/1^FS
                ^FT39,162^A0N,23,24^FH\^FDAnti Acne^FS";                
            $cmds .= "^PQ1,0,1,Y^XZ";*/

            //Create a ClientPrintJob obj that will be processed at the client side by the WCPP
            $cpj = new ClientPrintJob();
            //set Zebra ZPL commands to print...
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
            //set client printer
            if ($useDefaultPrinter || $printerName === 'null'){
                $cpj->clientPrinter = new DefaultPrinter();
            }else{
                $cpj->clientPrinter = new InstalledPrinter($printerName);
            }

            //Send ClientPrintJob back to the client
            ob_start();
            ob_clean();
            echo $cpj->sendToClient();
            ob_end_flush();
            exit();
        }
    }
?>

    <!-- Store User's SessionId -->
    <input type="hidden" id="sid" name="sid" value="<?php echo session_id(); ?>" />
    
    <label class="checkbox">
        <input type="checkbox" id="useDefaultPrinter" /> <strong>Use default printer</strong> or...
    </label>
    <div id="loadPrinters">
    <br />
    WebClientPrint can detect the installed printers in your machine.
    <br />
    <input type="button" onclick="javascript:jsWebClientPrint.getPrinters();" value="Load installed printers..." />
                    
    <br /><br />
    </div>
    <div id="installedPrinters" style="visibility:hidden">
    <br />
    <label for="installedPrinterName">Select an installed Printer:</label>
    <select name="installedPrinterName" id="installedPrinterName"></select>
    </div>
            
    <br /><br />
    <input type="button" style="font-size:18px" onclick="javascript:jsWebClientPrint.print('useDefaultPrinter=' + $('#useDefaultPrinter').attr('checked') + '&printerName=' + $('#installedPrinterName').val());" value="Print Label..." />
        
    <script type="text/javascript">
        var wcppGetPrintersDelay_ms = 5000; //5 sec

        function wcpGetPrintersOnSuccess(){
            // Display client installed printers
            if(arguments[0].length > 0){
                var p=arguments[0].split("|");
                var options = '';
                for (var i = 0; i < p.length; i++) {
                    options += '<option>' + p[i] + '</option>';
                }
                $('#installedPrinters').css('visibility','visible');
                $('#installedPrinterName').html(options);
                $('#installedPrinterName').focus();
                $('#loadPrinters').hide();                                                        
            }else{
                alert("No printers are installed in your system.");
            }
        }

        function wcpGetPrintersOnFailure() {
            // Do something if printers cannot be got from the client
            alert("No printers are installed in your system.");
        }
    </script>
    
    <!-- Add Reference to jQuery at Google CDN -->
    <!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script-->

    <?php
    //Specify the ABSOLUTE URL to the php file that will create the ClientPrintJob object
    //In this case, this same page
    echo WebClientPrint::createScript(Utils::getRoot().'/PrintZPLSample/PrintLabel.php')
    ?>
       
                    </div>
                </div>
            </div>
            </div></div>
            <div class="modal-footer">
                <div class="form-actions">
                    <?php echo anchor(null, '<i class="icon-printer"></i> Print', array('id' => 'button-save', 'class' => 'green btn', 'onclick' => "print_barcode(this.id, '#finput2', '#button-back')")); ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
       /* $('.chosen').chosen();*/
        /*$('.numeric').numeric();*/
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

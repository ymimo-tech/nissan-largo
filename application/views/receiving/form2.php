<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                    $hidden_form = array('id' => !empty($id) ? $id : '');
                    echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label" >Tanggal<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <div class="input-icon right">
                                <?php echo form_input('tanggal_bbk', !empty($default->tanggal_bbk) ? date('d-m-Y', strtotime($default->tanggal_bbk)) : date('d-m-Y'), 'class="onchange form-control date " readonly=""'); ?>
                                <i class="icon-calendar form-control-feedback"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kode BBK<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_bbk', !empty($default->kd_bbk) ? $default->kd_bbk : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="col-md-3 control-label">Proyek<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_proyek', !empty($default->kd_proyek) ? $default->kd_proyek : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div>  

                    <span class="required">* harus diisi</span>
                    
                    
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
       /* $('.chosen').chosen();*/
        /*$('.numeric').numeric();*/
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

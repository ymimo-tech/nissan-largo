<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                    $hidden_form = array('id' => !empty($id) ? $id : '');
                    echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label" >Date<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <div class="input-icon right">
                                <?php echo form_input('tanggal_receiving', !empty($default->tanggal_receiving) ? date('d-m-Y', strtotime($default->tanggal_receiving)) : date('d-m-Y'), 'class="onchange form-control date " readonly=""'); ?>
                                <i class="icon-calendar form-control-feedback"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Receiving Code<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_receiving', !empty($default->kd_receiving) ? $default->kd_receiving : $curent_rcv_code, 'class="col-md-12 form-control" readonly') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Purchase Order<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_dropdown('kd_po',$options_purchase_order, !empty($default->id_po) ? $default->id_po : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Vehicle Plate<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('vehicle_plate', !empty($default->vehicle_plate) ? $default->vehicle_plate : '', 'class="col-md-12 form-control" ') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Driver<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('dock', !empty($default->dock) ? $default->dock : '', 'class="col-md-12 form-control" ') ?>  
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Delivery Doc<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('delivery_doc', !empty($default->delivery_doc) ? $default->delivery_doc : '', 'class="col-md-12 form-control" ') ?>  
                        </div>
                    </div> 
                    <span class="required">* harus diisi</span>
                    
                    
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <?php if(empty($st_delete)){ ?>
                    <?php echo anchor(null, '<i class="icon-save"></i> Save', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); } else { ?>
                    <?php echo anchor(null, '<i class="icon-trash"></i> Delete', array('id' => 'button-save', 'class' => 'red btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); } ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Back', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
       /* $('.chosen').chosen();*/
        /*$('.numeric').numeric();*/
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

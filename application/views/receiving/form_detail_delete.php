<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="box-content">
                        <?php
                        $hidden_form = array('id' => !empty($id) ? $id : '',
                            'id_bbm' => !empty($id_bbm) ? $id_bbm : '');
                        echo form_open($form_action, array('id' => 'finput-detail-'.$id_bbm, 'class' => 'form-horizontal'), $hidden_form);
                        ?>


                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Barang<span class="required">*</span></label>
                            <div class="controls col-md-9">
                                <?php echo form_dropdown('kd_barang',$options_barang, !empty($default->id_barang) ? $default->id_barang : '', 'class="form-control"') ?>
                                
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jumlah<span class="required">*</span></label>
                            <div class="controls col-md-9">
                                <?php echo form_input('jumlah', !empty($default->jumlah_barang) ? $default->jumlah_barang : '', 'class="form-control"') ?>
                                
                            </div>
                        </div> 

                        <span class="required">* harus diisi</span>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <?php echo anchor(null, '<i class="fa fa-trash-o"></i> Delete', array('id' => 'button-save', 'class' => 'btn red', 'onclick' => "simpan_data(this.id, '#finput-detail-$id_bbm', '#button-back')")); ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        
    });
    function refresh_filter(){ 
        load_table('#content_table_detail_<?php echo $id_bbm;?>', 1,function(){

        });
    }
</script>


<div class="inner_content">

    <div class="widgets_area container-fluid">
        <div class="row-fluid">
            <div class="col-md-12">
                <div id="index-content" class="well-content blue">
                    <div class="row">
                        <div class=" col-md-6">
                            <?php echo hgenerator::render_button_group($button_group); ?>
                        </div>
                        <div class=" col-md-6">
                            <div class="pull-right ">
                                <?php
                                if ($this->access_right->otoritas('print')) 
                                    echo hgenerator::render_button_group($print_group);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-md-12">
                            <?php echo form_open_multipart('', array('id' => 'ffilter_detail_'.$id_bbm,'tools-filter'=>FALSE)); ?>
                            <?php echo form_hidden('id_bbm',$id_bbm);?>
                            
                            <div id="content_table_detail_<?php echo $id_bbm;?>" data-source="<?php echo $data_source; ?>" data-filter="#ffilter_detail_<?php echo $id_bbm;?>"></div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        load_table('#content_table_detail_<?php echo $id_bbm;?>', 1);
        $('.onchange').change(function(){
            load_table('#content_table_detail_<?php echo $id_bbm;?>', 1);
        });
    });

    function refresh_filter(){ 
        load_table('#content_table_detail_<?php echo $id_bbm;?>', 1,function(){

        });
    }

</script>


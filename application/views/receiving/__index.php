<!--	author : Nisa Safardian 
  last edited : 28.02.14
 -->
<div class="inner_content">
    <div class="user_bar">
        <div class="row-fluid">
            <div class="float_left">
                <h3><span><?php echo (isset($title)) ? $title : 'Untitle'; ?></span></h3>
            </div>
        </div>
    </div>
    <div class="widgets_area">
        <div class="row-fluid">
            <div class="span6">
                <div id="index-content" class="well-content">
                    <div class="well">
                        <div class="pull-left">
                            <?php echo hgenerator::render_button_group($button_group); ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            if ($this->access_right->otoritas('print')) 
                                echo hgenerator::render_button_group($print_group);
                            ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span12">
                            <div class="well">
                                <div class="well-content clearfix">
                                    <?php echo form_open_multipart('', array('id' => 'ffilter')); ?>
                                        <div class="form_row">
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Kode BBM :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_input('kd_bbm','', 'class="span12 onchange"') ?>
                                                </div>
                                            </div>  
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Purchase Order :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_input('kd_po', '', 'class="span12 onchange"') ?>
                                                </div>
                                            </div>      
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Supplier :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_input('kd_supplier', '', 'class="span12 onchange"') ?>
                                                </div>
                                            </div> 
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Tahun Aktif :</label>
                                                </div>
                                                <div>
                                                    <?php echo form_dropdown('tahun_aktif',$tahun_aktif,date('Y'), 'class="span12 onchange chosen"') ?>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="form_row">
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Tanggal Awal BBM :</label>
                                                </div>
                                                <div class="input-append date" id="dp3" style="margin-right: 30px;" data-date="" data-date-format="dd-mm-yyyy">
                                                    <?php echo form_input('tanggal_awal_bbm', '', 'class="span12 onchange date" readonly=""'); ?>
                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                </div>
                                            </div> 
                                            <div class="pull-left span3">
                                                <div>
                                                    <label>Tanggal Akhir BBM :</label>
                                                </div>
                                                <div class="input-append date" id="dp3" style="margin-right: 30px;" data-date="" data-date-format="dd-mm-yyyy">
                                                    <?php echo form_input('tanggal_akhir_bbm', '', 'class="span12 onchange date" readonly=""'); ?>
                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                </div>
                                            </div>   
                                                           
                                        </div>
   
                                    <?php echo form_close(); ?>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div id="content_table" data-source="<?php echo $data_source; ?>" data-filter="#ffilter"></div>
                    <div>&nbsp;</div>
                </div>
                
                <div id="form-content" class="modal fade modal-xlarge"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        load_table('#content_table', 1);
        $('.onchange').change(function(){
            load_table('#content_table', 1);
        });
    });

    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }

</script>


<tr class="added_row">
	<td><?php echo $seq+1; ?></td>
	<td class="form-group">
		<?php echo form_dropdown("id_barang[]", $items, isset($outbound_barang) ? $outbound_barang['id_barang'] : "", "class='form-control add_rule item-outbound'"); ?>
	</td>
	<td class="form-group">
		<div class="pull-right">
			<?php echo form_input("item_quantity[]", isset($outbound_barang) ? $outbound_barang['jumlah_barang'] : "", 'class="col-md-12 form-control add_rule" style="width: 100px;"'); ?>
			&nbsp;&nbsp;
<!-- 			<label class="label-control uom" style="margin-top: 7px;">
				<?php
					if(isset($outbound_barang['nama_satuan']))
						echo $outbound_barang['nama_satuan'];
					else
						echo'UoM';
				?>				
			</label>-->
		</div>
	</td>
    <td class="form-group">
        <div class="pull-right">
            <select name="unit[]" class="form-control uom">
                <?php
	                if(isset($outbound_barang)){
                        echo "<option value='".$outbound_barang['unit_id']."'>".$outbound_barang['unit_code']."</option>";
	                }
                ?>
            </select>
        </div>
    </td>
	<td class="form-group batch-column" style="text-align: center; display: none;">
		<?php
			if(!empty($outbound_barang['batch'])){
				$opt = '';
				foreach($batchs as $r){
					$sel = '';
					if($r['kd_batch'] == $outbound_barang['batch'])
						$sel = 'selected';
					
					$opt .= '<option value="'.$r['kd_batch'].'" '.$sel.'>'.$r['kd_batch'].'</option>';
				}
				
				echo '<select name="batchcode['.$outbound_barang['id_barang'].']" class="form-control mbatch add_rule" style="width: 100%;">
						'.$opt.'
					  </select>';
			}else
				echo '-';
		?>	
	</td>
	<td class="remove_row">
		<button class="btn btn-delete btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
	</td>
</tr>
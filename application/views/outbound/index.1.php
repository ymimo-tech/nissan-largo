<div class="row">
	
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Outbound
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?> 
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
                                
						<?php echo hgenerator::render_button_group($button_group); ?>
							
					<?php endif; ?>
					<!--
                    <a class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-up"></i> Create Picking </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">
			
				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">	
								
								
								<div class="row">
									<div class="col-md-6">
										
										<div class="form-group po-group">
											<label for="po" class="col-md-3">Doc. Number</label>
											<div class="col-md-8">												
												<div class="custom-select select-type4 high location">
													<select class="form-control" 
														name="po" id="po">
													</select>
												</div>
												<span class="help-block po-msg"></span>
											</div>
										</div>
										
										<div class="form-group supplier-group">
											<label for="supplier" class="col-md-3">Customer</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="customer" class="form-control">
													</select>
												</div>
												<span class="help-block supplier-msg"></span>
											</div>
										</div>
										
										<div class="form-group doc-type-group">
											<label for="doc-type" class="col-md-3">Doc. Type</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="doc-type" class="form-control">
													<?php
														
														foreach($outbound_docs as $k => $v){
															echo '<option value="'.$k.'">'.$v.'</option>';
														}
														
													?>
													</select>
												</div>
												<span class="help-block doc-type-msg"></span>
											</div>
										</div>
										
									</div>	
									
									<div class="col-md-6">
									
										<div class="form-group year-group">
											<label for="year" class="col-md-3">Periode</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">
													
												</select>
											
												<span class="help-block year-msg"></span>
											</div>
										</div>
									
										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>" 
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>
										
										<div class="form-group doc-status-group">
											<label for="doc-status" class="col-md-3">Doc. Status</label>
											<div class="col-md-6">
												<div class="custom-select select-type4 high location">
													<select id="doc-status" class="form-control">
													
													</select>
												</div>
												<span class="help-block doc-status-msg"></span>
											</div>
										</div>
										
									</div>
									
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->
			
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <th class="table-checkbox">
                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
                            <th> No </th>
                            <th> Doc. Number </th>
                            <th> Date </th>
                            <th> Doc. Type </th>
                            <th> Customer </th>
							<th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                    
                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form id="data-table-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Outbound Code<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('kd_outbound','', 'class="col-md-12 form-control"') ?>
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Date<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('tanggal_outbound','', 'class="col-md-12 form-control mdatepicker"') ?>
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Customer<span class="required">*</span></label>
                                            <div class="controls col-md-7">
                                                <?php echo form_dropdown('id_customer', $customers, '', 'data-live-search="true" class="selectpicker col-md-12 form-control"') ?>
                                            </div>
                                            <div class="controls col-md-2">
                                                <span id="new_customer" class="btn default form-control">Create new</span>
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Address</label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('alamat','', 'class="col-md-12 form-control"') ?>
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Outbound Type<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_dropdown('jenis_outbound', $outbound_docs, '', 'data-live-search="true" class="selectpicker col-md-12 form-control"') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="outbound-barang">
                                            <thead>
                                                <tr>
                                                    <th> No </th>
                                                    <th> Item Name </th>
                                                    <th> Quantity </th>
                                                    <th> Action </th>
                                                </tr>
                                            </thead>
                                            <tbody id="added_rows">
                                            </tbody>
                                            <tbody>
                                                <tr><td colspan="4" class="add_item">Add Item</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* harus diisi</span>
                                    <button type="submit" class="btn blue"><i class="icon-save"></i> Simpan</button>
                                    <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Kembali</a>
                                </div>
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>

            <div id="customer-form-content" class="modal fade" role="basic">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <form id="data-table-form-customer" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">New Customer</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Name<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('customer_name','', 'class="col-md-12 form-control"') ?>
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Phone<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('phone','', 'class="col-md-12 form-control"') ?>
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Address</label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('address','', 'class="col-md-12 form-control"') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* harus diisi</span>
                                    <button type="submit" class="btn blue"><i class="icon-save"></i> Simpan</button>
                                    <a class="form-close btn default"><i class="icon-circle-arrow-left"></i> Kembali</a>
                                </div>
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
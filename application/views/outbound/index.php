<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>OUTBOUND DOCUMENTS
				<small>List of documents for outgoing items/materials</small>
			</h1>
			<br/>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
									<div style="display:none;">

					<?php if ($this->access_right->otoritas('add')) : ?>

						<?php echo hgenerator::render_button_group($button_group); ?>

					<?php endif; ?>
				</div>
					<!--a  class="btn green btn-sm btn-create-picking min-width120">
						<i class="fa fa-file-o"></i> CREATE PICKING </a-->
					<a  class="btn green-haze btn-sm btn-sync min-width120">
						<i class="fa fa-refresh"></i> SYNC </a>
	
                    <a class="btn blue btn-sm min-width120" href="<?php echo base_url().'Api_integration/autoCreatePL'; ?>" target="_blank">
                        <i class="fa fa-plus"></i> Auto Create </a>

					<a id='export-so' class="btn blue btn-sm min-width120" target="_blank">
                        <i class="fa fa-file-excel-o"></i> Export SO </a>
					<!-- 
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-up"></i> Create Picking </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
                </div>
            </div>
            <div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

										<div class="form-group kd-outbound-group">
											<label for="po" class="col-md-4">Outbound Doc. #</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select class="form-control" name="do" id="kd-outbound">
													</select>
												</div>
												<span class="help-block po-msg"></span>
											</div>
										</div>

										<div class="form-group supplier-group">
											<label for="supplier" class="col-md-4">Destination</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="customer" class="form-control">
													</select>
												</div>
												<span class="help-block supplier-msg"></span>
											</div>
										</div>

										<div class="form-group doc-type-group">
											<label for="doc-type" class="col-md-4">Doc. Type</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="doc-type" class="form-control">
													<?php

														foreach($outbound_docs as $k => $v){
															echo '<option value="'.$k.'">'.$v.'</option>';
														}

													?>
													</select>
												</div>
												<span class="help-block doc-type-msg"></span>
											</div>
										</div>

										<div class="form-group doc-type-group">
											<label for="doc-type" class="col-md-4">Priority</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
													<select id="priority" class="form-control">
														<option value="">--Priority--</option>
														<option value="1">Normal</option>
														<option value="2">High</option>
													</select>
												</div>
												<span class="help-block doc-type-msg"></span>
											</div>
										</div>

									</div>

									<div class="col-md-6">

										<div class="form-group year-group">
											<label for="year" class="col-md-3">Period</label>
											<div class="col-md-6">

												<select id="periode" class="form-control">

												</select>

												<span class="help-block year-msg"></span>
											</div>
										</div>

										<div class="form-group periode-group">
											<label for="periode" class="col-md-3">Date</label>
											<div class="col-md-8">
												<div class="input-group input-large date-picker input-daterange">
													<input type="text" class="form-control" name="from" id="from" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
													<span class="input-group-addon">
													to </span>
													<input type="text" class="form-control" name="to" id="to" value="<?php echo $today; ?>"
														placeholder="dd-mm-yyyy" />
												</div>
												<span class="help-block periode-msg"></span>
												<!-- /input-group -->
											</div>
											<div class="col-md-1">
												<button id="clear-date" type="button" class="btn clear-date btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
											</div>
										</div>

										<div class="form-group doc-status-group">
											<label for="doc-status" class="col-md-3">Doc. Status</label>
											<div class="col-md-8">

												<div class="custom-select select-type4 high location">
													<select class="form-control" name="doc-status" id="doc-status" style="width: 100%;" multiple data-placeholder="-- All --">
														<?php

															foreach($doc_status as $k => $v){
																echo '<option value="'.$k.'">'.$v.'</option>';
															}

														?>
													</select>
												</div>

												<span class="help-block status-msg"></span>
											</div>
										</div>

									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
											<button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

				<div class="row">
					<div class="col-md-2" style="font-weight: bold;">
						<span>Total Selected :</span>
						<span id="total-selected">0</span>
						<span>Documents</span>
					</div>
					<div class="col-md-2" style="font-weight: bold;">
						<span>Total Weight :</span>
						<span id="total-weight">0</span>
						<span><?php echo WEIGHT_UOM?></span>
					</div>
					<div class="col-md-2" style="font-weight: bold;">
						<span>Total Volume :</span>
						<span id="total-volume">0</span>
						<span><?php echo VOLUME_UOM?></span>
					</div>
				</div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <th style="display: none;"> id_outbound </th>
														<!-- Bug: checkbox select all ini masih bisa memilih checkbox yg inactive. Diperbaiki dulu baru diaktifkan kembali -->
														<th style="vertical-align:top" class="table-checkbox"> <!--<input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" />--> </th>
														<!-- End of Bug Info -->
														<th style="vertical-align:top"> <div align="center">No</div> </th>
                            <th style="vertical-align:top"> <div align="center">Outbound Doc. #</div> </th>
                            <th style="vertical-align:top"> <div align="center">Doc. Date</div> </th>
                            <th style="vertical-align:top"> <div align="center">Ship Date</div> </th>
                            <th style="vertical-align:top"> <div align="center">Doc. Type</div> </th>
                            <th style="vertical-align:top"> <div align="center">Destination</div> </th>
                            <th style="vertical-align:top"> <div align="center">Weight</div> </th>
                            <th style="vertical-align:top"> <div align="center">Volume</div> </th>
                            <th style="vertical-align:top"> <div align="center">Priority</div> </th>
                            <th style="vertical-align:top"> <div align="center">Ship Group</div> </th>
														<th style="vertical-align:top"> <div align="center">Status</div> </th>
                            <th style="vertical-align:top"> <div align="center">Actions</div> </th>
                        </tr>
                    </thead>
                    <tbody>
										</tbody>
                </table>
            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div id="modal-multi-picking" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal-form" class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Create Picking</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							Are you sure want to create picking for selected outbound docs?
							<input type="hidden" name="ids_outbound" class="ids_outbound"/>
							<div class="list_outbound">

							</div>
						</div>

<!-- 						<div class="col-md-12" style="margin-top: 20px;">
							<label><strong>Priority</strong></label>
							<select class="form-control priority" name="priority">
								<option value="0">Normal</option>
								<option value="1">High</option>
							</select>
						</div>
 -->					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i> &nbsp;CLOSE</a>
						<button id="submit-multi-picking" class="btn blue min-width120" data-loading-text="Loading..."><i class="fa fa-file-o"></i> &nbsp;CREATE</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

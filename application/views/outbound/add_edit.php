<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>CREATE NEW OUTBOUND DOCUMENT
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i><?php echo $title; ?>
					</span>
				</div>
                <div class="actions">
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<div class="form-body">
					<form id="data-table-form" class="form-horizontal">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="params" value="" />
						<input type="hidden" name="not_used" value="<?php echo $not_usage; ?>" />

						<div class="row">
							<div class="col-md-12">
							   <div class="form-group">
									<label class="col-md-2">Outbound Doc. #<span class="required">*</span></label>
									<div class="controls col-md-6">
										<div style="">
											<input type="hidden" name="kd_outbound" class="form-control" placeholder="No. Outbound Document" />
											<strong><span class="outbound-code"></span></strong>
										</div>
									</div>
								</div>
							   <div class="form-group tanggal-outbound-group">
									<label class="col-md-2">Outbound Date<span class="required">*</span></label>
									<div class="col-md-6">
										<div class="input-group input-medium date date-picker tanggal_outbound">
											<input type="text" class="form-control" id="tanggal-outbound" name="tanggal_outbound" placeholder="dd/mm/yyyy"
												value="<?php echo $today; ?>" maxLength="10" />
											<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>
							   </div>
							   <div class="form-group">
									<label class="col-md-2">Doc Type<span class="required">*</span></label>
									<div class="controls col-md-6">
										<?php echo form_dropdown("id_outbound_document", [], "", "class='form-control' data-placeholder='-- Please Select --'"); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2"> From <span class="required">*</span></label>
									<div class="controls col-md-6">
										<select id="warehouse-form" name="warehouse" class="form-control">
										</select>
									</div>
								</div>

							   <div class="form-group delivery-date-group">
									<label class="col-md-2">Delivery Date<span class="required">*</span></label>
									<div class="col-md-6">
										<div class="input-group input-medium date date-picker delivery_date">
											<input type="text" class="form-control" id="delivery_date" name="delivery_date" placeholder="dd/mm/yyyy"
												value="<?php echo $today; ?>" maxLength="10" />
											<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</div>
							   </div>

 							   <div class="form-group">
									<label class="col-md-2">Destination<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select class="form-control" name="destination" id="destination">
											<?php
												if(isset($destination)){
											?>
												<option value="<?php echo $destination['destination_id'];?>"><?php echo $destination['destination_code'].' - '.$destination['destination_name'];?></option>
											<?php
												}
											?>
										</select>
									</div>
								</div>
<!--  							   <div class="form-group" id="cost-center-box" style="display: none;">
									<label class="col-md-2">Cost Center<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select class="form-control" name="cost_center" id="cost-center" style="width: 100%;">

											<option value=""> -- Please Select -- </option>

											<?php
												foreach ($cost_center as $costCenter) {
											?>
												<option value="<?php echo $costCenter['id'] ?>"><?php echo $costCenter['code'] ." - ". $costCenter['name'] ?></option>
											<?php
												}
											?>
										</select>
									</div>
								</div>
 -->								<div class="form-group">
								   <label class="col-md-2">Name<span class="required">*</span></label>
								   <div class="controls col-md-6">
									   <div style="">
										   <input type="text" id="name" name="name" class="form-control" value="" maxLength="50" />
									   </div>
								   </div>
							   </div>

 							   <div class="form-group">
								   <label class="col-md-2">Address<span class="required">*</span></label>
								   <div class="controls col-md-6">
									   <div style="">
										   <textarea id="addres" name="address" placeholder="address" class="form-control" value="" style="resize: none; height: 100px;"></textarea>
									   </div>
								   </div>
							   </div>

<!--  							   <div class="form-group">
									<label class="col-md-2">Destination<span class="required">*</span></label>
									<div class="controls col-md-6">
										<select class="form-control" name="destination" id="destination">
										</select>
									</div>
								</div>
 -->
<!-- 							   <div class="form-group">
								   <label class="col-md-2">Alamat<span class="required">*</span></label>
								   <div class="controls col-md-6">
									   <div style="">
										   <input type="text" id="DestinationAddressL1" name="DestinationAddressL1" placeholder="DestinationAddressL1" class="form-control" value="" maxLength="50" />
									   </div>
								   </div>
							   </div>
							   <div class="form-group">
								   <label class="col-md-2"><span class="required"></span></label>
								   <div class="controls col-md-6">
									   <div style="">
										   <input type="text" id="DestinationAddressL2" name="DestinationAddressL2" placeholder="DestinationAddressL2" class="form-control" value="" maxLength="50" />
									   </div>
								   </div>
							   </div>
							   <div class="form-group">
								   <label class="col-md-2"><span class="required"></span></label>
								   <div class="controls col-md-6">
									   <div style="">
										   <input type="text" id="DestinationAddressL3" name="DestinationAddressL3" placeholder="DestinationAddressL3" class="form-control" value="" maxLength="50" />
									   </div>
								   </div>
							   </div> -->
							   <div class="form-group">
								   <label class="col-md-2">Phone</label>
								   <div class="controls col-md-6">
									   <div style="">
										   <input type="text" id="Phone" name="Phone" class="form-control" value="" maxLength="50" />
									   </div>
								   </div>
							   </div>

							   <div class="form-group">
								   <label class="col-md-2">Shipping Group<span class="required">*</span></label>
								   <div class="controls col-md-6">
									   <div style="">
										   <input id="shipping_group" name="shipping_group" class="form-control" value="" />
									   </div>
								   </div>
							   </div>

							   <div class="form-group">
									<label class="col-md-2">Priority<span class="required">*</span></label>
									<div class="controls col-md-6">
										<?php echo form_dropdown("priority", $priority, "", "class='form-control' data-placeholder='-- Please Select --'"); ?>
									</div>
								</div>

							   <div class="form-group">
								   <label class="col-md-2">Shipping Note<span class="required">*</span></label>
								   <div class="controls col-md-6">
									   <div style="">
										   <textarea id="shipping-note" name="ShippingNote" class="form-control" value=""></textarea>
									   </div>
								   </div>
							   </div>
								<?php if(!empty($id)) { ?>
									<!--
									<div class="form-group">
										<label class="col-md-2">Status<span class="required">*</span></label>
										<div class="controls col-md-6">

										</div>
									</div>
									-->
								<?php } ?>
							</div>
						</div>

						<hr>
						<!--<h4>Items</h4>-->
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped table-bordered table-hover table-checkable order-column" id="inbound-barang">
									<thead>
										<tr>
											<th> No </th>
											<th> Item Name </th>
											<th style="text-align: center; width: 200px;"> Quantity </th>
                                            <th style="text-align: center; width: 200px;"> Uom </th>
											<th style="text-align: center; display: none;"> Batch </th>
											<th style="text-align: center"> Action </th></th>
										</tr>
									</thead>
									<tbody id="added_rows">
									</tbody>
									<tbody>
										<tr>
                                            <td colspan="5" class="add_item">
												<button type="button" class="btn blue min-width120 add-item"
													<?php if(empty($id)) { ?>data-toggle="tooltip" title="Click here to add item" data-placement="bottom"<?php } ?>><i class="fa fa-plus"></i> Add Item</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<span class="required pull-left">* is required</span>
									<div class="pull-right">

										<?php if(empty($id)) { ?>
											<a class="btn default min-width120" href="<?php echo base_url().$page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
										<?php } else { ?>
											<a class="btn default min-width120" href="javascript:;" onClick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
										<?php } ?>

										<?php if($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')){ ?>
											<button id="save" type="submit" class="btn blue min-width120"> SAVE</button>

											<?php if(empty($id)){ ?>
												<button id="save-new" type="button" class="btn green-haze min-width120"> SAVE & NEW</button>
											<?php } ?>

										<?php } ?>

									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

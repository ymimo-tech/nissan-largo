<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>OUTBOUND DETAIL
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-file"></i>
						Document Info
					</span>
				</div>
                <div class="actions">
					<div class="top-info">
						<label>Date : </label>
						<span class=""><?php echo $data['tanggal_outbound']; ?></span>
						<label>Doc. Type : </label>
						<span class=""><?php echo !empty($data['outbound_document_name']) ? $data['outbound_document_name'] : '-'; ?></span>
						<label>Created by : </label>
						<span class=""><?php echo !empty($data['user_name']) ? $data['user_name'] : '-'; ?></span>
					</div>
					<!--
					<table class="table-info">
						<tr>
							<td><strong>Date</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Doc. Type</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Created by</strong></td>
							<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
							<td></td>
						</tr>
					</table>
					-->
					<!--
                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>-->
                </div>
            </div>
            <div class="portlet-body form">
				<form id="data-table-form" class="form-horizontal">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<input type="hidden" name="params" value="" />

						<div class="row">
							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Outbound Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $data['kd_outbound']; ?>&size=45" style="">
									<div class="detail-info">
										<?php echo $data['kd_outbound']; ?>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>From<h1>
									<div class="bottom5 sup-title">
										<strong><?php echo $data['warehouse_from']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td>
												<?php echo $data['warehouse_address']; ?><br/>
											</td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>Destination<h1>
									<!--<?php if($data['id_outbound_document'] == '2'){ ?>
										<div class="bottom5 sup-title">
											<strong><?php echo $data['nama_supplier']; ?></strong>
										</div>
										<table>
											<tr>
												<td>Code</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $data['kd_supplier']; ?></td>
											</tr>
											<tr>
												<td>Address</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $data['alamat_supplier']; ?></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $data['telepon_supplier']; ?></td>
											</tr>
											<tr>
												<td style="width: 100px;">Contact Person</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $data['cp_supplier']; ?></td>
											</tr>
										</table>
									<?php }else{ ?>
										<div class="bottom5 sup-title">
											<strong><?php echo $data['customer_name']; ?></strong>
										</div>
										<table>
											<tr>
												<td>Address</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $data['address']; ?></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $data['phone']; ?></td>
											</tr>
										</table>
									<?php } ?>-->
									<div class="bottom5 sup-title">
										<strong><?php echo $data['DestinationName']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td>

												<?php echo $data['outbound_address']; ?>

<!-- 												<?php echo $data['DestinationAddressL1']; ?><br/>
												<?php echo $data['DestinationAddressL2']; ?><br/>
												<?php echo $data['DestinationAddressL3']; ?>
 -->
											</td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Outbound Status</h1>
									<div class="detail-status
										<?php
											if($data['nama_status'] == 'Open' || $data['nama_status'] == 'In Progress')
												echo 'status-blue';
											else
												echo 'status-green';
										?>">
										<?php echo $data['nama_status']; ?>
									</div>
								</div>
							</div>
						</div>

						<hr style="margin-top: 0;">
						<!--
						<div class="row">
							<div class="col-md-6">
								<h4>Items</h4>
							</div>
							<div class="col-md-6">
								<div class="pull-right">
									<table>
										<tr>
											<td>Total Item</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-item">0</span></strong></td>
										</tr>
										<tr>
											<td>Total Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><strong><span class="t-qty">0</span></strong></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						-->

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Ordered Items
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id </th>
											<th style="width: 30px;"> No </th>
											<th style="width: 130px;"> <div align="center">Item Code</div> </th>
											<th> Item Name </th>
											<th style="width: 120px;"> <div align="center">Ordered</div> </th>
											<th style="width: 120px; display: none;"> <div align="center">Batch</div> </th>
											<th style="width: 120px;"> <div align="center">Picked</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<hr class="top30">

						<div class="row">
							<div class="col-md-6">
								<div class="detail-title">
									<span class="caption-subject font-green-sharp bold uppercase">
										<i class="fa fa-th-list"></i></i> &nbsp;Picking Jobs
									</span>
								</div>
							</div>
							<?php if ($this->access_right->otoritas('add')) : ?>
								<?php if ($data['id_status_outbound'] != 2) : ?>

									<?php if($status_picking['total_scanned'] < $status_picking['total_document']){ ?>

										<div class="col-md-6">
											<div class="pull-right">
												<a data-id="<?php echo $id; ?>" id="button-add" class="btn blue btn-sm min-width120"><i class="fa fa-file-o"></i> &nbsp;NEW PICKING</a>
											</div>
										</div>

									<?php } ?>

								<?php endif; ?>
							<?php endif; ?>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list-p" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id_picking </th>
											<th style="text-align:center" class="table-checkbox" width="10px">
												<input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
											<th style="vertical-align: top;width: 30px;"> No </th>
											<th style="vertical-align: top;"> <div align="center">Picking Doc. #</div> </th>
											<th style="vertical-align: top;"> <div align="center">Outbound Doc. #</div> </th>
											<th style="vertical-align: top;"> <div align="center">Picking Date</div> </th>
											<th style="vertical-align: top;"> <div align="center">Destination</div> </th>
											<th style="vertical-align: top;"> <div align="center">Gate</div> </th>
											<th style="vertical-align: top;"> <div align="center">Start</div> </th>
											<th style="vertical-align: top;"> <div align="center">Finish</div> </th>
											<th style="vertical-align: top;"> <div align="center">Priority</div> </th>
											<th style="vertical-align: top;"> <div align="center">Status</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
						
						<hr class="top30">

						<div class="row">
							<div class="col-md-6">
								<div class="detail-title">
									<span class="caption-subject font-green-sharp bold uppercase">
										<i class="fa fa-th-list"></i></i> &nbsp;Packing List
									</span>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list-pc" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id </th>
											<th style="width: 30px;"> No </th>
											<th style="vertical-align:center"> <div align="center">Packing #</div> </th>
											<th style="vertical-align:center"> <div align="center">Loading Doc #</div> </th>
											<!--th style="vertical-align:center"> <div align="center">Outbound Doc #</div> </th-->
											<th style="vertical-align:center"> <div align="center">Weight</div> </th>
											<th style="vertical-align:center"> <div align="center">Volume</div> </th>
											<th style="vertical-align:center"> <div align="center">User</div> </th>
											<th style="width: 40px;vertical-align:center"> <div align="center">Items Packed</div> </th>
											<th style="vertical-align:center"> <div align="center">Packing Time</div> </th>
											<!--th style="vertical-align:center"> <div align="center">Actions</div> </th-->
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

						<hr class="top30">

						<div class="detail-title">
							<span class="caption-subject font-green-sharp bold uppercase">
								<i class="fa fa-th-list"></i></i> &nbsp;Loading
							</span>
						</div>

						<div class="row">
							<div class="col-md-12">
								<table id="table-list-s" class="table table-striped table-bordered table-hover table-checkable order-column">
									<thead>
										<tr>
											<th style="display: none;"> id_shipping </th>
											<th style="width: 30px;"> No </th>
											<th> <div align="center">Loading Doc. #</div> </th>
											<th> <div align="center">Loading Date</div> </th>
											<th> <div align="center">Destination</div> </th>
											<th> <div align="center">Driver Name</div> </th>
											<th> <div align="center">Transporter #</div> </th>
											<th> <div align="center">Start</div> </th>
											<th> <div align="center">Finish</div> </th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

					</div>
					<div class="form-actions top40">
						<div class="row">
							<div class="col-md-12">

								<div class="pull-right">
									<!--<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>-->
									<a class="btn default min-width120" href="javascript:;" onclick="javascript:history.go(-1);"><i class="fa fa-angle-double-left"></i> BACK</a>
									<?php if ($this->access_right->otoritas('edit')) : ?>

										<?php if ($not_usage) : ?>
											<!--<a href="<?php echo base_url() . $page_class; ?>/edit/<?php echo $id; ?>" class="btn blue min-width120"><i class="fa fa-pencil"></i> EDIT</a>-->
										<?php endif; ?>

									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>

				</form>
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

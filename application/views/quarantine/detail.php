<style>
	.qty h1 {
		font-size: 46px !important;
		font-weight: bold;
		color: #fff !important;
	}

	.qty h1 a {
		color: #fff !important;
	}

	.qty h1 a:hover {
		text-decoration: none;
	}

	.qty h1 a:active {
		text-decoration: none;
	}

	.qty h1 a:focus {
		text-decoration: none;
	}

	.qty span {
		font-weight: bold;
		color: #fff;
	}

	.widget-thumb {
		padding: 10px;
	}
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>QUARANTINE ITEM
				<small>Details of item with all statuses (Good/Hold/NG). You will be able to perform Release/Reject/Hold on specific lot/batch here</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
	<div class="col-md-12 col-sm-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<!-- <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase"><i class="fa fa-th-list"></i> SERIAL
						NUMBER LIST</span>
				</div> -->

				<div class="row">
					<div class="col-md-12">
						<input type="hidden" id="id_item" value="<?php echo $barang->id_barang; ?>">
						<h3><strong><?php echo $barang->kd_barang . " | " . $barang->nama_barang ?></strong></h3>
					</div>
					<div class="col-md-6">
						<table>

							<!--  this info is not needed for this module
							<tr>
								<td>Owner</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php //echo $barang->owner_name; ?></td>
							</tr>
							<tr>
								<td>Has Multi Qty</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php
								if($barang->has_qty == 1){
									echo 'YES';
								}else{
									echo 'NO';
								}
								 ?></td>
							</tr>-->
							<tr>
								<td><strong>Quarantine Duration</strong></td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $quarantine['quarantineduration'].' days'; ?></td>
							</tr>
							<tr>
								<td><strong>Auto Release</strong></td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $quarantine['autorelease'] == 1 ? 'YES' : 'NO'; ?></td>
							</tr>
						</table>
						<br>
					</div>
					<!-- this info is not needed for this module
					<div class="col-md-6">
						<table>
							<tr>
								<td>Category</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->kd_kategori . ' - '. $barang->nama_kategori; ?></td>
							</tr>
							<tr>
								<td>UoM</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->nama_satuan; ?></td>
							</tr>
							<tr>
								<td>Picking Strategy</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->shipment_type; ?></td>
							</tr>
							<tr>
								<td>FIFO/FEFO Period</td>
								<td style="width: 20px; text-align:center;">:</td>
								<td><?php echo $barang->fifo_period; ?></td>
							</tr>
						</table>
						<br>
					</div>-->
				</div>
				<div class="tools">
					<button id="export-detail" type="button" class="btn green" style="min-width: 120px;"><i
							class="fa fa-file-excel-o"></i> EXPORT</button>
					<button id="allrelease" type="button" class="btn green"
						style="min-width: 120px; background-color:green"><i class="fa fa-check"></i>
						RELEASE</button>
					<button id="allreject" type="button" class="btn green"
						style="min-width: 120px; background-color:brown"><i class="fa fa-trash-o"></i>
						REJECT</button>
					<button id="allhold" type="button" class="btn green"
						style="min-width: 120px; background-color:orange"><i class="fa fa-stop"></i>
						HOLD</button>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet box blue-hoki filter-container">
								<div class="portlet-title">
									<div class="caption">
										Filter
									</div>
									<div class="tools">
										<a href="javascript:;" class="filter-indicator">Show filter</a>
										<a href="javascript:;" class="expand show-hide">
										</a>
									</div>
								</div>
								<div class="portlet-body" style="display: none;">
									<!--YOUR CUSTOM FILTER HERE-->
									<form id="form-search" class="form-horizontal" role="form">
										<div class="form-body">


											<div class="row">
												<div class="col-md-6">

													<div class="form-group year-group">
														<label for="year" class="col-md-3">Entry Period</label>
														<div class="col-md-6">

															<select id="periode" class="form-control">

															</select>

															<span class="help-block year-msg"></span>
														</div>
													</div>

													<div class="form-group periode-group">
														<label for="periode" class="col-md-3">Entry Date</label>
														<div class="col-md-8">
															<div class="input-group input-large date-picker input-daterange">
																<input type="text" class="form-control" name="from" id="from"
																	value="<?php echo $today; ?>" placeholder="dd-mm-yyyy" />
																<span class="input-group-addon">
																	to </span>
																<input type="text" class="form-control" name="to" id="to"
																	value="<?php echo $today; ?>" placeholder="dd-mm-yyyy" />
															</div>
															<span class="help-block periode-msg"></span>
															<!-- /input-group -->
														</div>
														<div class="col-md-1">
															<button id="clear-date" type="button"
																class="btn clear-date btn-default btn-xs"><span
																	class="glyphicon glyphicon-remove"></span></button>
														</div>
													</div>

													<!-- <div class="form-group loc-group">
														<label for="loc" class="col-md-3">Lot Number</label>
														<div class="col-md-8">
															<div class="custom-select select-type4 high location">
																<select class="form-control" name="loc" id="loc"
																	style="width: 100%;" multiple data-placeholder="-- All --">
																</select>
															</div>
															<span class="help-block loc-msg"></span>
														</div>
													</div> -->

												</div>

												<div class="col-md-6">

													<div class="form-group year-group" style="display:none">
														<label for="year" class="col-md-3">Period Out</label>
														<div class="col-md-6">

															<select id="periode_out" class="form-control">

															</select>

															<span class="help-block year-msg"></span>
														</div>
													</div>

													<div class="form-group periode-group" style="display:none">
														<label for="periode" class="col-md-3">Out Date</label>
														<div class="col-md-8">
															<div class="input-group input-large date-picker input-daterange">
																<input type="text" class="form-control" name="from_out"
																	id="from_out" value="<?php echo $today; ?>"
																	placeholder="dd-mm-yyyy" />
																<span class="input-group-addon">
																	to </span>
																<input type="text" class="form-control" name="to_out" id="to_out"
																	value="<?php echo $today; ?>" placeholder="dd-mm-yyyy" />
															</div>
															<span class="help-block periode-msg"></span>
															<!-- /input-group -->
														</div>
														<div class="col-md-1">
															<button id="clear-date-out" type="button"
																class="btn clear-date btn-default btn-xs"><span
																	class="glyphicon glyphicon-remove"></span></button>
														</div>
													</div>

													<!-- <div class="form-group loc-group">
														<label for="loc" class="col-md-3">Duration</label>
														<div class="col-md-8">
															<div class="custom-select select-type4 high location">
																<select class="form-control" name="loc" id="loc"
																	style="width: 100%;" multiple data-placeholder="-- All --">
																</select>
															</div>
															<span class="help-block loc-msg"></span>
														</div>
													</div> -->

													<div class="form-group loc-type-group" style="display:none;">
														<label for="loc-type" class="col-md-3">Status Quarantine</label>
														<div class="col-md-8">
															<div class="custom-select select-type4 high location">
																<select class="form-control" name="status_quarantine"
																	id="status_quarantine" style="width: 100%;">
																	<option value="">Please Select</option>
																	<option value="1">RELEASE</option>
																	<option value="2">HOLD</option>
																	<option value="3">REJECT</option>
																</select>
															</div>
															<span class="help-block loc-type-msg"></span>
														</div>
													</div>

													<div class="form-group loc-type-group">
														<label for="loc-type" class="col-md-3">Item Status</label>
														<div class="col-md-8">
															<div class="custom-select select-type4 high location">
																<select class="form-control" name="status_item" id="status_item"
																	style="width: 100%;">
																	<option value="">Please Select</option>
																	<option value="1">GOOD</option>
																	<option value="2" selected="selected">QC HOLD</option>
																	<option value="3">NG</option>
																</select>
															</div>
															<span class="help-block loc-type-msg"></span>
														</div>
													</div>

													<div class="form-group loc-group">
														<label for="loc" class="col-md-3">Lot/Batch No</label>
														<div class="col-md-8">
															<div class="custom-select select-type4 high location">
																<select class="form-control" name="loc" id="loc"
																	style="width: 100%;" multiple data-placeholder="-- All --">
																</select>
															</div>
															<span class="help-block loc-msg"></span>
														</div>
													</div>

												</div>

											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-12">
													<div class="pull-right">
														<button id="reset" type="button" class="btn default"
															style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i>
															RESET</button>
														<button id="search" type="submit" class="btn green-haze"
															style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									<!--END-->
								</div>
							</div>
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<table id="table_receiving"
								class="table table-striped table-bordered table-hover table-checkable order-column">
								<thead>
									<tr>
										<th class="table-checkbox">
											<input type="checkbox" class="group-checkable"
												data-set="#sample_3 .checkboxes" />
										</th>
										<th style="vertical-align: top; text-align: center;width: 30px;"> No </th>
										<th style="vertical-align: top;text-align: center;"> LPN </th>
										<!-- <th style="text-align: center;"> License Plate </th> -->
										<th style="vertical-align: top;text-align: center;"> Qty </th>
										<th style="vertical-align: top;text-align: center;"> Lot/Batch </th>
										<th style="vertical-align: top;text-align: center;"> Doc. Reference # </th>
										<th style="vertical-align: top;text-align: center;"> Entry Date </th>
										<th style="vertical-align: top;text-align: center;"> Location </th>
										<th style="vertical-align: top;text-align: center;"> Status </th>
										<th style="vertical-align: top;text-align: center;"> Quarantine Status</th>
										<th style="vertical-align: top;text-align: center;"> Quarantine Until </th>
										<th style="vertical-align: top;text-align: center;"> Remain Quarantine Days</th>
										<th style="vertical-align: top;text-align: center;"> Reason (For Reject)</th>
										<th style="vertical-align: top;text-align: center;"> Action</th>
										<!-- <th style="text-align: center;"> Action </th> -->
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!--
				<div class="form-actions top40">
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn default min-width120" href="<?php echo base_url() . $page_class; ?>"><i class="fa fa-angle-double-left"></i> BACK</a>
							</div>
						</div>
					</div>
				</div>
				-->
			</div>

		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<div id="modal-remark" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal-remark" class="form-horizontal">
				<input type="hidden" name="id" />

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Reject Reason</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group do-group">
								<label class="col-md-2 control-labe">Remark</label>
								<div class="controls col-md-9">
									<textarea class="form-control" id="remark" name="remark"
										placeholder="remark"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i>
							&nbsp;CLOSE</a>
						<button id="submit-remark" type="submit" class="btn blue min-width120"
							data-loading-text="Loading..."><i class="fa fa-check"></i> &nbsp;SUBMIT</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>


<div id="modal-remark-allreject" class="modal fade" role="basic">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="data-modal-remark-allreject" class="form-horizontal">
				<input type="hidden" name="id" />

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Reject Reason</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group do-group">
								<label class="col-md-2 control-labe">Remark</label>
								<div class="controls col-md-9">
									<textarea class="form-control" id="remark_allreject" name="remark_allreject"
										placeholder="remark"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="form-actions">
						<a data-dismiss="modal" class="form-close btn default"><i class="fa fa-close"></i>
							&nbsp;CLOSE</a>
						<button id="submit-remark-allreject" type="submit" class="btn blue min-width120"
							data-loading-text="Loading..."><i class="fa fa-check"></i> &nbsp;SUBMIT</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

<?php if ($this->access_right->otoritas('print')) { ?>
<?php include 'print_modal.php'; ?>
<?php }?>

<script>
	function manualRelease(obj) {
		var id_barang = $(obj).attr('data-idbarang');
		var unique_code = $(obj).attr('data-uniquecode');

		console.log(id_barang);
		console.log(unique_code);
		$.ajax({
			url: BaseUrl + "quarantine/manual_release/?id_barang=" + id_barang + "&unique_code=" + unique_code,
			type: "GET",
			success: function (data) {
				location.reload();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error Release Item');
			}
		});
	}

	function allRelease() {

	}
</script>

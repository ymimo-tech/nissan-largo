<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php
                    $hidden_form = array('id' => !empty($id) ? $id : '');
                    echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Item Code<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('kd_barang', !empty($default->kd_barang) ? $default->kd_barang : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="col-md-3 control-label">Item Name<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('nama_barang', !empty($default->nama_barang) ? $default->nama_barang : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">Has Default Serial Number<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_dropdown('has_sernum',$options_has_sernum, !empty($default->has_sernum) ? $default->has_sernum : '', 'class="col-md-12 onchange form-control chosen"') ?>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Shipment Type and Exp Status<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_dropdown('shipment_type',$options_shipment_type, !empty($default->shipment_type) ? $default->shipment_type : '', 'class="col-md-12 onchange form-control chosen"') ?>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">First Time Qty <span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_input('jumlah_awal', !empty($default->jumlah_awal) ? $default->jumlah_awal : '', 'class="col-md-12 form-control"') ?>
                            
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-3 control-label">UoM<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_dropdown('id_satuan',$options_satuan, !empty($default->id_satuan) ? $default->id_satuan : '', 'class="col-md-12 onchange form-control chosen"') ?>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Category<span class="required">*</span></label>
                        <div class="controls col-md-9">
                            <?php echo form_dropdown('id_kategori',$options_kategori, !empty($default->id_kategori) ? $default->id_kategori : '', 'class="col-md-12 onchange form-control chosen"') ?>
                            
                        </div>
                    </div> 
                    

                    <span class="required">* harus diisi</span>
                    
                    
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
                    <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn default', 'onclick' => 'close_form_modal(this.id)')); ?>
                </div>

            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
       /* $('.chosen').chosen();*/
        /*$('.numeric').numeric();*/
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>




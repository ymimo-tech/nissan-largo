<style>
	.qty h1 {
		font-size: 46px !important;
		font-weight: bold;
		color: #fff !important;
	}

	.qty h1 a {
		color: #fff !important;
	}

	.qty h1 a:hover {
		text-decoration: none;
	}

	.qty h1 a:active {
		text-decoration: none;
	}

	.qty h1 a:focus {
		text-decoration: none;
	}

	.qty span {
		font-weight: bold;
		color: #fff;
	}

	.widget-thumb {
		padding: 10px;
	}
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>QUARANTINE LIST
				<small>This is the list of all items. By default, the list is filtered by items that are being <b>quarantined (or QC Hold)</b></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN ALERTS PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">

				<div class="caption col-md-12">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i> Items List
					</span>
				</div>

				<!-- <div class="row qty"> -->
				<!--<div class="col-md-2 total" style="text-align: center;">
						<div class="widget-thumb bg-grey-salt text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat total-inbound">0</h1>
								</div>
							</div>
						</div>
					</div>-->
				<!-- 					<div class="col-md-2 rec" style="text-align: center;">

						<div class="widget-thumb bg-blue text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 on" style="text-align: center;">
						<div class="widget-thumb bg-green text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 all" style="text-align: center;">
						<div class="widget-thumb bg-red-sunglo text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 sus" style="text-align: center;">
						<div class="widget-thumb bg-red-intense text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 ng" style="text-align: center;">
						<div class="widget-thumb bg-red-soft text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2 ava" style="text-align: center;">
						<div class="widget-thumb bg-green-turquoise text-uppercase">
							<span></span>
							<div class="widget-thumb-wrap">
								<div class="widget-thumb-body">
									<h1 class="widget-thumb-body-stat">0</h1>
								</div>
							</div>
						</div>
					</div> -->

				<!--
					<table id="table-qty" class="table table-striped table-bordered table-hover table-checkable order-column">
						<thead>
							<tr>
								<th style="width: 30px;"> No </th>
								<th style="text-align: center;"> Receiving Qty </th>
								<th style="text-align: center;"> On Hand Qty </th>
								<th style="text-align: center;"> Allocated Qty </th>
								<th style="text-align: center;"> Suspend Qty </th>
								<th style="text-align: center;"> Available Qty </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
					-->

				<!-- </div> -->

				<div class="tools">
					<!-- <button id="sync" type="button" class="btn blue" style="width: 120px;"><i class="fa fa-refresh"></i> SYNC</button> -->
					<!-- <button id="export" type="button" class="btn green" style="width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT</button> -->
					<!-- 					<button id="export-format2" type="button" class="btn green" style="width: 120px;"><i class="fa fa-file-excel-o"></i> EXPORT 2</button>
					<button id="export-format-soh" type="button" class="btn green" ><i class="fa fa-file-excel-o"></i> EXPORT SOH </button>
 -->
				</div>
			</div>
			<div class="portlet-body">
				<!--
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <?php if ($this->access_right->otoritas('add')) : ?>

                                <?php echo hgenerator::render_button_group($button_group); ?>

                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">

                            <div class="btn-group pull-right">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">

                                    <li>
                                        <a href="<?php echo base_url('item_stok/save_pdf');?>" target="_blank">
                                        Save as PDF </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="well-content clearfix">
                                    <?php echo form_open_multipart('', array('id' => 'ffilter')); ?>
                                        <div class="row">
                                            <div class="pull-left form-group col-md-3">
                                                <div>
                                                    <label>Item Code :</label>
                                                </div>
                                                <div>
                                                    <?php //echo form_input('kd_barang','', 'class="col-md-12 onchange form-control"') ?>
                                                </div>
                                            </div>
                                            <div class="pull-left col-md-3">
                                                <div>
                                                    <label>Item Name :</label>
                                                </div>
                                                <div>
                                                    <?php //echo form_input('nama_barang', '', 'class="col-md-12 onchange form-control chosen"') ?>
                                                </div>
                                            </div>
                                            <div class="pull-left col-md-3">
                                                <div>
                                                    <label>Oum :</label>
                                                </div>
                                                <div>
                                                    <?php //echo form_dropdown('id_satuan',$options_satuan,'', 'class="col-md-12 onchange form-control chosen"') ?>
                                                </div>
                                            </div>
                                        </div>

                                    <?php echo form_close(); ?>
                                </div>

                        </div>
                    </div>
                </div>
				-->
				<div class="row">
					<div class="col-md-12 col-sm-12">

						<!--COPY THIS SECTION FOR FILTER CONTAINER-->
						<div class="portlet box blue-hoki filter-container">
							<div class="portlet-title">
								<div class="caption">
									Filter
								</div>
								<div class="tools">
									<a href="javascript:;" class="filter-indicator">Show filter</a>
									<a href="javascript:;" class="expand show-hide">
									</a>
								</div>
							</div>
							<div class="portlet-body" style="display: none;">
								<!--YOUR CUSTOM FILTER HERE-->
								<form id="form-search" class="form-horizontal" role="form">
									<div class="form-body">


										<div class="row">
											<div class="col-md-6">

											<div class="form-group item-group">
													<label for="item" class="col-md-3">Item Name</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control"
																name="item" id="item" style="width: 100%;" multiple data-placeholder="-- All --">

															</select>
														</div>
														<span class="help-block item-msg"></span>
													</div>
												</div>

												<div class="form-group loc-group">
													<label for="code" class="col-md-3">Item Code</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control" name="code" id="code"
																style="width: 100%;" multiple
																data-placeholder="-- All --">
															</select>
														</div>
														<span class="help-block loc-msg"></span>
													</div>
												</div>



											</div>

											<div class="col-md-6">

												<div class="form-group loc-group">
													<label for="loc" class="col-md-3">Under Quarantine?</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control" name="quarantine" id="quarantine"
																style="width: 100%;">
																<!-- <option value="">Please Select</option> -->
																<option value="1">Yes</option>
																<option value="2">No</option>
															</select>
														</div>
														<span class="help-block loc-msg"></span>
													</div>
												</div>

												<div class="form-group loc-group">
													<label for="auto_release" class="col-md-3">Auto Release?</label>
													<div class="col-md-8">
														<div class="custom-select select-type4 high location">
															<select class="form-control" name="auto_release"
																id="auto_release" style="width: 100%;">
																<option value="">Please Select</option>
																<option value="1">Yes</option>
																<option value="2">No</option>
															</select>
														</div>
														<span class="help-block loc-msg"></span>
													</div>
												</div>

											</div>

										</div>
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-12">
												<div class="pull-right">
													<button id="reset" type="button" class="btn default"
														style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i>
														RESET</button>
													<button id="search" type="submit" class="btn green-haze"
														style="width: 120px;"><i class="fa fa-search"></i>
														SEARCH</button>
												</div>
											</div>
										</div>
									</div>
								</form>
								<!--END-->
							</div>
						</div>
						<!--END SECTION-->

						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<!--<div class="portlet light">
                            <div class="portlet-body">-->
						<table class="table table-striped table-bordered table-hover table-checkable order-column"
							id="datatable_ajax">
							<thead>
								<tr>
									<!--
                                            <th class="table-checkbox">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" /> </th>
											-->
									<th style="vertical-align: top;text-align: center;"> No </th>
									<th style="vertical-align: top;text-align: center;"> Item Code </th>
									<th style="vertical-align: top;text-align: center;"> Item Name </th>
									<th style="vertical-align: top;text-align: center;"> Qty on Hold </th>
									<!-- 											<th style="text-align: center;"> Location </th>
											<th style="text-align: center; width: 90px;"> Loc. Type </th>
 -->
									<th style="vertical-align: top;text-align: center;"> SN On Hold </th>
									<th style="vertical-align: top;text-align: center;"> Quarantine Duration</th>

									<!-- <th style="text-align: center;"> Min. Stock </th> -->
								</tr>
							</thead>
							<tbody> </tbody>

						</table>
						<div>&nbsp;</div>
						<!--
                            </div>
                            <div>&nbsp;</div>
                        </div>-->
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- /.modal -->
			</div>
		</div>
		<!-- END ALERTS PORTLET-->
	</div>
</div>

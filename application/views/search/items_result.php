<style>
	.qty h1{
		font-size: 46px !important;
		font-weight: bold;
		color: #337ab7;
	}

	.qty h1 a:hover{
		text-decoration: none;
		color: #337ab7;
	}

	.qty h1 a:active{
		text-decoration: none;
		color: #337ab7;
	}

	.qty h1 a:focus{
		text-decoration: none;
		color: #337ab7;
	}

	.qty span{
		font-weight: bold;
		color: #337ab7;
	}
</style>

	<div class="row">
			<div class="col-md-12">
				<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" />
				<input type="hidden" id="id-barang" name="id_barang" value="<?php echo $items['id_barang']; ?>" />
				<h4>
					<?php
						echo $items['kd_barang'] . ' - ' . $items['nama_barang'];
					?>
				</h4>
				<div class="row top20">
					<div class="col-md-4">
						<div class="detail-caption text-left">
							<table>
								<!--
								<tr>
									<td style="width: 160px;">Code</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo $items['kd_barang']; ?></td>
								</tr>
								-->
								<tr>
									<td>Picking Strategy</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo $items['shipment_type']; ?></td>
								</tr>
								<tr>
									<td>FIFO Period</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo $items['fifo_period']; ?></td>
								</tr>
								<tr>
									<td>Category Code</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo $items['kd_kategori']; ?></td>
								</tr>
								<tr>
									<td>Category</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo $items['nama_kategori']; ?></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-4">
						<div class="detail-caption text-left">
							<table>
								<tr>
									<td>Category Code 2</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo $items['kd_kategori_2']; ?></td>
								</tr>
								<tr>
									<td>Category 2</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo $items['nama_kategori_2']; ?></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-4">
						<div class="detail-caption text-left">
							<table>
								<tr>
									<td>UoM</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo !empty($items['nama_satuan']) ? $items['nama_satuan'] : '-'; ?></td>
								</tr>
								<tr>
									<td>Owner Name</td>
									<td align="center" style="width: 40px">:</td>
									<td><?php echo $items['owner_name']; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 qty qty-result">
						<br><br>
						<div class="col-md-2 col-md-offset-1 rec" style="text-align: center;">
							<span></span>
							<h1></h1>
						</div>
						<div class="col-md-2 on" style="text-align: center;">
							<span></span>
							<h1></h1>
						</div>
						<div class="col-md-2 all" style="text-align: center;">
							<span></span>
							<h1></h1>
						</div>
						<div class="col-md-2 sus" style="text-align: center;">
							<span></span>
							<h1></h1>
						</div>
						<div class="col-md-2 ava" style="text-align: center;">
							<span></span>
							<h1></h1>
						</div>
						<!--
						<table id="table-item-qty" class="table table-striped table-bordered table-hover table-checkable order-column">
							<thead>
								<tr>
									<th style="width: 30px;"> No </th>
									<th style="text-align: center;"> In Transit Qty </th>
									<th style="text-align: center;"> On Hand Qty </th>
									<th style="text-align: center;"> Allocated Qty </th>
									<th style="text-align: center;"> Suspend Qty </th>
									<th style="text-align: center;"> Available Qty </th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
						-->
					</div>
				</div>
				<div>
					&nbsp;
				</div>
			</div>
		</div>

		<div class="row top40">
			<div class="col-md-12">

				<div class="detail-title">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-arrow-circle-left"></i></i> &nbsp;Last Inbound
					</span>
				</div>

				<hr>

				<div class="row">

					<?php if(!empty($inbound)){ ?>

						<div class="col-md-3">
							<div class="detail-caption">
								<h1>Inbound Doc. #<h1>
								<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $inbound['kd_inbound']; ?>&size=45" style="">
								<div class="detail-info">
									<?php echo $inbound['kd_inbound']; ?>
								</div>
							</div>
						</div>

						<div class="col-md-4">
							<div class="detail-caption text-left">
								<h1>Source<h1>
								<?php if($inbound['id_inbound_document'] != '2'){ ?>
									<div class="bottom5 sup-title">
										<strong><?php echo $inbound['nama_supplier']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Code</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['kd_supplier']; ?></td>
										</tr>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['alamat_supplier']; ?></td>
										</tr>
										<tr>
											<td>Phone</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['telepon_supplier']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Contact Person</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['cp_supplier']; ?></td>
										</tr>
									</table>
								<?php }else{ ?>
									<div class="bottom5 sup-title">
										<strong><?php echo $inbound['customer_name']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['address']; ?></td>
										</tr>
										<tr>
											<td>Phone</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['phone']; ?></td>
										</tr>
									</table>
								<?php } ?>
							</div>
						</div>

						<div class="col-md-2">
							<div class="detail-caption text-left">
								<h1>Detail<h1>
								<table>
									<tr>
										<td>Date</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><?php echo $inbound['tanggal_inbound']; ?></td>
									</tr>
									<tr>
										<td>Doc. Type</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><?php echo $inbound['inbound_document_name']; ?></td>
									</tr>
									<tr>
										<td>Qty</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><?php echo $inbound['jumlah_barang']; ?></td>
									</tr>
									<tr>
										<td>Created by</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><?php echo $inbound['user_name']; ?></td>
									</tr>
								</table>
							</div>
						</div>

						<div class="col-md-3">
							<div class="detail-caption">
								<h1>Inbound Status</h1>
								<div class="detail-status
									<?php
										if($inbound['nama_status'] == 'Open' || $inbound['nama_status'] == 'In Progress')
											echo 'status-green';
										else
											echo 'status-blue';
									?>" style="font-size: 26px;">
									<?php echo $inbound['nama_status']; ?>
								</div>
							</div>
						</div>
					<?php }else{ ?>

						<div class="col-md-12">
							<div align="center">
								<h4>It looks like you have no Inbound Document yet...</h4>
								<a class="btn blue" href="<?php echo base_url(); ?>inbound/add">CREATE NEW DOCUMENT</a>
							</div>
						</div>

					<?php } ?>
				</div>

			</div>
		</div>

		<div class="row top40">
			<div class="col-md-12">

				<div class="detail-title">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-arrow-circle-down"></i></i> &nbsp;Last Received
					</span>
				</div>

				<hr>

				<div class="row">

					<?php if(!empty($inbound) && !empty($receiving)){ ?>
						<input type="hidden" id="id-receiving" name="id_receiving" value="<?php echo $receiving['id_receiving']; ?>" />
						<div class="col-md-3">
							<div class="detail-caption">
								<h1>Receiving Doc. #<h1>
								<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $receiving['kd_receiving']; ?>&size=45" style="">
								<div class="detail-info">
									<?php echo $receiving['kd_receiving']; ?>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="detail-caption text-left">
								<h1>Detail<h1>
								<table>
									<tr>
										<td>Vehicle Plate</td>
										<td style="width: 20px;" align="center">:</td>
										<td><?php echo $receiving['vehicle_plate']; ?></td>
									</tr>
									<tr>
										<td>Driver</td>
										<td align="center">:</td>
										<td><?php echo $receiving['dock']; ?></td>
									</tr>
									<tr>
										<td style="width: 100px;">Delivery Doc.</td>
										<td align="center">:</td>
										<td><?php echo $receiving['delivery_doc']; ?></td>
									</tr>
									<tr>
										<td style="width: 100px;">Receiving Date.</td>
										<td align="center">:</td>
										<td><?php echo $receiving['tanggal_receiving']; ?></td>
									</tr>
									<tr>
										<td style="width: 100px;">Time Receiving</td>
										<td align="center">:</td>
										<td><?php echo $receiving['time_receiving']; ?></td>
									</tr>
								</table>
							</div>
						</div>

						<div class="col-md-3">
							<div class="detail-caption text-left">
								<h1>&nbsp;<h1>
								<table>
									<tr>
										<td>Tally</td>
										<td style="width: 20px;" align="center">:</td>
										<td><?php echo $receiving['tally']; ?></td>
									</tr>
									<tr>
										<td style="width: 100px;">Put Away</td>
										<td align="center">:</td>
										<td><?php echo $receiving['putaway']; ?></td>
									</tr>
									<tr>
										<td style="width: 100px;">Created by</td>
										<td align="center">:</td>
										<td><?php echo $receiving['user_name']; ?></td>
									</tr>
								</table>
							</div>
						</div>

						<div class="col-md-3">
							<div class="detail-caption">
								<h1>Status</h1>
								<div style="font-size: 26px;" class="detail-status
									<?php
									if($receiving['status'] != 'Complete')
										echo 'status-green';
									else
										echo 'status-blue';
									?>
								">
									<?php echo $receiving['status']; ?>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<table id="table-list-receiving" class="table table-striped table-bordered table-hover table-checkable order-column">
								<thead>
									<tr>
										<th style="width: 30px;"> No </th>
										<th> <div align="center">Serial Number</div> </th>
										<th> <div align="center">Expired Date</div> </th>
										<th> <div align="center">Date In</div> </th>
										<th> <div align="center">Location</div> </th>
										<th> <div align="center">Status</div> </th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>

					<?php }else{ ?>

						<div class="col-md-12">
							<div align="center">
								<h4>It looks like you have no Receiving Document yet...</h4>
								<?php if(empty($receiving) && !empty($inbound)){ ?>
									<a class="btn blue" href="<?php echo base_url(); ?>receiving/add">CREATE NEW DOCUMENT</a>
								<?php } ?>
							</div>
						</div>

					<?php } ?>
				</div>

			</div>
		</div>

		<div class="row top40">
			<div class="col-md-12">

				<div class="detail-title">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-arrow-circle-right"></i></i> &nbsp;Last Outbound
					</span>
				</div>

				<hr>

				<div class="row">

					<?php if(!empty($outbound)){ ?>

						<div class="col-md-3">
							<div class="detail-caption">
								<h1>Outbound Doc. #<h1>
								<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $outbound['kd_outbound']; ?>&size=45" style="">
								<div class="detail-info">
									<?php echo $outbound['kd_outbound']; ?>
								</div>
							</div>
						</div>

						<div class="col-md-4">
							<div class="detail-caption text-left">
								<h1>Destination<h1>

								<?php if($outbound['id_outbound_document'] == '2'){ ?>
									<div class="bottom5 sup-title">
										<strong><?php echo $outbound['nama_supplier']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Code</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['kd_supplier']; ?></td>
										</tr>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['alamat_supplier']; ?></td>
										</tr>
										<tr>
											<td>Phone</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['telepon_supplier']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Contact Person</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['cp_supplier']; ?></td>
										</tr>
									</table>
								<?php }else{ ?>
									<div class="bottom5 sup-title">
										<strong><?php echo $outbound['customer_name']; ?></strong>
									</div>
									<table>
										<tr>
											<td>Address</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['address']; ?></td>
										</tr>
										<tr>
											<td>Phone</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['phone']; ?></td>
										</tr>
									</table>
								<?php } ?>
							</div>
						</div>

						<div class="col-md-2">
							<div class="detail-caption text-left">
								<h1>Detail<h1>
								<table>
									<tr>
										<td>Date</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><?php echo $outbound['tanggal_outbound']; ?></td>
									</tr>
									<tr>
										<td>Doc. Type</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><?php echo $outbound['outbound_document_name']; ?></td>
									</tr>
									<tr>
										<td>Qty</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><?php echo $outbound['jumlah_barang']; ?></td>
									</tr>
									<tr>
										<td>Created by</td>
										<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
										<td><?php echo $outbound['user_name']; ?></td>
									</tr>
								</table>
							</div>
						</div>

						<div class="col-md-3">
							<div class="detail-caption">
								<h1>Outbound Status</h1>
								<div class="detail-status
									<?php
										if($outbound['nama_status'] == 'Open' || $outbound['nama_status'] == 'In Progress')
											echo 'status-green';
										else
											echo 'status-blue';
									?>" style="font-size: 26px;">
									<?php echo $outbound['nama_status']; ?>
								</div>
							</div>
						</div>

					<?php }else{ ?>

						<div class="col-md-12">
							<div align="center">
								<h4>It looks like you have no Outbound Document yet...</h4>
								<a class="btn blue" href="<?php echo base_url(); ?>outbound/add">CREATE NEW DOCUMENT</a>
							</div>
						</div>

					<?php } ?>
				</div>

			</div>
		</div>

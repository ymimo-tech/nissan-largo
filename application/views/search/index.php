<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>License Plate Number (LPN)
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
		<?php
			if($type == 'item'){
				include 'items_result.php';
			}

			if($type == 'Serial Number'){
				include 'sn_result.php';
			}

			if($type == 'outerlabel'){
				include 'ol_result.php';
			}

		?>

        <!-- BEGIN EXAMPLE TABLE PORTLET
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-search"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">

                    <a  class="btn btn-default btn-sm data-table-add">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm btn-receiving">
                        <i class="fa fa-arrow-down"></i> Receiving </a>
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
                </div>
            </div>
            <div class="portlet-body">

            </div>
			<div>&nbsp;</div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

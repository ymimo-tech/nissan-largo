	<div class="row">
		<div class="col-md-12">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-green-sharp bold uppercase">
							<i class="fa fa-search"></i><i class="icon-plus-sign-alt"></i> LPN Info
						</span>
					</div>
					<div class="tools">
						<a href="" class="collapse" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" />
					<input type="hidden" id="id-barang" name="id_barang" value="<?php echo $items['id_barang']; ?>" />
					<input type="hidden" id="sn" name="sn" value="<?php echo $sn; ?>" />
					<div class="row">
						<h4 class="col-md-9" style="margin-top: 0;">
							<div class="row">
								<div class="col-md-12">
									<?php
										echo $sn . ' | ' . $items['kd_barang'] . ' - ' . $items['nama_barang'];
									?>
								</div>
							</div>
							<div class="row top20">
								<div class="col-md-5">
									<div class="detail-caption text-left">
										<table>
											<!--
											<tr>
												<td style="width: 160px;">Code</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['kd_barang']; ?></td>
											</tr>
											-->
											<tr>
												<td>Picking Strategy</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['shipment_type']; ?></td>
											</tr>
											<tr>
												<td>FIFO Period</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['fifo_period']; ?></td>
											</tr>
											<tr>
												<td>Date In</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['tgl_in']; ?></td>
											</tr>
											<tr>
												<td>Date Expired</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['tgl_exp']; ?></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="col-md-4">
									<div class="detail-caption text-left">
										<table>
											<tr>
												<td>Category Code</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['kd_kategori']; ?></td>
											</tr>
											<tr>
												<td>Category</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['nama_kategori']; ?></td>
											</tr>
											<tr>
												<td>Category Code 2</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['kd_kategori_2']; ?></td>
											</tr>
											<tr>
												<td>Category 2</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['nama_kategori_2']; ?></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="col-md-3">
									<div class="detail-caption text-left">
										<table>
											<tr>
												<td>UoM</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo !empty($items['nama_satuan']) ? $items['nama_satuan'] : '-'; ?></td>
											</tr>
											<tr>
												<td>Owner Name</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['owner_name']; ?></td>
											</tr>
											<tr>
												<td>Location</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['loc_name']; ?></td>
											</tr>
											<tr>
												<td>Loc. Type</td>
												<td align="center" style="width: 40px">:</td>
												<td><?php echo $items['loc_type']; ?></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</h4>
						<div class="col-md-3">
							<div class="detail-caption">
								<h1>Item Status</h1>
								<div class="detail-status
									<?php
										if(strtolower($items['kd_qc']) == 'qc hold' || strtolower($items['kd_qc']) == 'ng')
											echo 'status-green';
										else if(strtolower($items['kd_qc']) == 'cc hold')
											echo 'status-red';
										else if(strtolower($items['kd_qc']) == 'good')
											echo 'status-blue';
									?>" style="font-size: 26px;">
									<?php echo $items['kd_qc']; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table id="table-list-sn-movement" class="table table-striped table-bordered table-hover report-table">
								<thead>
									<tr>
										<th rowspan="2" style="display: none;">id</th>
										<th rowspan="2" style="width: 30px; vertical-align: middle; text-align: center;">No</th>
										<th rowspan="2" style="vertical-align: middle; text-align: center;">Item Code</th>
										<th rowspan="2" style="vertical-align: middle; text-align: center;">Item Name</th>
										<th rowspan="2" style="vertical-align: middle; text-align: center;">LPN</th>
										<th colspan="3" style="vertical-align: middle; text-align: center;">From</th>
										<th colspan="3" style="vertical-align: middle; text-align: center;">To</th>
										<th rowspan="2" style="vertical-align: middle; text-align: center;">Process Type</th>
									</tr>
									<tr>
										<th style="text-align: center;">Location</th>
										<th style="text-align: center;">By</th>
										<th style="text-align: center;">Time</th>
										<th style="text-align: center;">Location</th>
										<th style="text-align: center;">By</th>
										<th style="text-align: center;">Time</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div>&nbsp;</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-green-sharp bold uppercase">
							<i class="fa fa-arrow-circle-left"></i></i> &nbsp;Inbound
						</span>
					</div>
					<div class="tools">
						<a href="" class="collapse" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">

						<?php if(!empty($inbound)){ ?>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Inbound Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $inbound['kd_inbound']; ?>&size=45" style="">
									<div class="detail-info">
										<a href="<?php echo base_url() . 'inbound/detail/' . $inbound['id_inbound']; ?>"><?php echo $inbound['kd_inbound']; ?></a>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="detail-caption text-left">
									<h1>Source<h1>
									<?php if($inbound['id_inbound_document'] != '2' && $inbound['id_inbound_document'] != '7'){ ?>
										<div class="bottom5 sup-title">
											<strong><?php echo $inbound['nama_supplier']; ?></strong>
										</div>
										<table>
											<tr>
												<td>Code</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $inbound['kd_supplier']; ?></td>
											</tr>
											<tr>
												<td>Address</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $inbound['alamat_supplier']; ?></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $inbound['telepon_supplier']; ?></td>
											</tr>
											<tr>
												<td style="width: 100px;">Contact Person</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $inbound['cp_supplier']; ?></td>
											</tr>
										</table>
									<?php }else{ ?>
										<div class="bottom5 sup-title">
											<strong><?php echo $inbound['customer_name']; ?></strong>
										</div>
										<table>
											<tr>
												<td>Address</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $inbound['address']; ?></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $inbound['phone']; ?></td>
											</tr>
										</table>
									<?php } ?>
								</div>
							</div>

							<div class="col-md-2">
								<div class="detail-caption text-left">
									<h1>Detail<h1>
									<table>
										<tr>
											<td>Date</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['tanggal_inbound']; ?></td>
										</tr>
										<tr>
											<td>Doc. Type</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['inbound_document_name']; ?></td>
										</tr>
										<tr>
											<td>Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['jumlah_barang']; ?></td>
										</tr>
										<tr>
											<td>Created by</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $inbound['user_name']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Inbound Status</h1>
									<div class="detail-status
										<?php
											if($inbound['nama_status'] == 'Open' || $inbound['nama_status'] == 'In Progress')
												echo 'status-green';
											else
												echo 'status-blue';
										?>" style="font-size: 26px;">
										<?php echo $inbound['nama_status']; ?>
									</div>
								</div>
							</div>
						<?php }else{ ?>

							<div class="col-md-12">
								<div align="center">
									<h4>It looks like you have no Inbound Document yet...</h4>
									<a class="btn blue" href="<?php echo base_url(); ?>inbound/add">CREATE NEW DOCUMENT</a>
								</div>
							</div>
							<div>&nbsp;</div>

						<?php } ?>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-green-sharp bold uppercase">
							<i class="fa fa-arrow-circle-left"></i></i> &nbsp;Received
						</span>
					</div>
					<div class="tools">
						<a href="" class="collapse" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">

						<?php if(!empty($inbound) && !empty($receiving)){ ?>
							<input type="hidden" id="id-receiving" name="id_receiving" value="<?php echo $receiving['id_receiving']; ?>" />
							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Receiving Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $receiving['kd_receiving']; ?>&size=45" style="">
									<div class="detail-info">
										<a href="<?php echo base_url() . 'receiving/detail/' . $receiving['id_receiving']; ?>"><?php echo $receiving['kd_receiving']; ?></a>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>Detail<h1>
									<table>
										<tr>
											<td>Vehicle Plate</td>
											<td style="width: 20px;" align="center">:</td>
											<td><?php echo $receiving['vehicle_plate']; ?></td>
										</tr>
										<tr>
											<td>Driver</td>
											<td align="center">:</td>
											<td><?php echo $receiving['dock']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Delivery Doc.</td>
											<td align="center">:</td>
											<td><?php echo $receiving['delivery_doc']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Receiving Date.</td>
											<td align="center">:</td>
											<td><?php echo $receiving['tanggal_receiving']; ?></td>
										</tr>
										<tr>
											<td style="width: 100px;">Time Receiving</td>
											<td align="center">:</td>
											<td><?php echo $receiving['time_receiving']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>&nbsp;<h1>
									<table>
										<tr>
											<td>Tally</td>
											<td style="width: 20px;" align="center">:</td>
											<td><?php echo $receiving['tally']; ?></td>
										</tr>
										<tr>
											<td style="">Put Away</td>
											<td align="center">:</td>
											<td><?php echo $receiving['putaway']; ?></td>
										</tr>
										<tr>
											<td style="">Created by</td>
											<td align="center">:</td>
											<td><?php echo $receiving['user_name']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Status</h1>
									<div style="font-size: 26px;" class="detail-status
										<?php
										if($receiving['status'] != 'Complete')
											echo 'status-green';
										else
											echo 'status-blue';
										?>
									">
										<?php echo $receiving['status']; ?>
									</div>
								</div>
							</div>

						<?php }else{ ?>

							<div class="col-md-12">
								<div align="center">
									<h4>It looks like you have no Receiving Document yet...</h4>
									<?php if(empty($receiving) && !empty($inbound)){ ?>
										<a class="btn blue" href="<?php echo base_url(); ?>receiving/add">CREATE NEW DOCUMENT</a>
									<?php } ?>
								</div>
							</div>
							<div>&nbsp;</div>

						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-green-sharp bold uppercase">
							<i class="fa fa-arrow-circle-left"></i></i> &nbsp;Outbound
						</span>
					</div>
					<div class="tools">
						<a href="" class="collapse" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">

						<?php if(!empty($outbound)){ ?>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Outbound Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $outbound['kd_outbound']; ?>&size=45" style="">
									<div class="detail-info">
										<a href="<?php echo base_url() . 'outbound/detail/' . $outbound['id_outbound']; ?>"><?php echo $outbound['kd_outbound']; ?></a>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="detail-caption text-left">
									<h1>Destination<h1>

									<?php if($outbound['id_outbound_document'] == '2'){ ?>
										<div class="bottom5 sup-title">
											<strong><?php echo $outbound['nama_supplier']; ?></strong>
										</div>
										<table>
											<tr>
												<td>Code</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $outbound['kd_supplier']; ?></td>
											</tr>
											<tr>
												<td>Address</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $outbound['alamat_supplier']; ?></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $outbound['telepon_supplier']; ?></td>
											</tr>
											<tr>
												<td style="width: 100px;">Contact Person</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $outbound['cp_supplier']; ?></td>
											</tr>
										</table>
									<?php }else{ ?>
										<div class="bottom5 sup-title">
											<strong><?php echo $outbound['customer_name']; ?></strong>
										</div>
										<table>
											<tr>
												<td>Address</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $outbound['address']; ?></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
												<td><?php echo $outbound['phone']; ?></td>
											</tr>
										</table>
									<?php } ?>
								</div>
							</div>

							<div class="col-md-2">
								<div class="detail-caption text-left">
									<h1>Detail<h1>
									<table>
										<tr>
											<td>Date</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['tanggal_outbound']; ?></td>
										</tr>
										<tr>
											<td>Doc. Type</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['outbound_document_name']; ?></td>
										</tr>
										<tr>
											<td>Qty</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['jumlah_barang']; ?></td>
										</tr>
										<tr>
											<td>Created by</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $outbound['user_name']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Outbound Status</h1>
									<div class="detail-status
										<?php
											if($outbound['nama_status'] == 'Open' || $outbound['nama_status'] == 'In Progress')
												echo 'status-green';
											else
												echo 'status-blue';
										?>" style="font-size: 26px;">
										<?php echo $outbound['nama_status']; ?>
									</div>
								</div>
							</div>

						<?php }else{ ?>

							<div class="col-md-12">
								<div align="center">
									<h4>It looks like you have no Outbound Document yet...</h4>
									<a class="btn blue" href="<?php echo base_url(); ?>outbound/add">CREATE NEW DOCUMENT</a>
								</div>
							</div>
							<div>&nbsp;</div>

						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-green-sharp bold uppercase">
							<i class="fa fa-tags"></i></i> &nbsp;Picking
						</span>
					</div>
					<div class="tools">
						<a href="" class="collapse" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">

						<?php if(!empty($outbound) && !empty($picking)){ ?>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Picking Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $picking['pl_name']; ?>&size=45" style="">
									<div class="detail-info">
										<a href="<?php echo base_url() . 'picking_list/detail/' . $picking['pl_id']; ?>"><?php echo $picking['pl_name']; ?></a>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>Detail<h1>

									<table>
										<tr>
											<td>Date</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $picking['pl_date']; ?></td>
										</tr>
										<tr>
											<td>Start</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $picking['pl_start']; ?></td>
										</tr>
										<tr>
											<td>Finish</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $picking['pl_finish']; ?></td>
										</tr>

									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>&nbsp;<h1>
									<table>
										<tr>
											<td>Created by</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $picking['user_name']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Picking Status</h1>
									<div style="font-size: 26px;" class="detail-status
										<?php
										if($picking['status'] != 'Complete')
											echo 'status-green';
										else
											echo 'status-blue';
										?>
									">
										<?php echo $picking['status']; ?>
									</div>
								</div>
							</div>

						<?php }else{ ?>

							<div class="col-md-12">
								<div align="center">
									<h4>It looks like you have no Picking Document yet...</h4>
									<?php if(empty($picking) && !empty($outbound)){ ?>
										<a class="btn blue" href="<?php echo base_url(); ?>picking_list/add">CREATE NEW DOCUMENT</a>
									<?php } ?>
								</div>
							</div>
							<div>&nbsp;</div>

						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-green-sharp bold uppercase">
							<i class="fa fa-truck"></i></i> &nbsp;Loading
						</span>
					</div>
					<div class="tools">
						<a href="" class="collapse" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">

						<?php if(!empty($picking) && !empty($loading)){ ?>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Loading Doc. #<h1>
									<img alt="" src="<?php echo base_url(); ?>barcode?text=<?php echo $loading['shipping_code']; ?>&size=45" style="">
									<div class="detail-info">
										<a href="<?php echo base_url() . 'loading/detail/' . $loading['shipping_id']; ?>"><?php echo $loading['shipping_code']; ?></a>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>Detail<h1>
									<table>
										<tr>
											<td>Date</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $loading['shipping_date']; ?></td>
										</tr>
										<tr>
											<td>Driver</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $loading['driver_name']; ?></td>
										</tr>
										<tr>
											<td>Transporter #</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $loading['license_plate']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption text-left">
									<h1>&nbsp;<h1>
									<table>
										<tr>
											<td>Start</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $loading['shipping_start']; ?></td>
										</tr>
										<tr>
											<td>Finish</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $loading['shipping_finish']; ?></td>
										</tr>
										<tr>
											<td>Created by</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td><?php echo $loading['user_name']; ?></td>
										</tr>
									</table>
								</div>
							</div>

							<div class="col-md-3">
								<div class="detail-caption">
									<h1>Loading Status</h1>
									<div class="detail-status
										<?php
											if($loading['status'] == 'Open' || $loading['status'] == 'In Progress')
												echo 'status-green';
											else
												echo 'status-blue';
										?>" style="font-size: 26px;">
										<?php echo $loading['status']; ?>
									</div>
								</div>
							</div>

						<?php }else{ ?>

							<div class="col-md-12">
								<div align="center">
									<h4>It looks like you have no Loading Document yet...</h4>
									<?php if(empty($loading) && !empty($picking)){ ?>
										<a class="btn blue" href="<?php echo base_url(); ?>loading/add">CREATE NEW DOCUMENT</a>
									<?php } ?>
								</div>
							</div>
							<div>&nbsp;</div>

						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

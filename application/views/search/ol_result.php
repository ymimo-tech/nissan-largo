	<div class="row">
		<div class="col-md-12">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject font-green-sharp bold uppercase">
							<i class="fa fa-search"></i><i class="icon-plus-sign-alt"></i> GID Info
						</span>
					</div>
					<div class="tools">
						<a href="" class="collapse" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<input type="hidden" id="type" name="type" value="<?php echo $type; ?>" />
					<input type="hidden" id="ol" name="ol" value="<?php echo $ol; ?>" />
					<div class="row">
						<div class="col-md-12">
							<table id="table-list-sn-movement" class="table table-striped table-bordered table-hover report-table">
								<thead>
									<tr>
										<th style="display: none;">id</th>
										<th style="width: 30px; vertical-align: middle; text-align: center;">No</th>
										<th style="vertical-align: middle; text-align: center;">Item Code</th>
										<th style="vertical-align: middle; text-align: center;">Item Name</th>
										<th style="vertical-align: middle; text-align: center;">LPN</th>
										<th style="vertical-align: middle; text-align: center;">Location</th>
										<th style="vertical-align: middle; text-align: center;">Received Time</th>
										<th style="vertical-align: middle; text-align: center;">Received By</th>
										<th style="vertical-align: middle; text-align: center;">Picked Time</th>
										<th style="vertical-align: middle; text-align: center;">Picked By</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div>&nbsp;</div>
			</div>
		</div>
	</div>

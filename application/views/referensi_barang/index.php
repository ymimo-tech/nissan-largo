<style>
	.filter-container .select2-container {
		width: 100% !important;
		z-index: 9;
	}

	.select2-container {
		z-index: 100000;
	}
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>ITEM MASTER
				<small>Master data of all items/materials</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>

</div>

<div class="row">
	<div class="col-md-12 col-sm-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-th-list"></i> Items List
					</span>
				</div>
				<div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
					<a class="btn blue btn-sm data-table-add min-width120">
						<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>

					<?php if ($this->access_right->otoritas('print')) : ?>

					<a class="btn green btn-sm data-table-export min-width120">
						<i class="fa fa-file-excel-o"></i> EXPORT </a>

					<?php endif; ?>
					<a class="btn green-haze btn-sm btn-sync min-width120">
						<i class="fa fa-refresh"></i> SYNC </a>
					<!--
                    <a href="javascript:;" class="btn btn-default btn-sm">
                        <i class="fa fa-print"></i> Print </a>
					-->
				</div>
			</div>
			<div class="portlet-body">

				<!--COPY THIS SECTION FOR FILTER CONTAINER-->
				<div class="portlet box blue-hoki filter-container">
					<div class="portlet-title">
						<div class="caption">
							Filter
						</div>
						<div class="tools">
							<a href="javascript:;" class="filter-indicator">Show filter</a>
							<a href="javascript:;" class="expand show-hide">
							</a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<!--YOUR CUSTOM FILTER HERE-->
						<form id="form-search" class="form-horizontal" role="form">
							<div class="form-body">


								<div class="row">
									<div class="col-md-6">

										<div class="form-group item-group">
											<label for="item" class="col-md-4">Item</label>
											<div class="col-md-8">
												<div class="custom-select select-type4 high location">
												<select class="form-control"
																name="item" id="item" style="width: 100%;" multiple data-placeholder="-- All --">
															</select>
												</div>
												<span class="help-block item-msg"></span>
											</div>
										</div>

										<div class="form-group cat-group">
											<label for="cat" class="col-md-4">Category</label>
											<div class="col-md-8">
												<?php echo form_dropdown('id_kategori_1', $categories, '', 'class="col-md-12 form-control" style="width: 100%;" id="cat-1"'); ?>
												<span class="help-block cat-msg"></span>
											</div>
										</div>
										<!--
										<div class="form-group cat-group">
											<label for="cat" class="col-md-4">Pre Order</label>
											<div class="col-md-8">
												<select id="preorder" class="form-control">
												</select>
												<span class="help-block cat-msg"></span>
											</div>
										</div>
										-->
										<div class="form-group status-group">
											<label for="status" class="col-md-4">Status</label>
											<div class="col-md-8">
												<select id="status" class="form-control">
												</select>
												<span class="help-block status-msg"></span>
											</div>
										</div>
										<!--
										<div class="form-group cat2-group">
											<label for="cat2" class="col-md-4">Category 2</label>
											<div class="col-md-8">
												<?php echo form_dropdown('id_kategori_2', $categories2, '', 'class="col-md-12 form-control" style="width: 100%;" id="cat-2"'); ?>
												<span class="help-block cat2-msg"></span>
											</div>
										</div>

 										-->
									</div>

									<div class="col-md-6">

										<div class="form-group ship-group">
											<label for="ship" class="col-md-3">Picking Strategy</label>
											<div class="col-md-8">

												<select id="ship" class="form-control">
													<?php
														echo '<option value="">-- All --</option>';
														foreach($strategy as $row){
															echo '<option value="'.$row['shipment_type_code'].'">'.$row['shipment_type'].'</option>';
														}
													?>
												</select>

												<span class="help-block ship-msg"></span>
											</div>
										</div>

										<div class="form-group period-group">
											<label for="period" class="col-md-3">Picking Period</label>
											<div class="col-md-8">

												<select id="period" class="form-control">
													<?php
														echo '<option value="">-- All --</option>';
														foreach($period as $row){
															echo '<option value="'.$row['periode_code'].'">'.$row['periode'].'</option>';
														}
													?>
												</select>

												<span class="help-block period-msg"></span>
											</div>
										</div>

									</div>

								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-right">
											<button id="reset" type="button" class="btn default"
												style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i>
												RESET</button>
											<button id="search" type="submit" class="btn green-haze"
												style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!--END-->
					</div>
				</div>
				<!--END SECTION-->

				<table class="table table-striped table-bordered table-hover table-checkable order-column"
					id="datatable_ajax">
					<thead>
						<tr>
							<th class="table-checkbox">
								<input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes" />
							</th>
							<th style="vertical-align:top;text-align:center"> No </th>
							<th style="vertical-align:top;text-align:center"> Item Code </th>
							<th style="vertical-align:top;text-align:center"> Item Name </th>
							<th style="vertical-align:top;text-align:center"> Category </th>
							<th style="vertical-align:top;text-align:center"> UoM </th>
							<th style="vertical-align:top;text-align:center"> Multi Qty</th>
							<th style="vertical-align:top;text-align:center"> Def Qty</th>
							<th style="vertical-align:top;text-align:center"> Min Qty</th>
							<th style="vertical-align:top;text-align:center;display: none;"> batch </th>
							<th style="vertical-align:top;text-align:center"> Picking Strategy </th>
							<th style="vertical-align:top;text-align:center"> Picking Period </th>
							<th style="vertical-align:top;text-align:center"> Status </th>
							<!-- <th> Pre Order </th> -->
							<th style="vertical-align:top;text-align:center"> Action </th>
						</tr>
					</thead>
					<tbody> </tbody>

				</table>
			</div>
			<div>&nbsp;</div>
			<?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
			<div id="form-content" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<form id="referensi-barang-form" class="form-horizontal">
							<input type="hidden" name="id" />
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="col-md-3 control-label">Item Code<span
													class="required">*</span></label>
											<div class="controls col-md-9">
												<?php echo form_input('kd_barang','', 'class="col-md-12 form-control"') ?>
												<input type="hidden" name="ori_kd_barang" />
											</div>
										</div>
											<div class="form-group">
                                            <label class="col-md-3 control-label">SKU SAP<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('sku','', 'class="col-md-12 form-control"') ?>
                                            </div>
                                        </div>
 																	 			
										<div class="form-group">
											<label class="col-md-3 control-label">Item Name<span
													class="required">*</span></label>
											<div class="controls col-md-9">
												<?php echo form_input('nama_barang', '', 'class="col-md-12 form-control"') ?>

											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Item Category<span
													class="required">*</span></label>
											<div class="controls col-md-9">
												<?php echo form_dropdown('id_kategori_1', $categories, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>
											</div>
										</div>

										<div class="form-group" style="display:none">
											<label class="col-md-3 control-label">Parent/Child<span
													class="required">*</span></label>
											<div class="controls col-md-9">
													<select class="col-md-12 form-control" style="width: 100%;" name="parent_child" id="parent_child">
														<option value="">--------------Please Select--------------</option>
														<option value="1">Parent</option>
														<option value="2">Child</option>
													</select>


											</div>
										</div>


										<div class="form-group" style="display:none">
											<label class="col-md-3 control-label">Item Subtitute<span
													class="required">*</span></label>
											<div class="controls col-md-9">
												<div class="custom-select select-type4 high location">
													<select class="form-control"
														name="item_subtitute" id="item_subtitute" style="width: 100%;" multiple data-placeholder="-- All --">

													</select>
												</div>
												<span class="help-block item-msg"></span>
											</div>
										</div>



										</div>
										<!--  DISABLE UNTUK SALUBRITAS -->
										<!-- <div class="form-group">
                                            <label class="col-md-3 control-label">Item Category 2<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_dropdown('id_kategori_2', $categories2, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>
                                            </div>
                                        </div> -->
										<!--<div class="form-group">
                                            <label class="col-md-3 control-label">UoM<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_dropdown('id_satuan', $satuan, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>
                                            </div>
                                        </div>-->
										<!--
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Base QTY<span class="required">*</span></label>
                                            <div class="controls col-md-6">
                                                <?php echo form_input('base_qty', '', 'class="col-md-12 form-control"') ?>
                                            </div>
                                            <div class="controls col-md-3">
                                                <?php echo form_dropdown('child_satuan', $satuan, '', 'id="child_satuan" class="col-md-12 form-control" style="width: 100%;"'); ?>
                                            </div>
                                        </div>
																			  -->
										<div class="form-group" style="display: none;">
											<label class="col-md-3 control-label">Item Type </label>
											<div class="controls col-md-9">
												<?php echo form_input('tipe_barang', '', 'class="col-md-12 form-control"') ?>

											</div>
										</div>

										<div class="row">

											<div class="col-md-3"></div>

											<div class="col-md-3">

												<div class="form-group">

													<label class="col-md-5 control-label"> Height <span
															class="required">*</span></label>
													<div class="controls col-md-7">
														<input type="text" name="height" class="form-control"
															id="height">
													</div>

												</div>

											</div>

											<div class="col-md-3">

												<div class="form-group">

													<label class="col-md-5 control-label"> Length <span
															class="required">*</span></label>
													<div class="controls col-md-7">

														<input type="text" name="length" class="form-control"
															id="length">
														<!-- <?php echo form_input('length', '', 'class="col-md-8 form-control"') ?> -->
													</div>

												</div>

											</div>

											<div class="col-md-3">

												<div class="form-group">

													<label class="col-md-5 control-label"> Width <span
															class="required">*</span></label>
													<div class="controls col-md-7">
														<input type="text" name="width" class="form-control" id="width">
														<!-- <?php echo form_input('width', '', 'class="col-md-12 form-control"') ?> -->

													</div>

												</div>

											</div>

										</div>

										<div class="form-group">

											<label class="col-md-3 control-label"> Volume <span
													class="required">*</span></label>
											<div class="controls col-md-7">
												<input type="text" class="form-control" id="volume" disabled="disabled">
											</div>
											<div class="controls col-md-2">
												<h4><?php echo VOLUME_UOM; ?></h4>
											</div>

										</div>

										<div class="form-group">

											<label class="col-md-3 control-label"> Weight <span
													class="required">*</span></label>
											<div class="controls col-md-7">
												<input type="text" name="weight" class="form-control" id="weight">
											</div>
											<div class="controls col-md-2">
												<h4><?php echo WEIGHT_UOM; ?></h4>
											</div>

										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-6 control-label">Has Multi Qty </label>
													<div class="controls col-md-6">
														<label class="control-label">
															<input type="checkbox" id="multiQty" name="multiQty"
																class="form-control" />
															YES
														</label>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-4 control-label">Qty/Unit<span
															class="required">*</span></label>
													<div class="controls col-md-3" style="padding:0;">
														<?php echo form_input('DefQty', '', 'class="col-md-12 form-control" disable id="DefQty"') ?>

													</div>
													<div class="controls col-md-5">
														<?php echo form_dropdown('id_satuan', $satuan, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-5">
												<div class="form-group">
													<label class="col-md-7 control-label">Has Batch </label>
													<div class="controls col-md-5">
														<label class="control-label">
															<input type="checkbox" id="batch" name="batch"
																class="form-control" />
															YES
														</label>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-6 control-label">Has Expiry Date </label>
													<div class="controls col-md-6">
														<label class="control-label">
															<input type="checkbox" id="expdate" name="expdate"
																class="form-control" />
															YES
														</label>
													</div>
												</div>
											</div>

										</div>

										<label for="unitofmeasure"><strong>QUARANTINE</strong></label>
										<div class="form-group">
											<!-- <label class="col-md-7 control-label">AUTO QUARANTINE </label> -->
											<div class="controls col-md-5">
												<label class="control-label">
													<input type="checkbox" id="quarantine" name="quarantine"
														class="form-control" onclick="myQuarantine()" />
													AUTO QUARANTINE
												</label>
											</div>
										</div>
										<div class="form-group" id="duration" style="display:none;">
											<label class="col-md-3 control-label">DURATION </label>
											<div class="controls col-md-6">
												<label class="control-label">
													<input type="number" id="quarantineduration"
														name="quarantineduration" class="form-control" value=""
														placeholder="DAYS" />
												</label>
											</div>
										</div>
										<div class="form-group">
											<!-- <label class="col-md-6 control-label">Auto Release </label> -->
											<div class="controls col-md-6">
												<label class="control-label">
													<input type="checkbox" id="autorelease" name="autorelease"
														class="form-control" />
													AUTO RELEASE
												</label>
											</div>
										</div>

										<!--
										 										<div class="row">

																					<div class="col-md-5">
																						<div class="form-group">
																							<label class="col-md-7 control-label"> Pre Order </label>
																							<div class="controls col-md-5">
																								<label class="control-label">
																									<input type="checkbox" id="preorder" name="preorder" class="form-control" />
																									YES
																								</label>
																							</div>
																						</div>
																					</div>

																				</div>
										 										-->
										<div class="form-group strategy">
											<label class="col-md-3 control-label">Picking Strategy<span
													class="required">*</span></label>
											<div class="controls col-md-9">
												<?php echo form_dropdown('shipment_type', $shipment_types, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>

											</div>
										</div>
										<div class="form-group strategy">
											<label class="col-md-3 control-label">Picking Period<span
													class="required">*</span></label>
											<div class="controls col-md-9">
												<?php echo form_dropdown('fifo_period', $fifo_periods, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>

											</div>
										</div>

										<div class="form-group">
											<label class="col-md-3 control-label">Min. Replenish Qty </label>
											<div class="controls col-md-9">
												<input type="text" id="min" name="min" class="form-control" value=""
													style="width: 20%; text-align: right;" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Min. Re-Order Qty </label>
											<div class="controls col-md-9">
												<input type="text" id="min_reorder" name="min_reorder"
													class="form-control" value=""
													style="width: 20%; text-align: right;" />
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-3 control-label">Warehouse</label>
											<div class="controls col-md-9">
												<?php
                                                    echo form_dropdown('warehouse[]', $warehouses, '', 'class="col-md-12 form-control" style="width: 100%;" id="warehouse" multiple data-placeholder="--Please Select--"');
                                                ?>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-3 control-label">Item Area</label>
											<div class="controls col-md-9">
												<!-- <?php
                                                    //echo form_dropdown('item_area', $locations, '', 'class="col-md-12 form-control" style="width: 100%;" id="locations" data-placeholder="--Please Select--"');
                                                ?> -->
												<input type="text" name='item_area' id='item_area' placeholder="please fill out Bin Location"
													class="form-control" style="width: 100%;"/>
											</div>
										</div>

										<!-- DISABLE FOR SALUBRITAS -->
										<!-- <div class="form-group">
                                            <label class="col-md-3 control-label">Company<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_dropdown('id_owner', $owners, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>

                                            </div>
                                        </div> -->


										<div class="form-group">
											<label class="col-md-3 control-label"> Conversion (View Only)</label>
											<div class="col-md-9">
												<div class="row">
													<div class="controls col-md-3">
														<div>
															<input type="text" name='convert' id='convert'
																class="form-control" value="1" style="width: 100%;"
																disabled />
														</div>
														<div style="margin-top:5px;">
															<?php echo form_dropdown('unit_id', $satuan, '', 'class="col-md-12 form-control" style="width: 100%;" disabled'); ?>
														</div>
													</div>
													<div class="controls col-md-1" style="margin-top: 20px;">
														<h4>=</h4>
													</div>
													<div class="controls col-md-3">
														<div>
															<input type="text" id="convert_2" name="convert_2"
																class="form-control" value="" style="width: 100%;" />
														</div>
														<div style="margin-top:5px;">
															<?php echo form_dropdown('unit_id_2', $satuan, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>
														</div>
													</div>
													<div class="controls col-md-1" style="margin-top: 20px;">
														<h4>=</h4>
													</div>
													<div class="controls col-md-3">
														<div>
															<input type="text" id="convert_3" name="convert_3"
																class="form-control" value="" style="width: 100%;" />
														</div>
														<div style="margin-top:5px;">
															<?php echo form_dropdown('unit_id_3', $satuan, '', 'class="col-md-12 form-control" style="width: 100%;"'); ?>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="modal-footer">
								<div class="form-actions">
									<span class="required pull-left">* is required</span>
									<a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
									<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
			<?php }?>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<script>
	function myQuarantine() {
		var checkbox = document.getElementById('quarantine');
		var duration = document.getElementById('duration');

		if (checkbox.checked == true) {
			duration.style.display = 'block';
		} else {
			duration.style.display = 'none';
		}
	}
</script>

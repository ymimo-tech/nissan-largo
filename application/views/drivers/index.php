<style>
    .filter-container .select2-container{
        width: 100% !important;
        z-index: 9;
    }

    .select2-container{
        z-index: 100000;
    }
</style>

<div class="row">

	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		<div class="custom-page-title">
			<h1>Driver
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-cogs"></i><?php echo $title;?>
					</span>
				</div>
                <div class="actions">
					<?php if ($this->access_right->otoritas('add')) : ?>
						<a class="btn blue btn-sm data-table-add min-width120">
							<i class="fa fa-file-o"></i> NEW </a>
					<?php endif; ?>

					<?php if ($this->access_right->otoritas('print')) : ?>

						<a  class="btn green btn-sm data-table-export min-width120">
							<i class="fa fa-file-excel-o"></i> EXPORT </a>

					<?php endif; ?>
                </div>
            </div>
            <div class="portlet-body">

                <!--COPY THIS SECTION FOR FILTER CONTAINER-->
                <div class="portlet box blue-hoki filter-container">
                    <div class="portlet-title">
                        <div class="caption">
                            Filter
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="filter-indicator">Show filter</a>
                            <a href="javascript:;" class="expand show-hide">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display: none;">
                        <!--YOUR CUSTOM FILTER HERE-->
                        <form id="form-search" class="form-horizontal" role="form">
                            <div class="form-body"> 
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        
                                        <div class="form-group item-group">
                                            <label for="item" class="col-md-3">Driver Code</label>
                                            <div class="col-md-9">
                                                <input type="text" name="driver_code_filter" class="form-control">
                                                <span class="help-block item-msg"></span>
                                            </div>
                                        </div>

                                    </div>  
                                    <div class="col-md-6">

                                        <div class="form-group item-group">
                                            <label for="item" class="col-md-3">Driver Name</label>
                                            <div class="col-md-9">
                                                <input type="text" name="driver_name_filter" class="form-control">
                                                <span class="help-block item-msg"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-right">
                                            <button id="reset" type="button" class="btn default" style="width: 120px;"><i class="fa fa-arrow-circle-o-left"></i> RESET</button>
                                            <button id="search" type="submit" class="btn green-haze" style="width: 120px;"><i class="fa fa-search"></i> SEARCH</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!--END-->
                    </div>
                </div>
                <!--END SECTION-->

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="datatable_ajax">
                    <thead>
                        <tr>
                            <th rowspan="2" style="vertical-align: middle; text-align:center;"> No </th>
                            <th rowspan="2" style="vertical-align: middle; text-align:center;"> Type </th>
                            <th rowspan="2" style="vertical-align: middle; text-align:center;"> Driver Name </th>
                            <th rowspan="2" style="vertical-align: middle; text-align:center;"> Vehicle Plate </th>
                            <th rowspan="2" style="vertical-align: middle; text-align:center;"> Description </th>
                            <th rowspan="1" colspan="3"  style="vertical-align: middle; text-align:center;"> Dimensi </th>
                            <th rowspan="2" style="vertical-align: middle; text-align:center;"> Production </th>
                            <th rowspan="2" style="vertical-align: middle; text-align:center;"> Vehicle Type </th>
                            <th rowspan="2" style="vertical-align: middle; text-align:center;"> Action </th>
                        </tr>
                        <tr>
                            <th style="text-align: center; width:5%">Panjang</th>
                            <th style="text-align: center; width:5%">Lebar</th>
                            <th style="text-align: center;width:5%">Tinggi</th>
                        </tr>

                    </thead>
                    <tbody> </tbody>

                </table>
            </div>
            <div>&nbsp;</div>
            <?php if ($this->access_right->otoritas('add') OR $this->access_right->otoritas('edit')) { ?>
            <div id="form-content" class="modal fade" role="basic">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="referensi-supplier-form" class="form-horizontal">
                            <input type="hidden" name="id"/>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?php echo (isset($title)) ? $title : 'Untitle'; ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Drive Name<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('driver_name','', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Type<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('driver_type', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Description<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('description', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Length<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('length', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Height<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('height', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Width<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('width', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Production Date<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('production_date', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Vehicle Type<span class="required">*</span></label>
                                            <div class="controls col-md-9">
                                                <?php echo form_input('vehicle_plate', '', 'class="col-md-12 form-control"') ?>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-actions">
                                    <span class="required pull-left">* is required</span>
                                    <a class="form-close btn default"><i class="fa fa-close"></i> BACK</a>
									<button type="submit" class="btn blue"><i class="fa fa-check"></i> SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
